package hk.org.ha.model.pms.biz.drug;

import javax.ejb.Local;

@Local
public interface MsFmStatusServiceLocal {
	
	void retrieveMsFmStatus(String fmStatus);
	
	boolean isRetrieveSuccess();
	
	void destroy();
	
}
 