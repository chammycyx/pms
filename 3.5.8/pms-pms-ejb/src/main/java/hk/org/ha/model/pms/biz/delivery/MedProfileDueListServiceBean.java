package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.reftable.ActiveWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("medProfileDueListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MedProfileDueListServiceBean implements MedProfileDueListServiceLocal {

	private static final String MED_PROFILE_QUERY_HINT_FETCH_1 = "m.medCase";
	
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_1 = "i.medProfile.medCase";
	private static final String MED_PROFILE_ITEM_QUERY_HINT_BATCH_2 = "i.medProfileMoItem.medProfilePoItemList";
	
	@In
	private Workstore workstore;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private ActiveWardManagerLocal activeWardManager;	
	
	@Out
	private List<MedProfile> medProfileDueList;
	
	@Out
	protected List<MedProfileItem> medProfileItemDueList;	
	
    public MedProfileDueListServiceBean() {
    }
    
    @Override
	public void retrieveMedProfileDueList(String caseNum, String hkid) {
    	
    	medProfileDueList = new ArrayList<MedProfile>();
    	medProfileItemDueList = new ArrayList<MedProfileItem>();
    	
    	medProfileDueList = fetchMedProfileList(caseNum, hkid);
    	
    	if (medProfileDueList.size() == 1) {
    		medProfileItemDueList = fetchMedProfileItemList(medProfileDueList.get(0).getId());
    	} else if (medProfileDueList.size() > 1) {
    		fetchMedProfileItemList(buildMedProfileIdList(medProfileDueList));
    	}
    }
    
    @Override
	public void retrieveMedProfileItemDueList(Long medProfileId) {
    	
    	medProfileItemDueList = fetchMedProfileItemList(medProfileId);
    }
    
    private List<Long> buildMedProfileIdList(List<MedProfile> medProfileList) {
    	
    	Set<Long> idSet = new TreeSet<Long>();
    	for (MedProfile medProfile: medProfileList) {
    		idSet.add(medProfile.getId());
    	}
    	
    	return new ArrayList<Long>(idSet);
    }
    
    @SuppressWarnings("unchecked")
    private List<MedProfile> fetchMedProfileList(String caseNum, String hkid) {
    	
    	return em.createQuery(
    			"select m from MedProfile m" + // 20120312 index check : MedCase.caseNum Patient.hkid : I_MED_CASE_01 I_PATIENT_02
    			" where m.status = :activeStatus" +
    			" and m.hospCode = :hospCode" +
    			" and (m.medCase.caseNum = :caseNum" +
    			" or m.patient.hkid = :hkid)")
    			.setParameter("hospCode", workstore.getHospCode())
    			.setParameter("activeStatus", MedProfileStatus.Active)
    			.setParameter("caseNum", caseNum)
    			.setParameter("hkid", hkid)
    			.setHint(QueryHints.FETCH, MED_PROFILE_QUERY_HINT_FETCH_1)
    			.getResultList();
    }
    
    private List<MedProfileItem> fetchMedProfileItemList(Long medProfileId) {
    	return fetchMedProfileItemList(Arrays.asList(medProfileId));
    }
    
    @SuppressWarnings("unchecked")
	private List<MedProfileItem> fetchMedProfileItemList(List<Long> medProfileIdList) {
    	
    	List<MedProfileItem> medProfileItemList = QueryUtils.splitExecute(em.createQuery(
    			"select i from MedProfileItem i"+ // 20120312 index check : MedProfileItem.medProfile : FK_MED_PROFILE_ITEM_01
    			" where i.status = :verifiedStatus" +
    			" and i.medProfile.id in :medProfileIdList" +
    			" and (i.medProfileMoItem.transferFlag = false or i.medProfileMoItem.transferFlag is null)" +
    			" and (i.medProfileMoItem.frozenFlag = false or i.medProfileMoItem.frozenFlag is null)" +
    			" and i.medProfileMoItem.suspendCode is null and i.medProfileMoItem.pendingCode is null" +
    			" and (i.medProfileMoItem.pivasFlag = false or i.medProfileMoItem.pivasFlag is null)" +
    			" order by i.medProfileMoItem.itemNum")
    			.setParameter("verifiedStatus", MedProfileItemStatus.Verified)
    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_1)
    			.setHint(QueryHints.BATCH, MED_PROFILE_ITEM_QUERY_HINT_BATCH_2), "medProfileIdList", medProfileIdList);
    	
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_1);
    	MedProfileUtils.lookup(medProfileItemList, MED_PROFILE_ITEM_QUERY_HINT_BATCH_2);
    	medProfileItemList = filterAndCountQualifedItems(medProfileItemList);
    	
    	return medProfileItemList;
    }
    
    private void initInactivePasWardFlags(List<MedProfileItem> medProfileItemList) {
    	
    	Date now = new Date();
    	
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		activeWardManager.initInvalidPasWardFlag(medProfileItem.getMedProfile(), now);
    	}
    }
        
    private List<MedProfileItem> filterAndCountQualifedItems(List<MedProfileItem> medProfileItemList) {

    	initInactivePasWardFlags(medProfileItemList);
    	
    	List<MedProfileItem> resultList = new ArrayList<MedProfileItem>();
    	for (MedProfileItem medProfileItem: medProfileItemList) {
    		boolean added = false; 
    		for (MedProfilePoItem medProfilePoItem: medProfileItem.getMedProfileMoItem().getMedProfilePoItemList()) {
    			if (isQualifiedItem(medProfileItem, medProfilePoItem)) {
        			if (!added) {
        				resultList.add(medProfileItem);
        				added = true;
        			}
        			medProfileItem.getMedProfile().incrementNewItemCount();
    			}
    		}
    	}
    	return resultList;
    }
    
    private boolean isQualifiedItem(MedProfileItem medProfileItem, MedProfilePoItem medProfilePoItem) {
    	return (medProfileItem.getMedProfileMoItem().isManualItem() || !Boolean.TRUE.equals(medProfileItem.getMedProfile().getInactivePasWardFlag())) &&
    			(Boolean.TRUE.equals(medProfileItem.getMedProfile().getPrivateFlag()) || !medProfilePoItem.isSfiItem()) &&
    			medProfilePoItem.getLastDueDate() == null;
    }
    
    @Remove
	@Override
	public void destroy() {
	}
}
