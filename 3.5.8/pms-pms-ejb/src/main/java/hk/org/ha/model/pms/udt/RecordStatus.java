package hk.org.ha.model.pms.udt;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum RecordStatus implements StringValuedEnum {

	Active("A", "Active"),
	Delete("D", "Delete"),
	Suspend("S", "Suspend");
	
    public static final List<RecordStatus> Not_Delete = Arrays.asList(
    		StringValuedEnumReflect.getValuesExclude(RecordStatus.class, Delete));
		
    private final String dataValue;
    private final String displayValue;
        
    RecordStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static RecordStatus dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(RecordStatus.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<RecordStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<RecordStatus> getEnumClass() {
    		return RecordStatus.class;
    	}
    }
}
