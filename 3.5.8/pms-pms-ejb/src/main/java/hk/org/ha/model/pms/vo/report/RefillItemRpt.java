package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class RefillItemRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date dateFrom;
	
	private Date dateTo;
	
	private String statusFilter;
	
	private RefillItemRptSummary refillItemRptSummary;
	
	private List<RefillItemRptDtl> refillItemRptDtlList;
	
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public String getStatusFilter() {
		return statusFilter;
	}

	public void setStatusFilter(String statusFilter) {
		this.statusFilter = statusFilter;
	}

	public List<RefillItemRptDtl> getRefillItemRptDtlList() {
		return refillItemRptDtlList;
	}

	public void setRefillItemRptDtlList(List<RefillItemRptDtl> refillItemRptDtlList) {
		this.refillItemRptDtlList = refillItemRptDtlList;
	}

	public void setRefillItemRptSummary(RefillItemRptSummary refillItemRptSummary) {
		this.refillItemRptSummary = refillItemRptSummary;
	}

	public RefillItemRptSummary getRefillItemRptSummary() {
		return refillItemRptSummary;
	}




	
}
