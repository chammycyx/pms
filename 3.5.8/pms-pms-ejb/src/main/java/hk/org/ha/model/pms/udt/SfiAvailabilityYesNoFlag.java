package hk.org.ha.model.pms.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum SfiAvailabilityYesNoFlag implements StringValuedEnum {

	// For MsWorkstoreDrug.sfiDeactivate
	Yes("N", "Yes"),
	No("Y", "No");
	
    private final String dataValue;
    private final String displayValue;

    SfiAvailabilityYesNoFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    

    public static SfiAvailabilityYesNoFlag dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(SfiAvailabilityYesNoFlag.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<SfiAvailabilityYesNoFlag> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<SfiAvailabilityYesNoFlag> getEnumClass() {
    		return SfiAvailabilityYesNoFlag.class;
    	}
    }
}



