package hk.org.ha.model.pms.biz.reftable.mp;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.cache.PasSpecialtyCacherInf;
import hk.org.ha.model.pms.corp.cache.DmSiteCacher;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmSite;
import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnSpec;
import hk.org.ha.model.pms.dms.persistence.PmsFixedConcnStatus;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.patient.PasSpecialty;
import hk.org.ha.model.pms.vo.reftable.mp.FixedConcnStatusInfo;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("fixedConcnPrepActivationService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FixedConcnPrepActivationServiceBean implements FixedConcnPrepActivationServiceLocal {
	
	@In
	private Workstore workstore;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private PasSpecialtyCacherInf pasSpecialtyCacher;
	
	private static final String SPACE = " ";
	
	@In
	private DmSiteCacher dmSiteCacher;
	
	@Override
	public FixedConcnStatusInfo retrieveFixedConcnStatusInfo(DmDrug dmDrug) {
		
		FixedConcnStatusInfo fixedConcnStatusInfo = new FixedConcnStatusInfo();
		
		if( dmDrug == null ){
			return fixedConcnStatusInfo;
		}
		
		List<WorkstoreDrug> workstoreDrugList = new ArrayList<WorkstoreDrug>();
		
		if( StringUtils.isNotEmpty(dmDrug.getItemCode()) ){
			WorkstoreDrug wd = workstoreDrugCacher.getWorkstoreDrug(workstore, dmDrug.getItemCode());
			if( wd != null ){
				workstoreDrugList.add(wd);
			}
		}else if( dmDrug.getDrugKey() != null ){
			workstoreDrugList = workstoreDrugCacher.getWorkstoreDrugListByDrugKey(workstore, dmDrug.getDrugKey());
		}
		
		fixedConcnStatusInfo.setWorkstoreDrugList(workstoreDrugList);
		
		List<HospitalMapping> hospitalMappingList = workstore.getHospital().getHospitalMappingList();
			
		if ( !hospitalMappingList.isEmpty() ) {
			for (Iterator<HospitalMapping> itr = hospitalMappingList.iterator();itr.hasNext();) {
				HospitalMapping hospitalMapping = itr.next();
				if ( !hospitalMapping.getPrescribeFlag() ) {
					itr.remove();
				}
			}	
		}
			
		fixedConcnStatusInfo.setHospitalMappingList(hospitalMappingList);
		
		return fixedConcnStatusInfo;
	}

	@Override
	public FixedConcnStatusInfo retrievePmsFixedConcnStatusList( String patHospCode, DmDrug dmDrug ) {
		
		FixedConcnStatusInfo fixedConcnStatusInfo = new FixedConcnStatusInfo();
		
		List<PmsFixedConcnSpec> pmsFixedConcnSpecTempList = new ArrayList<PmsFixedConcnSpec>();
		Map<String, String> ipasSpecMap = new HashMap<String, String>();
		
		List<PasSpecialty> pasSpecialtyList = pasSpecialtyCacher.getPasSpecialtyList();
		for(PasSpecialty pasSpec : pasSpecialtyList){
			if ( patHospCode.equals(pasSpec.getPasHospCode()) && pasSpec.getType() == PasSpecialtyType.InPatient ) {
				PmsFixedConcnSpec fixedConcSpec = new PmsFixedConcnSpec();
				fixedConcSpec.setIpasSpecCode(pasSpec.getPasSpecCode());
				fixedConcSpec.setSpecDesc(pasSpec.getPasDescription());
				pmsFixedConcnSpecTempList.add(fixedConcSpec);
				ipasSpecMap.put(pasSpec.getPasSpecCode(), pasSpec.getPasDescription());
			}
		}
		
		Collections.sort(pmsFixedConcnSpecTempList);
		
		fixedConcnStatusInfo.setPmsFixedConcnSpecTempList(pmsFixedConcnSpecTempList);
		
		List<PmsFixedConcnStatus> pmsFixedConcnStatusList = dmsPmsServiceProxy.retrievePmsFixedConcnStatusList(patHospCode, dmDrug.getItemCode(), dmDrug.getDrugKey());
		
		for(PmsFixedConcnStatus pmsFixedConcnStatus : pmsFixedConcnStatusList){
			for(PmsFixedConcnSpec pmsFixedConcnSpec : pmsFixedConcnStatus.getPmsFixedConcnSpecList()){
				pmsFixedConcnSpec.setPmsFixedConcnStatus( pmsFixedConcnStatus );
				pmsFixedConcnSpec.setSpecDesc( ipasSpecMap.get(pmsFixedConcnSpec.getIpasSpecCode()) );
			}
		}
		
		fixedConcnStatusInfo.setPmsFixedConcnStatusTreeNode( getTreeNode(dmDrug, pmsFixedConcnStatusList) );

		return fixedConcnStatusInfo;
		
	}
	
	private List<Map<String, Object>> getTreeNode( DmDrug dmDrug, List<PmsFixedConcnStatus> pmsFixedConcnStatusList ){
		List<Map<String, Object>> displayNameNode = new ArrayList<Map<String, Object>>();
		Map<String, Object> displayNameMap = new HashMap<String, Object>();
		
		////////////////
		// 1 = display name
		// 2 = form
		// 3 = strength
		// 4 = site
		// 5 = concentration
		// 6 = fixed concentration preparation
		////////////////
		
		displayNameMap.put("treeNodeLevel", 1);
		displayNameMap.put("treeNode", WordUtils.capitalizeFully(dmDrug.getDmDrugProperty().getDisplayname()));
		displayNameMap.put("children", getFormTreeNode(dmDrug, pmsFixedConcnStatusList));
		
		displayNameNode.add(displayNameMap);
		
		return displayNameNode;
	}
	
	private List<Map<String, Object>> getFormTreeNode( DmDrug dmDrug, List<PmsFixedConcnStatus> pmsFixedConcnStatusList ){
		List<Map<String, Object>> formNode = new ArrayList<Map<String, Object>>();
		Map<String, Object> formMap = new HashMap<String, Object>();
		formMap.put("treeNodeLevel", 2);
		
		StringBuilder sb = new StringBuilder();
		sb.append(dmDrug.getMoeRouteFormDesc());
		if (dmDrug.getDmDrugProperty().getSaltProperty() != null) {
			sb.append(SPACE).append("(")
					  .append(dmDrug.getDmDrugProperty().getSaltProperty())
					  .append(")");
		}
		formMap.put("treeNode", WordUtils.capitalizeFully(sb.toString()));
		
		if( StringUtils.isNotBlank(dmDrug.getItemCode()) ){
			formMap.put("children", getStrengthTreeNode(dmDrug, pmsFixedConcnStatusList));
		}else{
			formMap.put("children", getSiteTreeNode(pmsFixedConcnStatusList));	
		}
		
		formNode.add(formMap);
		
		return formNode;
	}
	
	private List<Map<String, Object>> getStrengthTreeNode( DmDrug dmDrug, List<PmsFixedConcnStatus> pmsFixedConcnStatusList ){
		final NumberFormat NUMBERFORMAT = new DecimalFormat( "0.###" );
		List<Map<String, Object>> strengthNode = new ArrayList<Map<String, Object>>();
		Map<String, Object> strengthMap = new HashMap<String, Object>();
		strengthMap.put("treeNodeLevel", 3);
		StringBuilder sb = new StringBuilder();
		sb.append( dmDrug.getStrength() );
		if( dmDrug.getVolumeValue() != null && dmDrug.getVolumeValue() > 0 ){
			sb.append(SPACE);
			sb.append( NUMBERFORMAT.format(dmDrug.getVolumeValue()) );
			sb.append(SPACE);
			sb.append( dmDrug.getVolumeUnit() );
		}
		strengthMap.put("treeNode", sb.toString());
		strengthMap.put("children", getSiteTreeNode(pmsFixedConcnStatusList));
		
		strengthNode.add(strengthMap);
		
		return strengthNode;
	}
	
	private List<Map<String, Object>> getSiteTreeNode( List<PmsFixedConcnStatus> pmsFixedConcnStatusList ){
		Map<String, List<PmsFixedConcnStatus>> siteFcsMap = new HashMap<String, List<PmsFixedConcnStatus>>();
		//for sorting
		List<String> siteIpmoeDescList = new ArrayList<String>();
		for( PmsFixedConcnStatus fcs : pmsFixedConcnStatusList ){
			
			String siteIpmoeDesc = fcs.getSiteCode();
			DmSite dmSite = dmSiteCacher.getSiteBySiteCode(fcs.getSiteCode());
			if( dmSite != null ){
				siteIpmoeDesc = dmSite.getIpmoeDesc();
			}
			
			if( siteFcsMap.get(siteIpmoeDesc) == null ){
				List<PmsFixedConcnStatus> fcsList = new ArrayList<PmsFixedConcnStatus>();
				fcsList.add(fcs);
				siteIpmoeDescList.add(siteIpmoeDesc);
				siteFcsMap.put(siteIpmoeDesc, fcsList);
			}else{
				siteFcsMap.get(siteIpmoeDesc).add(fcs);
			}
		}
		
		Collections.sort(siteIpmoeDescList);
		
		List<Map<String, Object>> siteNode = new ArrayList<Map<String, Object>>();
		for(String ipmoeDesc : siteIpmoeDescList){
			List<PmsFixedConcnStatus> fcsList = siteFcsMap.get(ipmoeDesc);
			Map<String, Object> siteMap = new HashMap<String, Object>();
			siteMap.put("treeNodeLevel", 4);
			siteMap.put("treeNode", ipmoeDesc);
			siteMap.put("children", getFixedConcnNode(fcsList));
			
			siteNode.add(siteMap);
		}

		return siteNode;
	}
	
	private List<Map<String, Object>> getFixedConcnNode( List<PmsFixedConcnStatus> pmsFixedConcnStatusList ){
		Map<String, List<PmsFixedConcnStatus>> fcMap = new HashMap<String, List<PmsFixedConcnStatus>>();
		//for sorting
		List<String> fcKeyList = new ArrayList<String>();
		for( PmsFixedConcnStatus fcs : pmsFixedConcnStatusList ){
			String key = fcs.getDmFixedConcn().getConcnPerMlText();
			if( fcMap.get(key) == null ){
				List<PmsFixedConcnStatus> fcsList = new ArrayList<PmsFixedConcnStatus>();
				fcsList.add(fcs);
				fcKeyList.add(key);
				fcMap.put(key, fcsList);
			}else{
				fcMap.get(key).add(fcs);
			}
		}
		
		List<Map<String, Object>> fcNode = new ArrayList<Map<String, Object>>();
		for(String key : fcKeyList){
			List<PmsFixedConcnStatus> fcsList = fcMap.get(key);
			Map<String, Object> fcPrepMap = new HashMap<String, Object>();
			fcPrepMap.put("treeNodeLevel", 5);
			fcPrepMap.put("treeNode", key);
			fcPrepMap.put("children", getFixedConcnPrepNode(fcsList));
			
			fcNode.add(fcPrepMap);
		}
		
		return fcNode;
	}
	
	private List<Map<String, Object>> getFixedConcnPrepNode( List<PmsFixedConcnStatus> pmsFixedConcnStatusList ){
		List<Map<String, Object>> fcPrepNode = new ArrayList<Map<String, Object>>();
		for( PmsFixedConcnStatus fcs : pmsFixedConcnStatusList ){
			Map<String, Object> fcPrepMap = new HashMap<String, Object>();
			fcPrepMap.put("treeNodeLevel", 6);
			fcPrepMap.put("treeNode", builderPrepDesc(fcs) );
			fcPrepMap.put("prep", fcs);
			
			fcPrepNode.add(fcPrepMap);
		}
		
		return fcPrepNode;
	}
	
	private String builderPrepDesc(PmsFixedConcnStatus fcs){
		StringBuilder sb = new StringBuilder();
		sb.append(fcs.getDmFixedConcn().getConcnText());
		sb.append(SPACE);
		sb.append(fcs.getDiluentCode());
		sb.append(SPACE);
		if( fcs.getLocalStatus() != null && !fcs.getLocalStatus().equals(fcs.getDmFixedConcn().getDefaultStatus()) ){
			sb.append( ("A".equals(fcs.getLocalStatus()))?"[Local Active]":"[Local Inactive]" );
		}else{
			sb.append( ("A".equals(fcs.getDmFixedConcn().getDefaultStatus()))?"[HQ Active]":"[HQ Inactive]" );
		}
		return sb.toString();
	}

	@Override
	public void updateFixedConcnStatusList( List<PmsFixedConcnStatus> pmsFixedConcnStatusList ) {
		dmsPmsServiceProxy.updatePmsFixedConcnStatusList(pmsFixedConcnStatusList);
	}

	@Remove
	public void destroy() 
	{
		
	}

}



