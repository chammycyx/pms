package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class WorkloadStatRpt implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dayEndBatchDate;
	
	private Integer opOrderCount;
	
	private Integer opOrderItemCount;
	
	private Integer opRefillCount;
	
	private Integer opRefillItemCount;
	
	private Integer mpItemCount;
	
	private Integer mpSfiItemCount;

	private Integer mpSafetyNetItemCount;

	private Integer mpWardStockItemCount;
	
	private Integer mpDangerDrugItemCount;

	public Date getDayEndBatchDate() {
		return dayEndBatchDate;
	}

	public void setDayEndBatchDate(Date dayEndBatchDate) {
		this.dayEndBatchDate = dayEndBatchDate;
	}

	public Integer getOpOrderCount() {
		return opOrderCount;
	}

	public void setOpOrderCount(Integer opOrderCount) {
		this.opOrderCount = opOrderCount;
	}

	public Integer getOpOrderItemCount() {
		return opOrderItemCount;
	}

	public void setOpOrderItemCount(Integer opOrderItemCount) {
		this.opOrderItemCount = opOrderItemCount;
	}

	public Integer getOpRefillCount() {
		return opRefillCount;
	}

	public void setOpRefillCount(Integer opRefillCount) {
		this.opRefillCount = opRefillCount;
	}

	public Integer getOpRefillItemCount() {
		return opRefillItemCount;
	}

	public void setOpRefillItemCount(Integer opRefillItemCount) {
		this.opRefillItemCount = opRefillItemCount;
	}

	public Integer getMpItemCount() {
		return mpItemCount;
	}

	public void setMpItemCount(Integer mpItemCount) {
		this.mpItemCount = mpItemCount;
	}

	public Integer getMpSfiItemCount() {
		return mpSfiItemCount;
	}

	public void setMpSfiItemCount(Integer mpSfiItemCount) {
		this.mpSfiItemCount = mpSfiItemCount;
	}

	public Integer getMpSafetyNetItemCount() {
		return mpSafetyNetItemCount;
	}

	public void setMpSafetyNetItemCount(Integer mpSafetyNetItemCount) {
		this.mpSafetyNetItemCount = mpSafetyNetItemCount;
	}

	public Integer getMpWardStockItemCount() {
		return mpWardStockItemCount;
	}

	public void setMpWardStockItemCount(Integer mpWardStockItemCount) {
		this.mpWardStockItemCount = mpWardStockItemCount;
	}

	public Integer getMpDangerDrugItemCount() {
		return mpDangerDrugItemCount;
	}

	public void setMpDangerDrugItemCount(Integer mpDangerDrugItemCount) {
		this.mpDangerDrugItemCount = mpDangerDrugItemCount;
	}
}
