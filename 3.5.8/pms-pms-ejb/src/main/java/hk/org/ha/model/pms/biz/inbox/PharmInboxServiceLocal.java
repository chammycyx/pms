package hk.org.ha.model.pms.biz.inbox;
import java.util.List;

import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import javax.ejb.Local;

@Local
public interface PharmInboxServiceLocal {
	void init();
	void setMedProfileStatTypes(MedProfileStatAdminType adminType, MedProfileStatOrderType orderType);
	void setShowAllWards(Boolean showAllWards);
	void setShowChemoProfileOnly(Boolean showChemoProfileOnly);	
	void setStatTypesAndFilters(MedProfileStatAdminType adminType, MedProfileStatOrderType orderType,
			List<Ward> wardFilterList, List<WardGroup> wardGroupFilterList);
	void retrieveMedProfileStatList();
	void destroy();
}
