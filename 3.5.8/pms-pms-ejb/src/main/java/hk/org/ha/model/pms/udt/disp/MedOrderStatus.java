package hk.org.ha.model.pms.udt.disp;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedOrderStatus implements StringValuedEnum {

	Outstanding("O", "Outstanding"),
	Fulfilling("F", "Fulfilling"),
	Complete("C", "Complete"),
	Deleted("D", "Deleted"),
	SysDeleted("X", "SysDeleted"),
	MoeSuspended("S", "MoeSuspended"),
	PreVet("P", "PreVet"),
	Withhold("W", "WithHold");
	
    public static final List<MedOrderStatus> Fulfilling_Complete = Arrays.asList(Fulfilling,Complete);
    public static final List<MedOrderStatus> Fulfilling_Complete_Delete = Arrays.asList(Fulfilling,Complete,Deleted);
    public static final List<MedOrderStatus> Outstanding_Deleted_Withhold_PreVet = Arrays.asList(Outstanding,Deleted,Withhold,PreVet);
    public static final List<MedOrderStatus> Complete_Deleted = Arrays.asList(Complete,Deleted);
    public static final List<MedOrderStatus> Deleted_SysDeleted = Arrays.asList(Deleted,SysDeleted);
    public static final List<MedOrderStatus> Outstanding_Withhold = Arrays.asList(Outstanding,Withhold);
    public static final List<MedOrderStatus> Outstanding_Withhold_PreVet = Arrays.asList(Outstanding,Withhold,PreVet);

    public static final List<MedOrderStatus> Not_Outstanding_Deleted_SysDeleted = Arrays.asList(
    		StringValuedEnumReflect.getValuesExclude(MedOrderStatus.class,Outstanding,Deleted,SysDeleted));
    
    private final String dataValue;
    private final String displayValue;
    
    MedOrderStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }    

    public static MedOrderStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedOrderStatus.class, dataValue);
	}
    
	public static class Converter extends StringValuedEnumConverter<MedOrderStatus> {

		private static final long serialVersionUID = 1L;

    	public Class<MedOrderStatus> getEnumClass() {
    		return MedOrderStatus.class;
    	}
    }
}
