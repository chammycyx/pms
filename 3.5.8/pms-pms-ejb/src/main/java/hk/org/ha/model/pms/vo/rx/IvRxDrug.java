package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.dms.biz.formatengine.FormatHints;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatter;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterBase;
import hk.org.ha.model.pms.dms.biz.formatengine.ParsedOrderLineFormatterFactory;
import hk.org.ha.model.pms.dms.udt.formatengine.FormatType;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="IvRxDrug")
@ExternalizedBean(type=DefaultExternalizer.class)
public class IvRxDrug extends RxItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String lineCode;

	@XmlElement
	private String lineDesc;

	@XmlElement
	private String lineRemark;

	@XmlElement(name="IvAdditive")
	private List<IvAdditive> ivAdditiveList;

	@XmlElement(name="IvFluid")
	private List<IvFluid> ivFluidList;
		
	@XmlElement(name="StartAdminRate")
	private AdminRate startAdminRate;	

	@XmlElement(name="EndAdminRate")
	private AdminRate endAdminRate;
	
	@XmlElement(required = true)
	private Integer dispQty;
	
	@XmlElement(required = true)
	private String baseUnit;

	@XmlElement(name="DailyFreq")
	private Freq dailyFreq;

	@XmlElement(required = true)
	private Integer duration;
	
	@XmlElement(required = true)
	private RegimenDurationUnit durationUnit;

	@XmlElement
	private Date orgStartDate;
	
	@XmlElement(required = true)
	private Date startDate;
	
	@XmlElement
	private Date endDate;

	@XmlElement
	private Integer validPeriod;

	@XmlElement
	private String validPeriodUnit;

	@XmlElement
	private Date reviewDate;
			
	@XmlElement
	private String siteCode;
	
	@XmlElement(required = true)
	private String siteDesc;	

	@XmlElement
	private String ipmoeDesc;	
	
	@XmlElement
	private String supplSiteDesc;
	
	@XmlElement
	private String adminTimeCode;
	
	@XmlElement
	private String adminTimeDesc;
	
	@XmlElement
	private String fluidNameHtml;
	
	@XmlElement
	private String fluidNameTlf;
	
	@XmlElement
	private String fluidNameXml;
	
	@XmlTransient
	private String formattedFluidNameTlf;
	
	public String getLineCode() {
		return lineCode;
	}

	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}

	public List<IvFluid> getIvFluidList() {
		if (ivFluidList == null) {
			ivFluidList = new ArrayList<IvFluid>();
		}
		return ivFluidList;
	}

	public void setIvFluidList(List<IvFluid> ivFluidList) {
		this.ivFluidList = ivFluidList;
	}

	public String getLineDesc() {
		return lineDesc;
	}

	public void setLineDesc(String lineDesc) {
		this.lineDesc = lineDesc;
	}

	public String getLineRemark() {
		return lineRemark;
	}

	public void setLineRemark(String lineRemark) {
		this.lineRemark = lineRemark;
	}

	public List<IvAdditive> getIvAdditiveList() {
		if (ivAdditiveList == null) {
			ivAdditiveList = new ArrayList<IvAdditive>();
		}
		return ivAdditiveList;
	}

	public void setIvAdditiveList(List<IvAdditive> ivAdditiveList) {
		this.ivAdditiveList = ivAdditiveList;
	}

	public AdminRate getStartAdminRate() {
		return startAdminRate;
	}

	public void setStartAdminRate(AdminRate startAdminRate) {
		this.startAdminRate = startAdminRate;
	}

	public AdminRate getEndAdminRate() {
		return endAdminRate;
	}

	public void setEndAdminRate(AdminRate endAdminRate) {
		this.endAdminRate = endAdminRate;
	}
	
	public Integer getDispQty() {
		return dispQty;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	
	public Freq getDailyFreq() {
		return dailyFreq;
	}

	public void setDailyFreq(Freq dailyFreq) {
		this.dailyFreq = dailyFreq;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDurationUnit(RegimenDurationUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public RegimenDurationUnit getDurationUnit() {
		return durationUnit;
	}
	
	public Date getOrgStartDate() {
		return orgStartDate == null ? null : new Date(orgStartDate.getTime());
	}

	public void setOrgStartDate(Date orgStartDate) {
		this.orgStartDate = orgStartDate == null ? null : new Date(orgStartDate.getTime());
	}

	public Date getStartDate() {
		return (startDate == null)? null : new Date(startDate.getTime());
	}

	public void setStartDate(Date startDate) {
		this.startDate = (startDate == null) ? null : new Date(startDate.getTime());
	}

	public Date getEndDate() {
		return (endDate == null)? null : new Date(endDate.getTime());
	}

	public void setEndDate(Date endDate) {
		this.endDate = (endDate == null) ? null : new Date(endDate.getTime());
	}

	public Integer getValidPeriod() {
		return validPeriod;
	}

	public void setValidPeriod(Integer validPeriod) {
		this.validPeriod = validPeriod;
	}

	public String getValidPeriodUnit() {
		return validPeriodUnit;
	}

	public void setValidPeriodUnit(String validPeriodUnit) {
		this.validPeriodUnit = validPeriodUnit;
	}

	public Date getReviewDate() {
		return (reviewDate == null)? null : new Date(reviewDate.getTime());
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = (reviewDate == null) ? null : new Date(reviewDate.getTime());
	}	
	
	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}
	
	public String getIpmoeDesc() {
		return ipmoeDesc;
	}

	public void setIpmoeDesc(String ipmoeDesc) {
		this.ipmoeDesc = ipmoeDesc;
	}

	public String getSupplSiteDesc() {
		return supplSiteDesc;
	}

	public void setSupplSiteDesc(String supplSiteDesc) {
		this.supplSiteDesc = supplSiteDesc;
	}

	public String getAdminTimeCode() {
		return adminTimeCode;
	}

	public void setAdminTimeCode(String adminTimeCode) {
		this.adminTimeCode = adminTimeCode;
	}

	public String getAdminTimeDesc() {
		return adminTimeDesc;
	}

	public void setAdminTimeDesc(String adminTimeDesc) {
		this.adminTimeDesc = adminTimeDesc;
	}

	public String getFluidNameHtml() {
		return fluidNameHtml;
	}

	public void setFluidNameHtml(String fluidNameHtml) {
		this.fluidNameHtml = fluidNameHtml;
	}
	
	public String getFluidNameTlf() {
		return fluidNameTlf;
	}

	public void setFluidNameTlf(String fluidNameTlf) {
		this.fluidNameTlf = fluidNameTlf;
	}

	public String getFluidNameXml() {
		return fluidNameXml;
	}

	public void setFluidNameXml(String fluidNameXml) {
		this.fluidNameXml = fluidNameXml;
	}

	public String getFormattedFluidNameTlf() {
		return formattedFluidNameTlf;
	}

	public void setFormattedFluidNameTlf(String formattedFluidNameTlf) {
		this.formattedFluidNameTlf = formattedFluidNameTlf;
	}

	public String getMdsOrderDesc() {
		
		StringBuilder sb = new StringBuilder();
		
		for (IvAdditive ivAdditive : getIvAdditiveList()) {
			if (sb.length() > 0) {
				sb.append(" + ");
			}
			sb.append(ivAdditive.getMdsDisplayName());
		}
		
		Fluid baseFluid;
		if (getIvFluidList().isEmpty()) {
			baseFluid = null;
		}
		else {
			baseFluid = getIvFluidList().get(0);
		}
		
		if (baseFluid != null && baseFluid.getDiluentName() != null && baseFluid.getDiluentCode() != null) {
			sb.append(" in ");
			for (Fluid fluid : getIvFluidList()) {
				if (getIvFluidList().size() > 1) {
					sb.append(fluid.getFluidQty());
				}
				sb.append(fluid.getDiluentAbbr());
			}
		}
		
		return sb.toString();
	}
	
	public void buildTlf(){
		super.buildTlf();
		if( StringUtils.isNotEmpty( fluidNameXml ) ){
			ParsedOrderLineFormatterBase formatter =  ParsedOrderLineFormatterFactory.getParsedOrderLineFormatter(getInterfaceVersion());
			formatter.setHint(FormatHints.FORMAT_TYPE, FormatType.TLF);
			formatter.setHint(FormatHints.LINE_WIDTH, 45);
			formatter.setHint(FormatHints.HIDE_STRIKE_THROUGH, false);
			String result = formatter.format(fluidNameXml);
			formattedFluidNameTlf = result;
		}else if( StringUtils.isNotEmpty( fluidNameTlf ) ){
			formattedFluidNameTlf = fluidNameTlf;
		}
		if( ivAdditiveList != null && !ivAdditiveList.isEmpty() ){
			for( IvAdditive ivAdditive : ivAdditiveList ){
				ivAdditive.setInterfaceVersion(getInterfaceVersion());
				ivAdditive.buildTlf();
			}
		}
	}
}
