package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileStatTrxManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.pivas.PivasWorklistClientNotifierInf;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestGroup;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestGroupStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.security.PropMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dueOrderAsyncPrintManager")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@MeasureCalls
public class DueOrderAsyncPrintManagerBean implements DueOrderAsyncPrintManagerLocal {

	@Logger
	private Log logger;
	
	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private MedProfileStatTrxManagerLocal medProfileStatTrxManager;

	@In
	private PivasWorklistClientNotifierInf pivasWorklistClientNotifier;
	
    public DueOrderAsyncPrintManagerBean() {
    }
    
    @Override
    public void assignBatchAndPrintByDeliveryScheduleItem(Workstation workstation, UamInfo uamInfo, PropMap propMap, DeliveryRequestGroup deliveryRequestGroup, List<DeliveryRequest> requestList) {
    	
		Context context = Contexts.getSessionContext();
		context.set("uamInfo", uamInfo);
		context.set("hospital", workstation.getWorkstore().getHospital());
		context.set("workstore", workstation.getWorkstore());
		context.set("workstation", workstation);
		context.set("propMap", propMap);
		try {
			for(DeliveryRequest deliveryRequest:requestList){
				assignBatchAndPrintForDsi(deliveryRequestGroup, deliveryRequest, workstation);
				
				if( deliveryRequestGroup.getThrowable() != null ){
					break;
				}
			}
		}finally {
			endProcessDeliveryRequestGroup(deliveryRequestGroup);
    		deliveryRequestGroup.setStatusWithNotification(DeliveryRequestGroupStatus.Active);
		}
    }
    
    private void assignBatchAndPrintForDsi(DeliveryRequestGroup group, DeliveryRequest request, Workstation workstation) {

    	try {
        	dueOrderPrintManager.beginProcessDeliveryRequest(workstation, request);
	    	try {
	    		assignBatchAndPrint(request);
	    	} finally {
		    	printRemainingLabelAndReport(request);
		    	endProcessDeliveryRequest(request, workstation);
	    	}
    	} catch (Throwable t) {
    		request.setThrowable(t);
    		group.setThrowable(t);
    		logger.error(t);
    	} finally {
    		request.setStatus(DeliveryRequestStatus.Completed);
    		group.addErrorCount(request.getErrorCount());
    		group.addSuccessCount(request.getSuccessCount());
    	}
    }
    
    @Override
    public void assignBatchAndPrint(Workstation workstation, UamInfo uamInfo, PropMap propMap, DeliveryRequest request) {

    	try {
        	Context context = Contexts.getSessionContext();
        	context.set("uamInfo", uamInfo);
        	context.set("hospital", workstation.getWorkstore().getHospital());
        	context.set("workstore", workstation.getWorkstore());
        	context.set("workstation", workstation);
        	context.set("propMap", propMap);
        	
        	dueOrderPrintManager.beginProcessDeliveryRequest(workstation, request);
    	
	    	try {
	    		assignBatchAndPrint(request);
	    	} finally {
		    	printRemainingLabelAndReport(request);
		    	endProcessDeliveryRequest(request, workstation);
	    	}
    	} catch (Throwable t) {
    		request.setThrowable(t);
    		logger.error(t);
    	} finally {
    		request.setStatusWithNotification(DeliveryRequestStatus.Completed);
    	}
    }
    
    private void assignBatchAndPrint(DeliveryRequest request) {
    	
    	if (Boolean.TRUE.equals(request.getPivasFlag())) {
    		assignBatchAndPrint(request, dueOrderPrintManager.retrieveMedProfileListForPivas(request));
    	} else if (Boolean.TRUE.equals(request.getMaterialRequestFlag())) {
    		assignBatchAndPrint(request, dueOrderPrintManager.retrieveMedProfileListForMaterialRequest(request));
    	} else if (request.isCaseNumOrHkidSelected() || request.isBatchPrint()) {
    		List<MedProfile> medProfileList = dueOrderPrintManager.retrieveMedProfileList(null, request);
    		if (medProfileList.size() <= 1) {
    			assignBatchAndPrint(request, null, medProfileList);
    		} else {
    			assignBatchAndPrint(request, medProfileList);
    		}
		} else {
	    	for (Ward ward: request.getSortedWardSet()) {
	    		assignBatchAndPrint(request, ward, dueOrderPrintManager.retrieveMedProfileList(ward, request));
	    	}
		}
    }
    
    public List<Delivery> retrieveDeliveryListForDrugRefillList(DeliveryRequest request, boolean includeDDnWardStockFlag){
    	List<Delivery> deliveryList = new ArrayList<Delivery>();

    	if (request.isCaseNumOrHkidSelected()) {
    		List<MedProfile> medProfileList = dueOrderPrintManager.retrieveMedProfileList(null, request);
        	for (Entry<String, List<MedProfile>> entry: buildMedProfileMap(medProfileList).entrySet()) {
	    		MedProfileUtils.append(deliveryList, assignBatchForDrugRefillList(request, medProfileList.size() <= 1 ? null : new Ward(entry.getKey()),
	    				entry.getValue(), includeDDnWardStockFlag));
    		}
		} else {
	    	for (Ward ward: request.getSortedWardSet()) {
	    		MedProfileUtils.append(deliveryList, assignBatchForDrugRefillList(request, ward, dueOrderPrintManager.retrieveMedProfileList(ward, request), includeDDnWardStockFlag));
	    	}
		} 
    	return deliveryList;
    }
    
    private Delivery assignBatchForDrugRefillList(DeliveryRequest request, Ward ward, List<MedProfile> medProfileList, boolean includeDDnWardStockFlag) {

    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	String wardCode = null;
    	
		for (MedProfile medProfile: medProfileList) {
			Delivery delivery = dueOrderPrintManager.processMedProfile(request, ward == null, medProfile, false);
			if (delivery != null && delivery.getDeliveryItemList().size() > 0) {
				wardCode = delivery.getWardCode();
				deliveryItemList.addAll(delivery.getDeliveryItemList());
			}
		}
		
		if (deliveryItemList.size() > 0) {
			return dueOrderPrintManager.processDeliveryForDrugRefillList(request, ward == null ? wardCode : ward.getWardCode(), PrintMode.Batch,
					deliveryItemList, includeDDnWardStockFlag);
		}
		return null;
    }
    
    private void assignBatchAndPrint(DeliveryRequest request, List<MedProfile> medProfileList) {
    	
    	for (Entry<String, List<MedProfile>> entry: buildMedProfileMap(medProfileList).entrySet()) {
    		assignBatchAndPrint(request, new Ward(entry.getKey()), entry.getValue());
    	}
    }
    
    private Map<String, List<MedProfile>> buildMedProfileMap(List<MedProfile> medProfileList) {
    	
    	Map<String, List<MedProfile>> medProfileMap = new TreeMap<String, List<MedProfile>>();
    	
    	for (MedProfile medProfile: medProfileList) {
    		String wardCode = medProfile.getWardCode();
    		List<MedProfile> subList = medProfileMap.get(wardCode);
    		if (subList == null) {
    			subList = new ArrayList<MedProfile>();
    			medProfileMap.put(wardCode, subList);
    		}
    		subList.add(medProfile);
    	}
    	return medProfileMap;
    }
    
    private void assignBatchAndPrint(DeliveryRequest request, Ward ward, List<MedProfile> medProfileList) {

    	List<DeliveryItem> deliveryItemList = new ArrayList<DeliveryItem>();
    	
    	String wardCode = null;
    	request.markErrorCounts();

    	boolean success = false;
    	List<MedProfile> lockedMedProfileList = new ArrayList<MedProfile>();
    	boolean isPivas = Boolean.TRUE.equals(request.getPivasFlag());
    	
    	try {
			for (MedProfile medProfile: medProfileList) {
				Delivery delivery = isPivas ?
						dueOrderPrintManager.processMedProfileForPivas(request, medProfile) : dueOrderPrintManager.processMedProfile(request, ward == null, medProfile, true);
				if (delivery.getDeliveryItemList().size() > 0) {
					lockedMedProfileList.add(medProfile);
					wardCode = delivery.getWardCode();
					deliveryItemList.addAll(delivery.getDeliveryItemList());
				}
			}
			
			if (deliveryItemList.size() > 0) {
				dueOrderPrintManager.createAndProcessDeliveryList(request, ward == null ? wardCode : ward.getWardCode(), isPivas ? PrintMode.None : PrintMode.Batch,
						deliveryItemList);
			}
			success = true;
    	} finally {
    		if (!success) {
    			revertDeliveryGenFlags(lockedMedProfileList);
    		}
    	}
		medProfileStatTrxManager.recalculateStat(medProfileList, false, false, true);
    }
    
    private void revertDeliveryGenFlags(List<MedProfile> medProfileList) {

		try {
			dueOrderPrintManager.revertDeliveryGenFlags(medProfileList);
		} catch (Exception ex) {
			logger.error(ex);
		}
		
    }
    
    private void printRemainingLabelAndReport(DeliveryRequest request) {
    	
    	try {
    		dueOrderPrintManager.printRemainingLabelAndReport(request);
    	} catch (Exception ex) {
    		logger.error(ex);
    	}
    }
    
    private void endProcessDeliveryRequestGroup(DeliveryRequestGroup requestGroup) {
    	try {
    		dueOrderPrintManager.endProcessDeliveryRequestGroup(requestGroup);
    	} catch (Exception ex) {
    		logger.error(ex);
    	}
    }
    
    private void endProcessDeliveryRequest(DeliveryRequest request, Workstation workstation) {
    	try {
    		dueOrderPrintManager.endProcessDeliveryRequest(request);
    	} catch (Exception ex) {
    		logger.error(ex);
    	} finally {
    		if (Boolean.TRUE.equals(request.getPivasFlag())) {
    			pivasWorklistClientNotifier.dispatchUpdateEvent(workstation.getWorkstore().getWorkstoreGroup().getWorkstoreGroupCode());
    		}
    	}
    }
}
