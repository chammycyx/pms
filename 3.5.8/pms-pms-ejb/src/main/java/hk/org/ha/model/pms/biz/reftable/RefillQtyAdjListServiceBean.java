package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.RefillQtyAdj;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("refillQtyAdjListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RefillQtyAdjListServiceBean implements RefillQtyAdjListServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public List<RefillQtyAdj> retrieveRefillQtyAdjList()
	{
		return em.createQuery(
				"select o from RefillQtyAdj o" + // 20120303 index check : RefillQtyAdj.workstore : FK_REFILL_QTY_ADJ_01
				" where o.workstore = :workstore" +
				" order by o.formCode")
			.setParameter("workstore", workstore)
			.getResultList();
	}
	
	public void updateRefillQtyAdjList(Collection<RefillQtyAdj> updateRefillQtyAdjList, 
										Collection<RefillQtyAdj> delRefillQtyAdjList)
	{
		workstore = em.find(Workstore.class, workstore.getId());
		
		if(!delRefillQtyAdjList.isEmpty())
		{
			for(RefillQtyAdj delRefillQtyAdj:delRefillQtyAdjList)
			{
				if ( delRefillQtyAdj.getId() != null ) {
					em.remove(em.merge(delRefillQtyAdj));
				}
			}
		}
		em.flush();
		
		for(RefillQtyAdj refillQtyAdj:updateRefillQtyAdjList)
		{
			if(StringUtils.isNotBlank(refillQtyAdj.getFormCode()))
			{
				if(refillQtyAdj.getId()==null){
					refillQtyAdj.setWorkstore(workstore);
					em.persist(refillQtyAdj);
				}
			}
		}
		
		em.flush();
		em.clear();
	}

	@Remove
	public void destroy() 
	{
	}
	
}
