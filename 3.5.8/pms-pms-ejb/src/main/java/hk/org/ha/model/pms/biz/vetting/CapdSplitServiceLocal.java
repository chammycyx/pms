package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.model.pms.vo.vetting.CapdSplit;

import javax.ejb.Local;

@Local
public interface CapdSplitServiceLocal {

	void retrieveCapdSplit();
	
	void updateCapdSplit(CapdSplit capdSplit, boolean setGenNewVoucherFlag);
	
	void clearCapdSplit();
	
	String getErrMsg();
	
	String getErrItemCode();
	
	void destroy();
	
}
