package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.dms.vo.Capd;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(required = true)
	private String itemCode;
	
	@XmlElement
	private String supplierCode;

	@XmlElement(required = true)
	private String concentration;

	@XmlElement(required = true)
	private String fullConcentration;

	@XmlElement(required = true)
	private BigDecimal dosage;
	
	@XmlElement(required = true)
	private String dosageUnit;
	
	@XmlElement(required = true)
	private Boolean prn;
	
	@XmlElement
	private PrnPercentage prnPercent;	
	
	@XmlElement
	private Integer duration;
	
	@XmlElement
	private RegimenDurationUnit durationUnit;
	
	@XmlElement
	private Date orgStartDate;
	
	@XmlElement(required = true)
	private Date startDate;
	
	@XmlElement
	private Date endDate;
	
	@XmlElement
	private Integer dispQty;
	
	@XmlElement
	private String baseUnit;
	
	@XmlElement
	private String connectSystem;
	
	@XmlElement
	private String doseRuleCode;
	
	@XmlElement
	private Integer unitOfCharge;

	@XmlElement
	private Integer cmsConfirmUnitOfCharge;

	@XmlElement(required = true)
	private YesNoBlankFlag cmsConfirmChargeFlag;
	
	@XmlElement
	private Date reviewDate;
	
	@XmlElement(name="DailyFreq")
	private Freq dailyFreq;
	
	@XmlElement(name="PharmDrug")
	private List<PharmDrug> pharmDrugList;

	@XmlTransient
	private Integer groupNum;
	
	@XmlTransient
	private Capd capd;		
	
	public CapdItem() {
		prn = Boolean.FALSE;
		
		unitOfCharge = 0;
		cmsConfirmUnitOfCharge = 0;
		cmsConfirmChargeFlag = YesNoBlankFlag.No;
	}
	
	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public String getDosageUnit() {
		return dosageUnit;
	}

	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}

	public Boolean getPrn() {
		return prn;
	}

	public void setPrn(Boolean prn) {
		this.prn = prn;
	}

	public PrnPercentage getPrnPercent() {
		return prnPercent;
	}

	public void setPrnPercent(PrnPercentage prnPercent) {
		this.prnPercent = prnPercent;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public RegimenDurationUnit getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(RegimenDurationUnit durationUnit) {
		this.durationUnit = durationUnit;
	}
	
	public Date getOrgStartDate() {
		return orgStartDate == null ? null : new Date(orgStartDate.getTime());
	}

	public void setOrgStartDate(Date orgStartDate) {
		this.orgStartDate = orgStartDate == null ? null : new Date(orgStartDate.getTime());
	}

	public Date getStartDate() {
		return startDate == null ? null : new Date(startDate.getTime());
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate == null ? null : new Date(startDate.getTime());
	}

	public Date getEndDate() {
		return endDate == null ? null : new Date(endDate.getTime());
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new Date(endDate.getTime());
	}

	public Integer getDispQty() {
		return dispQty;
	}

	public void setDispQty(Integer dispQty) {
		this.dispQty = dispQty;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setConnectSystem(String connectSystem) {
		this.connectSystem = connectSystem;
	}

	public String getConnectSystem() {
		return connectSystem;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	public String getConcentration() {
		return concentration;
	}

	public String getFullConcentration() {
		return fullConcentration;
	}

	public void setFullConcentration(String fullConcentration) {
		this.fullConcentration = fullConcentration;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public String getDoseRuleCode() {
		return doseRuleCode;
	}

	public void setDoseRuleCode(String doseRuleCode) {
		this.doseRuleCode = doseRuleCode;
	}
	
	public void setUnitOfCharge(Integer unitOfCharge) {
		this.unitOfCharge = unitOfCharge;
	}

	public Integer getUnitOfCharge() {
		return unitOfCharge;
	}	
	
	public Integer getCmsConfirmUnitOfCharge() {
		return cmsConfirmUnitOfCharge;
	}

	public void setCmsConfirmUnitOfCharge(Integer cmsConfirmUnitOfCharge) {
		this.cmsConfirmUnitOfCharge = cmsConfirmUnitOfCharge;
	}

	public YesNoBlankFlag getCmsConfirmChargeFlag() {
		return cmsConfirmChargeFlag;
	}

	public void setCmsConfirmChargeFlag(YesNoBlankFlag cmsConfirmChargeFlag) {
		this.cmsConfirmChargeFlag = cmsConfirmChargeFlag;
	}

	public Date getReviewDate() {
		return reviewDate == null ? null : new Date(reviewDate.getTime());
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate == null ? null : new Date(reviewDate.getTime());
	}

	public Freq getDailyFreq() {
		return dailyFreq;
	}

	public void setDailyFreq(Freq dailyFreq) {
		this.dailyFreq = dailyFreq;
	}

	public List<PharmDrug> getPharmDrugList() {
		if (pharmDrugList == null) {
			pharmDrugList = new ArrayList<PharmDrug>();
		}
		return pharmDrugList;
	}

	public void setPharmDrugList(List<PharmDrug> pharmDrugList) {
		this.pharmDrugList = pharmDrugList;
	}
	
	public void addPharmDrug(PharmDrug pharmDrug) {
		this.getPharmDrugList().add(pharmDrug);
	}
	
	public Capd getCapd() {
		return capd;
	}

	public void setCapd(Capd capd) {
		this.capd = capd;
	}

	public void setGroupNum(Integer groupNum) {
		this.groupNum = groupNum;
	}

	public Integer getGroupNum() {
		return groupNum;
	}
	
	public int getDurationInDay() {
		if (getDuration() != null && getDurationUnit() != null) {			
			if (getDurationUnit() == RegimenDurationUnit.Week) {
				return getDuration().intValue() * 7;
			} else {
				return getDuration().intValue();
			}
		}
		return 0;
	}	
}
