package hk.org.ha.model.pms.vo.reftable;

import hk.org.ha.model.pms.persistence.reftable.Machine;
import hk.org.ha.model.pms.udt.reftable.MachineType;

import java.io.Serializable;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class ItemLocationInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long itemLocationId;
	
	private String itemCode;
	
	private Integer pickStationNum;
	
	private String binNum;
	
	private Machine machine;
	
	private MachineType machineType;
	
	private Integer maxQty;
	
	private String bakerCellNum;
	
	private String prepackQty;

	private List<Machine> machineList;
	
	private boolean markDelete = false;

	public Long getItemLocationId() {
		return itemLocationId;
	}

	public void setItemLocationId(Long itemLocationId) {
		this.itemLocationId = itemLocationId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getPickStationNum() {
		return pickStationNum;
	}

	public void setPickStationNum(Integer pickStationNum) {
		this.pickStationNum = pickStationNum;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public MachineType getMachineType() {
		return machineType;
	}

	public void setMachineType(MachineType machineType) {
		this.machineType = machineType;
	}

	public Integer getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}

	public String getBakerCellNum() {
		return bakerCellNum;
	}

	public void setBakerCellNum(String bakerCellNum) {
		this.bakerCellNum = bakerCellNum;
	}

	public String getPrepackQty() {
		return prepackQty;
	}

	public void setPrepackQty(String prepackQty) {
		this.prepackQty = prepackQty;
	}

	public void setMachineList(List<Machine> machineList) {
		this.machineList = machineList;
	}

	public List<Machine> getMachineList() {
		return machineList;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public Machine getMachine() {
		return machine;
	}
}