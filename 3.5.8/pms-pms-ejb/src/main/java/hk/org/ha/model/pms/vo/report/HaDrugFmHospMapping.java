package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class HaDrugFmHospMapping implements Serializable {

	private static final long serialVersionUID = 1L;

	private String patHospCode;
	
	private String fromClusterCode;
	
	private String toClusterCode;

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getFromClusterCode() {
		return fromClusterCode;
	}

	public void setFromClusterCode(String fromClusterCode) {
		this.fromClusterCode = fromClusterCode;
	}

	public String getToClusterCode() {
		return toClusterCode;
	}

	public void setToClusterCode(String toClusterCode) {
		this.toClusterCode = toClusterCode;
	}
}
