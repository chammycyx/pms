package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.BaseEntity;
import hk.org.ha.model.pms.udt.disp.MedCasePasPatInd;
import hk.org.ha.model.pms.udt.disp.MedCasePasPayFlagType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "MED_CASE")
@XmlAccessorType(XmlAccessType.FIELD)
public class MedCase extends BaseEntity {

	private static final long serialVersionUID = 1L;

	public static final String PAS_PAY_CODE_PIP = "PIP";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medCaseSeq")
	@SequenceGenerator(name = "medCaseSeq", sequenceName = "SQ_MED_CASE", initialValue = 100000000)
	@XmlElement
	private Long id;
	
	@Column(name = "CASE_NUM", length = 12)
	@XmlElement
	private String caseNum;
	
	@Column(name = "PAS_APPT_SEQ")
	@XmlTransient
	private Long pasApptSeq;
	
	@Column(name = "PAS_APPT_CASE_NUM", length = 12)
	@XmlTransient
	private String pasApptCaseNum;
	
	@Column(name = "PAS_PAY_CODE", length = 3)
	@XmlTransient
	private String pasPayCode;
	
	@Column(name = "CHARGE_FLAG", nullable = false)
	@XmlTransient
	private Boolean chargeFlag;
	
	@Converter(name = "MedCase.pasPayFlagCode", converterClass = MedCasePasPayFlagType.Converter.class)
	@Convert("MedCase.pasPayFlagCode")
	@Column(name = "PAS_PAY_FLAG_CODE", length = 1)
	@XmlTransient
	private MedCasePasPayFlagType pasPayFlagCode;

	@Column(name = "PAS_PAT_GROUP_CODE", length = 5)
	@XmlTransient
	private String pasPatGroupCode;
	
	@Column(name = "PAS_WARD_CODE", length = 4)
	@XmlTransient
	private String pasWardCode;
	
	@Column(name = "PAS_BED_NUM", length = 5)
	@XmlTransient
	private String pasBedNum;
	
	@Column(name = "PAS_SPEC_CODE", length = 4)
	@XmlElement
	private String pasSpecCode;
	
	@Column(name = "PAS_SUB_SPEC_CODE", length = 4)
	@XmlElement
	private String pasSubSpecCode;
	
	@Column(name = "PAS_APPT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlTransient
	private Date pasApptDate;
	
	@Column(name = "PAS_ATTEND_FLAG", nullable = false)
	@XmlTransient
	private Boolean pasAttendFlag;
	
    @Converter(name = "MedCase.pasPatInd", converterClass = MedCasePasPatInd.Converter.class)
    @Convert("MedCase.pasPatInd")
	@Column(name="PAS_PAT_IND", length = 1, nullable = false)
	@XmlTransient
	private MedCasePasPatInd pasPatInd;
   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ADMISSION_DATE")
    private Date admissionDate;
   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DISCHARGE_DATE")
    private Date dischargeDate;
    
    @Transient
	@XmlTransient
    private String phsSpecCode;
    
    @Transient
	@XmlTransient
    private String phsWardCode;
	
    @Transient
	@XmlTransient
    private Date admissionDatetime;
    
    @Transient
	@XmlTransient
    private Date dischargeDatetime;
    
	public MedCase() {
		chargeFlag = Boolean.TRUE;
		pasAttendFlag = Boolean.FALSE;
		pasPatInd = MedCasePasPatInd.Normal;
		pasPayFlagCode = MedCasePasPayFlagType.Blank;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public Long getPasApptSeq() {
		return pasApptSeq;
	}

	public void setPasApptSeq(Long pasApptSeq) {
		this.pasApptSeq = pasApptSeq;
	}

	public String getPasPayCode() {
		return pasPayCode;
	}

	public void setPasPayCode(String pasPayCode) {
		this.pasPayCode = pasPayCode;
	}

	public MedCasePasPayFlagType getPasPayFlagCode() {
		return pasPayFlagCode;
	}

	public void setPasPayFlagCode(MedCasePasPayFlagType pasPayFlagCode) {
		this.pasPayFlagCode = pasPayFlagCode;
	}

	public String getPasPatGroupCode() {
		return pasPatGroupCode;
	}

	public void setPasPatGroupCode(String pasPatGroupCode) {
		this.pasPatGroupCode = pasPatGroupCode;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public String getPasBedNum() {
		return pasBedNum;
	}

	public void setPasBedNum(String pasBedNum) {
		this.pasBedNum = pasBedNum;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}

	public Date getPasApptDate() {
		if (pasApptDate == null) {
			return null;
		}
		else {
			return new Date(pasApptDate.getTime());
		}
	}

	public void setPasApptDate(Date pasApptDate) {
		if (pasApptDate == null) {
			this.pasApptDate = null;
		}
		else {
			this.pasApptDate = new Date(pasApptDate.getTime());
		}
	}

	public Boolean getPasAttendFlag() {
		return pasAttendFlag;
	}

	public void setPasAttendFlag(Boolean pasAttendFlag) {
		this.pasAttendFlag = pasAttendFlag;
	}

	public MedCasePasPatInd getPasPatInd() {
		return pasPatInd;
	}

	public void setPasPatInd(MedCasePasPatInd pasPatInd) {
		this.pasPatInd = pasPatInd;
	}

	public String getPhsSpecCode() {
		return phsSpecCode;
	}

	public void setPhsSpecCode(String phsSpecCode) {
		this.phsSpecCode = phsSpecCode;
	}

	public String getPhsWardCode() {
		return phsWardCode;
	}

	public void setPhsWardCode(String phsWardCode) {
		this.phsWardCode = phsWardCode;
	}

	public Date getAdmissionDatetime() {
		if (admissionDatetime == null) {
			return null; 
		}
		else {
			return new Date(admissionDatetime.getTime());
		}
	}

	public void setAdmissionDatetime(Date admissionDatetime) {
		if (admissionDatetime == null) {
			this.admissionDatetime = null;
		}
		else {
			this.admissionDatetime = new Date(admissionDatetime.getTime());
		}
	}

	public Date getDischargeDatetime() {
		if (dischargeDatetime == null) {
			return null;
		}
		else {
			return new Date(dischargeDatetime.getTime());
		}
	}

	public void setDischargeDatetime(Date dischargeDatetime) {
		if (dischargeDatetime == null) {
			this.dischargeDatetime = null;
		}
		else{
			this.dischargeDatetime = new Date(dischargeDatetime.getTime());
		}
	}

	public Boolean getChargeFlag() {
		return chargeFlag;
	}

	public void setChargeFlag(Boolean chargeFlag) {
		this.chargeFlag = chargeFlag;
	}
		
	public String getPasApptCaseNum() {
		return pasApptCaseNum;
	}

	public void setPasApptCaseNum(String pasApptCaseNum) {
		this.pasApptCaseNum = pasApptCaseNum;
	}

	public boolean equalsMedCase(MedCase medCase) {
		return (this.getPasSpecCode().equals(medCase.getPasSpecCode()) &&
				this.getPasSubSpecCode().equals(medCase.getPasSubSpecCode()) &&
				this.getChargeFlag().equals(medCase.getChargeFlag()) &&
				this.getPasPayCode().equals(medCase.getPasPayCode()));
	}
	
	public void updateByMedCase(MedCase medCase) {
		caseNum           = medCase.getCaseNum();
		pasApptSeq        = medCase.getPasApptSeq();
		pasApptCaseNum    = medCase.getPasApptCaseNum();
		pasPayCode        = medCase.getPasPayCode();
		chargeFlag        = medCase.getChargeFlag();
		pasPayFlagCode    = medCase.getPasPayFlagCode();
		pasPatGroupCode   = medCase.getPasPatGroupCode();
		pasWardCode       = medCase.getPasWardCode();
		pasBedNum         = medCase.getPasBedNum();
		pasSpecCode       = medCase.getPasSpecCode();
		pasSubSpecCode    = medCase.getPasSubSpecCode();
		pasApptDate       = medCase.getPasApptDate();
		pasAttendFlag     = medCase.getPasAttendFlag();
		pasPatInd         = medCase.getPasPatInd();
		phsSpecCode       = medCase.getPhsSpecCode();
		phsWardCode       = medCase.getPhsWardCode();
		admissionDatetime = medCase.getAdmissionDatetime();
		dischargeDatetime = medCase.getDischargeDatetime();
		admissionDate = medCase.getAdmissionDate();
		dischargeDate = medCase.getDischargeDate();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedCase other = (MedCase) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setAdmissionDate(Date admissionDate) {
		if (admissionDate == null) {
			this.admissionDate = null;
		}
		else {
			this.admissionDate = new Date(admissionDate.getTime());
		}
	}

	public Date getAdmissionDate() {
		if (admissionDate == null) {
			return null; 
		}
		else {
			return new Date(admissionDate.getTime());
		}
	}

	public void setDischargeDate(Date dischargeDate) {
		if (dischargeDate == null) {
			this.dischargeDate = null;
		}
		else{
			this.dischargeDate = new Date(dischargeDate.getTime());
		}
	}

	public Date getDischargeDate() {
		if (dischargeDate == null) {
			return null;
		}
		else {
			return new Date(dischargeDate.getTime());
		}
	}	
}