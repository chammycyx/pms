package hk.org.ha.model.pms.vo.mr;

import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemInf;
import hk.org.ha.model.pms.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.udt.medprofile.MedProfileMoItemStatus;
import hk.org.ha.model.pms.udt.rx.RxItemType;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class MedProfileMrItem implements MedProfileMoItemInf, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer groupIndex;

	private Boolean mpFlag;
	
	private Date orderDate;
	
	private Integer orgItemNum;
	
	private Integer itemNum;

	private RxItemType rxItemType;
	
	private Date startDate;
	
	private Date endDate;
	
	private Integer unitOfCharge;
	
	private YesNoBlankFlag chargeFlag;
	
	private Boolean statFlag; 
	
	private Boolean ppmiFlag;
	
	private Boolean alertFlag;
	
	private Boolean fmFlag;
	
    private String doctorCode;

	private String doctorRankCode;
    
    private String doctorName;
    
    private MedProfileMoItemStatus status;
    
	private RxItem rxItem;

	private String patHospCode;	// additional order level info
	
	private String pasSpecCode;	// additional order level info
	
	private String prescTypeDesc;	// additional order level info

	private String orderNum;	// additional order level info

	private List<MedProfileMrItemFm> medProfileMrItemFmList;
	
	private List<MedProfileMrItemAlert> medProfileMrItemAlertList;
	
	private List<MedProfileMrItemDetail> medProfileMrItemDetailList;
	
	private MedProfileMrItemReview medProfileMrItemReview;
	
	
	public MedProfileMrItem() {
		chargeFlag = YesNoBlankFlag.Blank;
		statFlag = Boolean.FALSE;
		ppmiFlag = Boolean.FALSE;
		alertFlag = Boolean.FALSE;
		fmFlag = Boolean.FALSE;
		status = MedProfileMoItemStatus.Normal;
	}
	
	public Integer getGroupIndex() {
		return groupIndex;
	}

	public void setGroupIndex(Integer groupIndex) {
		this.groupIndex = groupIndex;
	}

	public Boolean getMpFlag() {
		return mpFlag;
	}

	public void setMpFlag(Boolean mpFlag) {
		this.mpFlag = mpFlag;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public RxItemType getRxItemType() {
		return rxItemType;
	}

	public void setRxItemType(RxItemType rxItemType) {
		this.rxItemType = rxItemType;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPrescTypeDesc() {
		return prescTypeDesc;
	}

	public void setPrescTypeDesc(String prescTypeDesc) {
		this.prescTypeDesc = prescTypeDesc;
	}

	public Date getStartDate() {
		return startDate == null ? null : new Date(startDate.getTime());
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate == null ? null : new Date(startDate.getTime());
	}
	
	public Date getEndDate() {
		return endDate == null ? null : new Date(endDate.getTime());
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate == null ? null : new Date(endDate.getTime());
	}

	public Integer getUnitOfCharge() {
		return unitOfCharge;
	}

	public void setUnitOfCharge(Integer unitOfCharge) {
		this.unitOfCharge = unitOfCharge;
	}

	public YesNoBlankFlag getChargeFlag() {
		return chargeFlag;
	}

	public void setChargeFlag(YesNoBlankFlag chargeFlag) {
		this.chargeFlag = chargeFlag;
	}
	
	public Boolean getStatFlag() {
		return statFlag;
	}

	public void setStatFlag(Boolean statFlag) {
		this.statFlag = statFlag;
	}

	public Boolean getPpmiFlag() {
		return ppmiFlag;
	}

	public void setPpmiFlag(Boolean ppmiFlag) {
		this.ppmiFlag = ppmiFlag;
	}
	
	public Boolean getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(Boolean alertFlag) {
		this.alertFlag = alertFlag;
	}
	
	public Boolean getFmFlag() {
		return fmFlag;
	}

	public void setFmFlag(Boolean fmFlag) {
		this.fmFlag = fmFlag;
	}

	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	public String getDoctorRankCode() {
		return doctorRankCode;
	}

	public void setDoctorRankCode(String doctorRankCode) {
		this.doctorRankCode = doctorRankCode;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	
	public MedProfileMoItemStatus getStatus() {
		return status;
	}

	public void setStatus(MedProfileMoItemStatus status) {
		this.status = status;
	}

	public RxItem getRxItem() {
		return rxItem;
	}

	public void setRxItem(RxItem rxItem) {
		this.rxItem = rxItem;
	}
	
	public List<MedProfileMrItemFm> getMedProfileMrItemFmList() {
		if (medProfileMrItemFmList == null) {
			medProfileMrItemFmList = new ArrayList<MedProfileMrItemFm>();
		}
		return medProfileMrItemFmList;
	}

	public void setMedProfileMrItemFmList(List<MedProfileMrItemFm> medProfileMrItemFmList) {
		this.medProfileMrItemFmList = medProfileMrItemFmList;
	}

	public void addMedProfileMrItemFm(MedProfileMrItemFm medProfileMrItemFm) {
		medProfileMrItemFm.setMedProfileMrItem(this);
		getMedProfileMrItemFmList().add(medProfileMrItemFm);
	}
	
	public void clearMedProfileMrItemFmList() {
		medProfileMrItemFmList = new ArrayList<MedProfileMrItemFm>();
	}
	
	public List<MedProfileMrItemAlert> getMedProfileMrItemAlertList() {
		if (medProfileMrItemAlertList == null) {
			medProfileMrItemAlertList = new ArrayList<MedProfileMrItemAlert>();
		}
		return medProfileMrItemAlertList;
	}
	
	public void setMedProfileMrItemAlertList(List<MedProfileMrItemAlert> medProfileMrItemAlertList) {
		this.medProfileMrItemAlertList = medProfileMrItemAlertList;
	}
	
	public void addMedProfileMrItemAlert(MedProfileMrItemAlert medProfileMrItemAlert) {
		medProfileMrItemAlert.setMedProfileMrItem(this);
		getMedProfileMrItemAlertList().add(medProfileMrItemAlert);
	}
	
	public void clearMedProfileMrItemAlertList() {
		medProfileMrItemAlertList = new ArrayList<MedProfileMrItemAlert>();
	}

	public List<MedProfileMrItemDetail> getMedProfileMrItemDetailList() {
		if (medProfileMrItemDetailList == null) {
			medProfileMrItemDetailList = new ArrayList<MedProfileMrItemDetail>();
		}
		return medProfileMrItemDetailList;
	}

	public void setMedProfileMrItemDetailList(List<MedProfileMrItemDetail> medProfileMrItemDetailList) {
		this.medProfileMrItemDetailList = medProfileMrItemDetailList;
	}

	public void addMedProfileMrItemDetail(MedProfileMrItemDetail medProfileMrItemDetail) {
		medProfileMrItemDetail.setMedProfileMrItem(this);
		getMedProfileMrItemDetailList().add(medProfileMrItemDetail);
	}
	
	public MedProfileMrItemReview getMedProfileMrItemReview() {
		return medProfileMrItemReview;
	}

	public void setMedProfileMrItemReview(MedProfileMrItemReview medProfileMrItemReview) {
		this.medProfileMrItemReview = medProfileMrItemReview;
	}
}
