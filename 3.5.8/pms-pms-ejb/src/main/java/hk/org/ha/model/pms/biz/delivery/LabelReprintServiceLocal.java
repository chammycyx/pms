package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.medprofile.Ward;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface LabelReprintServiceLocal {

	void destroy();

	List<Ward> retrieveWardSelectionList();
	
	void retrieveDeliveryList(String wardCode, String batchCode, String caseNum, Date dispenseDate, String trxId);
	
	void retrieveMpDispLabelListByBatchNum(String batchNum, Date dispenseDate, String caseNum, String trxId);
	
	void reprintMpDispLabel(List<MpDispLabel> mpDispLabels);
	
	void reprintDeliveryLabel();
	
	void reprintDividerLabel();
	
	void resendAtdpsMessage(List<MpDispLabel> mpDispLabelList);
	
}
