package hk.org.ha.model.pms.biz.label;

import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.reftable.CustomLabelManagerLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.reftable.CustomLabel;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.label.CustomLabelType;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.util.Arrays;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("hkidBarcodeLabelService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class HkidBarcodeLabelServiceBean implements HkidBarcodeLabelServiceLocal {
	@Logger
	private Log logger; 
		
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private Workstation workstation;
	
	@In
	private CustomLabelManagerLocal customLabelManager;
	
	@Out(required = false)
	private HkidBarcodeLabel hkidBarcodeLabel;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private PrinterSelectorLocal printerSelector;	
	
	private boolean retrieveSuccess;	
	
	@In
	private Workstore workstore;
	
	public void retrieveHkidBarcodeLabel(String name){
		hkidBarcodeLabel = null;
		CustomLabel customLabel = null;
		
		if(searchTextValidator.checkSearchInputType(name).equals(SearchTextType.Hkid)){
			customLabel = customLabelManager.retrieveCustomLabel(name, CustomLabelType.HkIdBarCode);

			if(customLabel!=null){
				hkidBarcodeLabel = customLabel.getHkidBarcodeLabel();
			}
			retrieveSuccess = true;
		} else {
			retrieveSuccess = false;
		}
	}
	
	public void updateHkidBarcodeLabel(HkidBarcodeLabel hkidBarcodeLabel){
		CustomLabel customLabel = new CustomLabel();
		customLabel.setHkidBarcodeLabel(hkidBarcodeLabel);
		customLabel.setName(hkidBarcodeLabel.getHkid());
		customLabel.setType(CustomLabelType.HkIdBarCode);
		customLabel.setWorkstore(workstore);
		
		logger.debug("updateHkidBarcodeLabel #0", customLabel.getLabelXml());
		customLabelManager.updateCustomLabel(customLabel);
		
	}
	
	public void printHkidBarcodeLabel(HkidBarcodeLabel hkidBarcodeLabel ){
		PrintLang printLang;
		if (hkidBarcodeLabel.getPrintLang() == null) {
			printLang = PrintLang.Chi;
		} else {
			printLang = hkidBarcodeLabel.getPrintLang();
		}
		
		PrinterSelect ps = printerSelector.retrievePrinterSelect(PrintDocType.OtherLabel);
		ps.setCopies(hkidBarcodeLabel.getCopies());
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"HkidBarcodeLabel", 
				new PrintOption(PrintType.Normal, printLang),
				ps, 
				Arrays.asList(hkidBarcodeLabel),
				"[HkidBarcodeLabel]"
				));
		
	}
	
	public void removeHkidBarcodeLabel(String name){
		customLabelManager.removeCustomLabel(name, CustomLabelType.HkIdBarCode);
	}

	@Remove
	public void destroy() {
	}

	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}

}
