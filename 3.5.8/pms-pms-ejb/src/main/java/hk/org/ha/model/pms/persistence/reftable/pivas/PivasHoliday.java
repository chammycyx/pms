package hk.org.ha.model.pms.persistence.reftable.pivas;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.WorkstoreGroup;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PIVAS_HOLIDAY")
@Customizer(AuditCustomizer.class)
public class PivasHoliday extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasHolidaySeq")
	@SequenceGenerator(name = "pivasHolidaySeq", sequenceName = "SQ_PIVAS_HOLIDAY", initialValue = 100000000)
	private Long id;
	
	@Column(name = "HOLIDAY", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date holiday;

	@ManyToOne
	@JoinColumn(name = "WORKSTORE_GROUP_CODE", nullable = false)
	private WorkstoreGroup workstoreGroup;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getHoliday() {
		if (holiday == null) {
			return null;
		}
		else {
			return new Date(holiday.getTime());
		}
	}

	public void setHoliday(Date holiday) {
		if (holiday == null) {
			this.holiday = null;
		}
		else {
			this.holiday = new Date(holiday.getTime());
		}
	}

	public void setWorkstoreGroup(WorkstoreGroup workstoreGroup) {
		this.workstoreGroup = workstoreGroup;
	}

	public WorkstoreGroup getWorkstoreGroup() {
		return workstoreGroup;
	}
	
	// helper methods
    public boolean isNew() {
    	return this.getId() == null;
    }	
}
