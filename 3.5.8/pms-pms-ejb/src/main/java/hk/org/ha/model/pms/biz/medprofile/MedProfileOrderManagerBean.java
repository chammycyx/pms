package hk.org.ha.model.pms.biz.medprofile;

import hk.org.ha.model.pms.corp.vo.medprofile.SignatureCountResult;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("medProfileOrderManager")
public class MedProfileOrderManagerBean implements MedProfileOrderManagerLocal {

	@PersistenceContext
	private EntityManager em;
			
	public void saveMedProfileOrder(MedProfileOrder medProfileOrder) {
		if (medProfileOrder.getWorkstore().getHospCode() == null) {
			medProfileOrder.setWorkstore(null);
		}
		
		em.persist(medProfileOrder);
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<SignatureCountResult> retrieveSignatureCountResult(Date batchDate) {
		DateTime nextDate = (new DateTime(batchDate)).plusDays(1);
		List<MedProfileOrderType> medProfileOrderTypeList = new ArrayList<MedProfileOrderType>();
		medProfileOrderTypeList.add(MedProfileOrderType.Create);
		medProfileOrderTypeList.add(MedProfileOrderType.Add);
		List<SignatureCountResult> signatureCountResultList = new ArrayList<SignatureCountResult>();
		
		List<Object[]> countList = (List<Object[]>)em.createQuery(
				"select o.patHospCode, count(o) from MedProfileOrder o where" + // 20130829 index check : MedProfileOrder.cmsCreateDate : I_MED_PROFILE_ORDER_03
				" o.type in :medProfileOrderTypeList" +
				" and o.cmsCreateDate >= :batchDate" +
				" and o.cmsCreateDate < :nextDate" +
				" group by o.patHospCode")
				.setParameter("medProfileOrderTypeList", medProfileOrderTypeList)
				.setParameter("batchDate", batchDate)
				.setParameter("nextDate", nextDate.toDate())
				.getResultList();
		
		for ( Object[] objs : countList ) {
			SignatureCountResult scr = new SignatureCountResult();
			scr.setHospitalCode((String)objs[0]);
			scr.setCount(Integer.valueOf(((Long) objs[1]).intValue()));
			scr.setSignedDate(batchDate);
			signatureCountResultList.add(scr);
		}
		
		return signatureCountResultList;
	}
}
