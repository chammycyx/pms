package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.vo.rx.RxItem;

import java.util.List;

public interface MedItemInf {

	public RxItem getRxItem();
	
	public Integer getItemNum();
	
	public List<? extends AlertEntity> getAlertList();
	
	public List<? extends AlertEntity> getPatAlertList(Boolean includeHlaFlag);
	
	public List<? extends AlertEntity> getDdiAlertList();
	
	public List<? extends AlertEntity> getDrcAlertList();
	
	public String getMdsOrderDesc();
	
	public String getDisplayNameSaltForm();
	
}
