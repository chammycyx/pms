package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class EdsStatusCount implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String timeRange;
	
	private StringValuedEnum status;
	
	private Long count;

	public EdsStatusCount(String timeRange, StringValuedEnum status, Long count) {
		super();
		this.timeRange = timeRange;
		this.status = status;
		this.count = count;
	}
	
	public void setStatus(StringValuedEnum status) {
		this.status = status;
	}

	public StringValuedEnum getStatus() {
		return status;
	}
	
	public void setCount(Long count) {
		this.count = count;
	}

	public Long getCount() {
		return count;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getTimeRange() {
		return timeRange;
	}


}
