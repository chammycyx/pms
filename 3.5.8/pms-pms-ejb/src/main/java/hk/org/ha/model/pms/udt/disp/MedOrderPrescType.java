package hk.org.ha.model.pms.udt.disp;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MedOrderPrescType implements StringValuedEnum {
	
	Out("O", "Out Patient"),
	In("I", "In Patient"),
	Discharge("D", "Discharge"),
	HomeLeave("H", "Home Leave"),
	MpDischarge("X", "Discharge"),
	MpHomeLeave("L", "Home Leave"),
	Dh("DH", "DOH");
	
	public static final List<MedOrderPrescType> MpDischarge_MpHomeLeave = Arrays.asList(MpDischarge, MpHomeLeave);
	public static final List<MedOrderPrescType> ALL_OUT_PATIENT = Arrays.asList(Out, Discharge, HomeLeave, MpDischarge, MpHomeLeave, Dh);
	public static final List<MedOrderPrescType> REFILL_IP_PAS_SPEC = Arrays.asList(In, Discharge, HomeLeave, MpDischarge, MpHomeLeave);
	
	private final String dataValue;
	private final String displayValue;

	MedOrderPrescType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static MedOrderPrescType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MedOrderPrescType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<MedOrderPrescType> {

		private static final long serialVersionUID = 1L;

		public Class<MedOrderPrescType> getEnumClass() {
    		return MedOrderPrescType.class;
    	}
    }
}
