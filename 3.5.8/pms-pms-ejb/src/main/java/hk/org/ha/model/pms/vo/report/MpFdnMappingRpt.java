package hk.org.ha.model.pms.vo.report;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class MpFdnMappingRpt implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String workstoreCode;
	
	private String itemCode;
	
	private String fullDrugDesc;
	
	private String floatDrugName;
	
	public MpFdnMappingRpt(){
		
	}
	
	public MpFdnMappingRpt(String hospCode, String workstoreCode, String itemCode, String fullDrugDesc, String floatDrugName){
		super();
		this.hospCode = hospCode;
		this.workstoreCode = workstoreCode;
		this.itemCode = itemCode;
		this.fullDrugDesc = fullDrugDesc;
		this.floatDrugName = floatDrugName;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getWorkstoreCode() {
		return workstoreCode;
	}

	public void setWorkstoreCode(String workstoreCode) {
		this.workstoreCode = workstoreCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getFloatDrugName() {
		return floatDrugName;
	}

	public void setFloatDrugName(String floatDrugName) {
		this.floatDrugName = floatDrugName;
	}
	
}
