package hk.org.ha.model.pms.biz.enquiry;

import hk.org.ha.model.pms.persistence.medprofile.MedProfileActionLog;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface OrderPendingSuspendLogEnqManagerLocal {

	void destroy();

	List<MedProfileActionLog> retrieveOrderPendingSuspendLogEnqListByCriteria(Date fromDate, Date toDate, String caseNum, String filter);
	
}
