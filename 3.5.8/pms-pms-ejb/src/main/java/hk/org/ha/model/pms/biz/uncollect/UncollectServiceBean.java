package hk.org.ha.model.pms.biz.uncollect;

import static hk.org.ha.model.pms.prop.Prop.UNCOLLECT_DAYENDORDER_ALERT_DURATION;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.Invoice;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceDocType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.uncollect.UncollectOrder;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

@Stateful
@Scope(ScopeType.SESSION)
@Name("uncollectService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UncollectServiceBean implements UncollectServiceLocal {
	
	private static final PeriodFormatter pf = new PeriodFormatterBuilder()
	 .appendYears()
	 .appendSuffix("y")
	 .appendSeparator(" ")
	 .appendMonths()
	 .appendSuffix("m")
	 .appendSeparator(" ")
	 .appendDays()
	 .appendSuffix("d")
	 .toFormatter();
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Workstore workstore;

	@Out(required = false)
	private UncollectOrder uncollectOrder;
	
	@SuppressWarnings("unchecked")
	public void retrieveUncollectOrder(Date ticketDate, String ticketNum) {
		uncollectOrder = null;
		List<DispOrder> dispOrderList = em.createQuery(
				"select o from DispOrder o" + // 20120306 index check : Ticket.workstore,orderType,ticketDate,ticketNum : UI_TICKET_01
				" where o.ticket.workstore = :workstore" + 
				" and o.ticket.orderType = :ticketOrderType" +
				" and o.ticket.ticketDate = :ticketDate" +
				" and o.ticket.ticketNum = :ticketNum" +
				" and o.dayEndStatus = :dispOrderDayEndStatus" +
				" and o.adminStatus = :dispOrderAdminStatus" +
				" and o.status not in :status") 
				.setParameter("workstore", workstore)
				.setParameter("ticketOrderType", OrderType.OutPatient)
				.setParameter("ticketDate", ticketDate)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderDayEndStatus", DispOrderDayEndStatus.Completed)
				.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
				.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
				.setHint(QueryHints.BATCH, "o.ticket")
				.getResultList();

		if ( !dispOrderList.isEmpty() ) {
				uncollectOrder = new UncollectOrder();
				uncollectOrder.setBold(true);
				uncollectOrder.setTicketDate(dispOrderList.get(0).getTicket().getTicketDate());
				uncollectOrder.setTicketNum(dispOrderList.get(0).getTicket().getTicketNum());
				if ( dispOrderList.get(0).getPharmOrder().getMedCase()!= null ){
					uncollectOrder.setCaseNum(dispOrderList.get(0).getPharmOrder().getMedCase().getCaseNum());
				}

				uncollectOrder.setDispOrderId(dispOrderList.get(0).getId());
				uncollectOrder.setPharmOrderVersion(dispOrderList.get(0).getPharmOrder().getVersion());
				
				boolean sfiFlag = false;
				
				for (Invoice invoice : dispOrderList.get(0).getInvoiceList()){
					if (InvoiceDocType.Standard.equals(invoice.getDocType())) {
						uncollectOrder.setReceiptNum(invoice.getReceiptNum());
					} else if (InvoiceDocType.Sfi.equals(invoice.getDocType())) {
						sfiFlag = true;
					}
				}
				
				if ( sfiFlag ) {
					uncollectOrder.setSfiFlag(true);
				}

				if ( checkTicketDate(dispOrderList.get(0).getTicket().getTicketDate()) < 0) {
					uncollectOrder.setWithinUncollectDuration(true);
				}
		}
	}

	private int checkTicketDate(Date ticketDate){
		
		String dayEndRange = UNCOLLECT_DAYENDORDER_ALERT_DURATION.get().toString();
		
		Period period = pf.parsePeriod(dayEndRange+"d");		 
		DateTime endDate = new DateTime();
		DateTime dispDateTime = new DateTime(ticketDate);
		
		DateTime startDate = endDate.minus(period);
		 
		return startDate.toDate().compareTo(dispDateTime.toDate());

	}

	@Remove
	public void destroy() {
		if ( uncollectOrder != null ) { 
			uncollectOrder = null;
		}
	}

}
