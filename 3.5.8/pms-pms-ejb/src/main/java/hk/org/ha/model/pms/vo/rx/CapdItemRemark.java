package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class CapdItemRemark extends CapdItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean fullConcentrationRemark;

	private Boolean dosageRemark;
	
	private Boolean dosageUnitRemark;
	
	private Boolean prnRemark;
	
	private Boolean prnPercentRemark;	
	
	private Boolean durationRemark;
	
	private Boolean durationUnitRemark;
	
	private Boolean startDateRemark;
	
	private Boolean endDateRemark;
	
	private Boolean dispQtyRemark;
	
	private Boolean baseUnitRemark;
	
	private Boolean dailyFreqRemark;
	
	public CapdItemRemark() {
		fullConcentrationRemark = Boolean.FALSE;
		dosageRemark = Boolean.FALSE;
		dosageUnitRemark = Boolean.FALSE;
		prnRemark = Boolean.FALSE;
		prnPercentRemark = Boolean.FALSE;	
		durationRemark = Boolean.FALSE;
		durationUnitRemark = Boolean.FALSE;
		startDateRemark = Boolean.FALSE;
		endDateRemark = Boolean.FALSE;
		dispQtyRemark = Boolean.FALSE;
		baseUnitRemark = Boolean.FALSE;
		dailyFreqRemark = Boolean.FALSE;
	}

	public Boolean getFullConcentrationRemark() {
		return fullConcentrationRemark;
	}

	public void setFullConcentrationRemark(Boolean fullConcentrationRemark) {
		this.fullConcentrationRemark = fullConcentrationRemark;
	}

	public Boolean getDosageRemark() {
		return dosageRemark;
	}

	public void setDosageRemark(Boolean dosageRemark) {
		this.dosageRemark = dosageRemark;
	}

	public Boolean getDosageUnitRemark() {
		return dosageUnitRemark;
	}

	public void setDosageUnitRemark(Boolean dosageUnitRemark) {
		this.dosageUnitRemark = dosageUnitRemark;
	}

	public Boolean getPrnRemark() {
		return prnRemark;
	}

	public void setPrnRemark(Boolean prnRemark) {
		this.prnRemark = prnRemark;
	}

	public Boolean getPrnPercentRemark() {
		return prnPercentRemark;
	}

	public void setPrnPercentRemark(Boolean prnPercentRemark) {
		this.prnPercentRemark = prnPercentRemark;
	}

	public Boolean getDurationRemark() {
		return durationRemark;
	}

	public void setDurationRemark(Boolean durationRemark) {
		this.durationRemark = durationRemark;
	}

	public Boolean getDurationUnitRemark() {
		return durationUnitRemark;
	}

	public void setDurationUnitRemark(Boolean durationUnitRemark) {
		this.durationUnitRemark = durationUnitRemark;
	}

	public Boolean getStartDateRemark() {
		return startDateRemark;
	}

	public void setStartDateRemark(Boolean startDateRemark) {
		this.startDateRemark = startDateRemark;
	}

	public Boolean getEndDateRemark() {
		return endDateRemark;
	}

	public void setEndDateRemark(Boolean endDateRemark) {
		this.endDateRemark = endDateRemark;
	}

	public Boolean getDispQtyRemark() {
		return dispQtyRemark;
	}

	public void setDispQtyRemark(Boolean dispQtyRemark) {
		this.dispQtyRemark = dispQtyRemark;
	}

	public Boolean getBaseUnitRemark() {
		return baseUnitRemark;
	}

	public void setBaseUnitRemark(Boolean baseUnitRemark) {
		this.baseUnitRemark = baseUnitRemark;
	}

	public void setDailyFreqRemark(Boolean dailyFreqRemark) {
		this.dailyFreqRemark = dailyFreqRemark;
	}

	public Boolean getDailyFreqRemark() {
		return dailyFreqRemark;
	}
}
