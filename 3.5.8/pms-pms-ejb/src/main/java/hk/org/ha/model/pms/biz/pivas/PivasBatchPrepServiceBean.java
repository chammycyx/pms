package hk.org.ha.model.pms.biz.pivas;

import static hk.org.ha.model.pms.prop.Prop.LABEL_PIVASBATCHMERGEFORMULAPRODUCTLABEL_DRUGLIST_VISIBLE;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.Pair;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.order.NumberGeneratorLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.corp.cache.DmWarningCacherInf;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmDrugProperty;
import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
import hk.org.ha.model.pms.dms.udt.pivas.ActionType;
import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
import hk.org.ha.model.pms.dms.vo.pivas.PivasBatchPrepDetail;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepCriteria;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepCriteriaDetail;
import hk.org.ha.model.pms.dms.vo.pivas.PivasPrepValidateResponse;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;
import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrepItem;
import hk.org.ha.model.pms.prop.Prop;
import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepReprintType;
import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepStatus;
import hk.org.ha.model.pms.udt.printing.PrintStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaProductLabel;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaProductLabelDrugDtl;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheet;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetDrugInstructionDtl;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetRawMaterialDtl;
import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetSolventInstructionDtl;
import hk.org.ha.model.pms.vo.label.PivasProductLabel;
import hk.org.ha.model.pms.vo.label.PivasWorksheet;
import hk.org.ha.model.pms.vo.pivas.AddPivasBatchPrepDetail;
import hk.org.ha.model.pms.vo.pivas.BatchPrepMethodInfo;
import hk.org.ha.model.pms.vo.pivas.PivasDiluentInfo;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateMidnight;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasBatchPrepService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasBatchPrepServiceBean implements PivasBatchPrepServiceLocal {
	
	private static final String PIVAS_BATCH_PREP_QUERY_HINT_BATCH_1 = "o.pivasBatchPrepItemList";
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private AuditLogger auditLogger; 
	
	@In
	private Workstore workstore;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;
	
	@In
	private DmDrugCacherInf dmDrugCacher;

	@In
	private DmWarningCacherInf dmWarningCacher;	
	
	@In
	private NumberGeneratorLocal numberGenerator;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PivasWorklistManagerLocal pivasWorklistManager;
	
	private static PivasFormulaComparator pivasFormulaComparator = new PivasFormulaComparator();
	private static PivasFormulaDrugComparator pivasFormulaDrugComparator = new PivasFormulaDrugComparator();
	private static BatchPrepMethodInfoComparator batchPrepMethodInfoComparator = new BatchPrepMethodInfoComparator();
	private static PivasFormulaMethodComparator pivasFormulaMethodComparator = new PivasFormulaMethodComparator();
	private static String VEND_SUPPLY_SOLV = "VEND_SUPPLY_SOLV";
	private static final String SPACE = " ";
	
	private boolean deleteSuccess;
	
	private PivasProductLabel pivasProductLabel;
	
	private PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel;
	
	public List<PivasFormula> retrievePivasFormulaListByPrefixDisplayName(String prefixDisplayName, boolean activeBatchPrepFormulaOnly) {
		List<PivasFormula> pivasFormulaList = null;
		if (activeBatchPrepFormulaOnly) {
			pivasFormulaList = dmsPmsServiceProxy.retrievePivasFormulaListWithActiveStatus(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), prefixDisplayName);
		} else {
			pivasFormulaList = dmsPmsServiceProxy.retrievePivasFormulaListByPrefixDisplayName(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), prefixDisplayName);
		}
		Collections.sort(pivasFormulaList, pivasFormulaComparator);
		return pivasFormulaList;
	}

	public List<PivasFormulaDrug> retrievePivasFormulaDrugListByPrefixDisplayName(String prefixDisplayName) {
		List<PivasFormulaDrug> pivasFormulaDrugList = dmsPmsServiceProxy.retrievePivasFormulaDrugListByPrefixDisplayName(workstore.getWorkstoreGroup().getWorkstoreGroupCode(), prefixDisplayName);		
		
		Map<Integer, PivasFormulaDrug> pivasFormulaDrugMap = new HashMap<Integer, PivasFormulaDrug>();
		for ( PivasFormulaDrug pivasFormulaDrug : pivasFormulaDrugList) {
			if ( !pivasFormulaDrugMap.containsKey(pivasFormulaDrug.getDrugKey())) {
				pivasFormulaDrugMap.put(pivasFormulaDrug.getDrugKey(), pivasFormulaDrug);
			}
		}
		List<PivasFormulaDrug> pivasFormulaSearchList = new ArrayList<PivasFormulaDrug>(pivasFormulaDrugMap.values());
		Collections.sort(pivasFormulaSearchList, pivasFormulaDrugComparator);
		return pivasFormulaSearchList;
	}
	
	public List<BatchPrepMethodInfo> retrieveBatchPrepMethodInfoList(Long pivasFormulaId){	
		List<PivasFormulaMethod> pivasFormulaMethodList = dmsPmsServiceProxy.retrievePivasFormulaMethodListByPivasFormula(pivasFormulaId);
		
		List<BatchPrepMethodInfo> batchPrepMethodInfoList = new ArrayList<BatchPrepMethodInfo>();
		for ( PivasFormulaMethod pivasFormulaMethod : pivasFormulaMethodList ) {			
			BatchPrepMethodInfo batchPrepMethodInfo = new BatchPrepMethodInfo();
			batchPrepMethodInfo.setCoreFlag(pivasFormulaMethod.getCoreFlag());
			
			PivasDrugManufMethod firstPivasDrugManufMethod = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod();
			PivasDrugManuf firstPivasDrugManuf = firstPivasDrugManufMethod.getPivasDrugManuf();
			PivasDrug firstPivasDrug = firstPivasDrugManuf.getPivasDrug();
			
			batchPrepMethodInfo.setItemCode(firstPivasDrug.getItemCode());
			batchPrepMethodInfo.setPivasDosageUnit(firstPivasDrug.getPivasDosageUnit());
			batchPrepMethodInfo.setCompanyCode(firstPivasDrugManuf.getCompany().getCompanyCode());
			batchPrepMethodInfo.setCompanyName(firstPivasDrugManuf.getCompany().getCompanyName());
			batchPrepMethodInfo.setRequireSolventFlag(firstPivasDrugManuf.getRequireSolventFlag());
			batchPrepMethodInfo.setDosageQty(firstPivasDrugManufMethod.getDosageQty());
			batchPrepMethodInfo.setDiluentCode(firstPivasDrugManufMethod.getDiluentCode());
			batchPrepMethodInfo.setAfterVolume(pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getAfterVolume());
			batchPrepMethodInfo.setAfterConcn(pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getAfterConcn());
			batchPrepMethodInfo.setVolume(pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getVolume());
			batchPrepMethodInfo.setDiluentVolume(pivasFormulaMethod.getDiluentVolume());
			batchPrepMethodInfo.setPivasFormulaMethodId(pivasFormulaMethod.getId());
			batchPrepMethodInfo.setPivasDrugManufMethodDesc(firstPivasDrugManufMethod.getMethodDesc());
			
			for (PivasFormulaMethodDrug pivasFormulaMethodDrug: pivasFormulaMethod.getPivasFormulaMethodDrugList()) {

				PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
				PivasDrugManuf pivasDrugManuf = pivasDrugManufMethod.getPivasDrugManuf();
				PivasDrug pivasDrug = pivasDrugManuf.getPivasDrug();
				
				WorkstoreDrug workstoreDrug = workstoreDrugCacher.getWorkstoreDrug(workstore, pivasDrug.getItemCode());
				if ( workstoreDrug != null && workstoreDrug.getMsWorkstoreDrug() != null) {
					MsWorkstoreDrug msWorkstoreDrug = workstoreDrug.getMsWorkstoreDrug();
					batchPrepMethodInfo.setHqSuspend(batchPrepMethodInfo.isHqSuspend() || ("Y".equals(msWorkstoreDrug.getHqSuspend()) ? true : false));
					batchPrepMethodInfo.setSuspend(batchPrepMethodInfo.isSuspend() || ("Y".equals(msWorkstoreDrug.getSuspend()) ? true : false));
				}
			}
			
			batchPrepMethodInfoList.add(batchPrepMethodInfo);
		}
		
		Collections.sort(batchPrepMethodInfoList, batchPrepMethodInfoComparator);
		return batchPrepMethodInfoList;
	}
	
	public List<PivasFormulaMethod> retrievePivasFormulaMethodListByFormulaId(Long pivasFormulaId) {
		
		List<PivasFormulaMethod> pivasFormulaMethodList = dmsPmsServiceProxy.retrievePivasFormulaMethodListByPivasFormula(pivasFormulaId);

		for (PivasFormulaMethod pivasFormulaMethod: pivasFormulaMethodList) {
			for (PivasFormulaMethodDrug pivasFormulaMethodDrug: pivasFormulaMethod.getPivasFormulaMethodDrugList()) {
				PivasDrug pivasDrug = pivasFormulaMethodDrug.getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug();
				pivasDrug.setDmDrug(dmDrugCacher.getDmDrug(pivasDrug.getItemCode()));
			}
		}
		
		Collections.sort(pivasFormulaMethodList, pivasFormulaMethodComparator);
		return pivasFormulaMethodList;
	}
	
	public AddPivasBatchPrepDetail createPivasBatchPrep(Long pivasFormulaMethodId) {
		PivasPrepCriteria pivasPrepCriteria = new PivasPrepCriteria();
		pivasPrepCriteria.setPivasFormulaMethodId(pivasFormulaMethodId);
		pivasPrepCriteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		pivasPrepCriteria.setHospCode(workstore.getHospCode());
		PivasBatchPrepDetail pivasBatchPrepDetail = dmsPmsServiceProxy.retrievePivasBatchPrepDetail(pivasPrepCriteria, true);
		PivasFormulaMethod pivasFormulaMethod = pivasBatchPrepDetail.getPivasFormulaMethod();
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();
		
		PivasBatchPrep pivasBatchPrep = new PivasBatchPrep();
		pivasBatchPrep.setPivasFormulaType(pivasFormula.getType());
		pivasBatchPrep.setPivasFormulaMethodId(pivasFormulaMethod.getId());
		pivasBatchPrep.setPivasFormulaMethod(pivasFormulaMethod);
		pivasBatchPrep.setManufactureDate(new Date());
		pivasBatchPrep.setItemCode(pivasFormula.getItemCode());
		pivasBatchPrep.setDrugKey(pivasFormula.getDrugKey());
		pivasBatchPrep.setExpiryDateTitle(pivasFormula.getExpiryDateTitle());
		pivasBatchPrep.setPivasDosageUnit(pivasFormula.getFirstPivasFormulaDrug().getPivasDosageUnit());
		pivasBatchPrep.setPrintName(pivasFormula.getPrintName());
		pivasBatchPrep.setShelfLife(pivasFormulaMethod.getShelfLife() == null ? pivasFormula.getShelfLife() : pivasFormulaMethod.getShelfLife());
		pivasBatchPrep.setVolume(pivasFormula.getType() == PivasFormulaType.Merge ? pivasFormula.getFinalVolume() : null);
		pivasBatchPrep.setPivasContainerId(pivasFormula.getPivasContainer().getId());	
		pivasBatchPrep.setWarnCode1(pivasFormulaMethod.getWarnCode1());
		pivasBatchPrep.setWarnCode2(pivasFormulaMethod.getWarnCode2());
		pivasBatchPrep.setWarnCode3(pivasFormulaMethod.getWarnCode3());
		pivasBatchPrep.setWarnCode4(pivasFormulaMethod.getWarnCode4());
		pivasBatchPrep.setSiteCode(pivasFormula.getSiteCode());
		pivasBatchPrep.setDiluentCode(pivasFormula.getDiluentCode());
		if (pivasFormula.getDmDiluent() != null) {
			pivasBatchPrep.setDiluentDesc(pivasFormula.getDmDiluent().getPivasDiluentDesc());
		}
		
		for (PivasFormulaMethodDrug pivasFormulaMethodDrug: pivasFormulaMethod.getPivasFormulaMethodDrugList()) {
			PivasFormulaDrug pivasFormulaDrug = pivasFormulaMethodDrug.getPivasFormulaDrug();
			PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
			PivasDrugManuf pivasDrugManuf = pivasDrugManufMethod.getPivasDrugManuf();
			PivasDrug pivasDrug = pivasDrugManuf.getPivasDrug();
			pivasDrug.setDmDrug(dmDrugCacher.getDmDrug(pivasDrug.getItemCode()));
			
			PivasBatchPrepItem pivasBatchPrepItem = new PivasBatchPrepItem();
			pivasBatchPrepItem.setDrugKey(pivasFormulaDrug.getDrugKey());
			pivasBatchPrepItem.setDosageQty(pivasFormulaDrug.getDosage());
			pivasBatchPrepItem.setPivasDosageUnit(pivasFormulaDrug.getPivasDosageUnit());
			
			pivasBatchPrepItem.setPivasFormulaMethodDrugId(pivasFormulaMethodDrug.getId());
			pivasBatchPrepItem.setPivasFormulaMethodDrug(pivasFormulaMethodDrug);
			pivasBatchPrepItem.setAfterConcn(pivasFormulaMethodDrug.getAfterConcn());
			pivasBatchPrepItem.setAfterVolume(pivasFormulaMethodDrug.getAfterVolume());
			
			pivasBatchPrepItem.setDrugItemCode(pivasDrug.getItemCode());
			pivasBatchPrepItem.setDrugManufCode(pivasDrugManuf.getManufacturerCode());
			pivasBatchPrepItem.setDrugBatchNum(pivasDrugManuf.getBatchNum());
			pivasBatchPrepItem.setDrugExpiryDate(pivasDrugManuf.getExpireDate());
			pivasBatchPrepItem.setSolventCode(pivasDrugManufMethod.getDiluentCode());
			if (pivasDrugManufMethod.getDmDiluent() != null) {
				pivasBatchPrepItem.setSolventDesc(pivasDrugManufMethod.getDmDiluent().getPivasDiluentDesc());
			}
			pivasBatchPrepItem.setPivasBatchPrep(pivasBatchPrep);
			pivasBatchPrep.getPivasBatchPrepItemList().add(pivasBatchPrepItem);
		}
	
		AddPivasBatchPrepDetail addPivasBatchPrepDetail = new AddPivasBatchPrepDetail();
		addPivasBatchPrepDetail.setDmWarningList(retrieveDmWarningList());
		addPivasBatchPrepDetail.setPivasBatchPrep(pivasBatchPrep);
		addPivasBatchPrepDetail.setPivasContainerList(pivasBatchPrepDetail.getPivasContainerList());
		addPivasBatchPrepDetail.setPivasDiluentList(pivasWorklistManager.convertPivasDiluentList(pivasBatchPrepDetail.getDiluentList(), workstore));
		addPivasBatchPrepDetail.setPivasSolventList(pivasWorklistManager.convertPivasSolventList(pivasBatchPrepDetail.getSolventList(), workstore));
		for (Entry<Long, List<PivasDrug>> entry: pivasBatchPrepDetail.getSolventListMapByPivasFormulaMethodDrugId().entrySet()) {
			addPivasBatchPrepDetail.getPivasSolventListMapByPivasFormulaMethodDrugId().put(entry.getKey(), pivasWorklistManager.convertPivasSolventList(entry.getValue(), workstore));
		}
		return addPivasBatchPrepDetail;
	}
	
	public AddPivasBatchPrepDetail quickAddPivasBatchPrep(String lotNo){
		PivasBatchPrep pivasBatchPrep = em.find(PivasBatchPrep.class, lotNo);
		pivasBatchPrep.getPivasBatchPrepItemList();
		if ( pivasBatchPrep == null ) {
			return null;
		}
		em.clear();
		PivasPrepCriteria pivasPrepCriteria = new PivasPrepCriteria();
		pivasPrepCriteria.setPivasFormulaMethodId(pivasBatchPrep.getPivasFormulaMethodId());
		pivasPrepCriteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		pivasPrepCriteria.setHospCode(workstore.getHospCode());
		PivasBatchPrepDetail pivasBatchPrepDetail = dmsPmsServiceProxy.retrievePivasBatchPrepDetail(pivasPrepCriteria, true);
		PivasFormulaMethod pivasFormulaMethod = pivasBatchPrepDetail.getPivasFormulaMethod();
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();
				
		PivasBatchPrep newPivasBatchPrep = new PivasBatchPrep();

		try {
			PropertyUtils.copyProperties(newPivasBatchPrep, pivasBatchPrep);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		newPivasBatchPrep.setPivasFormulaType(pivasFormula.getType());
		newPivasBatchPrep.setLotNum(null);
		newPivasBatchPrep.setCreateDate(null);
		newPivasBatchPrep.setCreateUser(null);
		newPivasBatchPrep.setPivasFormulaMethodId(pivasFormulaMethod.getId());
		newPivasBatchPrep.setPivasFormulaMethod(pivasFormulaMethod);
		newPivasBatchPrep.setDrugKey(pivasFormula.getDrugKey());
		newPivasBatchPrep.setExpiryDateTitle(pivasBatchPrep.getExpiryDateTitle());
		newPivasBatchPrep.setManufactureDate(new Date());
		newPivasBatchPrep.setPivasDosageUnit(pivasFormula.getFirstPivasFormulaDrug().getPivasDosageUnit());
		pivasBatchPrep.setShelfLife(pivasFormulaMethod.getShelfLife() == null ? pivasFormula.getShelfLife() : pivasFormulaMethod.getShelfLife());
		newPivasBatchPrep.setPivasContainerId(pivasFormula.getPivasContainer().getId());	
		newPivasBatchPrep.setPrepExpiryDate(null);
		newPivasBatchPrep.setSiteCode(pivasFormula.getSiteCode());
		newPivasBatchPrep.setDiluentCode(pivasFormula.getDiluentCode());
		if (pivasFormula.getDmDiluent() != null) {
			newPivasBatchPrep.setDiluentDesc(pivasFormula.getDmDiluent().getPivasDiluentDesc());
		}
		newPivasBatchPrep.setPivasBatchPrepItemList(null);
		
		for (PivasFormulaMethodDrug pivasFormulaMethodDrug: pivasFormulaMethod.getPivasFormulaMethodDrugList()) {
			PivasFormulaDrug pivasFormulaDrug = pivasFormulaMethodDrug.getPivasFormulaDrug();
			PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
			PivasDrugManuf pivasDrugManuf = pivasDrugManufMethod.getPivasDrugManuf();
			PivasDrug pivasDrug = pivasDrugManuf.getPivasDrug();
			pivasDrug.setDmDrug(dmDrugCacher.getDmDrug(pivasDrug.getItemCode()));
		
			PivasBatchPrepItem pivasBatchPrepItem = new PivasBatchPrepItem();
			pivasBatchPrepItem.setDrugKey(pivasFormulaDrug.getDrugKey());
			pivasBatchPrepItem.setDosageQty(pivasFormula.getType() == PivasFormulaType.Normal ? pivasBatchPrep.getPivasBatchPrepItemList().get(0).getDosageQty() : pivasFormulaDrug.getDosage());
			pivasBatchPrepItem.setPivasDosageUnit(pivasFormulaDrug.getPivasDosageUnit());
			
			pivasBatchPrepItem.setPivasFormulaMethodDrugId(pivasFormulaMethodDrug.getId());
			pivasBatchPrepItem.setPivasFormulaMethodDrug(pivasFormulaMethodDrug);
			pivasBatchPrepItem.setAfterConcn(pivasFormulaMethodDrug.getAfterConcn());
			pivasBatchPrepItem.setAfterVolume(pivasFormulaMethodDrug.getAfterVolume());
			
			pivasBatchPrepItem.setDrugItemCode(pivasDrug.getItemCode());
			pivasBatchPrepItem.setDrugManufCode(pivasDrugManuf.getManufacturerCode());
			pivasBatchPrepItem.setDrugBatchNum(pivasDrugManuf.getBatchNum());
			pivasBatchPrepItem.setDrugExpiryDate(pivasDrugManuf.getExpireDate());
			pivasBatchPrepItem.setSolventCode(pivasDrugManufMethod.getDiluentCode());
			if (pivasDrugManufMethod.getDmDiluent() != null) {
				pivasBatchPrepItem.setSolventDesc(pivasDrugManufMethod.getDmDiluent().getPivasDiluentDesc());
			}
			pivasBatchPrepItem.setPivasBatchPrep(newPivasBatchPrep);
			newPivasBatchPrep.getPivasBatchPrepItemList().add(pivasBatchPrepItem);
		}
		
		AddPivasBatchPrepDetail addPivasBatchPrepDetail = new AddPivasBatchPrepDetail();
		addPivasBatchPrepDetail.setDmWarningList(retrieveDmWarningList());
		addPivasBatchPrepDetail.setPivasBatchPrep(newPivasBatchPrep);
		addPivasBatchPrepDetail.setPivasContainerList(pivasBatchPrepDetail.getPivasContainerList());
		addPivasBatchPrepDetail.setPivasDiluentList(pivasWorklistManager.convertPivasDiluentList(pivasBatchPrepDetail.getDiluentList(), workstore));
		addPivasBatchPrepDetail.setPivasSolventList(pivasWorklistManager.convertPivasSolventList(pivasBatchPrepDetail.getSolventList(), workstore));
		for (Entry<Long, List<PivasDrug>> entry: pivasBatchPrepDetail.getSolventListMapByPivasFormulaMethodDrugId().entrySet()) {
			addPivasBatchPrepDetail.getPivasSolventListMapByPivasFormulaMethodDrugId().put(entry.getKey(), pivasWorklistManager.convertPivasSolventList(entry.getValue(), workstore));
		}
		return addPivasBatchPrepDetail;
	}
	
	private List<DmWarning> retrieveDmWarningList(){
		List<DmWarning> dmWarningList = new ArrayList<DmWarning>();
		List<DmWarning> allDmWarningList = dmWarningCacher.getDmWarningList();
		for (DmWarning dmWarning : allDmWarningList) {
			if ( "ST".equals(dmWarning.getWarnCatCode()) ) {
				dmWarningList.add(dmWarning);
			}
		}
		return dmWarningList;
	}
	
	public PivasPrepValidateResponse updatePivasBatchPrep(PivasBatchPrep pivasBatchPrep) {

		PivasPrepCriteria pivasprepValidateCriteria = new PivasPrepCriteria();
		pivasprepValidateCriteria.setPivasFormulaMethodId(pivasBatchPrep.getPivasFormulaMethodId());
		pivasprepValidateCriteria.setHospCode(workstore.getHospCode());
		pivasprepValidateCriteria.setWorkstoreCode(workstore.getWorkstoreCode());
		pivasprepValidateCriteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		pivasprepValidateCriteria.setDiluentItemCode(pivasBatchPrep.getDiluentItemCode());
		pivasprepValidateCriteria.setDiluentManufCode(pivasBatchPrep.getDiluentManufCode());
		pivasprepValidateCriteria.setWarnCode1(pivasBatchPrep.getWarnCode1());
		pivasprepValidateCriteria.setWarnCode2(pivasBatchPrep.getWarnCode2());
		pivasprepValidateCriteria.setPivasPrepCriteriaDetailList(new ArrayList<PivasPrepCriteriaDetail>());
		
		for ( PivasBatchPrepItem pivasBatchPrepItem : pivasBatchPrep.getPivasBatchPrepItemList() ) {
			PivasPrepCriteriaDetail criteriaDetail = new PivasPrepCriteriaDetail();
			criteriaDetail.setPivasFormulaMethodDrugId(pivasBatchPrepItem.getPivasFormulaMethodDrugId());
			criteriaDetail.setSolventItemCode(pivasBatchPrepItem.getSolventItemCode());
			criteriaDetail.setSolventManufCode(pivasBatchPrepItem.getSolventManufCode());
			pivasprepValidateCriteria.getPivasPrepCriteriaDetailList().add(criteriaDetail);
		}
		
		PivasPrepValidateResponse pivasPrepValidateResponse = dmsPmsServiceProxy.validateBatchPrepMethod(pivasprepValidateCriteria);
		
		if ( pivasPrepValidateResponse.getValidationResult() == Boolean.TRUE ) {
			String lotNum = numberGenerator.retrievePivasBatchPrepNum();
			if ( StringUtils.isBlank(lotNum) ) {
				//Failed to save batch preparation.  Maximum lot number is 99.
				pivasPrepValidateResponse.setErrorCode("0880");
				pivasPrepValidateResponse.setValidationResult(Boolean.FALSE);
			} else {
				pivasBatchPrep.setLotNum(lotNum);
				pivasBatchPrep.setHospital(workstore.getHospital());
				pivasBatchPrep.setStatus(PivasBatchPrepStatus.Printed);
				
				if (pivasBatchPrep.getPivasFormulaType() != PivasFormulaType.Merge) {
					pivasBatchPrep.setPivasWorksheet(conventPivasWorksheet(pivasBatchPrep));
					pivasBatchPrep.setPivasProductLabel(conventPivasProductLabel(pivasBatchPrep, false));
				} else {
					pivasBatchPrep.setPivasMergeFormulaWorksheet(conventPivasMergeFormulaWorksheet(pivasBatchPrep));
					pivasBatchPrep.setPivasMergeFormulaProductLabel(conventPivasMergeFormulaProductLabel(pivasBatchPrep, false));
				}
				
				em.persist(pivasBatchPrep);
				
				if (pivasBatchPrep.getNumOfLabel() > 0) {
					printPivasBatchPrepLabel(pivasBatchPrep, false);
				}
			}
		}
		return pivasPrepValidateResponse;
	}
	
	public void printPivasBatchPrepLabel(PivasBatchPrep pivasBatchPrep, boolean isReprint){
		
		PivasWorksheet pivasWorksheet = null;
		PivasProductLabel pivasProductLabel = null;
		PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet = null;
		PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel = null;
		
		PrintDocType printDocType = (pivasBatchPrep.getPrintDocType() != null ? pivasBatchPrep.getPrintDocType() : PrintDocType.OtherLabel);
		PrinterSelect printerSelect = printerSelector.retrievePrinterSelect(printDocType);
		
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		
		if (pivasBatchPrep.getPivasFormulaType() != PivasFormulaType.Merge) {
			pivasWorksheet = pivasBatchPrep.getPivasWorksheet();
			pivasProductLabel = pivasBatchPrep.getPivasProductLabel();
			pivasProductLabel.setReprintFlag(isReprint);
		} else {
			pivasMergeFormulaWorksheet = pivasBatchPrep.getPivasMergeFormulaWorksheet();
			pivasMergeFormulaProductLabel = pivasBatchPrep.getPivasMergeFormulaProductLabel();
			pivasMergeFormulaProductLabel.setReprintFlag(isReprint);
		}
		
		PrintOption printOption = new PrintOption();
		Map<String, Object> workSheetParameters = new HashMap<String, Object>();
		Map<String, Object> productLabelParameters = new HashMap<String, Object>();
		workSheetParameters.put("hospCode", workstore.getHospCode());
		productLabelParameters.put("hospCode", workstore.getHospCode());
		productLabelParameters.put("SHOW_DRUG_LIST", LABEL_PIVASBATCHMERGEFORMULAPRODUCTLABEL_DRUGLIST_VISIBLE.get(false));
		
		boolean printWorksheet = !isReprint || 
								(isReprint && (pivasBatchPrep.getReprintType() == PivasBatchPrepReprintType.Both || 
												pivasBatchPrep.getReprintType() == PivasBatchPrepReprintType.Worksheet));
		
		boolean printProductLabel = !isReprint || 
								(isReprint && (pivasBatchPrep.getReprintType() == PivasBatchPrepReprintType.Both || 
												pivasBatchPrep.getReprintType() == PivasBatchPrepReprintType.ProductLabel));
		
		
		if ( printWorksheet ) {
			if ( isReprint && pivasBatchPrep.getReprintType() == PivasBatchPrepReprintType.Worksheet ) {
				printerSelect.setCopies(pivasBatchPrep.getReprintNumOfLabel());
			}
			
			if (pivasBatchPrep.getPivasFormulaType() != PivasFormulaType.Merge) {
				renderAndPrintJobList.add(new RenderAndPrintJob("PivasBatchWorksheet",
																	printOption,
																	printerSelect,
																	workSheetParameters,
																	Arrays.asList(pivasWorksheet),
																	"[PivasBatchWorksheet] lotNum:" + pivasBatchPrep.getLotNum()
																	));
			} else {
				renderAndPrintJobList.add(new RenderAndPrintJob("PivasBatchMergeFormulaWorksheet",
																	printOption,
																	printerSelect,
																	workSheetParameters,
																	Arrays.asList(pivasMergeFormulaWorksheet),
																	"[PivasBatchMergeFormulaWorksheet] lotNum:" + pivasBatchPrep.getLotNum()
																	));
			}

		}
		
		if ( printProductLabel ) {
			PrinterSelect tmpPrintSelect = new PrinterSelect(printerSelect);
			
			if ( isReprint && pivasBatchPrep.getReprintType() != PivasBatchPrepReprintType.Worksheet ) {
				tmpPrintSelect.setCopies(pivasBatchPrep.getReprintNumOfLabel());
			} else {
				tmpPrintSelect.setCopies(pivasBatchPrep.getNumOfLabel());
			}
			
			if (pivasBatchPrep.getPivasFormulaType() != PivasFormulaType.Merge) {
				renderAndPrintJobList.add(new RenderAndPrintJob("PivasBatchProductLabel",
																	printOption,
																	tmpPrintSelect,
																	productLabelParameters,
																	Arrays.asList(pivasProductLabel),
																	"[PivasBatchProductLabel] lotNum:" + pivasBatchPrep.getLotNum()
																	));
			} else {
				renderAndPrintJobList.add(new RenderAndPrintJob("PivasBatchMergeFormulaProductLabel",
																	printOption,
																	tmpPrintSelect,
																	productLabelParameters,
																	Arrays.asList(pivasMergeFormulaProductLabel),
																	"[PivasBatchMergeFormulaProductLabel] lotNum:" + pivasBatchPrep.getLotNum()
																	));
			}
		}
		printAgent.renderAndPrint(renderAndPrintJobList);
		
		if ( isReprint ) {
			auditLogger.log("#0898", pivasBatchPrep.getLotNum(), pivasBatchPrep.getReprintNumOfLabel(), pivasBatchPrep.getReprintType().getDisplayValue());
		}
	}
	
	public void getPivasProductLabelPreview(PivasBatchPrep pivasBatchPrep, boolean isQuickAdd){
		
		if (pivasBatchPrep.getPivasFormulaType() != PivasFormulaType.Merge) {
			if ( pivasBatchPrep.getPivasProductLabel() != null && !isQuickAdd ){
				//Preview for existing record of Product Label
				pivasProductLabel = pivasBatchPrep.getPivasProductLabel();
				pivasProductLabel.setReprintFlag(true);
			}else{
				//Preview for add and quick add of Product Label
				pivasProductLabel = conventPivasProductLabel(pivasBatchPrep, false);
			}
		} else {						
			if ( pivasBatchPrep.getPivasMergeFormulaProductLabel() != null && !isQuickAdd ){
				//Preview for existing record of Merge Formula Product Label
				pivasMergeFormulaProductLabel = pivasBatchPrep.getPivasMergeFormulaProductLabel();
				pivasMergeFormulaProductLabel.setReprintFlag(true);
			}else{
				//Preview for add and quick add of Merge Formula Product Label
				pivasMergeFormulaProductLabel = conventPivasMergeFormulaProductLabel(pivasBatchPrep, false);
			}
		}
	}
	
	public void generatePivasProductLabel() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		PrintOption printOption = new PrintOption();
		printOption.setPrintStatus(PrintStatus.Preview);
		printOption.setDocType("PDF".equals(Prop.LABEL_PREVIEW_FORMAT.get()) ? ReportProvider.PDF : ReportProvider.PNG);
		
		printAgent.renderAndRedirect(new RenderJob(
											"PivasBatchProductLabel", 
											printOption, 
											parameters, 
											Arrays.asList(pivasProductLabel)));
	}
	
	public void generatePivasMergeFormulaProductLabel() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("_PNG_ZOOM_RATIO", Float.valueOf(1.3f));
		parameters.put("SHOW_DRUG_LIST", LABEL_PIVASBATCHMERGEFORMULAPRODUCTLABEL_DRUGLIST_VISIBLE.get(false));
		PrintOption printOption = new PrintOption();
		printOption.setPrintStatus(PrintStatus.Preview);
		printOption.setDocType("PDF".equals(Prop.LABEL_PREVIEW_FORMAT.get()) ? ReportProvider.PDF : ReportProvider.PNG);
		
		printAgent.renderAndRedirect(new RenderJob(
											"PivasBatchMergeFormulaProductLabel", 
											printOption, 
											parameters, 
											Arrays.asList(pivasMergeFormulaProductLabel)));
	}
	
	private PivasWorksheet conventPivasWorksheet(PivasBatchPrep pivasBatchPrep){
		PivasBatchPrepItem pivasBatchPrepItem = pivasBatchPrep.getPivasBatchPrepItemList().get(0);
		PivasFormulaMethod pivasFormulaMethod = pivasBatchPrep.getPivasFormulaMethod();
		PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod();
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();
		
		PivasWorksheet pivasWorksheet = new PivasWorksheet();
		pivasWorksheet.setPrintName(pivasBatchPrep.getPrintName());
		pivasWorksheet.setDosageQty(convertToString(pivasDrugManufMethod.getDosageQty()));
		pivasWorksheet.setVolume(convertToString(pivasBatchPrep.getVolume()));
		pivasWorksheet.setDiluentVolume(convertToString(pivasFormulaMethod.getDiluentVolume()));
		pivasWorksheet.setPivasDosageUnit(pivasBatchPrep.getPivasDosageUnit());
		pivasWorksheet.setSiteDesc(pivasFormula.getDmSite().getIpmoeDesc());
		pivasWorksheet.setSolventVolume(convertToString(pivasDrugManufMethod.getVolume()));
		pivasWorksheet.setVendSupplySolvFlag(VEND_SUPPLY_SOLV.equals(pivasBatchPrepItem.getSolventCode()));
		pivasWorksheet.setManufactureDate(pivasBatchPrep.getManufactureDate());
		pivasWorksheet.setPrepExpiryDate(pivasBatchPrep.getPrepExpiryDate());
		pivasWorksheet.setLotNum(pivasBatchPrep.getLotNum());
		pivasWorksheet.setDrugItemCode(pivasBatchPrepItem.getDrugItemCode());
		pivasWorksheet.setDrugVolume(convertToString(pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getVolume()));
		pivasWorksheet.setDrugManufCode(pivasBatchPrepItem.getDrugManufCode());
		pivasWorksheet.setDrugBatchNum(pivasBatchPrepItem.getDrugBatchNum());
		pivasWorksheet.setDrugExpiryDate(pivasBatchPrepItem.getDrugExpiryDate());
		if((pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getVolume() != null) && (pivasFormulaMethod.getDiluentVolume() != null)) {
			pivasWorksheet.setRatio(convertToString(pivasFormulaMethod.getFirstPivasFormulaMethodDrug().getVolume().add(pivasFormulaMethod.getDiluentVolume())));
		}

		if(pivasBatchPrep.getDiluentDesc() != null){
			pivasWorksheet.setDiluentDesc(pivasBatchPrep.getDiluentDesc());
		}	
				
		if(pivasBatchPrepItem.getSolventDesc() != null){
			pivasWorksheet.setSolventDesc(pivasBatchPrepItem.getSolventDesc());
		}
		
		pivasWorksheet.setSolventItemCode(pivasBatchPrepItem.getSolventItemCode());
		pivasWorksheet.setSolventManufCode(pivasBatchPrepItem.getSolventManufCode());
		pivasWorksheet.setSolventBatchNum(pivasBatchPrepItem.getSolventBatchNum());
		pivasWorksheet.setSolventExpiryDate(pivasBatchPrepItem.getSolventExpiryDate());
		pivasWorksheet.setDiluentItemCode(pivasBatchPrep.getDiluentItemCode());
		pivasWorksheet.setDiluentManufCode(pivasBatchPrep.getDiluentManufCode());
		pivasWorksheet.setDiluentBatchNum(pivasBatchPrep.getDiluentBatchNum());
		pivasWorksheet.setDiluentExpiryDate(pivasBatchPrep.getDiluentExpiryDate());
		
		pivasWorksheet.setPrepQty(convertToString(pivasBatchPrep.getPrepQty()));
		pivasWorksheet.setContainerName(pivasBatchPrep.getPivasContainer().getContainerName());
		pivasWorksheet.setConcn(convertToString(pivasBatchPrep.getPivasFormulaMethod().getPivasFormula().getConcn()));
		pivasWorksheet.setAfterConcn(convertToString(pivasBatchPrep.getPivasFormulaMethod().getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getAfterConcn()));
		
		DmDrug drugDmDrug = pivasBatchPrep.getPivasFormulaMethod().getFirstPivasFormulaMethodDrug().getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug().getDmDrug();
		String drugStrength = StringUtils.trimToEmpty(drugDmDrug.getStrength());
		String drugVolumeValue = convertToString(drugDmDrug.getVolumeValue());
		String drugVolumeUnit = StringUtils.trimToEmpty(drugDmDrug.getVolumeUnit());
		
		pivasWorksheet.setDrugStrength(drugStrength);
		pivasWorksheet.setDrugVolumeValue(drugVolumeValue);
		pivasWorksheet.setDrugVolumeUnit(drugVolumeUnit);
		pivasWorksheet.setDrugTruncatedStrengthVolume(getTruncatedStrengthVolume(drugStrength, drugVolumeValue, drugVolumeUnit));
		// save for PIVAS report
		pivasWorksheet.setDrugItemDesc(drugDmDrug.getFullDrugDesc());
		
		if ( StringUtils.isNoneBlank(pivasBatchPrep.getDiluentItemCode()) ) {
			DmDrug diluentDmDrug = pivasBatchPrep.getDiluentDmDrug();
			String diluentStrength = StringUtils.trimToEmpty(diluentDmDrug.getStrength());
			String diluentVolumeValue = convertToString(diluentDmDrug.getVolumeValue());
			String diluentVolumeUnit = StringUtils.trimToEmpty(diluentDmDrug.getVolumeUnit());
			
			pivasWorksheet.setDiluentStrength(diluentStrength);
			pivasWorksheet.setDiluentVolumeValue(diluentVolumeValue);
			pivasWorksheet.setDiluentVolumeUnit(diluentVolumeUnit);
			pivasWorksheet.setDiluentTruncatedStrengthVolume(getTruncatedStrengthVolume(diluentStrength, diluentVolumeValue, diluentVolumeUnit));
			// save for PIVAS report
			pivasWorksheet.setDiluentItemDesc(diluentDmDrug.getFullDrugDesc());
		}

		if ( StringUtils.isNoneBlank(pivasBatchPrepItem.getSolventItemCode()) ) {
			DmDrug solventDmDrug = pivasBatchPrepItem.getSolventDmDrug();
			String solventStrength = StringUtils.trimToEmpty(solventDmDrug.getStrength()); 
			String solventVolumeValue = convertToString(solventDmDrug.getVolumeValue());
			String solventVolumeUnit = StringUtils.trimToEmpty(solventDmDrug.getVolumeUnit());
			
			pivasWorksheet.setSolventStrength(solventStrength);
			pivasWorksheet.setSolventVolumeValue(solventVolumeValue);
			pivasWorksheet.setSolventVolumeUnit(solventVolumeUnit);
			pivasWorksheet.setSolventTruncatedStrengthVolume(getTruncatedStrengthVolume(solventStrength, solventVolumeValue, solventVolumeUnit));
			// save for PIVAS report
			pivasWorksheet.setSolventItemDesc(solventDmDrug.getFullDrugDesc());
		}
		pivasWorksheet.setFinalVolume(convertToString(pivasBatchPrep.getVolume()));
		pivasWorksheet.setFormulaPrintName(pivasFormula.getPrintName());
		pivasWorksheet.setFormCode(StringUtils.trimToEmpty(drugDmDrug.getDmDrugProperty().getFormCode()));
		pivasWorksheet.setSaltProperty(StringUtils.trimToEmpty(drugDmDrug.getDmDrugProperty().getSaltProperty()));
		
		return pivasWorksheet;
	}
	
	private PivasMergeFormulaWorksheet conventPivasMergeFormulaWorksheet(PivasBatchPrep pivasBatchPrep){
		
		PivasFormulaMethod pivasFormulaMethod = pivasBatchPrep.getPivasFormulaMethod();
		PivasFormula pivasFormula = pivasFormulaMethod.getPivasFormula();
		
		PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet = new PivasMergeFormulaWorksheet();
		
		pivasMergeFormulaWorksheet.setLotNum(pivasBatchPrep.getLotNum());
		pivasMergeFormulaWorksheet.setPrintName(pivasBatchPrep.getPrintName());
		pivasMergeFormulaWorksheet.setSiteDesc(pivasFormula.getDmSite().getIpmoeDesc());
		
		pivasMergeFormulaWorksheet.setContainerName(pivasBatchPrep.getPivasContainer().getContainerName());
		pivasMergeFormulaWorksheet.setPrepQty(convertToString(pivasBatchPrep.getPrepQty()));
		pivasMergeFormulaWorksheet.setExpiryDateTitle(pivasBatchPrep.getExpiryDateTitle());
		pivasMergeFormulaWorksheet.setPrepExpiryDate(pivasBatchPrep.getPrepExpiryDate());
		pivasMergeFormulaWorksheet.setManufactureDate(pivasBatchPrep.getManufactureDate());
		
		// save for PIVAS report
		pivasMergeFormulaWorksheet.setTargetProductItemCode(pivasBatchPrep.getItemCode());
		// save for PIVAS report
		pivasMergeFormulaWorksheet.setTargetProductItemDrugKey(pivasBatchPrep.getDrugKey());
		// save for PIVAS report
		pivasMergeFormulaWorksheet.setFormulaPrintName(pivasFormula.getPrintName());
		// save for PIVAS report
		pivasMergeFormulaWorksheet.setFinalVolume(convertToString(pivasBatchPrep.getVolume()));
		
		
		// sort fmd ID, build sortSeq for labelXml
		// sortSeq has same sorting order with fmd ID
		Map<Long, Pair<Integer, PivasBatchPrepItem>> fmdIdPivasBatchPrepItemMap = new TreeMap<Long, Pair<Integer, PivasBatchPrepItem>>();
		for (PivasBatchPrepItem pivasBatchPrepItem :pivasBatchPrep.getPivasBatchPrepItemList()) {
			fmdIdPivasBatchPrepItemMap.put(pivasBatchPrepItem.getPivasFormulaMethodDrugId(), new Pair<Integer, PivasBatchPrepItem>(null, pivasBatchPrepItem));
		}
		
		Integer mapPairSortSeq = 0;
		for (Long fmdId: fmdIdPivasBatchPrepItemMap.keySet()) {
			Pair<Integer, PivasBatchPrepItem> mapPair = fmdIdPivasBatchPrepItemMap.get(fmdId);
			mapPair.setFirst(mapPairSortSeq);
			mapPairSortSeq++;
		}
		
		
		List<PivasMergeFormulaWorksheetSolventInstructionDtl> pivasMergeFormulaWorksheetSolventInstructionDtlList = new ArrayList<PivasMergeFormulaWorksheetSolventInstructionDtl>();
		List<PivasMergeFormulaWorksheetDrugInstructionDtl> pivasMergeFormulaWorksheetDrugInstructionDtlList = new ArrayList<PivasMergeFormulaWorksheetDrugInstructionDtl>();
		List<PivasMergeFormulaWorksheetRawMaterialDtl> pivasMergeFormulaWorksheetRawMaterialDtlList = new ArrayList<PivasMergeFormulaWorksheetRawMaterialDtl>();
		
		for (Pair<Integer, PivasBatchPrepItem> pair :fmdIdPivasBatchPrepItemMap.values()) {
			Integer sortSeqForDrugAndSolvent = pair.getFirst();
			PivasBatchPrepItem pivasBatchPrepItem = pair.getSecond();
			
			// construct drug info
			Pair<PivasMergeFormulaWorksheetDrugInstructionDtl, PivasMergeFormulaWorksheetRawMaterialDtl> drugPair = constructPivasMergeFormulaWorksheetDrugInfo(pivasBatchPrepItem, sortSeqForDrugAndSolvent);
			pivasMergeFormulaWorksheetDrugInstructionDtlList.add(drugPair.getFirst());
			pivasMergeFormulaWorksheetRawMaterialDtlList.add(drugPair.getSecond());
			
			// construct solvent info
			if (StringUtils.isNoneBlank(pivasBatchPrepItem.getSolventDesc())) {
				Pair<PivasMergeFormulaWorksheetSolventInstructionDtl, PivasMergeFormulaWorksheetRawMaterialDtl> solventPair = constructPivasMergeFormulaWorksheetSolventInfo(pivasBatchPrepItem, sortSeqForDrugAndSolvent);
				pivasMergeFormulaWorksheetSolventInstructionDtlList.add(solventPair.getFirst());
				pivasMergeFormulaWorksheetRawMaterialDtlList.add(solventPair.getSecond());
			}
		}
		
		// construct diluent info
		if (StringUtils.isNoneBlank(pivasBatchPrep.getDiluentDesc())) {
			// diluent must be the last item in PivasMergeFormulaWorksheetRawMaterialDtl
			Integer diluentSortSeq = pivasMergeFormulaWorksheetRawMaterialDtlList.get(pivasMergeFormulaWorksheetRawMaterialDtlList.size()-1).getSortSeq() + 1;
			
			Pair<PivasMergeFormulaWorksheet, PivasMergeFormulaWorksheetRawMaterialDtl> diluentPair = constructPivasMergeFormulaWorksheetDiluentInfo(pivasMergeFormulaWorksheet, pivasBatchPrep, diluentSortSeq);
			pivasMergeFormulaWorksheet = diluentPair.getFirst();
			pivasMergeFormulaWorksheetRawMaterialDtlList.add(diluentPair.getSecond());
		}	
		
		pivasMergeFormulaWorksheet.setPivasMergeFormulaWorksheetSolventInstructionDtlList(pivasMergeFormulaWorksheetSolventInstructionDtlList);
		pivasMergeFormulaWorksheet.setPivasMergeFormulaWorksheetDrugInstructionDtlList(pivasMergeFormulaWorksheetDrugInstructionDtlList);
		pivasMergeFormulaWorksheet.setPivasMergeFormulaWorksheetRawMaterialDtlList(pivasMergeFormulaWorksheetRawMaterialDtlList);
		
		return pivasMergeFormulaWorksheet;
	}
	
	private Pair<PivasMergeFormulaWorksheetDrugInstructionDtl, PivasMergeFormulaWorksheetRawMaterialDtl> constructPivasMergeFormulaWorksheetDrugInfo(PivasBatchPrepItem pivasBatchPrepItem, Integer sortSeq) {
		
		PivasFormulaMethodDrug pivasFormulaMethodDrug = pivasBatchPrepItem.getPivasFormulaMethodDrug();
		PivasFormulaDrug pivasFormulaDrug = pivasFormulaMethodDrug.getPivasFormulaDrug();
		PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
		DmDrug drugDmDrug = pivasDrugManufMethod.getPivasDrugManuf().getPivasDrug().getDmDrug();
		DmDrugProperty drugDmDrugProperty = drugDmDrug.getDmDrugProperty();
		
		String drugItemCode = pivasBatchPrepItem.getDrugItemCode();
		
		// build drug row
		PivasMergeFormulaWorksheetDrugInstructionDtl drugInstructionDtl = new PivasMergeFormulaWorksheetDrugInstructionDtl();
		drugInstructionDtl.setSortSeq(sortSeq);
		drugInstructionDtl.setDrugVolume(convertToString(pivasBatchPrepItem.getAfterVolume()));
		drugInstructionDtl.setMutipleItemCodeWithSameDrugKeyExist(Boolean.FALSE);
		drugInstructionDtl.setDrugKey(pivasBatchPrepItem.getDrugKey());
		drugInstructionDtl.setDrugItemCode(drugItemCode);
		
		drugInstructionDtl.setDrugDisplayName(pivasFormulaDrug.getDisplayname());
		drugInstructionDtl.setDrugSaltProperty(StringUtils.trimToEmpty(pivasFormulaDrug.getSaltProperty()));
		drugInstructionDtl.setDrugStrength(StringUtils.trimToEmpty(pivasFormulaDrug.getStrength()));
		
		drugInstructionDtl.setPivasDosageUnit(pivasBatchPrepItem.getPivasDosageUnit());
		// concn. after recon.
		drugInstructionDtl.setAfterConcn(convertToString(pivasDrugManufMethod.getAfterConcn()));
		// save for PIVAS report
		drugInstructionDtl.setDrugConcn(convertToString(pivasBatchPrepItem.getAfterConcn()));
		// save for PIVAS report
		drugInstructionDtl.setDosage(convertToString(pivasBatchPrepItem.getDosageQty()));
		
		
		
		// build drug raw material
		PivasMergeFormulaWorksheetRawMaterialDtl drugRawMaterialDtl = new PivasMergeFormulaWorksheetRawMaterialDtl();
		drugRawMaterialDtl.setSortSeq(sortSeq);
		drugRawMaterialDtl.setDrugKey(drugDmDrug.getDrugKey());
		
		drugRawMaterialDtl.setSolventDesc(null);
		drugRawMaterialDtl.setDiluentDesc(null);
		drugRawMaterialDtl.setVendSupplySolvFlag(Boolean.FALSE);
		
		drugRawMaterialDtl.setItemCode(drugItemCode);
		
		drugRawMaterialDtl.setStrength(StringUtils.trimToEmpty(drugDmDrug.getStrength()));
		drugRawMaterialDtl.setVolumeValue(convertToString(drugDmDrug.getVolumeValue()));
		drugRawMaterialDtl.setVolumeUnit(StringUtils.trimToEmpty(drugDmDrug.getVolumeUnit()));
		
		drugRawMaterialDtl.setManufCode(pivasBatchPrepItem.getDrugManufCode());
		drugRawMaterialDtl.setBatchNum(pivasBatchPrepItem.getDrugBatchNum());
		drugRawMaterialDtl.setExpiryDate(pivasBatchPrepItem.getDrugExpiryDate());
		
		drugRawMaterialDtl.setDisplayName(drugDmDrugProperty.getDisplayname());
		drugRawMaterialDtl.setSaltProperty(StringUtils.trimToEmpty(drugDmDrugProperty.getSaltProperty()));
		drugRawMaterialDtl.setFormCode(StringUtils.trimToEmpty(drugDmDrugProperty.getFormCode()));
		// save for PIVAS report
		drugRawMaterialDtl.setItemDesc(drugDmDrug.getFullDrugDesc());

		return new Pair<PivasMergeFormulaWorksheetDrugInstructionDtl, PivasMergeFormulaWorksheetRawMaterialDtl>(drugInstructionDtl, drugRawMaterialDtl);
	}
	
	private Pair<PivasMergeFormulaWorksheetSolventInstructionDtl, PivasMergeFormulaWorksheetRawMaterialDtl> constructPivasMergeFormulaWorksheetSolventInfo(PivasBatchPrepItem pivasBatchPrepItem, Integer sortSeq) {
		
		PivasFormulaMethodDrug pivasFormulaMethodDrug = pivasBatchPrepItem.getPivasFormulaMethodDrug();
		PivasFormulaDrug pivasFormulaDrug = pivasFormulaMethodDrug.getPivasFormulaDrug();
		PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
		
		// handle vendSupplySolv
		boolean vendSupplySolvFlag = VEND_SUPPLY_SOLV.equals(pivasBatchPrepItem.getSolventCode());
		String solventItemCode = pivasBatchPrepItem.getSolventItemCode();
		
		// build solvent row with some of the drug info
		PivasMergeFormulaWorksheetSolventInstructionDtl solventInstructionDtl = new PivasMergeFormulaWorksheetSolventInstructionDtl();
		solventInstructionDtl.setSortSeq(sortSeq);
		solventInstructionDtl.setMutipleItemCodeWithSameDrugKeyExist(Boolean.FALSE);
		
		solventInstructionDtl.setDrugKey(pivasBatchPrepItem.getDrugKey());
		solventInstructionDtl.setDrugItemCode(pivasBatchPrepItem.getDrugItemCode());
		
		solventInstructionDtl.setDrugDisplayName(pivasFormulaDrug.getDisplayname());
		solventInstructionDtl.setDrugSaltProperty(StringUtils.trimToEmpty(pivasFormulaDrug.getSaltProperty()));
		solventInstructionDtl.setDrugStrength(StringUtils.trimToEmpty(pivasFormulaDrug.getStrength()));
		
		solventInstructionDtl.setDosageQty(convertToString(pivasDrugManufMethod.getDosageQty()));
		solventInstructionDtl.setPivasDosageUnit(pivasBatchPrepItem.getPivasDosageUnit());
		solventInstructionDtl.setSolventVolume(convertToString(pivasDrugManufMethod.getVolume()));
		solventInstructionDtl.setSolventDesc(pivasBatchPrepItem.getSolventDesc());
		// save for PIVAS report
		if ( !vendSupplySolvFlag && StringUtils.isNoneBlank(solventItemCode) ) {
			solventInstructionDtl.setSolventItemCode(solventItemCode);
		}
		// save for PIVAS report
		solventInstructionDtl.setVendSupplySolvFlag(vendSupplySolvFlag);
		
		
		
		// build solvent raw material solvent
		PivasMergeFormulaWorksheetRawMaterialDtl solventRawMaterialDtl = new PivasMergeFormulaWorksheetRawMaterialDtl();
		solventRawMaterialDtl.setSortSeq(sortSeq);
		
		solventRawMaterialDtl.setSolventDesc(pivasBatchPrepItem.getSolventDesc());
		solventRawMaterialDtl.setDiluentDesc(null);
		solventRawMaterialDtl.setVendSupplySolvFlag(vendSupplySolvFlag);

		solventRawMaterialDtl.setBatchNum(pivasBatchPrepItem.getSolventBatchNum());
		solventRawMaterialDtl.setExpiryDate(pivasBatchPrepItem.getSolventExpiryDate());
		
		// handle vendSupplySolv
		if ( !vendSupplySolvFlag && StringUtils.isNoneBlank(solventItemCode) ) {
			DmDrug solventDmDrug = dmDrugCacher.getDmDrug(solventItemCode);
			DmDrugProperty solventDmDrugProperty = solventDmDrug.getDmDrugProperty();
			
			solventRawMaterialDtl.setDrugKey(solventDmDrug.getDrugKey());
			
			solventRawMaterialDtl.setItemCode(solventItemCode);
			
			solventRawMaterialDtl.setStrength(StringUtils.trimToEmpty(solventDmDrug.getStrength()));
			solventRawMaterialDtl.setVolumeValue(convertToString(solventDmDrug.getVolumeValue()));
			solventRawMaterialDtl.setVolumeUnit(StringUtils.trimToEmpty(solventDmDrug.getVolumeUnit()));
			
			solventRawMaterialDtl.setManufCode(pivasBatchPrepItem.getSolventManufCode());
			
			solventRawMaterialDtl.setDisplayName(solventDmDrugProperty.getDisplayname());
			solventRawMaterialDtl.setSaltProperty(StringUtils.trimToEmpty(solventDmDrugProperty.getSaltProperty()));
			solventRawMaterialDtl.setFormCode(StringUtils.trimToEmpty(solventDmDrugProperty.getFormCode()));
			// save for PIVAS report
			solventRawMaterialDtl.setItemDesc(solventDmDrug.getFullDrugDesc());
		}
		
		return new Pair<PivasMergeFormulaWorksheetSolventInstructionDtl, PivasMergeFormulaWorksheetRawMaterialDtl>(solventInstructionDtl, solventRawMaterialDtl);
	}
	
	private Pair<PivasMergeFormulaWorksheet, PivasMergeFormulaWorksheetRawMaterialDtl> constructPivasMergeFormulaWorksheetDiluentInfo(PivasMergeFormulaWorksheet pivasMergeFormulaWorksheet, PivasBatchPrep pivasBatchPrep, Integer sortSeq) {
		
		PivasFormulaMethod pivasFormulaMethod = pivasBatchPrep.getPivasFormulaMethod();
		DmDrug diluentDmDrug = dmDrugCacher.getDmDrug(pivasBatchPrep.getDiluentItemCode());
		DmDrugProperty diluentDmDrugProperty = diluentDmDrug.getDmDrugProperty();
		
		String diluentItemCode = pivasBatchPrep.getDiluentItemCode();
		
		// build diluent row
		pivasMergeFormulaWorksheet.setDiluentDesc(pivasBatchPrep.getDiluentDesc());
		pivasMergeFormulaWorksheet.setDiluentVolume(convertToString(pivasFormulaMethod.getDiluentVolume()));
		// save for PIVAS report
		pivasMergeFormulaWorksheet.setDiluentItemCode(diluentItemCode);
		
		
		
		// build diluent raw material
		PivasMergeFormulaWorksheetRawMaterialDtl diluentRawMaterialDtl = new PivasMergeFormulaWorksheetRawMaterialDtl();
		// diluent must be the last item in PivasMergeFormulaWorksheetRawMaterialDtl
		diluentRawMaterialDtl.setSortSeq(sortSeq);
		diluentRawMaterialDtl.setDrugKey(diluentDmDrug.getDrugKey());
		
		diluentRawMaterialDtl.setSolventDesc(null);
		diluentRawMaterialDtl.setDiluentDesc(pivasBatchPrep.getDiluentDesc());
		diluentRawMaterialDtl.setVendSupplySolvFlag(Boolean.FALSE);
		
		diluentRawMaterialDtl.setItemCode(diluentItemCode);
		
		diluentRawMaterialDtl.setStrength(StringUtils.trimToEmpty(diluentDmDrug.getStrength()));
		diluentRawMaterialDtl.setVolumeValue(convertToString(diluentDmDrug.getVolumeValue()));
		diluentRawMaterialDtl.setVolumeUnit(StringUtils.trimToEmpty(diluentDmDrug.getVolumeUnit()));
		
		diluentRawMaterialDtl.setManufCode(pivasBatchPrep.getDiluentManufCode());
		diluentRawMaterialDtl.setBatchNum(pivasBatchPrep.getDiluentBatchNum());
		diluentRawMaterialDtl.setExpiryDate(pivasBatchPrep.getDiluentExpiryDate());
		
		diluentRawMaterialDtl.setDisplayName(diluentDmDrugProperty.getDisplayname());
		diluentRawMaterialDtl.setSaltProperty(StringUtils.trimToEmpty(diluentDmDrugProperty.getSaltProperty()));
		diluentRawMaterialDtl.setFormCode(StringUtils.trimToEmpty(diluentDmDrugProperty.getFormCode()));
		// save for PIVAS report
		diluentRawMaterialDtl.setItemDesc(diluentDmDrug.getFullDrugDesc());
		
		return new Pair<PivasMergeFormulaWorksheet, PivasMergeFormulaWorksheetRawMaterialDtl>(pivasMergeFormulaWorksheet, diluentRawMaterialDtl);
	}
	
	private PivasProductLabel conventPivasProductLabel(PivasBatchPrep pivasBatchPrep, boolean isReprint){
		PivasProductLabel pivasProductLabel = new PivasProductLabel();
		pivasProductLabel.setPrintName(pivasBatchPrep.getPrintName());
		pivasProductLabel.setSiteDesc(pivasBatchPrep.getPivasFormulaMethod().getPivasFormula().getDmSite().getIpmoeDesc());
		pivasProductLabel.setLotNum(pivasBatchPrep.getLotNum());
		pivasProductLabel.setPrepExpiryDate(pivasBatchPrep.getPrepExpiryDate());
		
		if(pivasBatchPrep.getDmWarning1() != null){
			pivasProductLabel.setWarnCode1(pivasBatchPrep.getDmWarning1().getWarnMessageEng());
		}else{
			pivasProductLabel.setWarnCode1("");
		}
		
		if(pivasBatchPrep.getDmWarning2() != null){
			pivasProductLabel.setWarnCode2(pivasBatchPrep.getDmWarning2().getWarnMessageEng());
		}else{
			pivasProductLabel.setWarnCode2("");
		}
		pivasProductLabel.setAfterConcn(convertToString(pivasBatchPrep.getPivasFormulaMethod().getPivasFormula().getConcn()));
		pivasProductLabel.setPivasDosageUnit(pivasBatchPrep.getPivasDosageUnit());
		pivasProductLabel.setManufactureDate(pivasBatchPrep.getManufactureDate());
		pivasProductLabel.setReprintFlag(isReprint);
		
		// save for PIVAS report
		pivasProductLabel.setNumOfLabelSet(pivasBatchPrep.getNumOfLabel());
		
		return pivasProductLabel;
	}
	
	private PivasMergeFormulaProductLabel conventPivasMergeFormulaProductLabel(PivasBatchPrep pivasBatchPrep, boolean isReprint){
		
		PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel = new PivasMergeFormulaProductLabel();
		
		pivasMergeFormulaProductLabel.setLotNum(pivasBatchPrep.getLotNum());
		pivasMergeFormulaProductLabel.setPrintName(pivasBatchPrep.getPrintName());
		pivasMergeFormulaProductLabel.setSiteDesc(pivasBatchPrep.getPivasFormulaMethod().getPivasFormula().getDmSite().getIpmoeDesc());
		
		if (pivasBatchPrep.getDmWarning1() != null) {
			pivasMergeFormulaProductLabel.setWarnCode1(pivasBatchPrep.getDmWarning1().getWarnMessageEng());
		} else {
			pivasMergeFormulaProductLabel.setWarnCode1("");
		}
		
		if (pivasBatchPrep.getDmWarning2() != null) {
			pivasMergeFormulaProductLabel.setWarnCode2(pivasBatchPrep.getDmWarning2().getWarnMessageEng());
		} else {
			pivasMergeFormulaProductLabel.setWarnCode2("");
		}
		
		if (pivasBatchPrep.getDmWarning3() != null) {
			pivasMergeFormulaProductLabel.setWarnCode3(pivasBatchPrep.getDmWarning3().getWarnMessageEng());
		} else {
			pivasMergeFormulaProductLabel.setWarnCode3("");
		}
		
		if (pivasBatchPrep.getDmWarning4() != null) {
			pivasMergeFormulaProductLabel.setWarnCode4(pivasBatchPrep.getDmWarning4().getWarnMessageEng());
		} else {
			pivasMergeFormulaProductLabel.setWarnCode4("");
		}
		
		pivasMergeFormulaProductLabel.setExpiryDateTitle(pivasBatchPrep.getExpiryDateTitle());
		pivasMergeFormulaProductLabel.setPrepExpiryDate(pivasBatchPrep.getPrepExpiryDate());
		pivasMergeFormulaProductLabel.setManufactureDate(pivasBatchPrep.getManufactureDate());
		// save for PIVAS report
		pivasMergeFormulaProductLabel.setTargetProductItemCode(pivasBatchPrep.getItemCode());
		// save for PIVAS report
		pivasMergeFormulaProductLabel.setTargetProductItemDrugKey(pivasBatchPrep.getDrugKey());
		// save for PIVAS report
		pivasMergeFormulaProductLabel.setNumOfLabelSet(pivasBatchPrep.getNumOfLabel());
		
		pivasMergeFormulaProductLabel.setReprintFlag(isReprint);
		
		pivasMergeFormulaProductLabel.setFinalVolume(convertToString(pivasBatchPrep.getVolume()));
		
		pivasMergeFormulaProductLabel.setPivasMergeFormulaProductLabelDrugDtlList(constructPivasMergeFormulaProductLabelDrugDtlList(pivasMergeFormulaProductLabel, pivasBatchPrep));
		
		return pivasMergeFormulaProductLabel;
	}	
	
	private List<PivasMergeFormulaProductLabelDrugDtl> constructPivasMergeFormulaProductLabelDrugDtlList(PivasMergeFormulaProductLabel pivasMergeFormulaProductLabel, PivasBatchPrep pivasBatchPrep){
		List<PivasMergeFormulaProductLabelDrugDtl> pivasMergeFormulaProductLabelDrugDtlList = new ArrayList<PivasMergeFormulaProductLabelDrugDtl>();
		
		
		// sort fmd ID, build sortSeq for labelXml
		// sortSeq has same sorting order with fmd ID
		Map<Long, Pair<Integer, PivasBatchPrepItem>> fmdIdPivasBatchPrepItemMap = new TreeMap<Long, Pair<Integer, PivasBatchPrepItem>>();
		for (PivasBatchPrepItem pivasBatchPrepItem :pivasBatchPrep.getPivasBatchPrepItemList()) {
			fmdIdPivasBatchPrepItemMap.put(pivasBatchPrepItem.getPivasFormulaMethodDrugId(), new Pair<Integer, PivasBatchPrepItem>(null, pivasBatchPrepItem));
		}
		
		Integer mapPairSortSeq = 0;
		for (Long fmdId: fmdIdPivasBatchPrepItemMap.keySet()) {
			Pair<Integer, PivasBatchPrepItem> mapPair = fmdIdPivasBatchPrepItemMap.get(fmdId);
			mapPair.setFirst(mapPairSortSeq);
			mapPairSortSeq++;
		}
		for (Pair<Integer, PivasBatchPrepItem> pair :fmdIdPivasBatchPrepItemMap.values()) {
			Integer drugSortSeq = pair.getFirst();
			PivasBatchPrepItem pivasBatchPrepItem = pair.getSecond();
				
			PivasFormulaMethodDrug pivasFormulaMethodDrug = pivasBatchPrepItem.getPivasFormulaMethodDrug();
			PivasFormulaDrug pivasFormulaDrug = pivasFormulaMethodDrug.getPivasFormulaDrug();
			PivasDrugManufMethod pivasDrugManufMethod = pivasFormulaMethodDrug.getPivasDrugManufMethod();
			
			//construct drug info
			PivasMergeFormulaProductLabelDrugDtl drugDtl = new PivasMergeFormulaProductLabelDrugDtl();		
			drugDtl.setSortSeq(drugSortSeq);
			drugDtl.setDrugVolume(convertToString(pivasBatchPrepItem.getAfterVolume()));
			drugDtl.setDrugDisplayName(pivasFormulaDrug.getDisplayname());
			drugDtl.setDrugSaltProperty(StringUtils.trimToEmpty(pivasFormulaDrug.getSaltProperty()));
			drugDtl.setDrugStrength(StringUtils.trimToEmpty(pivasFormulaDrug.getStrength()));
			drugDtl.setPivasDosageUnit(pivasBatchPrepItem.getPivasDosageUnit());		
			drugDtl.setAfterConcn(convertToString(pivasDrugManufMethod.getAfterConcn()));	

			drugDtl.setDiluentDesc(null);
			drugDtl.setDiluentVolume(null);
					
			pivasMergeFormulaProductLabelDrugDtlList.add(drugDtl);
			
			}		
		
		// construct diluent info
		if (StringUtils.isNoneBlank(pivasBatchPrep.getDiluentDesc())) {
			// diluent must be the last item in PivasMergeFormulaProductLabelDrugDtlList
			Integer diluentSortSeq = pivasMergeFormulaProductLabelDrugDtlList.get(pivasMergeFormulaProductLabelDrugDtlList.size()-1).getSortSeq() + 1;
			
			
			PivasMergeFormulaProductLabelDrugDtl diluentDrugDtl = new PivasMergeFormulaProductLabelDrugDtl();		
			diluentDrugDtl.setSortSeq(diluentSortSeq);
			diluentDrugDtl.setDiluentDesc(pivasBatchPrep.getDiluentDesc());
			diluentDrugDtl.setDiluentVolume(convertToString(pivasBatchPrep.getPivasFormulaMethod().getDiluentVolume()));
			pivasMergeFormulaProductLabelDrugDtlList.add(diluentDrugDtl);
		}
		
		return pivasMergeFormulaProductLabelDrugDtlList;
		
	}
	
	// PMSIPU-2601
	private String getTruncatedStrengthVolume(String strength, String volumeValue, String volumeUnit) {
		Integer strLenStartToTruncate = Integer.valueOf(20);
		String fullStr = "";
		
		if (StringUtils.isNotBlank(strength)) {
			fullStr += strength;
		}
		
		if (StringUtils.isNotBlank(volumeValue) && StringUtils.isNotBlank(volumeUnit)) {
			if (fullStr.length() > 0) {
				fullStr += SPACE;
			}
			fullStr += volumeValue + volumeUnit;
		}
		
		if (fullStr.length() > strLenStartToTruncate) {
			return fullStr.substring(0, strLenStartToTruncate) + "...";
		} else {
			return null;
		}
	}
	
	private String convertToString(Double value){
		if ( value == null ) {
			return StringUtils.EMPTY;
		}
		Double decimalValue =  value - value.intValue();
		if ( decimalValue == 0 ) {
			return Integer.toString(value.intValue());
		}
		return value.toString();
	}
	
	private String convertToString(BigDecimal value){
		if ( value == null ) {
			return StringUtils.EMPTY;
		}
		Double decimalValue =  value.doubleValue() - value.intValue();
		if ( decimalValue == 0 ) {
			return Integer.toString(value.intValue());
		}
		return value.toPlainString();
	}
	
	@SuppressWarnings("unchecked")
	public List<PivasBatchPrep> retrievePivasBatchPrepList(Date manufFromDate, Date manufToDate, Integer drugKey){
		DateMidnight toDate = new DateMidnight(manufToDate).plusDays(1);
		StringBuilder sb = new StringBuilder(
				"select o from PivasBatchPrep o" + // 20171023 index check : PivasBatchPrep.lotNum : PK_PIVAS_BATCH_PREP
				" where o.lotNum in " +
				"  (select distinct i.pivasBatchPrepLotNum from PivasBatchPrepItem i" + // 20160615 index check : PivasBatchPrep.hospCode,manufactureDate : I_PIVAS_BATCH_PREP_01
				"   where i.pivasBatchPrep.hospCode = :hospCode" +
				"   and i.pivasBatchPrep.manufactureDate >= :manufFromDate " +
				"   and i.pivasBatchPrep.manufactureDate < :manufToDate");
		
		if ( drugKey != null ) {
			sb.append("   and i.drugKey = :drugKey");
		}
		sb.append(" ) order by o.lotNum");
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("hospCode", workstore.getHospCode());
		query.setParameter("manufFromDate", manufFromDate, TemporalType.DATE);
		query.setParameter("manufToDate", toDate.toDate(), TemporalType.DATE);
		
		if ( drugKey != null ) {
			query.setParameter("drugKey", drugKey);
		}
		
		query.setHint(QueryHints.BATCH, PIVAS_BATCH_PREP_QUERY_HINT_BATCH_1);
		
		List<PivasBatchPrep> resultList = query.getResultList();
		MedProfileUtils.lookup(resultList, PIVAS_BATCH_PREP_QUERY_HINT_BATCH_1);
		
		return resultList;
	}
	
	public AddPivasBatchPrepDetail retrievePivasBatchPrep(String lotNo){
		PivasBatchPrep pivasBatchPrep = em.find(PivasBatchPrep.class, lotNo);
		if ( pivasBatchPrep == null || pivasBatchPrep.getStatus() == PivasBatchPrepStatus.Deleted) {
			return null;
		}
		PivasBatchPrepItem firstPivasBatchPrepItem = pivasBatchPrep.getPivasBatchPrepItemList().get(0);
		
		PivasPrepCriteria pivasPrepCriteria = new PivasPrepCriteria();
		pivasPrepCriteria.setPivasFormulaMethodId(pivasBatchPrep.getPivasFormulaMethodId());
		pivasPrepCriteria.setSolventManufCode(firstPivasBatchPrepItem.getSolventManufCode());
		pivasPrepCriteria.setDiluentManufCode(pivasBatchPrep.getDiluentManufCode());
		pivasPrepCriteria.setPivasContainerId(pivasBatchPrep.getPivasContainerId());
		pivasPrepCriteria.setWorkstoreGroupCode(workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		PivasBatchPrepDetail pivasBatchPrepDetail = dmsPmsServiceProxy.retrievePivasBatchPrepDetail(pivasPrepCriteria, false);
		
		for (PivasFormulaMethodDrug pivasFormulaMethodDrug: pivasBatchPrepDetail.getPivasFormulaMethod().getPivasFormulaMethodDrugList()) {
			PivasDrug pivasDrug = pivasFormulaMethodDrug.getPivasDrugManufMethod().getPivasDrugManuf().getPivasDrug();
			pivasDrug.setDmDrug(dmDrugCacher.getDmDrug(pivasDrug.getItemCode()));
			
		    for (PivasBatchPrepItem pivasBatchPrepItem: pivasBatchPrep.getPivasBatchPrepItemList()) {
			    if (pivasBatchPrepItem.getPivasFormulaMethodDrugId().equals(pivasFormulaMethodDrug.getId())) {
				   pivasBatchPrepItem.setPivasFormulaMethodDrug(pivasFormulaMethodDrug);
			    }
		    }
		}
		
		pivasBatchPrep.setPivasFormulaMethod(pivasBatchPrepDetail.getPivasFormulaMethod());
		List<DmWarning> dmWarningList = new ArrayList<DmWarning>();
		if ( StringUtils.isNotBlank(pivasBatchPrep.getWarnCode1()) ) {
			DmWarning dmWarning1 = dmWarningCacher.getDmWarningByWarnCode(pivasBatchPrep.getWarnCode1());
			if ( dmWarning1 != null ) {
				dmWarningList.add(dmWarning1);
			}
			pivasBatchPrep.setDmWarning1(dmWarning1);
		}
		if ( StringUtils.isNotBlank(pivasBatchPrep.getWarnCode2()) ) {
			DmWarning dmWarning2 = dmWarningCacher.getDmWarningByWarnCode(pivasBatchPrep.getWarnCode2());
			if ( dmWarning2 != null ) {
				dmWarningList.add(dmWarning2);
			}
			pivasBatchPrep.setDmWarning2(dmWarning2);
		}
		if ( StringUtils.isNotBlank(pivasBatchPrep.getWarnCode3()) ) {
			DmWarning dmWarning3 = dmWarningCacher.getDmWarningByWarnCode(pivasBatchPrep.getWarnCode3());
			if ( dmWarning3 != null ) {
				dmWarningList.add(dmWarning3);
			}
			pivasBatchPrep.setDmWarning3(dmWarning3);
		}
		if ( StringUtils.isNotBlank(pivasBatchPrep.getWarnCode4()) ) {
			DmWarning dmWarning4 = dmWarningCacher.getDmWarningByWarnCode(pivasBatchPrep.getWarnCode4());
			if ( dmWarning4 != null ) {
				dmWarningList.add(dmWarning4);
			}
			pivasBatchPrep.setDmWarning4(dmWarning4);
		}

		Map<Long, List<PivasDiluentInfo>> solventListMap = new HashMap<Long, List<PivasDiluentInfo>>();

		List<PivasDiluentInfo> solventList = new ArrayList<PivasDiluentInfo>();
		for (PivasBatchPrepItem pivasBatchPrepItem: pivasBatchPrep.getPivasBatchPrepItemList()) {
			if ( StringUtils.isNotBlank(pivasBatchPrepItem.getSolventItemCode()) ) {
				DmDrug solventDmDrug = dmDrugCacher.getDmDrug(pivasBatchPrepItem.getSolventItemCode());
				if ( solventDmDrug != null ) {
					pivasBatchPrepItem.setSolventDmDrug(solventDmDrug);
				}
				pivasBatchPrepItem.setSolventCompany(pivasBatchPrepDetail.getSolventCompany());
				
				List<PivasDrugManuf> solventPivasDrugManufList = new ArrayList<PivasDrugManuf>();
				PivasDrugManuf solventPivasDrugManuf = new PivasDrugManuf();
				solventPivasDrugManuf.setManufacturerCode(pivasBatchPrepItem.getSolventManufCode());
				solventPivasDrugManuf.setCompany(pivasBatchPrepDetail.getSolventCompany());
				solventPivasDrugManuf.setBatchNum(pivasBatchPrepItem.getSolventBatchNum());
				solventPivasDrugManuf.setExpireDate(pivasBatchPrepItem.getSolventExpiryDate());
				solventPivasDrugManufList.add(solventPivasDrugManuf);
				PivasDiluentInfo pivasSolventInfo = new PivasDiluentInfo();
				pivasSolventInfo.setItemCode(pivasBatchPrepItem.getSolventItemCode());
				pivasSolventInfo.setDmDrug(solventDmDrug);
				pivasSolventInfo.setActionType(ActionType.Default);
				pivasSolventInfo.setPivasDrugManufList(solventPivasDrugManufList);
				solventListMap.put(pivasBatchPrepItem.getPivasFormulaMethodDrugId(), Arrays.asList(pivasSolventInfo));
				solventList.add(pivasSolventInfo);
			}
		}

		List<PivasDiluentInfo> diluentList = new ArrayList<PivasDiluentInfo>();

		if ( StringUtils.isNotBlank(pivasBatchPrep.getDiluentItemCode()) ) {
			DmDrug diluentDmDrug = dmDrugCacher.getDmDrug(pivasBatchPrep.getDiluentItemCode());
			if ( diluentDmDrug != null ) {
				pivasBatchPrep.setDiluentDmDrug(diluentDmDrug);
			}
			pivasBatchPrep.setDiluentCompany(pivasBatchPrepDetail.getDiluentCompany());
			
			List<PivasDrugManuf> diluentPivasDrugManufList = new ArrayList<PivasDrugManuf>();
			PivasDrugManuf diluentPivasDrugManuf = new PivasDrugManuf();
			diluentPivasDrugManuf.setManufacturerCode(pivasBatchPrep.getDiluentManufCode());
			diluentPivasDrugManuf.setCompany(pivasBatchPrepDetail.getDiluentCompany());
			diluentPivasDrugManuf.setBatchNum(pivasBatchPrep.getDiluentBatchNum());
			diluentPivasDrugManuf.setExpireDate(pivasBatchPrep.getDiluentExpiryDate());
			diluentPivasDrugManufList.add(diluentPivasDrugManuf);
			PivasDiluentInfo pivasDiluentInfo = new PivasDiluentInfo();
			pivasDiluentInfo.setItemCode(pivasBatchPrep.getDiluentItemCode());
			pivasDiluentInfo.setDmDrug(diluentDmDrug);
			pivasDiluentInfo.setActionType(ActionType.Default);
			pivasDiluentInfo.setPivasDrugManufList(diluentPivasDrugManufList);
			diluentList.add(pivasDiluentInfo);
		}
		
		List<PivasContainer> pivasContainerList = new ArrayList<PivasContainer>();
		pivasContainerList.add(pivasBatchPrepDetail.getPivasContainer());
		pivasBatchPrep.setPivasContainer(pivasBatchPrepDetail.getPivasContainer());
		
		AddPivasBatchPrepDetail addPivasBatchPrepDetail = new AddPivasBatchPrepDetail();
		addPivasBatchPrepDetail.setDmWarningList(dmWarningList);
		addPivasBatchPrepDetail.setPivasBatchPrep(pivasBatchPrep);
		addPivasBatchPrepDetail.setPivasContainerList(pivasContainerList);
		addPivasBatchPrepDetail.setPivasDiluentList(diluentList);
		addPivasBatchPrepDetail.setPivasSolventList(solventList);
		addPivasBatchPrepDetail.setPivasSolventListMapByPivasFormulaMethodDrugId(solventListMap);
		return addPivasBatchPrepDetail;
	}
	
	public List<PivasBatchPrep> deletePivasBatchPrep(String lotNo, Date manufFromDate, Date manufToDate, Integer drugKey){
		PivasBatchPrep pivasBatchPrep = em.find(PivasBatchPrep.class, lotNo);
		if ( pivasBatchPrep == null || pivasBatchPrep.getStatus() == PivasBatchPrepStatus.Deleted) {
			deleteSuccess = false;
			return null;
		}
		pivasBatchPrep.setStatus(PivasBatchPrepStatus.Deleted);
		em.merge(pivasBatchPrep);
		em.flush();
		deleteSuccess = true;
		return retrievePivasBatchPrepList(manufFromDate, manufToDate, drugKey);
	}
	
	public static class PivasFormulaComparator implements Comparator<PivasFormula>, Serializable {
		
		private static final long serialVersionUID = 1L;
	
		@Override
		public int compare(PivasFormula pivasFormula1, PivasFormula pivasFormula2) {
			int compareResult = 0;
			
			compareResult = new CompareToBuilder()
							.append( pivasFormula1.getFirstPivasFormulaDrug().getDisplayname(), pivasFormula2.getFirstPivasFormulaDrug().getDisplayname() )
							.append( pivasFormula1.getFirstPivasFormulaDrug().getSaltProperty(), pivasFormula2.getFirstPivasFormulaDrug().getSaltProperty() )
							.append( pivasFormula1.getFirstPivasFormulaDrug().getFormCode(), pivasFormula2.getFirstPivasFormulaDrug().getFormCode() )
							.toComparison();
			
			if ( compareResult == 0 ) {
				String conc1 = "";
				String conc2 = "";
				
				if(pivasFormula1.getConcn() != null){
					conc1 = pivasFormula1.getConcn().toString();
					if ( !"ML".equals(pivasFormula1.getFirstPivasFormulaDrug().getPivasDosageUnit()) ) {
						conc1 += pivasFormula1.getFirstPivasFormulaDrug().getPivasDosageUnit() + " / ML";
					}
				}
				if(pivasFormula2.getConcn() != null){
					conc2 = pivasFormula2.getConcn().toString();
					if ( !"ML".equals(pivasFormula2.getFirstPivasFormulaDrug().getPivasDosageUnit()) ) {
						conc2 += pivasFormula2.getFirstPivasFormulaDrug().getPivasDosageUnit() + " / ML";
					}
				}

				compareResult = ObjectUtils.compare(conc1, conc2);	
			}
			
			if ( compareResult == 0 ) {
				String diluentStr1 = "";
				String diluentStr2 = "";
				
				if ( "iv bolus".equals(pivasFormula1.getDmSite().getIpmoeDesc().toLowerCase()) ) {
					diluentStr1 = "Not applicable";
				} else if ( pivasFormula1.getDmDiluent() != null ){
					diluentStr1 = pivasFormula1.getDmDiluent().getPivasDiluentDesc();
				}
				
				if ( "iv bolus".equals(pivasFormula2.getDmSite().getIpmoeDesc().toLowerCase()) ) {
					diluentStr2 = "Not applicable";
				} else if ( pivasFormula2.getDmDiluent() != null ){
					diluentStr2 = pivasFormula2.getDmDiluent().getPivasDiluentDesc();
				}
				compareResult = ObjectUtils.compare(diluentStr1, diluentStr2);
			}
			
			if ( compareResult == 0 ) {
				compareResult = ObjectUtils.compare(pivasFormula1.getDmSite().getIpmoeDesc(), pivasFormula2.getDmSite().getIpmoeDesc());
			}
			
			return compareResult;
		}
	}
	
	public static class PivasFormulaDrugComparator implements Comparator<PivasFormulaDrug>, Serializable {
	
		private static final long serialVersionUID = 1L;
	
		@Override
		public int compare(PivasFormulaDrug pivasFormulaDrug1, PivasFormulaDrug pivasFormulaDrug2) {
			int compareResult = 0;
			
			PivasFormula pivasFormula1 = pivasFormulaDrug1.getPivasFormula();
			PivasFormula pivasFormula2 = pivasFormulaDrug2.getPivasFormula();
			
			compareResult = new CompareToBuilder()
							.append( pivasFormulaDrug1.getDisplayname(), pivasFormulaDrug2.getDisplayname() )
							.append( pivasFormulaDrug1.getSaltProperty(), pivasFormulaDrug2.getSaltProperty() )
							.append( pivasFormulaDrug1.getFormCode(), pivasFormulaDrug2.getFormCode() )
							.toComparison();
			
			if ( compareResult == 0 ) {
				String conc1 = "";
				String conc2 = "";
				
				if(pivasFormula1.getConcn() != null){
					conc1 = pivasFormula1.getConcn().toString();
					if ( !"ML".equals(pivasFormulaDrug1.getPivasDosageUnit()) ) {
						conc1 += pivasFormulaDrug1.getPivasDosageUnit() + " / ML";
					}
				}
				if(pivasFormula2.getConcn() != null){
					conc2 = pivasFormula2.getConcn().toString();
					if ( !"ML".equals(pivasFormulaDrug2.getPivasDosageUnit()) ) {
						conc2 += pivasFormulaDrug2.getPivasDosageUnit() + " / ML";
					}
				}

				compareResult = ObjectUtils.compare(conc1, conc2);	
			}
			
			if ( compareResult == 0 ) {
				String diluentStr1 = "";
				String diluentStr2 = "";
				
				if ( "iv bolus".equals(pivasFormula1.getDmSite().getIpmoeDesc().toLowerCase()) ) {
					diluentStr1 = "Not applicable";
				} else if ( pivasFormula1.getDmDiluent() != null ){
					diluentStr1 = pivasFormula1.getDmDiluent().getPivasDiluentDesc();
				}
				
				if ( "iv bolus".equals(pivasFormula2.getDmSite().getIpmoeDesc().toLowerCase()) ) {
					diluentStr2 = "Not applicable";
				} else if ( pivasFormula2.getDmDiluent() != null ){
					diluentStr2 = pivasFormula2.getDmDiluent().getPivasDiluentDesc();
				}
				compareResult = ObjectUtils.compare(diluentStr1, diluentStr2);
			}
			
			if ( compareResult == 0 ) {
				compareResult = ObjectUtils.compare(pivasFormula1.getDmSite().getIpmoeDesc(), pivasFormula2.getDmSite().getIpmoeDesc());
			}
			
			return compareResult;
		}
	}
	 
	 private static class BatchPrepMethodInfoComparator implements Comparator<BatchPrepMethodInfo>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(BatchPrepMethodInfo batchPrepMethodInfo1, BatchPrepMethodInfo batchPrepMethodInfo2) {
			int compareResult = 0;
			
			compareResult = Boolean.valueOf(batchPrepMethodInfo1.isCoreFlag()).compareTo(Boolean.valueOf(batchPrepMethodInfo2.isCoreFlag())) * -1;
			
			if ( compareResult == 0 ) {
				compareResult = Boolean.valueOf(batchPrepMethodInfo1.isHqSuspend()).compareTo(Boolean.valueOf(batchPrepMethodInfo2.isHqSuspend()));
				
				if ( compareResult == 0 ) {
					compareResult = Boolean.valueOf(batchPrepMethodInfo1.isSuspend()).compareTo(Boolean.valueOf(batchPrepMethodInfo2.isSuspend()));
					
					if ( compareResult == 0 ) {
						compareResult = new CompareToBuilder()
											.append( batchPrepMethodInfo1.getItemCode(), batchPrepMethodInfo2.getItemCode() )
											.append( batchPrepMethodInfo1.getCompanyCode(), batchPrepMethodInfo2.getCompanyCode() )
											.append( batchPrepMethodInfo1.getAfterVolume(), batchPrepMethodInfo2.getAfterVolume() )
											.toComparison();
					}
				}
			}
			
			return compareResult;
		}
    }
	 
	private static class PivasFormulaMethodComparator implements Comparator<PivasFormulaMethod> {

		@Override
		public int compare(PivasFormulaMethod o1, PivasFormulaMethod o2) {
			return MedProfileUtils.compare(o1.getCoreFlag(), o2.getCoreFlag()) * -1;
		}
		
	}

	public boolean isDeleteSuccess() {
		return deleteSuccess;
	}

	@Remove
	public void destroy() {
	}

}
