package hk.org.ha.model.pms.vo.rx;

import java.io.Serializable;

import hk.org.ha.model.pms.udt.vetting.NameType;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class NameTypeAdapter extends XmlAdapter<String, NameType> implements Serializable {

	private static final long serialVersionUID = 1L;

	public String marshal( NameType v ) {		
		return (v == null) ? null : v.getDataValue();
	}
	
	public NameType unmarshal( String v ) {		
		return NameType.dataValueOf(v);
	}

}