package hk.org.ha.model.pms.biz.order;

import java.util.Date;

import hk.org.ha.model.pms.persistence.disp.Ticket;
import hk.org.ha.model.pms.udt.ticketgen.TicketType;

import javax.ejb.Local;

@Local
public interface NumberGeneratorLocal {
	
	String retrieveCapdVoucherNum();
	
	String retrievePivasBatchPrepNum();

	Integer retrieveSfiInvoiceNumber();

	Integer retrieveStandardInvoiceNumber();
	
	Ticket retrieveTicketByTickDateType(Date tickDate, TicketType ticketType, Integer customTicketNum);
	
	boolean isFastQueueTicketNum(String ticketNum);
}
