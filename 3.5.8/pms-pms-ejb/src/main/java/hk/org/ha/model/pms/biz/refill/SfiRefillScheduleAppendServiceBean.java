package hk.org.ha.model.pms.biz.refill;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.drug.ItemConvertManagerLocal;
import hk.org.ha.model.pms.biz.order.MedOrderManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.biz.vetting.MedOrderItemComparator;
import hk.org.ha.model.pms.biz.vetting.OrderViewManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
import hk.org.ha.model.pms.persistence.disp.RefillScheduleItem;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.refill.DocType;
import hk.org.ha.model.pms.udt.refill.RefillScheduleRefillStatus;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sfiRefillScheduleAppendService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SfiRefillScheduleAppendServiceBean implements SfiRefillScheduleAppendServiceLocal {
		
	@PersistenceContext
	private EntityManager em;
	
	@In
	private MedOrderManagerLocal medOrderManager;
	
	@In
	private OrderViewManagerLocal orderViewManager;
	
	@In
	private SearchTextValidatorLocal searchTextValidator;
	
	@In
	private ItemConvertManagerLocal itemConvertManager;
	
	@In
	private SystemMessageManagerLocal systemMessageManager;
	
	@In
	private PharmOrder pharmOrder;
		
	@In(required=false)
	private DispOrder dispOrder;
		
	private String errMsg;
	
	private Boolean useMoItemNumFlag;

	public Integer appendSfiRefillSchedule(OrderViewInfo inOrderViewInfo){
		orderViewManager.updateOrderFromOrderView(inOrderViewInfo);
		errMsg = StringUtils.EMPTY;
		
		if( !validateInputRefillCouponNum(inOrderViewInfo.getSfiRefillCouponNum(), inOrderViewInfo.getMedOrderNumForCoupon()) ){			
			inOrderViewInfo.setSfiRefillCouponNumErrorString(systemMessageManager.retrieveMessageDesc("0332"));
			return null;
		}
		
		return this.retrieveSfiRefillScheduleItemByRefillCouponNum(inOrderViewInfo);		
	}
	
	private boolean validateInputRefillCouponNum(String refillCouponNum, String refillMedOrderNum){
		SearchTextType searchTextType = searchTextValidator.checkSearchInputType(refillCouponNum);
		
		if ( !SearchTextType.SfiRefill.equals(searchTextType) ){
			return false;
		}
		
		MedOrder medOrder = pharmOrder.getMedOrder();
				
		if( !StringUtils.equals( medOrder.getPatHospCode() , StringUtils.trim(refillCouponNum.substring(0,3)) ) ){
			return false;
		}
		
		if( !StringUtils.equals( refillMedOrderNum.substring(3), refillCouponNum.substring(6,15)) ){
			return false;
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private Integer retrieveSfiRefillScheduleItemByRefillCouponNum(OrderViewInfo inOrderViewInfo){
		useMoItemNumFlag = Boolean.TRUE;
		Integer selectedIndex = Integer.valueOf(0);
		boolean itemExistFlag = false;
		inOrderViewInfo.getDiscontinueMessageTlf().clear();
		List<Integer> currSfiGroupNumList = new ArrayList<Integer>();

		MedOrder orgMedOrder = em.find(MedOrder.class, pharmOrder.getMedOrder().getId());
		                                             
		if (!orgMedOrder.getVersion().equals(pharmOrder.getMedOrder().getVersion())) {
			errMsg = "0187";
			return selectedIndex;
		}
		
		pharmOrder.reconnectRefillScheduleList();
		for ( RefillSchedule currSfiRs : pharmOrder.getRefillScheduleList(DocType.Sfi) ){			
			currSfiGroupNumList.add(currSfiRs.getGroupNum());			
		}

		List<RefillSchedule> refillScheduleList = new ArrayList<RefillSchedule>();

		String refillCouponNum = inOrderViewInfo.getSfiRefillCouponNum();
		if( inOrderViewInfo.getSfiRefillCouponNum() != null && inOrderViewInfo.getSfiRefillCouponNum().length() > 17 ){
			refillCouponNum = inOrderViewInfo.getSfiRefillCouponNum().substring(0, 17);
		}
		String trimRefillCouponNum = refillCouponNum.substring(0, 15);
		// for CMS SFI refill coupon, the 2 digit is MO original item num
		// for PMS SFI refill coupon, the 2 digit is MO item num after PMS-3.2.0, and MO original item num before PMS-3.2.0 
		Integer groupNum = Integer.valueOf(refillCouponNum.substring(15, 17));
		inOrderViewInfo.setSfiRefillCouponNum(StringUtils.EMPTY);
		
		RefillSchedule appendRs = null;
		
		refillScheduleList = (List<RefillSchedule>) em.createQuery(
				"select o from RefillSchedule o" + // 20150206 index check : RefillSchedule.trimRefillCouponNum : I_REFILL_SCHEDULE_02
				" where o.trimRefillCouponNum = :trimRefillCouponNum" +
				" and o.groupNum = :groupNum" +
				" and o.refillStatus in :refillStatusList" +
				" and o.docType = :docType" +
				" and o.pharmOrder.id = :pharmOrderId" +
				" order by o.refillCouponNum ")
				.setParameter("trimRefillCouponNum", trimRefillCouponNum )
				.setParameter("groupNum", groupNum )
				.setParameter("refillStatusList", RefillScheduleRefillStatus.Current_Next)
				.setParameter("docType", DocType.Sfi)
				.setParameter("pharmOrderId", pharmOrder.getId())
				.getResultList();

 		
		if (refillScheduleList.size() == 0) {
			refillScheduleList = (List<RefillSchedule>) em.createQuery(
					"select o from RefillSchedule o" + // 20150206 index check : RefillSchedule.trimRefillCouponNum : I_REFILL_SCHEDULE_02
					" where o.trimRefillCouponNum = :trimRefillCouponNum" +
					" and o.moOrgItemNum = :moOrgItemNum" +
					" and o.refillStatus in :refillStatusList" +
					" and o.docType = :docType" +
					" and o.pharmOrder.id = :pharmOrderId" +
					" order by o.refillCouponNum ")
					.setParameter("trimRefillCouponNum", trimRefillCouponNum )
					.setParameter("moOrgItemNum", groupNum )
					.setParameter("refillStatusList", RefillScheduleRefillStatus.Current_Next)
					.setParameter("docType", DocType.Sfi)
					.setParameter("pharmOrderId", pharmOrder.getId())
					.getResultList();
		}
		
		if( refillScheduleList.size() > 0){
			boolean isCurrRsSuspend = false;
			for( RefillSchedule refillSchedule : refillScheduleList ){
				if( currSfiGroupNumList.contains(refillSchedule.getGroupNum()) ){
					itemExistFlag = true;
					if( refillSchedule.getUseMoItemNumFlag() ){
						selectedIndex = refillScheduleList.get(0).getMedOrderItemList().get(0).getItemNum();
					}else{
						selectedIndex = refillScheduleList.get(0).getMedOrderItemList().get(0).getOrgItemNum();
					}
					refillScheduleList.clear();
					break;
				}else if( RefillScheduleRefillStatus.Current == refillSchedule.getRefillStatus() ) {
					if( refillSchedule.getDispOrder() != null ){
						if( dispOrder != null && refillSchedule.getDispOrder().getId().equals(dispOrder.getId()) ){
							appendRs = refillSchedule;
							break;
						}
						if( refillSchedule.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended || DispOrderStatus.Issued != refillSchedule.getDispOrder().getStatus() ){
							isCurrRsSuspend = true;
						}
					}
				}else if( RefillScheduleRefillStatus.Next == refillSchedule.getRefillStatus() ){
					if( !isCurrRsSuspend ){
						appendRs = refillSchedule;
					}
				}
			}
		}
		
		if( appendRs != null ){
			
			MedOrder medOrder = pharmOrder.getMedOrder();
			
			if (appendRs.isDiscontinue()) {
				List<String> messageTlfList = itemConvertManager.formatDiscontinueMessage(appendRs.getDiscontinueItem());
				for (String messageTlf : messageTlfList) {
					inOrderViewInfo.getDiscontinueMessageTlf().put(appendRs.getDiscontinueItem().get(messageTlfList.indexOf(messageTlf)).getItemNum(), messageTlf);
				}
			}
			else {
			
				appendRs.loadChild();
				
				em.clear();
				
				appendRs.setDispOrder(null);
				appendRs.setTicket(null);
				pharmOrder.addRefillSchedule(appendRs);
				
				List<Integer> moiOrgItemNumList = new ArrayList<Integer>();
				
				useMoItemNumFlag = appendRs.getUseMoItemNumFlag();
				
				for( RefillScheduleItem refillScheduleItem : appendRs.getRefillScheduleItemList() ){
					PharmOrderItem pharmOrderItem = refillScheduleItem.getPharmOrderItem();
					pharmOrderItem.getMedOrderItem().loadDmInfo();
					pharmOrderItem.loadDmInfo();
					pharmOrderItem.setSfiQty(refillScheduleItem.getRefillQty());
					
					//for SfiSubsidized item calculation
					pharmOrderItem.setOrgIssueQty(pharmOrderItem.getIssueQty());
					
					pharmOrderItem.setIssueQty(BigDecimal.valueOf(refillScheduleItem.getCalQty()));
					pharmOrderItem.clearDispOrderItemList();
					
					if( !StringUtils.isBlank(refillScheduleItem.getHandleExemptCode()) ){
						pharmOrderItem.getMedOrderItem().setShowExemptCodeFlag(true);
						pharmOrderItem.setHandleExemptCode(refillScheduleItem.getHandleExemptCode());
					}else{
						pharmOrderItem.getMedOrderItem().setShowExemptCodeFlag(false);
					}
					
					pharmOrder.addPharmOrderItem(pharmOrderItem);
					if( !moiOrgItemNumList.contains(pharmOrderItem.getMedOrderItem().getOrgItemNum()) ){
						medOrder.addMedOrderItem(pharmOrderItem.getMedOrderItem());
						moiOrgItemNumList.add(pharmOrderItem.getMedOrderItem().getOrgItemNum());
						
						if( useMoItemNumFlag ){
							selectedIndex = pharmOrderItem.getMedOrderItem().getItemNum();
						}else{
							selectedIndex = pharmOrderItem.getMedOrderItem().getOrgItemNum();
						}
					}
				}
				pharmOrder.clearDispOrderList();
			}
		}
		
		if( appendRs == null ){
			if (itemExistFlag) {
				errMsg = "0593";
			}
			else {
				errMsg = "0205";
			}
		}

		Collections.sort(pharmOrder.getMedOrder().getMedOrderItemList(), new MedOrderItemComparator());

		pharmOrder.disconnectRefillScheduleList();
		orderViewManager.updateOrderFromOrderView(inOrderViewInfo);
		medOrderManager.setPharmOrder(pharmOrder);
		return selectedIndex;
	}
	
	public String getErrMsg() {
		return errMsg;
	}
	
	public Boolean getUseMoItemNumFlag(){
		return useMoItemNumFlag;
	}
	
	@Remove
	public void destroy() {

	}
}
