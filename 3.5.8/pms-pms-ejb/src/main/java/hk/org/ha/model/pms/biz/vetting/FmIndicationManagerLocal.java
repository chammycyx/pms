package hk.org.ha.model.pms.biz.vetting;

import java.util.List;

import hk.org.ha.model.pms.asa.exception.moe.MoeException;
import hk.org.ha.model.pms.dms.vo.PmsFmStatusSearchCriteria;
import hk.org.ha.model.pms.persistence.FmEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderFm;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemFm;
import hk.org.ha.model.pms.vo.rx.RxDrug;
import hk.org.ha.model.pms.vo.vetting.FmIndication;

import javax.ejb.Local;

@Local
public interface FmIndicationManagerLocal {
	
	MedOrderFm retrieveMedOrderFmByRxDrug(RxDrug rxDrug) throws MoeException;

	MedProfileMoItemFm retrieveMedProfileMoItemFmByRxDrug(MedProfile medProfile, RxDrug rxDrug) throws MoeException;
	
	MedOrderFm retrieveLocalMedOrderFmByRxDrug(RxDrug rxDrug, List<MedOrderFm> medOrderFmList);

	List<FmIndication> retrieveLocalFmIndicationListByMedOrderItem(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList);
		
	List<FmIndication> retrieveLocalFmIndicationListForDrugOnHand(MedOrderItem medOrderItem, List<MedOrderFm> medOrderFmList);
	
	List<FmIndication> retrieveLocalFmIndicationListByMedProfileMoItem(MedProfileMoItem medProfileMoItem, List<MedProfileMoItemFm> medProfileMoItemFmList);
	
	boolean validateFmCorpIndByDms(RxDrug rxDrug, FmEntity fmEntity);
	
	Boolean checkSpecialDrug(PmsFmStatusSearchCriteria criteria);

	 String retrievePrescibingDoctorNameFromMoeUserCode(String patHospCode, String userCode);
	
	void destroy();
}
