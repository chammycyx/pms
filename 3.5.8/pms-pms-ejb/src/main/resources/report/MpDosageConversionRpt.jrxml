<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DosageConversionRpt" language="groovy" pageWidth="595" pageHeight="842" columnWidth="565" leftMargin="15" rightMargin="15" topMargin="15" bottomMargin="15">
	<property name="ireport.zoom" value="1.948717100000002"/>
	<property name="ireport.x" value="80"/>
	<property name="ireport.y" value="0"/>
	<style name="Row" mode="Transparent" fontName="Arial" fontSize="8" isBold="false">
		<conditionalStyle>
			<conditionExpression><![CDATA[$V{REPORT_COUNT}%2 == 0]]></conditionExpression>
			<style mode="Opaque" backcolor="#DEDEDE"/>
		</conditionalStyle>
	</style>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<field name="drug" class="java.lang.String"/>
	<field name="fullDrugDesc" class="java.lang.String"/>
	<field name="moDosageFrom" class="java.lang.Double"/>
	<field name="dispDosage" class="java.lang.Double"/>
	<field name="dispDosageUnit" class="java.lang.String"/>
	<field name="itemCode" class="java.lang.String"/>
	<field name="suspend" class="java.lang.String"/>
	<field name="moDosageTo" class="java.lang.Double"/>
	<field name="fixed" class="java.lang.String"/>
	<field name="moDosageUnit" class="java.lang.String"/>
	<field name="dispDosageUnitMultiply" class="java.lang.String"/>
	<field name="moDosageUnitMultiply" class="java.lang.String"/>
	<variable name="dispDosage" class="java.lang.String">
		<variableExpression><![CDATA[$F{dispDosage}!=null?
new DecimalFormat("0.#####").format($F{dispDosage}) + " " + $F{dispDosageUnitMultiply} + $F{dispDosageUnit}
:
""
]]></variableExpression>
	</variable>
	<variable name="moDosageFrom" class="java.lang.String">
		<variableExpression><![CDATA[new DecimalFormat("0.#####").format($F{moDosageFrom}) + " " + $F{moDosageUnitMultiply} + $F{moDosageUnit}]]></variableExpression>
	</variable>
	<variable name="moDosageTo" class="java.lang.String">
		<variableExpression><![CDATA[$F{moDosageTo}!=null?
new DecimalFormat("0.#####").format($F{moDosageTo}) + " " + $F{moDosageUnitMultiply} + $F{moDosageUnit}
:
""]]></variableExpression>
	</variable>
	<group name="dosage">
		<groupFooter>
			<band height="12">
				<staticText>
					<reportElement x="0" y="0" width="565" height="12"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[End]]></text>
				</staticText>
				<line>
					<reportElement x="0" y="0" width="565" height="1"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="385" height="18"/>
				<textElement>
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Dosage Conversion Maintenance Report (" + $P{hospCode} + " - " + $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="33">
			<staticText>
				<reportElement x="0" y="0" width="40" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Drug]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="22" width="50" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Item Code]]></text>
			</staticText>
			<staticText>
				<reportElement x="440" y="0" width="70" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dosage]]></text>
			</staticText>
			<staticText>
				<reportElement x="50" y="22" width="320" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Item Description]]></text>
			</staticText>
			<staticText>
				<reportElement x="415" y="22" width="40" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Variable]]></text>
			</staticText>
			<staticText>
				<reportElement x="455" y="22" width="95" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dispenseing Dosage]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="32" width="565" height="1"/>
			</line>
			<staticText>
				<reportElement x="370" y="22" width="40" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Suspend]]></text>
			</staticText>
			<staticText>
				<reportElement x="320" y="0" width="70" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dosage]]></text>
			</staticText>
			<staticText>
				<reportElement x="320" y="11" width="70" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[From]]></text>
			</staticText>
			<staticText>
				<reportElement x="440" y="11" width="70" height="11"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[To]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="22" splitType="Stretch">
			<rectangle>
				<reportElement style="Row" x="0" y="0" width="565" height="22"/>
				<graphicElement>
					<pen lineWidth="0.0"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="300" height="11"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drug}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="11" width="40" height="11"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="40" y="11" width="300" height="11"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{fullDrugDesc}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="370" y="11" width="40" height="11"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{suspend}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="455" y="11" width="115" height="11"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{dispDosage}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="415" y="11" width="40" height="11"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{fixed}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="320" y="0" width="120" height="11"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{moDosageFrom}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="440" y="0" width="120" height="11"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{moDosageTo}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="16">
			<textField isBlankWhenNull="true">
				<reportElement x="417" y="0" width="132" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<textField pattern="&apos;Printed on&apos; dd-MMM-yyyy &apos;at&apos; HH:mm ">
				<reportElement x="0" y="0" width="229" height="15"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="552" y="0" width="24" height="15"/>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
