<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="PivasBatchDispXls" language="groovy" pageWidth="5600" pageHeight="595" orientation="Landscape" columnWidth="5600" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="3.1384283767210093"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String">
		<defaultValueExpression><![CDATA["PivasConsumptionRpt/"]]></defaultValueExpression>
	</parameter>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<parameter name="supplyDateFrom" class="java.util.Date"/>
	<parameter name="supplyDateTo" class="java.util.Date"/>
	<parameter name="pivasRptItemCatFilterOption.displayValue" class="java.lang.String"/>
	<parameter name="itemCode" class="java.lang.String"/>
	<parameter name="drugKeyFlag" class="java.lang.Boolean"/>
	<parameter name="manufCode" class="java.lang.String"/>
	<parameter name="batchNum" class="java.lang.String"/>
	<field name="supplyDate" class="java.util.Date"/>
	<field name="lotNum" class="java.lang.String"/>
	<field name="pivasFormulaType.dataValue" class="java.lang.String"/>
	<field name="labelDesc" class="java.lang.String"/>
	<field name="siteDesc" class="java.lang.String"/>
	<field name="dosageQty" class="java.lang.String"/>
	<field name="modu" class="java.lang.String"/>
	<field name="concn" class="java.lang.String"/>
	<field name="drugVolume" class="java.lang.String"/>
	<field name="finalVolume" class="java.lang.String"/>
	<field name="prepQty" class="java.lang.String"/>
	<field name="containerName" class="java.lang.String"/>
	<field name="prepExpiryDate" class="java.util.Date"/>
	<field name="formulaPrintName" class="java.lang.String"/>
	<field name="numOfLabel" class="java.lang.String"/>
	<field name="drugItemCode" class="java.lang.String"/>
	<field name="drugItemDesc" class="java.lang.String"/>
	<field name="drugManufCode" class="java.lang.String"/>
	<field name="drugBatchNum" class="java.lang.String"/>
	<field name="drugExpiryDate" class="java.util.Date"/>
	<field name="solventItemCode" class="java.lang.String"/>
	<field name="solventItemDesc" class="java.lang.String"/>
	<field name="solventManufCode" class="java.lang.String"/>
	<field name="solventBatchNum" class="java.lang.String"/>
	<field name="solventExpiryDate" class="java.util.Date"/>
	<field name="diluentItemCode" class="java.lang.String"/>
	<field name="diluentItemDesc" class="java.lang.String"/>
	<field name="diluentManufCode" class="java.lang.String"/>
	<field name="diluentBatchNum" class="java.lang.String"/>
	<field name="diluentExpiryDate" class="java.util.Date"/>
	<field name="diluentDesc" class="java.lang.String"/>
	<field name="printDate" class="java.util.Date"/>
	<field name="pivasBatchPrepStatus.displayValue" class="java.lang.String"/>
	<field name="vendSupplySolvFlag" class="java.lang.Boolean"/>
	<field name="vendSupplySolvDesc" class="java.lang.String"/>
	<title>
		<band height="60">
			<textField>
				<reportElement x="0" y="0" width="5600" height="20"/>
				<textElement verticalAlignment="Middle" markup="none">
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["PIVAS Batch Dispensing Report" + " (" + $P{hospCode} + " - " + $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="20" width="5600" height="20"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Supply Date: " +
(
    $P{supplyDateTo} != null ?
    new SimpleDateFormat("dd-MMM-yyyy").format($P{supplyDateFrom}) + " to " + new SimpleDateFormat("dd-MMM-yyyy").format($P{supplyDateTo}):
    new SimpleDateFormat("dd-MMM-yyyy HH:mm").format($P{supplyDateFrom})
) + "   ,   " +
"Item Category: " + $P{pivasRptItemCatFilterOption.displayValue} +
($P{itemCode} != null ? "   ,   "  + "Item Code: " + $P{itemCode}: "") +
( ($P{drugKeyFlag} != null && $P{drugKeyFlag}) ? "   ,   "  + "Drug Key: Y" : "") +
($P{manufCode} != null ? "   ,   "  + "Manufacturer: " + $P{manufCode} : "") +
($P{batchNum} != null ? "   ,   "  + "Batch No.: " + $P{batchNum} : "")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="0" y="40" width="100" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Supply Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="100" y="40" width="100" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Lot No.]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="200" y="40" width="75" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Formula Type]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="275" y="40" width="600" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Preparation Description]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="875" y="40" width="135" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Route / Site]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1010" y="40" width="65" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Dosage]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1075" y="40" width="90" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[MODU]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1165" y="40" width="120" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Concentration]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1285" y="40" width="95" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Drug Volume (mL)]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1380" y="40" width="95" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Final Volume (mL)]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1475" y="40" width="60" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Issue Qty]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1535" y="40" width="120" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Container]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1655" y="40" width="120" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Preparation Expiry Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="1775" y="40" width="600" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Print Name]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="2375" y="40" width="80" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[No. of Label]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="2455" y="40" width="95" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Drug Item Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="2550" y="40" width="475" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Drug Item Description]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="3025" y="40" width="95" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Drug Manuf. Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="3120" y="40" width="175" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Drug Batch No.]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="3295" y="40" width="80" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Drug Exp. Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="3375" y="40" width="95" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Solvent Item Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="3470" y="40" width="475" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Solvent Item Description]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="3945" y="40" width="105" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Solvent Manuf. Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="4050" y="40" width="175" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Solvent Batch No.]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="4225" y="40" width="85" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Solvent Exp. Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="4310" y="40" width="95" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Diluent Item Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="4405" y="40" width="475" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Diluent Item Description]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="4880" y="40" width="105" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Diluent Manuf. Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="4985" y="40" width="175" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Diluent Batch No.]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="5160" y="40" width="85" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Diluent Exp. Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="5245" y="40" width="150" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Diluent]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="5395" y="40" width="100" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Print Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="5495" y="40" width="105" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="20">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[$F{supplyDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy HH:mm").format($F{supplyDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="100" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[$F{lotNum} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{lotNum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="200" y="0" width="75" height="20">
					<printWhenExpression><![CDATA[$F{pivasFormulaType.dataValue} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{pivasFormulaType.dataValue}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="275" y="0" width="600" height="20">
					<printWhenExpression><![CDATA[$F{labelDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="875" y="0" width="135" height="20">
					<printWhenExpression><![CDATA[$F{siteDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{siteDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1010" y="0" width="65" height="20">
					<printWhenExpression><![CDATA[$F{dosageQty} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{dosageQty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1075" y="0" width="90" height="20">
					<printWhenExpression><![CDATA[$F{modu} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{modu}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1165" y="0" width="120" height="20">
					<printWhenExpression><![CDATA[!("ML".equals($F{modu}))]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{concn} != null && $F{modu} != null ?
$F{concn} + $F{modu} + "/ML" :
""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1285" y="0" width="95" height="20">
					<printWhenExpression><![CDATA[$F{drugVolume} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugVolume}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1380" y="0" width="95" height="20">
					<printWhenExpression><![CDATA[$F{finalVolume} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{finalVolume}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1475" y="0" width="60" height="20">
					<printWhenExpression><![CDATA[$F{prepQty} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{prepQty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1535" y="0" width="120" height="20">
					<printWhenExpression><![CDATA[$F{containerName} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{containerName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1655" y="0" width="120" height="20">
					<printWhenExpression><![CDATA[$F{prepExpiryDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy HH:mm").format($F{prepExpiryDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="1775" y="0" width="600" height="20">
					<printWhenExpression><![CDATA[$F{formulaPrintName} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{formulaPrintName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="2375" y="0" width="80" height="20">
					<printWhenExpression><![CDATA[$F{numOfLabel} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{numOfLabel}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="2455" y="0" width="95" height="20">
					<printWhenExpression><![CDATA[$F{drugItemCode} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugItemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="2550" y="0" width="475" height="20">
					<printWhenExpression><![CDATA[$F{drugItemDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugItemDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="3025" y="0" width="95" height="20">
					<printWhenExpression><![CDATA[$F{drugManufCode} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugManufCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="3120" y="0" width="175" height="20">
					<printWhenExpression><![CDATA[$F{drugBatchNum} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugBatchNum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="3295" y="0" width="80" height="20">
					<printWhenExpression><![CDATA[$F{drugExpiryDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy").format($F{drugExpiryDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="3375" y="0" width="95" height="20">
					<printWhenExpression><![CDATA[$F{solventItemCode} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{solventItemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="3470" y="0" width="475" height="20">
					<printWhenExpression><![CDATA[$F{solventItemDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{solventItemDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="3945" y="0" width="105" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{vendSupplySolvFlag} ?
($F{vendSupplySolvDesc} != null ? $F{vendSupplySolvDesc} : "") :
($F{solventManufCode} != null ? $F{solventManufCode} : "")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="4050" y="0" width="175" height="20">
					<printWhenExpression><![CDATA[$F{solventBatchNum} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{solventBatchNum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="4225" y="0" width="85" height="20">
					<printWhenExpression><![CDATA[$F{solventExpiryDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy").format($F{solventExpiryDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="4310" y="0" width="95" height="20">
					<printWhenExpression><![CDATA[$F{diluentItemCode} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{diluentItemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="4405" y="0" width="475" height="20">
					<printWhenExpression><![CDATA[$F{diluentItemDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{diluentItemDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="4880" y="0" width="105" height="20">
					<printWhenExpression><![CDATA[$F{diluentManufCode} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{diluentManufCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="4985" y="0" width="175" height="20">
					<printWhenExpression><![CDATA[$F{diluentBatchNum} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{diluentBatchNum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="5160" y="0" width="85" height="20">
					<printWhenExpression><![CDATA[$F{diluentExpiryDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy").format($F{diluentExpiryDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="5245" y="0" width="150" height="20">
					<printWhenExpression><![CDATA[$F{diluentDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{diluentDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="5395" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[$F{printDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy HH:mm").format($F{printDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="5495" y="0" width="105" height="20">
					<printWhenExpression><![CDATA[$F{pivasBatchPrepStatus.displayValue} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{pivasBatchPrepStatus.displayValue}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
