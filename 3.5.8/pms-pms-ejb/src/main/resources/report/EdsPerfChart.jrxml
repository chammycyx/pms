<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="EdsWaitTimeRpt" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="812" leftMargin="15" rightMargin="15" topMargin="15" bottomMargin="15">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="328"/>
	<style name="Detail" isBold="false">
		<box topPadding="1" leftPadding="0" bottomPadding="1" rightPadding="5"/>
	</style>
	<style name="Header">
		<box leftPadding="0"/>
	</style>
	<style name="Row">
		<box topPadding="1" bottomPadding="1" rightPadding="5"/>
		<conditionalStyle>
			<conditionExpression><![CDATA[$V{REPORT_COUNT}%2 == 0]]></conditionExpression>
			<style mode="Opaque" backcolor="#DEDEDE"/>
		</conditionalStyle>
	</style>
	<field name="edsPerfRptHdr.hospCode" class="java.lang.String"/>
	<field name="edsPerfRptHdr.workstoreCode" class="java.lang.String"/>
	<field name="edsPerfRptHdr.activeProfileName" class="java.lang.String"/>
	<field name="edsPerfRptDtl.pickCount" class="java.lang.Integer"/>
	<field name="edsPerfRptDtl.assembleCount" class="java.lang.Integer"/>
	<field name="edsPerfRptDtl.checkCount" class="java.lang.Integer"/>
	<field name="edsPerfRptDtl.issueCount" class="java.lang.Integer"/>
	<field name="edsPerfRptDtl.suspendCount" class="java.lang.Integer"/>
	<pageHeader>
		<band height="40" splitType="Stretch">
			<textField>
				<reportElement x="705" y="0" width="106" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Operation Mode: " + $F{edsPerfRptHdr.activeProfileName}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="1" y="0" width="427" height="20" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="14" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Performance Analysis (" + $F{edsPerfRptHdr.hospCode} + " - " + $F{edsPerfRptHdr.workstoreCode} + ")"]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<pageFooter>
		<band height="15" splitType="Stretch">
			<textField pattern="&apos;Printed on &apos; dd-MMM-yyyy &apos;at&apos; HH:mm">
				<reportElement x="1" y="0" width="184" height="15"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="499">
			<bar3DChart>
				<chart isShowLegend="false" customizerClass="hk.org.ha.model.pms.vo.report.EdsPerfChart" renderType="draw" theme="default">
					<reportElement x="0" y="13" width="810" height="457"/>
					<chartTitle/>
					<chartSubtitle/>
					<chartLegend position="Top"/>
				</chart>
				<categoryDataset>
					<categorySeries>
						<seriesExpression><![CDATA[""]]></seriesExpression>
						<categoryExpression><![CDATA["Picking\n(Item)"]]></categoryExpression>
						<valueExpression><![CDATA[$F{edsPerfRptDtl.pickCount}]]></valueExpression>
					</categorySeries>
					<categorySeries>
						<seriesExpression><![CDATA[""]]></seriesExpression>
						<categoryExpression><![CDATA["Assembling\n(Item)"]]></categoryExpression>
						<valueExpression><![CDATA[$F{edsPerfRptDtl.assembleCount}]]></valueExpression>
					</categorySeries>
					<categorySeries>
						<seriesExpression><![CDATA[""]]></seriesExpression>
						<categoryExpression><![CDATA["Checking\n(Rx)"]]></categoryExpression>
						<valueExpression><![CDATA[$F{edsPerfRptDtl.checkCount}]]></valueExpression>
					</categorySeries>
					<categorySeries>
						<seriesExpression><![CDATA[""]]></seriesExpression>
						<categoryExpression><![CDATA["Issuing\n(Rx)"]]></categoryExpression>
						<valueExpression><![CDATA[$F{edsPerfRptDtl.issueCount}]]></valueExpression>
					</categorySeries>
					<categorySeries>
						<seriesExpression><![CDATA[""]]></seriesExpression>
						<categoryExpression><![CDATA["Suspend\n(Rx)"]]></categoryExpression>
						<valueExpression><![CDATA[$F{edsPerfRptDtl.suspendCount}]]></valueExpression>
					</categorySeries>
				</categoryDataset>
				<bar3DPlot isShowLabels="false" xOffset="5.0" yOffset="5.0">
					<plot>
						<seriesColor seriesOrder="0" color="#FF0000"/>
					</plot>
					<itemLabel color="#000000" backgroundColor="#FFFFFF">
						<font fontName="Arial" size="8"/>
					</itemLabel>
					<categoryAxisLabelExpression><![CDATA["Activity"]]></categoryAxisLabelExpression>
					<categoryAxisFormat>
						<axisFormat>
							<labelFont>
								<font fontName="Arial" size="8" isBold="true"/>
							</labelFont>
							<tickLabelFont/>
						</axisFormat>
					</categoryAxisFormat>
					<valueAxisLabelExpression><![CDATA["Outstanding Rx / Item No."]]></valueAxisLabelExpression>
					<valueAxisFormat>
						<axisFormat>
							<labelFont>
								<font fontName="Arial" size="8" isBold="true"/>
							</labelFont>
							<tickLabelFont/>
						</axisFormat>
					</valueAxisFormat>
				</bar3DPlot>
			</bar3DChart>
		</band>
	</summary>
</jasperReport>
