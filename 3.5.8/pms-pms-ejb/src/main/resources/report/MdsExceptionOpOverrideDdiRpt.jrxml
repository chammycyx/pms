<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MdsExceptionOpOverrideDdiRpt" language="groovy" pageWidth="595" pageHeight="842" columnWidth="541" leftMargin="24" rightMargin="30" topMargin="38" bottomMargin="15">
	<property name="ireport.zoom" value="1.948717100000002"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<field name="caseNum" class="java.lang.String"/>
	<field name="patName" class="java.lang.String"/>
	<field name="patNameChi" class="java.lang.String"/>
	<field name="hkid" class="java.lang.String"/>
	<field name="sex" class="java.lang.String"/>
	<field name="patCatCode" class="java.lang.String"/>
	<field name="patAllergy" class="java.lang.String"/>
	<field name="patAdr" class="java.lang.String"/>
	<field name="g6pdFlag" class="java.lang.Boolean"/>
	<field name="pregnancyCheckFlag" class="java.lang.Boolean"/>
	<field name="allergyServerAvailableFlag" class="java.lang.Boolean"/>
	<field name="drugDisplayName" class="java.lang.String"/>
	<field name="mdsExceptionOverrideRptDtlList" class="java.util.List"/>
	<field name="ehrAllergyServerAvailableFlag" class="java.lang.Boolean"/>
	<field name="ehrAllergy" class="java.lang.String"/>
	<field name="ehrAdr" class="java.lang.String"/>
	<group name="summary group">
		<groupHeader>
			<band height="50">
				<line>
					<reportElement positionType="Float" x="0" y="39" width="541" height="1">
						<printWhenExpression><![CDATA[!$F{ehrAllergyServerAvailableFlag} || $F{ehrAllergy} != null || $F{ehrAdr} != null]]></printWhenExpression>
					</reportElement>
					<graphicElement>
						<pen lineWidth="0.75" lineStyle="Dashed"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="0" y="2" width="389" height="18" isRemoveLineWhenBlank="true"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[!$F{ehrAllergyServerAvailableFlag} || $F{ehrAllergy}!=null ? "eHRSS Allergy:" : ""]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="0" y="20" width="389" height="18" isRemoveLineWhenBlank="true"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[!$F{ehrAllergyServerAvailableFlag} || $F{ehrAdr}!=null ? "eHRSS Adverse Drug Reaction:" : ""]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="104" y="0" width="437" height="18" isRemoveLineWhenBlank="true"/>
					<textElement>
						<font fontName="HA_MingLiu"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{ehrAllergyServerAvailableFlag}?
($F{ehrAllergy} != null && !("").equals($F{ehrAllergy}) ? $F{ehrAllergy} + "\n" : ""):
"Allergy Information Unavailable" + "\n"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="179" y="18" width="362" height="18" isRemoveLineWhenBlank="true"/>
					<textElement>
						<font fontName="HA_MingLiu"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{ehrAllergyServerAvailableFlag}?
($F{ehrAdr} != null && !("").equals($F{ehrAdr}) ? $F{ehrAdr} + "\n" : ""):
"Adverse Drug Reaction Information Unavailable" + "\n"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="221" splitType="Stretch">
				<staticText>
					<reportElement x="0" y="9" width="526" height="25"/>
					<textElement>
						<font fontName="Courier New" isBold="true"/>
					</textElement>
					<text><![CDATA[Please either amend the prescription or give reason(s) for overriding of the above drug alert:]]></text>
				</staticText>
				<staticText>
					<reportElement x="24" y="41" width="417" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[The prescription has been amended in response to the above drug alert]]></text>
				</staticText>
				<staticText>
					<reportElement x="24" y="55" width="417" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[No other alternative available]]></text>
				</staticText>
				<staticText>
					<reportElement x="24" y="83" width="417" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Patient is taking this drug combination without problem]]></text>
				</staticText>
				<rectangle>
					<reportElement x="0" y="41" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="0" y="55" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="0" y="69" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="0" y="83" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement x="0" y="97" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement x="24" y="97" width="417" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Others:]]></text>
				</staticText>
				<line>
					<reportElement x="66" y="106" width="375" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement x="0" y="128" width="91" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Name of Doctor:]]></text>
				</staticText>
				<staticText>
					<reportElement x="253" y="128" width="56" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[MO Code:]]></text>
				</staticText>
				<staticText>
					<reportElement x="419" y="128" width="83" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Rank:]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="148" width="91" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Signature:]]></text>
				</staticText>
				<staticText>
					<reportElement x="197" y="148" width="56" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Date:]]></text>
				</staticText>
				<staticText>
					<reportElement x="358" y="148" width="83" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[Contact No.:]]></text>
				</staticText>
				<line>
					<reportElement x="89" y="139" width="151" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="306" y="139" width="106" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="449" y="139" width="88" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="64" y="159" width="88" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="235" y="159" width="88" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement x="431" y="159" width="88" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement x="0" y="172" width="541" height="29"/>
					<textElement>
						<font fontName="Courier New" isBold="true"/>
					</textElement>
					<text><![CDATA[Kindly return this report along with the prescription (amended if necessary) in order for dispensing to proceed. Thank you for your co-operation.]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="202" width="541" height="12"/>
					<textElement>
						<font fontName="Courier New" isBold="true"/>
					</textElement>
					<text><![CDATA[Should you need further clarification or assistance, please call pharmacy at  ]]></text>
				</staticText>
				<line>
					<reportElement x="457" y="213" width="82" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement x="24" y="69" width="417" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[The on hand drug has been discontinued]]></text>
				</staticText>
				<staticText>
					<reportElement x="537" y="202" width="12" height="12"/>
					<textElement>
						<font fontName="Courier New"/>
					</textElement>
					<text><![CDATA[.]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="187" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="189" height="13"/>
				<textElement>
					<font fontName="Courier New" size="11" isBold="true"/>
				</textElement>
				<text><![CDATA[To: Attending Doctor / Dr. ]]></text>
			</staticText>
			<line>
				<reportElement x="190" y="10" width="52" height="1"/>
				<graphicElement>
					<pen lineWidth="0.75"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="116" y="21" width="338" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Exception Report (Medication Decision Support Alerts)]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="55" width="541" height="1"/>
				<graphicElement>
					<pen lineWidth="0.75" lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="0" y="57" width="541" height="1"/>
				<graphicElement>
					<pen lineWidth="0.75" lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<textField isBlankWhenNull="true">
				<reportElement x="79" y="63" width="100" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{caseNum}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="63" width="79" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Case No.]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="80" width="79" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Patient Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="97" width="79" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[HKID]]></text>
			</staticText>
			<staticText>
				<reportElement x="179" y="97" width="27" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Sex:]]></text>
			</staticText>
			<staticText>
				<reportElement x="251" y="97" width="63" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Category:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="79" y="80" width="225" height="12"/>
				<textElement>
					<font fontName="Courier New" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{patName}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="304" y="78" width="86" height="12"/>
				<textElement>
					<font fontName="HA_MingLiu" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{patNameChi}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="79" y="97" width="100" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{hkid} != null && $F{hkid}.length() == 8 ?
$F{hkid}.substring(0, 7) + "(" + $F{hkid}.substring(7, 8) + ")" :
$F{hkid}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="206" y="97" width="45" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{sex}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="315" y="97" width="75" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{patCatCode}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="72" y="63" width="9" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="72" y="80" width="9" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement x="72" y="97" width="9" height="12"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="0" y="115" width="179" height="18"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Drug Allergy:]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="0" y="169" width="541" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{pregnancyCheckFlag}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Pregnancy Contraindication Checking: ON]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="152" y="115" width="389" height="18"/>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[!$F{allergyServerAvailableFlag} ? "Allergy Information Unavailable": (
    $F{patAllergy} == null ? "No Known Drug Allergy" : $F{patAllergy}
)]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="152" y="133" width="389" height="18" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[!$F{allergyServerAvailableFlag} || $F{patAdr} != null]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[!$F{allergyServerAvailableFlag} ? "Adverse Drug Reaction Information Unavailable": $F{patAdr}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="133" width="179" height="18" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[!$F{allergyServerAvailableFlag} || $F{patAdr} != null]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<text><![CDATA[Adverse Drug Reaction:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="151" width="541" height="18" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{g6pdFlag} || !$F{allergyServerAvailableFlag}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Courier New"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{g6pdFlag} ? "Alert: G6PD deficiency" : "Alert: Alert Information Unavailable"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="184" width="541" height="1"/>
				<graphicElement>
					<pen lineWidth="0.75" lineStyle="Dashed"/>
				</graphicElement>
			</line>
		</band>
	</pageHeader>
	<detail>
		<band height="73" splitType="Prevent">
			<rectangle>
				<reportElement positionType="Float" x="0" y="0" width="541" height="12" forecolor="#666666" backcolor="#666666"/>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="79" y="0" width="460" height="12" forecolor="#FFFFFF"/>
				<textElement>
					<font fontName="Courier New" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugDisplayName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="0" width="79" height="12" forecolor="#FFFFFF"/>
				<textElement>
					<font fontName="Courier New" isBold="true"/>
				</textElement>
				<text><![CDATA[CAUTION For]]></text>
			</staticText>
			<rectangle>
				<reportElement positionType="Float" stretchType="RelativeToBandHeight" x="0" y="13" width="541" height="59" isPrintWhenDetailOverflows="true"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="2" y="14" width="539" height="44"/>
				<subreportParameter name="SUBREPORT_DIR">
					<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{mdsExceptionOverrideRptDtlList})]]></dataSourceExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "MdsExceptionOpOverrideDdiRptDtl.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Immediate"/>
	</columnFooter>
	<pageFooter>
		<band height="12" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="390" y="0" width="132" height="12"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Courier New" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<textField pattern="&apos;Printed on&apos; dd-MMM-yyyy &apos;at&apos; HH:mm ">
				<reportElement x="0" y="0" width="229" height="12"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Courier New" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="525" y="0" width="24" height="12"/>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Courier New" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
