<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MedSummaryRptChiDtl" language="groovy" pageWidth="480" pageHeight="802" columnWidth="480" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="110"/>
	<import value="hk.org.ha.model.pms.util.QrCodeHelper"/>
	<field name="itemCode" class="java.lang.String"/>
	<field name="labelDesc" class="java.lang.String"/>
	<field name="labelInstructionText" class="java.lang.String"/>
	<field name="issueQty" class="java.lang.String"/>
	<field name="baseUnit" class="java.lang.String"/>
	<field name="warningText" class="java.lang.String"/>
	<field name="startDate" class="java.util.Date"/>
	<field name="itemXmlBarCode" class="java.lang.String"/>
	<field name="barCodeEnable" class="java.lang.Boolean"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="183" splitType="Prevent">
			<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}!=null]]></printWhenExpression>
			<line>
				<reportElement positionType="Float" x="0" y="0" width="480" height="1" isPrintWhenDetailOverflows="true"/>
			</line>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="1" width="422" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="380" y="1" width="100" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="17" width="480" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelInstructionText}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="33" width="60" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[開始日期: ]]></text>
			</staticText>
			<textField pattern="">
				<reportElement positionType="Float" x="60" y="35" width="39" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getYear() + 1900)]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="54" width="480" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="MingLiU"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{warningText}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="293" y="69" width="187" height="20" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{issueQty}+" "+$F{baseUnit}]]></textFieldExpression>
			</textField>
			<image scaleImage="Clip" hAlign="Center" vAlign="Middle">
				<reportElement positionType="Float" x="11" y="75" width="90" height="90">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}!=null]]></printWhenExpression>
				</reportElement>
				<imageExpression class="java.io.InputStream"><![CDATA[QrCodeHelper.createQrCodeImageStream($F{itemXmlBarCode}, 90, 90)]]></imageExpression>
			</image>
			<line>
				<reportElement positionType="Float" x="0" y="170" width="480" height="1">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}!=null]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement positionType="Float" x="99" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[年]]></text>
			</staticText>
			<textField pattern="">
				<reportElement positionType="Float" x="114" y="35" width="22" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getMonth()+1)]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="136" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[月]]></text>
			</staticText>
			<textField pattern="">
				<reportElement positionType="Float" x="151" y="35" width="22" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getDate())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="173" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[日]]></text>
			</staticText>
		</band>
		<band height="90" splitType="Prevent">
			<printWhenExpression><![CDATA[!$F{barCodeEnable}]]></printWhenExpression>
			<staticText>
				<reportElement positionType="Float" x="136" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[月]]></text>
			</staticText>
			<textField>
				<reportElement positionType="Float" x="293" y="69" width="187" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{issueQty}+" "+$F{baseUnit}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="33" width="60" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[開始日期: ]]></text>
			</staticText>
			<textField pattern="">
				<reportElement positionType="Float" x="151" y="35" width="22" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getDate())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="99" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[年]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="1" width="422" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="380" y="1" width="100" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="173" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[日]]></text>
			</staticText>
			<line>
				<reportElement positionType="Float" x="0" y="0" width="480" height="1" isPrintWhenDetailOverflows="true"/>
			</line>
			<line>
				<reportElement positionType="Float" x="0" y="84" width="480" height="1" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[!$F{barCodeEnable} || ($F{barCodeEnable} && $F{itemXmlBarCode}==null)]]></printWhenExpression>
				</reportElement>
			</line>
			<textField pattern="">
				<reportElement positionType="Float" x="60" y="35" width="39" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getYear() + 1900)]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="17" width="480" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelInstructionText}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="54" width="480" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="MingLiU"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{warningText}]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement positionType="Float" x="114" y="35" width="22" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getMonth()+1)]]></textFieldExpression>
			</textField>
		</band>
		<band height="131" splitType="Prevent">
			<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
			<staticText>
				<reportElement positionType="Float" x="136" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[月]]></text>
			</staticText>
			<textField>
				<reportElement positionType="Float" x="293" y="69" width="187" height="20" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{issueQty}+" "+$F{baseUnit}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="33" width="60" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[開始日期: ]]></text>
			</staticText>
			<textField pattern="">
				<reportElement positionType="Float" x="151" y="35" width="22" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getDate())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="99" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[年]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="1" width="422" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="380" y="1" width="100" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="173" y="35" width="15" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top" markup="none">
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<text><![CDATA[日]]></text>
			</staticText>
			<line>
				<reportElement positionType="Float" x="0" y="0" width="480" height="1" isPrintWhenDetailOverflows="true"/>
			</line>
			<rectangle>
				<reportElement positionType="Float" x="0" y="102" width="219" height="14">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement positionType="Float" x="1" y="102" width="217" height="14">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font isItalic="true"/>
				</textElement>
				<text><![CDATA[No barcode information is available for this item.]]></text>
			</staticText>
			<line>
				<reportElement positionType="Float" x="0" y="124" width="480" height="1" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[!$F{barCodeEnable} || ($F{barCodeEnable} && $F{itemXmlBarCode}==null)]]></printWhenExpression>
				</reportElement>
			</line>
			<textField pattern="">
				<reportElement positionType="Float" x="60" y="35" width="39" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getYear() + 1900)]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="17" width="480" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="MingLiU" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelInstructionText}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="54" width="480" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="MingLiU"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{warningText}]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement positionType="Float" x="114" y="35" width="22" height="19" isPrintWhenDetailOverflows="true">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{startDate}.getMonth()+1)]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
