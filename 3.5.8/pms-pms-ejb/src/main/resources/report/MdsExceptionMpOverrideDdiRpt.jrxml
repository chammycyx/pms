<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MdsExceptionMpOverrideDdiRpt" language="groovy" pageWidth="595" pageHeight="842" columnWidth="541" leftMargin="24" rightMargin="30" topMargin="38" bottomMargin="15">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<field name="caseNum" class="java.lang.String"/>
	<field name="patName" class="java.lang.String"/>
	<field name="patNameChi" class="java.lang.String"/>
	<field name="hkid" class="java.lang.String"/>
	<field name="sex" class="java.lang.String"/>
	<field name="patCatCode" class="java.lang.String"/>
	<field name="patAllergy" class="java.lang.String"/>
	<field name="patAdr" class="java.lang.String"/>
	<field name="g6pdFlag" class="java.lang.Boolean"/>
	<field name="pregnancyCheckFlag" class="java.lang.Boolean"/>
	<field name="allergyServerAvailableFlag" class="java.lang.Boolean"/>
	<field name="drugDisplayName" class="java.lang.String"/>
	<field name="mdsExceptionOverrideRptDtlList" class="java.util.List"/>
	<field name="refillFlag" class="java.lang.Boolean"/>
	<field name="ward" class="java.lang.String"/>
	<field name="remark" class="java.lang.String"/>
	<field name="drugDisplayName1" class="java.lang.String"/>
	<field name="drugDisplayName2" class="java.lang.String"/>
	<field name="crossDdiFlag" class="java.lang.Boolean"/>
	<field name="ehrAllergyServerAvailableFlag" class="java.lang.Boolean"/>
	<field name="ehrAllergy" class="java.lang.String"/>
	<field name="ehrAdr" class="java.lang.String"/>
	<group name="summary group">
		<groupHeader>
			<band height="47">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="0" y="2" width="421" height="12" isRemoveLineWhenBlank="true"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[!$F{ehrAllergyServerAvailableFlag} || $F{ehrAllergy}!=null ? "eHRSS Allergy:" : ""]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="0" y="16" width="421" height="12" isRemoveLineWhenBlank="true"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[!$F{ehrAllergyServerAvailableFlag} || $F{ehrAdr}!=null ? "eHRSS Adverse Drug Reaction:" : ""]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="80" y="0" width="461" height="12" isRemoveLineWhenBlank="true"/>
					<textElement verticalAlignment="Top">
						<font fontName="HA_MingLiu" size="10"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{ehrAllergyServerAvailableFlag}?
($F{ehrAllergy} != null && !("").equals($F{ehrAllergy}) ? $F{ehrAllergy} + "\n" : ""):
"Allergy Information Unavailable" + "\n"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" isPrintRepeatedValues="false" x="157" y="14" width="384" height="12" isRemoveLineWhenBlank="true"/>
					<textElement verticalAlignment="Top">
						<font fontName="HA_MingLiu" size="10"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{ehrAllergyServerAvailableFlag}?
($F{ehrAdr} != null && !("").equals($F{ehrAdr}) ? $F{ehrAdr} + "\n" : ""):
"Adverse Drug Reaction Information Unavailable" + "\n"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="Float" x="0" y="35" width="541" height="1">
						<printWhenExpression><![CDATA[!$F{ehrAllergyServerAvailableFlag} || $F{ehrAllergy} != null || $F{ehrAdr} != null]]></printWhenExpression>
					</reportElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="229" splitType="Stretch">
				<staticText>
					<reportElement positionType="Float" x="20" y="77" width="521" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[No other alternative available]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="20" y="91" width="521" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Patient is taking this drug combination without problem]]></text>
				</staticText>
				<rectangle>
					<reportElement x="0" y="49" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement positionType="Float" x="0" y="63" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement positionType="Float" x="0" y="77" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement positionType="Float" x="0" y="91" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<rectangle>
					<reportElement positionType="Float" x="0" y="105" width="10" height="10"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</rectangle>
				<staticText>
					<reportElement positionType="Float" x="20" y="105" width="521" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Others:]]></text>
				</staticText>
				<line>
					<reportElement positionType="Float" x="61" y="115" width="375" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement positionType="Float" x="0" y="130" width="91" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Name of Doctor:]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="258" y="130" width="56" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[MO Code:]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="419" y="130" width="83" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Rank:]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="0" y="150" width="91" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Signature:]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="197" y="150" width="56" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Date:]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="358" y="150" width="83" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Contact No.:]]></text>
				</staticText>
				<line>
					<reportElement positionType="Float" x="74" y="141" width="177" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="Float" x="303" y="141" width="98" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="Float" x="446" y="141" width="91" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="Float" x="56" y="161" width="98" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="Float" x="230" y="161" width="98" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="Float" x="421" y="161" width="98" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75"/>
					</graphicElement>
				</line>
				<staticText>
					<reportElement positionType="Float" x="0" y="174" width="541" height="29"/>
					<textElement>
						<font fontName="Arial" isBold="true"/>
					</textElement>
					<text><![CDATA[Please kindly return this report to pharmacy at your earliest convenience for update of patient
medication profile. Thank you for your co-operation.]]></text>
				</staticText>
				<textField>
					<reportElement x="0" y="17" width="526" height="25"/>
					<textElement>
						<font fontName="Arial" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA["Please discontinue one or both of the medication items or give reason(s) for overriding of the above drug alert:"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement x="20" y="49" width="521" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{drugDisplayName1}+($F{crossDdiFlag} ? " is discontinued in IPMOE":" is discontinued and updated MAR has been faxed to pharmacy")]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement positionType="Float" x="0" y="204" width="541" height="20" isRemoveLineWhenBlank="true"/>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{remark}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement positionType="Float" x="20" y="63" width="521" height="12"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{drugDisplayName2}+" is discontinued and updated MAR has been faxed to pharmacy"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement positionType="Float" x="0" y="61" width="541" height="2"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[EmptyLine1]]></text>
				</staticText>
				<staticText>
					<reportElement positionType="Float" x="0" y="75" width="541" height="2"/>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[EmptyLine2]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="150">
			<staticText>
				<reportElement x="323" y="0" width="218" height="12"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[To: Attending Doctor / Dr._______________]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement x="0" y="28" width="541" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" size="16" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Exception Report (Medication Decision Support Alerts)" + ($F{refillFlag} ? "\nfor Automatic Refill" : "")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="455" y="65" width="29" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Ward:]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="0" y="93" width="275" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[!$F{allergyServerAvailableFlag} || $F{patAdr} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Adverse Drug Reaction:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="303" y="65" width="20" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{sex}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="281" y="65" width="22" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Sex:]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="0" y="65" width="68" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Patient:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="31" y="107" width="510" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{g6pdFlag} || !$F{allergyServerAvailableFlag}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{g6pdFlag} ? "G6PD deficiency" : "Alert Information Unavailable"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="198" y="63" width="83" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{patNameChi} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="HA_MingLiu" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["(" + $F{patNameChi} + ")"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="142" width="541" height="1"/>
			</line>
			<staticText>
				<reportElement positionType="Float" x="323" y="65" width="28" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Case:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="120" y="93" width="421" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[!$F{allergyServerAvailableFlag} || $F{patAdr} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[!$F{allergyServerAvailableFlag} ? "Adverse Drug Reaction Information Unavailable": $F{patAdr}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="120" y="79" width="421" height="12" isRemoveLineWhenBlank="true"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[!$F{allergyServerAvailableFlag} ? "Allergy Information Unavailable": (
    $F{patAllergy} == null ? "No Known Drug Allergy" : $F{patAllergy}
)]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="351" y="65" width="104" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{caseNum}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="185" y="121" width="77" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{pregnancyCheckFlag}]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<text><![CDATA[ON]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="0" y="121" width="185" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{pregnancyCheckFlag}]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Pregnancy Contraindication Checking:]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="0" y="107" width="259" height="12" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[$F{g6pdFlag} || !$F{allergyServerAvailableFlag}]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Alert:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="41" y="65" width="157" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{patName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="79" width="275" height="12" isRemoveLineWhenBlank="true"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Drug Allergy:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="484" y="65" width="57" height="12"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ward}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="73" splitType="Prevent">
			<rectangle>
				<reportElement positionType="Float" x="0" y="0" width="541" height="12" forecolor="#666666" backcolor="#666666"/>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="68" y="0" width="471" height="12" forecolor="#FFFFFF"/>
				<textElement>
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{drugDisplayName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="0" width="68" height="12" forecolor="#FFFFFF"/>
				<textElement>
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<text><![CDATA[CAUTION For]]></text>
			</staticText>
			<rectangle>
				<reportElement positionType="Float" stretchType="RelativeToBandHeight" x="0" y="13" width="541" height="59" isPrintWhenDetailOverflows="true"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="2" y="14" width="539" height="44"/>
				<subreportParameter name="SUBREPORT_DIR">
					<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{mdsExceptionOverrideRptDtlList})]]></dataSourceExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "MdsExceptionMpOverrideDdiRptDtl.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Immediate"/>
	</columnFooter>
	<pageFooter>
		<band height="12" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="157" y="0" width="132" height="12"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<textField pattern="yyyy-MM-dd HH:mm:ss">
				<reportElement x="358" y="0" width="183" height="12"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="292" y="0" width="24" height="12"/>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
