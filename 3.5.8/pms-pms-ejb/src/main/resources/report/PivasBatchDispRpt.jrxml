<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="PivasBatchDispRpt" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="812" leftMargin="15" rightMargin="15" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.948717100000005"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String">
		<defaultValueExpression><![CDATA["PivasBatchDispRpt/"]]></defaultValueExpression>
	</parameter>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<field name="pivasRptHdr.supplyDateFrom" class="java.util.Date"/>
	<field name="pivasRptHdr.supplyDateTo" class="java.util.Date"/>
	<field name="pivasRptHdr.pivasRptItemCatFilterOption.displayValue" class="java.lang.String"/>
	<field name="pivasRptHdr.itemCode" class="java.lang.String"/>
	<field name="pivasRptHdr.drugKeyFlag" class="java.lang.Boolean"/>
	<field name="pivasRptHdr.manufCode" class="java.lang.String"/>
	<field name="pivasRptHdr.batchNum" class="java.lang.String"/>
	<field name="pivasBatchDispRptDtlList" class="java.util.List"/>
	<detail>
		<band height="48">
			<textField>
				<reportElement x="0" y="0" width="812" height="20"/>
				<textElement verticalAlignment="Middle" markup="none">
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["PIVAS Batch Dispensing Report" + " (" + $P{hospCode} + " - " + $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="22" width="260" height="12"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Supply Date: " +
(
    $F{pivasRptHdr.supplyDateTo} != null ?
    new SimpleDateFormat("dd-MMM-yyyy").format($F{pivasRptHdr.supplyDateFrom}) + " to " + new SimpleDateFormat("dd-MMM-yyyy").format($F{pivasRptHdr.supplyDateTo}):
    new SimpleDateFormat("dd-MMM-yyyy HH:mm").format($F{pivasRptHdr.supplyDateFrom})
)]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="260" y="22" width="552" height="12"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Item Category: " + $F{pivasRptHdr.pivasRptItemCatFilterOption.displayValue}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="34" width="180" height="12"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Item Code: " +
($F{pivasRptHdr.itemCode} != null ?
$F{pivasRptHdr.itemCode}: "")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="180" y="34" width="80" height="12">
					<printWhenExpression><![CDATA[$F{pivasRptHdr.drugKeyFlag} != null && $F{pivasRptHdr.drugKeyFlag}]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<text><![CDATA[Drug Key: Y]]></text>
			</staticText>
			<textField>
				<reportElement x="260" y="34" width="260" height="12"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Manufacturer: " +
($F{pivasRptHdr.manufCode} != null ?
$F{pivasRptHdr.manufCode}: "")]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="520" y="34" width="292" height="12"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Batch No.: " +
($F{pivasRptHdr.batchNum} != null ?
$F{pivasRptHdr.batchNum}: "")]]></textFieldExpression>
			</textField>
		</band>
		<band height="44">
			<line>
				<reportElement x="0" y="0" width="812" height="1"/>
				<graphicElement>
					<pen lineWidth="1.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<subreport>
				<reportElement x="0" y="3" width="812" height="39"/>
				<subreportParameter name="SUBREPORT_DIR">
					<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{pivasBatchDispRptDtlList})]]></dataSourceExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "PivasBatchDispRptDtl.jasper"]]></subreportExpression>
			</subreport>
		</band>
		<band height="16">
			<staticText>
				<reportElement x="0" y="1" width="812" height="12"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[End]]></text>
			</staticText>
		</band>
	</detail>
	<pageFooter>
		<band height="15">
			<textField pattern="&apos;Printed on&apos; dd-MMM-yyyy &apos;at&apos; HH:mm ">
				<reportElement x="0" y="0" width="229" height="15"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="784" y="0" width="28" height="15"/>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="649" y="0" width="132" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
