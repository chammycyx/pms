-- update RefillSchedule Workstore
update refill_schedule r1
set (r1.hosp_code,r1.workstore_code) = ( select po1.hosp_code, po1.workstore_code from pharm_order po1 where po1.id = r1.pharm_order_id )
where r1.id in( 
select r1.id from pharm_order po1, refill_schedule r1
where po1.id = r1.pharm_order_id
and ( po1.hosp_code <> r1.hosp_code or po1.workstore_code <> r1.workstore_code )
);

--update RefillScheduleItem Workstore
update refill_schedule_item ri2
set (ri2.hosp_code, ri2.workstore_code) = ( select r2.hosp_code, r2.workstore_code from refill_schedule r2 where r2.id = ri2.refill_schedule_id )
where ri2.id in(
select ri2.id from refill_schedule r2, refill_schedule_item ri2
where r2.id = ri2.refill_schedule_id
and ( r2.hosp_code <> ri2.hosp_code or r2.workstore_code <> ri2.workstore_code )
);

--//@UNDO
--//