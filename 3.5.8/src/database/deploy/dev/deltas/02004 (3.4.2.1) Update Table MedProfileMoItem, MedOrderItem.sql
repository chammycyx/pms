alter table MED_PROFILE_MO_ITEM add MODIFY_FLAG number(1);
comment on column MED_PROFILE_MO_ITEM.MODIFY_FLAG is 'Modify flag for IPMOE';

alter table MED_ORDER_ITEM add MODIFY_FLAG number(1);
comment on column MED_ORDER_ITEM.MODIFY_FLAG is 'Modify flag for IPMOE';

--//@UNDO
alter table MED_PROFILE_MO_ITEM set unused column MODIFY_FLAG;
alter table MED_ORDER_ITEM set unused column MODIFY_FLAG;
--//