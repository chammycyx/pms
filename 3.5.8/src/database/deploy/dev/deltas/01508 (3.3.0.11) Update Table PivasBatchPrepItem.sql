alter table PIVAS_BATCH_PREP_ITEM add SOLVENT_CODE varchar2(25);
alter table PIVAS_BATCH_PREP_ITEM add SOLVENT_DESC varchar2(100);

comment on column PIVAS_BATCH_PREP_ITEM.SOLVENT_CODE is 'Solvent code';
comment on column PIVAS_BATCH_PREP_ITEM.SOLVENT_DESC is 'Solvent description';

--//@UNDO
alter table PIVAS_BATCH_PREP_ITEM drop column SOLVENT_CODE;
alter table PIVAS_BATCH_PREP_ITEM drop column SOLVENT_DESC;
--//