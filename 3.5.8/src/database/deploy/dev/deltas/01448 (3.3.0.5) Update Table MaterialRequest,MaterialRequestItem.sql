alter table MATERIAL_REQUEST drop column STATUS;
alter table MATERIAL_REQUEST_ITEM add STATUS varchar2(1) not null;

comment on column MATERIAL_REQUEST_ITEM.STATUS is 'Status';

--//@UNDO
alter table MATERIAL_REQUEST add STATUS varchar2(1) not null;
alter table MATERIAL_REQUEST_ITEM drop column STATUS;
--//