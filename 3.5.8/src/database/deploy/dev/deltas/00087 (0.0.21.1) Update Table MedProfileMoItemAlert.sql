alter table MED_PROFILE_MO_ITEM_ALERT add SORT_SEQ number(19) null;
update MED_PROFILE_MO_ITEM_ALERT set SORT_SEQ = ID where SORT_SEQ is null;
alter table MED_PROFILE_MO_ITEM_ALERT modify SORT_SEQ not null;

comment on column MED_PROFILE_MO_ITEM_ALERT.SORT_SEQ is 'Sort sequence';

--//@UNDO
alter table MED_PROFILE_MO_ITEM_ALERT drop column SORT_SEQ;
--//
