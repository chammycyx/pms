update PROP set DESCRIPTION='Maximium duration (in weeks) of CAPD regimen', TYPE='ww', VALIDATION='1-130' where ID=32037;
update PROP set VALIDATION='r8,c50' where ID=14004;
update PROP set VALIDATION='1-50' where ID=18001;
update PROP set VALIDATION='1-50' where ID=18003;
update PROP set VALIDATION='r8,c50' where ID=25007;

--//@UNDO
update PROP set DESCRIPTION='Maximium duration (in months) of CAPD regimen', TYPE='MM', VALIDATION='1-30' where ID=32037;
update PROP set VALIDATION='r8,c25' where ID=14004;
update PROP set VALIDATION='1-25' where ID=18001;
update PROP set VALIDATION='1-25' where ID=18003;
update PROP set VALIDATION='r8,c25' where ID=25007;
--//