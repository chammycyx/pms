create table DRS_CONFIG (
    ID number(19) not null,
    TYPE varchar2(1) not null,
    PAS_SPEC_CODE varchar2(4) not null,    
    ITEM_COUNT number(3) not null,
    THRESHOLD number(3) not null,
    INTERVAL number(3) not null,
    LAST_INTERVAL_MAX_DAY number(3) not null,
    MR_REMINDER_FLAG number(1) default 0 not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DRS_CONFIG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_DRS_CONFIG_01 unique (HOSP_CODE, WORKSTORE_CODE, TYPE, PAS_SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  DRS_CONFIG is 'Drs config';
comment on column DRS_CONFIG.ID is 'Drs config id';
comment on column DRS_CONFIG.TYPE is 'Specialty type';
comment on column DRS_CONFIG.PAS_SPEC_CODE is 'PAS specialty code';
comment on column DRS_CONFIG.ITEM_COUNT is 'Item count';
comment on column DRS_CONFIG.THRESHOLD is 'Split drug item after';
comment on column DRS_CONFIG.INTERVAL is 'Refill interval';
comment on column DRS_CONFIG.LAST_INTERVAL_MAX_DAY is 'Maximum interval for the last refill';
comment on column DRS_CONFIG.MR_REMINDER_FLAG is 'Mr reminder flag';
comment on column DRS_CONFIG.HOSP_CODE is 'Hospital code';
comment on column DRS_CONFIG.WORKSTORE_CODE is 'Workstore code';
comment on column DRS_CONFIG.CREATE_USER is 'Created user';
comment on column DRS_CONFIG.CREATE_DATE is 'Created date';
comment on column DRS_CONFIG.UPDATE_USER is 'Last updated user';
comment on column DRS_CONFIG.UPDATE_DATE is 'Last updated date';
comment on column DRS_CONFIG.VERSION is 'Version to serve as optimistic lock value';

create table DRS_PATIENT (
    ID number(19) not null,
    PATIENT_ID number(19) not null,    
    ENABLE_FLAG number(1) default 0 not null,
    DURATION_CHECK_FLAG number(1) default 0 not null,
    ITEM_COUNT_CHECK_FLAG number(1) default 0 not null,    
    REMARK_TEXT varchar2(1000) null,
    HOSP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DRS_PATIENT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_DRS_PATIENT_01 unique (HOSP_CODE, PATIENT_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  DRS_PATIENT is 'Drs patient';
comment on column DRS_PATIENT.ID is 'Drs patient id';
comment on column DRS_PATIENT.PATIENT_ID is 'Drs patient patient id';
comment on column DRS_PATIENT.ENABLE_FLAG is 'Enable Flag';
comment on column DRS_PATIENT.DURATION_CHECK_FLAG is 'Duration check flag';
comment on column DRS_PATIENT.ITEM_COUNT_CHECK_FLAG is 'Item count check flag';
comment on column DRS_PATIENT.REMARK_TEXT is 'Remark text';
comment on column DRS_PATIENT.HOSP_CODE is 'Hospital code';
comment on column DRS_PATIENT.CREATE_USER is 'Created user';
comment on column DRS_PATIENT.CREATE_DATE is 'Created date';
comment on column DRS_PATIENT.UPDATE_USER is 'Last updated user';
comment on column DRS_PATIENT.UPDATE_DATE is 'Last updated date';
comment on column DRS_PATIENT.VERSION is 'Version to serve as optimistic lock value';

create table DRS_PATIENT_CONTACT (
    ID number(19) not null,
    DRS_PATIENT_ID number(19) not null,
   	DESCRIPTION varchar2(30) not null,
   	PHONE varchar2(10) not null,
	PHONE_EXT varchar2(4) null,
   	SMS_FLAG number(1) default 0 not null,
   	VOICE_FLAG number(1) default 0 not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DRS_PATIENT_CONTACT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_DRS_PATIENT_CONTACT_01 unique (DESCRIPTION, DRS_PATIENT_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  DRS_PATIENT_CONTACT is 'Drs patient contact';
comment on column DRS_PATIENT_CONTACT.ID is 'Drs patient contact id';
comment on column DRS_PATIENT_CONTACT.DRS_PATIENT_ID is 'Drs patient id';
comment on column DRS_PATIENT_CONTACT.DESCRIPTION is 'Description';
comment on column DRS_PATIENT_CONTACT.PHONE is 'Phone number';
comment on column DRS_PATIENT_CONTACT.PHONE_EXT is 'Extension';
comment on column DRS_PATIENT_CONTACT.SMS_FLAG is 'Sms notification';
comment on column DRS_PATIENT_CONTACT.VOICE_FLAG is 'Telephony notification';
comment on column DRS_PATIENT_CONTACT.CREATE_USER is 'Created user';
comment on column DRS_PATIENT_CONTACT.CREATE_DATE is 'Created date';
comment on column DRS_PATIENT_CONTACT.UPDATE_USER is 'Last updated user';
comment on column DRS_PATIENT_CONTACT.UPDATE_DATE is 'Last updated date';
comment on column DRS_PATIENT_CONTACT.VERSION is 'Version to serve as optimistic lock value';

alter table DRS_CONFIG add constraint FK_DRS_CONFIG_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE(HOSP_CODE, WORKSTORE_CODE);
create index FK_DRS_CONFIG_01 on DRS_CONFIG (HOSP_CODE, WORKSTORE_CODE) tablespace PMS_INDX_01 online;

alter table DRS_PATIENT add constraint FK_DRS_PATIENT_01 foreign key (PATIENT_ID) references PATIENT (ID);
alter table DRS_PATIENT add constraint FK_DRS_PATIENT_02 foreign key (HOSP_CODE) references HOSPITAL(HOSP_CODE);
create index FK_DRS_PATIENT_01 on DRS_PATIENT (PATIENT_ID) tablespace PMS_INDX_01 online;
create index FK_DRS_PATIENT_02 on DRS_PATIENT (HOSP_CODE) tablespace PMS_INDX_01 online;

alter table DRS_PATIENT_CONTACT add constraint FK_DRS_PATIENT_CONTACT_01 foreign key (DRS_PATIENT_ID) references DRS_PATIENT (ID);
create index FK_DRS_PATIENT_CONTACT_01 on DRS_PATIENT_CONTACT (DRS_PATIENT_ID) tablespace PMS_INDX_01 online;

--//@UNDO
drop index FK_DRS_PATIENT_CONTACT_01;
alter table DRS_PATIENT_CONTACT drop constraint FK_DRS_PATIENT_CONTACT_01;

drop index FK_DRS_PATIENT_02;
drop index FK_DRS_PATIENT_01;
alter table DRS_PATIENT drop constraint FK_DRS_PATIENT_02;
alter table DRS_PATIENT drop constraint FK_DRS_PATIENT_01;

drop index FK_DRS_CONFIG_01;
alter table DRS_CONFIG drop constraint FK_DRS_CONFIG_01;

drop table DRS_PATIENT_CONTACT;
drop table DRS_PATIENT;
drop table DRS_CONFIG;
--//