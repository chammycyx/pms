create index I_MED_ORDER_08 on MED_ORDER (HOSP_CODE, PRESC_TYPE, STATUS, PRE_VET_DATE) local online;

--//@UNDO
drop index I_MED_ORDER_08;
--//