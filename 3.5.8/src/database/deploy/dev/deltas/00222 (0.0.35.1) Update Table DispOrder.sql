alter table DISP_ORDER add BATCH_PROCESSING_FLAG varchar2(1);
alter table DISP_ORDER add BATCH_PROCESSING_TYPE varchar2(1);
alter table DISP_ORDER add EXTERNAL_ADMIN_STATUS varchar2(1);
alter table DISP_ORDER rename column DAY_END_REMARK to BATCH_REMARK;

comment on column DISP_ORDER.BATCH_PROCESSING_FLAG is 'Batch processing';
comment on column DISP_ORDER.BATCH_PROCESSING_TYPE is 'Batch processing type';
comment on column DISP_ORDER.EXTERNAL_ADMIN_STATUS is 'Current pharmacy admin status in external systems which obtain the latest status via batch job';
comment on column DISP_ORDER.BATCH_REMARK is 'Batch remark';

update DISP_ORDER set BATCH_PROCESSING_FLAG = 0;
update DISP_ORDER set BATCH_PROCESSING_TYPE = '-';
update DISP_ORDER set EXTERNAL_ADMIN_STATUS = ADMIN_STATUS;

alter table DISP_ORDER modify BATCH_PROCESSING_FLAG not null;
alter table DISP_ORDER modify BATCH_PROCESSING_TYPE not null;
alter table DISP_ORDER modify EXTERNAL_ADMIN_STATUS not null;


--//@UNDO
alter table DISP_ORDER drop column BATCH_PROCESSING_FLAG;
alter table DISP_ORDER drop column BATCH_PROCESSING_TYPE;
alter table DISP_ORDER drop column EXTERNAL_ADMIN_STATUS;
alter table DISP_ORDER rename column BATCH_REMARK to DAY_END_REMARK;
--//