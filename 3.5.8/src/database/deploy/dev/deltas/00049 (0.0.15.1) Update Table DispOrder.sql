alter table DISP_ORDER add UNCOLLECT_FLAG number(1) null;
update DISP_ORDER DO set DO.UNCOLLECT_FLAG = (select 1 from PHARM_ORDER PO where PO.ID = DO.PHARM_ORDER_ID and DO.DAY_END_STATUS='C' and PO.ADMIN_STATUS = 'U');
update DISP_ORDER set UNCOLLECT_FLAG = 0 where UNCOLLECT_FLAG is null;
alter table DISP_ORDER modify UNCOLLECT_FLAG default 0 not null;

comment on column DISP_ORDER.UNCOLLECT_FLAG is 'Uncollected';

--//@UNDO
alter table DISP_ORDER drop column UNCOLLECT_FLAG;
--//
