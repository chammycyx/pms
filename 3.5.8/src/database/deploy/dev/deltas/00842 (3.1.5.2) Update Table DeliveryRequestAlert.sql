alter table DELIVERY_REQUEST_ALERT add PRIVATE_FLAG number(1);
comment on column DELIVERY_REQUEST_ALERT.PRIVATE_FLAG is 'Private flag';

update DELIVERY_REQUEST_ALERT set PRIVATE_FLAG = 0;

alter table DELIVERY_REQUEST_ALERT modify PRIVATE_FLAG not null;

--//@UNDO
alter table DELIVERY_REQUEST_ALERT drop column PRIVATE_FLAG;
--//