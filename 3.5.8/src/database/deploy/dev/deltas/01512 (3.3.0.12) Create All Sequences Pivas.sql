create sequence SQ_PIVAS_HOLIDAY increment by 50 start with 10000000000049;
create sequence SQ_PIVAS_SERVICE_HOUR increment by 50 start with 10000000000049;
create sequence SQ_PIVAS_BATCH_PREP_ITEM increment by 50 start with 10000000000049;
create sequence SQ_PIVAS_PREP increment by 50 start with 10000000000049;
create sequence SQ_MATERIAL_REQUEST increment by 50 start with 10000000000049;
create sequence SQ_MATERIAL_REQUEST_ITEM increment by 50 start with 10000000000049;
create sequence SQ_PIVAS_WORKLIST increment by 50 start with 10000000000049;
create sequence SQ_PIVAS_PREP_METHOD increment by 50 start with 10000000000049;
create sequence SQ_PIVAS_PREP_METHOD_ITEM increment by 50 start with 10000000000049;

--//@UNDO
drop sequence SQ_PIVAS_HOLIDAY;
drop sequence SQ_PIVAS_SERVICE_HOUR;
drop sequence SQ_PIVAS_BATCH_PREP_ITEM;
drop sequence SQ_PIVAS_PREP;
drop sequence SQ_MATERIAL_REQUEST;
drop sequence SQ_MATERIAL_REQUEST_ITEM;
drop sequence SQ_PIVAS_WORKLIST;
drop sequence SQ_PIVAS_PREP_METHOD;
drop sequence SQ_PIVAS_PREP_METHOD_ITEM;
--//