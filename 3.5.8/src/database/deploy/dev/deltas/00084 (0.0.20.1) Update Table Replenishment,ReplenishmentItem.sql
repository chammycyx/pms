alter table REPLENISHMENT drop column NO_OF_DOSE;

alter table REPLENISHMENT_ITEM modify ISSUE_QTY null;

--//@UNDO
alter table REPLENISHMENT add NO_OF_DOSE number(10) null;
update REPLENISHMENT set NO_OF_DOSE = 0 where NO_OF_DOSE is null;
alter table REPLENISHMENT modify NO_OF_DOSE not null;

update REPLENISHMENT_ITEM set ISSUE_QTY = 0 where ISSUE_QTY is null;
alter table REPLENISHMENT_ITEM modify ISSUE_QTY not null;
--//