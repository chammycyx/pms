alter table MED_PROFILE_PO_ITEM add LAST_ITEM_CODE varchar2(6) null;

comment on column MED_PROFILE_PO_ITEM.LAST_ITEM_CODE is 'Last item code';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column LAST_ITEM_CODE;
--//