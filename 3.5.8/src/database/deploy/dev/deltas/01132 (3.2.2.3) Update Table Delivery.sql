alter table DELIVERY add PRINTED_COUNT number(10) null;


comment on column DELIVERY.PRINTED_COUNT is 'Printed item count';

--//@UNDO
alter table DELIVERY drop column PRINTED_COUNT;
--//