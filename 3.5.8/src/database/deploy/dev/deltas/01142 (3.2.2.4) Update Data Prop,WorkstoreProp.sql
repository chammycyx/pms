update PROP set DESCRIPTION = 'Enable printing IP dispensing label of own stock items', UPDATE_DATE=sysdate where ID=20018;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (20019,'label.mpDispLabel.zeroQtyNonOwnStockItem.printing.enabled','Enable printing IP dispensing label of zero quanity non-own-stock items','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,20019,'N',20019,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION = 'Enable printing of IP dispensing label for own stock item', UPDATE_DATE=sysdate where ID=20018;

delete from WORKSTORE_PROP where PROP_ID in (20019);
delete from PROP where ID in (20019);    
--//