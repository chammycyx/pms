update PURCHASE_REQUEST
set INVOICE_STATUS = '-' 
where INVOICE_STATUS is null
and CREATE_DATE between to_date('20120101', 'yyyymmdd') and to_date('20160101', 'yyyymmdd');

--//@UNDO

--//