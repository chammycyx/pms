alter table MED_PROFILE_PO_ITEM add TOTAL_ISSUE_QTY number(19,4) null;
alter table MED_PROFILE_PO_ITEM add REMAIN_ISSUE_QTY number(19,4) null;

comment on column MED_PROFILE_PO_ITEM.TOTAL_ISSUE_QTY is 'Total issue quantity';
comment on column MED_PROFILE_PO_ITEM.REMAIN_ISSUE_QTY is 'Remaining issue quantity';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column TOTAL_ISSUE_QTY;
alter table MED_PROFILE_PO_ITEM drop column REMAIN_ISSUE_QTY;
--//
