create table PURCHASE_REQUEST (
    ID number(19) not null,
    ISSUE_QTY number(19,4) not null,
    DIRECT_PRINT_FLAG number(1) default 0 not null,
    DUE_DATE timestamp null,
    PRINT_DATE timestamp null,
    INVOICE_DATE date null,
    INVOICE_NUM varchar2(15) null,
    INVOICE_STATUS varchar2(1) null,
    MANUAL_RECEIPT_NUM varchar2(20) null,
    FORCE_PROCEED_FLAG number(1) default 0 not null,
    FORCE_PROCEED_DATE date null,
    RECEIPT_NUM varchar2(20) null,
    FCS_CHECK_DATE timestamp null,
    FORCE_PROCEED_USER varchar2(12) null,
    FORCE_PROCEED_CODE varchar2(12) null,
    TOTAL_AMOUNT number(19) null,
    MANUAL_INVOICE_NUM varchar2(15) null,
    RECEIVE_AMOUNT number(19) null,
    DURATION_IN_DAY number(10) null,
    DOSE_QTY number(19,4) null,
    REQUEST_BY varchar2(100) null,
    MED_PROFILE_ID number(19) not null,
    MED_PROFILE_PO_ITEM_ID number(19) not null,
    LIFESTYLE_FLAG number(1) default 0 not null,
    ONCOLOGY_FLAG number(1) default 0 not null,
    PAS_PAY_CODE varchar2(12) null,
    PAS_PAT_GROUP_CODE varchar2(12) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_PURCHASE_REQUEST primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (maxvalue) tablespace P_PMS_MP_DATA_2012);    

comment on table PURCHASE_REQUEST is 'Purchase request log';
comment on column PURCHASE_REQUEST.ID is 'Purchase request id';
comment on column PURCHASE_REQUEST.ISSUE_QTY is 'Issued quantity';
comment on column PURCHASE_REQUEST.DIRECT_PRINT_FLAG is 'Direct printed';
comment on column PURCHASE_REQUEST.DUE_DATE is 'Due date';
comment on column PURCHASE_REQUEST.PRINT_DATE is 'Label print date';
comment on column PURCHASE_REQUEST.INVOICE_DATE is 'Invoice date';
comment on column PURCHASE_REQUEST.INVOICE_NUM is 'Generated invoice number';
comment on column PURCHASE_REQUEST.INVOICE_STATUS is 'Invoice status';
comment on column PURCHASE_REQUEST.MANUAL_RECEIPT_NUM is 'Manual receipt number';
comment on column PURCHASE_REQUEST.FORCE_PROCEED_FLAG is 'Force proceeded';
comment on column PURCHASE_REQUEST.FORCE_PROCEED_DATE is 'Force proceeded date';
comment on column PURCHASE_REQUEST.RECEIPT_NUM is 'Receipt number';
comment on column PURCHASE_REQUEST.FCS_CHECK_DATE is 'Checked FCS date';
comment on column PURCHASE_REQUEST.FORCE_PROCEED_USER is 'Force proceeded user';
comment on column PURCHASE_REQUEST.FORCE_PROCEED_CODE is 'Force proceeded code';
comment on column PURCHASE_REQUEST.TOTAL_AMOUNT is 'Total amount';
comment on column PURCHASE_REQUEST.MANUAL_INVOICE_NUM is 'Manual invoice number';
comment on column PURCHASE_REQUEST.RECEIVE_AMOUNT is 'Received amount';
comment on column PURCHASE_REQUEST.MED_PROFILE_ID is 'Medication profile id';
comment on column PURCHASE_REQUEST.MED_PROFILE_PO_ITEM_ID is 'Pharmacy order item id (medication profile)';
comment on column PURCHASE_REQUEST.LIFESTYLE_FLAG is 'Lifestyle item';
comment on column PURCHASE_REQUEST.ONCOLOGY_FLAG is 'Oncology item';
comment on column PURCHASE_REQUEST.PAS_PAY_CODE is 'PAS pay code';
comment on column PURCHASE_REQUEST.PAS_PAT_GROUP_CODE is 'PAS patient group code';
comment on column PURCHASE_REQUEST.CREATE_USER is 'Created user';
comment on column PURCHASE_REQUEST.CREATE_DATE is 'Created date';
comment on column PURCHASE_REQUEST.UPDATE_USER is 'Last updated user';
comment on column PURCHASE_REQUEST.UPDATE_DATE is 'Last updated date';

create sequence SQ_PURCHASE_REQUEST increment by 50 start with 10000000000049;

alter table PURCHASE_REQUEST add constraint FK_PURCHASE_REQUEST_01 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);
alter table PURCHASE_REQUEST add constraint FK_PURCHASE_REQUEST_02 foreign key (MED_PROFILE_PO_ITEM_ID) references MED_PROFILE_PO_ITEM (ID);

create index FK_PURCHASE_REQUEST_01 on PURCHASE_REQUEST (MED_PROFILE_ID) tablespace PMS_INDX_01;
create index FK_PURCHASE_REQUEST_02 on PURCHASE_REQUEST (MED_PROFILE_PO_ITEM_ID) tablespace PMS_INDX_01;

--//@UNDO
drop index FK_PURCHASE_REQUEST_02;
drop index FK_PURCHASE_REQUEST_01;

alter table PURCHASE_REQUEST drop constraint FK_PURCHASE_REQUEST_02;
alter table PURCHASE_REQUEST drop constraint FK_PURCHASE_REQUEST_01;

drop sequence SQ_PURCHASE_REQUEST;

drop table PURCHASE_REQUEST;
--//
