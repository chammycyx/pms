alter table MED_ORDER_ITEM add CMS_CHARGE_FLAG varchar2(1) null;
update MED_ORDER_ITEM set CMS_CHARGE_FLAG = CHARGE_FLAG where CMS_CHARGE_FLAG is null;
alter table MED_ORDER_ITEM modify CMS_CHARGE_FLAG not null;

comment on column MED_ORDER_ITEM.CMS_CHARGE_FLAG is 'CMS value on Charging required';

--//@UNDO
alter table MED_ORDER_ITEM drop column CMS_CHARGE_FLAG;
--//