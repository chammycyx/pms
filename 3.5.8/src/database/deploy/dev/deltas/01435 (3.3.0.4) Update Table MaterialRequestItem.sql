alter table MATERIAL_REQUEST_ITEM drop column NUM_OF_DOSE;

--//@UNDO
alter table MATERIAL_REQUEST_ITEM add NUM_OF_DOSE number(10) not null;
--//
