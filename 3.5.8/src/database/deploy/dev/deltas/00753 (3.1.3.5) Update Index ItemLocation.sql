create index I_ITEM_LOCATION_02 on ITEM_LOCATION (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE) tablespace PMS_INDX_01 online;
alter table ITEM_LOCATION drop constraint UI_ITEM_LOCATION_01;

--//@UNDO
alter table ITEM_LOCATION add constraint UI_ITEM_LOCATION_01 unique (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE, BIN_NUM) using index tablespace PMS_INDX_01 online;
drop index I_ITEM_LOCATION_02;
--//
