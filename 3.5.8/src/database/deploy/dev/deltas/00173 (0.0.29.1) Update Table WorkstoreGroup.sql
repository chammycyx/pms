alter table WORKSTORE_GROUP add INST_CODE varchar2(3);

update WORKSTORE_GROUP set INST_CODE = WORKSTORE_GROUP_CODE;
update WORKSTORE_GROUP set INST_CODE = 'QEH' where WORKSTORE_GROUP_CODE='QEE';

alter table WORKSTORE_GROUP modify INST_CODE not null;

alter table WORKSTORE_GROUP add constraint FK_WORKSTORE_GROUP_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

create index FK_WORKSTORE_GROUP_01 on WORKSTORE_GROUP (INST_CODE) tablespace PMS_INDX_01;


--//@UNDO
drop index FK_WORKSTORE_GROUP_01;

alter table WORKSTORE_GROUP drop constraint FK_WORKSTORE_GROUP_01;

alter table WORKSTORE_GROUP drop column INST_CODE;
--//