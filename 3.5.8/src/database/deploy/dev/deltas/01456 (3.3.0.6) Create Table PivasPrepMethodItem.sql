create table PIVAS_PREP_METHOD_ITEM (
    ID number(19) not null,
    DRUG_ITEM_CODE varchar2(10) not null,
    DRUG_MANUF_CODE varchar2(4) not null,
    DRUG_BATCH_NUM varchar2(20) not null,
    DRUG_EXPIRY_DATE date not null,
    SOLVENT_ITEM_CODE varchar2(10),
    SOLVENT_MANUF_CODE varchar2(4),
    SOLVENT_BATCH_NUM varchar2(20),
    SOLVENT_EXPIRY_DATE date,
    DRUG_CAL_QTY number(19,4) not null,
    DRUG_DISP_QTY number(19,4) not null,
    PIVAS_PREP_METHOD_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_PREP_METHOD_ITEM primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_PREP_METHOD_ITEM is 'Pivas preparation method item';
comment on column PIVAS_PREP_METHOD_ITEM.ID is 'Pivas preparation method item id';
comment on column PIVAS_PREP_METHOD_ITEM.DRUG_ITEM_CODE is 'Drug item code';
comment on column PIVAS_PREP_METHOD_ITEM.DRUG_MANUF_CODE is 'Drug manufacturer';
comment on column PIVAS_PREP_METHOD_ITEM.DRUG_BATCH_NUM is 'Drug batch number';
comment on column PIVAS_PREP_METHOD_ITEM.DRUG_EXPIRY_DATE is 'Drug expiry date';
comment on column PIVAS_PREP_METHOD_ITEM.SOLVENT_ITEM_CODE is 'Solvent item code';
comment on column PIVAS_PREP_METHOD_ITEM.SOLVENT_MANUF_CODE is 'Solvent manufacturer';
comment on column PIVAS_PREP_METHOD_ITEM.SOLVENT_BATCH_NUM is 'Solvent batch number';
comment on column PIVAS_PREP_METHOD_ITEM.SOLVENT_EXPIRY_DATE is 'Solvent expiry date';
comment on column PIVAS_PREP_METHOD_ITEM.DRUG_CAL_QTY is 'Drug calculated quantity';
comment on column PIVAS_PREP_METHOD_ITEM.DRUG_DISP_QTY is 'Drug dispense quantity';
comment on column PIVAS_PREP_METHOD_ITEM.PIVAS_PREP_METHOD_ID is 'Pivas preparation method id';
comment on column PIVAS_PREP_METHOD_ITEM.CREATE_USER is 'Created user';
comment on column PIVAS_PREP_METHOD_ITEM.CREATE_DATE is 'Created date';
comment on column PIVAS_PREP_METHOD_ITEM.UPDATE_USER is 'Last updated user';
comment on column PIVAS_PREP_METHOD_ITEM.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_PREP_METHOD_ITEM.VERSION is 'Version to serve as optimistic lock value';

alter table PIVAS_PREP_METHOD_ITEM add constraint FK_PIVAS_PREP_METHOD_ITEM_01 foreign key (PIVAS_PREP_METHOD_ID) references PIVAS_PREP_METHOD (ID);

create index FK_PIVAS_PREP_METHOD_ITEM_01 on PIVAS_PREP_METHOD_ITEM (PIVAS_PREP_METHOD_ID) tablespace PMS_INDX_01;

--//@UNDO
drop table PIVAS_PREP_METHOD_ITEM;
--//