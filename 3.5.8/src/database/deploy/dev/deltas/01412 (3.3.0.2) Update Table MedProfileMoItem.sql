alter table MED_PROFILE_MO_ITEM add PIVAS_FORMULA_ID number(19);
alter table MED_PROFILE_MO_ITEM add PIVAS_FLAG number(1);

comment on column MED_PROFILE_MO_ITEM.PIVAS_FORMULA_ID is 'PIVAS formula id';
comment on column MED_PROFILE_MO_ITEM.PIVAS_FLAG is 'PIVAS flag';

--//@UNDO
alter table MED_PROFILE_MO_ITEM set unused column PIVAS_FORMULA_ID;
alter table MED_PROFILE_MO_ITEM set unused column PIVAS_FLAG;
--//