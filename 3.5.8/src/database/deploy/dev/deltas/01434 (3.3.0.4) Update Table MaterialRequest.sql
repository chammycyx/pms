alter table MATERIAL_REQUEST add FIRST_DOSE_FLAG number(1) default 0 not null;

comment on column MATERIAL_REQUEST.FIRST_DOSE_FLAG is 'First dose flag';

--//@UNDO
alter table MATERIAL_REQUEST drop column FIRST_DOSE_FLAG;
--//

