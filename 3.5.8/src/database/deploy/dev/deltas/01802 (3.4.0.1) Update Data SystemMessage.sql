update SYSTEM_MESSAGE set SEVERITY_CODE = 'I', MAIN_MSG = 'No allergy, adverse drug reaction or alert record were captured in HA CMS, please clarify with patient accordingly' where MESSAGE_CODE = '0330';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'I', MAIN_MSG = 'No known drug allergy in HA CMS, last verified on #0' where MESSAGE_CODE = '0638';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please check the total quantity / issue quantity against the order' where MESSAGE_CODE = '0988';
update SYSTEM_MESSAGE set MAIN_MSG = 'Drug on Hand service and Drug-Drug Interaction checking between medications on the current prescription and on-hand drugs prescribed at all hospitals are not available.  Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' where MESSAGE_CODE = '0514';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'I', MAIN_MSG = 'Multiple dose conversion cannot be done for this item because doctor must select preparation during prescribing.' where MESSAGE_CODE = '0759';

--//@UNDO
update SYSTEM_MESSAGE set SEVERITY_CODE = 'W', MAIN_MSG = 'This drug [#0] is duplicated with [#1] which exists on [#2].' where MESSAGE_CODE = '0330';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'W', MAIN_MSG = 'This drug [#0] is duplicated.  The same item exists on [#1].' where MESSAGE_CODE = '0638';
update SYSTEM_MESSAGE set MAIN_MSG = 'Frozen item exists.' where MESSAGE_CODE = '0988';
update SYSTEM_MESSAGE set MAIN_MSG = 'Drug on Hand service and Drug-Drug Interaction checking between medications on the current prescription and on-hand drugs prescribed at your local hospital are not available. Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' where MESSAGE_CODE = '0514';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'A', MAIN_MSG = 'IP Manual Profile|Activate Profile|OrderNum [#0]|CaseNum [#1]|' where MESSAGE_CODE = '0759';
--//