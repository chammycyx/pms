alter table PIVAS_PREP modify FREQ_INTERVAL number(19, 4);

--//@UNDO
alter table PIVAS_PREP modify FREQ_INTERVAL number(10);
--//