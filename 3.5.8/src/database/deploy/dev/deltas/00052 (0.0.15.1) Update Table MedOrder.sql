alter table MED_ORDER add UNVET_FLAG number(1) null;
update MED_ORDER set UNVET_FLAG = 0;
alter table MED_ORDER modify UNVET_FLAG default 0 not null;

comment on column MED_ORDER.UNVET_FLAG is 'Unvetted';

--//@UNDO
alter table MED_ORDER drop column UNVET_FLAG;
--//
