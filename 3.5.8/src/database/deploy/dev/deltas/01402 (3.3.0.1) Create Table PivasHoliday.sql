create table PIVAS_HOLIDAY (
    ID number(19) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    HOLIDAY timestamp not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_HOLIDAY primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PIVAS_HOLIDAY unique (WORKSTORE_GROUP_CODE, HOLIDAY) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_HOLIDAY is 'Holiday list of pivas model';
comment on column PIVAS_HOLIDAY.ID is 'Pivas holiday id';
comment on column PIVAS_HOLIDAY.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column PIVAS_HOLIDAY.HOLIDAY is 'Date of public holiday';
comment on column PIVAS_HOLIDAY.CREATE_USER is 'Created user';
comment on column PIVAS_HOLIDAY.CREATE_DATE is 'Created date';
comment on column PIVAS_HOLIDAY.UPDATE_USER is 'Last updated user';
comment on column PIVAS_HOLIDAY.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_HOLIDAY.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table PIVAS_HOLIDAY;
--//