create index I_MED_PROFILE_06 on MED_PROFILE (HOSP_CODE, WARD_CODE, STATUS) local online;
create index I_MED_PROFILE_07 on MED_PROFILE (PAT_HOSP_CODE, STATUS) local online;

--//@UNDO
drop index I_MED_PROFILE_07;
drop index I_MED_PROFILE_06;
--//