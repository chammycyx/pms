alter table MED_PROFILE_PO_ITEM add ADDITIVE_NUM number(10) null;
alter table MED_PROFILE_PO_ITEM add FLUID_NUM number(10) null;

comment on column MED_PROFILE_PO_ITEM.ADDITIVE_NUM is 'Additive Number';
comment on column MED_PROFILE_PO_ITEM.FLUID_NUM is 'Fluid Number';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column ADDITIVE_NUM;
alter table MED_PROFILE_PO_ITEM drop column FLUID_NUM;
--//