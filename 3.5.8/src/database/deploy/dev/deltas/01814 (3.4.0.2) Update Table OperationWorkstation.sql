alter table OPERATION_WORKSTATION modify PRINTER_TYPE varchar2(3);

--//@UNDO
alter table OPERATION_WORKSTATION modify PRINTER_TYPE varchar2(2);
--//