update SYSTEM_MESSAGE set MAIN_MSG = 'Refill interval should be less than max. day for last refill' where MESSAGE_CODE = '0142';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input max. day for last refill' where MESSAGE_CODE = '0367';
update SYSTEM_MESSAGE set MAIN_MSG = 'Max. day for last refill should be greater than zero' where MESSAGE_CODE = '0368';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please specify duration unit' where MESSAGE_CODE = '0444';

update SYSTEM_MESSAGE set MAIN_MSG='This session has timed out for security reasons.  Please re-logon.' where MESSAGE_CODE='0029';
update SYSTEM_MESSAGE set MAIN_MSG='Cannot assemble.  Over [#0] days.' where MESSAGE_CODE='0075';
update SYSTEM_MESSAGE set MAIN_MSG='MO dosage unit was updated in [Drug Master].  The selected prescribe range [#0 - #1] cannot be edited.' where MESSAGE_CODE='0093';
update SYSTEM_MESSAGE set MAIN_MSG='Operation failure.  Control has been taken by workstation [#0].' where MESSAGE_CODE='0104';
update SYSTEM_MESSAGE set MAIN_MSG='Batch issue has been processed.  Only report printing is allowed.' where MESSAGE_CODE='0105';
update SYSTEM_MESSAGE set MAIN_MSG='[#0] is currently selected.  You cannot modify the meal button.' where MESSAGE_CODE='0154';
update SYSTEM_MESSAGE set MAIN_MSG='Invalid item label.  Process Abort.' where MESSAGE_CODE='0175';
update SYSTEM_MESSAGE set MAIN_MSG='Due Date of split [#0] is adjusted to be earlier than or equal to the due date of the previous split.  Please amend the due date manually.' where MESSAGE_CODE='0181';
update SYSTEM_MESSAGE set MAIN_MSG='This refill coupon is expired.  It can only be processed as [Force completed].' where MESSAGE_CODE='0182';
update SYSTEM_MESSAGE set MAIN_MSG='The original prescription is being [#0].  Cannot create new SFI prescription.' where MESSAGE_CODE='0191';
update SYSTEM_MESSAGE set MAIN_MSG='Failed to generate ticket.  Maximum ticket number is 9999.' where MESSAGE_CODE='0361';
update SYSTEM_MESSAGE set MAIN_MSG='Alert!  This patient has allergy / alert information.  Please check.' where MESSAGE_CODE='0386';
update SYSTEM_MESSAGE set MAIN_MSG='Order status is changed.  Please clear screen and retrieve again.' where MESSAGE_CODE='0590';
update SYSTEM_MESSAGE set MAIN_MSG='Order status is changed.  The order has been cancelled.' where MESSAGE_CODE='0604';
update SYSTEM_MESSAGE set MAIN_MSG='Order status is changed.  The order has been vetted.' where MESSAGE_CODE='0605';
update SYSTEM_MESSAGE set MAIN_MSG='Dispensing status is changed.  Please clear screen and retrieve again.' where MESSAGE_CODE='0611';
update SYSTEM_MESSAGE set MAIN_MSG='Item #0 cannot be saved.  Please review and determine the desired option.' where MESSAGE_CODE='0634';

update SYSTEM_MESSAGE set MAIN_MSG='Please select a Force Proceed option.' where MESSAGE_CODE='0092';
update SYSTEM_MESSAGE set MAIN_MSG='Please input the receipt no.' where MESSAGE_CODE='0473';
update SYSTEM_MESSAGE set MAIN_MSG='Please input the Downtime / Standalone System invoice no.' where MESSAGE_CODE='0556';
update SYSTEM_MESSAGE set MAIN_MSG='Invalid input' where MESSAGE_CODE='0034';
update SYSTEM_MESSAGE set MAIN_MSG='FCS service is not available.  Proceed anyway?' where MESSAGE_CODE='0063';

--//@UNDO
--//