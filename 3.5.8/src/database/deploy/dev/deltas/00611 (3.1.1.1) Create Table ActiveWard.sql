create table ACTIVE_WARD (
    PAT_HOSP_CODE varchar2(3) not null,
    PAS_WARD_CODE varchar2(4) not null,
    EFFECTIVE_DATE timestamp not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_ACTIVE_WARD primary key (PAT_HOSP_CODE, PAS_WARD_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  ACTIVE_WARD is 'Active ward from CMS';
comment on column ACTIVE_WARD.PAT_HOSP_CODE is 'Patient hospital code';
comment on column ACTIVE_WARD.PAS_WARD_CODE is 'PAS ward code';
comment on column ACTIVE_WARD.EFFECTIVE_DATE is 'Effective date';
comment on column ACTIVE_WARD.CREATE_USER is 'Created user';
comment on column ACTIVE_WARD.CREATE_DATE is 'Created date';
comment on column ACTIVE_WARD.UPDATE_USER is 'Last updated user';
comment on column ACTIVE_WARD.UPDATE_DATE is 'Last updated date';
comment on column ACTIVE_WARD.VERSION is 'Version to serve as optimistic lock value';
comment on column ACTIVE_WARD.STATUS is 'Record status';


--//@UNDO
drop table ACTIVE_WARD;
--//