alter table PIVAS_PREP drop column AD_HOC_FLAG;

--//@UNDO
alter table PIVAS_PREP add AD_HOC_FLAG number(1) default 0 not null;
--//