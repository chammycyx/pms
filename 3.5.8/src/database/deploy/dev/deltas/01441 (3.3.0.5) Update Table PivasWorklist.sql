alter table PIVAS_WORKLIST modify ADJ_QTY default 0 not null;
alter table PIVAS_WORKLIST modify ISSUE_QTY default 0 not null;
alter table PIVAS_WORKLIST modify MR_ISSUE_QTY default 0 not null;
alter table PIVAS_WORKLIST drop column NEXT_SUPPLY_DATE;

--//@UNDO
alter table PIVAS_WORKLIST modify ADJ_QTY null;
alter table PIVAS_WORKLIST modify ISSUE_QTY null;
alter table PIVAS_WORKLIST modify MR_ISSUE_QTY null;
alter table PIVAS_WORKLIST add NEXT_SUPPLY_DATE timestamp;
--//