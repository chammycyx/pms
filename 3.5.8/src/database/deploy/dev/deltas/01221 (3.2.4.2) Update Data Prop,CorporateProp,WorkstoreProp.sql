update PROP set CACHE_EXPIRE_TIME=0, UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID in (30005, 30008, 30011);
update PROP set CACHE_IN_SESSION_FLAG=1, UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID in (21011);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27015,'report.currentWaitingTime.criteria.order.max','Maximum number of last Rx for average Current Waiting Time calculation','1-100','I',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,27015,'50',27015,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (35016,'medProfile.timer1.interval','Interval (in minutes) for Medication Profile timer 1','5-15','mm',0,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (35016,35016,'5',35016,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (35017,'medProfile.timer2.interval','Interval (in minutes) for Medication Profile timer 2','15-60','mm',0,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (35017,35017,'15',35017,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
update PROP set CACHE_EXPIRE_TIME=60, UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID in (30005, 30008, 30011);
update PROP set CACHE_IN_SESSION_FLAG=0, UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID in (21011);

delete from CORPORATE_PROP where PROP_ID in (35016, 35017);

delete from WORKSTORE_PROP where PROP_ID in (27015);

delete from PROP where ID in (27015, 35016, 35017);
--//