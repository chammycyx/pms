alter table MED_ORDER_ITEM add DISCONTINUE_STATUS varchar2(1);
alter table REFILL_SCHEDULE_ITEM add STATUS varchar2(1);

comment on column MED_ORDER_ITEM.DISCONTINUE_STATUS is 'Discontinue status';
comment on column REFILL_SCHEDULE_ITEM.STATUS is 'Status';

--//@UNDO
alter table MED_ORDER_ITEM drop column DISCONTINUE_STATUS;
alter table REFILL_SCHEDULE_ITEM drop column STATUS;
--//