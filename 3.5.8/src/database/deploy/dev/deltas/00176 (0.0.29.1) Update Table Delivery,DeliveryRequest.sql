alter table DELIVERY add ERROR_FLAG number(1) default 0 null;
update DELIVERY set ERROR_FLAG = 0;
alter table DELIVERY modify ERROR_FLAG not null;
alter table DELIVERY_REQUEST add WORKSTATION_CODE varchar2(4);

comment on column DELIVERY.ERROR_FLAG is 'Error Flag';
comment on column DELIVERY_REQUEST.WORKSTATION_CODE is 'Workstation Code';

--//@UNDO
alter table DELIVERY drop column ERROR_FLAG;
alter table DELIVERY_REQUEST drop column WORKSTATION_CODE;
--//