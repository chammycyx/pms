insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (15005,'check.fastQueue.issueWindowNum','Issue window number of fast queue for Checking','0-20','I',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,15005,'0',15005,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (15006,'check.fastQueue.postVetItem.allowCheck','Allow Checking vetted / picked / assembled items of fast queue orders','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,15006,'Y',15006,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

update PROP set DESCRIPTION='Enable auto Ticket Generation in One Stop Entry', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=22012;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (22019,'oneStop.ticket.ticketNum.max','Maximum ticket number for One Stop Entry','9999','I',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (22019,22019,'9999',22019,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

update PROP set DESCRIPTION='Current ticket number of Ticket Generation queue 1', VALIDATION='1-4999', MAINT_SCREEN='-', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30005;
update PROP set DESCRIPTION='Maximum ticket number of Ticket Generation queue 1', VALIDATION='1-4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30006;
update PROP set DESCRIPTION='Minimum ticket number of Ticket Generation queue 1', VALIDATION='1-4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30007;

update PROP set DESCRIPTION='Current ticket number of Ticket Generation queue 2', VALIDATION='1-4999', MAINT_SCREEN='-', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30008;
update PROP set DESCRIPTION='Maximum ticket number of Ticket Generation queue 2', VALIDATION='1-4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30009;
update PROP set DESCRIPTION='Minimum ticket number of Ticket Generation queue 2', VALIDATION='1-4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30010;

update PROP set NAME='ticketGen.fastQueue.ticket.ticketNum', DESCRIPTION='Current ticket number of Ticket Generation fast queue', VALIDATION='1-4999', MAINT_SCREEN='-', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30011;
update WORKSTORE_PROP set VALUE='3001', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30011;

update PROP set NAME='ticketGen.fastQueue.ticket.ticketNum.max', DESCRIPTION='Maximum ticket number of Ticket Generation fast queue', VALIDATION='1-4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30012;
update WORKSTORE_PROP set VALUE='4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30012;

update PROP set NAME='ticketGen.fastQueue.ticket.ticketNum.min', DESCRIPTION='Minimum ticket number of Ticket Generation fast queue', VALIDATION='1-4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30013;
update WORKSTORE_PROP set VALUE='3001', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30013;

update PROP set NAME='ticketGen.fastQueue.name', DESCRIPTION='Name of Ticket Generation fast queue', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30017;
update WORKSTORE_PROP set VALUE='Fast Queue', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30017;

update PROP set DESCRIPTION='Number of tickets printed for batch Ticket Generation', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30020;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (30021,'ticketGen.queue2.enabled','Enable Ticket Generation queue 2','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,30021,'N',30021,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

update WORKSTORE_PROP t1
set VALUE='Y', UPDATE_DATE=sysdate
where PROP_ID=30021 
and exists (     select 1 
                from PMS.WORKSTORE_PROP t2
                where t1.HOSP_CODE=t2.HOSP_CODE
                and t1.WORKSTORE_CODE=t2.WORKSTORE_CODE
                and PROP_ID=30010
                and VALUE <> 0
            );

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (30022,'ticketGen.fastQueue.enabled','Enable Ticket Generation fast queue','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,30022,'N',30022,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (30023,'ticketGen.fastQueue.moItem.max','Maximum number of items per Rx allowed for Ticket Generation fast queue','1-5','I',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,30023,'1',30023,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP set DESCRIPTION='Enable auto Ticket Generation', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=22012;

update PROP set DESCRIPTION='Current ticket number for Ticket Generation queue 1', VALIDATION='0-9999', MAINT_SCREEN='S', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30005;
update PROP set DESCRIPTION='Maximum ticket number for Ticket Generation queue 1', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30006;
update PROP set DESCRIPTION='Minimum ticket number for Ticket Generation queue 1', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30007;

update PROP set DESCRIPTION='Current ticket number for Ticket Generation queue 2', VALIDATION='0-9999', MAINT_SCREEN='S', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30008;
update PROP set DESCRIPTION='Maximum ticket number for Ticket Generation queue 2', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30009;
update PROP set DESCRIPTION='Minimum ticket number for Ticket Generation queue 2', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30010;

update PROP set NAME='ticketGen.queue3.ticket.ticketNum', DESCRIPTION='Current ticket number for Ticket Generation queue 3', VALIDATION='0-9999', MAINT_SCREEN='S', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30011;
update WORKSTORE_PROP set VALUE='0', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30011;

update PROP set NAME='ticketGen.queue3.ticket.ticketNum.max', DESCRIPTION='Maximum ticket number for Ticket Generation queue 3', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30012;
update WORKSTORE_PROP set VALUE='0', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30012;

update PROP set NAME='ticketGen.queue3.ticket.ticketNum.min', DESCRIPTION='Minimum ticket number for Ticket Generation queue 3', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30013;
update WORKSTORE_PROP set VALUE='0', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30013;

update PROP set NAME='ticketGen.queue3.name', DESCRIPTION='Name of Ticket Generation queue 3', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30017;
update WORKSTORE_PROP set VALUE='SFI Patient', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=30017;

update PROP set DESCRIPTION='Number of ticket(s) printed for batch Ticket Generation', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=30020;

delete from CORPORATE_PROP where PROP_ID=22019;
delete from WORKSTORE_PROP where PROP_ID in (15005, 15006, 30021, 30022, 30023);
delete from PROP where ID in (15005, 15006, 22019, 30021, 30022, 30023);
--//