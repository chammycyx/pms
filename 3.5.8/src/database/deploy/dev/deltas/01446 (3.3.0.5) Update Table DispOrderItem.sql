alter table DISP_ORDER_ITEM add PIVAS_FLAG number(1);

comment on column DISP_ORDER_ITEM.PIVAS_FLAG is 'PIVAS flag';

--//@UNDO
alter table DISP_ORDER_ITEM set unused column PIVAS_FLAG;
--//