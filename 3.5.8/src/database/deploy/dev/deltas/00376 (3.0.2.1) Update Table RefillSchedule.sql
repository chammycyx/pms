alter table REFILL_SCHEDULE add RE_DISP_FLAG number(1) default 0 null;
update REFILL_SCHEDULE set RE_DISP_FLAG = 0;

alter table REFILL_SCHEDULE modify RE_DISP_FLAG not null;
comment on column REFILL_SCHEDULE.RE_DISP_FLAG is 'Check if the Refill Schedule is re-generated';
--//@UNDO
alter table REFILL_SCHEDULE drop column RE_DISP_FLAG;
--//