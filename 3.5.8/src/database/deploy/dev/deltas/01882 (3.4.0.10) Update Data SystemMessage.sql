update SYSTEM_MESSAGE set MAIN_MSG = 'Printer Connection must be [Yes] as the following workstations have been redirected to this printer.  To set the printer connection to [No] or [Private], all fast queue printer redirection from the following workstations will be cancelled.' where MESSAGE_CODE = '1010';
update SYSTEM_MESSAGE set MAIN_MSG = 'Multiple Dose Conversion Maintenance is not allowed for single order line.  Please use Dosage Conversion Maintenance for single order line.' where MESSAGE_CODE = '1011';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1012',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'This computer has not been set up with merge formula printer.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1012';

update SYSTEM_MESSAGE set MAIN_MSG = 'Fast queue printer must be checked as the following workstations have been redirected to this printer.  All fast queue printer redirection from the following workstations will be cancelled.' where MESSAGE_CODE = '1010';
update SYSTEM_MESSAGE set MAIN_MSG = 'Multiple dose conversion maintenance is not allowed for single line order.  Please use dosage conversion maintenance.' where MESSAGE_CODE = '1011';
--//