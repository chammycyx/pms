create table INSTITUTION (
    INST_CODE varchar2(3) not null,
    CLUSTER_CODE varchar2(3) not null,
    GOPC_FLAG number(1) not null,
    PCU_INST_CODE varchar2(3) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_INSTITUTION primary key (INST_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  INSTITUTION is 'Institutions';
comment on column INSTITUTION.INST_CODE is 'Institution code';
comment on column INSTITUTION.CLUSTER_CODE is 'Hospital cluster code';
comment on column INSTITUTION.GOPC_FLAG is 'GOPC Flag';
comment on column INSTITUTION.PCU_INST_CODE is 'PCU Institution code';
comment on column INSTITUTION.STATUS is 'Record status';
comment on column INSTITUTION.CREATE_USER is 'Created user';
comment on column INSTITUTION.CREATE_DATE is 'Created date';
comment on column INSTITUTION.UPDATE_USER is 'Last updated user';
comment on column INSTITUTION.UPDATE_DATE is 'Last updated date';
comment on column INSTITUTION.VERSION is 'Version to serve as optimistic lock value';

alter table INSTITUTION add constraint FK_INSTITUTION_01 foreign key (CLUSTER_CODE) references HOSPITAL_CLUSTER (CLUSTER_CODE);

create index FK_INSTITUTION_01 on INSTITUTION (CLUSTER_CODE) tablespace PMS_INDX_01;


--//@UNDO
drop index FK_INSTITUTION_01;

alter table INSTITUTION drop constraint FK_INSTITUTION_01;

drop table INSTITUTION;
--//