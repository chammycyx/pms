insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1010',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'Fast queue printer must be checked as the following workstations have been redirected to this printer.  All fast queue printer redirection from the following workstations will be cancelled.','#0',sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1010';
--//