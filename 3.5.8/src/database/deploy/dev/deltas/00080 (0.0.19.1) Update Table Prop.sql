alter table PROP add DEF_VALUE varchar2(1000) null;

update PROP set DEF_VALUE = 'N' where ID = 10012;
update PROP set DEF_VALUE = 'N' where ID = 16003;
update PROP set DEF_VALUE = '3' where ID = 25001;
update PROP set DEF_VALUE = 'N' where ID = 36002;

comment on column PROP.DEF_VALUE is 'Default value';

--//@UNDO
alter table PROP drop column DEF_VALUE;
--//