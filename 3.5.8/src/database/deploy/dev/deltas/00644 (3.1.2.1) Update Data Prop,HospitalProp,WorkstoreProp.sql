insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (16004,'checkIssue.patient.pas.patient.enabled','Enable PAS Patient enquiry for Checking and Issuing','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,16004,'Y',16004,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27012,'report.mpOrderPendingForm.remark','Information on IPMOE Order Pending Form','r3,c100','T',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'I',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,27012,'Should you need further clarification or assistance, please call pharmacy at ________.',27012,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (35012,'medProfile.checkMsgSeqNum.rollout','[to be obsolete] IPMOE rollout: Checking message sequence number','Y,N','B',0,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,35012,'N',HOSP_CODE,35012,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (35013,'medProfile.sendMsgSeqNum.rollout','[to be obsolete] IPMOE rollout: Sending message sequence number','Y,N','B',0,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,35013,'N',HOSP_CODE,35013,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
delete from WORKSTORE_PROP where PROP_ID in (16004, 27012);

delete from HOSPITAL_PROP where PROP_ID in (35012, 35013);

delete from PROP where ID in (16004, 27012, 35012, 35013);
--//