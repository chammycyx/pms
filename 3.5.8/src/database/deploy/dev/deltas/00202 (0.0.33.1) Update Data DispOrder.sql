update DISP_ORDER set DAY_END_REMARK = '[' || replace(DAY_END_REMARK,',','][') || ']' where DAY_END_REMARK is not null;

--//@UNDO
update DISP_ORDER set DAY_END_REMARK = replace(day_end_remark,'][',',') where DAY_END_REMARK is not null;
update DISP_ORDER set DAY_END_REMARK = replace(day_end_remark,'[','') where DAY_END_REMARK is not null;
update DISP_ORDER set DAY_END_REMARK = replace(DAY_END_REMARK,']','') where DAY_END_REMARK is not null;
--//
