alter table MED_PROFILE_PO_ITEM add LAST_ADJ_QTY number(19,4) null;

comment on column MED_PROFILE_PO_ITEM.LAST_ADJ_QTY is 'Last adjusted quantity';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column LAST_ADJ_QTY;
--//
