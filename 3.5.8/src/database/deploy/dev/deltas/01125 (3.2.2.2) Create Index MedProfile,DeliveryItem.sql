create index I_MED_PROFILE_08 on MED_PROFILE (HOSP_CODE, STATUS, PRIVATE_FLAG) local online;

create index I_DELIVERY_ITEM_01 on DELIVERY_ITEM (MED_PROFILE_ID, STATUS, CHARGE_QTY) local online;


--//@UNDO
drop index I_MED_PROFILE_08;

drop index I_DELIVERY_ITEM_01;
--//