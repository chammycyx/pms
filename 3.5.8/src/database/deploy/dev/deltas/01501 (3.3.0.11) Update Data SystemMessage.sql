insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0983',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Suspended drug item.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'Due date is expired' where MESSAGE_CODE = '0934';
update SYSTEM_MESSAGE set MAIN_MSG = 'Drug expired before the preparation.' where MESSAGE_CODE = '0927';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input solvent.' where MESSAGE_CODE = '0812';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0983';

update SYSTEM_MESSAGE set MAIN_MSG = 'Due date should be later than or equal to current date' where MESSAGE_CODE = '0934';
update SYSTEM_MESSAGE set MAIN_MSG = 'Expiry date should be later than preparation expiry date.' where MESSAGE_CODE = '0927';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input solvent code.' where MESSAGE_CODE = '0812';
--//