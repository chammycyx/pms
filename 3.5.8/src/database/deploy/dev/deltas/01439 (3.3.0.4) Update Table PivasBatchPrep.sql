alter table PIVAS_BATCH_PREP add LABEL_XML clob;
comment on column PIVAS_BATCH_PREP.LABEL_XML is 'Label xml';

update PIVAS_BATCH_PREP 
set LABEL_XML = concat (concat(concat('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><LabelContainer><pivasWorksheet>', SUBSTR(Pivas_Worksheet_Xml, 72, (LENGTH(Pivas_Worksheet_Xml)-88))), '</pivasWorksheet>'), 
        concat(concat('<pivasProductLabel>', SUBSTR(Pivas_Product_Label_Xml, 75, (LENGTH(Pivas_Product_Label_Xml)-94))), '</pivasProductLabel></LabelContainer>') );
        
        
alter table PIVAS_BATCH_PREP drop column PIVAS_WORKSHEET_XML;
alter table PIVAS_BATCH_PREP drop column PIVAS_PRODUCT_LABEL_XML;

--//@UNDO
alter table PIVAS_BATCH_PREP drop column LABEL_XML;

alter table PIVAS_BATCH_PREP add PIVAS_WORKSHEET_XML clob;
comment on column PIVAS_BATCH_PREP.PIVAS_WORKSHEET_XML is 'Pivas worksheet xml';

alter table PIVAS_BATCH_PREP add PIVAS_PRODUCT_LABEL_XML clob;
comment on column PIVAS_BATCH_PREP.PIVAS_PRODUCT_LABEL_XML is 'Pivas product label xml';
--//