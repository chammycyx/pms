update REFILL_SCHEDULE rs
set rs.STATUS = 'X'
where exists (select 1 from PHARM_ORDER po where po.ID = rs.PHARM_ORDER_ID and po.STATUS in ('D', 'X'))
and rs.STATUS <> 'X';

update REFILL_SCHEDULE rs
set rs.MO_ORG_ITEM_NUM = 
(select distinct moi.ORG_ITEM_NUM  
from MED_ORDER_ITEM moi, PHARM_ORDER_ITEM poi, REFILL_SCHEDULE_ITEM rsi
where moi.ID = poi.MED_ORDER_ITEM_ID and poi.ID = rsi.PHARM_ORDER_ITEM_ID and rs.ID = rsi.REFILL_SCHEDULE_ID) 
where rs.MO_ORG_ITEM_NUM is null
and rs.DOC_TYPE = 'F';

update REFILL_SCHEDULE 
set TRIM_REFILL_COUPON_NUM = substr(TRIM_REFILL_COUPON_NUM, 1, 15)
where length(TRIM_REFILL_COUPON_NUM) > 15 
and DOC_TYPE = 'F';

--//@UNDO
--//