update SYSTEM_MESSAGE set MAIN_MSG = 'The concentration after reconstitution is out of the 95% - 105% calculated range.  Confirm to proceed?', SEVERITY_CODE = 'Q' where MESSAGE_CODE = '0823';
update SYSTEM_MESSAGE set MAIN_MSG = 'PIVAS due order not found.  Invalid supply schedule.' where MESSAGE_CODE = '0914';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please enter patient''s date of birth before TPN order entry.', SEVERITY_CODE = 'W' where MESSAGE_CODE = '0976';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'The concentration after reconstitution is out of the range 95% - 105% of the default calculated value.', SEVERITY_CODE = 'W' where MESSAGE_CODE = '0823';
update SYSTEM_MESSAGE set MAIN_MSG = 'Generation time should be smaller than next refill schedule.' where MESSAGE_CODE = '0914';
update SYSTEM_MESSAGE set MAIN_MSG = 'TPN request update has not been saved.  Are you sure to quit without saving the profile?', SEVERITY_CODE = 'Q' where MESSAGE_CODE = '0976';
--//