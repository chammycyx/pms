alter table PHARM_ORDER_ITEM add ROUTE_CODE varchar2(7) null;
alter table PHARM_ORDER_ITEM add ROUTE_DESC varchar2(20) null;

comment on column PHARM_ORDER_ITEM.ROUTE_CODE is 'Route code';
comment on column PHARM_ORDER_ITEM.ROUTE_DESC is 'Route description';

--//@UNDO
alter table PHARM_ORDER_ITEM drop column ROUTE_CODE;
alter table PHARM_ORDER_ITEM drop column ROUTE_DESC;
--//