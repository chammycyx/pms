update PROP set DESCRIPTION = 'PIVAS drug dispense quantity for total deduction' where ID = '39007';

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (39018,'pivas.drugCalQty.roundingMode','Rounding mode of PIVAS drug calculated quantity for stock deduction','2','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,39018,'2',HOSP_CODE,39018,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
delete from HOSPITAL_PROP where PROP_ID in (39018);
delete from PROP where ID in (39018);

update PROP set DESCRIPTION = 'PIVAS drug dispense quantity for stock deduction' where ID = '39007';
--//