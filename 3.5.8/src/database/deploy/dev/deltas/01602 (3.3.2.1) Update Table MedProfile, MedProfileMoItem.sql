alter table MED_PROFILE add SHOW_TRANSFER_ALERT_FLAG number(1);
alter table MED_PROFILE_MO_ITEM add FROZEN_FLAG number(1);
comment on column MED_PROFILE.SHOW_TRANSFER_ALERT_FLAG is 'Show alert once for inactive items marked due to ward transfer activity';
comment on column MED_PROFILE_MO_ITEM.FROZEN_FLAG is 'Frozen status by ward transfer';

--//@UNDO
alter table MED_PROFILE set unused column SHOW_TRANSFER_ALERT_FLAG;
alter table MED_PROFILE_MO_ITEM set unused column FROZEN_FLAG;
--//