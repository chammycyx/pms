alter table REFILL_SCHEDULE add MO_ORG_ITEM_NUM number(10);

comment on column REFILL_SCHEDULE.MO_ORG_ITEM_NUM is 'MO original item number';

--//@UNDO
alter table REFILL_SCHEDULE drop column MO_ORG_ITEM_NUM;
--//