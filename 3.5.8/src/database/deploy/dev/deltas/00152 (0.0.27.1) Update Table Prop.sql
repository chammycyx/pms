alter table PROP add SYNC_FLAG number(1) null;

comment on column PROP.SYNC_FLAG is 'Synchronize flag';

update PROP set SYNC_FLAG=0;
update PROP set SYNC_FLAG=1 where ID in (14002,14004,14005,14006,14007,14008,14009,14010,14027,18003,18004,20004,20007,20008,32025,32026);

alter table PROP modify SYNC_FLAG not null;

--//@UNDO
alter table PROP drop column SYNC_FLAG;
--//