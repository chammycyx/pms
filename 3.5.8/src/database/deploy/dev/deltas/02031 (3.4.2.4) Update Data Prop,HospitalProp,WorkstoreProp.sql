insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (20041,'label.pivasBatchMergeFormulaProductLabel.drugList.visible','Print drug list on PIVAS batch merge product label','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,20041,'N',HOSP_CODE,20041,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37020,'delivery.tabPage','Entry tab page of Due Order Label Generation','Main,Schedule Template','S',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'I',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,37020,'Main',37020,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37021,'delivery.schedule.template','Template name of schedule template in Due Order Label Generation',null,'S',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,37021,null,37021,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');


--//@UNDO
delete from WORKSTORE_PROP where PROP_ID in (37020, 37021);
delete from HOSPITAL_PROP where PROP_ID in (20041);

delete from PROP where ID in (20041, 37020, 37021);

--//