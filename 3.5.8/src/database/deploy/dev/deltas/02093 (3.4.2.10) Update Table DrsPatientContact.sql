alter table DRS_PATIENT_CONTACT drop constraint UI_DRS_PATIENT_CONTACT_01;

--//@UNDO
alter table DRS_PATIENT_CONTACT add constraint UI_DRS_PATIENT_CONTACT_01 unique (DESCRIPTION, DRS_PATIENT_ID) using index tablespace PMS_INDX_01 online;
--//