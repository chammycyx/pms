update PROP 
set REMINDER_MESSAGE='HA patient alert profile enquiry service is not available.  Therefore, the system cannot perform Allergy, ADR and G6PD Deficiency Contraindication checking.  Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' 
,UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate
where ID=10001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG,REMINDER_MESSAGE) 
values (10015,'alert.ehr.profile.enabled','Enable eHR Alert Enquiry screen','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0, 'eHR patient alert profile enquiry service is not available. Please exercise your professional judgement during the downtime period and contact IT
system support for assistance.');

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (10015,10015,'Y',10015,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32052,'vetting.report.infusionInstructionSummary.print','Print Order and Dispensing Summary for IPMOE Discharge Rx in OP Vetting','Y,N','B',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,32052,'Y',32052,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP 
set REMINDER_MESSAGE='Patient alert profile enquiry service is not available.  Therefore, the system cannot perform Allergy, ADR and G6PD Deficiency Contraindication checking.  Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' 
,UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate
where ID=10001;

delete from CORPORATE_PROP where PROP_ID in (10015);
delete from WORKSTORE_PROP where PROP_ID in (32052);

delete from PROP where ID in (10015, 32052);
--//