alter table PIVAS_BATCH_PREP add constraint FK_PIVAS_BATCH_PREP_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);
create index FK_PIVAS_BATCH_PREP_01 on PIVAS_BATCH_PREP (HOSP_CODE) tablespace PMS_INDX_01;

--//@UNDO
alter table PIVAS_BATCH_PREP drop constraint FK_PIVAS_BATCH_PREP_01;
drop index FK_PIVAS_BATCH_PREP_01;
--//