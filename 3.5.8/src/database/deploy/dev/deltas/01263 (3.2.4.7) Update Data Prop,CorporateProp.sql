update prop set name = 'report.waitingTime.audit.criteria.month.duration', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where id=27021;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (14030,'charging.sfi.dischargePrivatePatient.duration','Duration (in days) of discharge-private patients lookup in IP SFI Charging Module','1-14','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (14030,14030,'14',14030,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (35018,'medProfile.timer.fcs.enabled','Enable FCS clearance checking in Medication Profile timer','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (35018,35018,'Y',35018,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
delete from CORPORATE_PROP where PROP_ID in (14030,35018);
delete from PROP where ID in (14030,35018);
--//
