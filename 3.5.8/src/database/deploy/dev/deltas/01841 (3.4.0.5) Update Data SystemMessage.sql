update SYSTEM_MESSAGE set MAIN_MSG = 'Dosage conversion cannot be done for this item because doctor must select preparation during prescribing.' where MESSAGE_CODE = '0759';
update SYSTEM_MESSAGE set MAIN_MSG = 'No allergy, adverse drug reaction or alert record were captured in CMS Alert, please clarify with patient accordingly' where MESSAGE_CODE = '0330';
update SYSTEM_MESSAGE set MAIN_MSG = 'Duplicate conversion sets [#0], [#1].' where MESSAGE_CODE = '0328';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Multiple dose conversion cannot be done for this item because doctor must select preparation during prescribing.' where MESSAGE_CODE = '0759';
update SYSTEM_MESSAGE set MAIN_MSG = 'No allergy, adverse drug reaction or alert record were captured in HA CMS, please clarify with patient accordingly' where MESSAGE_CODE = '0330';
update SYSTEM_MESSAGE set MAIN_MSG = 'Duplicate conversion set [#0].' where MESSAGE_CODE = '0328';
--//