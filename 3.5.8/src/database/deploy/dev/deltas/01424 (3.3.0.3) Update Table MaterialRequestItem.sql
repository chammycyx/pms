alter table MATERIAL_REQUEST_ITEM add BASE_UNIT varchar2(4);
alter table MATERIAL_REQUEST_ITEM add ITEM_CODE varchar2(6) not null;
alter table MATERIAL_REQUEST_ITEM add NUM_OF_DOSE number(10) not null;
alter table MATERIAL_REQUEST_ITEM modify MATERIAL_REQUEST_ID not null;

comment on column MATERIAL_REQUEST_ITEM.BASE_UNIT is 'Base unit';
comment on column MATERIAL_REQUEST_ITEM.ITEM_CODE is 'Item code';

--//@UNDO
alter table MATERIAL_REQUEST_ITEM drop column BASE_UNIT;
alter table MATERIAL_REQUEST_ITEM drop column ITEM_CODE;
alter table MATERIAL_REQUEST_ITEM drop column NUM_OF_DOSE;
--//