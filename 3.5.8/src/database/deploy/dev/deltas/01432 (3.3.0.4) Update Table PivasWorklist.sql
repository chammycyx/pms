alter table PIVAS_WORKLIST modify EXPIRY_DATE null;
alter table PIVAS_WORKLIST add NEXT_SUPPLY_DATE timestamp;
alter table PIVAS_WORKLIST add MR_ISSUE_QTY number(19, 4);

comment on column PIVAS_WORKLIST.NEXT_SUPPLY_DATE is 'Next supply date';
comment on column PIVAS_WORKLIST.MR_ISSUE_QTY is 'Material request issue quantity';

--//@UNDO
alter table PIVAS_WORKLIST drop column NEXT_SUPPLY_DATE;
alter table PIVAS_WORKLIST drop column MR_ISSUE_QTY;
--//