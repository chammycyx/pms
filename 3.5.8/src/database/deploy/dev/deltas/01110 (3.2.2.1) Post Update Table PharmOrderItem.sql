alter table PHARM_ORDER_ITEM add CHEST_FLAG number(1) default 0 not null;

comment on column PHARM_ORDER_ITEM.CHEST_FLAG is 'Indicate the drug item is chest item';

update PHARM_ORDER_ITEM poi set poi.CHEST_FLAG = 1 
where poi.PHARM_ORDER_ID in (select po.ID from PHARM_ORDER po where po.CHEST_FLAG = 1
and po.CREATE_DATE between to_date('20120101', 'yyyymmdd') and to_date('20160101', 'yyyymmdd'));

--//@UNDO
alter table PHARM_ORDER_ITEM drop column CHEST_FLAG;
--//