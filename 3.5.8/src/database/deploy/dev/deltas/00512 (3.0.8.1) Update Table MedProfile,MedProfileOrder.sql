alter table MED_PROFILE add CANCEL_DISCHARGE_DATE timestamp;
alter table MED_PROFILE add CONFIRM_CANCEL_DISCHARGE_DATE timestamp;
alter table MED_PROFILE add CONFIRM_CANCEL_DISCHARGE_USER varchar2(12);

alter table MED_PROFILE_ORDER add DISCHARGE_DATE timestamp;
alter table MED_PROFILE_ORDER add CANCEL_DISCHARGE_DATE timestamp;


comment on column MED_PROFILE.CANCEL_DISCHARGE_DATE is 'Cancel Discharge Date';
comment on column MED_PROFILE.CONFIRM_CANCEL_DISCHARGE_DATE is 'Confirm Cancel Discharge Date';
comment on column MED_PROFILE.CONFIRM_CANCEL_DISCHARGE_USER is 'Confirm Cancel Discharge User';

comment on column MED_PROFILE_ORDER.DISCHARGE_DATE is 'Discharge Date';
comment on column MED_PROFILE_ORDER.CANCEL_DISCHARGE_DATE is 'Cancel Discharge Date';


--//@UNDO
alter table MED_PROFILE drop column CANCEL_DISCHARGE_DATE;
alter table MED_PROFILE drop column CONFIRM_CANCEL_DISCHARGE_DATE;
alter table MED_PROFILE drop column CONFIRM_CANCEL_DISCHARGE_USER;
alter table MED_PROFILE_ORDER drop column DISCHARGE_DATE;
alter table MED_PROFILE_ORDER drop column CANCEL_DISCHARGE_DATE;
--//