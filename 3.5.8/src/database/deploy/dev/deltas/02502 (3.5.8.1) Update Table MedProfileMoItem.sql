alter table MED_PROFILE_MO_ITEM add CHEMO_INFO_XML clob;
alter table MED_PROFILE_MO_ITEM add CYCLE number(10);
alter table MED_PROFILE_MO_ITEM add CYCLE_START_DATE timestamp;
alter table MED_PROFILE_MO_ITEM add CHEMO_CATEGORY varchar2(1);
alter table MED_PROFILE_MO_ITEM add CMM_FORMULA_CODE varchar2(20);

comment on column MED_PROFILE_MO_ITEM.CHEMO_INFO_XML is 'Chemo info xml';
comment on column MED_PROFILE_MO_ITEM.CYCLE is 'Chemo treatment cycle';
comment on column MED_PROFILE_MO_ITEM.CYCLE_START_DATE is 'Chemo treatment cycle start date';
comment on column MED_PROFILE_MO_ITEM.CHEMO_CATEGORY is 'Chemo category';
comment on column MED_PROFILE_MO_ITEM.CMM_FORMULA_CODE is 'Cmm formula code';

--//@UNDO
alter table MED_PROFILE_MO_ITEM set unused column CHEMO_INFO_XML;
alter table MED_PROFILE_MO_ITEM set unused column CYCLE;
alter table MED_PROFILE_MO_ITEM set unused column CYCLE_START_DATE;
alter table MED_PROFILE_MO_ITEM set unused column CHEMO_CATEGORY;
alter table MED_PROFILE_MO_ITEM set unused column CMM_FORMULA_CODE;
--//