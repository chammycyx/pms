insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0972',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Please check the DH hardcopy prescription for patient''s allergy / ADR / alert information.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0973',null,'en_US',null,1007,'A',null,null,0,null,null,null,0,sysdate,null,null,null,'Download Transaction File|Download Daily Transaction File|FileName[#0]|ElapseTime[#1]|',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0974',null,'en_US',null,1007,'A',null,null,0,null,null,null,0,sysdate,null,null,null,'Download Transaction File|Download Monthly Archive File|FileName[#0]|ElapseTime[#1]|',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'Multiple Purchase Requests exist.  Please go to Purchase Request tab-page for detail.' where MESSAGE_CODE = '0779';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0972';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0973';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0974';

update SYSTEM_MESSAGE set MAIN_MSG = 'Multiple Purchase Request exist.  Please go to Purchase Request tab-page for detail.' where MESSAGE_CODE = '0779';
--//