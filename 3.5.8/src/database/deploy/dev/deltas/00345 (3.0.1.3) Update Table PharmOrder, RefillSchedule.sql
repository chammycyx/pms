alter table REFILL_SCHEDULE add REFILL_INTERVAL number(10) null;
alter table REFILL_SCHEDULE add REFILL_INTERVAL_LAST number(10) null;

comment on column REFILL_SCHEDULE.REFILL_INTERVAL is 'Current refill interval';
comment on column REFILL_SCHEDULE.REFILL_INTERVAL_LAST is 'Last refill interval';

alter table PHARM_ORDER drop column REFILL_INTERVAL;
alter table PHARM_ORDER drop column REFILL_INTERVAL_LAST;

update REFILL_SCHEDULE rs
set (rs.REFILL_INTERVAL, rs.REFILL_INTERVAL_LAST) = (select rsi.REFILL_DURATION, rsi.REFILL_DURATION from REFILL_SCHEDULE_ITEM rsi where rsi.REFILL_SCHEDULE_ID = rs.id group by rsi.REFILL_DURATION, rsi.REFILL_SCHEDULE_ID) 
where rs.REFILL_NUM = 1 and rs.DOC_TYPE = 'S';

update REFILL_SCHEDULE rs1
set (rs1.REFILL_INTERVAL, rs1.REFILL_INTERVAL_LAST) = (select rs.REFILL_INTERVAL, rs.REFILL_INTERVAL_LAST from REFILL_SCHEDULE rs where rs.PHARM_ORDER_ID = rs1.PHARM_ORDER_ID and rs.DOC_TYPE = 'S' and rs.GROUP_NUM = rs1.GROUP_NUM and rs.REFILL_NUM = 1 )
where rs1.DOC_TYPE = 'S' and rs1.REFILL_NUM > 1;

--//@UNDO
alter table REFILL_SCHEDULE drop column REFILL_INTERVAL;
alter table REFILL_SCHEDULE drop column REFILL_INTERVAL_LAST;

alter table PHARM_ORDER add REFILL_INTERVAL number(10) null;
alter table PHARM_ORDER add REFILL_INTERVAL_LAST number(10) null;

comment on column PHARM_ORDER.REFILL_INTERVAL is 'Current refill interval';
comment on column PHARM_ORDER.REFILL_INTERVAL_LAST is 'Last refill interval';
--//