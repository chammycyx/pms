alter table MED_ORDER_ITEM add DH_RX_ITEM_XML clob null;

comment on column MED_ORDER_ITEM.DH_RX_ITEM_XML is 'DH item XML in DH order';

--//@UNDO
alter table MED_ORDER_ITEM set unused column DH_RX_ITEM_XML;
--//
