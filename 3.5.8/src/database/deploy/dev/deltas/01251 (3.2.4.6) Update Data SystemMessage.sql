insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0777',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'Do you want to delete this profile?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0778',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Label has been generated in the same day.  Profile deletion aborted.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'Uncollected : Contains refrigerated item' where MESSAGE_CODE = '0768';
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected : MOE order is marked as allow modify' where MESSAGE_CODE = '0769';
update SYSTEM_MESSAGE set MAIN_MSG = 'Manual prescription cannot be marked as uncollect' where MESSAGE_CODE = '0770';
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected : Prescription is already marked as uncollected' where MESSAGE_CODE = '0771';
update SYSTEM_MESSAGE set MAIN_MSG = 'SFI invoice (Invoice No.: #0) associated to this Purchase Request will be voided.  Please confirm.' where MESSAGE_CODE = '0773';
--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0777';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0778';

update SYSTEM_MESSAGE set MAIN_MSG = 'Uncollected - Contains refrigerated item' where MESSAGE_CODE = '0768';
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected - MOE order is marked as allow modify' where MESSAGE_CODE = '0769';
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected - Manual prescription has passed dayend' where MESSAGE_CODE = '0770';
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected - Prescription is already marked as uncollected' where MESSAGE_CODE = '0771';
update SYSTEM_MESSAGE set MAIN_MSG = 'SFI invoice (Invoice No.: #0) associated to this purchase request will be voided.  Please confirm.' where MESSAGE_CODE = '0773';
--//