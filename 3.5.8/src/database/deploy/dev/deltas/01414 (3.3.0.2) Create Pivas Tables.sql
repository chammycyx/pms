create table PIVAS_PREP (
    ID number(19) not null,
    PIVAS_FORMULA_ID number(19) not null,
    PIVAS_FORMULA_METHOD_ID number(19) not null,
    DRUG_KEY number(10) not null,
    PIVAS_DOSAGE_UNIT varchar2(15) not null,
    VOLUME number(19,4) not null,
    PRINT_NAME varchar2(59) not null,
    SHELF_LIFE number(10) not null,
    PIVAS_CONTAINER_ID number(19) not null,
    SPLIT_COUNT number(10) not null,
    DILUENT_ITEM_CODE varchar2(10),
    DILUENT_MANUF_CODE varchar2(4),
    DILUENT_BATCH_NUM varchar2(20),
    DILUENT_EXPIRY_DATE date,
    WARN_CODE1 varchar2(4),
    WARN_CODE2 varchar2(4),
    DUE_DATE timestamp,
    FREQ_INTERVAL number(10) not null,
    SATELLITE_FLAG number(1) default 0 not null,
    SINGLE_DISP_FLAG number(1) default 0 not null,
    LAST_PRINT_DATE timestamp,
    EXTRA_XML clob,
    STATUS varchar2(1) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_PREP primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_PREP is 'Pivas preparation';
comment on column PIVAS_PREP.ID is 'Pivas preparation id';
comment on column PIVAS_PREP.PIVAS_FORMULA_ID is 'Pivas formula method id';
comment on column PIVAS_PREP.PIVAS_FORMULA_METHOD_ID is 'Pivas formula method id';
comment on column PIVAS_PREP.DRUG_KEY is 'Drug key';
comment on column PIVAS_PREP.PIVAS_DOSAGE_UNIT is 'Pivas dosage unit';
comment on column PIVAS_PREP.VOLUME is 'Volume';
comment on column PIVAS_PREP.PRINT_NAME is 'Print name';
comment on column PIVAS_PREP.SHELF_LIFE is 'Shelf life';
comment on column PIVAS_PREP.PIVAS_CONTAINER_ID is 'Pivas container id';
comment on column PIVAS_PREP.SPLIT_COUNT is 'Split count';
comment on column PIVAS_PREP.DILUENT_ITEM_CODE is 'Solvent item code';
comment on column PIVAS_PREP.DILUENT_MANUF_CODE is 'Solvent manufacturer';
comment on column PIVAS_PREP.DILUENT_BATCH_NUM is 'Diluent batch number';
comment on column PIVAS_PREP.DILUENT_EXPIRY_DATE is 'Diluent expiry date';
comment on column PIVAS_PREP.WARN_CODE1 is 'Warning code 1';
comment on column PIVAS_PREP.WARN_CODE2 is 'Warning code 2';
comment on column PIVAS_PREP.DUE_DATE is 'Due date';
comment on column PIVAS_PREP.FREQ_INTERVAL is 'Frequency interval';
comment on column PIVAS_PREP.SATELLITE_FLAG is 'Satellite flag';
comment on column PIVAS_PREP.SINGLE_DISP_FLAG is 'Single dispense flag';
comment on column PIVAS_PREP.LAST_PRINT_DATE is 'Last print date';
comment on column PIVAS_PREP.EXTRA_XML is 'Extra xml';
comment on column PIVAS_PREP.STATUS is 'Status';
comment on column PIVAS_PREP.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column PIVAS_PREP.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column PIVAS_PREP.CREATE_USER is 'Created user';
comment on column PIVAS_PREP.CREATE_DATE is 'Created date';
comment on column PIVAS_PREP.UPDATE_USER is 'Last updated user';
comment on column PIVAS_PREP.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_PREP.VERSION is 'Version to serve as optimistic lock value';

alter table PIVAS_PREP add constraint FK_PIVAS_PREP_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);
alter table PIVAS_PREP add constraint FK_PIVAS_PREP_02 foreign key (MED_PROFILE_MO_ITEM_ID) references MED_PROFILE_MO_ITEM (ID);

create index FK_PIVAS_PREP_01 on PIVAS_PREP (WORKSTORE_GROUP_CODE) tablespace PMS_INDX_01;
create index FK_PIVAS_PREP_02 on PIVAS_PREP (MED_PROFILE_MO_ITEM_ID) tablespace PMS_INDX_01;


create table MATERIAL_REQUEST (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    HOSP_CODE varchar2(3) not null,
    MED_PROFILE_ID number(19) not null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MATERIAL_REQUEST primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MATERIAL_REQUEST is 'Material request';
comment on column MATERIAL_REQUEST.ID is 'Material request id';
comment on column MATERIAL_REQUEST.STATUS is 'Status';
comment on column MATERIAL_REQUEST.HOSP_CODE is 'Hospital code';
comment on column MATERIAL_REQUEST.MED_PROFILE_ID is 'Medication profile id';
comment on column MATERIAL_REQUEST.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column MATERIAL_REQUEST.CREATE_USER is 'Created user';
comment on column MATERIAL_REQUEST.CREATE_DATE is 'Created date';
comment on column MATERIAL_REQUEST.UPDATE_USER is 'Last updated user';
comment on column MATERIAL_REQUEST.UPDATE_DATE is 'Last updated date';
comment on column MATERIAL_REQUEST.VERSION is 'Version to serve as optimistic lock value';

alter table MATERIAL_REQUEST add constraint FK_MATERIAL_REQUEST_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);
alter table MATERIAL_REQUEST add constraint FK_MATERIAL_REQUEST_02 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);
alter table MATERIAL_REQUEST add constraint FK_MATERIAL_REQUEST_03 foreign key (MED_PROFILE_MO_ITEM_ID) references MED_PROFILE_MO_ITEM (ID);

create index FK_MATERIAL_REQUEST_01 on MATERIAL_REQUEST (HOSP_CODE) tablespace PMS_INDX_01;
create index FK_MATERIAL_REQUEST_02 on MATERIAL_REQUEST (MED_PROFILE_ID) tablespace PMS_INDX_01;
create index FK_MATERIAL_REQUEST_03 on MATERIAL_REQUEST (MED_PROFILE_MO_ITEM_ID) tablespace PMS_INDX_01;


create table MATERIAL_REQUEST_ITEM (
    ID number(19) not null,
    ISSUE_QTY number(19,4) not null,
    DUE_DATE timestamp not null,
    MATERIAL_REQUEST_ID number(19) null,
    MED_PROFILE_PO_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MATERIAL_REQUEST_ITEM primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MATERIAL_REQUEST_ITEM is 'Material request item';
comment on column MATERIAL_REQUEST_ITEM.ID is 'Material request item id';
comment on column MATERIAL_REQUEST_ITEM.ISSUE_QTY is 'Issue qty';
comment on column MATERIAL_REQUEST_ITEM.DUE_DATE is 'Due date';
comment on column MATERIAL_REQUEST_ITEM.MATERIAL_REQUEST_ID  is 'Material request id';
comment on column MATERIAL_REQUEST_ITEM.MED_PROFILE_PO_ITEM_ID is 'Pharmacy order item id (medication profile)';
comment on column MATERIAL_REQUEST_ITEM.CREATE_USER is 'Created user';
comment on column MATERIAL_REQUEST_ITEM.CREATE_DATE is 'Created date';
comment on column MATERIAL_REQUEST_ITEM.UPDATE_USER is 'Last updated user';
comment on column MATERIAL_REQUEST_ITEM.UPDATE_DATE is 'Last updated date';
comment on column MATERIAL_REQUEST_ITEM.VERSION is 'Version to serve as optimistic lock value';

alter table MATERIAL_REQUEST_ITEM add constraint FK_MATERIAL_REQUEST_ITEM_01 foreign key (MATERIAL_REQUEST_ID) references MATERIAL_REQUEST (ID);
alter table MATERIAL_REQUEST_ITEM add constraint FK_MATERIAL_REQUEST_ITEM_02 foreign key (MED_PROFILE_PO_ITEM_ID) references MED_PROFILE_PO_ITEM (ID);

create index FK_MATERIAL_REQUEST_ITEM_01 on MATERIAL_REQUEST_ITEM (MATERIAL_REQUEST_ID) tablespace PMS_INDX_01;
create index FK_MATERIAL_REQUEST_ITEM_02 on MATERIAL_REQUEST_ITEM (MED_PROFILE_PO_ITEM_ID) tablespace PMS_INDX_01;


create table PIVAS_WORKLIST (
    ID number(19) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    SUPPLY_DATE timestamp not null,
    DUE_DATE timestamp not null,
    EXPIRY_DATE timestamp not null,
    NEXT_DUE_DATE timestamp,
    REFILL_FLAG number(1) default 0 not null,
    ISSUE_QTY number(19,4) not null,
    ADJ_QTY number(19,4),
    STATUS varchar2(1) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    PIVAS_PREP_ID number(19) not null,
    MATERIAL_REQUEST_ID number(19),
    MED_PROFILE_ID number(19) not null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_WORKLIST primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_WORKLIST is 'Pivas worklist';
comment on column PIVAS_WORKLIST.ID is 'Pivas worklist id';
comment on column PIVAS_WORKLIST.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column PIVAS_WORKLIST.SUPPLY_DATE is 'Supple date';
comment on column PIVAS_WORKLIST.DUE_DATE is 'Due date';
comment on column PIVAS_WORKLIST.EXPIRY_DATE is 'Expiry date';
comment on column PIVAS_WORKLIST.NEXT_DUE_DATE is 'Next due date';
comment on column PIVAS_WORKLIST.REFILL_FLAG is 'Refill flag';
comment on column PIVAS_WORKLIST.ISSUE_QTY is 'Issue quantity';
comment on column PIVAS_WORKLIST.ADJ_QTY is 'Adjusted quantity';
comment on column PIVAS_WORKLIST.STATUS is 'Status';
comment on column PIVAS_WORKLIST.HOSP_CODE is 'Hospital code';
comment on column PIVAS_WORKLIST.WORKSTORE_CODE is 'Workstore code';
comment on column PIVAS_WORKLIST.PIVAS_PREP_ID is 'Pivas preparation id';
comment on column PIVAS_WORKLIST.MATERIAL_REQUEST_ID is 'Material request id';
comment on column PIVAS_WORKLIST.MED_PROFILE_ID is 'Medication profile id';
comment on column PIVAS_WORKLIST.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column PIVAS_WORKLIST.CREATE_USER is 'Created user';
comment on column PIVAS_WORKLIST.CREATE_DATE is 'Created date';
comment on column PIVAS_WORKLIST.UPDATE_USER is 'Last updated user';
comment on column PIVAS_WORKLIST.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_WORKLIST.VERSION is 'Version to serve as optimistic lock value';

alter table PIVAS_WORKLIST add constraint FK_PIVAS_WORKLIST_01 foreign key (HOSP_CODE, WORKSTORE_CODE) references WORKSTORE (HOSP_CODE, WORKSTORE_CODE);
alter table PIVAS_WORKLIST add constraint FK_PIVAS_WORKLIST_02 foreign key (PIVAS_PREP_ID) references PIVAS_PREP (ID);
alter table PIVAS_WORKLIST add constraint FK_PIVAS_WORKLIST_03 foreign key (MATERIAL_REQUEST_ID) references MATERIAL_REQUEST (ID);
alter table PIVAS_WORKLIST add constraint FK_PIVAS_WORKLIST_04 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);
alter table PIVAS_WORKLIST add constraint FK_PIVAS_WORKLIST_05 foreign key (MED_PROFILE_MO_ITEM_ID) references MED_PROFILE_MO_ITEM (ID);

create index FK_PIVAS_WORKLIST_01 on PIVAS_WORKLIST (HOSP_CODE, WORKSTORE_CODE) tablespace PMS_INDX_01;
create index FK_PIVAS_WORKLIST_02 on PIVAS_WORKLIST (PIVAS_PREP_ID) tablespace PMS_INDX_01;
create index FK_PIVAS_WORKLIST_03 on PIVAS_WORKLIST (MATERIAL_REQUEST_ID) tablespace PMS_INDX_01;
create index FK_PIVAS_WORKLIST_04 on PIVAS_WORKLIST (MED_PROFILE_ID) tablespace PMS_INDX_01;
create index FK_PIVAS_WORKLIST_05 on PIVAS_WORKLIST (MED_PROFILE_MO_ITEM_ID) tablespace PMS_INDX_01;


--//@UNDO
drop table PIVAS_WORKLIST;
drop table MATERIAL_REQUEST_ITEM;
drop table MATERIAL_REQUEST;
drop table PIVAS_PREP;
--//
