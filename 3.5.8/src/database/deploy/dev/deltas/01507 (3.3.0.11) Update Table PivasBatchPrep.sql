alter table PIVAS_BATCH_PREP add DILUENT_CODE varchar2(25);
alter table PIVAS_BATCH_PREP add DILUENT_DESC varchar2(100);

comment on column PIVAS_BATCH_PREP.DILUENT_CODE is 'Diluent code';
comment on column PIVAS_BATCH_PREP.DILUENT_DESC is 'Diluent description';

--//@UNDO
alter table PIVAS_BATCH_PREP drop column DILUENT_CODE;
alter table PIVAS_BATCH_PREP drop column DILUENT_DESC;
--//