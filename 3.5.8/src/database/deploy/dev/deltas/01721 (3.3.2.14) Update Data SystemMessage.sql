insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1005',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Input date should not later than today',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1006',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'The unsaved replenishment record will be cancelled after suspending this item, continue to suspend?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'PHS DH-HA drug mapping has been updated.  Display name of the item codes in pharmacy line do not match with DH-HA drug mapping.  Please review the item codes in pharmacy line and modify if necessary.' where MESSAGE_CODE = '0905';
update SYSTEM_MESSAGE set MAIN_MSG = 'This is an obsolete prescription.' where MESSAGE_CODE = '0900';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1006';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1005';

update SYSTEM_MESSAGE set MAIN_MSG = 'HA drug mapping has been updated, please change the pharmacy line.' where MESSAGE_CODE = '0905';
update SYSTEM_MESSAGE set MAIN_MSG = 'This is an old prescription.' where MESSAGE_CODE = '0900';
--//