alter table MED_PROFILE_PO_ITEM add ORG_MED_PROFILE_PO_ITEM_ID number(19) null;
comment on column MED_PROFILE_PO_ITEM.ORG_MED_PROFILE_PO_ITEM_ID is 'Pharmacy order item id (medication profile)';

--//@UNDO
alter table MED_PROFILE_PO_ITEM set unused column ORG_MED_PROFILE_PO_ITEM_ID;
--//