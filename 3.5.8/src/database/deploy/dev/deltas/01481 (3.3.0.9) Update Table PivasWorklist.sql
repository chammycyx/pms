alter table PIVAS_WORKLIST add PREV_PIVAS_WORKLIST_ID number(19);

comment on column PIVAS_WORKLIST.PREV_PIVAS_WORKLIST_ID is 'Previous PIVAS worklist id';

--//@UNDO
alter table PIVAS_WORKLIST drop column PREV_PIVAS_WORKLIST_ID;
--//