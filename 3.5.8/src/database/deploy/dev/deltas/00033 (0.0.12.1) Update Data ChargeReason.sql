insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (2,'pmsadmin','Dispense to Day Patient/ In-Patient','IP','E',to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),'pmsadmin',1);
insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (1,'pmsadmin','Drug replacement','DR','E',to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),'pmsadmin',1);
insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (3,'pmsadmin','Special approval by HCE','SA','E',to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),'pmsadmin',1);
insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (5,'pmsadmin','Contingency supply','CS','E',to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),'pmsadmin',1);
insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (4,'pmsadmin','Insufficient stock','IS','E',to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),'pmsadmin',1);
insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (0,'pmsadmin','Shroff is closed','SC','F',to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),to_date('08/26/2011 11:46','mm/dd/yyyy hh24:mi'),'pmsadmin',1);

--//@UNDO
delete CHARGE_REASON where ID=0;
delete CHARGE_REASON where ID=1;
delete CHARGE_REASON where ID=2;
delete CHARGE_REASON where ID=3;
delete CHARGE_REASON where ID=4;
delete CHARGE_REASON where ID=5;

--//