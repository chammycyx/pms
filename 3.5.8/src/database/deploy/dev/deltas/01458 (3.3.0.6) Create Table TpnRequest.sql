create table TPN_REQUEST (
    ID number(19) not null,
    DURATION number(2) not null,
    STATUS varchar2(1) not null,
    MED_PROFILE_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_TPN_REQUEST primary key (ID) using index tablespace PMS_INDX_01)    
tablespace PMS_DATA_01;

comment on table  TPN_REQUEST is 'Tpn request';
comment on column TPN_REQUEST.ID is 'Tpn request id';
comment on column TPN_REQUEST.DURATION is 'Tpn request duration';
comment on column TPN_REQUEST.STATUS is 'Tpn request status';
comment on column TPN_REQUEST.MED_PROFILE_ID is 'Medication profile id';
comment on column TPN_REQUEST.CREATE_USER is 'Created user';
comment on column TPN_REQUEST.CREATE_DATE is 'Created date';
comment on column TPN_REQUEST.UPDATE_USER is 'Last updated user';
comment on column TPN_REQUEST.UPDATE_DATE is 'Last updated date';
comment on column TPN_REQUEST.VERSION is 'Version to serve as optimistic lock value';

alter table TPN_REQUEST add constraint FK_TPN_REQUEST_01 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);

create index FK_TPN_REQUEST_01 on TPN_REQUEST (MED_PROFILE_ID) tablespace PMS_INDX_01;

--//@UNDO
drop table TPN_REQUEST;
--//