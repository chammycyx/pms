alter table MED_PROFILE_ORDER add MR_ANNOTATION varchar2(255) null;

comment on column MED_PROFILE_ORDER.MR_ANNOTATION is 'MR Annotation';

--//@UNDO
alter table MED_PROFILE_ORDER drop column MR_ANNOTATION;
--//