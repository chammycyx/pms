create index I_MED_ORDER_T1 on MED_ORDER (HOSP_CODE, PRESC_TYPE, STATUS, ORDER_DATE) local online;
drop index I_MED_ORDER_07;
alter index I_MED_ORDER_T1 rename to I_MED_ORDER_07;

--//@UNDO
create index I_MED_ORDER_T1 on MED_ORDER (HOSP_CODE, PRESC_TYPE) local online;
drop index I_MED_ORDER_07;
alter index I_MED_ORDER_T1 rename to I_MED_ORDER_07;
--//