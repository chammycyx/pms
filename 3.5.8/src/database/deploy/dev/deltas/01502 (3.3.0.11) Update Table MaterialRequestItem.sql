alter table MATERIAL_REQUEST_ITEM add HOSP_CODE varchar2(3);

comment on column MATERIAL_REQUEST_ITEM.HOSP_CODE is 'Hospital code';

update MATERIAL_REQUEST_ITEM i set HOSP_CODE = (select HOSP_CODE from MATERIAL_REQUEST r where r.id = i.MATERIAL_REQUEST_ID);

alter table MATERIAL_REQUEST_ITEM modify HOSP_CODE not null;

--//@UNDO
alter table MATERIAL_REQUEST_ITEM drop column HOSP_CODE;
--//