alter table PIVAS_WORKLIST add MODIFY_DATE timestamp;
alter table PIVAS_WORKLIST modify ISSUE_QTY null;
alter table PIVAS_WORKLIST modify PIVAS_PREP_ID null;

comment on column PIVAS_WORKLIST.MODIFY_DATE is 'Modify date';

--//@UNDO
alter table PIVAS_WORKLIST drop column MODIFY_DATE;
--//