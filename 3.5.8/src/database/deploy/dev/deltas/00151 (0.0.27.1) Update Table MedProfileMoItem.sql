alter table MED_PROFILE_MO_ITEM add URGENT_NUM number(10) null;

comment on column MED_PROFILE_MO_ITEM.URGENT_NUM is 'Urgent Dispense Number';

--//@UNDO
alter table MED_PROFILE_MO_ITEM drop column URGENT_NUM;
--//