create table MP_RENAL_ECRCL_LOG (
    ID number(19) not null,
    CREATE_DATE timestamp not null,
    CREATE_USER varchar2(12) not null,
    BODY_WEIGHT number(5,2) not null,
    SERUM_CREATININE number(5,2) not null,
    ECRCL number(10,2) not null,
    MED_PROFILE_ID number(19) not null,
    constraint PK_MP_RENAL_ECRCL_LOG primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (maxvalue) tablespace P_PMS_MP_DATA_2012);

comment on table  MP_RENAL_ECRCL_LOG is 'Renal dose adjustment logs';
comment on column MP_RENAL_ECRCL_LOG.ID is 'Renal dose adjustment log id';
comment on column MP_RENAL_ECRCL_LOG.CREATE_DATE is 'Created date';
comment on column MP_RENAL_ECRCL_LOG.CREATE_USER is 'Created user';
comment on column MP_RENAL_ECRCL_LOG.BODY_WEIGHT is 'Body weight';
comment on column MP_RENAL_ECRCL_LOG.SERUM_CREATININE is 'Serum creatinine';
comment on column MP_RENAL_ECRCL_LOG.ECRCL is 'Estimated creatinine clearance level';
comment on column MP_RENAL_ECRCL_LOG.MED_PROFILE_ID is 'Medication profile id';

alter table MP_RENAL_ECRCL_LOG add constraint FK_MP_RENAL_ECRCL_LOG_01 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);

create index FK_MP_RENAL_ECRCL_LOG_01 on MP_RENAL_ECRCL_LOG (MED_PROFILE_ID) local;
create index I_MP_RENAL_ECRCL_LOG_01 on MP_RENAL_ECRCL_LOG (MED_PROFILE_ID, CREATE_DATE) local;

create sequence SQ_MP_RENAL_ECRCL_LOG increment by 50 start with 10000000000049;


--//@UNDO
drop index FK_MP_RENAL_ECRCL_LOG_01;
drop index I_MP_RENAL_ECRCL_LOG_01;

alter table MP_RENAL_ECRCL_LOG drop constraint FK_MP_RENAL_ECRCL_LOG_01;

drop table MP_RENAL_ECRCL_LOG;

drop sequence SQ_MP_RENAL_ECRCL_LOG;
--//