alter table PROP add REMINDER_MESSAGE varchar2(1000) null;

comment on column PROP.REMINDER_MESSAGE is 'Reminder message';

update PROP set REMINDER_MESSAGE='Patient alert profile service is not available.' where id=10001;
update PROP set REMINDER_MESSAGE='Medication Decision Support (MDS) service is not available for all drugs. Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' where id=10003;
update PROP set REMINDER_MESSAGE='Fee Collection System (FCS) service is not available.' where id=14020;
update PROP set REMINDER_MESSAGE='Drug on-hand service is not available.' ||chr(13)||chr(10)|| 'Formulary management indication enquiry service is not available.' where id=23001;
update PROP set REMINDER_MESSAGE='Outpatient Appointment (OPAS) service is not available.' where id=23002;
update PROP set REMINDER_MESSAGE='Hong Kong Patient Master Index (HKPMI) service is not available.' where id=23003;

update PROP set MAINT_SCREEN='-' where id=22012;

update PROP set CACHE_IN_SESSION_FLAG=1 where id=23001;

update PROP set NAME='refTable.fdn.substitute' where id=26001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (26002,'refTable.itemLocation.sync.interval','Interval (in hours) to synchronize item location from PMS cluster to PMS corporate','1-24','HH',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert into CORPORATE_PROP (ID,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (26002,26002,'1',26002,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
alter table PROP drop column REMINDER_MESSAGE;

update PROP set MAINT_SCREEN='S' where id=22012;

update PROP set CACHE_IN_SESSION_FLAG=0 where id=23001;

update PROP set NAME='reftable.fdn.substitute' where id=26001;

delete from CORPORATE_PROP where PROP_ID=26002;
delete from PROP where ID=26002;

--//