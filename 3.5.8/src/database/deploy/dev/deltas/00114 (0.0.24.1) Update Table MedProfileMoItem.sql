alter table MED_PROFILE_MO_ITEM add OVERRIDE_DESC varchar2(255) null;
alter table MED_PROFILE_MO_ITEM add OVERRIDE_DOCTOR_CODE varchar2(12) null;
alter table MED_PROFILE_MO_ITEM add OVERRIDE_DOCTOR_RANK_CODE varchar2(10) null;
alter table MED_PROFILE_MO_ITEM add OVERRIDE_DOCTOR_NAME varchar2(48) null;
alter table MED_PROFILE_MO_ITEM add OVERRIDE_DATE timestamp null;
alter table MED_PROFILE_MO_ITEM add RESUME_PENDING_REMARK varchar2(255) null;
alter table MED_PROFILE_MO_ITEM modify PENDING_SUPPL_DESC varchar2(255);


comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DESC is 'Override pending description';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DOCTOR_CODE is 'Override pending doctor code';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DOCTOR_RANK_CODE is 'Override pending doctor rank code';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DOCTOR_NAME is 'Override pending doctor name';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DATE is 'Override date';
comment on column MED_PROFILE_MO_ITEM.RESUME_PENDING_REMARK is 'Resume pending remark';


--//@UNDO
alter table MED_PROFILE_MO_ITEM drop column OVERRIDE_DESC;
alter table MED_PROFILE_MO_ITEM drop column OVERRIDE_DOCTOR_CODE;
alter table MED_PROFILE_MO_ITEM drop column OVERRIDE_DOCTOR_RANK_CODE;
alter table MED_PROFILE_MO_ITEM drop column OVERRIDE_DOCTOR_NAME;
alter table MED_PROFILE_MO_ITEM drop column OVERRIDE_DATE;
alter table MED_PROFILE_MO_ITEM drop column RESUME_PENDING_REMARK;
alter table MED_PROFILE_MO_ITEM modify PENDING_SUPPL_DESC varchar2(44);
--//