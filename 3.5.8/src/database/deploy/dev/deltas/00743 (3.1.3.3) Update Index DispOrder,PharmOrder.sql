drop index I_DISP_ORDER_02;
drop index I_DISP_ORDER_11;
drop index I_PHARM_ORDER_03;


--//@UNDO
create index I_DISP_ORDER_02 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, ADMIN_STATUS) local online;
create index I_DISP_ORDER_11 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, DAY_END_STATUS) local online;
create index I_PHARM_ORDER_03 on PHARM_ORDER (HOSP_CODE, WORKSTORE_CODE, ADMIN_STATUS) local online;
--//