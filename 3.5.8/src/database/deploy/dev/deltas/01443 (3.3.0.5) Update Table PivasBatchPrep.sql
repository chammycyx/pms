alter table PIVAS_BATCH_PREP add SITE_CODE varchar2(25) not null;

comment on column PIVAS_BATCH_PREP.SITE_CODE is 'Site code';

--//@UNDO
alter table PIVAS_BATCH_PREP drop column SITE_CODE;
--//