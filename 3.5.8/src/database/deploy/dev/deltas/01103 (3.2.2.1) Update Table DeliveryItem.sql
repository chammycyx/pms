alter table DELIVERY_ITEM add CHARGE_QTY number(19,4) null;
alter table DELIVERY_ITEM add PURCHASE_REQUEST_ID number(19) null;

comment on column DELIVERY_ITEM.CHARGE_QTY is 'Charge quantity (private patient)';
comment on column DELIVERY_ITEM.PURCHASE_REQUEST_ID is 'Purchase request id';

alter table DELIVERY_ITEM add constraint FK_DELIVERY_ITEM_04 foreign key (PURCHASE_REQUEST_ID) references PURCHASE_REQUEST (ID);

create index FK_DELIVERY_ITEM_04 on DELIVERY_ITEM (PURCHASE_REQUEST_ID) tablespace PMS_INDX_01;

--//@UNDO
drop index FK_DELIVERY_ITEM_04;

alter table DELIVERY_ITEM drop constraint FK_DELIVERY_ITEM_04;

alter table DELIVERY_ITEM drop column CHARGE_QTY;
alter table DELIVERY_ITEM drop column PURCHASE_REQUEST_ID;
--//