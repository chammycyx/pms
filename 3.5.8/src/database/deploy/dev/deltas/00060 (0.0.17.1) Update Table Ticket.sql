alter table TICKET add ORDER_NUM varchar2(12) null;

comment on column TICKET.ORDER_NUM is 'Order Number Printed on Ticket Label';

--//@UNDO
alter table TICKET drop column ORDER_NUM;
--//