update DELIVERY_ITEM di1 
set di1.MED_PROFILE_PO_ITEM_ID = 
(select ri.MED_PROFILE_PO_ITEM_ID 
  from REPLENISHMENT_ITEM ri
  join DELIVERY_ITEM di2 on (di2.REPLENISHMENT_ITEM_ID = ri.ID) 
  where di2.MED_PROFILE_PO_ITEM_ID is null and di2.ID = di1.ID)
where di1.MED_PROFILE_PO_ITEM_ID is null;

alter table DELIVERY_ITEM modify MED_PROFILE_PO_ITEM_ID not null;


--//@UNDO
alter table DELIVERY_ITEM modify MED_PROFILE_PO_ITEM_ID null;

update DELIVERY_ITEM
set MED_PROFILE_PO_ITEM_ID = null where REPLENISHMENT_ITEM_ID is not null;
--//