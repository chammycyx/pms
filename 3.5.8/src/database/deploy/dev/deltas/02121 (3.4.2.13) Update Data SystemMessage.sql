insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1074',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'Change(s) has been made without save.  Continue to close and abort the changes?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1075',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'There is no non-HA data in patient''s eHR',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'This patient is under Drug Refill Services.  Please perform medication reconciliation.' where MESSAGE_CODE = '1055';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please review this patient under Drug Refill Services.  Confirm to split the dispensing of prescription?' where MESSAGE_CODE = '1056';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please review this patient under Drug Refill Services (Clinical Service Model).  Confirm to split the dispensing of prescription?' where MESSAGE_CODE = '1057';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'This patient is under drug refill service.  Please perform medication reconciliation.' where MESSAGE_CODE = '1055';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please review this patient under drug refill services.  Confirm to split the dispensing of prescription?' where MESSAGE_CODE = '1056';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please review this patient under drug refill services (Clinical Service Model).  Confirm to split the dispensing of prescription?' where MESSAGE_CODE = '1057';

delete SYSTEM_MESSAGE where MESSAGE_CODE = '1074';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1075';
--//