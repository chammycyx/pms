alter table PIVAS_PREP add LABEL_OPTION number(10);
alter table PIVAS_PREP add LABEL_OPTION_TYPE varchar2(3);

comment on column PIVAS_PREP.LABEL_OPTION is 'Label option';
comment on column PIVAS_PREP.LABEL_OPTION_TYPE is 'Label option type';

--//@UNDO
alter table PIVAS_PREP drop column LABEL_OPTION;
alter table PIVAS_PREP drop column LABEL_OPTION_TYPE;
--//