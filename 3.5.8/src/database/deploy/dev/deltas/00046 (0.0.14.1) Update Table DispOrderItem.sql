alter table DISP_ORDER_ITEM add ITEM_NUM number(10) null;
update DISP_ORDER_ITEM
SET ITEM_NUM = (select ITEM_NUM from PHARM_ORDER_ITEM where PHARM_ORDER_ITEM.ID = DISP_ORDER_ITEM.PHARM_ORDER_ITEM_ID);
alter table DISP_ORDER_ITEM modify ITEM_NUM not null;

comment on column DISP_ORDER_ITEM.ITEM_NUM is 'Item number';

--//@UNDO
alter table DISP_ORDER_ITEM drop column ITEM_NUM;
--//