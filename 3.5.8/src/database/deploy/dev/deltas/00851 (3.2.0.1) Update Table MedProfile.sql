alter table MED_PROFILE add MAX_ITEM_NUM number(10);
alter table MED_PROFILE add DIAGNOSIS varchar2(15);
alter table MED_PROFILE add DRUG_SENSITIVITY varchar2(30);
alter table MED_PROFILE modify ORDER_NUM varchar2(36);

comment on column MED_PROFILE.MAX_ITEM_NUM is 'Maximum Item Number';
comment on column MED_PROFILE.DIAGNOSIS is 'Diagnosis';
comment on column MED_PROFILE.DRUG_SENSITIVITY is 'Drug Sensitivity';

--//@UNDO
alter table MED_PROFILE drop column MAX_ITEM_NUM;
alter table MED_PROFILE drop column DIAGNOSIS;
alter table MED_PROFILE drop column DRUG_SENSITIVITY;
--//