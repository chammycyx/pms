alter table MED_PROFILE add LAST_INVOICE_PRINT_DATE timestamp;
alter table MED_PROFILE add CHARGE_ORDER_NUM varchar2(12) null;

comment on column MED_PROFILE.LAST_INVOICE_PRINT_DATE is 'Last Invoice Print Date';
comment on column MED_PROFILE.CHARGE_ORDER_NUM is 'Order Num for IPSFI Invoice';

--//@UNDO
alter table MED_PROFILE drop column LAST_INVOICE_PRINT_DATE;
alter table MED_PROFILE drop column CHARGE_ORDER_NUM;
--//