update OPERATION_WORKSTATION set TYPE='#' 
 where ID in ( select w.ID from OPERATION_PROFILE p, OPERATION_WORKSTATION w
                where p.ID = w.OPERATION_PROFILE_ID
                and p.TYPE = 'P'
                and w.TYPE not in ('T', 'E', 'R', 'I', '#'));