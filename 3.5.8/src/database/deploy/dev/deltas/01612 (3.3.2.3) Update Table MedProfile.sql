alter table MED_PROFILE set unused column SHOW_TRANSFER_ALERT_FLAG;
alter table MED_PROFILE add TRANSFER_ALERT_TYPE varchar2(1) null;

comment on column MED_PROFILE.TRANSFER_ALERT_TYPE is 'Ward Transfer Alert Type';

--//@UNDO
alter table MED_PROFILE add SHOW_TRANSFER_ALERT_FLAG number(1);
alter table MED_PROFILE set unused column TRANSFER_ALERT_TYPE;