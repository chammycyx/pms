comment on column CHEST_ITEM.DOSE_QTY is 'Dose quantity';
comment on column MED_ORDER.HLA_FLAG is 'with HLA alert';
comment on column PATIENT.STATUS is 'Record status';
comment on column PATIENT_CAT.INST_CODE is 'Institution code';
comment on column PRN_ROUTE.STATUS is 'Record status';
comment on column SPECIALTY.INST_CODE is 'Institution code';
comment on column WARD.INST_CODE is 'Institution code';
comment on column WARD_STOCK.INST_CODE is 'Institution code';
comment on column WORKSTORE_GROUP.INST_CODE is 'Institution code';