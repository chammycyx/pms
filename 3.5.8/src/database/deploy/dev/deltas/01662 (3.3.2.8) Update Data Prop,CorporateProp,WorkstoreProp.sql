update PROP set DESCRIPTION='Limited duration (in days) of items allowed for Dispensing Item Status Enquiry',UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=38001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (10016,'alert.ehr.profile.rollout','[to be obsolete] CMS rollout: Enhancement for eHR Alert','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (10016,10016,'N',10016,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (38002,'enquiry.dispItemStatus.criteria.item.duration','Duration (in days) of items for Dispensing Item Status Enquiry','1-7','dd',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,38002,'7',38002,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP set DESCRIPTION='Limited duration (in days) of orders allowed for Dispensing Item Status Enquiry',UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=38001;

delete from CORPORATE_PROP where PROP_ID in (10016);
delete from WORKSTORE_PROP where PROP_ID in (38002);

delete from PROP where ID in (10016, 38002);
--//