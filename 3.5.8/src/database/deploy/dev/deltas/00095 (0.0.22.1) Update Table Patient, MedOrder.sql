alter table PATIENT drop column HLA_BEFORE_FLAG;
alter table MED_ORDER add HLA_FLAG number(1) default 0 null;
comment on column MED_ORDER.HLA_FLAG is 'HLA';
update MED_ORDER set HLA_FLAG=0;
alter table MED_ORDER modify HLA_FLAG not null;

--//@UNDO
alter table PATIENT add HLA_BEFORE_FLAG number(1) default 0 null;
update PATIENT set HLA_BEFORE_FLAG=0;
alter table PATIENT modify HLA_BEFORE_FLAG not null;
alter table MED_ORDER drop column HLA_FLAG;
--//
