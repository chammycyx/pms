alter table PRN_ROUTE add ROUTE_DESC varchar2(20) null;

update PRN_ROUTE set ROUTE_DESC = 'BUCCAL' where ROUTE_CODE = 'BUCCAL';         
update PRN_ROUTE set ROUTE_DESC = 'CARTRIDGE' where ROUTE_CODE = 'CART';        
update PRN_ROUTE set ROUTE_DESC = 'DENTAL' where ROUTE_CODE = 'DENTAL';         
update PRN_ROUTE set ROUTE_DESC = 'EAR' where ROUTE_CODE = 'EAR';               
update PRN_ROUTE set ROUTE_DESC = 'EPIDURAL' where ROUTE_CODE = 'EPIDUR';       
update PRN_ROUTE set ROUTE_DESC = 'EYE' where ROUTE_CODE = 'EYE';               
update PRN_ROUTE set ROUTE_DESC = 'EYE/EAR' where ROUTE_CODE = 'EYE/EAR';       
update PRN_ROUTE set ROUTE_DESC = 'EYE/EAR/NOSE' where ROUTE_CODE = 'EYE/EN';   
update PRN_ROUTE set ROUTE_DESC = 'EYE/NASAL' where ROUTE_CODE = 'EYE/NAS';     
update PRN_ROUTE set ROUTE_DESC = 'GAS' where ROUTE_CODE = 'GAS';               
update PRN_ROUTE set ROUTE_DESC = 'I-ARTERIAL' where ROUTE_CODE = 'I-ARTER';    
update PRN_ROUTE set ROUTE_DESC = 'I-ARTICULAR' where ROUTE_CODE = 'I-ARTIC';   
update PRN_ROUTE set ROUTE_DESC = 'INTRADERMAL' where ROUTE_CODE = 'I-DERMA';   
update PRN_ROUTE set ROUTE_DESC = 'I-OCULAR' where ROUTE_CODE = 'I-OCULA';      
update PRN_ROUTE set ROUTE_DESC = 'I-THECAL' where ROUTE_CODE = 'I-THECA';      
update PRN_ROUTE set ROUTE_DESC = 'I-TRACHEAL' where ROUTE_CODE = 'I-TRACH';    
update PRN_ROUTE set ROUTE_DESC = 'INTRAVESICAL' where ROUTE_CODE = 'I-VESIC';  
update PRN_ROUTE set ROUTE_DESC = 'I-VITREAL' where ROUTE_CODE = 'I-VITRE';     
update PRN_ROUTE set ROUTE_DESC = 'INTRATHECAL' where ROUTE_CODE = 'IA';        
update PRN_ROUTE set ROUTE_DESC = 'INTRAMUSCUL.' where ROUTE_CODE = 'IM';       
update PRN_ROUTE set ROUTE_DESC = 'IMPLANT' where ROUTE_CODE = 'IMPLANT';       
update PRN_ROUTE set ROUTE_DESC = 'INHALE' where ROUTE_CODE = 'INHALE';         
update PRN_ROUTE set ROUTE_DESC = 'INJECTION' where ROUTE_CODE = 'INJ';         
update PRN_ROUTE set ROUTE_DESC = 'INTRAPERITON' where ROUTE_CODE = 'IP';       
update PRN_ROUTE set ROUTE_DESC = 'IRRIGATION' where ROUTE_CODE = 'IRRI';       
update PRN_ROUTE set ROUTE_DESC = 'INTRAVENOUS' where ROUTE_CODE = 'IV';        
update PRN_ROUTE set ROUTE_DESC = 'KIT' where ROUTE_CODE = 'KIT';               
update PRN_ROUTE set ROUTE_DESC = 'TOPICAL' where ROUTE_CODE = 'LA';            
update PRN_ROUTE set ROUTE_DESC = 'LIQUID GAS' where ROUTE_CODE = 'LIQ_GAS';    
update PRN_ROUTE set ROUTE_DESC = 'MISCELLANOUS' where ROUTE_CODE = 'MISC';     
update PRN_ROUTE set ROUTE_DESC = 'NASAL' where ROUTE_CODE = 'NASAL';           
update PRN_ROUTE set ROUTE_DESC = 'NERVE BLOCK' where ROUTE_CODE = 'NERVE B';   
update PRN_ROUTE set ROUTE_DESC = 'OPHTHALMIC' where ROUTE_CODE = 'OPH';        
update PRN_ROUTE set ROUTE_DESC = 'PERCUTANEOUS' where ROUTE_CODE = 'PERCUT';   
update PRN_ROUTE set ROUTE_DESC = 'ORAL' where ROUTE_CODE = 'PO';               
update PRN_ROUTE set ROUTE_DESC = 'ORAL/NASAL' where ROUTE_CODE = 'PO/NASA';    
update PRN_ROUTE set ROUTE_DESC = 'ORAL/RECTAL' where ROUTE_CODE = 'PO/RECT';   
update PRN_ROUTE set ROUTE_DESC = 'POWDER' where ROUTE_CODE = 'POWDER';         
update PRN_ROUTE set ROUTE_DESC = 'RECTAL' where ROUTE_CODE = 'RECTAL';         
update PRN_ROUTE set ROUTE_DESC = 'SUBCUTANEOUS' where ROUTE_CODE = 'SC';       
update PRN_ROUTE set ROUTE_DESC = 'SOLUTION' where ROUTE_CODE = 'SOLN';         
update PRN_ROUTE set ROUTE_DESC = 'SUBLINGUAL' where ROUTE_CODE = 'SUBL';       
update PRN_ROUTE set ROUTE_DESC = 'SUSPENSION' where ROUTE_CODE = 'SUSP';       
update PRN_ROUTE set ROUTE_DESC = 'TRANSDERMAL' where ROUTE_CODE = 'T-DERMA';   
update PRN_ROUTE set ROUTE_DESC = 'TEST AGENT' where ROUTE_CODE = 'TEST';       
update PRN_ROUTE set ROUTE_DESC = 'URETHRAL' where ROUTE_CODE = 'URETHRA';      
update PRN_ROUTE set ROUTE_DESC = 'VACCINE' where ROUTE_CODE = 'VAC';           
update PRN_ROUTE set ROUTE_DESC = 'VAGINAL' where ROUTE_CODE = 'VAGINAL';    

delete from PRN_ROUTE where ROUTE_DESC is null;
alter table PRN_ROUTE modify ROUTE_DESC not null;

comment on column PRN_ROUTE.ROUTE_DESC is 'Route description';

--//@UNDO
alter table PRN_ROUTE drop column ROUTE_DESC;
--//