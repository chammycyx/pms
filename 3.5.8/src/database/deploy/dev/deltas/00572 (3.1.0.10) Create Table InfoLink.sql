create table INFO_LINK (
ID number(19) not null,
NAME varchar2(100) not null,
URL varchar2(100) not null,
SORT_SEQ number(10) not null,
constraint PK_INFO_LINK primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table INFO_LINK is 'Information link';
comment on column INFO_LINK.ID is 'Information link id';
comment on column INFO_LINK.NAME is 'Name';
comment on column INFO_LINK.URL is 'Url';
comment on column INFO_LINK.SORT_SEQ is 'Sort sequence';

create sequence SQ_INFO_LINK increment by 50 start with 10000000000049;

--//@UNDO
drop sequence SQ_INFO_LINK;
drop table INFO_LINK;
--//