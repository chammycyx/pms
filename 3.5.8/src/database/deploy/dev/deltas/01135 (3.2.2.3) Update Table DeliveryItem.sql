alter table DELIVERY_ITEM add ITEM_CODE varchar2(6) null;

comment on column DELIVERY_ITEM.ITEM_CODE is 'Item code';

--//@UNDO
alter table DELIVERY_ITEM drop column ITEM_CODE;
--//