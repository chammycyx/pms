update PROP set DESCRIPTION='Label option for PIVAS Formula Maintenance', VALIDATION=',extraProductLabel,extraSetLabel', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=39002;
update HOSPITAL_PROP set VALUE='', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=39002;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (39006,'pivas.formula.label.extra.copies','Extra copies of label printed for PIVAS Formula Maintenance','1-2','I',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,39006,'1',HOSP_CODE,39006,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION='Label option for Formula Maintenance', VALIDATION='extraProductLabel,extraSetLabel,noLabel', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=39002;
update HOSPITAL_PROP set VALUE='noLabel', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=39002;

delete from HOSPITAL_PROP where PROP_ID in (39006);
delete from PROP where ID in (39006);
--//