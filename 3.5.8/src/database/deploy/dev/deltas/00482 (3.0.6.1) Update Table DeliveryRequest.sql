alter table DELIVERY_REQUEST add ORDER_TYPE varchar2(1);

comment on column DELIVERY_REQUEST.ORDER_TYPE is 'Due Order Label Generation Order Type';



--//@UNDO

alter table DELIVERY_REQUEST drop column ORDER_TYPE;
--//
