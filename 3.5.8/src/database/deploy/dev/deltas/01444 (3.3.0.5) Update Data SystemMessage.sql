insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0917',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Patient information cannot be converted.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0918',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'One of versions of this DH order number is processed by [#0].  This DH order cannot be processed. ',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0919',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'The supply schedule of the orders in the worklist [#0] is different from the supply schedule of the selected order(s) [#1].  Please process the orders from the worklist before adding another batch.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'Please input supply date' where MESSAGE_CODE = '0911';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'I', MAIN_MSG = 'Preparation method has already been created.' where MESSAGE_CODE = '0912';
update SYSTEM_MESSAGE set MAIN_MSG = 'Value should be #0' where MESSAGE_CODE = '0250';
update SYSTEM_MESSAGE set MAIN_MSG = 'The supply schedule of the orders in the worklist [#0] is different from the supply schedule for generation [#1].  Confirm to proceed?' where MESSAGE_CODE = '0916';
update SYSTEM_MESSAGE set MAIN_MSG = 'No label will be printed.  Confirm to proceed?' where MESSAGE_CODE = '0874';
update SYSTEM_MESSAGE set MAIN_MSG = 'Item code: [#0], manufacturer: [#1] is currently used as the preparation method for formula.  Please check.' where MESSAGE_CODE = '0834';
update SYSTEM_MESSAGE set MAIN_MSG = 'Item code: [#0], manufacturer: [#1], reconstitution method: [#2] is currently used as the preparation method for formula.  Please check.' where MESSAGE_CODE = '0835';
update SYSTEM_MESSAGE set MAIN_MSG = 'Item code: [#0], manufacturer: [#1], reconstitution method: [#2], route / site: [#3] is currently used as the preparation method for formula.  Please check.' where MESSAGE_CODE = '0836';
update SYSTEM_MESSAGE set MAIN_MSG = 'No label will be printed.  Confirm to proceed?' where MESSAGE_CODE = '0874';
update SYSTEM_MESSAGE set MAIN_MSG = 'Failed to save batch preparation.  Maximum lot number is [#0].' where MESSAGE_CODE = '0880';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0917';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0918';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0919';

update SYSTEM_MESSAGE set MAIN_MSG = 'Please input supply date.' where MESSAGE_CODE = '0911';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'W', MAIN_MSG = 'Invalid supply date.' where MESSAGE_CODE = '0912';
update SYSTEM_MESSAGE set MAIN_MSG = 'Value should be between [#0] and [#1]' where MESSAGE_CODE = '0250';
update SYSTEM_MESSAGE set MAIN_MSG = 'The supply schedule of the orders in the work list [#0] is different from the supply schedule for generation [#1]. Confirm to proceed?' where MESSAGE_CODE = '0916';
update SYSTEM_MESSAGE set MAIN_MSG = 'No of label will be printed.  Confirm to proceed?' where MESSAGE_CODE = '0874';
--//