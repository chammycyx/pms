update PROP set DESCRIPTION='Enable TPN in IP Vetting', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32048;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32051,'vetting.medProfile.tpn.desktopName','TPN desktop name for IP Vetting','0-100','S',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,32051,null,HOSP_CODE,32051,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION='Enable TPN', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32048;

delete from HOSPITAL_PROP where PROP_ID in (32051);
delete from PROP where ID in (32051);
--//