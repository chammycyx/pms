alter table MED_PROFILE_MO_ITEM add VERBAL_FLAG number(1);

comment on column MED_PROFILE_MO_ITEM.VERBAL_FLAG is 'Verbal flag';

--//@UNDO
alter table MED_PROFILE_MO_ITEM drop column VERBAL_FLAG;
--//