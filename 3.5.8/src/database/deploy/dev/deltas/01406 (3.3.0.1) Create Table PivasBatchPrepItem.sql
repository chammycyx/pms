create table PIVAS_BATCH_PREP_ITEM (
    ID number(19) not null,
    DRUG_ITEM_CODE varchar2(10) not null,
    DRUG_MANUF_CODE varchar2(4) not null,
    DRUG_BATCH_NUM varchar2(20) not null,
    DRUG_EXPIRY_DATE date not null,
    SOLVENT_ITEM_CODE varchar2(10),
    SOLVENT_MANUF_CODE varchar2(4),
    SOLVENT_BATCH_NUM varchar2(20),
    SOLVENT_EXPIRY_DATE date,
    PIVAS_BATCH_PREP_LOT_NUM varchar2(14) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_BATCH_PREP_ITEM primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_BATCH_PREP_ITEM is 'Pivas batch preparation item';
comment on column PIVAS_BATCH_PREP_ITEM.ID is 'Pivas batch preparation item id';
comment on column PIVAS_BATCH_PREP_ITEM.PIVAS_BATCH_PREP_LOT_NUM is 'Pivas batch preparation lot number';
comment on column PIVAS_BATCH_PREP_ITEM.DRUG_ITEM_CODE is 'Drug item code';
comment on column PIVAS_BATCH_PREP_ITEM.DRUG_MANUF_CODE is 'Drug manufacturer';
comment on column PIVAS_BATCH_PREP_ITEM.DRUG_BATCH_NUM is 'Drug batch number';
comment on column PIVAS_BATCH_PREP_ITEM.DRUG_EXPIRY_DATE is 'Drug expiry date';
comment on column PIVAS_BATCH_PREP_ITEM.SOLVENT_ITEM_CODE is 'Solvent item code';
comment on column PIVAS_BATCH_PREP_ITEM.SOLVENT_MANUF_CODE is 'Solvent manufacturer';
comment on column PIVAS_BATCH_PREP_ITEM.SOLVENT_BATCH_NUM is 'Solvent batch number';
comment on column PIVAS_BATCH_PREP_ITEM.SOLVENT_EXPIRY_DATE is 'Solvent expiry date';
comment on column PIVAS_BATCH_PREP_ITEM.CREATE_USER is 'Created user';
comment on column PIVAS_BATCH_PREP_ITEM.CREATE_DATE is 'Created date';
comment on column PIVAS_BATCH_PREP_ITEM.UPDATE_USER is 'Last updated user';
comment on column PIVAS_BATCH_PREP_ITEM.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_BATCH_PREP_ITEM.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table PIVAS_BATCH_PREP_ITEM;
--//