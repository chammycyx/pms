alter table MED_PROFILE_STAT add STAT_FLAG number(1) default 0;
alter table MED_PROFILE_STAT add OVERRIDE_PENDING_FLAG number(1) default 0;

comment on column MED_PROFILE_STAT.STAT_FLAG is 'Contain STAT item(s)';
comment on column MED_PROFILE_STAT.OVERRIDE_PENDING_FLAG is 'Contain cancel pending item(s)';

update MED_PROFILE_STAT set STAT_FLAG = nvl2(DUE_DATE, 0, 1);
update MED_PROFILE_STAT set OVERRIDE_PENDING_FLAG = 0;

alter table MED_PROFILE_STAT modify STAT_FLAG not null;
alter table MED_PROFILE_STAT modify OVERRIDE_PENDING_FLAG not null;

alter table MED_PROFILE_ITEM drop column ADMIN_STATUS;

--//@UNDO
alter table MED_PROFILE_STAT drop column STAT_FLAG;
alter table MED_PROFILE_STAT drop column OVERRIDE_PENDING_FLAG;

alter table MED_PROFILE_ITEM add ADMIN_STATUS varchar2(1);
comment on column MED_PROFILE_ITEM.ADMIN_STATUS is 'Pharmacy admin status';
update MED_PROFILE_ITEM set ADMIN_STATUS = 'N';
alter table MED_PROFILE_ITEM modify ADMIN_STATUS not null;
--//