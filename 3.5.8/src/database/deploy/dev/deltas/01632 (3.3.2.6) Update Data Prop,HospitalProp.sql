update PROP set NAME='check.medProfile.checkList.duration', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=15009;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32053,'vetting.medProfile.ipmoe.adminStatus.enabled','Enable IPMOE Admin Status service in IP Vetting','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,32053,'N',HOSP_CODE,32053,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set NAME='check.mp.checkList.duration', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=15009;

delete from HOSPITAL_PROP where PROP_ID in (32053);
delete from PROP where ID in (32053);
--//