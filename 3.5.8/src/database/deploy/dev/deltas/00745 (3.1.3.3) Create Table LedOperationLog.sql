create table LED_OPERATION_LOG (
    ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    TICKET_DATE date not null,
    TICKET_NUM varchar2(4) not null,
    WINDOW_NUM number(2) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_LED_OPERATION_LOG primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (maxvalue) tablespace P_PMS_DISP_DATA_2012);    

comment on table LED_OPERATION_LOG is 'Led operation log';
comment on column LED_OPERATION_LOG.ID is 'Led operation log id';
comment on column LED_OPERATION_LOG.HOSP_CODE is 'Hospital code';
comment on column LED_OPERATION_LOG.WORKSTORE_CODE is 'Workstore code';
comment on column LED_OPERATION_LOG.TICKET_DATE is 'Ticket date';
comment on column LED_OPERATION_LOG.TICKET_NUM is 'Ticket number';
comment on column LED_OPERATION_LOG.WINDOW_NUM is 'Window number';
comment on column LED_OPERATION_LOG.STATUS is 'Led operation log status';
comment on column LED_OPERATION_LOG.CREATE_USER is 'Created user';
comment on column LED_OPERATION_LOG.CREATE_DATE is 'Created date';
comment on column LED_OPERATION_LOG.UPDATE_USER is 'Last updated user';
comment on column LED_OPERATION_LOG.UPDATE_DATE is 'Last updated date';

create sequence SQ_LED_OPERATION_LOG increment by 50 start with 10000000000049;

create index I_LED_OPERATION_LOG_01 on LED_OPERATION_LOG (HOSP_CODE, WORKSTORE_CODE, TICKET_DATE, TICKET_NUM) local;

--//@UNDO
drop index I_LED_OPERATION_LOG_01;
drop sequence SQ_LED_OPERATION_LOG;
drop table LED_OPERATION_LOG;
--//
