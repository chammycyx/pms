alter table DISP_ORDER add REVOKE_FLAG number(1) null;
alter table DISP_ORDER add REVOKE_USER varchar2(12) null;
alter table DISP_ORDER add REVOKE_DATE timestamp null;
alter table DISP_ORDER add REFRIG_ITEM_FLAG number(1) null;

comment on column DISP_ORDER.REVOKE_FLAG is 'Revoke flag';
comment on column DISP_ORDER.REVOKE_USER is 'Revoke user';
comment on column DISP_ORDER.REVOKE_DATE is 'Revoke date';
comment on column DISP_ORDER.REFRIG_ITEM_FLAG is 'Refrigerated item';

--//@UNDO
alter table DISP_ORDER drop column REVOKE_FLAG;
alter table DISP_ORDER drop column REVOKE_USER;
alter table DISP_ORDER drop column REVOKE_DATE;
alter table DISP_ORDER drop column REFRIG_ITEM_FLAG;
--//