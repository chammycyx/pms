alter table HOSPITAL_MAPPING add PRESCRIBE_FLAG number(1) null;
update HOSPITAL_MAPPING set PRESCRIBE_FLAG = 0;
alter table HOSPITAL_MAPPING modify PRESCRIBE_FLAG default 0 not null;

comment on column HOSPITAL_MAPPING.PRESCRIBE_FLAG is 'Prescribe flag';

--//@UNDO
alter table HOSPITAL_MAPPING drop column PRESCRIBE_FLAG;
--//
