alter table MED_PROFILE_ORDER add ORDER_DATE timestamp null;
comment on column MED_PROFILE_ORDER.ORDER_DATE is 'Order Date';

update MED_PROFILE_ORDER set ORDER_DATE = CREATE_DATE;

alter table MED_PROFILE_ORDER modify ORDER_DATE not null;


alter table MED_PROFILE_MO_ITEM add ORDER_DATE timestamp null;
comment on column MED_PROFILE_MO_ITEM.ORDER_DATE is 'Order Date';

update MED_PROFILE_MO_ITEM mpmi set mpmi.ORDER_DATE =
(select mpo.ORDER_DATE from MED_PROFILE_ORDER mpo where mpmi.MED_PROFILE_ORDER_ID = mpo.ID);

update MED_PROFILE_MO_ITEM set ORDER_DATE = CREATE_DATE where ORDER_DATE is null;

alter table MED_PROFILE_MO_ITEM modify ORDER_DATE not null;


--//@UNDO
alter table MED_PROFILE_ORDER drop column ORDER_DATE;

alter table MED_PROFILE_MO_ITEM drop column ORDER_DATE;
--//