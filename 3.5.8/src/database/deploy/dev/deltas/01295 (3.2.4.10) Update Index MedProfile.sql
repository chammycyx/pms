create index I_MED_PROFILE_T1 on MED_PROFILE (STATUS, UPDATE_DATE) local online;
drop index I_MED_PROFILE_02;
alter index I_MED_PROFILE_T1 rename to I_MED_PROFILE_02;

--//@UNDO
create index I_MED_PROFILE_T1 on MED_PROFILE (STATUS) local online;
drop index I_MED_PROFILE_02;
alter index I_MED_PROFILE_T1 rename to I_MED_PROFILE_02;
--//
