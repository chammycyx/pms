create sequence SQ_DELIVERY_SCHEDULE increment by 50 start with 10000000000049;
create sequence SQ_DELIVERY_SCHEDULE_ITEM increment by 50 start with 10000000000049;
create sequence SQ_DELIVERY_REQUEST_GROUP increment by 50 start with 10000000000049;

create sequence SQ_DRS_CONFIG increment by 50 start with 10000000000049;
create sequence SQ_DRS_PATIENT increment by 50 start with 10000000000049;
create sequence SQ_DRS_PATIENT_CONTACT increment by 50 start with 10000000000049;


--//@UNDO
drop sequence SQ_DELIVERY_SCHEDULE;
drop sequence SQ_DELIVERY_SCHEDULE_ITEM;
drop sequence SQ_DELIVERY_REQUEST_GROUP;

drop sequence SQ_DRS_CONFIG;
drop sequence SQ_DRS_PATIENT;
drop sequence SQ_DRS_PATIENT_CONTACT;
--//