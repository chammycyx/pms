alter table MED_ORDER_FM modify AUTH_DOCTOR_NAME varchar2(70);
alter table MED_PROFILE_MO_ITEM_FM modify AUTH_DOCTOR_NAME varchar2(70);

--//@UNDO
alter table MED_ORDER_FM modify AUTH_DOCTOR_NAME varchar2(35);
alter table MED_PROFILE_MO_ITEM_FM modify AUTH_DOCTOR_NAME varchar2(35);
--//