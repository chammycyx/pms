alter table SPECIALTY drop constraint FK_SPECIALTY_01;
alter table SPECIALTY rename column WORKSTORE_GROUP_CODE to INST_CODE;
update SPECIALTY set INST_CODE = 'QEH' where INST_CODE = 'QEE';
alter table SPECIALTY add constraint FK_SPECIALTY_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

alter table PATIENT_CAT drop constraint FK_PATIENT_CAT_01;
alter table PATIENT_CAT rename column WORKSTORE_GROUP_CODE to INST_CODE;
update PATIENT_CAT set INST_CODE = 'QEH' where INST_CODE = 'QEE';
alter table PATIENT_CAT add constraint FK_PATIENT_CAT_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);

alter table WARD_STOCK drop constraint FK_WARD_STOCK_01;
alter table WARD drop constraint FK_WARD_01;
alter table WARD_STOCK rename column WORKSTORE_GROUP_CODE to INST_CODE;
alter table WARD rename column WORKSTORE_GROUP_CODE to INST_CODE;
update WARD_STOCK set INST_CODE = 'QEH' where INST_CODE = 'QEE';
update WARD set INST_CODE = 'QEH' where INST_CODE = 'QEE';
alter table WARD add constraint FK_WARD_01 foreign key (INST_CODE) references INSTITUTION (INST_CODE);
alter table WARD_STOCK add constraint FK_WARD_STOCK_01 foreign key (INST_CODE,WARD_CODE) references WARD (INST_CODE,WARD_CODE);

--//@UNDO
alter table SPECIALTY drop constraint FK_SPECIALTY_01;
alter table SPECIALTY rename column INST_CODE to WORKSTORE_GROUP_CODE;
update SPECIALTY set WORKSTORE_GROUP_CODE = 'QEE' where WORKSTORE_GROUP_CODE = 'QEH';
alter table SPECIALTY add constraint FK_SPECIALTY_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);

alter table PATIENT_CAT drop constraint FK_PATIENT_CAT_01;
alter table PATIENT_CAT rename column INST_CODE to WORKSTORE_GROUP_CODE;
update PATIENT_CAT set WORKSTORE_GROUP_CODE = 'QEE' where WORKSTORE_GROUP_CODE = 'QEH';
alter table PATIENT_CAT add constraint FK_PATIENT_CAT_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);

alter table WARD_STOCK drop constraint FK_WARD_STOCK_01;
alter table WARD drop constraint FK_WARD_01;
alter table WARD_STOCK rename column INST_CODE to WORKSTORE_GROUP_CODE;
alter table WARD rename column INST_CODE to WORKSTORE_GROUP_CODE;
update WARD_STOCK set WORKSTORE_GROUP_CODE = 'QEE' where WORKSTORE_GROUP_CODE = 'QEH';
update WARD set WORKSTORE_GROUP_CODE = 'QEE' where WORKSTORE_GROUP_CODE = 'QEH';
alter table WARD add constraint FK_WARD_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);
alter table WARD_STOCK add constraint FK_WARD_STOCK_01 foreign key (WORKSTORE_GROUP_CODE,WARD_CODE) references WARD (WORKSTORE_GROUP_CODE,WARD_CODE);
--//

