alter table CHEST_ITEM rename column DOSE_QTY to DEL_DOSE_QTY;
alter table CHEST_ITEM add DOSE_QTY number(10,3);
update CHEST_ITEM set DOSE_QTY = DEL_DOSE_QTY;
alter table CHEST_ITEM drop column DEL_DOSE_QTY;

alter table CHEST_ITEM modify DOSE_QTY not null;
--//@UNDO
--//