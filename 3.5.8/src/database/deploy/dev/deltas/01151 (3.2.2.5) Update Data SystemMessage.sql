update SYSTEM_MESSAGE set MAIN_MSG = 'The selected drug item [#0] is associated to an active invoice (Invoice No.: [#1]) of IPSFI Charging Module.  Please void the associated invoice before the label deletion.' where MESSAGE_CODE = '0754';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0756',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Please input base unit',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');


--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'The selected drug item [#0] is associated to an active invoice (Invoice No.: [#1]) of IPSFI Charging Module.  Please void the associated invoice before the label deletion.  Aborted!' where MESSAGE_CODE = '0754';

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0756';
--//