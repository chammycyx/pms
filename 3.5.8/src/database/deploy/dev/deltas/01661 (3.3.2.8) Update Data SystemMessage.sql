update SYSTEM_MESSAGE set MAIN_MSG = 'Due Date must be set before next supply time' where MESSAGE_CODE = '0937';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Due date should be earlier than or equal to next supply date' where MESSAGE_CODE = '0937';
--//