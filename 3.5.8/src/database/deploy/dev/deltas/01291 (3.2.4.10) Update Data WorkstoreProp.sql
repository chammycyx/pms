insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,35014,'N',35014,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

update WORKSTORE_PROP w
set VALUE='Y', UPDATE_DATE=sysdate
where PROP_ID=35014 
and exists (     select 1 
                from PMS.HOSPITAL_PROP h
                where w.HOSP_CODE=h.HOSP_CODE
                and PROP_ID=35014
                and VALUE = 'Y'
            );
            
insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,35015,'N',35015,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

update WORKSTORE_PROP w
set VALUE='Y', UPDATE_DATE=sysdate
where PROP_ID=35015
and exists (     select 1 
                from PMS.HOSPITAL_PROP h
                where w.HOSP_CODE=h.HOSP_CODE
                and PROP_ID=35015
                and VALUE = 'Y'
            );
            
--//@UNDO
delete from WORKSTORE_PROP where PROP_ID in (35014, 35015);
--/