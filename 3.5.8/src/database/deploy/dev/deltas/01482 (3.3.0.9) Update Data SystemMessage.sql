update SYSTEM_MESSAGE set MAIN_MSG = 'PIVAS supply date has been passed for a day.  Confirm to remove the whole worklist?' where MESSAGE_CODE = '0913';
update SYSTEM_MESSAGE set MAIN_MSG = 'System cannot perform clinical intelligence checking for the allergen / ADR / alert records printed on the DH prescription hardcopy.  Please exercise your professional judgement.' where MESSAGE_CODE = '0972';
update SYSTEM_MESSAGE set MAIN_MSG = 'No PIVAS supply will be made and the due date will be updated, PIVAS request will be sent, are you sure you want to proceed?' where MESSAGE_CODE = '0944';
update SYSTEM_MESSAGE set MAIN_MSG = 'The supply date is set after the due date.  Confirm?' where MESSAGE_CODE = '0946';
update SYSTEM_MESSAGE set MAIN_MSG = 'Supply date is set after the due date' where MESSAGE_CODE = '0961';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0980',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'No shelf life.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'The manufacture date has been passed for a day.  Confirm to remove the whole worklist?' where MESSAGE_CODE = '0913';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please check the DH hardcopy prescription for patient''s allergy / ADR / alert information.' where MESSAGE_CODE = '0972';
update SYSTEM_MESSAGE set MAIN_MSG = 'No PIVAS supply will be made and the due date will be updated, PIVAS Request will be sent, are you sure you want to proceed?' where MESSAGE_CODE = '0944';
update SYSTEM_MESSAGE set MAIN_MSG = 'Due date should be later than or equal to supply date.  Confirm to proceed?' where MESSAGE_CODE = '0946';
update SYSTEM_MESSAGE set MAIN_MSG = 'Due date is earlier than supply date' where MESSAGE_CODE = '0961';

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0980';
--//