-- Add Column in PivasBatchPrepItem
alter table PIVAS_BATCH_PREP_ITEM add PIVAS_FORMULA_METHOD_DRUG_ID number(19);

alter table PIVAS_BATCH_PREP_ITEM add DRUG_KEY number(10);
alter table PIVAS_BATCH_PREP_ITEM add DOSAGE_QTY number(19,4);
alter table PIVAS_BATCH_PREP_ITEM add PIVAS_DOSAGE_UNIT varchar2(15);

comment on column PIVAS_BATCH_PREP_ITEM.PIVAS_FORMULA_METHOD_DRUG_ID is 'Pivas formula method drug id';

comment on column PIVAS_BATCH_PREP_ITEM.DRUG_KEY is 'Drug key';
comment on column PIVAS_BATCH_PREP_ITEM.DOSAGE_QTY is 'Dosage qty';
comment on column PIVAS_BATCH_PREP_ITEM.PIVAS_DOSAGE_UNIT is 'Pivas dosage unit';


-- Data Patch in PivasBatchPrepItem
update PIVAS_BATCH_PREP_ITEM bpi set (PIVAS_FORMULA_METHOD_DRUG_ID, DRUG_KEY, DOSAGE_QTY, PIVAS_DOSAGE_UNIT) = 
(select PIVAS_FORMULA_METHOD_ID, DRUG_KEY, DOSAGE_QTY, PIVAS_DOSAGE_UNIT from PIVAS_BATCH_PREP bp where bp.LOT_NUM = bpi.PIVAS_BATCH_PREP_LOT_NUM);


-- Clean up after Data Patch in PivasBatchPrepItem
alter table PIVAS_BATCH_PREP_ITEM modify PIVAS_FORMULA_METHOD_DRUG_ID not null;

alter table PIVAS_BATCH_PREP_ITEM modify DRUG_KEY not null;
alter table PIVAS_BATCH_PREP_ITEM modify DOSAGE_QTY not null;
alter table PIVAS_BATCH_PREP_ITEM modify PIVAS_DOSAGE_UNIT not null;

alter table PIVAS_BATCH_PREP modify DRUG_KEY null;
update PIVAS_BATCH_PREP set DRUG_KEY = null;

alter table PIVAS_BATCH_PREP set unused column DOSAGE_QTY;
alter table PIVAS_BATCH_PREP set unused column PIVAS_DOSAGE_UNIT;


-- Add/Modify Column in PivasBatchPrep
alter table PIVAS_BATCH_PREP modify PRINT_NAME varchar2(200);

alter table PIVAS_BATCH_PREP add WARN_CODE3 varchar2(4);
alter table PIVAS_BATCH_PREP add WARN_CODE4 varchar2(4);
alter table PIVAS_BATCH_PREP add ITEM_CODE varchar2(10);

alter table PIVAS_BATCH_PREP add PIVAS_FORMULA_TYPE varchar2(1);

update PIVAS_BATCH_PREP set PIVAS_FORMULA_TYPE = 'N' where PIVAS_FORMULA_TYPE is null;
alter table PIVAS_BATCH_PREP modify PIVAS_FORMULA_TYPE not null;

alter table PIVAS_BATCH_PREP add EXPIRY_DATE_TITLE varchar2(1);

update PIVAS_BATCH_PREP set EXPIRY_DATE_TITLE = '1' where EXPIRY_DATE_TITLE is null;
alter table PIVAS_BATCH_PREP modify EXPIRY_DATE_TITLE not null;

comment on column PIVAS_BATCH_PREP.WARN_CODE3 is 'Warning code 3';
comment on column PIVAS_BATCH_PREP.WARN_CODE4 is 'Warning code 4';
comment on column PIVAS_BATCH_PREP.ITEM_CODE is 'Item code';
comment on column PIVAS_BATCH_PREP.PIVAS_FORMULA_TYPE is 'Pivas formula type';
comment on column PIVAS_BATCH_PREP.EXPIRY_DATE_TITLE is 'Expiry date Title';



--//@UNDO

-- Revert Add/Modify Column in PivasBatchPrep
alter table PIVAS_BATCH_PREP modify PRINT_NAME varchar2(100);

alter table PIVAS_BATCH_PREP set unused column WARN_CODE3;
alter table PIVAS_BATCH_PREP set unused column WARN_CODE4;
alter table PIVAS_BATCH_PREP set unused column ITEM_CODE;
alter table PIVAS_BATCH_PREP set unused column PIVAS_FORMULA_TYPE;
alter table PIVAS_BATCH_PREP set unused column EXPIRY_DATE_TITLE;


-- Revert Clean up after Data Patch in PivasBatchPrepItem
alter table PIVAS_BATCH_PREP add DOSAGE_QTY number(19,4);
alter table PIVAS_BATCH_PREP add PIVAS_DOSAGE_UNIT varchar2(15);

comment on column PIVAS_BATCH_PREP.DOSAGE_QTY is 'Dosage qty';
comment on column PIVAS_BATCH_PREP.PIVAS_DOSAGE_UNIT is 'Pivas dosage unit';


-- Revert Data Patch in PivasBatchPrepItem
update PIVAS_BATCH_PREP bp set (DRUG_KEY, DOSAGE_QTY, PIVAS_DOSAGE_UNIT) = 
(select DRUG_KEY, DOSAGE_QTY, PIVAS_DOSAGE_UNIT from PIVAS_BATCH_PREP_ITEM bpi where bpi.PIVAS_BATCH_PREP_LOT_NUM = bp.LOT_NUM);

alter table PIVAS_BATCH_PREP modify DRUG_KEY not null;
alter table PIVAS_BATCH_PREP modify DOSAGE_QTY not null;
alter table PIVAS_BATCH_PREP modify PIVAS_DOSAGE_UNIT not null;


-- Revert Add Column in PivasBatchPrepItem
alter table PIVAS_BATCH_PREP_ITEM set unused column PIVAS_FORMULA_METHOD_DRUG_ID;

alter table PIVAS_BATCH_PREP_ITEM set unused column DRUG_KEY;
alter table PIVAS_BATCH_PREP_ITEM set unused column DOSAGE_QTY;
alter table PIVAS_BATCH_PREP_ITEM set unused column PIVAS_DOSAGE_UNIT;

--//