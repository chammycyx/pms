alter table PIVAS_PREP add AD_HOC_FLAG number(1) default 0 not null;
alter table PIVAS_PREP add MED_PROFILE_ID number(19) not null;
alter table PIVAS_PREP add PIVAS_WORKLIST_ID number(19);
alter table PIVAS_PREP drop column EXTRA_XML;

comment on column PIVAS_PREP.AD_HOC_FLAG is 'Ad hoc flag';
comment on column PIVAS_PREP.MED_PROFILE_ID is 'Medication profile id';
comment on column PIVAS_PREP.PIVAS_WORKLIST_ID is 'PIVAS worklist id';

--//@UNDO
alter table PIVAS_PREP drop column AD_HOC_FLAG;
alter table PIVAS_PREP drop column MED_PROFILE_ID;
alter table PIVAS_PREP drop column PIVAS_WORKLIST_ID;
alter table PIVAS_PREP add EXTRA_XML clob;
--//