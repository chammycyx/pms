alter table DISP_ORDER add ITEM_COUNT number(10);
comment on column DISP_ORDER.ITEM_COUNT is 'Item count';

--//@UNDO
alter table DISP_ORDER set unused column ITEM_COUNT;
--//