alter table MED_ORDER add CMS_EXTRA_XML clob;
alter table MED_ORDER_ITEM add CMS_EXTRA_XML clob;

comment on column MED_ORDER.CMS_EXTRA_XML is 'CMS extra xml';
comment on column MED_ORDER_ITEM.CMS_EXTRA_XML is 'CMS extra xml';

--//@UNDO
alter table MED_ORDER drop column CMS_EXTRA_XML;
alter table MED_ORDER_ITEM drop column CMS_EXTRA_XML;
--//