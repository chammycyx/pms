alter table REFILL_CONFIG add ENABLE_ADJ_HOLIDAY_FLAG number(1) null;
alter table REFILL_CONFIG add ENABLE_ADJ_SUNDAY_FLAG number(1) null;
alter table REFILL_CONFIG add COUPON_PRINT_COPIES number(1) null;

comment on column REFILL_CONFIG.ENABLE_ADJ_HOLIDAY_FLAG is 'Enable adjustment holiday flag';
comment on column REFILL_CONFIG.ENABLE_ADJ_SUNDAY_FLAG is 'Enable adjustment sunday flag';
comment on column REFILL_CONFIG.COUPON_PRINT_COPIES is 'Copies of coupons printed';

update REFILL_CONFIG rc
 set rc.ENABLE_ADJ_HOLIDAY_FLAG = (select 1 from WORKSTORE_PROP wp, PROP p
                                    where wp.PROP_ID = p.ID
                                    and wp.HOSP_CODE = rc.HOSP_CODE
                                    and wp.WORKSTORE_CODE = rc.WORKSTORE_CODE
                                    and p.ID = 25002
                                    and wp.VALUE = 'Y');
                                    
update REFILL_CONFIG rc
SET rc.ENABLE_ADJ_HOLIDAY_FLAG = 0 where rc.ENABLE_ADJ_HOLIDAY_FLAG is null;

update REFILL_CONFIG rc
 set rc.ENABLE_ADJ_SUNDAY_FLAG = (select 1 from WORKSTORE_PROP wp, PROP p
                                    where wp.PROP_ID = p.ID
                                    and wp.HOSP_CODE = rc.HOSP_CODE
                                    and wp.WORKSTORE_CODE = rc.WORKSTORE_CODE
                                    and p.ID = 25003
                                    and wp.VALUE = 'Y');
                                    
update REFILL_CONFIG rc
 set rc.ENABLE_ADJ_SUNDAY_FLAG = 0 where rc.ENABLE_ADJ_SUNDAY_FLAG is null;

update REFILL_CONFIG rc
 set rc.COUPON_PRINT_COPIES = (select wp.VALUE from WORKSTORE_PROP wp, PROP p
                                where wp.PROP_ID = p.ID
                                and wp.HOSP_CODE = rc.HOSP_CODE
                                and wp.WORKSTORE_CODE = rc.WORKSTORE_CODE
                                and p.ID = 25006);

alter table REFILL_CONFIG modify ENABLE_ADJ_HOLIDAY_FLAG not null;
alter table REFILL_CONFIG modify ENABLE_ADJ_SUNDAY_FLAG not null;
alter table REFILL_CONFIG modify COUPON_PRINT_COPIES not null;

--//@UNDO
alter table REFILL_CONFIG drop column ENABLE_ADJ_HOLIDAY_FLAG;
alter table REFILL_CONFIG drop column ENABLE_ADJ_SUNDAY_FLAG;
alter table REFILL_CONFIG drop column COUPON_PRINT_COPIES;
--//