alter table MED_PROFILE_STAT rename column CMM_FLAG to CHEMO_FLAG;

comment on column MED_PROFILE_STAT.CHEMO_FLAG is 'Contain chemo item(s)';

--//@UNDO
alter table MED_PROFILE_STAT rename column CHEMO_FLAG to CMM_FLAG;

comment on column MED_PROFILE_STAT.CMM_FLAG is 'Contain CMM item(s)';
--//