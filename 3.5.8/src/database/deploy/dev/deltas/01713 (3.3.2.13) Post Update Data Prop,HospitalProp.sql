delete from CORPORATE_PROP where PROP_ID in (14028);
delete from PROP where ID in (14028);

--//@UNDO
insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (14028,'charging.fcs.gsDirectPayment.rollout','[to be obsolete] FCS rollout: Enhancement of GS direct payment','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (14028,14028,'Y',14028,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
--//