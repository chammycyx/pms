alter table MED_ORDER add VET_DATE timestamp null;
comment on column MED_ORDER.VET_DATE is 'Vetted date';
update MED_ORDER set VET_DATE=CREATE_DATE where VET_FLAG=1;
alter table MED_ORDER drop column VET_FLAG;

--//@UNDO
alter table MED_ORDER add VET_FLAG number(1) default 0 not null;
update MED_ORDER set VET_FLAG=1 where VET_DATE is not null;
alter table MED_ORDER drop column VET_DATE;
--//
