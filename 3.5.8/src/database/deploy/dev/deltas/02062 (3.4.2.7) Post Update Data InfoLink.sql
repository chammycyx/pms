insert into INFO_LINK (ID, NAME, URL, SORT_SEQ) values (3, 'TouchMed PIL Enquiry', 'http://xxx:xxx/pms-cddb-enquiry/#!pil', '30');
insert into INFO_LINK (ID, NAME, URL, SORT_SEQ) values (4, 'Salient Medication Reminder Enquiry', 'http://xxx:xxx/pms-cddb-enquiry/#!smr', '40');

--//@UNDO
delete INFO_LINK where id = 4;
delete INFO_LINK where id = 3;
--//