alter table PHARM_ORDER_ITEM add ADDITIVE_NUM number(10);
alter table PHARM_ORDER_ITEM add FLUID_NUM number(10);

comment on column PHARM_ORDER_ITEM.ADDITIVE_NUM is 'Additive number';
comment on column PHARM_ORDER_ITEM.FLUID_NUM is 'Fluid number';

--//@UNDO
alter table PHARM_ORDER_ITEM drop column ADDITIVE_NUM;
alter table PHARM_ORDER_ITEM drop column FLUID_NUM;
--//