insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1011',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Multiple dose conversion maintenance is not allowed for single line order.  Please use dosage conversion maintenance.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set SUPPL_MSG = 'Item Code: #0<br/>Dispensing Dosage: #1<br/>Dispensing Dosage Unit: #2<br/>Daily Frequency: #3<br/>Frequency Value 1: #4<br/>Suppl. Frequency: #5<br/>Frequency Value 2: #6<br/>Frequency Value 3: #7' where MESSAGE_CODE = '0233';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please add pharmacy line item to conversion set [#0]' where MESSAGE_CODE = '0263';
update SYSTEM_MESSAGE set MAIN_MSG = 'Prescribing dosage unit of this item has changed and the dosage conversion record is no longer valid.  Please delete the record and create a new one using the current prescribing dosage unit.' where MESSAGE_CODE = '0515';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1011';

update SYSTEM_MESSAGE set SUPPL_MSG = 'Item Code: #0<br/>Dispensed Dosage: #1<br/>Dispensed Unit: #2<br/>Daily Frequency: #3<br/>Frequency Value 1: #4<br/>Suppl. Frequency: #5<br/>Frequency Value 2: #6<br/>Frequency Value 3: #7' where MESSAGE_CODE = '0233';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please add pharmacy item to conversion set [#0]' where MESSAGE_CODE = '0263';
update SYSTEM_MESSAGE set MAIN_MSG = 'Dosage unit of this item has changed and the dosage conversion record is no longer valid.  Please delete the record and create a new one using the current dosage unit.' where MESSAGE_CODE = '0515';
--//