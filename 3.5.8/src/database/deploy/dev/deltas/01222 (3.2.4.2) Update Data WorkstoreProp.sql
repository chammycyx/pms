update WORKSTORE_PROP wp1
set VALUE='3000', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate
where PROP_ID=30006
and VALUE='4999'
and exists (     select 1
                from OPERATION_PROFILE op, WORKSTORE W
                where op.ID=w.OPERATION_PROFILE_ID
                and wp1.HOSP_CODE=w.HOSP_CODE
                and wp1.WORKSTORE_CODE=w.WORKSTORE_CODE
                and w.STATUS='A'
                and (op.TYPE in ('E', 'N') or (op.type = 'P' and op.version > 1))
            )
and exists (    select 1
                from WORKSTORE_PROP wp2
                where wp1.HOSP_CODE=wp2.HOSP_CODE
                and wp1.WORKSTORE_CODE=wp2.WORKSTORE_CODE
                and PROP_ID=30007
                and to_number(VALUE) between 1 and 2001
            );

update WORKSTORE_PROP
set VALUE='1', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate
where PROP_ID in (30009, 30010)
and VALUE='0';

--//@UNDO
update WORKSTORE_PROP wp1
set VALUE='4999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate
where PROP_ID=30006
and VALUE='3000'
and UPDATE_USER='pmsadmin'
and sysdate - UPDATE_DATE <= interval '1' day
and exists (     select 1
                from OPERATION_PROFILE op, WORKSTORE W
                where op.ID=w.OPERATION_PROFILE_ID
                and wp1.HOSP_CODE=w.HOSP_CODE
                and wp1.WORKSTORE_CODE=w.WORKSTORE_CODE
                and w.STATUS='A'
                and (op.TYPE in ('E', 'N') or (op.type='P' and op.version > 1))
            )
and exists (     select 1
                from WORKSTORE_PROP wp2
                where wp1.HOSP_CODE=wp2.HOSP_CODE
                and wp1.WORKSTORE_CODE=wp2.WORKSTORE_CODE
                and PROP_ID=30007
                and to_number(VALUE) between 1 and 2001
            );
            
update WORKSTORE_PROP
set VALUE='0', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate
where PROP_ID in (30009, 30010)
and VALUE='1'
and UPDATE_USER='pmsadmin'
and UPDATE_DATE=sysdate-1;
--//