alter table DELIVERY_ITEM add ADJ_QTY number(19,4) null;

comment on column DELIVERY_ITEM.ADJ_QTY is 'Adjusted quantity';

--//@UNDO
alter table DELIVERY_ITEM drop column ADJ_QTY;
--//