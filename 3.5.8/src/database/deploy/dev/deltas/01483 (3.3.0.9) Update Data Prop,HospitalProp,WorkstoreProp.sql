update PROP set DESCRIPTION='Enable PAS Patient enquiry in Checking and Issuing', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=16004;
update PROP set DESCRIPTION='List to be set focus in Checking and Issuing', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=16012;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (15007,'check.dhAlert.popup','Popup DH alert enquiry screen in Checking','Y,N','B',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,15007,'Y',15007,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (15008,'check.mp.patient.pas.patient.enabled','Enable PAS Patient enquiry in IP Checking','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (15008,15008,'N',15008,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (19004,'issue.dhAlert.popup','Popup DH alert enquiry screen in Issuing','Y,N','B',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,19004,'Y',19004,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP set DESCRIPTION='Enable PAS Patient enquiry for Checking and Issuing', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=16004;
update PROP set DESCRIPTION='List to be set focus in Checking & Issuing', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=16012;

delete from CORPORATE_PROP where PROP_ID in (15008);
delete from WORKSTORE_PROP where PROP_ID in (15007, 19004);

delete from PROP where ID in (15007, 15008, 19004);
--//