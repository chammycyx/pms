update DELIVERY_ITEM di
set di.MED_PROFILE_ID = 
(select mp.ID from MED_PROFILE mp 
join MED_PROFILE_ITEM mpi on (mpi.MED_PROFILE_ID = mp.ID) 
join MED_PROFILE_MO_ITEM mpmi on (mpmi.MED_PROFILE_ITEM_ID = mpi.ID)
join MED_PROFILE_PO_ITEM mppi on (mppi.MED_PROFILE_MO_ITEM_ID = mpmi.ID) 
where mppi.ID = di.MED_PROFILE_PO_ITEM_ID)
where di.MED_PROFILE_ID is null;

alter table DELIVERY_ITEM modify MED_PROFILE_ID not null;


--//@UNDO
alter table DELIVERY_ITEM modify MED_PROFILE_ID null;
--//