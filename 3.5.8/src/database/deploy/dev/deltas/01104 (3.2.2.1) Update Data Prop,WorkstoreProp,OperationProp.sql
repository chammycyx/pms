update PROP set NAME='ticketGen.ticket.pharmLocationChi', UPDATE_DATE=sysdate where ID=30003;
update PROP set NAME='ticketGen.ticket.pharmLocationEng', UPDATE_DATE=sysdate where ID=30004;
update PROP set VALIDATION='r3,c150', UPDATE_DATE=sysdate where ID=27008;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (30018,'ticketGen.ticket.remark','Information of ticket','0-60','S',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into OPERATION_PROP (ID,SORT_SEQ,PROP_ID,VALUE,OPERATION_PROFILE_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_OPERATION_PROP.nextval,30018,30018,null,ID,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select ID from OPERATION_PROFILE);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (30019,'ticketGen.batch.enabled','Enable batch Ticket Generation','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O','E',0);

insert all into OPERATION_PROP (ID,SORT_SEQ,PROP_ID,VALUE,OPERATION_PROFILE_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_OPERATION_PROP.nextval,30019,30019,'N',ID,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select ID from OPERATION_PROFILE);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (30020,'ticketGen.batch.noOfTicket','Number of ticket(s) printed for batch Ticket Generation','1-50','I',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O','E',0);

insert all into OPERATION_PROP (ID,SORT_SEQ,PROP_ID,VALUE,OPERATION_PROFILE_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_OPERATION_PROP.nextval,30020,30020,'10',ID,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select ID from OPERATION_PROFILE);

update OPERATION_PROP
set VALUE='50'
where PROP_ID=30020 
and exists (     select 1 from OPERATION_PROFILE
                where OPERATION_PROP.OPERATION_PROFILE_ID = OPERATION_PROFILE.ID
                and TYPE='P'
            );

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (21009,'machine.scriptPro.message.encoding','[to be obsolete] Encoding of ScriptPro message','Big5-HKSCS,UTF-8','S',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,21009,'Big5-HKSCS',21009,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

--//@UNDO
update PROP set NAME='ticketGen.ticket.remarkChi', UPDATE_DATE=sysdate where ID=30003;
update PROP set NAME='ticketGen.ticket.remarkEng', UPDATE_DATE=sysdate where ID=30004;
update PROP set VALIDATION='r3,c50', UPDATE_DATE=sysdate where ID=27008;

delete from OPERATION_PROP where PROP_ID in (30018, 30019, 30020);
delete from PROP where ID in (30018, 30019, 30020);

delete from WORKSTORE_PROP where PROP_ID in (21009);
delete from PROP where ID in (21009);

--//