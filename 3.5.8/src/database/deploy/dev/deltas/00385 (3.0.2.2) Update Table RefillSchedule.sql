alter table REFILL_SCHEDULE drop column FORCE_COMPLETE_DATE;

--//@UNDO
alter table REFILL_SCHEDULE add FORCE_COMPLETE_DATE date null;
comment on column REFILL_SCHEDULE.FORCE_COMPLETE_DATE is 'Force completed date';
--//