alter table PURCHASE_REQUEST add INVOICE_XML CLOB null;

comment on column PURCHASE_REQUEST.INVOICE_XML is 'Invoice and invoice item in XML format';

--//@UNDO
alter table PURCHASE_REQUEST set unused column INVOICE_XML;
--//