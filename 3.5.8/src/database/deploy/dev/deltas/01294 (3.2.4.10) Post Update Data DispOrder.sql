update DISP_ORDER do set do.REFRIG_ITEM_FLAG = 1 where exists 
(select doi.ID from DISP_ORDER_ITEM doi
join PHARM_ORDER_ITEM poi on (poi.ID = doi.PHARM_ORDER_ITEM_ID)
where (poi.WARN_CODE_1 in (' 1','35','3K','48','4U','5R','I1','8T')
or poi.WARN_CODE_2 in (' 1','35','3K','48','4U','5R','I1','8T')
or poi.WARN_CODE_3 in (' 1','35','3K','48','4U','5R','I1','8T')
or poi.WARN_CODE_4 in (' 1','35','3K','48','4U','5R','I1','8T'))
and doi.DISP_QTY > 0
and doi.DISP_ORDER_ID = do.ID)
and do.STATUS not in ('X')
and do.REFRIG_ITEM_FLAG is null 
and do.CREATE_DATE between to_date('20120101', 'yyyymmdd') and to_date('20160101', 'yyyymmdd');

--//@UNDO
--//