alter table MED_PROFILE_ORDER add TRANSFER_DATE timestamp;
alter table MED_PROFILE_ORDER add PREV_PAS_WARD_CODE varchar2(4);

comment on column MED_PROFILE_ORDER.TRANSFER_DATE is 'Transfer Date';
comment on column MED_PROFILE_ORDER.PREV_PAS_WARD_CODE is 'Previous PAS Ward Code';

--//@UNDO
alter table MED_PROFILE_ORDER drop column TRANSFER_DATE;
alter table MED_PROFILE_ORDER drop column PREV_PAS_WARD_CODE;
--//