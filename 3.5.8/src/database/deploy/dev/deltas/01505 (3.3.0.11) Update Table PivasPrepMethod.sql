alter table PIVAS_PREP_METHOD add DILUENT_CODE varchar2(25);
alter table PIVAS_PREP_METHOD add DILUENT_DESC varchar2(100);

comment on column PIVAS_PREP_METHOD.DILUENT_CODE is 'Diluent code';
comment on column PIVAS_PREP_METHOD.DILUENT_DESC is 'Diluent description';

--//@UNDO
alter table PIVAS_PREP_METHOD drop column DILUENT_CODE;
alter table PIVAS_PREP_METHOD drop column DILUENT_DESC;
--//