alter table FOLLOW_UP_WARNING add MODIFY_DATE timestamp null;

comment on column FOLLOW_UP_WARNING.MODIFY_DATE is 'Modified date';

update FOLLOW_UP_WARNING set MODIFY_DATE = UPDATE_DATE where MODIFY_DATE is null;

alter table FOLLOW_UP_WARNING modify MODIFY_DATE not null;

--//@UNDO
alter table FOLLOW_UP_WARNING drop column MODIFY_DATE;
--//