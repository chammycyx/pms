insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0782',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Cannot mark uncollected : Refill coupon has been processed by dayend',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0783',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Cannot mark uncollected : Manual prescription has been processed by dayend',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0784',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Cannot mark uncollected : Refill coupon has already been marked as suspend',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected : Manual prescription has already been marked as suspend' where MESSAGE_CODE = '0770';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0782';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0783';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0784';

update SYSTEM_MESSAGE set MAIN_MSG = 'Manual prescription cannot be marked as uncollect' where MESSAGE_CODE = '0770';
--//