alter table MED_PROFILE_ORDER add PROTOCOL_CODE varchar2(20);
alter table MED_PROFILE_ORDER add PROTOCOL_VERSION number(10);

comment on column MED_PROFILE_ORDER.PROTOCOL_CODE is 'Chemo Protocol code';
comment on column MED_PROFILE_ORDER.PROTOCOL_VERSION is 'Chemo Protocol version';

--//@UNDO
alter table MED_PROFILE_ORDER set unused column PROTOCOL_CODE;
alter table MED_PROFILE_ORDER set unused column PROTOCOL_VERSION;
--//