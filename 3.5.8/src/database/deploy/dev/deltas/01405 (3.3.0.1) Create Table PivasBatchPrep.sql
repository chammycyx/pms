create table PIVAS_BATCH_PREP (
    LOT_NUM varchar2(14) not null,
    PIVAS_FORMULA_METHOD_ID number(19) not null,
    MANUFACTURE_DATE timestamp not null,
    DOSAGE_QTY number(19,4) not null,
    PIVAS_DOSAGE_UNIT varchar2(15) not null,
    VOLUME number(19,4) not null,
    PRINT_NAME varchar2(100) not null,
    SHELF_LIFE number(10) not null,
    PREP_EXPIRY_DATE timestamp not null,
    PREP_QTY number(19,4) not null,
    NUM_OF_LABEL number(3) not null,
    PIVAS_CONTAINER_ID number(19) not null,
    DILUENT_ITEM_CODE varchar2(10),
    DILUENT_MANUF_CODE varchar2(4),
    DILUENT_BATCH_NUM varchar2(20),
    DILUENT_EXPIRY_DATE date,
    WARN_CODE1 varchar2(4),
    WARN_CODE2 varchar2(4),
    CONCN number(19,4),
    DRUG_KEY number(10) not null,
      STATUS varchar2(1) not null,
      PIVAS_WORKSHEET_XML clob null,
      PIVAS_PRODUCT_LABEL_XML clob null,
    HOSP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_BATCH_PREP primary key (LOT_NUM) using index tablespace PMS_INDX_01,
    constraint UI_PIVAS_BATCH_PREP_01 unique (HOSP_CODE, LOT_NUM) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_BATCH_PREP is 'Pivas batch preparation';
comment on column PIVAS_BATCH_PREP.LOT_NUM is 'Lot number';
comment on column PIVAS_BATCH_PREP.PIVAS_FORMULA_METHOD_ID is 'Pivas formula method id';
comment on column PIVAS_BATCH_PREP.MANUFACTURE_DATE is 'Manufacture date';
comment on column PIVAS_BATCH_PREP.DOSAGE_QTY is 'Dosage qty';
comment on column PIVAS_BATCH_PREP.PIVAS_DOSAGE_UNIT is 'Pivas dosage unit';
comment on column PIVAS_BATCH_PREP.VOLUME is 'Volume';
comment on column PIVAS_BATCH_PREP.PRINT_NAME is 'Print name';
comment on column PIVAS_BATCH_PREP.SHELF_LIFE is 'Shelf life';
comment on column PIVAS_BATCH_PREP.PREP_EXPIRY_DATE is 'Preparation expiry date';
comment on column PIVAS_BATCH_PREP.PREP_QTY is 'Preparation qty';
comment on column PIVAS_BATCH_PREP.NUM_OF_LABEL is 'Number of label';
comment on column PIVAS_BATCH_PREP.PIVAS_CONTAINER_ID is 'Pivas container id';
comment on column PIVAS_BATCH_PREP.DILUENT_ITEM_CODE is 'Solvent item code';
comment on column PIVAS_BATCH_PREP.DILUENT_MANUF_CODE is 'Solvent manufacturer';
comment on column PIVAS_BATCH_PREP.DILUENT_BATCH_NUM is 'Diluent batch number';
comment on column PIVAS_BATCH_PREP.DILUENT_EXPIRY_DATE is 'Diluent expiry date';
comment on column PIVAS_BATCH_PREP.WARN_CODE1 is 'Warning code 1';
comment on column PIVAS_BATCH_PREP.WARN_CODE2 is 'Warning code 2';
comment on column PIVAS_BATCH_PREP.CONCN is 'Concentration';
comment on column PIVAS_BATCH_PREP.DRUG_KEY is 'Drug key';
comment on column PIVAS_BATCH_PREP.STATUS is 'Status';
comment on column PIVAS_BATCH_PREP.PIVAS_WORKSHEET_XML is 'pivas worksheet xml';
comment on column PIVAS_BATCH_PREP.PIVAS_PRODUCT_LABEL_XML is 'pivas product label xml';
comment on column PIVAS_BATCH_PREP.HOSP_CODE is 'Hospital code';
comment on column PIVAS_BATCH_PREP.CREATE_USER is 'Created user';
comment on column PIVAS_BATCH_PREP.CREATE_DATE is 'Created date';
comment on column PIVAS_BATCH_PREP.UPDATE_USER is 'Last updated user';
comment on column PIVAS_BATCH_PREP.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_BATCH_PREP.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table PIVAS_BATCH_PREP;
--//