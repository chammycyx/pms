insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (6,'pmsadmin','Urgent administration of drug','UA','F',sysdate,sysdate,'pmsadmin',1);
insert into CHARGE_REASON (ID,CREATE_USER,DESCRIPTION,REASON_CODE,TYPE,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) values (7,'pmsadmin','Defer payment for Private Inpatient','DP','F',sysdate,sysdate,'pmsadmin',1);

--//@UNDO
delete CHARGE_REASON where ID = 6;
delete CHARGE_REASON where ID = 7;
--//