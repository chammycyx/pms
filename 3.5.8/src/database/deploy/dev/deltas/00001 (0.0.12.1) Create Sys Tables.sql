create table AUDIT_LOG (
    ID number(19) not null,
    HOSP_CODE varchar2(3) null,
    WORKSTORE_CODE varchar2(4) null,
    WORKSTATION_ID varchar2(30) null,
    USER_ID varchar2(20) null,
    APPLICATION_ID number(10) null,
    REQUEST_URI varchar2(500) null,
    TRAN_ID varchar2(50) null,
    ENTITY_NAME varchar2(100) null,
    MESSAGE_ID varchar2(5) null,
    MESSAGE varchar2(1000) not null,
    DETAIL_MESSAGE clob null,
    CREATE_DATE timestamp not null,
    PACKAGE_NAME varchar2(100) null,
    CLASS_NAME varchar2(100) null,
    METHOD_NAME varchar2(100) null,
    LINE_NUMBER varchar2(20) null,
    FILE_NAME varchar2(100) null,
    ELAPSE_TIME number(10) null,
    LOG_TYPE varchar2(1) not null,
    HOST_NAME varchar2(50) null,
    constraint PK_AUDIT_LOG primary key (ID) using index tablespace PMS_LOG_INDX_01)
partition by range (CREATE_DATE) (
    partition P_PMS_LOG_DATA_2012 values less than (maxvalue) tablespace P_PMS_LOG_DATA_2012);

create index I_AUDIT_LOG_01 on AUDIT_LOG (CREATE_DATE) local;

comment on table  AUDIT_LOG is 'Audit logs written by audit logger';
comment on column AUDIT_LOG.ID is 'Audit log id';
comment on column AUDIT_LOG.HOSP_CODE is 'Hospital code';
comment on column AUDIT_LOG.WORKSTORE_CODE is 'Workstore code';
comment on column AUDIT_LOG.WORKSTATION_ID is 'Workstation id';
comment on column AUDIT_LOG.USER_ID is 'Actioned user';
comment on column AUDIT_LOG.APPLICATION_ID is 'Application id';
comment on column AUDIT_LOG.REQUEST_URI is 'Access URL';
comment on column AUDIT_LOG.TRAN_ID is 'EclipseLink customizer - unit of work';
comment on column AUDIT_LOG.ENTITY_NAME is 'Java entity name';
comment on column AUDIT_LOG.MESSAGE_ID is 'System message code';
comment on column AUDIT_LOG.MESSAGE is 'Log message';
comment on column AUDIT_LOG.DETAIL_MESSAGE is 'Log message more than 1000 characters';
comment on column AUDIT_LOG.CREATE_DATE is 'Created date';
comment on column AUDIT_LOG.CLASS_NAME is 'Java class name';
comment on column AUDIT_LOG.METHOD_NAME is 'Java method name';
comment on column AUDIT_LOG.LINE_NUMBER is 'Line number of Java source code';
comment on column AUDIT_LOG.PACKAGE_NAME is 'Java package name';
comment on column AUDIT_LOG.FILE_NAME is 'Java file name';
comment on column AUDIT_LOG.ELAPSE_TIME is 'Elapsed time';
comment on column AUDIT_LOG.LOG_TYPE is 'Log type';
comment on column AUDIT_LOG.HOST_NAME is 'Workstation name';


create table SYSTEM_MESSAGE (
    MESSAGE_CODE varchar2(5) not null,
    APPLICATION_ID number(10) not null,
    FUNCTION_ID number(10) not null,
    OPERATION_ID number(10) null,
    SEVERITY_CODE varchar2(1) not null,
    MAIN_MSG varchar2(500) not null,
    localE varchar2(5) not null,
    SUPPL_MSG varchar2(500) null,
    DETAIL varchar2(500) null,
    BTN_YES_CAPTION varchar2(30) null,
    BTN_YES_SIZE varchar2(1) null,
    BTN_YES_SHORTCUT varchar2(1) null,
    BTN_NO_CAPTION varchar2(30) null,
    BTN_NO_SIZE varchar2(1) null,
    BTN_NO_SHORTCUT varchar2(1) null,
    BTN_CANCEL_CAPTION varchar2(30) null,
    BTN_CANCEL_SIZE varchar2(1) null,
    BTN_CANCEL_SHORTCUT varchar2(1) null,
    EFFECTIVE timestamp null,
    EXPIRY timestamp null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SYSTEM_MESSAGE primary key (MESSAGE_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  SYSTEM_MESSAGE is 'System messages';
comment on column SYSTEM_MESSAGE.MESSAGE_CODE is 'System message code';
comment on column SYSTEM_MESSAGE.APPLICATION_ID is 'Application id';
comment on column SYSTEM_MESSAGE.FUNCTION_ID is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.OPERATION_ID is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.SEVERITY_CODE is 'Severity code';
comment on column SYSTEM_MESSAGE.MAIN_MSG is 'System message content';
comment on column SYSTEM_MESSAGE.LOCALE is 'Locale';
comment on column SYSTEM_MESSAGE.SUPPL_MSG is 'Supplementary message';
comment on column SYSTEM_MESSAGE.DETAIL is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_YES_CAPTION is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_YES_SIZE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_YES_SHORTCUT is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_NO_CAPTION is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_NO_SIZE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_NO_SHORTCUT is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_CANCEL_CAPTION is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_CANCEL_SIZE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_CANCEL_SHORTCUT is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.EFFECTIVE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.EXPIRY is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.CREATE_USER is 'Created user';
comment on column SYSTEM_MESSAGE.CREATE_DATE is 'Created date';
comment on column SYSTEM_MESSAGE.UPDATE_USER is 'Last updated user';
comment on column SYSTEM_MESSAGE.UPDATE_DATE is 'Last updated date';
comment on column SYSTEM_MESSAGE.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop index I_AUDIT_LOG_01;

drop table AUDIT_LOG;
drop table SYSTEM_MESSAGE;
--//