alter table PURCHASE_REQUEST add HOSP_CODE varchar2(3) null;
alter table PURCHASE_REQUEST add WORKSTORE_CODE varchar2(4) null;

comment on column PURCHASE_REQUEST.HOSP_CODE is 'Hospital code';
comment on column PURCHASE_REQUEST.WORKSTORE_CODE is 'Workstore code';

--//@UNDO
alter table PURCHASE_REQUEST drop column HOSP_CODE;
alter table PURCHASE_REQUEST drop column WORKSTORE_CODE;
--//