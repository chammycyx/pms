alter table DISP_ORDER_ITEM add SORT_SEQ number(10) null;

comment on column DISP_ORDER_ITEM.SORT_SEQ is 'Sort sequence';

--//@UNDO
alter table DISP_ORDER_ITEM drop column SORT_SEQ;
--//
