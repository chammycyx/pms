alter table REFILL_SCHEDULE add USE_MO_ITEM_NUM_FLAG number(1) default 0;

comment on column REFILL_SCHEDULE.USE_MO_ITEM_NUM_FLAG is 'Determine to use MO Item Num as Group Num';

--//@UNDO
alter table REFILL_SCHEDULE drop column USE_MO_ITEM_NUM_FLAG;
--//