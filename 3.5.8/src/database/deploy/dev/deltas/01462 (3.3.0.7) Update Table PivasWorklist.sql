alter table PIVAS_WORKLIST add NUM_OF_LABEL number(10);

comment on column PIVAS_WORKLIST.NUM_OF_LABEL is 'Number of label';

--//@UNDO
alter table PIVAS_WORKLIST drop column NUM_OF_LABEL;
--//