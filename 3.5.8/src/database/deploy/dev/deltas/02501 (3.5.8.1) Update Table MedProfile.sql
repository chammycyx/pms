alter table MED_PROFILE add TYPE varchar2(1);
alter table MED_PROFILE add PROTOCOL_CODE varchar2(20);
alter table MED_PROFILE add PROTOCOL_VERSION number(10);

comment on column MED_PROFILE.TYPE is 'Medication Profile type';
comment on column MED_PROFILE.PROTOCOL_CODE is 'Chemo Protocol code';
comment on column MED_PROFILE.PROTOCOL_VERSION is 'Chemo Protocol version';

--//@UNDO
alter table MED_PROFILE set unused column TYPE;
alter table MED_PROFILE set unused column PROTOCOL_CODE;
alter table MED_PROFILE set unused column PROTOCOL_VERSION;
--//