update PROP set MAINT_SCREEN = 'S', UPDATE_DATE=sysdate where ID in (30019, 30020);
update PROP set VALIDATION = '1-100', UPDATE_DATE=sysdate where ID = 30020;

update OPERATION_PROP
set VALUE='1', UPDATE_DATE=sysdate
where PROP_ID=30020 
and exists (     select 1 from OPERATION_PROFILE
                where OPERATION_PROP.OPERATION_PROFILE_ID = OPERATION_PROFILE.ID
                and TYPE='E'
            );

--//@UNDO
update PROP set MAINT_SCREEN = 'O', UPDATE_DATE=sysdate where ID in (30019, 30020);
update PROP set VALIDATION = '1-50', UPDATE_DATE=sysdate where ID = 30020;

update OPERATION_PROP
set VALUE='10', UPDATE_DATE=sysdate, UPDATE_USER='pmsadmin'
where PROP_ID=30020 
and exists (     select 1 from OPERATION_PROFILE
                where OPERATION_PROP.OPERATION_PROFILE_ID = OPERATION_PROFILE.ID
                and TYPE='E'
            );
--//