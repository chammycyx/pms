alter table PIVAS_WORKLIST add FIRST_DOSE_MATERIAL_REQUEST_ID number(19) null;
alter table PIVAS_WORKLIST add SATELLITE_FLAG number(1) default 0 not null;
alter table PIVAS_WORKLIST add SHELF_LIFE number(10);
alter table PIVAS_WORKLIST add PIVAS_PREP_METHOD_ID number(19);

comment on column PIVAS_WORKLIST.FIRST_DOSE_MATERIAL_REQUEST_ID is 'First dose material request';
comment on column PIVAS_WORKLIST.SATELLITE_FLAG is 'Satellite flag';
comment on column PIVAS_WORKLIST.SHELF_LIFE is 'Shelf life';
comment on column PIVAS_WORKLIST.PIVAS_PREP_METHOD_ID is 'Pivas preparation method id';

alter table PIVAS_WORKLIST add constraint FK_PIVAS_WORKLIST_06 foreign key (PIVAS_PREP_METHOD_ID) references PIVAS_PREP_METHOD (ID);
create index FK_PIVAS_WORKLIST_06 on PIVAS_WORKLIST (PIVAS_PREP_METHOD_ID) tablespace PMS_INDX_01;

--//@UNDO
alter table PIVAS_WORKLIST drop column FIRST_DOSE_MATERIAL_REQUEST_ID;
alter table PIVAS_WORKLIST drop column SATELLITE_FLAG;
alter table PIVAS_WORKLIST drop column SHELF_LIFE;
alter table PIVAS_WORKLIST drop column PIVAS_PREP_METHOD_ID;
--//