update PROP set DESCRIPTION = 'Enable urgent print', UPDATE_DATE=sysdate where ID=32020;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (20018,'label.mpDispLabel.ownStockItem.printing.enabled','Enable printing of IP dispensing label for own stock item','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,20018,'N',20018,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION = 'Enable urgent printing', UPDATE_DATE=sysdate where ID=32020;

delete from WORKSTORE_PROP where PROP_ID in (20018);
delete from PROP where ID in (20018);    
--//