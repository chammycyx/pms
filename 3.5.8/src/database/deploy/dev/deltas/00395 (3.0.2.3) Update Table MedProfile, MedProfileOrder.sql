alter table MED_PROFILE modify BODY_HEIGHT number(5,2);
alter table MED_PROFILE modify BODY_WEIGHT number(5,2);
alter table MED_PROFILE_ORDER modify BODY_HEIGHT number(5,2);
alter table MED_PROFILE_ORDER modify BODY_WEIGHT number(5,2);

--//@UNDO
alter table MED_PROFILE rename column BODY_HEIGHT to DEL_BODY_HEIGHT;
alter table MED_PROFILE rename column BODY_WEIGHT to DEL_BODY_WEIGHT;
alter table MED_PROFILE_ORDER rename column BODY_HEIGHT to DEL_BODY_HEIGHT;
alter table MED_PROFILE_ORDER rename column BODY_WEIGHT to DEL_BODY_WEIGHT;

alter table MED_PROFILE add BODY_HEIGHT number(4,1);
alter table MED_PROFILE add BODY_WEIGHT number(4,1);
alter table MED_PROFILE_ORDER add BODY_HEIGHT number(4,1);
alter table MED_PROFILE_ORDER add BODY_WEIGHT number(4,1);

update MED_PROFILE set BODY_HEIGHT = DEL_BODY_HEIGHT, BODY_WEIGHT = DEL_BODY_WEIGHT;
update MED_PROFILE_ORDER set BODY_HEIGHT = DEL_BODY_HEIGHT, BODY_WEIGHT = DEL_BODY_WEIGHT;

alter table MED_PROFILE drop column DEL_BODY_HEIGHT;
alter table MED_PROFILE drop column DEL_BODY_WEIGHT;
alter table MED_PROFILE_ORDER drop column DEL_BODY_HEIGHT;
alter table MED_PROFILE_ORDER drop column DEL_BODY_WEIGHT;
--//