-- Sys

create sequence SQ_AUDIT_LOG start with 10000000000000;

-- Ref

create sequence SQ_CHARGE_REASON increment by 50 start with 10000000000049;
create sequence SQ_CHARGE_SPECIALTY increment by 50 start with 10000000000049;
create sequence SQ_CHEST increment by 50 start with 10000000000049;
create sequence SQ_CHEST_ITEM increment by 50 start with 10000000000049;
create sequence SQ_CORPORATE_PROP increment by 50 start with 10000000000049;
create sequence SQ_CUSTOM_LABEL increment by 50 start with 10000000000049;
create sequence SQ_FDN_MAPPING increment by 50 start with 10000000000049;
create sequence SQ_FOLLOW_UP_WARNING increment by 50 start with 10000000000049;
create sequence SQ_HOSPITAL_PROP increment by 50 start with 10000000000049;
create sequence SQ_ITEM_DISP_CONFIG increment by 50 start with 10000000000049;
create sequence SQ_ITEM_LOCATION increment by 50 start with 10000000000049;
create sequence SQ_ITEM_SPECIALTY increment by 50 start with 10000000000049;
create sequence SQ_LOCAL_WARNING increment by 50 start with 10000000000049;
create sequence SQ_MACHINE increment by 50 start with 10000000000049;
create sequence SQ_OPERATION_PROFILE increment by 50 start with 10000000000049;
create sequence SQ_OPERATION_PROP increment by 50 start with 10000000000049;
create sequence SQ_OPERATION_WORKSTATION increment by 50 start with 10000000000049;
create sequence SQ_PENDING_REASON increment by 50 start with 10000000000049;
create sequence SQ_PHARM_REMARK_TEMPLATE increment by 50 start with 10000000000049;
create sequence SQ_PICK_STATION increment by 50 start with 10000000000049;
create sequence SQ_PREPACK increment by 50 start with 10000000000049;
create sequence SQ_PRN_ROUTE increment by 50 start with 10000000000049;
create sequence SQ_PROP increment by 50 start with 10000000000049;
create sequence SQ_REFILL_CONFIG increment by 50 start with 10000000000049;
create sequence SQ_REFILL_HOLIDAY increment by 50 start with 10000000000049;
create sequence SQ_REFILL_QTY_ADJ increment by 50 start with 10000000000049;
create sequence SQ_REFILL_QTY_ADJ_EXCLUSION increment by 50 start with 10000000000049;
create sequence SQ_REFILL_SELECTION increment by 50 start with 10000000000049;
create sequence SQ_SUSPEND_REASON increment by 50 start with 10000000000049;
create sequence SQ_SUSPEND_STAT increment by 50 start with 10000000000049;
create sequence SQ_WARD_CONFIG increment by 50 start with 10000000000049;
create sequence SQ_WARD_FILTER increment by 50 start with 10000000000049;
create sequence SQ_WARD_GROUP increment by 50 start with 10000000000049;
create sequence SQ_WARD_GROUP_ITEM increment by 50 start with 10000000000049;
create sequence SQ_WORKLOAD_STAT increment by 50 start with 10000000000049;
create sequence SQ_WORKSTATION increment by 50 start with 10000000000049;
create sequence SQ_WORKSTATION_PRINT increment by 50 start with 10000000000049;
create sequence SQ_WORKSTATION_PROP increment by 50 start with 10000000000049;
create sequence SQ_WORKSTORE_PROP increment by 50 start with 10000000000049;

-- Disp

create sequence SQ_BATCH_ISSUE_LOG increment by 50 start with 10000000000049;
create sequence SQ_BATCH_ISSUE_LOG_DETAIL increment by 50 start with 10000000000049;
create sequence SQ_CAPD_VOUCHER increment by 50 start with 10000000000049;
create sequence SQ_CAPD_VOUCHER_ITEM increment by 50 start with 10000000000049;
create sequence SQ_DISP_ORDER increment by 50 start with 10000000000049;
create sequence SQ_DISP_ORDER_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER_FM increment by 50 start with 10000000000049;
create sequence SQ_INVOICE increment by 50 start with 10000000000049;
create sequence SQ_INVOICE_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_CASE increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_ORDER_ITEM_ALERT increment by 50 start with 10000000000049;
create sequence SQ_PATIENT increment by 50 start with 10000000000049;
create sequence SQ_PHARM_ORDER increment by 50 start with 10000000000049;
create sequence SQ_PHARM_ORDER_ITEM increment by 50 start with 10000000000049;
create sequence SQ_REFILL_SCHEDULE_ID increment by 50 start with 10000000000049;
create sequence SQ_REFILL_SCHEDULE_ITEM increment by 50 start with 10000000000049;
create sequence SQ_TICKET increment by 50 start with 10000000000049;

-- MedProfile

create sequence SQ_DELIVERY increment by 50 start with 10000000000049;
create sequence SQ_DELIVERY_ITEM increment by 50 start with 10000000000049;
create sequence SQ_DELIVERY_REQUEST increment by 50 start with 10000000000049;
create sequence SQ_DELIVERY_REQUEST_ALERT increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_ERROR_LOG increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_ERROR_STAT increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_MO_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_MO_ITEM_ALERT increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_MO_ITEM_FM increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_ORDER start with 10000000000000;
create sequence SQ_MED_PROFILE_PO_ITEM increment by 50 start with 10000000000049;
create sequence SQ_MED_PROFILE_STAT increment by 50 start with 10000000000049;
create sequence SQ_REPLENISHMENT increment by 50 start with 10000000000049;
create sequence SQ_REPLENISHMENT_ITEM increment by 50 start with 10000000000049;


--//@UNDO
-- Sys

drop sequence SQ_AUDIT_LOG;

-- Ref

drop sequence SQ_CHARGE_REASON;
drop sequence SQ_CHARGE_SPECIALTY;
drop sequence SQ_CHEST;
drop sequence SQ_CHEST_ITEM;
drop sequence SQ_CORPORATE_PROP;
drop sequence SQ_CUSTOM_LABEL;
drop sequence SQ_FDN_MAPPING;
drop sequence SQ_FOLLOW_UP_WARNING;
drop sequence SQ_HOSPITAL_PROP;
drop sequence SQ_ITEM_DISP_CONFIG;
drop sequence SQ_ITEM_LOCATION;
drop sequence SQ_ITEM_SPECIALTY;
drop sequence SQ_LOCAL_WARNING;
drop sequence SQ_MACHINE;
drop sequence SQ_OPERATION_PROFILE;
drop sequence SQ_OPERATION_PROP;
drop sequence SQ_OPERATION_WORKSTATION;
drop sequence SQ_PENDING_REASON;
drop sequence SQ_PHARM_REMARK_TEMPLATE;
drop sequence SQ_PICK_STATION;
drop sequence SQ_PREPACK;
drop sequence SQ_PRN_ROUTE;
drop sequence SQ_PROP;
drop sequence SQ_REFILL_CONFIG;
drop sequence SQ_REFILL_HOLIDAY;
drop sequence SQ_REFILL_QTY_ADJ;
drop sequence SQ_REFILL_QTY_ADJ_EXCLUSION;
drop sequence SQ_REFILL_SELECTION;
drop sequence SQ_SUSPEND_REASON;
drop sequence SQ_SUSPEND_STAT;
drop sequence SQ_WARD_CONFIG;
drop sequence SQ_WARD_FILTER;
drop sequence SQ_WARD_GROUP;
drop sequence SQ_WARD_GROUP_ITEM;
drop sequence SQ_WORKLOAD_STAT;
drop sequence SQ_WORKSTATION;
drop sequence SQ_WORKSTATION_PRINT;
drop sequence SQ_WORKSTATION_PROP;
drop sequence SQ_WORKSTORE_PROP;

-- Disp

drop sequence SQ_BATCH_ISSUE_LOG;
drop sequence SQ_BATCH_ISSUE_LOG_DETAIL;
drop sequence SQ_CAPD_VOUCHER;
drop sequence SQ_CAPD_VOUCHER_ITEM;
drop sequence SQ_DISP_ORDER;
drop sequence SQ_DISP_ORDER_ITEM;
drop sequence SQ_MED_ORDER_FM;
drop sequence SQ_INVOICE;
drop sequence SQ_INVOICE_ITEM;
drop sequence SQ_MED_CASE;
drop sequence SQ_MED_ORDER;
drop sequence SQ_MED_ORDER_ITEM;
drop sequence SQ_MED_ORDER_ITEM_ALERT;
drop sequence SQ_PATIENT;
drop sequence SQ_PHARM_ORDER;
drop sequence SQ_PHARM_ORDER_ITEM;
drop sequence SQ_REFILL_SCHEDULE_ID;
drop sequence SQ_REFILL_SCHEDULE_ITEM;
drop sequence SQ_TICKET;

-- MedProfile

drop sequence SQ_DELIVERY;
drop sequence SQ_DELIVERY_ITEM;
drop sequence SQ_DELIVERY_REQUEST;
drop sequence SQ_DELIVERY_REQUEST_ALERT;
drop sequence SQ_MED_PROFILE;
drop sequence SQ_MED_PROFILE_ERROR_LOG;
drop sequence SQ_MED_PROFILE_ERROR_STAT;
drop sequence SQ_MED_PROFILE_ITEM;
drop sequence SQ_MED_PROFILE_MO_ITEM;
drop sequence SQ_MED_PROFILE_MO_ITEM_ALERT;
drop sequence SQ_MED_PROFILE_MO_ITEM_FM;
drop sequence SQ_MED_PROFILE_ORDER;
drop sequence SQ_MED_PROFILE_PO_ITEM;
drop sequence SQ_MED_PROFILE_STAT;
drop sequence SQ_REPLENISHMENT;
drop sequence SQ_REPLENISHMENT_ITEM;

--//