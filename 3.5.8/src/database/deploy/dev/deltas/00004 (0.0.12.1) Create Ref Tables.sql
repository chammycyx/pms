create table CHARGE_REASON (
    ID number(19) not null,
    REASON_CODE varchar2(2) not null,
    TYPE varchar2(1) not null,
    DESCRIPTION varchar2(1000) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CHARGE_REASON primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CHARGE_REASON_01 unique (REASON_CODE, TYPE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CHARGE_REASON is 'Charging reasons';
comment on column CHARGE_REASON.ID is 'Charging reason id';
comment on column CHARGE_REASON.REASON_CODE is 'Charging reason code';
comment on column CHARGE_REASON.TYPE is 'Charging reason type';
comment on column CHARGE_REASON.DESCRIPTION is 'Description';
comment on column CHARGE_REASON.CREATE_USER is 'Created user';
comment on column CHARGE_REASON.CREATE_DATE is 'Created date';
comment on column CHARGE_REASON.UPDATE_USER is 'Last updated user';
comment on column CHARGE_REASON.UPDATE_DATE is 'Last updated date';
comment on column CHARGE_REASON.VERSION is 'Version to serve as optimistic lock value';


create table CHARGE_SPECIALTY (
    ID number(19) not null,
    SPEC_CODE varchar2(4) not null,
    CHARGE_SPEC_CODE varchar2(4) not null,
    TYPE varchar2(1) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CHARGE_SPECIALTY primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CHARGE_SPECIALTY_01 unique (WORKSTORE_GROUP_CODE, TYPE, SPEC_CODE, CHARGE_SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CHARGE_SPECIALTY is 'Charging specialties';
comment on column CHARGE_SPECIALTY.ID is 'Charge specialty id';
comment on column CHARGE_SPECIALTY.SPEC_CODE is 'Specialty code';
comment on column CHARGE_SPECIALTY.CHARGE_SPEC_CODE is 'Charging specialty code';
comment on column CHARGE_SPECIALTY.TYPE is ' type';
comment on column CHARGE_SPECIALTY.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column CHARGE_SPECIALTY.CREATE_USER is 'Created user';
comment on column CHARGE_SPECIALTY.CREATE_DATE is 'Created date';
comment on column CHARGE_SPECIALTY.UPDATE_USER is 'Last updated user';
comment on column CHARGE_SPECIALTY.UPDATE_DATE is 'Last updated date';
comment on column CHARGE_SPECIALTY.VERSION is 'Version to serve as optimistic lock value';


create table CHEST (
    ID number(19) not null,
    UNIT_DOSE_NAME varchar2(100) not null,
    DEFAULT_ADV_START number(1) not null,
    DEFAULT_DURATION number(2) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CHEST primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CHEST_01 unique (HOSP_CODE, WORKSTORE_CODE, UNIT_DOSE_NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CHEST is 'Chest groups';
comment on column CHEST.ID is 'Chest id';
comment on column CHEST.UNIT_DOSE_NAME is 'Unit dose for chest clinic';
comment on column CHEST.DEFAULT_ADV_START is 'Default advance start day';
comment on column CHEST.DEFAULT_DURATION is 'Default duration';
comment on column CHEST.HOSP_CODE is 'Hospital code';
comment on column CHEST.WORKSTORE_CODE is 'Workstore code';
comment on column CHEST.CREATE_USER is 'Created user';
comment on column CHEST.CREATE_DATE is 'Created date';
comment on column CHEST.UPDATE_USER is 'Last updated user';
comment on column CHEST.UPDATE_DATE is 'Last updated date';
comment on column CHEST.VERSION is 'Version to serve as optimistic lock value';


create table CHEST_ITEM (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    DOSE_QTY number(10) not null,
    DOSE_UNIT varchar2(15) not null,
    CHEST_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CHEST_ITEM primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CHEST_ITEM is 'Chest items by chest groups';
comment on column CHEST_ITEM.ID is 'Chest item id';
comment on column CHEST_ITEM.ITEM_CODE is 'Item code';
comment on column CHEST_ITEM.DOSE_QTY is 'Dose quantity';
comment on column CHEST_ITEM.DOSE_UNIT is 'Dose unit';
comment on column CHEST_ITEM.CHEST_ID is 'Chest id';
comment on column CHEST_ITEM.CREATE_USER is 'Created user';
comment on column CHEST_ITEM.CREATE_DATE is 'Created date';
comment on column CHEST_ITEM.UPDATE_USER is 'Last updated user';
comment on column CHEST_ITEM.UPDATE_DATE is 'Last updated date';
comment on column CHEST_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table CORPORATE_PROP (
    ID number(19) not null,
    PROP_ID number(19) not null,
    VALUE varchar2(1000) null,
    SORT_SEQ number(10) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CORPORATE_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CORPORATE_PROP_01 unique (PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CORPORATE_PROP is 'Properties configured globally in hospital cluster';
comment on column CORPORATE_PROP.ID is 'Corporate property id';
comment on column CORPORATE_PROP.PROP_ID is 'Property id';
comment on column CORPORATE_PROP.VALUE is 'Property value';
comment on column CORPORATE_PROP.SORT_SEQ is 'Sort sequence';
comment on column CORPORATE_PROP.CREATE_USER is 'Created user';
comment on column CORPORATE_PROP.CREATE_DATE is 'Created date';
comment on column CORPORATE_PROP.UPDATE_USER is 'Last updated user';
comment on column CORPORATE_PROP.UPDATE_DATE is 'Last updated date';
comment on column CORPORATE_PROP.VERSION is 'Version to serve as optimistic lock value';


create table CUSTOM_LABEL (
    ID number(19) not null,
    NAME varchar2(12) not null,
    DESCRIPTION varchar2(1000) null,
    LABEL_XML clob null,
    TYPE varchar2(1) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CUSTOM_LABEL primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CUSTOM_LABEL_01 unique (HOSP_CODE, WORKSTORE_CODE, NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CUSTOM_LABEL is 'Custom label templates';
comment on column CUSTOM_LABEL.DESCRIPTION is 'Description';
comment on column CUSTOM_LABEL.ID is 'Custom label id';
comment on column CUSTOM_LABEL.NAME is 'Custom label name';
comment on column CUSTOM_LABEL.LABEL_XML is 'Label content in XML format';
comment on column CUSTOM_LABEL.TYPE is ' type';
comment on column CUSTOM_LABEL.HOSP_CODE is 'Hospital code';
comment on column CUSTOM_LABEL.WORKSTORE_CODE is 'Workstore code';
comment on column CUSTOM_LABEL.CREATE_USER is 'Created user';
comment on column CUSTOM_LABEL.CREATE_DATE is 'Created date';
comment on column CUSTOM_LABEL.UPDATE_USER is 'Last updated user';
comment on column CUSTOM_LABEL.UPDATE_DATE is 'Last updated date';
comment on column CUSTOM_LABEL.VERSION is 'Version to serve as optimistic lock value';


create table FDN_MAPPING (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    ORDER_TYPE varchar2(1) not null,
    TYPE varchar2(3) not null,
    FLOAT_DRUG_NAME varchar2(46) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_FDN_MAPPING primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_FDN_MAPPING_01 unique (HOSP_CODE, WORKSTORE_CODE, ORDER_TYPE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  FDN_MAPPING is 'Floating drug name mapping';
comment on column FDN_MAPPING.ID is 'FDN mapping id';
comment on column FDN_MAPPING.ITEM_CODE is 'Item code';
comment on column FDN_MAPPING.ORDER_TYPE is 'Order type';
comment on column FDN_MAPPING.TYPE is 'FDN type';
comment on column FDN_MAPPING.FLOAT_DRUG_NAME is 'Floating drug name';
comment on column FDN_MAPPING.HOSP_CODE is 'Hospital code';
comment on column FDN_MAPPING.WORKSTORE_CODE is 'Workstore code';
comment on column FDN_MAPPING.CREATE_USER is 'Created user';
comment on column FDN_MAPPING.CREATE_DATE is 'Created date';
comment on column FDN_MAPPING.UPDATE_USER is 'Last updated user';
comment on column FDN_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column FDN_MAPPING.VERSION is 'Version to serve as optimistic lock value';


create table FOLLOW_UP_WARNING (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_FOLLOW_UP_WARNING primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_FOLLOW_UP_WARNING_01 unique (WORKSTORE_GROUP_CODE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  FOLLOW_UP_WARNING is 'Warnings from drug master required users to follow up';
comment on column FOLLOW_UP_WARNING.ID is 'Follow-up warning id';
comment on column FOLLOW_UP_WARNING.ITEM_CODE is 'Item code';
comment on column FOLLOW_UP_WARNING.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column FOLLOW_UP_WARNING.CREATE_USER is 'Created user';
comment on column FOLLOW_UP_WARNING.CREATE_DATE is 'Created date';
comment on column FOLLOW_UP_WARNING.UPDATE_USER is 'Last updated user';
comment on column FOLLOW_UP_WARNING.UPDATE_DATE is 'Last updated date';
comment on column FOLLOW_UP_WARNING.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_PROP (
    ID number(19) not null,
    SORT_SEQ number(10) null,
    VALUE varchar2(1000) null,
    HOSP_CODE varchar2(3) not null,
    PROP_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_HOSPITAL_PROP_01 unique (HOSP_CODE, PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_PROP is 'Properties configured by hospital level';
comment on column HOSPITAL_PROP.ID is 'Hospital property id';
comment on column HOSPITAL_PROP.SORT_SEQ is 'Sort sequence';
comment on column HOSPITAL_PROP.VALUE is 'Property value';
comment on column HOSPITAL_PROP.HOSP_CODE is 'Hospital code';
comment on column HOSPITAL_PROP.PROP_ID is 'Property id';
comment on column HOSPITAL_PROP.CREATE_USER is 'Created user';
comment on column HOSPITAL_PROP.CREATE_DATE is 'Created date';
comment on column HOSPITAL_PROP.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_PROP.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_PROP.VERSION is 'Version to serve as optimistic lock value';


create table ITEM_DISP_CONFIG (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    DAILY_REFILL_FLAG number(1) default 0 not null,
    SINGLE_DISP_FLAG number(1) default 0 not null,
    WARNING_MESSAGE varchar2(255) null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_ITEM_DISP_CONFIG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_ITEM_DISP_CONFIG_01 unique (WORKSTORE_GROUP_CODE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  ITEM_DISP_CONFIG is 'Dispensing configurations of IP items';
comment on column ITEM_DISP_CONFIG.ID is 'Item dispensing configuration id';
comment on column ITEM_DISP_CONFIG.ITEM_CODE is 'Item code';
comment on column ITEM_DISP_CONFIG.DAILY_REFILL_FLAG is 'Daily refill';
comment on column ITEM_DISP_CONFIG.SINGLE_DISP_FLAG is 'Single dispensing';
comment on column ITEM_DISP_CONFIG.WARNING_MESSAGE is 'Warning message for specialties using the special control item';
comment on column ITEM_DISP_CONFIG.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column ITEM_DISP_CONFIG.CREATE_USER is 'Created user';
comment on column ITEM_DISP_CONFIG.CREATE_DATE is 'Created date';
comment on column ITEM_DISP_CONFIG.UPDATE_USER is 'Last updated user';
comment on column ITEM_DISP_CONFIG.UPDATE_DATE is 'Last updated date';
comment on column ITEM_DISP_CONFIG.VERSION is 'Version to serve as optimistic lock value';


create table ITEM_LOCATION (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    BIN_NUM varchar2(4) not null,
    BAKER_CELL_NUM varchar2(5) null,
    MAX_QTY number(5) not null,
    MACHINE_ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_ITEM_LOCATION primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_ITEM_LOCATION_01 unique (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE, BIN_NUM) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  ITEM_LOCATION is 'Items routed to the target picking stations / dispensing machines';
comment on column ITEM_LOCATION.ID is 'Item location id';
comment on column ITEM_LOCATION.ITEM_CODE is 'Item code';
comment on column ITEM_LOCATION.BIN_NUM is 'Bin number';
comment on column ITEM_LOCATION.BAKER_CELL_NUM is 'Baker cell number';
comment on column ITEM_LOCATION.MAX_QTY is 'Maximum quantity';
comment on column ITEM_LOCATION.MACHINE_ID is 'Machine id';
comment on column ITEM_LOCATION.HOSP_CODE is 'Hospital code';
comment on column ITEM_LOCATION.WORKSTORE_CODE is 'Workstore code';
comment on column ITEM_LOCATION.CREATE_USER is 'Created user';
comment on column ITEM_LOCATION.CREATE_DATE is 'Created date';
comment on column ITEM_LOCATION.UPDATE_USER is 'Last updated user';
comment on column ITEM_LOCATION.UPDATE_DATE is 'Last updated date';
comment on column ITEM_LOCATION.VERSION is 'Version to serve as optimistic lock value';


create table ITEM_SPECIALTY (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    SPEC_CODE varchar2(4) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_ITEM_SPECIALTY primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_ITEM_SPECIALTY_01 unique (WORKSTORE_GROUP_CODE, SPEC_CODE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  ITEM_SPECIALTY is 'Special control IP items by specialty';
comment on column ITEM_SPECIALTY.ID is 'Item specialty id';
comment on column ITEM_SPECIALTY.ITEM_CODE is 'Item code';
comment on column ITEM_SPECIALTY.SPEC_CODE is 'Specialty code';
comment on column ITEM_SPECIALTY.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column ITEM_SPECIALTY.CREATE_USER is 'Created user';
comment on column ITEM_SPECIALTY.CREATE_DATE is 'Created date';
comment on column ITEM_SPECIALTY.UPDATE_USER is 'Last updated user';
comment on column ITEM_SPECIALTY.UPDATE_DATE is 'Last updated date';
comment on column ITEM_SPECIALTY.VERSION is 'Version to serve as optimistic lock value';


create table LOCAL_WARNING (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    WARN_CODE varchar2(2) not null,
    SORT_SEQ number(1) null,
    ORG_WARN_CODE varchar2(2) null,
    ORG_WARN_CAT varchar2(2) null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_LOCAL_WARNING primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_LOCAL_WARNING_01 unique (WORKSTORE_GROUP_CODE, ITEM_CODE, WARN_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  LOCAL_WARNING is 'Local warnings';
comment on column LOCAL_WARNING.ID is 'Local warning id';
comment on column LOCAL_WARNING.ITEM_CODE is 'Item code';
comment on column LOCAL_WARNING.WARN_CODE is 'Warning code';
comment on column LOCAL_WARNING.SORT_SEQ is 'Sort sequence';
comment on column LOCAL_WARNING.ORG_WARN_CODE is 'Original warning code';
comment on column LOCAL_WARNING.ORG_WARN_CAT is 'Original warning category';
comment on column LOCAL_WARNING.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column LOCAL_WARNING.CREATE_USER is 'Created user';
comment on column LOCAL_WARNING.CREATE_DATE is 'Created date';
comment on column LOCAL_WARNING.UPDATE_USER is 'Last updated user';
comment on column LOCAL_WARNING.UPDATE_DATE is 'Last updated date';
comment on column LOCAL_WARNING.VERSION is 'Version to serve as optimistic lock value';


create table MACHINE (
    ID number(19) not null,
    HOST_NAME varchar2(15) null,
    TYPE varchar2(1) not null,
    PICK_STATION_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MACHINE primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MACHINE is 'Dispensing machines';
comment on column MACHINE.ID is 'Machine id';
comment on column MACHINE.HOST_NAME is 'Workstation name';
comment on column MACHINE.TYPE is 'Machine type';
comment on column MACHINE.PICK_STATION_ID is 'Picking station id';
comment on column MACHINE.CREATE_USER is 'Created user';
comment on column MACHINE.CREATE_DATE is 'Created date';
comment on column MACHINE.UPDATE_USER is 'Last updated user';
comment on column MACHINE.UPDATE_DATE is 'Last updated date';
comment on column MACHINE.VERSION is 'Version to serve as optimistic lock value';


create table OPERATION_PROFILE (
    ID number(19) not null,
    NAME varchar2(100) not null,
    TYPE varchar2(1) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_OPERATION_PROFILE primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  OPERATION_PROFILE is 'Active operation profile';
comment on column OPERATION_PROFILE.ID is 'Operation profile id';
comment on column OPERATION_PROFILE.NAME is 'Profile name';
comment on column OPERATION_PROFILE.TYPE is 'Operation profile type';
comment on column OPERATION_PROFILE.HOSP_CODE is 'Hospital code';
comment on column OPERATION_PROFILE.WORKSTORE_CODE is 'Workstore code';
comment on column OPERATION_PROFILE.CREATE_USER is 'Created user';
comment on column OPERATION_PROFILE.CREATE_DATE is 'Created date';
comment on column OPERATION_PROFILE.UPDATE_USER is 'Last updated user';
comment on column OPERATION_PROFILE.UPDATE_DATE is 'Last updated date';
comment on column OPERATION_PROFILE.VERSION is 'Version to serve as optimistic lock value';


create table OPERATION_PROP (
    ID number(19) not null,
    SORT_SEQ number(10) null,
    VALUE varchar2(1000) null,
    PROP_ID number(19) not null,
    OPERATION_PROFILE_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_OPERATION_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_OPERATION_PROP_01 unique (OPERATION_PROFILE_ID, PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  OPERATION_PROP is 'Properties configured by operation level';
comment on column OPERATION_PROP.ID is 'Operation Property id';
comment on column OPERATION_PROP.SORT_SEQ is 'Sort sequence';
comment on column OPERATION_PROP.VALUE is 'Property value';
comment on column OPERATION_PROP.PROP_ID is 'Property id';
comment on column OPERATION_PROP.OPERATION_PROFILE_ID is 'Operation profile id';
comment on column OPERATION_PROP.CREATE_USER is 'Created user';
comment on column OPERATION_PROP.CREATE_DATE is 'Created date';
comment on column OPERATION_PROP.UPDATE_USER is 'Last updated user';
comment on column OPERATION_PROP.UPDATE_DATE is 'Last updated date';
comment on column OPERATION_PROP.VERSION is 'Version to serve as optimistic lock value';


create table OPERATION_WORKSTATION (
    ID number(19) not null,
    PRINTER_CONNECTION_TYPE varchar2(1) not null,
    PRINTER_TYPE varchar2(2) not null,
    TYPE varchar2(1) not null,
    ENABLE_FLAG number(1) default 0 not null,
    REDIRECT_PRINT_STATION varchar2(4) null,
    ISSUE_WINDOW_NUM number(2) null,
    WORKSTATION_ID number(19) not null,
    MACHINE_ID number(19) null,
    OPERATION_PROFILE_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_OPERATION_WORKSTATION primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_OPERATION_WORKSTATION_01 unique (OPERATION_PROFILE_ID, WORKSTATION_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  OPERATION_WORKSTATION is 'Workstations by operation profile';
comment on column OPERATION_WORKSTATION.ID is 'Operation workstation id';
comment on column OPERATION_WORKSTATION.PRINTER_CONNECTION_TYPE is 'Printer connection type';
comment on column OPERATION_WORKSTATION.PRINTER_TYPE is 'Printer typet';
comment on column OPERATION_WORKSTATION.TYPE is 'Workstation type';
comment on column OPERATION_WORKSTATION.ENABLE_FLAG is 'Enabled';
comment on column OPERATION_WORKSTATION.REDIRECT_PRINT_STATION is 'Redirect to the target label printing station';
comment on column OPERATION_WORKSTATION.ISSUE_WINDOW_NUM is 'Issue window number';
comment on column OPERATION_WORKSTATION.WORKSTATION_ID is 'Workstation id';
comment on column OPERATION_WORKSTATION.MACHINE_ID is 'Machine id';
comment on column OPERATION_WORKSTATION.OPERATION_PROFILE_ID is 'Operation profile id';
comment on column OPERATION_WORKSTATION.CREATE_USER is 'Created user';
comment on column OPERATION_WORKSTATION.CREATE_DATE is 'Created date';
comment on column OPERATION_WORKSTATION.UPDATE_USER is 'Last updated user';
comment on column OPERATION_WORKSTATION.UPDATE_DATE is 'Last updated date';
comment on column OPERATION_WORKSTATION.VERSION is 'Version to serve as optimistic lock value';


create table PENDING_REASON (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    DESCRIPTION varchar2(44) null,
    PENDING_CODE varchar2(2) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PENDING_REASON primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PENDING_REASON_01 unique (WORKSTORE_GROUP_CODE, PENDING_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PENDING_REASON is 'Pending reasons';
comment on column PENDING_REASON.ID is 'Pending reason id';
comment on column PENDING_REASON.STATUS is 'Record status';
comment on column PENDING_REASON.DESCRIPTION is 'Description';
comment on column PENDING_REASON.PENDING_CODE is 'Pending reason code';
comment on column PENDING_REASON.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column PENDING_REASON.CREATE_USER is 'Created user';
comment on column PENDING_REASON.CREATE_DATE is 'Created date';
comment on column PENDING_REASON.UPDATE_USER is 'Last updated user';
comment on column PENDING_REASON.UPDATE_DATE is 'Last updated date';
comment on column PENDING_REASON.VERSION is 'Version to serve as optimistic lock value';


create table PHARM_REMARK_TEMPLATE (
    ID number(19) not null,
    TEMPLATE varchar2(100) not null,
    SORT_SEQ number(10) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PHARM_REMARK_TEMPLATE primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PHARM_REMARK_TEMPLATE is 'Pharmacy remark templates';
comment on column PHARM_REMARK_TEMPLATE.ID is 'Pharmacy remark template id';
comment on column PHARM_REMARK_TEMPLATE.TEMPLATE is 'Template name';
comment on column PHARM_REMARK_TEMPLATE.SORT_SEQ is 'Sort sequence';
comment on column PHARM_REMARK_TEMPLATE.HOSP_CODE is 'Hospital code';
comment on column PHARM_REMARK_TEMPLATE.WORKSTORE_CODE is 'Workstore code';
comment on column PHARM_REMARK_TEMPLATE.CREATE_USER is 'Created user';
comment on column PHARM_REMARK_TEMPLATE.CREATE_DATE is 'Created date';
comment on column PHARM_REMARK_TEMPLATE.UPDATE_USER is 'Last updated user';
comment on column PHARM_REMARK_TEMPLATE.UPDATE_DATE is 'Last updated date';
comment on column PHARM_REMARK_TEMPLATE.VERSION is 'Version to serve as optimistic lock value';


create table PICK_STATION (
    ID number(19) not null,
    PICK_STATION_NUM number(6) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PICK_STATION primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PICK_STATION_01 unique (HOSP_CODE, WORKSTORE_CODE, PICK_STATION_NUM) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PICK_STATION is 'Picking stations';
comment on column PICK_STATION.ID is 'Picking station id';
comment on column PICK_STATION.PICK_STATION_NUM is 'Picking station number';
comment on column PICK_STATION.HOSP_CODE is 'Hospital code';
comment on column PICK_STATION.WORKSTORE_CODE is 'Workstore code';
comment on column PICK_STATION.CREATE_USER is 'Created user';
comment on column PICK_STATION.CREATE_DATE is 'Created date';
comment on column PICK_STATION.UPDATE_USER is 'Last updated user';
comment on column PICK_STATION.UPDATE_DATE is 'Last updated date';
comment on column PICK_STATION.VERSION is 'Version to serve as optimistic lock value';


create table PREPACK (
    ID number(19) not null,
    PREPACK_QTY varchar2(1000) null,
    ITEM_CODE varchar2(6) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PREPACK primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PREPACK_01 unique (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PREPACK is 'Items for pre-packing';
comment on column PREPACK.ID is 'Prepack id';
comment on column PREPACK.PREPACK_QTY is 'Prepack quantity';
comment on column PREPACK.ITEM_CODE is 'Item code';
comment on column PREPACK.HOSP_CODE is 'Hospital code';
comment on column PREPACK.WORKSTORE_CODE is 'Workstore code';
comment on column PREPACK.CREATE_USER is 'Created user';
comment on column PREPACK.CREATE_DATE is 'Created date';
comment on column PREPACK.UPDATE_USER is 'Last updated user';
comment on column PREPACK.UPDATE_DATE is 'Last updated date';
comment on column PREPACK.VERSION is 'Version to serve as optimistic lock value';


create table PRN_ROUTE (
    ID number(19) not null,
    ROUTE_CODE varchar2(30) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PRN_ROUTE primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PRN_ROUTE_01 unique (HOSP_CODE, WORKSTORE_CODE, ROUTE_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PRN_ROUTE is 'PRN maintainence';
comment on column PRN_ROUTE.ID is 'PRN Route id';
comment on column PRN_ROUTE.ROUTE_CODE is 'Route code';
comment on column PRN_ROUTE.HOSP_CODE is 'Hospital code';
comment on column PRN_ROUTE.WORKSTORE_CODE is 'Workstore code';
comment on column PRN_ROUTE.CREATE_USER is 'Created user';
comment on column PRN_ROUTE.CREATE_DATE is 'Created date';
comment on column PRN_ROUTE.UPDATE_USER is 'Last updated user';
comment on column PRN_ROUTE.UPDATE_DATE is 'Last updated date';
comment on column PRN_ROUTE.VERSION is 'Version to serve as optimistic lock value';


create table PROP (
    ID number(19) not null,
    NAME varchar2(100) not null,
    DESCRIPTION varchar2(1000) null,
    VALIDATION varchar2(100) null,
    TYPE varchar2(2) not null,
    SUPPORT_MAINT_FLAG number(1) default 0 not null,
    CACHE_IN_SESSION_FLAG number(1) default 0 not null,
    CACHE_EXPIRE_TIME number(10) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PROP_01 unique (NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PROP is 'Full set of all corporate, hospital, workstore and workstation properties';
comment on column PROP.ID is 'Property id';
comment on column PROP.NAME is 'Property name';
comment on column PROP.DESCRIPTION is 'Description';
comment on column PROP.VALIDATION is 'Validation of property value';
comment on column PROP.TYPE is 'Property data type';
comment on column PROP.SUPPORT_MAINT_FLAG is 'Support ITD maintanance';
comment on column PROP.CACHE_IN_SESSION_FLAG is 'Cache in session';
comment on column PROP.CACHE_EXPIRE_TIME is 'Cache expiry time (if not cache in session)';
comment on column PROP.CREATE_USER is 'Created user';
comment on column PROP.CREATE_DATE is 'Created date';
comment on column PROP.UPDATE_USER is 'Last updated user';
comment on column PROP.UPDATE_DATE is 'Last updated date';
comment on column PROP.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_CONFIG (
    ID number(19) not null,
    ENABLE_SELECTION_FLAG number(1) default 0 not null,
    THRESHOLD number(3) not null,
    INTERVAL number(3) not null,
    LAST_INTERVAL_MAX_DAY number(3) not null,
    ENABLE_QTY_ADJ_FLAG number(1) default 0 not null,
    LATE_REFILL_ALERT_DAY number(3) not null,
    ABORT_REFILL_DAY number(3) not null,
    MANU_RX_NO_PAS_SPEC_FLAG number(1) default 0 not null,
    ENABLE_PHARM_CLINIC_FLAG number(1) default 0 not null,
    PRINT_REMINDER_FLAG number(1) default 0 not null,
    DEFAULT_LATE_REFILL_ALERT_BTN varchar2(1) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_CONFIG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_REFILL_CONFIG_01 unique (HOSP_CODE, WORKSTORE_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  REFILL_CONFIG is 'Configuration of refill model';
comment on column REFILL_CONFIG.ID is 'Refill configuration id';
comment on column REFILL_CONFIG.ENABLE_SELECTION_FLAG is 'Use refill model for all items and PAS specialties';
comment on column REFILL_CONFIG.THRESHOLD is 'Split drug item after';
comment on column REFILL_CONFIG.INTERVAL is 'Refill interval';
comment on column REFILL_CONFIG.LAST_INTERVAL_MAX_DAY is 'Maximum interval for the last refill';
comment on column REFILL_CONFIG.ENABLE_QTY_ADJ_FLAG is 'Enable quantity adjustment';
comment on column REFILL_CONFIG.LATE_REFILL_ALERT_DAY is 'Prompt alert if patient is late by more than specified day(s)';
comment on column REFILL_CONFIG.ABORT_REFILL_DAY is 'Abort refill coupon if the refill end date has exceeded more than specified day(s)';
comment on column REFILL_CONFIG.MANU_RX_NO_PAS_SPEC_FLAG is 'Perform refill for manual prescription without PAS specialty';
comment on column REFILL_CONFIG.ENABLE_PHARM_CLINIC_FLAG is 'Pharmacist clinic available';
comment on column REFILL_CONFIG.PRINT_REMINDER_FLAG is 'Print refill coupon reminder';
comment on column REFILL_CONFIG.DEFAULT_LATE_REFILL_ALERT_BTN is 'Default focus of late refill alert message buttons';
comment on column REFILL_CONFIG.HOSP_CODE is 'Hospital code';
comment on column REFILL_CONFIG.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_CONFIG.CREATE_USER is 'Created user';
comment on column REFILL_CONFIG.CREATE_DATE is 'Created date';
comment on column REFILL_CONFIG.UPDATE_USER is 'Last updated user';
comment on column REFILL_CONFIG.UPDATE_DATE is 'Last updated date';
comment on column REFILL_CONFIG.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_HOLIDAY (
    ID number(19) not null,
    HOLIDAY timestamp not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_HOLIDAY primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_REFILL_HOLIDAY_01 unique (HOSP_CODE, WORKSTORE_CODE, HOLIDAY) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  REFILL_HOLIDAY is 'Holiday list of refill model';
comment on column REFILL_HOLIDAY.ID is 'Refill holiday id';
comment on column REFILL_HOLIDAY.HOLIDAY is 'Date of public holiday';
comment on column REFILL_HOLIDAY.HOSP_CODE is 'Hospital code';
comment on column REFILL_HOLIDAY.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_HOLIDAY.CREATE_USER is 'Created user';
comment on column REFILL_HOLIDAY.CREATE_DATE is 'Created date';
comment on column REFILL_HOLIDAY.UPDATE_USER is 'Last updated user';
comment on column REFILL_HOLIDAY.UPDATE_DATE is 'Last updated date';
comment on column REFILL_HOLIDAY.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_QTY_ADJ (
    ID number(19) not null,
    FORM_CODE varchar2(3) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_QTY_ADJ primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_REFILL_QTY_ADJ_01 unique (HOSP_CODE, WORKSTORE_CODE, FORM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  REFILL_QTY_ADJ is 'Late refill quantity adjustment';
comment on column REFILL_QTY_ADJ.ID is 'Refill quantity adjustment id';
comment on column REFILL_QTY_ADJ.FORM_CODE is 'Form code';
comment on column REFILL_QTY_ADJ.HOSP_CODE is 'Hospital code';
comment on column REFILL_QTY_ADJ.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_QTY_ADJ.CREATE_USER is 'Created user';
comment on column REFILL_QTY_ADJ.CREATE_DATE is 'Created date';
comment on column REFILL_QTY_ADJ.UPDATE_USER is 'Last updated user';
comment on column REFILL_QTY_ADJ.UPDATE_DATE is 'Last updated date';
comment on column REFILL_QTY_ADJ.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_QTY_ADJ_EXCLUSION (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_QTY_ADJ_EXCLUSION primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_REFILL_QTY_ADJ_EXCLUSION_01 unique (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  REFILL_QTY_ADJ_EXCLUSION is 'Exclusion of late refill quantity adjustment';
comment on column REFILL_QTY_ADJ_EXCLUSION.ID is 'Exclusion of refill quantity adjustment id';
comment on column REFILL_QTY_ADJ_EXCLUSION.ITEM_CODE is 'Item code';
comment on column REFILL_QTY_ADJ_EXCLUSION.HOSP_CODE is 'Hospital code';
comment on column REFILL_QTY_ADJ_EXCLUSION.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_QTY_ADJ_EXCLUSION.CREATE_USER is 'Created user';
comment on column REFILL_QTY_ADJ_EXCLUSION.CREATE_DATE is 'Created date';
comment on column REFILL_QTY_ADJ_EXCLUSION.UPDATE_USER is 'Last updated user';
comment on column REFILL_QTY_ADJ_EXCLUSION.UPDATE_DATE is 'Last updated date';
comment on column REFILL_QTY_ADJ_EXCLUSION.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_SELECTION (
    ID number(19) not null,
    PAS_SPEC_CODE varchar2(4) not null,
    TYPE varchar2(1) not null,
    ITEM_CODE varchar2(6) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_SELECTION primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_REFILL_SELECTION_01 unique (HOSP_CODE, WORKSTORE_CODE, ITEM_CODE, PAS_SPEC_CODE, TYPE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  REFILL_SELECTION is 'Items allowed for refill';
comment on column REFILL_SELECTION.ID is 'Refill selection id';
comment on column REFILL_SELECTION.PAS_SPEC_CODE is 'PAS specialty code';
comment on column REFILL_SELECTION.TYPE is 'Refill selection type';
comment on column REFILL_SELECTION.ITEM_CODE is 'Item code';
comment on column REFILL_SELECTION.HOSP_CODE is 'Hospital code';
comment on column REFILL_SELECTION.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_SELECTION.CREATE_USER is 'Created user';
comment on column REFILL_SELECTION.CREATE_DATE is 'Created date';
comment on column REFILL_SELECTION.UPDATE_USER is 'Last updated user';
comment on column REFILL_SELECTION.UPDATE_DATE is 'Last updated date';
comment on column REFILL_SELECTION.VERSION is 'Version to serve as optimistic lock value';


create table SUSPEND_REASON (
    ID number(19) not null,
    SUSPEND_CODE varchar2(2) not null,
    STATUS varchar2(1) not null,
    DESCRIPTION varchar2(44) null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SUSPEND_REASON primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_SUSPEND_REASON_01 unique (WORKSTORE_GROUP_CODE, SUSPEND_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  SUSPEND_REASON is 'Suspend reason';
comment on column SUSPEND_REASON.ID is 'Suspend reason id';
comment on column SUSPEND_REASON.SUSPEND_CODE is 'Suspend code';
comment on column SUSPEND_REASON.STATUS is 'Record status';
comment on column SUSPEND_REASON.DESCRIPTION is 'Description';
comment on column SUSPEND_REASON.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column SUSPEND_REASON.CREATE_USER is 'Created user';
comment on column SUSPEND_REASON.CREATE_DATE is 'Created date';
comment on column SUSPEND_REASON.UPDATE_USER is 'Last updated user';
comment on column SUSPEND_REASON.UPDATE_DATE is 'Last updated date';
comment on column SUSPEND_REASON.VERSION is 'Version to serve as optimistic lock value';


create table SUSPEND_STAT (
    ID number(19) not null,
    RECORD_COUNT number(10) not null,
    DAY_END_BATCH_DATE date not null,
    SUSPEND_CODE varchar2(2) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SUSPEND_STAT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_SUSPEND_STAT_01 unique (HOSP_CODE, WORKSTORE_CODE, DAY_END_BATCH_DATE, SUSPEND_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  SUSPEND_STAT is 'Consolidated statisitcs of suspension counts';
comment on column SUSPEND_STAT.ID is 'Suspend statistic id';
comment on column SUSPEND_STAT.RECORD_COUNT is 'Record count';
comment on column SUSPEND_STAT.DAY_END_BATCH_DATE is 'Batch date of day end ';
comment on column SUSPEND_STAT.SUSPEND_CODE is 'Suspend code';
comment on column SUSPEND_STAT.HOSP_CODE is 'Hospital code';
comment on column SUSPEND_STAT.WORKSTORE_CODE is 'Workstore code';
comment on column SUSPEND_STAT.CREATE_USER is 'Created user';
comment on column SUSPEND_STAT.CREATE_DATE is 'Created date';
comment on column SUSPEND_STAT.UPDATE_USER is 'Last updated user';
comment on column SUSPEND_STAT.UPDATE_DATE is 'Last updated date';
comment on column SUSPEND_STAT.VERSION is 'Version to serve as optimistic lock value';


create table WARD_CONFIG (
    ID number(19) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    WARD_CODE varchar2(4) not null,
    PRINT_DOSAGE_INSTRUCTION_FLAG number(1) default 0 not null,
    DOSAGE_INSTRUCTION_LANG_FLAG varchar2(10) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD_CONFIG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WARD_CONFIG_01 unique (WORKSTORE_GROUP_CODE, WARD_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD_CONFIG is 'Ward configurations';
comment on column WARD_CONFIG.ID is 'Ward configuration id';
comment on column WARD_CONFIG.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WARD_CONFIG.WARD_CODE is 'Ward code';
comment on column WARD_CONFIG.PRINT_DOSAGE_INSTRUCTION_FLAG is 'Print dosage instruction';
comment on column WARD_CONFIG.DOSAGE_INSTRUCTION_LANG_FLAG is 'Language of dosage instruction';
comment on column WARD_CONFIG.CREATE_USER is 'Created user';
comment on column WARD_CONFIG.CREATE_DATE is 'Created date';
comment on column WARD_CONFIG.UPDATE_USER is 'Last updated user';
comment on column WARD_CONFIG.UPDATE_DATE is 'Last updated date';
comment on column WARD_CONFIG.VERSION is 'Version to serve as optimistic lock value';


create table WARD_FILTER (
    ID number(19) not null,
    WARD_CODE varchar2(4) not null,
    MON_FLAG number(1) default 0 not null,
    TUE_FLAG number(1) default 0 not null,
    WED_FLAG number(1) default 0 not null,
    THU_FLAG number(1) default 0 not null,
    FRI_FLAG number(1) default 0 not null,
    SAT_FLAG number(1) default 0 not null,
    SUN_FLAG number(1) default 0 not null,
    START_TIME varchar2(4) not null,
    END_TIME varchar2(4) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD_FILTER primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD_FILTER is 'Ward filter rules for pharmacy inbox';
comment on column WARD_FILTER.ID is 'Ward filter id';
comment on column WARD_FILTER.WARD_CODE is 'Ward code';
comment on column WARD_FILTER.MON_FLAG is 'Enable showing Monday records';
comment on column WARD_FILTER.TUE_FLAG is 'Enable showing Tuesday records';
comment on column WARD_FILTER.WED_FLAG is 'Enable showing Wednesday records';
comment on column WARD_FILTER.THU_FLAG is 'Enable showing Thursday records';
comment on column WARD_FILTER.FRI_FLAG is 'Enable showing Friday records';
comment on column WARD_FILTER.SAT_FLAG is 'Enable showing Saturday records';
comment on column WARD_FILTER.SUN_FLAG is 'Enable showing Sunday records';
comment on column WARD_FILTER.START_TIME is 'Start time to enable ward filter rule';
comment on column WARD_FILTER.END_TIME is 'End time to enable ward filter rule';
comment on column WARD_FILTER.HOSP_CODE is 'Hospital code';
comment on column WARD_FILTER.WORKSTORE_CODE is 'Workstore code';
comment on column WARD_FILTER.CREATE_USER is 'Created user';
comment on column WARD_FILTER.CREATE_DATE is 'Created date';
comment on column WARD_FILTER.UPDATE_USER is 'Last updated user';
comment on column WARD_FILTER.UPDATE_DATE is 'Last updated date';
comment on column WARD_FILTER.VERSION is 'Version to serve as optimistic lock value';


create table WARD_GROUP (
    ID number(19) not null,
    WARD_GROUP_CODE varchar2(4) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD_GROUP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WARD_GROUP_01 unique (WORKSTORE_GROUP_CODE, WARD_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD_GROUP is 'Ward groups for pharmacy inbox';
comment on column WARD_GROUP.ID is 'Ward group id';
comment on column WARD_GROUP.WARD_GROUP_CODE is 'Ward group code';
comment on column WARD_GROUP.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WARD_GROUP.CREATE_USER is 'Created user';
comment on column WARD_GROUP.CREATE_DATE is 'Created date';
comment on column WARD_GROUP.UPDATE_USER is 'Last updated user';
comment on column WARD_GROUP.UPDATE_DATE is 'Last updated date';
comment on column WARD_GROUP.VERSION is 'Version to serve as optimistic lock value';


create table WARD_GROUP_ITEM (
    ID number(19) not null,
    WARD_CODE varchar2(4) not null,
    WARD_GROUP_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD_GROUP_ITEM primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WARD_GROUP_ITEM_01 unique (WARD_GROUP_ID, WARD_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD_GROUP_ITEM is 'Group of wards for pharmacy inbox';
comment on column WARD_GROUP_ITEM.ID is 'Ward group item id';
comment on column WARD_GROUP_ITEM.WARD_CODE is 'Ward code';
comment on column WARD_GROUP_ITEM.WARD_GROUP_ID is 'Ward group id';
comment on column WARD_GROUP_ITEM.CREATE_USER is 'Created user';
comment on column WARD_GROUP_ITEM.CREATE_DATE is 'Created date';
comment on column WARD_GROUP_ITEM.UPDATE_USER is 'Last updated user';
comment on column WARD_GROUP_ITEM.UPDATE_DATE is 'Last updated date';
comment on column WARD_GROUP_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table WORKLOAD_STAT (
    ID number(19) not null,
    DAY_END_BATCH_DATE date not null,
    REFILL_COUNT number(10) not null,
    ORDER_ITEM_COUNT number(10) not null,
    REFILL_ITEM_COUNT number(10) not null,
    ORDER_COUNT number(10) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKLOAD_STAT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WORKLOAD_STAT_01 unique (HOSP_CODE, WORKSTORE_CODE, DAY_END_BATCH_DATE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKLOAD_STAT is 'Consolidated workload statistics';
comment on column WORKLOAD_STAT.ID is 'Workload statistic id';
comment on column WORKLOAD_STAT.DAY_END_BATCH_DATE is 'Batch date of day end';
comment on column WORKLOAD_STAT.REFILL_COUNT is 'Count of refill dispensing orders';
comment on column WORKLOAD_STAT.ORDER_ITEM_COUNT is 'Count of dispensing order items';
comment on column WORKLOAD_STAT.REFILL_ITEM_COUNT is 'Count of refill dispensing order items';
comment on column WORKLOAD_STAT.ORDER_COUNT is 'Count of dispensing orders';
comment on column WORKLOAD_STAT.HOSP_CODE is 'Hospital code';
comment on column WORKLOAD_STAT.WORKSTORE_CODE is 'Workstore code';
comment on column WORKLOAD_STAT.CREATE_USER is 'Created user';
comment on column WORKLOAD_STAT.CREATE_DATE is 'Created date';
comment on column WORKLOAD_STAT.UPDATE_USER is 'Last updated user';
comment on column WORKLOAD_STAT.UPDATE_DATE is 'Last updated date';
comment on column WORKLOAD_STAT.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTATION (
    ID number(19) not null,
    WORKSTATION_CODE varchar2(4) not null,
    COMMON_ACCOUNT_FLAG number(1) default 0 not null,
    HOST_NAME varchar2(15) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTATION primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WORKSTATION_01 unique (HOSP_CODE, WORKSTORE_CODE, WORKSTATION_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTATION is 'Workstations allowed to access PMS';
comment on column WORKSTATION.ID is 'Workstation id';
comment on column WORKSTATION.WORKSTATION_CODE is 'Workstation code defined by users';
comment on column WORKSTATION.COMMON_ACCOUNT_FLAG is 'Enable common account login';
comment on column WORKSTATION.HOST_NAME is 'Workstation name';
comment on column WORKSTATION.HOSP_CODE is 'Hospital code';
comment on column WORKSTATION.WORKSTORE_CODE is 'Workstore code';
comment on column WORKSTATION.CREATE_USER is 'Created user';
comment on column WORKSTATION.CREATE_DATE is 'Created date';
comment on column WORKSTATION.UPDATE_USER is 'Last updated user';
comment on column WORKSTATION.UPDATE_DATE is 'Last updated date';
comment on column WORKSTATION.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTATION_PRINT (
    ID number(19) not null,
    PRINT_DOC_TYPE number(2) not null,
    PRINT_TYPE number(1) not null,
    VALUE varchar2(100) null,
    WORKSTATION_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTATION_PRINT primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WORKSTATION_PRINT_01 unique (WORKSTATION_ID, PRINT_DOC_TYPE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTATION_PRINT is 'Non-dispensing label printers';
comment on column WORKSTATION_PRINT.ID is 'Workstation print id';
comment on column WORKSTATION_PRINT.PRINT_DOC_TYPE is 'Printing document type excluding dispensing label';
comment on column WORKSTATION_PRINT.PRINT_TYPE is 'Printing type';
comment on column WORKSTATION_PRINT.VALUE is 'Local printer name / Workstation code redirected';
comment on column WORKSTATION_PRINT.WORKSTATION_ID is 'Workstation id';
comment on column WORKSTATION_PRINT.CREATE_USER is 'Created user';
comment on column WORKSTATION_PRINT.CREATE_DATE is 'Created date';
comment on column WORKSTATION_PRINT.UPDATE_USER is 'Last updated user';
comment on column WORKSTATION_PRINT.UPDATE_DATE is 'Last updated date';
comment on column WORKSTATION_PRINT.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTATION_PROP (
    ID number(19) not null,
    PROP_ID number(19) not null,
    VALUE varchar2(1000) null,
    SORT_SEQ number(10) null,
    WORKSTATION_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTATION_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WORKSTATION_PROP_01 unique (WORKSTATION_ID, PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTATION_PROP is 'Properties configured by workstation level';
comment on column WORKSTATION_PROP.ID is 'Workstation id';
comment on column WORKSTATION_PROP.PROP_ID is 'Property id';
comment on column WORKSTATION_PROP.VALUE is 'Property value';
comment on column WORKSTATION_PROP.SORT_SEQ is 'Sort sequence';
comment on column WORKSTATION_PROP.WORKSTATION_ID is 'Workstation id';
comment on column WORKSTATION_PROP.CREATE_USER is 'Created user';
comment on column WORKSTATION_PROP.CREATE_DATE is 'Created date';
comment on column WORKSTATION_PROP.UPDATE_USER is 'Last updated user';
comment on column WORKSTATION_PROP.UPDATE_DATE is 'Last updated date';
comment on column WORKSTATION_PROP.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE_PROP (
    ID number(19) not null,
    PROP_ID number(19) not null,
    VALUE varchar2(1000) null,
    SORT_SEQ number(10) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE_PROP primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_WORKSTORE_PROP_01 unique (HOSP_CODE, WORKSTORE_CODE, PROP_ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE_PROP is 'Properties configured by workstore level';
comment on column WORKSTORE_PROP.ID is 'Workstore property id';
comment on column WORKSTORE_PROP.PROP_ID is 'Property id';
comment on column WORKSTORE_PROP.VALUE is 'Property value';
comment on column WORKSTORE_PROP.SORT_SEQ is 'Sort sequence';
comment on column WORKSTORE_PROP.HOSP_CODE is 'Hospital code';
comment on column WORKSTORE_PROP.WORKSTORE_CODE is 'Workstore code';
comment on column WORKSTORE_PROP.CREATE_USER is 'Created user';
comment on column WORKSTORE_PROP.CREATE_DATE is 'Created date';
comment on column WORKSTORE_PROP.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE_PROP.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE_PROP.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table CHARGE_REASON;
drop table CHARGE_SPECIALTY;
drop table CHEST;
drop table CHEST_ITEM;
drop table CORPORATE_PROP;
drop table CUSTOM_LABEL;
drop table FDN_MAPPING;
drop table FOLLOW_UP_WARNING;
drop table HOSPITAL_PROP;
drop table ITEM_DISP_CONFIG;
drop table ITEM_LOCATION;
drop table ITEM_SPECIALTY;
drop table LOCAL_WARNING;
drop table MACHINE;
drop table OPERATION_PROFILE;
drop table OPERATION_PROP;
drop table OPERATION_WORKSTATION;
drop table PENDING_REASON;
drop table PHARM_REMARK_TEMPLATE;
drop table PICK_STATION;
drop table PREPACK;
drop table PRN_ROUTE;
drop table PROP;
drop table REFILL_CONFIG;
drop table REFILL_HOLIDAY;
drop table REFILL_QTY_ADJ;
drop table REFILL_QTY_ADJ_EXCLUSION;
drop table REFILL_SELECTION;
drop table SUSPEND_REASON;
drop table SUSPEND_STAT;
drop table WARD_CONFIG;
drop table WARD_FILTER;
drop table WARD_GROUP;
drop table WARD_GROUP_ITEM;
drop table WORKLOAD_STAT;
drop table WORKSTATION;
drop table WORKSTATION_PRINT;
drop table WORKSTATION_PROP;
drop table WORKSTORE_PROP;
--//