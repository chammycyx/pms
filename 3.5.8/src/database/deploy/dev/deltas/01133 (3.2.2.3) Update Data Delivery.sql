update DELIVERY d set d.PRINTED_COUNT = 
(select count(di.DELIVERY_ID) from DELIVERY_ITEM di 
where di.STATUS not in ('K','U','D') and di.DELIVERY_ID = d.ID 
group by di.DELIVERY_ID)
where d.BATCH_DATE >= trunc(sysdate - 1) and d.PRINTED_COUNT is null;

update DELIVERY d set d.PRINTED_COUNT = 0
where d.BATCH_DATE >= trunc(sysdate - 1) and d.PRINTED_COUNT is null;

--//@UNDO
--//