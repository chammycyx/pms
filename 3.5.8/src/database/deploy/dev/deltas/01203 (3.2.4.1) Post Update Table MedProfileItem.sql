alter table MED_PROFILE_ITEM drop column DUE_DATE;

--//@UNDO
alter table MED_PROFILE_ITEM add DUE_DATE timestamp null;
comment on column MED_PROFILE_ITEM.DUE_DATE is 'Due date';
--//