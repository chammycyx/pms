alter table WORKSTATION add CHEMO_FLAG number(1);
comment on column WORKSTATION.CHEMO_FLAG is 'Chemo flag';

--//@UNDO
alter table WORKSTATION set unused column CHEMO_FLAG;
--//