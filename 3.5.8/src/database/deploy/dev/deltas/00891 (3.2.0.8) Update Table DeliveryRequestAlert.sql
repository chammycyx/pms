alter table DELIVERY_REQUEST_ALERT add MED_PROFILE_ID number(19);

comment on column DELIVERY_REQUEST_ALERT.MED_PROFILE_ID is 'Medication profile id';

--//@UNDO
alter table DELIVERY_REQUEST_ALERT drop column MED_PROFILE_ID;
--//