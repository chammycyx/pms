alter table DELIVERY_ITEM add MED_PROFILE_ID number(19);
alter table DELIVERY_ITEM add constraint FK_DELIVERY_ITEM_05 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);

create index FK_DELIVERY_ITEM_05 on DELIVERY_ITEM (MED_PROFILE_ID) local;


--//@UNDO
drop index FK_DELIVERY_ITEM_05;

alter table DELIVERY_ITEM drop constraint FK_DELIVERY_ITEM_05;
alter table DELIVERY_ITEM drop column MED_PROFILE_ID;
--//