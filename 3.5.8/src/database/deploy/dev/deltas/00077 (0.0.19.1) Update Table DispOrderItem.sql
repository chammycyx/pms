alter table DISP_ORDER_ITEM add NUM_OF_LABEL number(1) null;
update DISP_ORDER_ITEM set NUM_OF_LABEL = 1 where NUM_OF_LABEL is null;
alter table DISP_ORDER_ITEM modify NUM_OF_LABEL not null;

comment on column DISP_ORDER_ITEM.NUM_OF_LABEL is 'Number of label(s)';

alter table DISP_ORDER_ITEM modify DURATION_IN_DAY number(4,0);

--//@UNDO
alter table DISP_ORDER_ITEM drop column NUM_OF_LABEL;

alter table DISP_ORDER_ITEM modify DURATION_IN_DAY number(3,0);
--//
