insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (25019,'refill.drs.coupon.remarkChi','Information of Refill coupon in Chinese (DRS)','r8,c50','T',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);
insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (25020,'refill.drs.coupon.remarkEng','Information of Refill coupon in English (DRS)','r8,c50','T',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into OPERATION_PROP (ID,SORT_SEQ,PROP_ID,VALUE,OPERATION_PROFILE_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_OPERATION_PROP.nextval,25019,25019,VALUE,OPERATION_PROFILE_ID,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select OPERATION_PROFILE_ID, VALUE from OPERATION_PROP where PROP_ID = '25007');

insert all into OPERATION_PROP (ID,SORT_SEQ,PROP_ID,VALUE,OPERATION_PROFILE_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_OPERATION_PROP.nextval,25020,25020,VALUE,OPERATION_PROFILE_ID,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select OPERATION_PROFILE_ID, VALUE from OPERATION_PROP where PROP_ID = '25008');

update PROP set DESCRIPTION='[consult CD3 for change] Duration (in days) of ended Rx to be retained for Rx reconciliation in OP Vetting', MAINT_SCREEN='-', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32063;

--//@UNDO
delete from OPERATION_PROP where PROP_ID in (25019, 25020);

delete from PROP where ID in (25019, 25020);

update PROP set DESCRIPTION='Duration (in days) of ended Rx to be retained for Rx reconciliation in OP Vetting', MAINT_SCREEN='S', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32063;
--//