update SYSTEM_MESSAGE set MAIN_MSG = 'Frequency Value 3 should be greater than Frequency Value 2' where MESSAGE_CODE = '0254';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Frequency value 3 should be greater than frequency value 2' where MESSAGE_CODE = '0254';
--//