delete from CORPORATE_PROP where PROP_ID in (10016);
delete from PROP where ID in (10016);

--//@UNDO
insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (10016,'alert.ehr.profile.rollout','[to be obsolete] CMS rollout: Enhancement for eHR Alert','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (10016,10016,'Y',10016,'pmsadmin',sysdate,'pmsadmin',sysdate,1);
--//