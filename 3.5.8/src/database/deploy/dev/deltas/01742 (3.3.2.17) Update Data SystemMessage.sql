update SYSTEM_MESSAGE set MAIN_MSG = 'The profile you selected is currently being edited by workstation [#0].  Would you like to open this profile as read-only?' where MESSAGE_CODE = '0317';
update SYSTEM_MESSAGE set MAIN_MSG = '[PHS DH-HA Drug Mapping] has been updated.  Display name of the item codes in pharmacy line do not match with [PHS DH-HA Drug Mapping].  Please review the item codes in pharmacy line and modify if necessary.' where MESSAGE_CODE = '0905';
update SYSTEM_MESSAGE set MAIN_MSG = 'Non-parenteral item cannot be selected in PIVAS drug maintenance.' where MESSAGE_CODE = '0786';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'The profile you selected is currently editing by workstation [#0].  Would you like to open this profile as read-only?' where MESSAGE_CODE = '0317';
update SYSTEM_MESSAGE set MAIN_MSG = 'PHS DH-HA drug mapping has been updated.  Display name of the item codes in pharmacy line do not match with DH-HA drug mapping.  Please review the item codes in pharmacy line and modify if necessary.' where MESSAGE_CODE = '0905';
update SYSTEM_MESSAGE set MAIN_MSG = 'PIVAS drug cannot be done for this non-parenteral item.' where MESSAGE_CODE = '0786';
--//