insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (14028,'charging.fcs.gsDirectPayment.rollout','For external system: FCS rollouts enhancement of GS direct payment (to be obsolete)','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (14028,14028,'N',14028,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

update PROP set REMINDER_MESSAGE='Fees Collection System (FCS) service is not available.' where id=14020;

--//@UNDO
delete from CORPORATE_PROP where PROP_ID=14028;
delete from PROP where ID=14028;
--//