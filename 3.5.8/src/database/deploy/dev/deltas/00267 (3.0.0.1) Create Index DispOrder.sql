create index I_DISP_ORDER_08 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, DAY_END_BATCH_DATE) local;
create index I_DISP_ORDER_09 on DISP_ORDER (PREV_DISP_ORDER_ID) local;


--//@UNDO
drop index I_DISP_ORDER_08;
drop index I_DISP_ORDER_09;
--//