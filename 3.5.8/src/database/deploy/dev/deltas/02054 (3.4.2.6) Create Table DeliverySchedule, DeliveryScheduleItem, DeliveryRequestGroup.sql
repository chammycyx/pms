create table DELIVERY_SCHEDULE (
    ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    NAME varchar2(50) not null,
    DESCRIPTION varchar2(100) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY_SCHEDULE primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  DELIVERY_SCHEDULE is 'Delivery schedule';
comment on column DELIVERY_SCHEDULE.ID is 'Delivery schedule id';
comment on column DELIVERY_SCHEDULE.HOSP_CODE is 'Hospital code';
comment on column DELIVERY_SCHEDULE.NAME is 'Name';
comment on column DELIVERY_SCHEDULE.DESCRIPTION is 'Description';
comment on column DELIVERY_SCHEDULE.STATUS is 'Record status';
comment on column DELIVERY_SCHEDULE.CREATE_USER is 'Created user';
comment on column DELIVERY_SCHEDULE.CREATE_DATE is 'Created date';
comment on column DELIVERY_SCHEDULE.UPDATE_USER is 'Last updated user';
comment on column DELIVERY_SCHEDULE.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY_SCHEDULE.VERSION is 'Version to serve as optimistic lock value';

create table DELIVERY_SCHEDULE_ITEM (
    ID number(19) not null,
    ORDER_TYPE varchar2(2) not null,
   	WARD_TYPE varchar2(1) not null,
	WARD_CODE_CSV clob null,
    WARD_GROUP_CSV clob null,
    SCHEDULE_TIME number(4) not null,
   	DUE_HOUR number(2) not null,
   	DUE_ON_TYPE varchar2(1) not null,
   	GENERATE_DAY number(10) not null,
   	URGENT_FLAG number(1) default 0 not null,
   	STAT_FLAG number(1) default 0 not null,
   	OVERDUE_FLAG number(1) default 0 not null,
   	HIGHLIGHT_FLAG number(1) default 0 not null,
    STATUS varchar2(1) not null,
    DELIVERY_SCHEDULE_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY_SCHEDULE_ITEM primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  DELIVERY_SCHEDULE_ITEM is 'Delivery schedule item';
comment on column DELIVERY_SCHEDULE_ITEM.ID is 'Delivery schedule item id';
comment on column DELIVERY_SCHEDULE_ITEM.ORDER_TYPE is 'Order type';
comment on column DELIVERY_SCHEDULE_ITEM.WARD_TYPE is 'Ward type';
comment on column DELIVERY_SCHEDULE_ITEM.WARD_CODE_CSV  is 'Ward code in CSV format';
comment on column DELIVERY_SCHEDULE_ITEM.WARD_GROUP_CSV is 'Ward group in CSV format';
comment on column DELIVERY_SCHEDULE_ITEM.SCHEDULE_TIME is 'Schedule time';
comment on column DELIVERY_SCHEDULE_ITEM.DUE_HOUR is 'Due hour';
comment on column DELIVERY_SCHEDULE_ITEM.DUE_ON_TYPE is 'Due on type';
comment on column DELIVERY_SCHEDULE_ITEM.GENERATE_DAY is 'Generate day';
comment on column DELIVERY_SCHEDULE_ITEM.URGENT_FLAG is 'Urgent';
comment on column DELIVERY_SCHEDULE_ITEM.STAT_FLAG is 'Stat';
comment on column DELIVERY_SCHEDULE_ITEM.OVERDUE_FLAG is 'Overdue';
comment on column DELIVERY_SCHEDULE_ITEM.HIGHLIGHT_FLAG is 'Highlighted';
comment on column DELIVERY_SCHEDULE_ITEM.STATUS is 'Record status';
comment on column DELIVERY_SCHEDULE_ITEM.DELIVERY_SCHEDULE_ID is 'Delivery schedule id';
comment on column DELIVERY_SCHEDULE_ITEM.CREATE_USER is 'Created user';
comment on column DELIVERY_SCHEDULE_ITEM.CREATE_DATE is 'Created date';
comment on column DELIVERY_SCHEDULE_ITEM.UPDATE_USER is 'Last updated user';
comment on column DELIVERY_SCHEDULE_ITEM.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY_SCHEDULE_ITEM.VERSION is 'Version to serve as optimistic lock value';

create table DELIVERY_REQUEST_GROUP (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    DELIVERY_SCHEDULE_ITEM_ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY_REQUEST_GROUP primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (maxvalue) tablespace P_PMS_MP_DATA_2012);

comment on table  DELIVERY_REQUEST_GROUP is 'Delivery request group';
comment on column DELIVERY_REQUEST_GROUP.ID is 'Delivery request group id';
comment on column DELIVERY_REQUEST_GROUP.STATUS is 'Record status';
comment on column DELIVERY_REQUEST_GROUP.DELIVERY_SCHEDULE_ITEM_ID is 'Delivery schedule item id';
comment on column DELIVERY_REQUEST_GROUP.HOSP_CODE is 'Hospital code';
comment on column DELIVERY_REQUEST_GROUP.WORKSTORE_CODE is 'Workstore code';
comment on column DELIVERY_REQUEST_GROUP.CREATE_USER is 'Created user';
comment on column DELIVERY_REQUEST_GROUP.CREATE_DATE is 'Created date';
comment on column DELIVERY_REQUEST_GROUP.UPDATE_USER is 'Last updated user';
comment on column DELIVERY_REQUEST_GROUP.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY_REQUEST_GROUP.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table DELIVERY_SCHEDULE;
drop table DELIVERY_SCHEDULE_ITEM;
drop table DELIVERY_REQUEST_GROUP;
--//