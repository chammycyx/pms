update PROP set DESCRIPTION='Print delta change indicators on large dispensing label', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=20042;
update OPERATION_PROP set SORT_SEQ='6', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=25019;
update OPERATION_PROP set SORT_SEQ='7', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=25020;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (21015,'machine.scriptPro.message.deltaChangeIndicator.required','[to be obsolete] Require delta change indicators in ScriptPro message','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,1);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,21015,'N',21015,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27027,'report.refill.criteria.drs.enquire','Enquire DRS patient only for Refill Item Report','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,1);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,27027,'N',27027,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP set DESCRIPTION='Print delta change indicator on large dispensing label', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=20042;
update OPERATION_PROP set SORT_SEQ='25019', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=25019;
update OPERATION_PROP set SORT_SEQ='25020', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where PROP_ID=25020;

delete from WORKSTORE_PROP where PROP_ID in (21015, 27027);

delete from PROP where ID in (21015, 27027);

--//