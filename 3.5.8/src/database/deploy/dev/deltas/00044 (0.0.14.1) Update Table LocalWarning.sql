alter table LOCAL_WARNING drop constraint UI_LOCAL_WARNING_01;
create index I_LOCAL_WARNING_01 on LOCAL_WARNING (WORKSTORE_GROUP_CODE, ITEM_CODE) tablespace PMS_INDX_01;

alter table LOCAL_WARNING modify WARN_CODE null;
update LOCAL_WARNING set WARN_CODE = null where WARN_CODE = ' ';


--//@UNDO
alter table LOCAL_WARNING add constraint UI_LOCAL_WARNING_01 unique (WORKSTORE_GROUP_CODE, ITEM_CODE, WARN_CODE) using index tablespace PMS_INDX_01;

drop index I_LOCAL_WARNING_01;

update LOCAL_WARNING set WARN_CODE = ' ' where WARN_CODE is null;
alter table LOCAL_WARNING modify WARN_CODE not null;
--//