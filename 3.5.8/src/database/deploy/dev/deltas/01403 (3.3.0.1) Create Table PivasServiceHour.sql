create table PIVAS_SERVICE_HOUR (
    ID number(19) not null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    DAY_OF_WEEK number(1) not null,
    SUPPLY_TIME number(4) not null,
    TYPE varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PIVAS_SERVICE_HOUR primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_PIVAS_SERVICE_HOUR unique (WORKSTORE_GROUP_CODE, DAY_OF_WEEK, SUPPLY_TIME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PIVAS_SERVICE_HOUR is 'Pivas service hour';
comment on column PIVAS_SERVICE_HOUR.ID is 'Pivas service hour id';
comment on column PIVAS_SERVICE_HOUR.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column PIVAS_SERVICE_HOUR.DAY_OF_WEEK is 'Day of week';
comment on column PIVAS_SERVICE_HOUR.SUPPLY_TIME is 'Supply time';
comment on column PIVAS_SERVICE_HOUR.TYPE is 'Type';
comment on column PIVAS_SERVICE_HOUR.CREATE_USER is 'Created user';
comment on column PIVAS_SERVICE_HOUR.CREATE_DATE is 'Created date';
comment on column PIVAS_SERVICE_HOUR.UPDATE_USER is 'Last updated user';
comment on column PIVAS_SERVICE_HOUR.UPDATE_DATE is 'Last updated date';
comment on column PIVAS_SERVICE_HOUR.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table PIVAS_SERVICE_HOUR;
--//