alter table PIVAS_PREP add UPPER_DOSE_LIMIT number(19,4);
alter table PIVAS_PREP add CHECK_DOSE_LIMIT_FLAG number(1) default 1 not null;
alter table PIVAS_PREP add CHECK_DUE_DATE_FLAG number(1) default 1 not null;

comment on column PIVAS_PREP.UPPER_DOSE_LIMIT is 'Upper dose limit';
comment on column PIVAS_PREP.CHECK_DOSE_LIMIT_FLAG is 'Check dose limit flag';
comment on column PIVAS_PREP.CHECK_DUE_DATE_FLAG is 'Check due date flag';

alter table PIVAS_PREP drop column SATELLITE_FLAG;
alter table PIVAS_PREP drop column SHELF_LIFE;
alter table PIVAS_PREP drop column PIVAS_FORMULA_METHOD_ID;
alter table PIVAS_PREP drop column DILUENT_ITEM_CODE;
alter table PIVAS_PREP drop column DILUENT_MANUF_CODE;
alter table PIVAS_PREP drop column DILUENT_BATCH_NUM;
alter table PIVAS_PREP drop column DILUENT_EXPIRY_DATE;
alter table PIVAS_PREP drop column WARN_CODE1;
alter table PIVAS_PREP drop column WARN_CODE2;

--//@UNDO
alter table PIVAS_PREP drop column UPPER_DOSE_LIMIT;
alter table PIVAS_PREP drop column CHECK_DOSE_LIMIT_FLAG;
alter table PIVAS_PREP drop column CHECK_DUE_DATE_FLAG;

alter table PIVAS_PREP add SATELLITE_FLAG number(1) default 0 not null;
alter table PIVAS_PREP add SHELF_LIFE number(10) not null;
alter table PIVAS_PREP add PIVAS_FORMULA_METHOD_ID number(19) not null;
alter table PIVAS_PREP add DILUENT_ITEM_CODE varchar2(10);
alter table PIVAS_PREP add DILUENT_MANUF_CODE varchar2(4);
alter table PIVAS_PREP add DILUENT_BATCH_NUM varchar2(20);
alter table PIVAS_PREP add DILUENT_EXPIRY_DATE date;
alter table PIVAS_PREP add WARN_CODE1 varchar2(4);
alter table PIVAS_PREP add WARN_CODE2 varchar2(4);
--//