update PROP set DESCRIPTION='Enable CMS Alert Enquiry screen in IP Vetting, IP Manual Profile entry', UPDATE_DATE=sysdate where ID=32032; 
update PROP set DESCRIPTION='Popup CMS Alert Enquiry screen in IP Vetting, IP Manual Profile entry', UPDATE_DATE=sysdate where ID=32033; 
update PROP set NAME='vetting.order.manual.alert.mds.check', UPDATE_DATE=sysdate where ID=32018; 

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (35014,'medProfile.manual.enabled','Enable IP Manual Profile entry','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,35014,'N',HOSP_CODE,35014,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32044,'vetting.medProfile.manual.alert.mds.check','Check MDS in IP Manual Profile entry','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,32044,'Y',HOSP_CODE,32044,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37010,'delivery.alert.mds.ddi.check','Check MDS Drug-Drug Interaction in Due Order Label Generation','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37010,'Y',HOSP_CODE,37010,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37011,'delivery.medProfile.manual.alert.mds.check','Check MDS for IP Manual Profile in Due Order Label Generation','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37011,'Y',HOSP_CODE,37011,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION = 'Enable CMS Alert Enquiry screen in IP Vetting', UPDATE_DATE=sysdate where ID=32032; 
update PROP set DESCRIPTION = 'Popup CMS Alert Enquiry screen in IP Vetting', UPDATE_DATE=sysdate where ID=32033; 
update PROP set NAME='vetting.manual.order.alert.mds.check', UPDATE_DATE=sysdate where ID=32018;

delete from HOSPITAL_PROP where PROP_ID in (35014, 32044, 37010, 37011);
delete from PROP where ID in (35014, 32044, 37010, 37011);

--//