update PROP set NAME='report.refill.criteria.schedule.duration',DESCRIPTION='Duration (in days) of refill date for Refill Item Report'  where ID=27003;

--//@UNDO
update PROP set NAME='report.refill.schedule.refillDate.duration', DESCRIPTION='Duration of refill date for Refill Enquiry' where ID=27003;
--//