create index I_MED_ORDER_T1 on MED_ORDER (HOSP_CODE, WORKSTORE_CODE, STATUS, ORDER_TYPE, ORDER_DATE) local online;
drop index I_MED_ORDER_06;
alter index I_MED_ORDER_T1 rename to I_MED_ORDER_06;

--//@UNDO
create index I_MED_ORDER_T1 on MED_ORDER (HOSP_CODE, WORKSTORE_CODE, ORDER_TYPE, ORDER_DATE) local online;
drop index I_MED_ORDER_06;
alter index I_MED_ORDER_T1 rename to I_MED_ORDER_06;
--//