alter table PIVAS_WORKLIST add AD_HOC_FLAG number(1) default 0 not null;

comment on column PIVAS_WORKLIST.AD_HOC_FLAG is 'Ad hoc flag';

--//@UNDO
alter table PIVAS_WORKLIST drop column AD_HOC_FLAG;
--//