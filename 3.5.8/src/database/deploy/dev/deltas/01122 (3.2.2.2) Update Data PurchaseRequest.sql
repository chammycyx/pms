update PURCHASE_REQUEST pr
set (pr.HOSP_CODE, pr.WORKSTORE_CODE) = 
(select mp.HOSP_CODE, mp.WORKSTORE_CODE from MED_PROFILE mp 
where mp.ID = pr.MED_PROFILE_ID)
where (pr.HOSP_CODE is null or pr.WORKSTORE_CODE is null);

alter table PURCHASE_REQUEST modify HOSP_CODE not null;
alter table PURCHASE_REQUEST modify WORKSTORE_CODE not null;

--//@UNDO
alter table PURCHASE_REQUEST modify HOSP_CODE null;
alter table PURCHASE_REQUEST modify WORKSTORE_CODE null;
--//