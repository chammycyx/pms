alter table MED_PROFILE_MO_ITEM add UNIT_DOSE_EXCEPT_FLAG number(1) null;
alter table MED_PROFILE_STAT add UNIT_DOSE_EXCEPT_FLAG number(1) null;

comment on column MED_PROFILE_MO_ITEM.UNIT_DOSE_EXCEPT_FLAG is 'Unit dose exception flag';
comment on column MED_PROFILE_STAT.UNIT_DOSE_EXCEPT_FLAG is 'Unit dose exception flag';

--//@UNDO
alter table MED_PROFILE_MO_ITEM drop column UNIT_DOSE_EXCEPT_FLAG;
alter table MED_PROFILE_STAT drop column UNIT_DOSE_EXCEPT_FLAG;
--//