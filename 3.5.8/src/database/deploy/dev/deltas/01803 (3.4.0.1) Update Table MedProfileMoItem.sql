alter table MED_PROFILE_MO_ITEM add SUSPEND_SUPPL_DESC varchar2(255);
comment on column MED_PROFILE_MO_ITEM.SUSPEND_SUPPL_DESC is 'Suspend supplementary description';

--//@UNDO
alter table MED_PROFILE_MO_ITEM set unused column SUSPEND_SUPPL_DESC;
--//