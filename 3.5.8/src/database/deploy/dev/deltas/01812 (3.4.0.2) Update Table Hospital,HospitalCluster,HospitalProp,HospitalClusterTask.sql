update HOSPITAL h set h.STATUS = 'D' 
where h.HOSP_CODE not in (select distinct w.HOSP_CODE from WORKSTORE w where w.STATUS = 'A') 
and h.STATUS = 'A';

delete from HOSPITAL_PROP hp
where hp.HOSP_CODE not in (select h.HOSP_CODE from HOSPITAL h where h.STATUS = 'A');

update HOSPITAL_CLUSTER hc set hc.STATUS = 'D' 
where hc.CLUSTER_CODE not in (select distinct h.CLUSTER_CODE from HOSPITAL h where h.STATUS = 'A') 
and hc.STATUS = 'A';

delete from HOSPITAL_CLUSTER_TASK hct 
where hct.CLUSTER_CODE not in (select hc.CLUSTER_CODE from HOSPITAL_CLUSTER hc where hc.STATUS = 'A');


--//@UNDO
--//