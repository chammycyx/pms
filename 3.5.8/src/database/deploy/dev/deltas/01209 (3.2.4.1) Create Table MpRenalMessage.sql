create table MP_RENAL_MESSAGE (
    ID number(19) not null,
    CREATE_DATE timestamp not null,
    CREATE_USER varchar2(12) not null,
    MESSAGE varchar2(200) not null,
    MED_PROFILE_ID number(19) not null,
    constraint PK_MP_RENAL_MESSAGE primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (maxvalue) tablespace P_PMS_MP_DATA_2012);

comment on table  MP_RENAL_MESSAGE is 'Renal message';
comment on column MP_RENAL_MESSAGE.ID is 'Renal message id';
comment on column MP_RENAL_MESSAGE.CREATE_DATE is 'Created date';
comment on column MP_RENAL_MESSAGE.CREATE_USER is 'Created user';
comment on column MP_RENAL_MESSAGE.MESSAGE is 'Message';
comment on column MP_RENAL_MESSAGE.MED_PROFILE_ID is 'Medication profile id';

alter table MP_RENAL_MESSAGE add constraint FK_MP_RENAL_MESSAGE_01 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);

create index FK_MP_RENAL_MESSAGE_01 on MP_RENAL_MESSAGE (MED_PROFILE_ID) local;
create index I_MP_RENAL_MESSAGE_01 on MP_RENAL_MESSAGE (MED_PROFILE_ID, CREATE_DATE) local;

create sequence SQ_MP_RENAL_MESSAGE increment by 50 start with 10000000000049;


--//@UNDO
drop index FK_MP_RENAL_MESSAGE_01;
drop index I_MP_RENAL_MESSAGE_01;

alter table MP_RENAL_MESSAGE drop constraint FK_MP_RENAL_MESSAGE_01;

drop table MP_RENAL_MESSAGE;

drop sequence SQ_MP_RENAL_MESSAGE;
--//