alter table MED_PROFILE_TRX_LOG rename column ITEM_NUM to REF_NUM;
comment on column MED_PROFILE_TRX_LOG.REF_NUM is 'Reference number';


--//@UNDO
alter table MED_PROFILE_TRX_LOG rename column REF_NUM to ITEM_NUM;
comment on column MED_PROFILE_TRX_LOG.ITEM_NUM is 'Item number';
--//