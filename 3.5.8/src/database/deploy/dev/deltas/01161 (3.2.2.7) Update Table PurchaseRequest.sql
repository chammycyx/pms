alter table PURCHASE_REQUEST add VERSION number(19) null;

update PURCHASE_REQUEST set VERSION = 1;

alter table PURCHASE_REQUEST modify VERSION not null;

comment on column PURCHASE_REQUEST.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
alter table PURCHASE_REQUEST drop column VERSION;
--//