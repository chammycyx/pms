alter table DELIVERY_ITEM add PIVAS_WORKLIST_ID number(19);
alter table DELIVERY_ITEM add MATERIAL_REQUEST_ITEM_ID number(19);
alter table DELIVERY_ITEM modify MED_PROFILE_PO_ITEM_ID null;

--//@UNDO
alter table DELIVERY_ITEM set unused column PIVAS_WORKLIST_ID;
alter table DELIVERY_ITEM set unused column MATERIAL_REQUEST_ITEM_ID;
alter table DELIVERY_ITEM modify MED_PROFILE_PO_ITEM_ID not null enable novalidate;
--//