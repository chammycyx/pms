alter table MED_ORDER add COMMENT_FLAG number(1) default 0 null;
update MED_ORDER MO set MO.COMMENT_FLAG = 
 case when exists
  (select 1 from MED_ORDER_ITEM MOI 
    where MO.ID = MOI.MED_ORDER_ID and MOI.COMMENT_TEXT is not null and MOI.STATUS != 'X') 
 then 1 else 0 end;
alter table MED_ORDER modify COMMENT_FLAG not null;

comment on column MED_ORDER.COMMENT_FLAG is 'Comment flag';

--//@UNDO
alter table MED_ORDER drop column COMMENT_FLAG;
--//
