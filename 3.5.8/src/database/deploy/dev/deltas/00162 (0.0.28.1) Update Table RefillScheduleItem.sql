alter table REFILL_SCHEDULE_ITEM add NUM_OF_REFILL number(10) null;

comment on column REFILL_SCHEDULE_ITEM.NUM_OF_REFILL is 'Number of Refill in Item level';

--update RefillScheduleItem NnumOfRefill
update REFILL_SCHEDULE_ITEM rsi
set rsi.NUM_OF_REFILL = ( select count(1) from REFILL_SCHEDULE_ITEM rsi2 where rsi2.PHARM_ORDER_ITEM_ID = rsi.PHARM_ORDER_ITEM_ID );

alter table REFILL_SCHEDULE_ITEM modify NUM_OF_REFILL not null;

--//@UNDO
alter table REFILL_SCHEDULE_ITEM drop column NUM_OF_REFILL;
--//