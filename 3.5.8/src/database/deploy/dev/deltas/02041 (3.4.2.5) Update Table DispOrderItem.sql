alter table DISP_ORDER_ITEM add SFI_OVERRIDE_WARN_FLAG number(1);
comment on column DISP_ORDER_ITEM.SFI_OVERRIDE_WARN_FLAG is 'SFI override warning flag';

--//@UNDO
alter table DISP_ORDER_ITEM set unused column SFI_OVERRIDE_WARN_FLAG;
--//