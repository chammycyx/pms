alter table PHARM_ORDER_ITEM add ORG_FORM_CODE varchar2(3) null;
update PHARM_ORDER_ITEM set ORG_FORM_CODE = FORM_CODE;

comment on column PHARM_ORDER_ITEM.ORG_FORM_CODE is 'Original form code';

--//@UNDO
alter table PHARM_ORDER_ITEM drop column ORG_FORM_CODE;
--//