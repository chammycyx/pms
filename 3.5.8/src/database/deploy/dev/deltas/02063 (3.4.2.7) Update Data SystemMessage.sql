update SYSTEM_MESSAGE set MAIN_MSG = 'This patient is subsidized by SF / CCF.  The amount of drug dispensed should not exceed a maximum of #0-day supply.  Proceed?' where MESSAGE_CODE = '1034';
update SYSTEM_MESSAGE set MAIN_MSG = 'This patient is subsidized by SF / CCF.  The amount of drug dispensed (#0) should not exceed a maximum of #1-day supply.  Proceed?' where MESSAGE_CODE = '1035';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'This patient is subsidized by SF / CCF.  The amount of drug dispensed should not exceed a maximun of #0-day supply.  Proceed?' where MESSAGE_CODE = '1034';
update SYSTEM_MESSAGE set MAIN_MSG = 'This patient is subsidized by SF / CCF.  The amount of drug dispensed (#0) should not exceed a maximum of #1-day supply.  Proceed?' where MESSAGE_CODE = '1035';
--//