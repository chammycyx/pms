update DISP_ORDER d set d.REVOKE_FLAG = 1 where exists
(select do.ID
from DISP_ORDER do 
join PHARM_ORDER po on (do.PHARM_ORDER_ID = po.ID)
join MED_ORDER mo on (po.MED_ORDER_ID = mo.ID)
where ((mo.DOC_TYPE = 'M' and do.DAY_END_STATUS = '-' and do.ADMIN_STATUS = 'S' and do.SUSPEND_CODE = 'UC')
or (mo.DOC_TYPE = 'N' and po.ADMIN_STATUS = 'U' and do.ADMIN_STATUS = 'S' and do.SUSPEND_CODE = 'UC' and do.REFILL_FLAG = 0)
or (mo.DOC_TYPE = 'N' and do.ADMIN_STATUS = 'S' and do.SUSPEND_CODE = 'UC' and do.REFILL_FLAG = 1))
and do.ID = d.ID)
and d.STATUS not in ('X')
and d.REVOKE_FLAG is null
and d.CREATE_DATE between to_date('20120101', 'yyyymmdd') and to_date('20160101', 'yyyymmdd');

--//@UNDO
--//