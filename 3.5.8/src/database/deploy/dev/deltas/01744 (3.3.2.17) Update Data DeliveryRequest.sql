update DELIVERY_REQUEST set STATUS = 'C' where STATUS is null;

alter table DELIVERY_REQUEST modify STATUS not null;


--//@UNDO
alter table DELIVERY_REQUEST modify STATUS null;
--//