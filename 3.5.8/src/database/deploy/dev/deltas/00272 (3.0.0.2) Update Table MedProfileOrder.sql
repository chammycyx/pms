alter table MED_PROFILE_ORDER add DISCONTINUE_HOSP_CODE varchar2(3) null;
alter table MED_PROFILE_ORDER add DISCONTINUE_DATE timestamp null;

comment on column MED_PROFILE_ORDER.DISCONTINUE_HOSP_CODE is 'Discontinue Hospital Code';
comment on column MED_PROFILE_ORDER.DISCONTINUE_DATE is 'Discontinue Date';

--//@UNDO
alter table MED_PROFILE_ORDER drop column DISCONTINUE_HOSP_CODE;
alter table MED_PROFILE_ORDER drop column DISCONTINUE_DATE;
--//