alter table PIVAS_BATCH_PREP_ITEM add AFTER_CONCN number(19,4);
alter table PIVAS_BATCH_PREP_ITEM add AFTER_VOLUME number(19,4);

comment on column PIVAS_BATCH_PREP_ITEM.AFTER_CONCN is 'After concentration';
comment on column PIVAS_BATCH_PREP_ITEM.AFTER_VOLUME is 'After volume';

--//@UNDO
alter table PIVAS_BATCH_PREP_ITEM set unused column AFTER_CONCN;
alter table PIVAS_BATCH_PREP_ITEM set unused column AFTER_VOLUME;
--//