insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1008',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'The original prescription is being [#0].  Unable to process refill coupon.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1009',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'This order will be set as pending.  Processing of the unsaved replenishment request of the same order will be cancelled.  Continue to save?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'This order will be set as suspended.  Processing of the unsaved replenishment request of the same order will be cancelled.  Continue to save?' where MESSAGE_CODE = '1006';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1008';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '1009';

update SYSTEM_MESSAGE set MAIN_MSG = 'The unsaved replenishment record will be cancelled after suspending this item, continue to suspend?' where MESSAGE_CODE = '1006';
--//