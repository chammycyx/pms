alter table MED_PROFILE_PO_ITEM add LAST_DELIVERY_ITEM_ID number(19) null;

comment on column MED_PROFILE_PO_ITEM.LAST_DELIVERY_ITEM_ID is 'Last delivery item id';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column LAST_DELIVERY_ITEM_ID;
--//