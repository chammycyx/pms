alter table MED_ORDER modify PRE_VET_USER_NAME varchar2(150);
alter table MED_ORDER modify WITHHOLD_USER_NAME varchar2(150);
alter table MED_PROFILE_MO_ITEM modify PENDING_USER_NAME varchar2(150);
alter table MED_PROFILE_ACTION_LOG modify ACTION_USER_NAME varchar2(150);

--//@UNDO
alter table MED_ORDER modify PRE_VET_USER_NAME varchar2(48);
alter table MED_ORDER modify WITHHOLD_USER_NAME varchar2(48);
alter table MED_PROFILE_MO_ITEM modify PENDING_USER_NAME varchar2(48);
alter table MED_PROFILE_ACTION_LOG modify ACTION_USER_NAME varchar2(48);
--//