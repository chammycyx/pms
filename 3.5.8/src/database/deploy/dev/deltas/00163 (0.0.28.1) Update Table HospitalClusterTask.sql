alter table HOSPITAL_CLUSTER_TASK drop column VERSION;

--//@UNDO
alter table HOSPITAL_CLUSTER_TASK add VERSION number(19) null;
update HOSPITAL_CLUSTER_TASK set VERSION = 1;
alter table HOSPITAL_CLUSTER_TASK modify VERSION not null;
--//