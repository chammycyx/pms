update SYSTEM_MESSAGE set MAIN_MSG = 'Patient Status has been changed<br><br>Discharge on: #0<br>Cancel Discharge on: #1<br>Time gap: #2<br><br>Would you like to reactivate the profile for dispensing?' where MESSAGE_CODE = '0693';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Patient Status has been changed<br><br>Discharge on: #0<br>Cancel Discharge on: #1<br>Time gap: #2<br><br>Would you like to re-active the profile for dispensing?' where MESSAGE_CODE = '0693';
--//