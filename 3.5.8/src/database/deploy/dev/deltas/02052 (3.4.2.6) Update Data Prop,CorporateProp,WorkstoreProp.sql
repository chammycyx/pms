update PROP set CACHE_IN_SESSION_FLAG=0, UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37021;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (20042,'label.dispLabel.largeLayout.deltaChangeIndicator.visible','Print delta change indicator on large dispensing label','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,1);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,20042,'N',20042,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32061,'vetting.order.recon.startDate.duration','Duration (in days) of start date for Rx reconciliation in OP Vetting','180,270,360','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,1);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,32061,'180',32061,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32062,'vetting.order.recon.endRx.duration','Duration (in days) of ended Rx for Rx reconciliation in OP Vetting','14','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,1);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,32062,'14',32062,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32063,'vetting.order.recon.endRx.retain.duration','Duration (in days) of ended Rx to be retained for Rx reconciliation in OP Vetting','90','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,1);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (32063,32063,'90',32063,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

--//@UNDO
update PROP set CACHE_IN_SESSION_FLAG=1, UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37021;

delete from WORKSTORE_PROP where PROP_ID in (20042, 32061, 32062);
delete from CORPORATE_PROP where PROP_ID in (32063);

delete from PROP where ID in (20042, 32061, 32062, 32063);

--//