alter table MED_PROFILE_PO_ITEM add TRADE_NAME varchar2(20) null;

comment on column MED_PROFILE_PO_ITEM.TRADE_NAME is 'Trade name';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column TRADE_NAME;
--//