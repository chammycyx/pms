create table CHEMO_SPECIALTY (
    ID number(19) not null,
    SPEC_CODE varchar2(4) not null,
    HOSP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CHEMO_SPECIALTY primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CHEMO_SPECIALTY_01 unique (HOSP_CODE, SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CHEMO_SPECIALTY is 'Chemo specialties';
comment on column CHEMO_SPECIALTY.ID is 'Chemo specialty id';
comment on column CHEMO_SPECIALTY.SPEC_CODE is 'Specialty code';
comment on column CHEMO_SPECIALTY.HOSP_CODE is 'Hospital code';
comment on column CHEMO_SPECIALTY.CREATE_USER is 'Created user';
comment on column CHEMO_SPECIALTY.CREATE_DATE is 'Created date';
comment on column CHEMO_SPECIALTY.UPDATE_USER is 'Last updated user';
comment on column CHEMO_SPECIALTY.UPDATE_DATE is 'Last updated date';
comment on column CHEMO_SPECIALTY.VERSION is 'Version to serve as optimistic lock value';

alter table CHEMO_SPECIALTY add constraint FK_CHEMO_SPECIALTY_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

create sequence SQ_CHEMO_SPECIALTY increment by 50 start with 10000000000049;

create index FK_CHEMO_SPECIALTY_01 on CHEMO_SPECIALTY (HOSP_CODE) tablespace PMS_INDX_01;

--//@UNDO
drop index FK_CHEMO_SPECIALTY_01;

drop sequence SQ_CHEMO_SPECIALTY;

alter table CHEMO_SPECIALTY drop constraint FK_CHEMO_SPECIALTY_01;

drop table CHEMO_SPECIALTY;
--//