create table CDDH_SPECIALTY_MAPPING (
    SPEC_CODE varchar2(4) not null,
    HOSP_CODE varchar2(3) not null,
    CDDH_SPEC_CODE varchar2(4) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CDDH_SPECIALTY_MAPPING primary key (HOSP_CODE, SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CDDH_SPECIALTY_MAPPING is 'Specialty mapping for CDDH display';
comment on column CDDH_SPECIALTY_MAPPING.SPEC_CODE is 'Specialty code';
comment on column CDDH_SPECIALTY_MAPPING.HOSP_CODE is 'Hospital code';
comment on column CDDH_SPECIALTY_MAPPING.CDDH_SPEC_CODE is 'Specialty code for CDDH display';
comment on column CDDH_SPECIALTY_MAPPING.STATUS is 'Record status';
comment on column CDDH_SPECIALTY_MAPPING.CREATE_USER is 'Created user';
comment on column CDDH_SPECIALTY_MAPPING.CREATE_DATE is 'Created date';
comment on column CDDH_SPECIALTY_MAPPING.UPDATE_USER is 'Last updated user';
comment on column CDDH_SPECIALTY_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column CDDH_SPECIALTY_MAPPING.VERSION is 'Version to serve as optimistic lock value';


create table CDDH_WORKSTORE_GROUP (
    CDDH_WORKSTORE_GROUP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CDDH_WORKSTORE_GROUP primary key (CDDH_WORKSTORE_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CDDH_WORKSTORE_GROUP is 'Workstore group for CDDH display';
comment on column CDDH_WORKSTORE_GROUP.CDDH_WORKSTORE_GROUP_CODE is 'Workstore group code for CDDH display';
comment on column CDDH_WORKSTORE_GROUP.STATUS is 'Record status';
comment on column CDDH_WORKSTORE_GROUP.CREATE_USER is 'Created user';
comment on column CDDH_WORKSTORE_GROUP.CREATE_DATE is 'Created date';
comment on column CDDH_WORKSTORE_GROUP.UPDATE_USER is 'Last updated user';
comment on column CDDH_WORKSTORE_GROUP.UPDATE_DATE is 'Last updated date';
comment on column CDDH_WORKSTORE_GROUP.VERSION is 'Version to serve as optimistic lock value';


create table CDDH_WORKSTORE_GROUP_HOS (
    HOSP_CODE varchar2(3) not null,
    CDDH_WORKSTORE_GROUP_CODE varchar2(3) not null,
    constraint PK_CDDH_WORKSTORE_GROUP_HOS primary key (HOSP_CODE, CDDH_WORKSTORE_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CDDH_WORKSTORE_GROUP_HOS is 'Middle table of CDDH workstore group and hospital';
comment on column CDDH_WORKSTORE_GROUP_HOS.HOSP_CODE is 'Hospital code';
comment on column CDDH_WORKSTORE_GROUP_HOS.CDDH_WORKSTORE_GROUP_CODE is 'Workstore group code for CDDH display';


create table HOSPITAL (
    HOSP_CODE varchar2(3) not null,
    CLUSTER_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL primary key (HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL is 'Hospitals';
comment on column HOSPITAL.HOSP_CODE is 'Hospital code';
comment on column HOSPITAL.CLUSTER_CODE is 'Hospital cluster code';
comment on column HOSPITAL.STATUS is 'Record status';
comment on column HOSPITAL.CREATE_USER is 'Created user';
comment on column HOSPITAL.CREATE_DATE is 'Created date';
comment on column HOSPITAL.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_CLUSTER (
    CLUSTER_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_CLUSTER primary key (CLUSTER_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_CLUSTER is 'Hospital clusters';
comment on column HOSPITAL_CLUSTER.CLUSTER_CODE is 'Hospital cluster code';
comment on column HOSPITAL_CLUSTER.STATUS is 'Record status';
comment on column HOSPITAL_CLUSTER.CREATE_USER is 'Created user';
comment on column HOSPITAL_CLUSTER.CREATE_DATE is 'Created date';
comment on column HOSPITAL_CLUSTER.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_CLUSTER.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_CLUSTER.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_CLUSTER_TASK (
    CLUSTER_CODE varchar2(3) not null,
    TASK_NAME varchar2(100) not null,
    TASK_DATE timestamp null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_CLUSTER_TASK primary key (CLUSTER_CODE, TASK_NAME) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_CLUSTER_TASK is 'Task control of hospital cluster to ensure only one WebLogic node running the listed tasks';
comment on column HOSPITAL_CLUSTER_TASK.CLUSTER_CODE is 'Hospital cluster code';
comment on column HOSPITAL_CLUSTER_TASK.TASK_NAME is 'Task name';
comment on column HOSPITAL_CLUSTER_TASK.TASK_DATE is 'Task execution date';
comment on column HOSPITAL_CLUSTER_TASK.CREATE_USER is 'Created user';
comment on column HOSPITAL_CLUSTER_TASK.CREATE_DATE is 'Created date';
comment on column HOSPITAL_CLUSTER_TASK.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_CLUSTER_TASK.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_CLUSTER_TASK.VERSION is 'Version to serve as optimistic lock value';


create table HOSPITAL_MAPPING (
    HOSP_CODE varchar2(3) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_HOSPITAL_MAPPING primary key (HOSP_CODE, PAT_HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  HOSPITAL_MAPPING is 'Relationship between patient hospitals and dispensing hospitals to allow orders of the patient hospital dispensed in the corresponding dispensing hospitals';
comment on column HOSPITAL_MAPPING.HOSP_CODE is 'Hospital code';
comment on column HOSPITAL_MAPPING.PAT_HOSP_CODE is 'Patient hospital code';
comment on column HOSPITAL_MAPPING.STATUS is 'Record status';
comment on column HOSPITAL_MAPPING.CREATE_USER is 'Created user';
comment on column HOSPITAL_MAPPING.CREATE_DATE is 'Created date';
comment on column HOSPITAL_MAPPING.UPDATE_USER is 'Last updated user';
comment on column HOSPITAL_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column HOSPITAL_MAPPING.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE_GROUP (
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE_GROUP primary key (WORKSTORE_GROUP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE_GROUP is 'Group of workstores using the same set of drugs';
comment on column WORKSTORE_GROUP.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WORKSTORE_GROUP.STATUS is 'Record status';
comment on column WORKSTORE_GROUP.CREATE_USER is 'Created user';
comment on column WORKSTORE_GROUP.CREATE_DATE is 'Created date';
comment on column WORKSTORE_GROUP.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE_GROUP.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE_GROUP.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE (
    WORKSTORE_CODE varchar2(4) not null,
    HOSP_CODE varchar2(3) not null,
    DEF_PAT_HOSP_CODE varchar2(3) null,
    OPERATION_PROFILE_ID number(19) null,
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    CDDH_WORKSTORE_GROUP_CODE varchar2(3) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE primary key (HOSP_CODE, WORKSTORE_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE is 'Pharmacy working stores';
comment on column WORKSTORE.WORKSTORE_CODE is 'Workstore code';
comment on column WORKSTORE.HOSP_CODE is 'Hospital code';
comment on column WORKSTORE.DEF_PAT_HOSP_CODE is 'Default patient hospital code';
comment on column WORKSTORE.OPERATION_PROFILE_ID is 'Operation profile id';
comment on column WORKSTORE.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WORKSTORE.CDDH_WORKSTORE_GROUP_CODE is 'Workstore group code for CDDH display';
comment on column WORKSTORE.STATUS is 'Record status';
comment on column WORKSTORE.CREATE_USER is 'Created user';
comment on column WORKSTORE.CREATE_DATE is 'Created date';
comment on column WORKSTORE.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE.VERSION is 'Version to serve as optimistic lock value';


create table WORKSTORE_GROUP_MAPPING (
    WORKSTORE_GROUP_CODE varchar2(3) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    SORT_SEQ number(10) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WORKSTORE_GROUP_MAPPING primary key (WORKSTORE_GROUP_CODE, PAT_HOSP_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WORKSTORE_GROUP_MAPPING is 'Relationship between patient hospitals and workstore groups to maintain the mapping of PHS and PAS specialities';
comment on column WORKSTORE_GROUP_MAPPING.WORKSTORE_GROUP_CODE is 'Workstore group code';
comment on column WORKSTORE_GROUP_MAPPING.PAT_HOSP_CODE is 'Patient hospital code';
comment on column WORKSTORE_GROUP_MAPPING.STATUS is 'Record status';
comment on column WORKSTORE_GROUP_MAPPING.SORT_SEQ is 'Sort sequence';
comment on column WORKSTORE_GROUP_MAPPING.CREATE_USER is 'Created user';
comment on column WORKSTORE_GROUP_MAPPING.CREATE_DATE is 'Created date';
comment on column WORKSTORE_GROUP_MAPPING.UPDATE_USER is 'Last updated user';
comment on column WORKSTORE_GROUP_MAPPING.UPDATE_DATE is 'Last updated date';
comment on column WORKSTORE_GROUP_MAPPING.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table CDDH_SPECIALTY_MAPPING;
drop table CDDH_WORKSTORE_GROUP;
drop table CDDH_WORKSTORE_GROUP_HOS;
drop table HOSPITAL;
drop table HOSPITAL_CLUSTER;
drop table HOSPITAL_CLUSTER_TASK;
drop table HOSPITAL_MAPPING;
drop table WORKSTORE;
drop table WORKSTORE_GROUP;
drop table WORKSTORE_GROUP_MAPPING;
--//