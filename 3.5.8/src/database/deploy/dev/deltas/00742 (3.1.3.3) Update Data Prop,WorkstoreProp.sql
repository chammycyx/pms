update prop set CACHE_IN_SESSION_FLAG = 1 where ID = 22016;

update prop set NAME = 'uncollect.dayEndOrder.alert.duration', DESCRIPTION='Duration (in days) of day end orders to alert in Uncollect' where ID=31001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG,REMINDER_MESSAGE) 
values (31002,'uncollect.dayEndOrder.duration','Duration (in days) of day end orders for Uncollect','1-365','dd',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,0,null,'S',null,0,null);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,31002,'365',31002,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

--//@UNDO
update prop set CACHE_IN_SESSION_FLAG = 0 where ID=22016;

update prop set NAME='uncollect.order.duration', DESCRIPTION='Duration (in days) of orders for Uncollect' where ID=31001;

delete from WORKSTORE_PROP where PROP_ID=31002;

delete from PROP where ID=31002;
--//