alter table PIVAS_PREP add CAL_END_DATE timestamp;

comment on column PIVAS_PREP.CAL_END_DATE is 'Calculated end date (used for IPMOE dose specified order, and is overridden by MED_PROFILE_MO_ITEM.END_DATE if exists)';

--//@UNDO
alter table PIVAS_PREP drop column CAL_END_DATE;
--//