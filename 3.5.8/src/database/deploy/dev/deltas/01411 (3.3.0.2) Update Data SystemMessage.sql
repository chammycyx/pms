update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid route / site.' where MESSAGE_CODE = '0376';    
update SYSTEM_MESSAGE set MAIN_MSG = 'The final concentration must be equal to the formula concentration.' where MESSAGE_CODE = '0897';    

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid route / site' where MESSAGE_CODE = '0376';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid route / site.' where MESSAGE_CODE = '0897';
--//