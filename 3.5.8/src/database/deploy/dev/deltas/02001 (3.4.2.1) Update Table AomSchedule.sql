alter table AOM_SCHEDULE add TYPE VARCHAR2(1) null;
comment on column AOM_SCHEDULE.TYPE is 'Type';
update AOM_SCHEDULE set TYPE = 'G' where TYPE is null;
alter table AOM_SCHEDULE modify TYPE not null;

--//@UNDO
alter table AOM_SCHEDULE set unused column TYPE;
--//