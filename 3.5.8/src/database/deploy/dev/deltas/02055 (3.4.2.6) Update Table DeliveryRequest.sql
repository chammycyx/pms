alter table DELIVERY_REQUEST add DELIVERY_REQUEST_GROUP_ID number(19);
comment on column DELIVERY_REQUEST.DELIVERY_REQUEST_GROUP_ID is 'Delivery request group id';

--//@UNDO
alter table DELIVERY_REQUEST set unused column DELIVERY_REQUEST_GROUP_ID;
--//