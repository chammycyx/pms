alter table WAIT_TIME_RPT_STAT add BATCH_DATE timestamp null;
comment on column WAIT_TIME_RPT_STAT.BATCH_DATE is 'Batch date';

update wait_time_rpt_stat s
set BATCH_DATE = to_date('01/' || s.month || '/' || s.year, 'dd/MM/YYYY')
where batch_date is null;

--//@UNDO
alter table WAIT_TIME_RPT_STAT drop column BATCH_DATE;
--//