update SYSTEM_MESSAGE set MAIN_MSG = 'FCS service is not available' where MESSAGE_CODE = '0014';
update SYSTEM_MESSAGE set MAIN_MSG = 'This prescription has not obtained payment clearance' where MESSAGE_CODE = '0015';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid format of MOE order no. / case no.' where MESSAGE_CODE = '0016';
update SYSTEM_MESSAGE set MAIN_MSG = 'PAS service is not available' where MESSAGE_CODE = '0023';
update SYSTEM_MESSAGE set MAIN_MSG = 'Prescription is not yet paid.  Proceed?' where MESSAGE_CODE = '0062';
update SYSTEM_MESSAGE set MAIN_MSG = 'FCS service is not available.  Proceed?' where MESSAGE_CODE = '0063';
update SYSTEM_MESSAGE set MAIN_MSG = 'FCS information cannot be retrieved.  Proceed?' where MESSAGE_CODE = '0064';
update SYSTEM_MESSAGE set MAIN_MSG = 'SFI invoice is not found in FCS' where MESSAGE_CODE = '0070';
update SYSTEM_MESSAGE set MAIN_MSG = 'Drug Charging Uncollect should not be marked for prescription processed within the last #0 days.<BR>Do you wish to continue?' where MESSAGE_CODE = '0122';
update SYSTEM_MESSAGE set MAIN_MSG = 'User does not have right to authorize SFI Force Proceed' where MESSAGE_CODE = '0170';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Drug Receipt No.' where MESSAGE_CODE = '0230';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Invoice No.', SUPPL_MSG = 'Incorrect format of Invoice No.' where MESSAGE_CODE = '0231';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Invoice No.', SUPPL_MSG = 'This SFI invoice does not belong to this hospital.' where MESSAGE_CODE = '0232';
update SYSTEM_MESSAGE set MAIN_MSG = 'The invoice has been voided' where MESSAGE_CODE = '0296';
update SYSTEM_MESSAGE set MAIN_MSG = 'Standard item / Rx is not found' where MESSAGE_CODE = '0401';
update SYSTEM_MESSAGE set MAIN_MSG = 'SFI invoice is not found for the Rx' where MESSAGE_CODE = '0402';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid format of invoice no. / MOE order no. / ticket no.' where MESSAGE_CODE = '0411';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input Drug Receipt No.' where MESSAGE_CODE = '0473';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input Drug Receipt No. / MOE Order No.' where MESSAGE_CODE = '0475';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input Downtime System Invoice No.' where MESSAGE_CODE = '0556';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invoice no. of the orginal SFI invoice is not accepted. Please input another invoice no.' where MESSAGE_CODE = '0624';
update SYSTEM_MESSAGE set MAIN_MSG = 'The invoice [#0] has been voided.' where MESSAGE_CODE = '0625';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0651',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'[#0] is suspended',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0652',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'CAPD split quantity cannot larger than 999999.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0651';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0652';
--//