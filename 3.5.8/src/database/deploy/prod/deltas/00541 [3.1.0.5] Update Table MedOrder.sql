alter table MED_ORDER add PRE_VET_USER_NAME varchar2(48);

comment on column MED_ORDER.PRE_VET_USER_NAME is 'Pre vet user name';

--//@UNDO
alter table MED_ORDER drop column PRE_VET_USER_NAME;
--//