insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0988',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Frozen item exists.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'Due order label generation for [#0] is not enabled.  Please check and update the setting in ward maintenance if required.', SEVERITY_CODE = 'I' where MESSAGE_CODE = '0683';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = '[#0] is not an IPMOE ward.  Please check and update the specialty mapping when required.', SEVERITY_CODE = 'W' where MESSAGE_CODE = '0683';

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0988';
--//