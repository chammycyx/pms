alter table MED_PROFILE_MO_ITEM add VERIFY_BEFORE_FLAG number(1) default 0;

comment on column MED_PROFILE_MO_ITEM.VERIFY_BEFORE_FLAG is 'Indicate the drug item that is verified before';

--//@UNDO
alter table MED_PROFILE_MO_ITEM drop column VERIFY_BEFORE_FLAG;
--//