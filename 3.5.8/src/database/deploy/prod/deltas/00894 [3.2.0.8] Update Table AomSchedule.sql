create table AOM_SCHEDULE (
    ID number(19) not null,
    ADMIN_TIME varchar2(1000) null,
    DAILY_FREQ_CODE varchar2(7) not null,
    HOSP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_AOM_SCHEDULE primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  AOM_SCHEDULE is 'Administration of Medications Schedule';
comment on column AOM_SCHEDULE.ID is 'Administration of Medications Schedule id';
comment on column AOM_SCHEDULE.ADMIN_TIME is 'Administration time';
comment on column AOM_SCHEDULE.DAILY_FREQ_CODE is 'Daily frequency code';
comment on column AOM_SCHEDULE.HOSP_CODE is 'Hospital code';
comment on column AOM_SCHEDULE.CREATE_USER is 'Created user';
comment on column AOM_SCHEDULE.CREATE_DATE is 'Created date';
comment on column AOM_SCHEDULE.UPDATE_USER is 'Last updated user';
comment on column AOM_SCHEDULE.UPDATE_DATE is 'Last updated date';
comment on column AOM_SCHEDULE.VERSION is 'Version to serve as optimistic lock value';

create sequence SQ_AOM_SCHEDULE increment by 50 start with 10000000000049;

alter table AOM_SCHEDULE add constraint FK_AOM_SCHEDULE_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

create index FK_AOM_SCHEDULE_01 on AOM_SCHEDULE (HOSP_CODE) tablespace PMS_INDX_01;

--//@UNDO
drop index FK_AOM_SCHEDULE_01;
drop sequence SQ_AOM_SCHEDULE;
drop table AOM_SCHEDULE;
--//