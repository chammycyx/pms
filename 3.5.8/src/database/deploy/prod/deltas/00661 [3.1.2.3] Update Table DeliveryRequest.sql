alter table DELIVERY_REQUEST add STATUS varchar2(1);

comment on column DELIVERY_REQUEST.STATUS is 'Delivery Request Status';

--//@UNDO
alter table DELIVERY_REQUEST drop column STATUS;
--//