alter table MED_PROFILE_MO_ITEM drop column CMM_ITEM_FLAG;
alter table MED_PROFILE_MO_ITEM drop column CMM_ITEM_STATUS;
alter table DELIVERY drop column CMM_DISP_ID;

--//@UNDO
alter table MED_PROFILE_MO_ITEM add (CMM_ITEM_FLAG number(1) default 0 not null)
comment on column MED_PROFILE_MO_ITEM.CMM_ITEM_FLAG is 'CMM item';

alter table MED_PROFILE_MO_ITEM add (CMM_ITEM_STATUS varchar2(1) default '-' not null)
comment on column MED_PROFILE_MO_ITEM.CMM_ITEM_STATUS is 'CMM item status';

alter table DELIVERY add (CMM_DISP_ID number(19) null);
comment on column DELIVERY.CMM_DISP_ID is 'Unique id of CMM dispensing record';
--//