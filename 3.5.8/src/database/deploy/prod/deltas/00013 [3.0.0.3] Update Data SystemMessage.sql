update SYSTEM_MESSAGE set MAIN_MSG = 'Drug checking completed without problem.' where MESSAGE_CODE = '0210';
update SYSTEM_MESSAGE set MAIN_MSG = 'You do not have access right to set order as pending.' where MESSAGE_CODE = '0302';
update SYSTEM_MESSAGE set MAIN_MSG = 'You do not have access right to resume from pending status.' where MESSAGE_CODE = '0303';
update SYSTEM_MESSAGE set MAIN_MSG = 'No invoice will be printed as all SFI drug charge is exempted for this patient [HA Eligible Person].' where MESSAGE_CODE = '0097';
update SYSTEM_MESSAGE set MAIN_MSG = 'PHS warning codes of this item have been updated. Please review the local warning codes in Follow Up on Modified PHS Warning Code.' where MESSAGE_CODE = '0036';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please select a SFI Force Proceed option.' where MESSAGE_CODE = '0553';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input daily frequency' where MESSAGE_CODE = '0370';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please select a force proceed reason.', SEVERITY_CODE ='W' where MESSAGE_CODE = '0092';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0633',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Please input dispense date',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0633';
--//