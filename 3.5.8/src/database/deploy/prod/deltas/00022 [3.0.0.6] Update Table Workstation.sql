alter table WORKSTATION add ADMIN_LEVEL number(10) null;
comment on column WORKSTATION.ADMIN_LEVEL is 'Admin level';

update WORKSTATION set ADMIN_LEVEL = 0;

alter table WORKSTATION modify ADMIN_LEVEL not null;

--//@UNDO
alter table WORKSTATION drop column ADMIN_LEVEL;
--//