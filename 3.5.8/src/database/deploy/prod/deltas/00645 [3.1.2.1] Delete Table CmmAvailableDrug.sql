drop table CMM_AVAILABLE_DRUG;

--//@UNDO
create table CMM_AVAILABLE_DRUG (
    ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    DISPLAY_NAME varchar2(70) not null,
    FORM_CODE varchar2(3) not null,
    SALT_PROPERTY varchar2(35) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CMM_AVAILABLE_DRUG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CMM_AVAILABLE_DRUG_01 unique (HOSP_CODE, DISPLAY_NAME, FORM_CODE, SALT_PROPERTY) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;
--//