alter table MED_PROFILE_TRX_LOG add REF_NUM_TEMP varchar2(100) null;
update MED_PROFILE_TRX_LOG set REF_NUM_TEMP = to_char(REF_NUM) ;
alter table MED_PROFILE_TRX_LOG drop column REF_NUM;
alter table MED_PROFILE_TRX_LOG rename column REF_NUM_TEMP to REF_NUM;
alter table MED_PROFILE_TRX_LOG modify REF_NUM not null;
comment on column MED_PROFILE_TRX_LOG.REF_NUM is 'Reference number';

--//@UNDO
alter table MED_PROFILE_TRX_LOG add REF_NUM_TEMP number(10);
update MED_PROFILE_TRX_LOG set REF_NUM_TEMP = (case nvl(to_number(regexp_substr(REF_NUM,'[0-9]+',1,2)),1) when 1 then to_number(REF_NUM) else to_number(regexp_substr(REF_NUM,'[0-9]+',1,2)) end);
alter table MED_PROFILE_TRX_LOG drop column REF_NUM;
alter table MED_PROFILE_TRX_LOG rename column REF_NUM_TEMP to REF_NUM;
alter table MED_PROFILE_TRX_LOG modify REF_NUM not null;
comment on column MED_PROFILE_TRX_LOG.REF_NUM is 'Reference number';
--//