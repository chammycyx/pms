alter table MED_ORDER add ORG_MED_ORDER_ID number(19);
alter table MED_ORDER add PREV_MED_ORDER_ID number(19);

comment on column MED_ORDER.ORG_MED_ORDER_ID is 'Original Medication order id';
comment on column MED_ORDER.PREV_MED_ORDER_ID is 'Previous Medication order id';

--//@UNDO
alter table MED_ORDER drop column ORG_MED_ORDER_ID;
alter table MED_ORDER drop column PREV_MED_ORDER_ID;
--//