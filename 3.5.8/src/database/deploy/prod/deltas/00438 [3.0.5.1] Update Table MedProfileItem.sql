alter table MED_PROFILE_ITEM add END_TYPE varchar2(1) null;

comment on column MED_PROFILE_ITEM.END_TYPE is 'End Type';

--//@UNDO
alter table MED_PROFILE_ITEM drop column END_TYPE;
--//