insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0666',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Dosage range conversion cannot be done for this item because non-parenteral item conversion will be based on out patient dosage conversion maintenance.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0667',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'Error case(s) is/are identified:<br>Ward: #0<br>Case number: #1','Please call IT support hotline (9038 2477) for handling the error case(s).<br><br>No label will be generated for this episode.<br><br>Please exclude the error cases in your generation criteria and generate the non-problematic cases again.',sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0668',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'Annotation has been successfully created.  Select CANCEL will return to inbox. Proceed?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'The batch date does not match with the batch code.  Please check.' where MESSAGE_CODE = '0644';
update SYSTEM_MESSAGE set MAIN_MSG = 'Select CANCEL will return to inbox.  Proceed?' where MESSAGE_CODE = '0079';
update SYSTEM_MESSAGE set MAIN_MSG = 'Confirm to delink and / or delete the drug label(s)?' where MESSAGE_CODE = '0203';
update SYSTEM_MESSAGE set MAIN_MSG = 'Drug checking completed.  The problem item(s) is / are deleted or delinked.' where MESSAGE_CODE = '0212';
update SYSTEM_MESSAGE set MAIN_MSG = 'Not all problem item is delinked.  Are you sure to save?' where MESSAGE_CODE = '0213';
update SYSTEM_MESSAGE set MAIN_MSG = 'Drug item is already deleted or delinked.' where MESSAGE_CODE = '0216';
update SYSTEM_MESSAGE set MAIN_MSG = 'Item has not been delinked.' where MESSAGE_CODE = '0348';
update SYSTEM_MESSAGE set MAIN_MSG = 'Not all problem item is delinked.  Are you sure to save and print transaction report?' where MESSAGE_CODE = '0657';
update SYSTEM_MESSAGE set MAIN_MSG = 'Delink / Delete Drug completed without problem.' where MESSAGE_CODE = '0664';
update SYSTEM_MESSAGE set MAIN_MSG = 'Delink / Delete Drug completed.  The problem item(s) is / are deleted or delinked.' where MESSAGE_CODE = '0665';


--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0666';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0667';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0668';
--//