update PROP set DESCRIPTION = 'Issue window number default for fast queue' where ID=15005;
update PROP set DESCRIPTION = 'Issue window number default for Uncollect prescriptions' where ID=16005;
update PROP set DESCRIPTION = 'Popup reverse Uncollect alert message in Checking and Issuing' where ID=16006;
update PROP set DESCRIPTION = 'Current ticket number for fast queue' where ID=30011;
update PROP set DESCRIPTION = 'Maximum ticket number for fast queue' where ID=30012;
update PROP set DESCRIPTION = 'Minimum ticket number for fast queue' where ID=30013;
update PROP set DESCRIPTION = 'Name of fast queue button in Ticket Generation' where ID=30017;
update PROP set DESCRIPTION = 'Enable fast queue at Ticket Generation' where ID=30022;
update PROP set DESCRIPTION = 'Maximum number of items per Rx for fast queue' where ID=30023;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32047,'vetting.medProfile.report.trx.print','Print Transaction Report for direct label print in IP Vetting','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,32047,'Y',32047,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP set DESCRIPTION = 'Issue window number of fast queue in Checking' where ID=15005;
update PROP set DESCRIPTION = 'Issue window number for Uncollect in Checking and Issuing' where ID=16005;
update PROP set DESCRIPTION = 'Popup reverse Uncollect in Checking and Issuing' where ID=16006;
update PROP set DESCRIPTION = 'Current ticket number of Ticket Generation fast queue' where ID=30011;
update PROP set DESCRIPTION = 'Maximum ticket number of Ticket Generation fast queue' where ID=30012;
update PROP set DESCRIPTION = 'Minimum ticket number of Ticket Generation fast queue' where ID=30013;
update PROP set DESCRIPTION = 'Name of Ticket Generation fast queue' where ID=30017;
update PROP set DESCRIPTION = 'Enable Ticket Generation fast queue' where ID=30022;
update PROP set DESCRIPTION = 'Maximum number of items per Rx allowed for Ticket Generation fast queue' where ID=30023;

delete from WORKSTORE_PROP where PROP_ID in (32047);
delete from PROP where ID in (32047);
--//