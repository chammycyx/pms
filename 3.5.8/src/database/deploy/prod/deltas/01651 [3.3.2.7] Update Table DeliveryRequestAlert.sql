alter table DELIVERY_REQUEST_ALERT add EHR_ALERT_PROFILE_FLAG number(1);
comment on column DELIVERY_REQUEST_ALERT.EHR_ALERT_PROFILE_FLAG is 'eHR Alert profile flag';

--//@UNDO
alter table DELIVERY_REQUEST_ALERT set unused column EHR_ALERT_PROFILE_FLAG;
--//