alter table MED_PROFILE_PO_ITEM add LAST_ISSUE_QTY number(19,4);

comment on column MED_PROFILE_PO_ITEM.LAST_ISSUE_QTY is 'Last issued quantity';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column LAST_ISSUE_QTY;
--//