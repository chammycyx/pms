insert into INFO_LINK (ID, NAME, URL, SORT_SEQ) values (1,'ePR','http://wcdciis09s/IS1_VH3/Content/test_data.asp',1);
insert into INFO_LINK (ID, NAME, URL, SORT_SEQ) values (2,'DoH - Search Drug Database','http://www.drugoffice.gov.hk/eps/do/en/consumer/search_drug_database.html',2);

--//@UNDO
delete INFO_LINK where id = 1;
delete INFO_LINK where id = 2;
--//