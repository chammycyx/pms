alter table MED_PROFILE_MO_ITEM add PENDING_USER_NAME varchar2(48);
alter table MED_PROFILE_MO_ITEM add LAST_ALERT_DATE timestamp;



comment on column MED_PROFILE_MO_ITEM.PENDING_USER_NAME is 'User name who pended item';
comment on column MED_PROFILE_MO_ITEM.LAST_ALERT_DATE is 'Last date which AD2 message received';




--//@UNDO

alter table MED_PROFILE_MO_ITEM drop column PENDING_USER_NAME;
alter table MED_PROFILE_MO_ITEM drop column LAST_ALERT_DATE;
--//
