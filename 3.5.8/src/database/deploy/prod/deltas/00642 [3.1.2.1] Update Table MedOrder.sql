alter table MED_ORDER add WITHHOLD_USER varchar2(12);
alter table MED_ORDER add WITHHOLD_USER_NAME varchar2(48);
alter table MED_ORDER add WITHHOLD_DATE timestamp;

comment on column MED_ORDER.WITHHOLD_USER is 'Withhold User';
comment on column MED_ORDER.WITHHOLD_USER_NAME is 'Withhold user name';
comment on column MED_ORDER.WITHHOLD_DATE is 'Withhold Date';

--//@UNDO
alter table MED_ORDER drop column WITHHOLD_USER;
alter table MED_ORDER drop column WITHHOLD_USER_NAME;
alter table MED_ORDER drop column WITHHOLD_DATE;
--//