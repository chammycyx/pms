alter table MED_PROFILE_ERROR_LOG add ERROR_XML clob;
comment on column MED_PROFILE_ERROR_LOG.ERROR_XML is 'Error Info in XML format';

alter table MED_PROFILE_ERROR_STAT add ERROR_XML clob;
comment on column MED_PROFILE_ERROR_STAT.ERROR_XML is 'Error Info in XML format';

--//@UNDO
alter table MED_PROFILE_ERROR_LOG drop column ERROR_XML;
alter table MED_PROFILE_ERROR_STAT drop column ERROR_XML;
--//


