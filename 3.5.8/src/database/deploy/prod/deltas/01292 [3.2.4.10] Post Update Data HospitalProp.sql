delete from HOSPITAL_PROP where PROP_ID in (35014, 35015);

--//@UNDO
insert all into HOSPITAL_PROP (ID,HOSP_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,HOSP_CODE,35014,'N',35014,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE from HOSPITAL where STATUS='A');

insert all into HOSPITAL_PROP (ID,HOSP_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,HOSP_CODE,35015,'N',35015,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE from HOSPITAL where STATUS='A');

--//