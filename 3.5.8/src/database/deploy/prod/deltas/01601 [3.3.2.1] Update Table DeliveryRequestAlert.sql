alter table DELIVERY_REQUEST_ALERT add DDI_CHECK_FLAG number(1);
comment on column DELIVERY_REQUEST_ALERT.DDI_CHECK_FLAG is 'Ddi checking flag';

--//@UNDO
alter table DELIVERY_REQUEST_ALERT set unused column DDI_CHECK_FLAG;
--//