update SYSTEM_MESSAGE set MAIN_MSG = 'Please input the route / site', SUPPL_MSG = '' where MESSAGE_CODE = '0360';
update SYSTEM_MESSAGE set SUPPL_MSG = 'For MOE prescription, please contact ITD to unvet the prescription and process again with a new ticket.  For manual prescription, please delete it and re-enter using a new ticket.' where MESSAGE_CODE = '0260';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0637',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'No Item in [#0] Batch. Process Aborted.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0637';
--//