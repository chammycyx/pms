update PHARM_ORDER_ITEM set DRUG_NAME = substr(DRUG_NAME, 1, 59); 

alter table PHARM_ORDER_ITEM modify DRUG_NAME varchar2(59);
alter table PHARM_ORDER_ITEM modify STRENGTH varchar2(59);
alter table MED_PROFILE_PO_ITEM modify DRUG_NAME varchar2(59);


--//@UNDO
alter table PHARM_ORDER_ITEM modify DRUG_NAME varchar2(171);
alter table PHARM_ORDER_ITEM modify STRENGTH varchar2(177);
alter table MED_PROFILE_PO_ITEM modify DRUG_NAME varchar2(57);
--//