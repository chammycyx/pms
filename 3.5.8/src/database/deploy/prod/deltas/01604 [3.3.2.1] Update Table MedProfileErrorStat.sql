alter table MED_PROFILE_ERROR_STAT add FROZEN_FLAG number(1);
comment on column MED_PROFILE_MO_ITEM.FROZEN_FLAG is 'Frozen status by ward transfer';

--//@UNDO
alter table MED_PROFILE_ERROR_STAT set unused column FROZEN_FLAG;
--//