update PROP set NAME='vetting.modifiedMoItem.printing.urgent' where ID=32019;

update PROP set CACHE_EXPIRE_TIME=0 where ID=15003;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32041,'vetting.urgentReconOrder.printing.urgent','Default urgent print for urgent Reconciliation Rx','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into OPERATION_PROP (ID,SORT_SEQ,PROP_ID,VALUE,OPERATION_PROFILE_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_OPERATION_PROP.nextval,32041,32041,'Y',ID,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select ID from OPERATION_PROFILE);

--//@UNDO
update PROP set NAME='vetting.modifiedMoItem.printing.urgent.enabled' where ID=32019;

update PROP set CACHE_EXPIRE_TIME=60 where ID=15003;

delete from OPERATION_PROP where PROP_ID=32041;
delete from PROP where ID=32041;
--//