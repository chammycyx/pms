alter table MED_PROFILE_PO_ITEM add ROUTE_CODE varchar2(7) null;
alter table MED_PROFILE_PO_ITEM add ROUTE_DESC varchar2(20) null;

comment on column MED_PROFILE_PO_ITEM.ROUTE_CODE is 'Route code';
comment on column MED_PROFILE_PO_ITEM.ROUTE_DESC is 'Route description';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column ROUTE_CODE;
alter table MED_PROFILE_PO_ITEM drop column ROUTE_DESC;
--//