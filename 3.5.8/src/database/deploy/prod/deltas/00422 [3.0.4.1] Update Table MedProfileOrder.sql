alter table MED_PROFILE_ORDER add REVIEW_DATE timestamp null;

comment on column MED_PROFILE_ORDER.REVIEW_DATE is 'Review Date';

--//@UNDO
alter table MED_PROFILE_ORDER drop column REVIEW_DATE;
--//