alter table WARD_CONFIG add MP_WARD_FLAG number(1);
update WARD_CONFIG set MP_WARD_FLAG = 0;
alter table WARD_CONFIG modify MP_WARD_FLAG number(1) not null;

comment on column WARD_CONFIG.MP_WARD_FLAG is 'Mp Ward Flag';

--//@UNDO
alter table WARD_CONFIG drop column MP_WARD_FLAG;
--//