update SYSTEM_MESSAGE set SEVERITY_CODE = 'Q', MAIN_MSG = 'Patient Status : Home Leave<br>This profile is read-only unless the patient status is changed to [Active].', SUPPL_MSG = 'Would you like to change the patient status from [Home Leave] to [Active]?' where MESSAGE_CODE = '0672';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'Q', MAIN_MSG = 'Patient Status : Pending Discharge<br>This profile is read-only unless the patient status is changed to [Active].', SUPPL_MSG = 'Would you like to change the patient status from [Pending Discharge] to [Active]?' where MESSAGE_CODE = '0673';

--//@UNDO
update SYSTEM_MESSAGE set SEVERITY_CODE = 'I', MAIN_MSG = 'The patient status is Home Leave.  This profile is read only unless the patient status is changed to Active.', SUPPL_MSG = null where MESSAGE_CODE = '0672';
update SYSTEM_MESSAGE set SEVERITY_CODE = 'I', MAIN_MSG = 'The patient status is Pending Discharge.  This profile is read only unless the patient status is changed to Active.', SUPPL_MSG = null where MESSAGE_CODE = '0673';
--//