alter table DELIVERY_REQUEST_ALERT add ALERT_PROFILE_FLAG number(1) default 0;
alter table DELIVERY_REQUEST_ALERT add MDS_CHECK_FLAG number(1) default 0;

comment on column DELIVERY_REQUEST_ALERT.ALERT_PROFILE_FLAG is 'Alert profile flag';
comment on column DELIVERY_REQUEST_ALERT.MDS_CHECK_FLAG is 'MDS check flag';

update DELIVERY_REQUEST_ALERT set ALERT_PROFILE_FLAG = case when ALERT_PROFILE_XML is null then 0 else 1 end;
alter table DELIVERY_REQUEST_ALERT modify ALERT_PROFILE_FLAG not null;
update DELIVERY_REQUEST_ALERT set MDS_CHECK_FLAG = 1;
alter table DELIVERY_REQUEST_ALERT modify MDS_CHECK_FLAG not null;

--//@UNDO
alter table DELIVERY_REQUEST_ALERT drop column ALERT_PROFILE_FLAG;
alter table DELIVERY_REQUEST_ALERT drop column MDS_CHECK_FLAG;
--//