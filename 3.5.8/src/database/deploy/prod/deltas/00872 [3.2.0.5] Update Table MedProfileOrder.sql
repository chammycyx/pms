alter table MED_PROFILE_ORDER add ITEM_NUM number(10);
alter table MED_PROFILE_ORDER add ORG_ITEM_NUM number(10);

comment on column MED_PROFILE_ORDER.ITEM_NUM is 'Item number';
comment on column MED_PROFILE_ORDER.ORG_ITEM_NUM is 'Original item number';

--//@UNDO
alter table MED_PROFILE_ORDER drop column ITEM_NUM;
alter table MED_PROFILE_ORDER drop column ORG_ITEM_NUM;
--//