alter table MED_PROFILE_MO_ITEM add MDS_ACK_SUSPEND_REASON varchar2(44) null;



comment on column MED_PROFILE_MO_ITEM.MDS_ACK_SUSPEND_REASON is 'Suspend reason for MDS acknowledgement';




--//@UNDO

alter table MED_PROFILE_MO_ITEM drop column MDS_ACK_SUSPEND_REASON;

--//