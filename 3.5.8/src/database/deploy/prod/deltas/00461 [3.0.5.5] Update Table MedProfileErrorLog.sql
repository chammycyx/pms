alter table MED_PROFILE_ERROR_LOG add HIDE_ERROR_STAT_FLAG number(1) default 0;

update MED_PROFILE_ERROR_LOG set HIDE_ERROR_STAT_FLAG = 0;

alter table MED_PROFILE_ERROR_LOG modify HIDE_ERROR_STAT_FLAG not null;


comment on column MED_PROFILE_ERROR_LOG.HIDE_ERROR_STAT_FLAG is 'Hide Error Stat Flag';



--//@UNDO

alter table MED_PROFILE_ERROR_LOG drop column HIDE_ERROR_STAT_FLAG;
--//





