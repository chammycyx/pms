create table CAPD_SUPPLIER_ITEM (
    ITEM_CODE varchar2(6) not null,
    SUPPLIER_CODE varchar2(4) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CAPD_SUPPLIER_ITEM primary key (ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CAPD_SUPPLIER_ITEM is 'Items available by CAPD suppliers';
comment on column CAPD_SUPPLIER_ITEM.ITEM_CODE is 'Item code';
comment on column CAPD_SUPPLIER_ITEM.SUPPLIER_CODE is 'Supplier code';
comment on column CAPD_SUPPLIER_ITEM.STATUS is 'Record status';
comment on column CAPD_SUPPLIER_ITEM.CREATE_USER is 'Created user';
comment on column CAPD_SUPPLIER_ITEM.CREATE_DATE is 'Created date';
comment on column CAPD_SUPPLIER_ITEM.UPDATE_USER is 'Last updated user';
comment on column CAPD_SUPPLIER_ITEM.UPDATE_DATE is 'Last updated date';
comment on column CAPD_SUPPLIER_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table CMM_AVAILABLE_DRUG (
    ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    DISPLAY_NAME varchar2(70) not null,
    FORM_CODE varchar2(3) not null,
    SALT_PROPERTY varchar2(35) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CMM_AVAILABLE_DRUG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_CMM_AVAILABLE_DRUG_01 unique (HOSP_CODE, DISPLAY_NAME, FORM_CODE, SALT_PROPERTY) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  CMM_AVAILABLE_DRUG is 'Available drugs from CMM';
comment on column CMM_AVAILABLE_DRUG.ID is 'CMM available drug id';
comment on column CMM_AVAILABLE_DRUG.HOSP_CODE is 'Hospital code';
comment on column CMM_AVAILABLE_DRUG.DISPLAY_NAME is 'Drug display name';
comment on column CMM_AVAILABLE_DRUG.FORM_CODE is 'Form code';
comment on column CMM_AVAILABLE_DRUG.SALT_PROPERTY is 'Salt property';
comment on column CMM_AVAILABLE_DRUG.STATUS is 'Record status';
comment on column CMM_AVAILABLE_DRUG.CREATE_USER is 'Created user';
comment on column CMM_AVAILABLE_DRUG.CREATE_DATE is 'Created date';
comment on column CMM_AVAILABLE_DRUG.UPDATE_USER is 'Last updated user';
comment on column CMM_AVAILABLE_DRUG.UPDATE_DATE is 'Last updated date';
comment on column CMM_AVAILABLE_DRUG.VERSION is 'Version to serve as optimistic lock value';


create table PATIENT_CAT (
    PAT_CAT_CODE varchar2(2) not null,
    DESCRIPTION varchar2(50) null,
    STATUS varchar2(1) not null,
    INST_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PATIENT_CAT primary key (INST_CODE, PAT_CAT_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PATIENT_CAT is 'Patient categories';
comment on column PATIENT_CAT.PAT_CAT_CODE is 'Patient category code';
comment on column PATIENT_CAT.DESCRIPTION is 'Description';
comment on column PATIENT_CAT.STATUS is 'Record status';
comment on column PATIENT_CAT.INST_CODE is 'Institution code';
comment on column PATIENT_CAT.CREATE_USER is 'Created user';
comment on column PATIENT_CAT.CREATE_DATE is 'Created date';
comment on column PATIENT_CAT.UPDATE_USER is 'Last updated user';
comment on column PATIENT_CAT.UPDATE_DATE is 'Last updated date';
comment on column PATIENT_CAT.VERSION is 'Version to serve as optimistic lock value';


create table SPECIALTY (
    SPEC_CODE varchar2(4) not null,
    INST_CODE varchar2(3) not null,
    DESCRIPTION varchar2(100) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SPECIALTY primary key (INST_CODE, SPEC_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  SPECIALTY is 'Specialties';
comment on column SPECIALTY.SPEC_CODE is 'Specialty code';
comment on column SPECIALTY.INST_CODE is 'Institution code';
comment on column SPECIALTY.DESCRIPTION is 'Description';
comment on column SPECIALTY.STATUS is 'Record status';
comment on column SPECIALTY.CREATE_USER is 'Created user';
comment on column SPECIALTY.CREATE_DATE is 'Created date';
comment on column SPECIALTY.UPDATE_USER is 'Last updated user';
comment on column SPECIALTY.UPDATE_DATE is 'Last updated date';
comment on column SPECIALTY.VERSION is 'Version to serve as optimistic lock value';


create table SUPPLIER (
    SUPPLIER_CODE varchar2(4) not null,
    NAME varchar2(100) null,
    NAME_CHI varchar2(100) null,
    CAPD_PHONE varchar2(13) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SUPPLIER primary key (SUPPLIER_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  SUPPLIER is 'Supplier contact information maintained for PMS use';
comment on column SUPPLIER.SUPPLIER_CODE is 'Supplier code';
comment on column SUPPLIER.NAME is 'English name';
comment on column SUPPLIER.NAME_CHI is 'Chinese name';
comment on column SUPPLIER.CAPD_PHONE is 'Phone number of CAPD supplier';
comment on column SUPPLIER.STATUS is 'Record status';
comment on column SUPPLIER.CREATE_USER is 'Created user';
comment on column SUPPLIER.CREATE_DATE is 'Created date';
comment on column SUPPLIER.UPDATE_USER is 'Last updated user';
comment on column SUPPLIER.UPDATE_DATE is 'Last updated date';
comment on column SUPPLIER.VERSION is 'Version to serve as optimistic lock value';


create table WARD (
    WARD_CODE varchar2(4) not null,
    INST_CODE varchar2(3) not null,
    DESCRIPTION varchar2(40) null,
    WARD_GROUP_CODE varchar2(4) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD primary key (INST_CODE, WARD_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD is 'Ward information from PHS';
comment on column WARD.WARD_CODE is 'Ward code';
comment on column WARD.INST_CODE is 'Institution code';
comment on column WARD.DESCRIPTION is 'Description';
comment on column WARD.WARD_GROUP_CODE is 'Ward group code';
comment on column WARD.STATUS is 'Record status';
comment on column WARD.CREATE_USER is 'Created user';
comment on column WARD.CREATE_DATE is 'Created date';
comment on column WARD.UPDATE_USER is 'Last updated user';
comment on column WARD.UPDATE_DATE is 'Last updated date';
comment on column WARD.VERSION is 'Version to serve as optimistic lock value';


create table WARD_ADMIN_FREQ (
    PAT_HOSP_CODE varchar2(3) not null,
    PAS_WARD_CODE varchar2(4) not null,
    FREQ_CODE varchar2(5) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD_ADMIN_FREQ primary key (PAT_HOSP_CODE, PAS_WARD_CODE, FREQ_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD_ADMIN_FREQ is 'Ward administration frequency from CMS';
comment on column WARD_ADMIN_FREQ.PAT_HOSP_CODE is 'Patient hospital code';
comment on column WARD_ADMIN_FREQ.PAS_WARD_CODE is 'PAS ward code';
comment on column WARD_ADMIN_FREQ.FREQ_CODE is 'Daily frequency code';
comment on column WARD_ADMIN_FREQ.CREATE_USER is 'Created user';
comment on column WARD_ADMIN_FREQ.CREATE_DATE is 'Created date';
comment on column WARD_ADMIN_FREQ.UPDATE_USER is 'Last updated user';
comment on column WARD_ADMIN_FREQ.UPDATE_DATE is 'Last updated date';
comment on column WARD_ADMIN_FREQ.VERSION is 'Version to serve as optimistic lock value';
comment on column WARD_ADMIN_FREQ.STATUS is 'Record status';


create table WARD_STOCK (
    WARD_CODE varchar2(4) not null,
    INST_CODE varchar2(3) not null,
    ITEM_CODE varchar2(6) not null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_WARD_STOCK primary key (INST_CODE, WARD_CODE, ITEM_CODE) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WARD_STOCK is 'Ward stock items from PHS';
comment on column WARD_STOCK.WARD_CODE is 'Ward code';
comment on column WARD_STOCK.INST_CODE is 'Institution code';
comment on column WARD_STOCK.ITEM_CODE is 'Item code';
comment on column WARD_STOCK.STATUS is 'Record status';
comment on column WARD_STOCK.CREATE_USER is 'Created user';
comment on column WARD_STOCK.CREATE_DATE is 'Created date';
comment on column WARD_STOCK.UPDATE_USER is 'Last updated user';
comment on column WARD_STOCK.UPDATE_DATE is 'Last updated date';
comment on column WARD_STOCK.VERSION is 'Version to serve as optimistic lock value';


--//@UNDO
drop table CAPD_SUPPLIER_ITEM;
drop table CMM_AVAILABLE_DRUG;
drop table PATIENT_CAT;
drop table SPECIALTY;
drop table SUPPLIER;
drop table WARD;
drop table WARD_ADMIN_FREQ;
drop table WARD_STOCK;
--//