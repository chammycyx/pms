update PROP set DESCRIPTION='Subtracted time buffer (in hours) for PIVAS supply date', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=39010;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (39016,'pivas.currentDate.buffer','Subtracted time buffer (in hours) for PIVAS current date','0-9','HH',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,39016,'2',HOSP_CODE,39016,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION='Added time buffer (in hours) for PIVAS supply date', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=39010;

delete from HOSPITAL_PROP where PROP_ID in (39016);
delete from PROP where ID in (39016);
--//