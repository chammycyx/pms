alter table MED_PROFILE_ORDER add CMS_ADMIN_STATUS number(10);

comment on column MED_PROFILE_ORDER.CMS_ADMIN_STATUS is 'CMS Admin Status';

--//@UNDO
alter table MED_PROFILE_ORDER drop column CMS_ADMIN_STATUS;
--//