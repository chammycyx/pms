alter table CHEST_ITEM add SORT_SEQ number(10) null;

comment on column CHEST_ITEM.SORT_SEQ is 'Sort sequence';

update PROP set CACHE_IN_SESSION_FLAG=1 where ID = 22010;

--//@UNDO
alter table CHEST_ITEM drop column SORT_SEQ;
update PROP set CACHE_IN_SESSION_FLAG=0 where ID = 22010;
--//