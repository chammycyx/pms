update SYSTEM_MESSAGE set MAIN_MSG = 'Purchase quantity should be greater than zero.' where MESSAGE_CODE = '0750';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Purchase quantity should be greater than or equal to zero.' where MESSAGE_CODE = '0750';
--//