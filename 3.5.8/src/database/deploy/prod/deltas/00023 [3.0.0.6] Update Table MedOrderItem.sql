alter table MED_ORDER_ITEM add CMS_FM_STATUS varchar2(1) null;

comment on column MED_ORDER_ITEM.CMS_FM_STATUS is 'CMS FM status';

--//@UNDO
alter table MED_ORDER_ITEM drop column CMS_FM_STATUS;
--//