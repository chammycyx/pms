alter table MED_PROFILE add OUT_CURR_SEQ_NUM number(10);

comment on column MED_PROFILE.OUT_CURR_SEQ_NUM is 'Current sequence number for outgoing message';

--//@UNDO
alter table MED_PROFILE drop column OUT_CURR_SEQ_NUM;
--//