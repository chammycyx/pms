update PROP set MAINT_SCREEN='I' where ID in (32034, 35001);
update PROP set MAINT_SCREEN='O', DESCRIPTION='Default patient category' where ID=22008;
update PROP set MAINT_SCREEN='O', DESCRIPTION='Patient category compulsory' where ID=22009;
update PROP set MAINT_SCREEN='O', DESCRIPTION='Alert time (in minutes)' where ID=34001;

delete from WORKSTORE_PROP where PROP_ID=34002;
delete from PROP where ID=34002;

--//@UNDO
update PROP set MAINT_SCREEN='S' where ID in (32034, 35001);
update PROP set MAINT_SCREEN='S', DESCRIPTION='Default patient category for One Stop Entry' where ID=22008;
update PROP set MAINT_SCREEN='S', DESCRIPTION='Patient category compulsory for One Stop Entry' where ID=22009;
update PROP set MAINT_SCREEN='S', DESCRIPTION='Elapsed time (in minutes) to treat non-issued orders as outstanding ones' where ID=34001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG,REMINDER_MESSAGE) values (34002,'workstore.name','Workstore description','1-50','S',1,0,'pmsadmin',to_timestamp('29-JUL-11 12.00.00.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('29-JUL-11 12.00.00.000000000 AM','DD-MON-RR HH.MI.SS.FF AM'),0,null,'S',null,0,null);
insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,34002,'Workstore description',34002,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE);
--//