update PROP set DESCRIPTION='[to be obsolete] FCS rollout: Enhancement of GS direct payment' where ID=14028;
update PROP set DESCRIPTION='Print dispensing barcode on dispensing label' where ID=20001;
update PROP set DESCRIPTION='Print instruction on ScriptPro dispensing label' where ID=20002;
update PROP set DESCRIPTION='Print itemcode barcode on dispensing label' where ID=20003;
update PROP set DESCRIPTION='Print user ID on dispensing label' where ID=20008;
update PROP set DESCRIPTION='Print ward return barcode on IP dispensing label' where ID=20011;

update PROP set VALIDATION='1-90' where ID in (37006, 37007);

update CORPORATE_PROP set VALUE='90' where ID in (37006, 37007);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32040,'vetting.alert.mds.ddi.check.rollout','[to be obsolete] MOE and IPMOE rollout: MDS Drug-Drug Interaction checking on continuous infusion order','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,32040,'N',HOSP_CODE,32040,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (36005,'inbox.dischargeOrder.duration.limit','Limited duration (in days) of discharge orders allowed for Pharmacy Inbox','1-90','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,36005,'90',HOSP_CODE,36005,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION='[to be obsolete] For external system: FCS rollouts enhancement of GS direct payment' where ID=14028;
update PROP set DESCRIPTION='Show dispensing label with dispensing barcode' where ID=20001;
update PROP set DESCRIPTION='Show dispensing label with instruction' where ID=20002;
update PROP set DESCRIPTION='Show dispensing label with itemcode barcode' where ID=20003;
update PROP set DESCRIPTION='Show dispensing label with user ID' where ID=20008;
update PROP set DESCRIPTION='Show IP dispensing label with ward return barcode' where ID=20011;

update PROP set VALIDATION='1-999' where ID in (37006, 37007);

delete from HOSPITAL_PROP where PROP_ID=32040;
delete from PROP where ID=32040;

delete from HOSPITAL_PROP where PROP_ID=36005;
delete from PROP where ID=36005;
--//