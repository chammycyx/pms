alter table PRN_ROUTE add ROUTE_DESC varchar2(20) not null;

comment on column PRN_ROUTE.ROUTE_DESC is 'Route description';

--//@UNDO
alter table PRN_ROUTE drop column ROUTE_DESC;
--//