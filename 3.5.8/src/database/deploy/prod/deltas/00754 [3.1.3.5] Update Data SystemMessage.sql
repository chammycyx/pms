insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0711',null,'en_US',null,1007,'A',null,null,0,null,null,null,0,sysdate,null,null,null,'Formulary Management Report|Download Report|PatHospCode [#0]|FileName [#1]|',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'The dispensing method cannot be revert to [Manual] once it set as [Script Pro] , [Bakercell] or [ATDPS]. Are you sure this setting is correct?' where MESSAGE_CODE = '0533';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0711';
update SYSTEM_MESSAGE set MAIN_MSG = 'The dispensing method cannot be revert to [Manual] once it set as [Script Pro] or [Bakercell]. Are you sure this setting is correct?' where MESSAGE_CODE = '0533';
--//