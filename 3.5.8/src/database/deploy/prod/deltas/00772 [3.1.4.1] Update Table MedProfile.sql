alter table MED_PROFILE add ADMISSION_DATE timestamp;

comment on column MED_PROFILE.ADMISSION_DATE is 'Admission Date';

--//@UNDO
alter table MED_PROFILE drop column ADMISSION_DATE;
--//