create table MED_PROFILE_ACTION_LOG (
    ID number(19) not null,
    CREATE_DATE timestamp not null,
    TYPE varchar2(2) not null,
    MESSAGE varchar2(255) null,
    SUPPL_MESSAGE varchar2(255) null,
    WARD_CODE varchar2(4) null,
    ACTION_USER varchar2(12) not null,
    ACTION_USER_NAME varchar2(48) null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    constraint PK_MED_PROFILE_ACTION_LOG primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012,
    partition P_PMS_MP_DATA_2013 values less than (to_date('20140101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2013);

comment on table  MED_PROFILE_ACTION_LOG is 'Action logs of medication profiles';
comment on column MED_PROFILE_ACTION_LOG.ID is 'Medication profile action log id';
comment on column MED_PROFILE_ACTION_LOG.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_ACTION_LOG.TYPE is 'Medication profile action log type';
comment on column MED_PROFILE_ACTION_LOG.MESSAGE is 'Message';
comment on column MED_PROFILE_ACTION_LOG.SUPPL_MESSAGE is 'Supplementary message';
comment on column MED_PROFILE_ACTION_LOG.ACTION_USER is 'Action user';
comment on column MED_PROFILE_ACTION_LOG.ACTION_USER_NAME is 'Action user name';
comment on column MED_PROFILE_ACTION_LOG.MED_PROFILE_MO_ITEM_ID is 'Medication profile mo item id';

alter table MED_PROFILE_ACTION_LOG add constraint FK_MED_PROFILE_ACTION_LOG_01 foreign key (MED_PROFILE_MO_ITEM_ID) references MED_PROFILE_MO_ITEM (ID);

create index FK_MED_PROFILE_ACTION_LOG_01 on MED_PROFILE_ACTION_LOG (MED_PROFILE_MO_ITEM_ID) local;


--//@UNDO
drop index FK_MED_PROFILE_ACTION_LOG_01;

alter table MED_PROFILE_ACTION_LOG drop constraint FK_MED_PROFILE_ACTION_LOG_01;

drop table MED_PROFILE_ACTION_LOG;
--//