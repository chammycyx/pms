alter table MED_PROFILE add INACTIVE_TYPE varchar2(1) null;
alter table MED_PROFILE add KEEP_ACTIVE_UNTIL_DATE date null;
alter table MED_PROFILE add CANCEL_ADMISSION_DATE timestamp null;

comment on column MED_PROFILE.INACTIVE_TYPE is 'Inactive type';
comment on column MED_PROFILE.KEEP_ACTIVE_UNTIL_DATE is 'Keep the profile status as Active/Home Leave/Pending Discharge until';
comment on column MED_PROFILE.CANCEL_ADMISSION_DATE is 'Cancel admission date';

--//@UNDO
alter table MED_PROFILE drop column INACTIVE_TYPE;
alter table MED_PROFILE drop column KEEP_ACTIVE_UNTIL_DATE;
alter table MED_PROFILE drop column CANCEL_ADMISSION_DATE;
--//