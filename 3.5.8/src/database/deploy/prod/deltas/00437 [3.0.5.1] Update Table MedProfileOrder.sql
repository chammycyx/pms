alter table MED_PROFILE_ORDER add CMS_CREATE_DATE timestamp null;

comment on column MED_PROFILE_ORDER.CMS_CREATE_DATE is 'CMS Create Date';

--//@UNDO
alter table MED_PROFILE_ORDER drop column CMS_CREATE_DATE;
--//