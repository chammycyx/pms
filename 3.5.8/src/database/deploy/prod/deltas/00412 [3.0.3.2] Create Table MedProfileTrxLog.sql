create table MED_PROFILE_TRX_LOG (
    ID number(19) not null,
    CREATE_DATE timestamp not null,
    ITEM_NUM number(10) not null,
    TYPE varchar2(3) not null,
    MED_PROFILE_ID number(19) not null,
    constraint PK_MED_PROFILE_TRX_LOG primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012,
    partition P_PMS_MP_DATA_2013 values less than (to_date('20140101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2013);

comment on table  MED_PROFILE_TRX_LOG is 'Transaction logs of medication profiles';
comment on column MED_PROFILE_TRX_LOG.ID is 'Medication profile transaction log id';
comment on column MED_PROFILE_TRX_LOG.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_TRX_LOG.ITEM_NUM is 'Item number';
comment on column MED_PROFILE_TRX_LOG.TYPE is 'Medication profile transaction log type';
comment on column MED_PROFILE_TRX_LOG.MED_PROFILE_ID is 'Medication profile id';

alter table MED_PROFILE_TRX_LOG add constraint FK_MED_PROFILE_TRX_LOG_01 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);

create index FK_MED_PROFILE_TRX_LOG_01 on MED_PROFILE_TRX_LOG (MED_PROFILE_ID) local;


--//@UNDO
drop index FK_MED_PROFILE_TRX_LOG_01;

alter table MED_PROFILE_TRX_LOG drop constraint FK_MED_PROFILE_TRX_LOG_01;

drop table MED_PROFILE_TRX_LOG;
--//