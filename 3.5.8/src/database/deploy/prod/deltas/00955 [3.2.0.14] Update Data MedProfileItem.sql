update MED_PROFILE_ITEM set AMEND_FLAG = 0 where AMEND_FLAG = 1;

update MED_PROFILE_ITEM i1 set i1.AMEND_FLAG = 1 where i1.ID in 
(select distinct i2.ID from MED_PROFILE_MO_ITEM moi2 
join MED_PROFILE_ITEM i2 on (moi2.MED_PROFILE_ITEM_ID = i2.ID)
where i2.AMEND_FLAG = 0 and i2.STATUS in ('U','V','W') and 
i2.MED_PROFILE_MO_ITEM_ID <> moi2.ID and moi2.STATUS = 'N');

update MED_PROFILE_ITEM i1 set i1.AMEND_FLAG = 1 where i1.ID in 
(select distinct i2.ID from MED_PROFILE_MO_ITEM moi2 
join MED_PROFILE_ITEM i2 on (moi2.MED_PROFILE_ITEM_ID = i2.ID)
where i2.AMEND_FLAG = 0 and i2.STATUS not in ('U','V','W')
and moi2.STATUS = 'N');

--//@UNDO
--//