alter table PHARM_ORDER_ITEM add TRADE_NAME varchar2(20) null;

comment on column PHARM_ORDER_ITEM.TRADE_NAME is 'Trade name';

--//@UNDO
alter table PHARM_ORDER_ITEM drop column TRADE_NAME;
--//