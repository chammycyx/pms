update PROP set MAINT_SCREEN = 'S' where ID = 30005;
update PROP set MAINT_SCREEN = 'S' where ID = 30008;
update PROP set MAINT_SCREEN = 'S' where ID = 30011;

update PROP set DESCRIPTION='Minimum ticket number for private patient' where ID=30007;

update PROP set REMINDER_MESSAGE='Patient alert profile enquiry service is not available.  Therefore, the system cannot perform Allergy, ADR and G6PD Deficiency Contraindication checking.  Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' where ID=10001;
update PROP set REMINDER_MESSAGE='Medication Decision Support (MDS) service is not available for all drugs. Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' || chr(13) || chr(10) || 'Drug information enquiry service is not available.' where ID=10003;
update PROP set REMINDER_MESSAGE='Drug on-hand enquiry service is not available.  Therefore, the system cannot perform Drug-Drug Interaction checking between medications on the current prescription and on-hand drugs prescribed at your local hospital.  Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' || chr(13) || chr(10) || 'Formulary management indication enquiry service is not available.' where ID=23001;

delete from HOSPITAL_PROP where PROP_ID=10013;
delete from PROP where ID=10013;

--//@UNDO
update PROP set MAINT_SCREEN = '-' where ID = 30005;
update PROP set MAINT_SCREEN = '-' where ID = 30008;
update PROP set MAINT_SCREEN = '-' where ID = 30011;

update PROP set DESCRIPTION = 'Minimum ticket Number for private patient' where ID=30007;

update PROP set REMINDER_MESSAGE='Patient alert profile service is not available.' where id=10001;
update PROP set REMINDER_MESSAGE='Medication Decision Support (MDS) service is not available for all drugs. Please exercise your professional judgement during the downtime period and contact IT system support for assistance.' where id=10003;
update PROP set REMINDER_MESSAGE='Drug on-hand service is not available.' || chr(13) || chr(10) || 'Formulary management indication enquiry service is not available.' where id=23001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG,REMINDER_MESSAGE) values (10013,'alert.drugInfo.enabled','Enable Drug Information service','Y,N','B',1,60,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),0,null,'S',null,0,null);

insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000384,10013,'Y','VH',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000385,10013,'Y','QES',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000386,10013,'Y','QMH',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'itdadmin',to_timestamp('24-NOV-12 10.21.10.309000000 AM','DD-MON-RR HH.MI.SS.FF AM'),3);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000387,10013,'Y','TMH',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000388,10013,'Y','LKK',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000389,10013,'Y','QEH',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000390,10013,'Y','UCH',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);
insert into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) values (1000391,10013,'Y','ALC',10013,'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'pmsadmin',to_timestamp('06-JUN-12 06.43.05.000000000 PM','DD-MON-RR HH.MI.SS.FF AM'),1);

--//