insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (36006,'inbox.historicalDischargeOrder.duration.limit','Limited duration (in days) of historical discharge orders allowed for Pharmacy Inbox','1-90','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,36006,'90',HOSP_CODE,36006,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

update HOSPITAL_PROP set VALUE='30' where PROP_ID=36005;

--//@UNDO
delete from HOSPITAL_PROP where PROP_ID=36006;
delete from PROP where ID=36006;

update HOSPITAL_PROP set VALUE='90' where PROP_ID=36005;
--//