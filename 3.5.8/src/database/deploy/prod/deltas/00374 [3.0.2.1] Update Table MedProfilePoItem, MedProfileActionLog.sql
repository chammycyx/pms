alter table MED_PROFILE_PO_ITEM add UNIT_DOSE_FLAG number(1);
comment on column MED_PROFILE_PO_ITEM.UNIT_DOSE_FLAG is 'Unit dose';

update MED_PROFILE_PO_ITEM set UNIT_DOSE_FLAG = 0;
alter table MED_PROFILE_PO_ITEM modify UNIT_DOSE_FLAG not null;

alter table MED_PROFILE_ACTION_LOG add ACTION_CODE varchar2(2);
comment on column MED_PROFILE_ACTION_LOG.ACTION_CODE is 'Action code (Pending / Suspend code)';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column UNIT_DOSE_FLAG;
alter table MED_PROFILE_ACTION_LOG drop column ACTION_CODE;
--//