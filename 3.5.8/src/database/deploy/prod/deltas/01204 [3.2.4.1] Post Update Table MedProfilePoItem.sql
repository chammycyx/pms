alter table MED_PROFILE_PO_ITEM drop column ITEM_NUM;

--//@UNDO
alter table MED_PROFILE_PO_ITEM add ITEM_NUM number(10) null;
comment on column MED_PROFILE_PO_ITEM.ITEM_NUM is 'Item number';
--//