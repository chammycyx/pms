alter table DELIVERY_REQUEST add SORT_NAME varchar2(10);
alter table DELIVERY_REQUEST add DIVIDER_LABEL_FLAG number(1) default 0;

update DELIVERY_REQUEST set DIVIDER_LABEL_FLAG = 0;

alter table DELIVERY_REQUEST modify DIVIDER_LABEL_FLAG not null;

comment on column DELIVERY_REQUEST.SORT_NAME is 'Label sorting mode from rule';


comment on column DELIVERY_REQUEST.DIVIDER_LABEL_FLAG is 'Print Divider Label Flag';



--//@UNDO

alter table DELIVERY_REQUEST drop column SORT_NAME;
alter table DELIVERY_REQUEST drop column DIVIDER_LABEL_FLAG;
--//