update SYSTEM_MESSAGE set MAIN_MSG = 'EDS waiting time audit report will only present weekdays records.' where MESSAGE_CODE = '0772';

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Cannot mark uncollected - Prescription is marked as suspend' where MESSAGE_CODE = '0772';
--//