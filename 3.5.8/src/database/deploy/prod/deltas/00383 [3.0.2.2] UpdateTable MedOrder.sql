alter table MED_ORDER add REF_PHARM_ORDER_ID number(19) null;

comment on column MED_ORDER.REF_PHARM_ORDER_ID is 'Pharmacy order reference id';

--//@UNDO
alter table MED_ORDER drop column REF_PHARM_ORDER_ID;
--//
