alter table DELIVERY add MODIFY_DATE timestamp(6) null;

comment on column DELIVERY.MODIFY_DATE is 'For Transaction Report to show reprint with update';

--//@UNDO

alter table DELIVERY drop column MODIFY_DATE;
--//