alter table DISP_ORDER add PATIENT_ID number(19);
alter table DISP_ORDER add constraint FK_DISP_ORDER_04 foreign key (PATIENT_ID) references PATIENT (ID);

create index FK_DISP_ORDER_04 on DISP_ORDER (PATIENT_ID) local;

comment on column DISP_ORDER.PATIENT_ID is 'Patient id';


--//@UNDO
drop index FK_DISP_ORDER_04;

alter table DISP_ORDER drop constraint FK_DISP_ORDER_04;
alter table DISP_ORDER drop column PATIENT_ID;
--//