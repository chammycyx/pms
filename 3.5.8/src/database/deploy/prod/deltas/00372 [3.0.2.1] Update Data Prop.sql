update PROP set MAINT_SCREEN='S' where ID=20006;

update PROP set NAME='inbox.alertUrgentOrder.interval', DESCRIPTION='Interval (in minutes) to alert urgent order in Pharmacy Inbox', MAINT_SCREEN='I' where ID=36001;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (36003,'inbox.alertPendingSuspendOrder.interval','Interval (in minutes) to alert pending/suspend order in Pharmacy Inbox','0-720','mm',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'I',null,0);
insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,36003,'10',36003,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE);

--//@UNDO
update PROP set MAINT_SCREEN='O' where ID=20006;

update PROP set NAME='inbox.medProfile.alertAdminTime.interval', DESCRIPTION='Interval (in minutes) to alert drug administration time in Pharmacy Inbox', MAINT_SCREEN='S' where ID=36001;

delete from WORKSTORE_PROP where PROP_ID=36003;
delete from PROP where ID=36003;
--//