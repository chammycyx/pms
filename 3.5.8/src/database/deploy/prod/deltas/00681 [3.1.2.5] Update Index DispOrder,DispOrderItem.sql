create index I_DISP_ORDER_11 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, DAY_END_STATUS) local online;

drop index I_DISP_ORDER_ITEM_01;

--//@UNDO
create index I_DISP_ORDER_ITEM_01 on DISP_ORDER_ITEM (WORKSTATION_ID, STATUS) local online;

drop index I_DISP_ORDER_11;
--//