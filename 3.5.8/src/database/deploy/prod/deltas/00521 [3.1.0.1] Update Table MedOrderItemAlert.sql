alter table MED_ORDER_ITEM_ALERT add ADDITIVE_NUM number(10);

comment on column MED_ORDER_ITEM_ALERT.ADDITIVE_NUM is 'Additive number';

--//@UNDO
alter table MED_ORDER_ITEM_ALERT drop column ADDITIVE_NUM;
--//