create table MED_PROFILE_PO_ITEM_STAT (
    ID number(19) not null,    
    HOSP_CODE varchar2(3) not null,
    MED_PROFILE_PO_ITEM_ID number(19) not null,
    MED_PROFILE_ID number(19) not null,
    DUE_DATE timestamp not null,
    LAST_DUE_DATE timestamp null,
    WARD_CODE varchar2(4) not null,
    URGENT_FLAG number(1) default 0 not null,
    STAT_FLAG number(1) default 0 not null,
    RETURN_DATE timestamp null,
    PAT_HOSP_CODE varchar2(3) not null,
    PAS_WARD_CODE varchar2(4) null,
    constraint PK_MED_PROFILE_PO_ITEM_STAT primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MED_PROFILE_PO_ITEM_STAT is 'Consolidated statistics of Pharmacy order items (of medication profiles) statuses';
comment on column MED_PROFILE_PO_ITEM_STAT.ID is 'Pharmacy order items (of medication profiles) statistic id';
comment on column MED_PROFILE_PO_ITEM_STAT.HOSP_CODE is 'Hospital code';
comment on column MED_PROFILE_PO_ITEM_STAT.MED_PROFILE_PO_ITEM_ID is 'Pharmacy order item id (medication profile)';
comment on column MED_PROFILE_PO_ITEM_STAT.MED_PROFILE_ID is 'Medication profile id';
comment on column MED_PROFILE_PO_ITEM_STAT.DUE_DATE is 'Due date';
comment on column MED_PROFILE_PO_ITEM_STAT.LAST_DUE_DATE is 'Last due date';
comment on column MED_PROFILE_PO_ITEM_STAT.WARD_CODE is 'Ward code';
comment on column MED_PROFILE_PO_ITEM_STAT.URGENT_FLAG is 'Contain urgent item(s)';
comment on column MED_PROFILE_PO_ITEM_STAT.STAT_FLAG is 'STAT then item';
comment on column MED_PROFILE_PO_ITEM_STAT.RETURN_DATE is 'Returned date after home leave';
comment on column MED_PROFILE_PO_ITEM_STAT.PAT_HOSP_CODE is 'Patient hospital code';
comment on column MED_PROFILE_PO_ITEM_STAT.PAS_WARD_CODE is 'PAS ward code';

alter table MED_PROFILE_PO_ITEM_STAT add constraint FK_MED_PROFILE_PO_ITEM_STAT_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);
alter table MED_PROFILE_PO_ITEM_STAT add constraint FK_MED_PROFILE_PO_ITEM_STAT_02 foreign key (MED_PROFILE_PO_ITEM_ID) references MED_PROFILE_PO_ITEM (ID);
alter table MED_PROFILE_PO_ITEM_STAT add constraint FK_MED_PROFILE_PO_ITEM_STAT_03 foreign key (MED_PROFILE_ID) references MED_PROFILE (ID);

create index FK_MED_PROFILE_PO_ITEM_STAT_01 on MED_PROFILE_PO_ITEM_STAT (HOSP_CODE) tablespace PMS_INDX_01;
create unique index FK_MED_PROFILE_PO_ITEM_STAT_02 on MED_PROFILE_PO_ITEM_STAT (MED_PROFILE_PO_ITEM_ID) tablespace PMS_INDX_01;
create index FK_MED_PROFILE_PO_ITEM_STAT_03 on MED_PROFILE_PO_ITEM_STAT (MED_PROFILE_ID) tablespace PMS_INDX_01;

create sequence SQ_MED_PROFILE_PO_ITEM_STAT increment by 50 start with 10000000000049;

--//@UNDO
drop sequence SQ_MED_PROFILE_PO_ITEM_STAT;

drop index FK_MED_PROFILE_PO_ITEM_STAT_01;
drop index FK_MED_PROFILE_PO_ITEM_STAT_02;
drop index FK_MED_PROFILE_PO_ITEM_STAT_03;

alter table MED_PROFILE_PO_ITEM_STAT drop constraint FK_MED_PROFILE_PO_ITEM_STAT_01;
alter table MED_PROFILE_PO_ITEM_STAT drop constraint FK_MED_PROFILE_PO_ITEM_STAT_02;
alter table MED_PROFILE_PO_ITEM_STAT drop constraint FK_MED_PROFILE_PO_ITEM_STAT_03;

drop table MED_PROFILE_PO_ITEM_STAT;
--//
