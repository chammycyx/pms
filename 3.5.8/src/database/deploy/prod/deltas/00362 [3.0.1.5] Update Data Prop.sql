update PROP set CACHE_IN_SESSION_FLAG=1 where ID = 31001;
update PROP set DESCRIPTION='Proccess to apply SFI force proceed for unpaid Rx' where ID=16002;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (35006,'medProfile.alert.hla.enabled','Enable HLA-B*1502 test alert checking for Medication Profile','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,35006,'N',HOSP_CODE,35006,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (35007,'medProfile.normalItem.sendStatusMsg','Process to send dispensing status of normal item of Medication Profile to ordering','vet,verify,check','S',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,35007,'check',HOSP_CODE,35007,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (35008,'medProfile.noLabelItem.sendStatusMsg','Process to send dispensing status of no label item of Medication Profile to ordering','vet,verify,check','S',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,35008,'verify',HOSP_CODE,35008,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL);

--//@UNDO
update PROP set CACHE_IN_SESSION_FLAG=0 where ID = 31001;
update PROP set DESCRIPTION='Apply SFI force proceed of unpaid Rx in the specified Dispensing process' where ID=16002;

delete from HOSPITAL_PROP where PROP_ID in ('35006', '35007', '35008');
delete from PROP where ID in ('35006', '35007', '35008');
--//