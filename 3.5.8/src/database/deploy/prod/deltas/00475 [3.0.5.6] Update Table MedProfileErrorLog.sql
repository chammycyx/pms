alter table MED_PROFILE_ERROR_LOG add HIDE_FOLLOW_UP_FLAG number(1) default 0;

update MED_PROFILE_ERROR_LOG set HIDE_FOLLOW_UP_FLAG = 0;

alter table MED_PROFILE_ERROR_LOG modify HIDE_FOLLOW_UP_FLAG not null;


comment on column MED_PROFILE_ERROR_LOG.HIDE_FOLLOW_UP_FLAG is 'Hide Follow Up Flag';



--//@UNDO

alter table MED_PROFILE_ERROR_LOG drop column HIDE_FOLLOW_UP_FLAG;
--//





