alter table CHARGE_SPECIALTY drop constraint FK_CHARGE_SPECIALTY_01;
alter table CHARGE_SPECIALTY rename column WORKSTORE_GROUP_CODE to HOSP_CODE;
delete from CHARGE_SPECIALTY where HOSP_CODE not in ( select HOSP_CODE from HOSPITAL );
alter table CHARGE_SPECIALTY add constraint FK_CHARGE_SPECIALTY_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

alter table SUSPEND_REASON drop constraint FK_SUSPEND_REASON_01;
alter table SUSPEND_REASON rename column WORKSTORE_GROUP_CODE to HOSP_CODE;
delete from SUSPEND_REASON where HOSP_CODE not in ( select HOSP_CODE from HOSPITAL );
alter table SUSPEND_REASON add constraint FK_SUSPEND_REASON_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

alter table PENDING_REASON drop constraint FK_PENDING_REASON_01;
alter table PENDING_REASON rename column WORKSTORE_GROUP_CODE to HOSP_CODE;
delete from PENDING_REASON where HOSP_CODE not in ( select HOSP_CODE from HOSPITAL );
alter table PENDING_REASON add constraint FK_PENDING_REASON_01 foreign key (HOSP_CODE) references HOSPITAL (HOSP_CODE);

--//@UNDO
alter table CHARGE_SPECIALTY drop constraint FK_CHARGE_SPECIALTY_01;
alter table CHARGE_SPECIALTY rename column HOSP_CODE to WORKSTORE_GROUP_CODE;
delete from CHARGE_SPECIALTY where WORKSTORE_GROUP_CODE not in (select WORKSTORE_GROUP_CODE from WORKSTORE_GROUP);
alter table CHARGE_SPECIALTY add constraint FK_CHARGE_SPECIALTY_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);

alter table SUSPEND_REASON drop constraint FK_SUSPEND_REASON_01;
alter table SUSPEND_REASON rename column HOSP_CODE to WORKSTORE_GROUP_CODE;
delete from SUSPEND_REASON where WORKSTORE_GROUP_CODE not in (select WORKSTORE_GROUP_CODE from WORKSTORE_GROUP);
alter table SUSPEND_REASON add constraint FK_SUSPEND_REASON_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);

alter table PENDING_REASON drop constraint FK_PENDING_REASON_01;
alter table PENDING_REASON rename column HOSP_CODE to WORKSTORE_GROUP_CODE;
delete from PENDING_REASON where WORKSTORE_GROUP_CODE not in (select WORKSTORE_GROUP_CODE from WORKSTORE_GROUP);
alter table PENDING_REASON add constraint FK_PENDING_REASON_01 foreign key (WORKSTORE_GROUP_CODE) references WORKSTORE_GROUP (WORKSTORE_GROUP_CODE);
--//
