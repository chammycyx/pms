alter table MED_PROFILE add TRANSFER_FLAG number(1) default 0;
alter table MED_PROFILE add TRANSFER_DATE timestamp;
alter table MED_PROFILE add ITEM_CUT_OFF_DATE timestamp;

alter table MED_PROFILE_MO_ITEM add TRANSFER_FLAG number(1) default 0;
alter table MED_PROFILE_MO_ITEM add TRANSFER_DATE timestamp;


update MED_PROFILE set TRANSFER_FLAG = 0 where TRANSFER_FLAG is null;
update MED_PROFILE_MO_ITEM set TRANSFER_FLAG = 0 where TRANSFER_FLAG is null;


comment on column MED_PROFILE.TRANSFER_FLAG is 'Transfer flag';
comment on column MED_PROFILE.TRANSFER_DATE is 'Transfer date';
comment on column MED_PROFILE.ITEM_CUT_OFF_DATE is 'Item cut off date';

comment on column MED_PROFILE_MO_ITEM.TRANSFER_FLAG is 'Transfer flag';
comment on column MED_PROFILE_MO_ITEM.TRANSFER_DATE is 'Transfer date';


--//@UNDO
alter table MED_PROFILE drop column TRANSFER_FLAG;
alter table MED_PROFILE drop column TRANSFER_DATE;
alter table MED_PROFILE drop column ITEM_CUT_OFF_DATE;

alter table MED_PROFILE_MO_ITEM drop column TRANSFER_FLAG;
alter table MED_PROFILE_MO_ITEM drop column TRANSFER_DATE;
--//


