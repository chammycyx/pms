alter table MED_PROFILE_PO_ITEM add FIRST_DUE_DATE timestamp;

comment on column MED_PROFILE_PO_ITEM.FIRST_DUE_DATE is 'First due date';

--//@UNDO
alter table MED_PROFILE_PO_ITEM drop column FIRST_DUE_DATE;
--//