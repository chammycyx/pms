create table BATCH_ISSUE_LOG (
    ID number(19) not null,
    ISSUE_DATE date not null,
    ISSUE_COUNT number(10) not null,
    ERROR_COUNT number(10) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    UPDATE_WORKSTATION_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_BATCH_ISSUE_LOG primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_BATCH_ISSUE_LOG_01 unique (HOSP_CODE, WORKSTORE_CODE, ISSUE_DATE) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  BATCH_ISSUE_LOG is 'Batch issued prescription logs';
comment on column BATCH_ISSUE_LOG.ID is 'Batch issue log id';
comment on column BATCH_ISSUE_LOG.ISSUE_DATE is 'Issued date';
comment on column BATCH_ISSUE_LOG.ISSUE_COUNT is 'Count of issued items';
comment on column BATCH_ISSUE_LOG.ERROR_COUNT is 'Count of items with error';
comment on column BATCH_ISSUE_LOG.HOSP_CODE is 'Hospital code';
comment on column BATCH_ISSUE_LOG.WORKSTORE_CODE is 'Workstore code';
comment on column BATCH_ISSUE_LOG.UPDATE_WORKSTATION_ID is 'Last updated workstation id';
comment on column BATCH_ISSUE_LOG.CREATE_USER is 'Created user';
comment on column BATCH_ISSUE_LOG.CREATE_DATE is 'Created date';
comment on column BATCH_ISSUE_LOG.UPDATE_USER is 'Last updated user';
comment on column BATCH_ISSUE_LOG.UPDATE_DATE is 'Last updated date';
comment on column BATCH_ISSUE_LOG.VERSION is 'Version to serve as optimistic lock value';


create table BATCH_ISSUE_LOG_DETAIL (
    ID number(19) not null,
    DISP_ORDER_ID number(19) not null,
    STATUS varchar2(2) not null,
    ISSUE_FLAG number(1) default 0 not null,
    RESULT_MSG varchar2(1000) null,
    BATCH_ISSUE_LOG_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_BATCH_ISSUE_LOG_DETAIL primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  BATCH_ISSUE_LOG_DETAIL is 'Details of batch issue logs';
comment on column BATCH_ISSUE_LOG_DETAIL.ID is 'Batch issue log detail id';
comment on column BATCH_ISSUE_LOG_DETAIL.DISP_ORDER_ID is 'Dispensing Order id';
comment on column BATCH_ISSUE_LOG_DETAIL.STATUS is 'Last dispensing order status';
comment on column BATCH_ISSUE_LOG_DETAIL.ISSUE_FLAG is 'Issued';
comment on column BATCH_ISSUE_LOG_DETAIL.RESULT_MSG is 'Result message';
comment on column BATCH_ISSUE_LOG_DETAIL.BATCH_ISSUE_LOG_ID is 'Batch issue log id';
comment on column BATCH_ISSUE_LOG_DETAIL.CREATE_USER is 'Created user';
comment on column BATCH_ISSUE_LOG_DETAIL.CREATE_DATE is 'Created date';
comment on column BATCH_ISSUE_LOG_DETAIL.UPDATE_USER is 'Last updated user';
comment on column BATCH_ISSUE_LOG_DETAIL.UPDATE_DATE is 'Last updated date';
comment on column BATCH_ISSUE_LOG_DETAIL.VERSION is 'Version to serve as optimistic lock value';


create table CAPD_VOUCHER (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    VOUCHER_NUM varchar2(11) not null,
    MANUAL_VOUCHER_NUM varchar2(11) null,
    SUPPLIER_CODE varchar2(4) not null,
    DISP_ORDER_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CAPD_VOUCHER primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  CAPD_VOUCHER is 'CAPD vouchers';
comment on column CAPD_VOUCHER.ID is 'Voucher id';
comment on column CAPD_VOUCHER.STATUS is 'Record status';
comment on column CAPD_VOUCHER.VOUCHER_NUM is 'Generated voucher number';
comment on column CAPD_VOUCHER.MANUAL_VOUCHER_NUM is 'Manual voucher number';
comment on column CAPD_VOUCHER.SUPPLIER_CODE is 'Supplier code';
comment on column CAPD_VOUCHER.DISP_ORDER_ID is 'Dispensing Order id';
comment on column CAPD_VOUCHER.CREATE_USER is 'Created user';
comment on column CAPD_VOUCHER.CREATE_DATE is 'Created date';
comment on column CAPD_VOUCHER.UPDATE_USER is 'Last updated user';
comment on column CAPD_VOUCHER.UPDATE_DATE is 'Last updated date';
comment on column CAPD_VOUCHER.VERSION is 'Version to serve as optimistic lock value';


create table CAPD_VOUCHER_ITEM (
    ID number(19) not null,
    QTY number(6) not null,
    CAPD_VOUCHER_ID number(19) not null,
    DISP_ORDER_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_CAPD_VOUCHER_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  CAPD_VOUCHER_ITEM is 'CAPD voucher items';
comment on column CAPD_VOUCHER_ITEM.ID is 'CAPD voucher item id';
comment on column CAPD_VOUCHER_ITEM.QTY is 'Quantity';
comment on column CAPD_VOUCHER_ITEM.CAPD_VOUCHER_ID is 'CAPD voucher id';
comment on column CAPD_VOUCHER_ITEM.DISP_ORDER_ITEM_ID is 'Dispensing order item id';
comment on column CAPD_VOUCHER_ITEM.CREATE_USER is 'Created user';
comment on column CAPD_VOUCHER_ITEM.CREATE_DATE is 'Created date';
comment on column CAPD_VOUCHER_ITEM.UPDATE_USER is 'Last updated user';
comment on column CAPD_VOUCHER_ITEM.UPDATE_DATE is 'Last updated date';
comment on column CAPD_VOUCHER_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table DISP_ORDER (
    ID number(19) not null,
    STATUS varchar2(2) not null,
    DISP_DATE date not null,
    ADMIN_STATUS varchar2(1) not null,
    CHECK_USER varchar2(12) null,
    CHECK_DATE timestamp null,
    ISSUE_USER varchar2(12) null,
    ISSUE_DATE timestamp null,
    PICK_DATE timestamp null,
    ASSEMBLE_DATE timestamp null,
    UNCOLLECT_USER varchar2(12) null,
    UNCOLLECT_DATE timestamp null,
    URGENT_FLAG number(1) default 0 not null,
    PRINT_LANG varchar2(10) not null,
    CHECK_WORKSTATION_CODE varchar2(4) null,
    ISSUE_WINDOW_NUM number(2) null,
    SFI_FLAG number(1) default 0 not null,
    REFILL_FLAG number(1) default 0 not null,
    CHEST_LABEL_XML clob null,
    SUSPEND_CODE varchar2(2) null,
    WARD_CODE varchar2(4) null,
    SPEC_CODE varchar2(4) not null,
    DAY_END_STATUS varchar2(1) not null,
    DAY_END_BATCH_DATE date null,
    PAT_CAT_CODE varchar2(2) null,
    ORG_DISP_ORDER_ID number(19) null,
    PREV_DISP_ORDER_ID number(19) null,
    DISP_ORDER_NUM number(19) null,
    ORG_DISP_ORDER_NUM number(19) null,
    PREV_DISP_ORDER_NUM number(19) null,
    ALERT_CHECK_REMARK clob null,
    ALERT_CHECK_FLAG number(1) default 0 not null,
    UNCOLLECT_FLAG number(1) default 0 not null,
    BATCH_PROCESSING_FLAG varchar2(1) default 0 not null,
    BATCH_PROCESSING_TYPE varchar2(1) not null,
    BATCH_REMARK varchar2(500) null,
    EXTERNAL_ADMIN_STATUS varchar2(1) not null,
    PHARM_ORDER_ID number(19) not null,
    TICKET_ID number(19) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DISP_ORDER primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  DISP_ORDER is 'Dispensing orders';
comment on column DISP_ORDER.ID is 'Dispensing order id';
comment on column DISP_ORDER.STATUS is 'Record status';
comment on column DISP_ORDER.DISP_DATE is 'Dispensing date';
comment on column DISP_ORDER.ADMIN_STATUS is 'Pharmacy admin status';
comment on column DISP_ORDER.CHECK_USER is 'Checked user';
comment on column DISP_ORDER.CHECK_DATE is 'Checked date';
comment on column DISP_ORDER.ISSUE_USER is 'Issued user';
comment on column DISP_ORDER.ISSUE_DATE is 'Issued date';
comment on column DISP_ORDER.PICK_DATE is 'Picked date';
comment on column DISP_ORDER.ASSEMBLE_DATE is 'Assembled date';
comment on column DISP_ORDER.UNCOLLECT_USER is 'Uncollected user';
comment on column DISP_ORDER.UNCOLLECT_DATE is 'Uncollected date';
comment on column DISP_ORDER.URGENT_FLAG is 'Urgent order';
comment on column DISP_ORDER.PRINT_LANG is 'Printed language';
comment on column DISP_ORDER.CHECK_WORKSTATION_CODE is 'Checked workstation code';
comment on column DISP_ORDER.ISSUE_WINDOW_NUM is 'Issued window number';
comment on column DISP_ORDER.SFI_FLAG is 'Contain SFI item';
comment on column DISP_ORDER.REFILL_FLAG is 'Contain refill item';
comment on column DISP_ORDER.CHEST_LABEL_XML is 'Chest label content in XML format';
comment on column DISP_ORDER.SUSPEND_CODE is 'Suspend code';
comment on column DISP_ORDER.WARD_CODE is 'Ward code';
comment on column DISP_ORDER.SPEC_CODE is 'Specialty code';
comment on column DISP_ORDER.DAY_END_STATUS is 'Day end status';
comment on column DISP_ORDER.DAY_END_BATCH_DATE is 'Batch date of day end';
comment on column DISP_ORDER.PAT_CAT_CODE is 'Patient category code';
comment on column DISP_ORDER.ORG_DISP_ORDER_ID is 'Orignial dispensing order id';
comment on column DISP_ORDER.PREV_DISP_ORDER_ID is 'Previous dispensing order id';
comment on column DISP_ORDER.DISP_ORDER_NUM is 'Dispensing order number for systems integration';
comment on column DISP_ORDER.ORG_DISP_ORDER_NUM is 'Original dispensing order number for systems integration';
comment on column DISP_ORDER.PREV_DISP_ORDER_NUM is 'Previous dispensing order number for systems integration';
comment on column DISP_ORDER.ALERT_CHECK_REMARK is 'Alert remark from CMS';
comment on column DISP_ORDER.ALERT_CHECK_FLAG is 'Checked alert';
comment on column DISP_ORDER.UNCOLLECT_FLAG is 'Uncollected';
comment on column DISP_ORDER.BATCH_PROCESSING_FLAG is 'Batch processing';
comment on column DISP_ORDER.BATCH_PROCESSING_TYPE is 'Batch processing type';
comment on column DISP_ORDER.BATCH_REMARK is 'Batch remark';    
comment on column DISP_ORDER.EXTERNAL_ADMIN_STATUS is 'Current pharmacy admin status in external systems which obtain the latest status via batch job';
comment on column DISP_ORDER.PHARM_ORDER_ID is 'Pharmacy order id';
comment on column DISP_ORDER.TICKET_ID is 'Ticket id';
comment on column DISP_ORDER.HOSP_CODE is 'Hospital code';
comment on column DISP_ORDER.WORKSTORE_CODE is 'Workstore code';
comment on column DISP_ORDER.CREATE_USER is 'Created user';
comment on column DISP_ORDER.CREATE_DATE is 'Created date';
comment on column DISP_ORDER.UPDATE_USER is 'Last updated user';
comment on column DISP_ORDER.UPDATE_DATE is 'Last updated date';
comment on column DISP_ORDER.VERSION is 'Version to serve as optimistic lock value';


create table DISP_ORDER_ITEM (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    DISP_QTY number(19,4) not null,
    START_DATE date null,
    END_DATE date null,
    DURATION_IN_DAY number(4,0) null,
    WARN_CODE_1 varchar2(2) null,
    WARN_CODE_2 varchar2(2) null,
    WARN_CODE_3 varchar2(2) null,
    WARN_CODE_4 varchar2(2) null,
    WARN_DESC_1 varchar2(40) null,
    WARN_DESC_2 varchar2(40) null,
    WARN_DESC_3 varchar2(40) null,
    WARN_DESC_4 varchar2(40) null,
    LIFESTYLE_FLAG number(1) default 0 not null,
    ONCOLOGY_FLAG number(1) default 0 not null,
    LABEL_XML clob null,
    REMARK_TEXT varchar2(50) null,
    REMARK_TYPE varchar2(1) not null,
    REMARK_UPDATE_USER varchar2(12) null,
    REMARK_UPDATE_DATE timestamp null,
    BIN_NUM varchar2(4) null,
    WORKSTATION_ID number(19) null,
    WORKSTATION_CODE varchar2(4) null,
    PICK_DATE timestamp null,
    PICK_USER varchar2(12) null,
    ASSEMBLE_USER varchar2(12) null,
    ASSEMBLE_DATE timestamp null,
    UNIT_PRICE number(19,4) null,
    PRINT_FLAG number(1) default 0 not null,
    RE_DISP_FLAG number(1) default 0 not null,
    CHARGE_SPEC_CODE varchar2(4) not null,
    ITEM_NUM number(10) not null,
    NUM_OF_LABEL number(1) not null,
    DISP_ORDER_ID number(19) not null,
    PHARM_ORDER_ITEM_ID number(19) not null,
    DELIVERY_ITEM_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DISP_ORDER_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  DISP_ORDER_ITEM is 'Dispensing order items';
comment on column DISP_ORDER_ITEM.ID is 'Dispensing order item id';
comment on column DISP_ORDER_ITEM.STATUS is 'Record status';
comment on column DISP_ORDER_ITEM.DISP_QTY is 'Dispensed quantity';
comment on column DISP_ORDER_ITEM.START_DATE is 'Start date of administration';
comment on column DISP_ORDER_ITEM.END_DATE is 'End date of administration';
comment on column DISP_ORDER_ITEM.DURATION_IN_DAY is 'Duration in day(s)';
comment on column DISP_ORDER_ITEM.WARN_CODE_1 is 'Warning code 1';
comment on column DISP_ORDER_ITEM.WARN_CODE_2 is 'Warning code 2';
comment on column DISP_ORDER_ITEM.WARN_CODE_3 is 'Warning code 3';
comment on column DISP_ORDER_ITEM.WARN_CODE_4 is 'Warning code 4';
comment on column DISP_ORDER_ITEM.WARN_DESC_1 is 'Warning description 1';
comment on column DISP_ORDER_ITEM.WARN_DESC_2 is 'Warning description 2';
comment on column DISP_ORDER_ITEM.WARN_DESC_3 is 'Warning description 3';
comment on column DISP_ORDER_ITEM.WARN_DESC_4 is 'Warning description 4';
comment on column DISP_ORDER_ITEM.LIFESTYLE_FLAG is 'Lifestyle item';
comment on column DISP_ORDER_ITEM.ONCOLOGY_FLAG is 'Oncology item';
comment on column DISP_ORDER_ITEM.LABEL_XML is 'Label content in XML format';
comment on column DISP_ORDER_ITEM.REMARK_TEXT is 'Pharmacy remark';
comment on column DISP_ORDER_ITEM.REMARK_TYPE is 'Pharmacy remark type';
comment on column DISP_ORDER_ITEM.REMARK_UPDATE_USER is 'Created / Updated user of pharmacy remark';
comment on column DISP_ORDER_ITEM.REMARK_UPDATE_DATE is 'Updated date of pharmacy remark';
comment on column DISP_ORDER_ITEM.BIN_NUM is 'Bin number';
comment on column DISP_ORDER_ITEM.WORKSTATION_ID is 'Workstation id';
comment on column DISP_ORDER_ITEM.WORKSTATION_CODE is 'Workstation code defined by users';
comment on column DISP_ORDER_ITEM.PICK_DATE is 'Picked date';
comment on column DISP_ORDER_ITEM.PICK_USER is 'Picked user';
comment on column DISP_ORDER_ITEM.ASSEMBLE_USER is 'Assembled user';
comment on column DISP_ORDER_ITEM.ASSEMBLE_DATE is 'Assembled date';
comment on column DISP_ORDER_ITEM.UNIT_PRICE is 'Drug unit price';
comment on column DISP_ORDER_ITEM.PRINT_FLAG is 'Printed';
comment on column DISP_ORDER_ITEM.RE_DISP_FLAG is 'Re-dispensed';
comment on column DISP_ORDER_ITEM.CHARGE_SPEC_CODE is 'Charging specialty code';
comment on column DISP_ORDER_ITEM.ITEM_NUM is 'Item number';
comment on column DISP_ORDER_ITEM.NUM_OF_LABEL is 'Number of label(s)';
comment on column DISP_ORDER_ITEM.DISP_ORDER_ID is 'Dispensing Order id';
comment on column DISP_ORDER_ITEM.PHARM_ORDER_ITEM_ID is 'Pharmacy order item id';
comment on column DISP_ORDER_ITEM.DELIVERY_ITEM_ID is 'Delivery item id';
comment on column DISP_ORDER_ITEM.CREATE_USER is 'Created user';
comment on column DISP_ORDER_ITEM.CREATE_DATE is 'Created date';
comment on column DISP_ORDER_ITEM.UPDATE_USER is 'Last updated user';
comment on column DISP_ORDER_ITEM.UPDATE_DATE is 'Last updated date';
comment on column DISP_ORDER_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table INVOICE (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    INVOICE_NUM varchar2(15) not null,
    MANUAL_RECEIPT_NUM varchar2(20) null,
    DOC_TYPE varchar2(1) not null,
    FORCE_PROCEED_FLAG number(1) default 0 not null,
    FORCE_PROCEED_DATE date null,
    ORG_INVOICE_ID number(19) null,
    RECEIPT_NUM varchar2(20) null,
    FCS_CHECK_DATE timestamp null,
    FORCE_PROCEED_USER varchar2(12) null,
    FORCE_PROCEED_CODE varchar2(12) null,
    TOTAL_AMOUNT number(19) not null,
    MANUAL_INVOICE_NUM varchar2(15) null,
    PREV_INVOICE_ID number(19) null,
    RECEIVE_AMOUNT number(19) null,
    PRINT_FLAG number(1) default 0 not null,
    DISP_ORDER_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_INVOICE primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  INVOICE is 'Invoices';
comment on column INVOICE.ID is 'Invoice id';
comment on column INVOICE.STATUS is 'Record status';
comment on column INVOICE.INVOICE_NUM is 'Generated invoice number';
comment on column INVOICE.MANUAL_RECEIPT_NUM is 'Manual receipt number';
comment on column INVOICE.DOC_TYPE is 'Invoice type';
comment on column INVOICE.FORCE_PROCEED_FLAG is 'Force proceeded';
comment on column INVOICE.FORCE_PROCEED_DATE is 'Force proceeded date';
comment on column INVOICE.ORG_INVOICE_ID is 'Original invoice id';
comment on column INVOICE.RECEIPT_NUM is 'Receipt number';
comment on column INVOICE.FCS_CHECK_DATE is 'Checked FCS date';
comment on column INVOICE.FORCE_PROCEED_USER is 'Force proceeded user';
comment on column INVOICE.FORCE_PROCEED_CODE is 'Force proceeded code';
comment on column INVOICE.TOTAL_AMOUNT is 'Total amount';
comment on column INVOICE.MANUAL_INVOICE_NUM is 'Manual invoice number';
comment on column INVOICE.PREV_INVOICE_ID is 'Previous invoice id';
comment on column INVOICE.RECEIVE_AMOUNT is 'Received amount';
comment on column INVOICE.PRINT_FLAG is 'Printed';
comment on column INVOICE.DISP_ORDER_ID is 'Dispensing Order id';
comment on column INVOICE.CREATE_USER is 'Created user';
comment on column INVOICE.CREATE_DATE is 'Created date';
comment on column INVOICE.UPDATE_USER is 'Last updated user';
comment on column INVOICE.UPDATE_DATE is 'Last updated date';
comment on column INVOICE.VERSION is 'Version to serve as optimistic lock value';


create table INVOICE_ITEM (
    ID number(19) not null,
    COST number(19,4) not null,
    MARKUP_AMOUNT number(19) not null,
    HANDLE_AMOUNT number(19) not null,
    HANDLE_EXEMPT_AMOUNT number(19) not null,
    HANDLE_EXEMPT_CODE varchar2(2) null,
    NEXT_HANDLE_EXEMPT_CODE varchar2(2) null,
    HANDLE_EXEMPT_USER varchar2(12) null,
    HANDLE_EXEMPT_DATE date null,
    AMOUNT number(19) not null,
    ACTUAL_MARKUP_AMOUNT number(19,4) not null,
    CAP_MARKUP_AMOUNT number(19,4) not null,
    INVOICE_ID number(19) not null,
    DISP_ORDER_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_INVOICE_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  INVOICE_ITEM is 'Invoice items';
comment on column INVOICE_ITEM.ID is 'Invoice item id';
comment on column INVOICE_ITEM.COST is 'Cost';
comment on column INVOICE_ITEM.MARKUP_AMOUNT is 'Markup amount';
comment on column INVOICE_ITEM.HANDLE_AMOUNT is 'Amount of handling charge';
comment on column INVOICE_ITEM.HANDLE_EXEMPT_AMOUNT is 'Amount of handling charge exempted';
comment on column INVOICE_ITEM.HANDLE_EXEMPT_CODE is 'Exemption code (Charging reason code)';
comment on column INVOICE_ITEM.NEXT_HANDLE_EXEMPT_CODE is 'Exemption code for next SFI refill (Charging reason code)';
comment on column INVOICE_ITEM.HANDLE_EXEMPT_USER is 'User to exempt handling charge';
comment on column INVOICE_ITEM.HANDLE_EXEMPT_DATE is 'Date of handling charge exempted';
comment on column INVOICE_ITEM.AMOUNT is 'Invoice amount';
comment on column INVOICE_ITEM.ACTUAL_MARKUP_AMOUNT is 'Actual markup amount';
comment on column INVOICE_ITEM.CAP_MARKUP_AMOUNT is 'Capped markup amount';
comment on column INVOICE_ITEM.INVOICE_ID is 'Invoice id';
comment on column INVOICE_ITEM.DISP_ORDER_ITEM_ID is 'Dispensing order item id';
comment on column INVOICE_ITEM.CREATE_USER is 'Created user';
comment on column INVOICE_ITEM.CREATE_DATE is 'Created date';
comment on column INVOICE_ITEM.UPDATE_USER is 'Last updated user';
comment on column INVOICE_ITEM.UPDATE_DATE is 'Last updated date';
comment on column INVOICE_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table MED_CASE (
    ID number(19) not null,
    CASE_NUM varchar2(12) null,
    PAS_APPT_SEQ number(19) null,
    PAS_PAT_GROUP_CODE varchar2(5) null,
    PAS_APPT_CASE_NUM varchar2(12) null,
    PAS_SPEC_CODE varchar2(4) null,
    PAS_WARD_CODE varchar2(4) null,
    PAS_SUB_SPEC_CODE varchar2(4) null,
    PAS_PAY_FLAG_CODE varchar2(1) not null,
    PAS_APPT_DATE timestamp null,
    PAS_ATTEND_FLAG number(1) default 0 not null,
    PAS_PAY_CODE varchar2(3) null,
    PAS_BED_NUM varchar2(5) null,
    CHARGE_FLAG number(1) default 0 not null,
    PAS_PAT_IND varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_MED_CASE primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  MED_CASE is 'Medication cases';
comment on column MED_CASE.ID is 'Medication case id';
comment on column MED_CASE.CASE_NUM is 'Case number';
comment on column MED_CASE.PAS_APPT_SEQ is 'PAS appointment sequence number';
comment on column MED_CASE.PAS_PAT_GROUP_CODE is 'PAS patient group code';
comment on column MED_CASE.PAS_APPT_CASE_NUM is 'PAS appointment case number';
comment on column MED_CASE.PAS_SPEC_CODE is 'PAS specialty code';
comment on column MED_CASE.PAS_WARD_CODE is 'PAS ward code';
comment on column MED_CASE.PAS_SUB_SPEC_CODE is 'PAS sub-specialty code';
comment on column MED_CASE.PAS_PAY_FLAG_CODE is 'PAS pay flag code';
comment on column MED_CASE.PAS_APPT_DATE is 'PAS appointment date';
comment on column MED_CASE.PAS_ATTEND_FLAG is 'Attended';
comment on column MED_CASE.PAS_PAY_CODE is 'PAS pay code';
comment on column MED_CASE.PAS_BED_NUM is 'PAS bed number';
comment on column MED_CASE.CHARGE_FLAG is 'Charging required';
comment on column MED_CASE.PAS_PAT_IND is 'PAS patient indicator';
comment on column MED_CASE.CREATE_USER is 'Created user';
comment on column MED_CASE.CREATE_DATE is 'Created date';
comment on column MED_CASE.UPDATE_USER is 'Last updated user';
comment on column MED_CASE.UPDATE_DATE is 'Last updated date';


create table MED_ORDER (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    ORDER_NUM varchar2(12) not null,
    ORDER_DATE timestamp not null,
    REF_NUM varchar2(5) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    PRESC_TYPE varchar2(1) not null,
    ORDER_TYPE varchar2(1) not null,
    DOC_TYPE varchar2(1) not null,
    PREV_ORDER_NUM varchar2(12) null,
    PREGNANCY_CHECK_FLAG number(1) default 0 not null,
    MAX_ITEM_NUM number(10) not null,
    PROCESSING_FLAG number(1) default 0 not null,
    CMS_ORDER_TYPE varchar2(1) not null,
    CMS_ORDER_SUB_TYPE varchar2(1) not null,
    DOCTOR_CODE varchar2(12) null,
    DOCTOR_NAME varchar2(48) null,
    DOCTOR_RANK_CODE varchar2(10) null,
    SPEC_CODE varchar2(4) not null,
    WARD_CODE varchar2(4) null,
    WORKSTATION_ID number(19) null,
    REMARK_STATUS varchar2(1) not null,
    EIS_SPEC_CODE varchar2(4) null,
    CMS_UPDATE_DATE timestamp not null,
    ORG_PATIENT_ID number(19) null,
    PREV_PATIENT_ID number(19) null,
    VET_BEFORE_FLAG number(1) default 0 not null,
    VET_DATE timestamp null,
    REMARK_TEXT varchar2(100) null,
    REMARK_CREATE_USER varchar2(12) null,
    REMARK_CREATE_DATE timestamp null,
    REMARK_CONFIRM_USER varchar2(12) null,
    REMARK_CONFIRM_DATE timestamp null,
    UNVET_FLAG number(1) default 0 not null,
    COMMENT_FLAG number(1) default 0 not null,
    HLA_FLAG number(1) default 0 not null,    
    ITEM_REMARK_CONFIRMED_FLAG varchar2(1) default 0 not null,
    PATIENT_ID number(19) null,
    MED_CASE_ID number(19) null,
    TICKET_ID number(19) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_ORDER primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  MED_ORDER is 'Medication orders of prescriptions';
comment on column MED_ORDER.ID is 'Medication order id';
comment on column MED_ORDER.STATUS is 'Record status';
comment on column MED_ORDER.ORDER_NUM is 'Medication order number';
comment on column MED_ORDER.ORDER_DATE is 'Order date';
comment on column MED_ORDER.REF_NUM is 'Reference number';
comment on column MED_ORDER.PAT_HOSP_CODE is 'Patient hospital code';
comment on column MED_ORDER.PRESC_TYPE is 'Prescription type';
comment on column MED_ORDER.ORDER_TYPE is 'Order type';
comment on column MED_ORDER.DOC_TYPE is 'Document type';
comment on column MED_ORDER.PREV_ORDER_NUM is 'Previous order number';
comment on column MED_ORDER.PREGNANCY_CHECK_FLAG is 'Checked pregnancy';
comment on column MED_ORDER.MAX_ITEM_NUM is 'Maximum item number';
comment on column MED_ORDER.PROCESSING_FLAG is 'Processing';
comment on column MED_ORDER.CMS_ORDER_TYPE is 'CMS order type';
comment on column MED_ORDER.CMS_ORDER_SUB_TYPE is 'CMS order sub-type';
comment on column MED_ORDER.DOCTOR_CODE is 'Doctor code';
comment on column MED_ORDER.DOCTOR_NAME is 'Doctor name';
comment on column MED_ORDER.DOCTOR_RANK_CODE is 'Doctor rank code';
comment on column MED_ORDER.SPEC_CODE is 'Specialty code';
comment on column MED_ORDER.WARD_CODE is 'Ward code';
comment on column MED_ORDER.WORKSTATION_ID is 'Workstation id';
comment on column MED_ORDER.REMARK_STATUS is 'Pharmacy remark status';
comment on column MED_ORDER.EIS_SPEC_CODE is 'EIS specialty code';
comment on column MED_ORDER.CMS_UPDATE_DATE is 'CMS last updated date';
comment on column MED_ORDER.ORG_PATIENT_ID is 'Original patient id';
comment on column MED_ORDER.PREV_PATIENT_ID is 'Previous patient id';
comment on column MED_ORDER.VET_BEFORE_FLAG is 'Vetted before';
comment on column MED_ORDER.VET_DATE is 'Vetted date';    
comment on column MED_ORDER.REMARK_TEXT is 'Pharmacy remark';
comment on column MED_ORDER.REMARK_CREATE_USER is 'Created user of pharmacy remark';
comment on column MED_ORDER.REMARK_CREATE_DATE is 'Created date of pharmacy remark';
comment on column MED_ORDER.REMARK_CONFIRM_USER is 'Confirmed user of pharmacy remark';
comment on column MED_ORDER.REMARK_CONFIRM_DATE is 'Confirmed date of pharmacy remark';
comment on column MED_ORDER.UNVET_FLAG is 'Unvetted';    
comment on column MED_ORDER.COMMENT_FLAG is 'Comment flag';
comment on column MED_ORDER.HLA_FLAG is 'with HLA alert';
comment on column MED_ORDER.ITEM_REMARK_CONFIRMED_FLAG is 'Remark has been confirmed in item level';
comment on column MED_ORDER.PATIENT_ID is 'Patient id';
comment on column MED_ORDER.MED_CASE_ID is 'Medication case id';
comment on column MED_ORDER.TICKET_ID is 'Ticket id';
comment on column MED_ORDER.HOSP_CODE is 'Hospital code';
comment on column MED_ORDER.WORKSTORE_CODE is 'Workstore code';
comment on column MED_ORDER.CREATE_USER is 'Created user';
comment on column MED_ORDER.CREATE_DATE is 'Created date';
comment on column MED_ORDER.UPDATE_USER is 'Last updated user';
comment on column MED_ORDER.UPDATE_DATE is 'Last updated date';
comment on column MED_ORDER.VERSION is 'Version to serve as optimistic lock value';


create table MED_ORDER_FM (
    ID number(19) not null,
    ITEM_CODE varchar2(6) null,
    DISPLAY_NAME varchar2(70) not null,
    FORM_CODE varchar2(3) not null,
    SALT_PROPERTY varchar2(35) null,
    CORP_IND_CODE varchar2(10) null,
    SUPPL_IND_CODE varchar2(10) null,
    OTHER_MESSAGE varchar2(100) null,
    AUTH_DOCTOR_NAME varchar2(70) null,
    FM_CREATE_USER varchar2(12) null,
    FM_CREATE_DATE timestamp null,
    IND_VALID_PERIOD number(10) null,
    PRESC_OPTION varchar2(2) not null,
    FM_STATUS varchar2(1) not null,
    REF_ORDER_NUM varchar2(16) not null,
    REF_ITEM_NUM number(10) not null,
    MED_ORDER_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_ORDER_FM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  MED_ORDER_FM is 'Formulary management based on prescription';
comment on column MED_ORDER_FM.ID is 'Formulary management id of medication order';
comment on column MED_ORDER_FM.ITEM_CODE is 'Item code';
comment on column MED_ORDER_FM.DISPLAY_NAME is 'Drug display name';
comment on column MED_ORDER_FM.FORM_CODE is 'Form code';
comment on column MED_ORDER_FM.SALT_PROPERTY is 'Salt property';
comment on column MED_ORDER_FM.CORP_IND_CODE is 'Corporate indication code';
comment on column MED_ORDER_FM.SUPPL_IND_CODE is 'Supplementary indication code';
comment on column MED_ORDER_FM.OTHER_MESSAGE is 'Other message';
comment on column MED_ORDER_FM.AUTH_DOCTOR_NAME is 'Authorized doctor name';
comment on column MED_ORDER_FM.FM_CREATE_USER is 'Created user of formulary management';
comment on column MED_ORDER_FM.FM_CREATE_DATE is 'Created date of formulary management';
comment on column MED_ORDER_FM.IND_VALID_PERIOD is 'Indication valid period';
comment on column MED_ORDER_FM.PRESC_OPTION is 'Prescription option';
comment on column MED_ORDER_FM.FM_STATUS is 'Formulary management status';
comment on column MED_ORDER_FM.REF_ORDER_NUM is 'Reference order number';
comment on column MED_ORDER_FM.REF_ITEM_NUM is 'Reference item number';
comment on column MED_ORDER_FM.MED_ORDER_ID is 'Medication order id';
comment on column MED_ORDER_FM.CREATE_USER is 'Created user';
comment on column MED_ORDER_FM.CREATE_DATE is 'Created date';
comment on column MED_ORDER_FM.UPDATE_USER is 'Last updated user';
comment on column MED_ORDER_FM.UPDATE_DATE is 'Last updated date';
comment on column MED_ORDER_FM.VERSION is 'Version to serve as optimistic lock value';


create table MED_ORDER_ITEM (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    ITEM_NUM number(10) not null,
    ORG_ITEM_NUM number(10) null,
    PRINT_SEQ number(19) null,
    RX_ITEM_XML clob null,
    RX_ITEM_TYPE varchar2(1) not null,
    PREV_MO_ITEM_ID number(19) null,
    NUM_OF_LABEL number(1) null,
    COMMENT_TEXT varchar2(100) null,
    COMMENT_USER varchar2(12) null,
    COMMENT_DATE timestamp null,
    REMARK_ITEM_STATUS varchar2(2) null,
    PPMI_FLAG number(1) default 0 not null,
    CHARGE_FLAG varchar2(1) not null,
    UNIT_OF_CHARGE number(10) null,
    REMARK_TEXT varchar2(100) null,
    REMARK_CREATE_USER varchar2(12) null,
    REMARK_CREATE_DATE timestamp null,
    REMARK_CONFIRM_USER varchar2(12) null,
    REMARK_CONFIRM_DATE timestamp null,
    CMS_CHARGE_FLAG varchar2(1) not null,
    MED_ORDER_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_ORDER_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  MED_ORDER_ITEM is 'Medication order items';
comment on column MED_ORDER_ITEM.ID is 'Medication order item id';
comment on column MED_ORDER_ITEM.STATUS is 'Record status';
comment on column MED_ORDER_ITEM.ITEM_NUM is 'Item number';
comment on column MED_ORDER_ITEM.ORG_ITEM_NUM is 'Original item number';
comment on column MED_ORDER_ITEM.PRINT_SEQ is 'Printing sequence';
comment on column MED_ORDER_ITEM.RX_ITEM_XML is 'Medication order item in XML format';
comment on column MED_ORDER_ITEM.RX_ITEM_TYPE is 'Medication order item type';
comment on column MED_ORDER_ITEM.PREV_MO_ITEM_ID is 'Previous medication order item id';
comment on column MED_ORDER_ITEM.NUM_OF_LABEL is 'Number of label(s)';
comment on column MED_ORDER_ITEM.COMMENT_TEXT is 'Comment';
comment on column MED_ORDER_ITEM.COMMENT_USER is 'Commented user';
comment on column MED_ORDER_ITEM.COMMENT_DATE is 'Commented date';
comment on column MED_ORDER_ITEM.REMARK_ITEM_STATUS is 'Status of pharmacy remark';
comment on column MED_ORDER_ITEM.PPMI_FLAG is 'PPMI item';
comment on column MED_ORDER_ITEM.CHARGE_FLAG is 'Charging required';
comment on column MED_ORDER_ITEM.UNIT_OF_CHARGE is 'Unit(s) of charge';
comment on column MED_ORDER_ITEM.REMARK_TEXT is 'Pharmacy remark';
comment on column MED_ORDER_ITEM.REMARK_CREATE_USER is 'Created user of pharmacy remark';
comment on column MED_ORDER_ITEM.REMARK_CREATE_DATE is 'Created date of pharmacy remark';
comment on column MED_ORDER_ITEM.REMARK_CONFIRM_USER is 'Confirmed user of pharmacy remark';
comment on column MED_ORDER_ITEM.REMARK_CONFIRM_DATE is 'Confirmed date of pharmacy remark';
comment on column MED_ORDER_ITEM.CMS_CHARGE_FLAG is 'CMS value on Charging required';
comment on column MED_ORDER_ITEM.MED_ORDER_ID is 'Medication order id';
comment on column MED_ORDER_ITEM.CREATE_USER is 'Created user';
comment on column MED_ORDER_ITEM.CREATE_DATE is 'Created date';
comment on column MED_ORDER_ITEM.UPDATE_USER is 'Last updated user';
comment on column MED_ORDER_ITEM.UPDATE_DATE is 'Last updated date';
comment on column MED_ORDER_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table MED_ORDER_ITEM_ALERT (
    ID number(19) not null,
    ALERT_XML clob null,
    OVERRIDE_REASON varchar2(500) null,
    ALERT_TYPE varchar2(8) not null,
    ACK_USER varchar2(12) null,
    ACK_DATE timestamp null,
    SORT_SEQ number(19) not null,
    MED_ORDER_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_ORDER_ITEM_ALERT primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  MED_ORDER_ITEM_ALERT is 'Alerts of medication order items';
comment on column MED_ORDER_ITEM_ALERT.ID is 'Medication order item alert id';
comment on column MED_ORDER_ITEM_ALERT.ALERT_XML is 'Alert content in XML format';
comment on column MED_ORDER_ITEM_ALERT.OVERRIDE_REASON is 'Overriding reason';
comment on column MED_ORDER_ITEM_ALERT.ALERT_TYPE is 'Alert type';
comment on column MED_ORDER_ITEM_ALERT.ACK_USER is 'Acknowledged user';
comment on column MED_ORDER_ITEM_ALERT.ACK_DATE is 'Acknowledged date';
comment on column MED_ORDER_ITEM_ALERT.SORT_SEQ is 'Sort sequence';
comment on column MED_ORDER_ITEM_ALERT.MED_ORDER_ITEM_ID is 'Medication order item id';
comment on column MED_ORDER_ITEM_ALERT.CREATE_USER is 'Created user';
comment on column MED_ORDER_ITEM_ALERT.CREATE_DATE is 'Created date';
comment on column MED_ORDER_ITEM_ALERT.UPDATE_USER is 'Last updated user';
comment on column MED_ORDER_ITEM_ALERT.UPDATE_DATE is 'Last updated date';
comment on column MED_ORDER_ITEM_ALERT.VERSION is 'Version to serve as optimistic lock value';


create table PATIENT (
    ID number(19) not null,
    PAT_KEY varchar2(8) null,
    HKID varchar2(12) null,
    DOB timestamp null,
    SEX varchar2(1) null,
    NAME varchar2(48) null,
    NAME_CHI varchar2(18) null,
    PHONE varchar2(10) null,
    ADDRESS varchar2(435) null,
    DEATH_FLAG number(1) default 0 not null,
    OTHER_DOC varchar2(12) null,
    CC_CODE varchar2(100) null,
    STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_PATIENT primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  PATIENT is 'Patient records';
comment on column PATIENT.ID is 'Patient id';
comment on column PATIENT.PAT_KEY is 'Patient key';
comment on column PATIENT.HKID is 'HKID number';
comment on column PATIENT.DOB is 'Date of birth';
comment on column PATIENT.SEX is 'Sex';
comment on column PATIENT.NAME is 'English name';
comment on column PATIENT.NAME_CHI is 'Chinese name';
comment on column PATIENT.PHONE is 'Phone number';
comment on column PATIENT.ADDRESS is 'Address';
comment on column PATIENT.DEATH_FLAG is 'Died';
comment on column PATIENT.OTHER_DOC is 'Other identity document';
comment on column PATIENT.CC_CODE is 'HKID CC code';
comment on column PATIENT.STATUS is 'Record status';
comment on column PATIENT.CREATE_USER is 'Created user';
comment on column PATIENT.CREATE_DATE is 'Created date';
comment on column PATIENT.UPDATE_USER is 'Last updated user';
comment on column PATIENT.UPDATE_DATE is 'Last updated date';


create table PHARM_ORDER (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    WARD_CODE varchar2(4) null,
    SPEC_CODE varchar2(4) not null,
    PAT_CAT_CODE varchar2(2) null,
    PAT_TYPE varchar2(1) not null,
    ALLOW_COMMENT_TYPE varchar2(1) not null,
    ADMIN_STATUS varchar2(1) not null,
    NUM_OF_GROUP number(10) not null,
    NUM_OF_REFILL number(10) not null,
    MAX_ITEM_NUM number(10) not null,
    CHEST_FLAG number(1) default 0 not null,
    AWAIT_SFI_FLAG number(1) default 0 not null,
    PRIVATE_FLAG number(1) default 0 not null,
    PREGNANCY_CHECK_FLAG number(1) default 0 not null,
    REFILL_INTERVAL number(10) null,
    REFILL_INTERVAL_LAST number(10) null,
    CAPD_SPLIT_XML clob null,
    WORKSTATION_CODE varchar2(4) null,
    DOCTOR_CODE varchar2(12) null,
    DOCTOR_NAME varchar2(48) null,
    DEFAULT_DOCTOR_CODE varchar2(12) null,
    ORDER_DATE timestamp not null,
    TICKET_DATE date not null,
    PAT_KEY varchar2(8) null,
    HKID varchar2(12) null,
    DOB timestamp null,
    SEX varchar2(1) null,
    NAME varchar2(48) null,
    NAME_CHI varchar2(18) null,
    PHONE varchar2(10) null,
    ADDRESS varchar2(435) null,
    DEATH_FLAG number(1) default 0 not null,
    OTHER_DOC varchar2(12) null,
    CC_CODE varchar2(100) null,
    FORCE_PROCEED_FLAG number(1) default 0 not null,
    RECEIPT_NUM varchar2(20) null,
    REFILL_COUNT number(4) not null,    
    MED_CASE_ID number(19) null,
    MED_ORDER_ID number(19) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PHARM_ORDER primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  PHARM_ORDER is 'Pharmacy orders to bridge between medication orders and dispensing orders';
comment on column PHARM_ORDER.ID is 'Pharmacy order id';
comment on column PHARM_ORDER.STATUS is 'Record status';
comment on column PHARM_ORDER.WARD_CODE is 'Ward code';
comment on column PHARM_ORDER.SPEC_CODE is 'Specialty code';
comment on column PHARM_ORDER.PAT_CAT_CODE is 'Patient category code';
comment on column PHARM_ORDER.PAT_TYPE is 'Patient type';
comment on column PHARM_ORDER.ALLOW_COMMENT_TYPE is 'Type of allow post-dispensing comment';
comment on column PHARM_ORDER.ADMIN_STATUS is 'Pharmacy admin status';
comment on column PHARM_ORDER.NUM_OF_GROUP is 'Refill group';
comment on column PHARM_ORDER.NUM_OF_REFILL is 'Number of refills';
comment on column PHARM_ORDER.MAX_ITEM_NUM is 'Maximum item number';
comment on column PHARM_ORDER.CHEST_FLAG is 'Contain chest item(s)';
comment on column PHARM_ORDER.AWAIT_SFI_FLAG is 'Awaiting SFI payment';
comment on column PHARM_ORDER.PRIVATE_FLAG is 'Private patient';
comment on column PHARM_ORDER.PREGNANCY_CHECK_FLAG is 'Checked pregnancy';
comment on column PHARM_ORDER.REFILL_INTERVAL is 'Current refill interval';
comment on column PHARM_ORDER.REFILL_INTERVAL_LAST is 'Last refill interval';
comment on column PHARM_ORDER.CAPD_SPLIT_XML is 'CAPD split information in XML format';
comment on column PHARM_ORDER.WORKSTATION_CODE is 'Workstation code defined by users';
comment on column PHARM_ORDER.DOCTOR_CODE is 'Doctor code';
comment on column PHARM_ORDER.DOCTOR_NAME is 'Doctor name';
comment on column PHARM_ORDER.DEFAULT_DOCTOR_CODE is 'Default doctor code';
comment on column PHARM_ORDER.ORDER_DATE is 'Order date';
comment on column PHARM_ORDER.TICKET_DATE is 'Ticket date';
comment on column PHARM_ORDER.PAT_KEY is 'Patient key';
comment on column PHARM_ORDER.HKID is 'HKID number';
comment on column PHARM_ORDER.DOB is 'Date of birth';
comment on column PHARM_ORDER.SEX is 'Sex';
comment on column PHARM_ORDER.NAME is 'English name';
comment on column PHARM_ORDER.NAME_CHI is 'Chinese name';
comment on column PHARM_ORDER.PHONE is 'Phone number';
comment on column PHARM_ORDER.ADDRESS is 'Address';
comment on column PHARM_ORDER.DEATH_FLAG is 'Died';
comment on column PHARM_ORDER.OTHER_DOC is 'Other identity document';
comment on column PHARM_ORDER.CC_CODE is 'Patient cc code';
comment on column PHARM_ORDER.FORCE_PROCEED_FLAG is 'Force proceeded';
comment on column PHARM_ORDER.RECEIPT_NUM is 'Receipt number';
comment on column PHARM_ORDER.REFILL_COUNT is 'Dispensed RefillSchedule counter';
comment on column PHARM_ORDER.MED_CASE_ID is 'Medication case id';
comment on column PHARM_ORDER.MED_ORDER_ID is 'Medication order id';
comment on column PHARM_ORDER.HOSP_CODE is 'Hospital code';
comment on column PHARM_ORDER.WORKSTORE_CODE is 'Workstore code';    
comment on column PHARM_ORDER.CREATE_USER is 'Created user';
comment on column PHARM_ORDER.CREATE_DATE is 'Created date';
comment on column PHARM_ORDER.UPDATE_USER is 'Last updated user';
comment on column PHARM_ORDER.UPDATE_DATE is 'Last updated date';
comment on column PHARM_ORDER.VERSION is 'Version to serve as optimistic lock value';


create table PHARM_ORDER_ITEM (
    ID number(19) not null,
    STATUS varchar2(1) not null,
    ITEM_CODE varchar2(6) not null,
    REGIMEN_XML clob null,
    DOSE_QTY number(19,4) null,
    DOSE_UNIT varchar2(15) null,
    CAL_QTY number(19,4) not null,
    ADJ_QTY number(19,4) not null,
    ISSUE_QTY number(19,4) not null,
    BASE_UNIT varchar2(4) null,
    WARN_CODE_1 varchar2(2) null,
    WARN_CODE_2 varchar2(2) null,
    WARN_CODE_3 varchar2(2) null,
    WARN_CODE_4 varchar2(2) null,
    DRUG_KEY number(10) null,
    DRUG_NAME varchar2(171) null,
    FORM_CODE varchar2(3) null,
    ORG_FORM_CODE varchar2(3) null,
    FORM_LABEL_DESC varchar2(30) null,
    STRENGTH varchar2(177) null,
    VOLUME_TEXT varchar2(23) null,
    ORG_ITEM_NUM number(10) not null,
    ITEM_NUM number(10) not null,
    SFI_QTY number(10) null,
    CONNECT_SYSTEM varchar2(20) null,
    CAPD_VOUCHER_FLAG number(1) default 0 not null,
    MO_ISSUE_QTY number(10) null,
    MO_BASE_UNIT varchar2(4) null,
    DUP_CHK_DRUG_TYPE varchar2(1) not null,
    DUP_CHK_DISPLAY_NAME varchar2(70) null,
    ACTION_STATUS varchar2(1) not null,
    FM_STATUS varchar2(1) null,
    DANGER_DRUG_FLAG number(1) default 0 not null,
    WARD_STOCK_FLAG number(1) default 0 not null,
    REMARK_TEXT varchar2(125) null,    
    PHARM_ORDER_ID number(19) not null,
    MED_ORDER_ITEM_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_PHARM_ORDER_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  PHARM_ORDER_ITEM is 'Pharmacy order items to bridge between medication order items and dispensing order items';
comment on column PHARM_ORDER_ITEM.ID is 'Pharmacy order item id';
comment on column PHARM_ORDER_ITEM.STATUS is 'Record status';
comment on column PHARM_ORDER_ITEM.ITEM_CODE is 'Item code';
comment on column PHARM_ORDER_ITEM.REGIMEN_XML is 'Regimen content in XML format';
comment on column PHARM_ORDER_ITEM.DOSE_QTY is 'Dose quantity';
comment on column PHARM_ORDER_ITEM.DOSE_UNIT is 'Dose unit';
comment on column PHARM_ORDER_ITEM.CAL_QTY is 'Calculated quantity';
comment on column PHARM_ORDER_ITEM.ADJ_QTY is 'Adjusted quantity';
comment on column PHARM_ORDER_ITEM.ISSUE_QTY is 'Issued quantity';
comment on column PHARM_ORDER_ITEM.BASE_UNIT is 'Base unit';
comment on column PHARM_ORDER_ITEM.WARN_CODE_1 is 'Warning code 1';
comment on column PHARM_ORDER_ITEM.WARN_CODE_2 is 'Warning code 2';
comment on column PHARM_ORDER_ITEM.WARN_CODE_3 is 'Warning code 3';
comment on column PHARM_ORDER_ITEM.WARN_CODE_4 is 'Warning code 4';
comment on column PHARM_ORDER_ITEM.DRUG_KEY is 'Drug key';
comment on column PHARM_ORDER_ITEM.DRUG_NAME is 'Drug name';
comment on column PHARM_ORDER_ITEM.FORM_CODE is 'Form code';
comment on column PHARM_ORDER_ITEM.ORG_FORM_CODE is 'Original form code';
comment on column PHARM_ORDER_ITEM.FORM_LABEL_DESC is 'Form description shown on label';
comment on column PHARM_ORDER_ITEM.STRENGTH is 'Strength';
comment on column PHARM_ORDER_ITEM.VOLUME_TEXT is 'Volume text';
comment on column PHARM_ORDER_ITEM.ORG_ITEM_NUM is 'Original item number';
comment on column PHARM_ORDER_ITEM.ITEM_NUM is 'Item number';
comment on column PHARM_ORDER_ITEM.SFI_QTY is 'Quantity of SFI item';
comment on column PHARM_ORDER_ITEM.CONNECT_SYSTEM is 'CAPD connect system';
comment on column PHARM_ORDER_ITEM.CAPD_VOUCHER_FLAG is 'Contain CAPD voucher';
comment on column PHARM_ORDER_ITEM.MO_ISSUE_QTY is 'Issued quantity of medication order item';
comment on column PHARM_ORDER_ITEM.MO_BASE_UNIT is 'Base unit of medication order item';
comment on column PHARM_ORDER_ITEM.DUP_CHK_DRUG_TYPE is 'Drug type of duplicate drug check';
comment on column PHARM_ORDER_ITEM.DUP_CHK_DISPLAY_NAME is 'Display name for duplicate drug check';
comment on column PHARM_ORDER_ITEM.ACTION_STATUS is 'Action status';
comment on column PHARM_ORDER_ITEM.FM_STATUS is 'Formulary management status';
comment on column PHARM_ORDER_ITEM.DANGER_DRUG_FLAG is 'Dangerous drug';
comment on column PHARM_ORDER_ITEM.WARD_STOCK_FLAG is 'Ward stock item';
comment on column PHARM_ORDER_ITEM.REMARK_TEXT is 'Pharmacy remark';
comment on column PHARM_ORDER_ITEM.PHARM_ORDER_ID is 'Pharmacy order id';
comment on column PHARM_ORDER_ITEM.MED_ORDER_ITEM_ID is 'Medication order item id';
comment on column PHARM_ORDER_ITEM.CREATE_USER is 'Created user';
comment on column PHARM_ORDER_ITEM.CREATE_DATE is 'Created date';
comment on column PHARM_ORDER_ITEM.UPDATE_USER is 'Last updated user';
comment on column PHARM_ORDER_ITEM.UPDATE_DATE is 'Last updated date';
comment on column PHARM_ORDER_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_SCHEDULE (
    ID number(19) not null,
    REFILL_COUPON_NUM varchar2(19) not null,
    TRIM_REFILL_COUPON_NUM varchar2(17) not null,
    GROUP_NUM number(10) not null,
    REFILL_NUM number(10) not null,
    NUM_OF_REFILL number(10) not null,
    REFILL_DATE date not null,
    DOC_TYPE varchar2(1) not null,
    REFILL_STATUS varchar2(1) not null,
    STATUS varchar2(1) not null,
    NEXT_PHARM_APPT_DATE date null,
    BEFORE_ADJ_REFILL_DATE date null,
    NUM_OF_COUPON number(10) null,
    PRINT_FLAG number(1) default 0 not null,
    FORCE_COMPLETE_DATE date null,
    PRINT_LANG varchar2(10) not null,
    PRINT_REMINDER_FLAG number(1) default 0 not null,
    DISP_ORDER_ID number(19) null,
    PHARM_ORDER_ID number(19) not null,
    TICKET_ID number(19) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_SCHEDULE primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  REFILL_SCHEDULE is 'Refill schedules';
comment on column REFILL_SCHEDULE.ID is 'Refill schedule id';
comment on column REFILL_SCHEDULE.REFILL_COUPON_NUM is 'Refill coupon number';
comment on column REFILL_SCHEDULE.TRIM_REFILL_COUPON_NUM is 'Trimmed refill coupon number for enquiry in one stop entry';
comment on column REFILL_SCHEDULE.GROUP_NUM is 'Refill group number';
comment on column REFILL_SCHEDULE.REFILL_NUM is 'Generated refill number for sequence';
comment on column REFILL_SCHEDULE.NUM_OF_REFILL is 'Number of refill(s)';
comment on column REFILL_SCHEDULE.REFILL_DATE is 'Refill date';
comment on column REFILL_SCHEDULE.DOC_TYPE is 'Refill schedule type';
comment on column REFILL_SCHEDULE.REFILL_STATUS is 'Refill schedule status';
comment on column REFILL_SCHEDULE.STATUS is 'Record status';
comment on column REFILL_SCHEDULE.NEXT_PHARM_APPT_DATE is 'Next pharmacy appointment date';
comment on column REFILL_SCHEDULE.BEFORE_ADJ_REFILL_DATE is 'Refill date before holiday adjustment';
comment on column REFILL_SCHEDULE.NUM_OF_COUPON is 'Number of coupon(s)';
comment on column REFILL_SCHEDULE.PRINT_FLAG is 'Printed';
comment on column REFILL_SCHEDULE.FORCE_COMPLETE_DATE is 'Force completed date';
comment on column REFILL_SCHEDULE.PRINT_LANG is 'Printed language';
comment on column REFILL_SCHEDULE.PRINT_REMINDER_FLAG is 'Reminder printed';
comment on column REFILL_SCHEDULE.DISP_ORDER_ID is 'Dispensing Order id';
comment on column REFILL_SCHEDULE.PHARM_ORDER_ID is 'Pharmacy order id';
comment on column REFILL_SCHEDULE.TICKET_ID is 'Ticket id';
comment on column REFILL_SCHEDULE.HOSP_CODE is 'Hospital code';
comment on column REFILL_SCHEDULE.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_SCHEDULE.CREATE_USER is 'Created user';
comment on column REFILL_SCHEDULE.CREATE_DATE is 'Created date';
comment on column REFILL_SCHEDULE.UPDATE_USER is 'Last updated user';
comment on column REFILL_SCHEDULE.UPDATE_DATE is 'Last updated date';
comment on column REFILL_SCHEDULE.VERSION is 'Version to serve as optimistic lock value';


create table REFILL_SCHEDULE_ITEM (
    ID number(19) not null,
    REFILL_DURATION number(10) null,
    REFILL_END_DATE date null,
    CAL_QTY number(10) not null,
    ADJ_QTY number(10) not null,
    REFILL_QTY number(10) not null,
    HANDLE_EXEMPT_CODE varchar2(2) null,
    HANDLE_EXEMPT_USER varchar2(12) null,
    NEXT_HANDLE_EXEMPT_CODE varchar2(2) null,
    ITEM_NUM number(10) not null,
    SUPPLY_FOR_DAY number(10) null,
    ACTIVE_FLAG number(1) default 0 not null,
    UNIT_PRICE number(19,4) null,
    NUM_OF_REFILL number(10) not null,
    REFILL_SCHEDULE_ID number(19) not null,
    PHARM_ORDER_ITEM_ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REFILL_SCHEDULE_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);

comment on table  REFILL_SCHEDULE_ITEM is 'Refill schedule items';
comment on column REFILL_SCHEDULE_ITEM.ID is 'Refill schedule item id';
comment on column REFILL_SCHEDULE_ITEM.REFILL_DURATION is 'Refill duration';
comment on column REFILL_SCHEDULE_ITEM.REFILL_END_DATE is 'Refill end date';
comment on column REFILL_SCHEDULE_ITEM.CAL_QTY is 'Calculated quantity';
comment on column REFILL_SCHEDULE_ITEM.ADJ_QTY is 'Adjusted quantity';
comment on column REFILL_SCHEDULE_ITEM.REFILL_QTY is 'Refill qty';
comment on column REFILL_SCHEDULE_ITEM.HANDLE_EXEMPT_CODE is 'Exemption code (Charging reason code)';
comment on column REFILL_SCHEDULE_ITEM.HANDLE_EXEMPT_USER is 'User to exempt handling charge';
comment on column REFILL_SCHEDULE_ITEM.NEXT_HANDLE_EXEMPT_CODE is 'Exemption code for next SFI refill (Charging reason code)';
comment on column REFILL_SCHEDULE_ITEM.ITEM_NUM is 'Item number';
comment on column REFILL_SCHEDULE_ITEM.SUPPLY_FOR_DAY is 'Supply for day(s) as of refill date';
comment on column REFILL_SCHEDULE_ITEM.ACTIVE_FLAG is 'Active refill schedule item for refill report';
comment on column REFILL_SCHEDULE_ITEM.UNIT_PRICE is 'Drug unit price';
comment on column REFILL_SCHEDULE_ITEM.NUM_OF_REFILL is 'Number of Refill in Item level';
comment on column REFILL_SCHEDULE_ITEM.REFILL_SCHEDULE_ID is 'Refill schedule id';
comment on column REFILL_SCHEDULE_ITEM.PHARM_ORDER_ITEM_ID is 'Pharmacy order item id';
comment on column REFILL_SCHEDULE_ITEM.HOSP_CODE is 'Hospital code';
comment on column REFILL_SCHEDULE_ITEM.WORKSTORE_CODE is 'Workstore code';
comment on column REFILL_SCHEDULE_ITEM.CREATE_USER is 'Created user';
comment on column REFILL_SCHEDULE_ITEM.CREATE_DATE is 'Created date';
comment on column REFILL_SCHEDULE_ITEM.UPDATE_USER is 'Last updated user';
comment on column REFILL_SCHEDULE_ITEM.UPDATE_DATE is 'Last updated date';
comment on column REFILL_SCHEDULE_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table TICKET (
    ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    ORDER_TYPE varchar2(1) not null,
    TICKET_DATE date not null,
    TICKET_NUM varchar2(4) not null,
    ORDER_NUM varchar2(13) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_TICKET primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_TICKET_01 unique (HOSP_CODE, WORKSTORE_CODE, ORDER_TYPE, TICKET_DATE, TICKET_NUM) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_DISP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_DISP_DATA_2012);
    
comment on table  TICKET is 'Tickets distributed to patients to collect prescribed drugs';
comment on column TICKET.ID is 'Ticket id';
comment on column TICKET.HOSP_CODE is 'Hospital code';
comment on column TICKET.WORKSTORE_CODE is 'Workstore code';
comment on column TICKET.ORDER_TYPE is 'Order type';
comment on column TICKET.TICKET_DATE is 'Ticket date';
comment on column TICKET.TICKET_NUM is 'Ticket number';
comment on column TICKET.ORDER_NUM is 'Order Number Printed on Ticket Label';
comment on column TICKET.CREATE_USER is 'Created user';
comment on column TICKET.CREATE_DATE is 'Created date';
comment on column TICKET.UPDATE_USER is 'Last updated user';
comment on column TICKET.UPDATE_DATE is 'Last updated date';
comment on column TICKET.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table BATCH_ISSUE_LOG;
drop table BATCH_ISSUE_LOG_DETAIL;
drop table CAPD_VOUCHER;
drop table CAPD_VOUCHER_ITEM;
drop table DISP_ORDER;
drop table DISP_ORDER_ITEM;
drop table INVOICE;
drop table INVOICE_ITEM;
drop table MED_CASE;
drop table MED_ORDER;
drop table MED_ORDER_FM;
drop table MED_ORDER_ITEM;
drop table MED_ORDER_ITEM_ALERT;
drop table PATIENT;
drop table PHARM_ORDER;
drop table PHARM_ORDER_ITEM;
drop table REFILL_SCHEDULE;
drop table REFILL_SCHEDULE_ITEM;
drop table TICKET;
--//
