insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (14028,'charging.fcs.gsDirectPayment.rollout','For external system: FCS rollouts enhancement of GS direct payment (to be obsolete)','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

update PROP set REMINDER_MESSAGE='Fees Collection System (FCS) service is not available.' where id=14020;

--//@UNDO
delete from PROP where ID=14028;
--//