create table DELIVERY (
    ID number(19) not null,
    WARD_CODE varchar2(4) null,
    BATCH_NUM varchar2(4) not null,
    BATCH_DATE date not null,
    DELIVERY_DATE timestamp null,
    PRINT_MODE varchar2(1) not null,
    STATUS varchar2(1) not null,
    TYPE varchar2(1) not null,
    CHECK_DATE timestamp null,
    CMM_DISP_ID number(19) null,
    ERROR_FLAG number(1) default 0 not null,
    DELIVERY_REQUEST_ID number(19) null,
    HOSP_CODE varchar2(3) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  DELIVERY is 'Deliveries by batch';
comment on column DELIVERY.ID is 'Delivery id';
comment on column DELIVERY.WARD_CODE is 'Ward code';
comment on column DELIVERY.BATCH_NUM is 'Batch number';
comment on column DELIVERY.BATCH_DATE is 'Batch date';
comment on column DELIVERY.DELIVERY_DATE is 'Delivery date';
comment on column DELIVERY.PRINT_MODE is 'Printing mode';
comment on column DELIVERY.STATUS is 'Record status';
comment on column DELIVERY.TYPE is 'Delivery type';
comment on column DELIVERY.CHECK_DATE is 'Checked date';
comment on column DELIVERY.CMM_DISP_ID is 'Unique id of CMM dispensing record';
comment on column DELIVERY.ERROR_FLAG is 'Error Flag';
comment on column DELIVERY.DELIVERY_REQUEST_ID is 'Delivery request id';
comment on column DELIVERY.HOSP_CODE is 'Hospital code';
comment on column DELIVERY.CREATE_USER is 'Created user';
comment on column DELIVERY.CREATE_DATE is 'Created date';
comment on column DELIVERY.UPDATE_USER is 'Last updated user';
comment on column DELIVERY.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY.VERSION is 'Version to serve as optimistic lock value';


create table DELIVERY_ITEM (
    ID number(19) not null,
    LABEL_XML clob null,
    DUE_DATE timestamp not null,
    ISSUE_QTY number(19,4) not null,
    ISSUE_DURATION number(19,4) not null,
    STATUS varchar2(1) not null,
    REFILL_FLAG number(1) default 0 not null,
    DELIVERY_ID number(19) not null,
    MED_PROFILE_PO_ITEM_ID number(19) null,
    REPLENISHMENT_ITEM_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  DELIVERY_ITEM is 'Delivery items';
comment on column DELIVERY_ITEM.ID is 'Delivery item id';
comment on column DELIVERY_ITEM.LABEL_XML is 'Label content in XML format';
comment on column DELIVERY_ITEM.DUE_DATE is 'Due date';
comment on column DELIVERY_ITEM.ISSUE_QTY is 'Issued quantity';
comment on column DELIVERY_ITEM.ISSUE_DURATION is 'Duration of issued item';
comment on column DELIVERY_ITEM.STATUS is 'Record status';
comment on column DELIVERY_ITEM.REFILL_FLAG is 'Refill item';
comment on column DELIVERY_ITEM.DELIVERY_ID is 'Delivery id';
comment on column DELIVERY_ITEM.MED_PROFILE_PO_ITEM_ID is 'Pharmacy order item id (medication profile)';
comment on column DELIVERY_ITEM.REPLENISHMENT_ITEM_ID is 'Replenishment item id';
comment on column DELIVERY_ITEM.CREATE_USER is 'Created user';
comment on column DELIVERY_ITEM.CREATE_DATE is 'Created date';
comment on column DELIVERY_ITEM.UPDATE_USER is 'Last updated user';
comment on column DELIVERY_ITEM.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table DELIVERY_REQUEST (
    ID number(19) not null,
    WARD_CODE_CSV clob null,
    CASE_NUM varchar2(12) null,
    DUE_DATE timestamp null,
    GENERATE_DAY number(10) null,
    WORKSTATION_CODE varchar2(4) null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY_REQUEST primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  DELIVERY_REQUEST is 'Delivery requests by users';
comment on column DELIVERY_REQUEST.ID is 'Delivery request id';
comment on column DELIVERY_REQUEST.WARD_CODE_CSV is 'Ward code in CSV format';
comment on column DELIVERY_REQUEST.CASE_NUM is 'Case number';
comment on column DELIVERY_REQUEST.DUE_DATE is 'Due date';
comment on column DELIVERY_REQUEST.GENERATE_DAY is 'Generated day';
comment on column DELIVERY_REQUEST.WORKSTATION_CODE is 'Workstation Code';    
comment on column DELIVERY_REQUEST.HOSP_CODE is 'Hospital code';
comment on column DELIVERY_REQUEST.WORKSTORE_CODE is 'Workstore code';
comment on column DELIVERY_REQUEST.CREATE_USER is 'Created user';
comment on column DELIVERY_REQUEST.CREATE_DATE is 'Created date';
comment on column DELIVERY_REQUEST.UPDATE_USER is 'Last updated user';
comment on column DELIVERY_REQUEST.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY_REQUEST.VERSION is 'Version to serve as optimistic lock value';


create table DELIVERY_REQUEST_ALERT (
    ID number(19) not null,
    CASE_NUM varchar2(12) null,
    ALERT_PROFILE_XML clob null,
    MDS_CHECK_RESULT_XML clob null,
    PREGNANCY_CHECK_FLAG number(1) default 0 not null,
    DELIVERY_REQUEST_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_DELIVERY_REQUEST_ALERT primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  DELIVERY_REQUEST_ALERT is 'Alerts of delivery requests';
comment on column DELIVERY_REQUEST_ALERT.ID is 'Delivery request id';
comment on column DELIVERY_REQUEST_ALERT.CASE_NUM is 'Case number';
comment on column DELIVERY_REQUEST_ALERT.ALERT_PROFILE_XML is 'Alert profile content in XML format';
comment on column DELIVERY_REQUEST_ALERT.MDS_CHECK_RESULT_XML is 'MDS result content in XML format';
comment on column DELIVERY_REQUEST_ALERT.PREGNANCY_CHECK_FLAG is 'Checked pregnancy';
comment on column DELIVERY_REQUEST_ALERT.DELIVERY_REQUEST_ID is 'Delivery request id';
comment on column DELIVERY_REQUEST_ALERT.CREATE_USER is 'Created user';
comment on column DELIVERY_REQUEST_ALERT.CREATE_DATE is 'Created date';
comment on column DELIVERY_REQUEST_ALERT.UPDATE_USER is 'Last updated user';
comment on column DELIVERY_REQUEST_ALERT.UPDATE_DATE is 'Last updated date';
comment on column DELIVERY_REQUEST_ALERT.VERSION is 'Version to serve as optimistic lock value';



create table MED_PROFILE (
    ID number(19) not null,
    ORDER_NUM varchar2(12) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    STATUS varchar2(1) not null,
    DISCHARGE_DATE timestamp null,
    RESUME_ACTIVE_DATE timestamp null,
    RETURN_DATE timestamp null,
    CURR_SEQ_NUM number(10) null,
    PROCESSING_FLAG number(1) default 0 not null,
    PREGNANCY_CHECK_FLAG number(1) default 0 not null,
    OTHER_DOC varchar2(12) null,
    HOME_LEAVE_DATE timestamp null,
    LAST_ITEM_DATE timestamp null,
    PRINT_LANG varchar2(10) not null,
    DELIVERY_GEN_FLAG number(1) default 0 not null,
    WORKSTATION_ID number(19) null,
    PATIENT_ID number(19) not null,
    MED_CASE_ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    WARD_CODE varchar2(4) null,
    SPEC_CODE varchar2(4) null,
    PAT_CAT_CODE varchar2(2) null,
    PRIVATE_FLAG number(1) default 0 not null,
    BODY_WEIGHT number(4,1) null,
    BODY_HEIGHT number(4,1) null,
    BODY_INFO_UPDATE_DATE timestamp null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_MED_PROFILE_01 unique (ORDER_NUM) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE is 'Medication profiles by medication orders';
comment on column MED_PROFILE.ID is 'Medication profile id';
comment on column MED_PROFILE.ORDER_NUM is 'Medication order number';
comment on column MED_PROFILE.PAT_HOSP_CODE is 'Patient hospital code';
comment on column MED_PROFILE.STATUS is 'Record status';
comment on column MED_PROFILE.DISCHARGE_DATE is 'Discharge date';
comment on column MED_PROFILE.RESUME_ACTIVE_DATE is 'Date of resuming medication profile to be active';
comment on column MED_PROFILE.RETURN_DATE is 'Returned date after home leave';
comment on column MED_PROFILE.CURR_SEQ_NUM is 'Current sequence number';
comment on column MED_PROFILE.PROCESSING_FLAG is 'Processing';
comment on column MED_PROFILE.PREGNANCY_CHECK_FLAG is 'Checked pregnancy';
comment on column MED_PROFILE.OTHER_DOC is 'Other identity document';
comment on column MED_PROFILE.HOME_LEAVE_DATE is 'Date of home leave';
comment on column MED_PROFILE.LAST_ITEM_DATE is 'Last item arrival date';
comment on column MED_PROFILE.PRINT_LANG is 'Printed language';
comment on column MED_PROFILE.DELIVERY_GEN_FLAG is 'Delivery generating';
comment on column MED_PROFILE.WORKSTATION_ID is 'Workstation id';
comment on column MED_PROFILE.PATIENT_ID is 'Patient id';
comment on column MED_PROFILE.MED_CASE_ID is 'Medication case id';
comment on column MED_PROFILE.HOSP_CODE is 'Hospital code';
comment on column MED_PROFILE.WORKSTORE_CODE is 'Workstore code';
comment on column MED_PROFILE.WARD_CODE is 'Ward code';
comment on column MED_PROFILE.SPEC_CODE is 'Specialty code';
comment on column MED_PROFILE.PAT_CAT_CODE is 'Patient category code';
comment on column MED_PROFILE.PRIVATE_FLAG is 'Private patient';
comment on column MED_PROFILE.BODY_WEIGHT is 'Body weight';
comment on column MED_PROFILE.BODY_HEIGHT is 'Body height';
comment on column MED_PROFILE.BODY_INFO_UPDATE_DATE is 'Last update date of body information';
comment on column MED_PROFILE.CREATE_USER is 'Created user';
comment on column MED_PROFILE.CREATE_DATE is 'Created date';
comment on column MED_PROFILE.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_ERROR_LOG (
    ID number(19) not null,
    TYPE varchar2(1) not null,
    MESSAGE varchar2(100) null,
    CREATE_DATE timestamp not null,
    MED_PROFILE_ITEM_ID number(19) not null,
    DELIVERY_REQUEST_ID number(19) null,
    constraint PK_MED_PROFILE_ERROR_LOG primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_ERROR_LOG is 'Error logs of medication profiles';
comment on column MED_PROFILE_ERROR_LOG.ID is 'Medication profile error log id';
comment on column MED_PROFILE_ERROR_LOG.TYPE is 'Medication profile error log type';
comment on column MED_PROFILE_ERROR_LOG.MESSAGE is 'Error message';
comment on column MED_PROFILE_ERROR_LOG.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_ERROR_LOG.MED_PROFILE_ITEM_ID is 'Medication profile item id';
comment on column MED_PROFILE_ERROR_LOG.DELIVERY_REQUEST_ID is 'Delivery request id';


create table MED_PROFILE_ERROR_STAT (
    ID number(19) not null,
    DUE_DATE timestamp null,
    ERROR_DATE timestamp null,
    SUSPEND_FLAG number(1) default 0 not null,
    MISS_SPEC_FLAG number(1) default 0 not null,
    MDS_ERROR_FLAG number(1) default 0 not null,
    CHECK_USER varchar2(12) null,
    CHECK_DATE timestamp null,
    MED_PROFILE_ID number(19) not null,
    constraint PK_MED_PROFILE_ERROR_STAT primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MED_PROFILE_ERROR_STAT is 'Consolidated error statistics of medication profiles';
comment on column MED_PROFILE_ERROR_STAT.ID is 'Medication profile error statistic id';
comment on column MED_PROFILE_ERROR_STAT.DUE_DATE is 'Due date';
comment on column MED_PROFILE_ERROR_STAT.ERROR_DATE is 'Error date';
comment on column MED_PROFILE_ERROR_STAT.SUSPEND_FLAG is 'Suspended';
comment on column MED_PROFILE_ERROR_STAT.MISS_SPEC_FLAG is 'Missing specialty';
comment on column MED_PROFILE_ERROR_STAT.MDS_ERROR_FLAG is 'MDS error';
comment on column MED_PROFILE_ERROR_STAT.CHECK_USER is 'Checked user';
comment on column MED_PROFILE_ERROR_STAT.CHECK_DATE is 'Checked date';
comment on column MED_PROFILE_ERROR_STAT.MED_PROFILE_ID is 'Medication profile id';


create table MED_PROFILE_ITEM (
    ID number(19) not null,
    ORG_ITEM_NUM number(10) not null,
    DUE_DATE timestamp null,
    ADMIN_DATE timestamp null,
    STATUS varchar2(1) not null,
    STATUS_UPDATE_DATE timestamp null,
    ADMIN_STATUS varchar2(1) not null,
    REPLENISH_FLAG number(1) default 0 not null,
    AMEND_FLAG number(1) default 0 not null,
    VERIFY_DATE timestamp null,
    DELIVERY_GEN_FLAG number(1) default 0 not null,
    MED_PROFILE_ID number(19) not null,
    MED_PROFILE_MO_ITEM_ID number(19) null,
    MED_PROFILE_ERROR_LOG_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE_ITEM primary key (ID) using index tablespace PMS_INDX_01,
    constraint UI_MED_PROFILE_ITEM_01 unique (MED_PROFILE_ID, ORG_ITEM_NUM) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_ITEM is 'Medication profile items';
comment on column MED_PROFILE_ITEM.ID is 'Medication profile id';
comment on column MED_PROFILE_ITEM.ORG_ITEM_NUM is 'Original item number';
comment on column MED_PROFILE_ITEM.DUE_DATE is 'Due date';
comment on column MED_PROFILE_ITEM.ADMIN_DATE is 'Administration date';
comment on column MED_PROFILE_ITEM.STATUS is 'Record status';
comment on column MED_PROFILE_ITEM.STATUS_UPDATE_DATE is 'Last update date of record status';
comment on column MED_PROFILE_ITEM.ADMIN_STATUS is 'Pharmacy admin status';
comment on column MED_PROFILE_ITEM.REPLENISH_FLAG is 'Replenished item';
comment on column MED_PROFILE_ITEM.AMEND_FLAG is 'Amended';
comment on column MED_PROFILE_ITEM.VERIFY_DATE is 'Verified date';
comment on column MED_PROFILE_ITEM.DELIVERY_GEN_FLAG is 'Delivery generating';
comment on column MED_PROFILE_ITEM.MED_PROFILE_ID is 'Medication profile id';
comment on column MED_PROFILE_ITEM.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column MED_PROFILE_ITEM.MED_PROFILE_ERROR_LOG_ID is 'Error log id of medication profile';
comment on column MED_PROFILE_ITEM.CREATE_USER is 'Created user';
comment on column MED_PROFILE_ITEM.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_ITEM.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE_ITEM.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_MO_ITEM (
    ID number(19) not null,
    ORG_ITEM_NUM number(10) null,
    ITEM_NUM number(10) not null,
    RX_ITEM_XML clob null,
    RX_ITEM_TYPE varchar2(1) null,
    START_DATE timestamp null,
    END_DATE timestamp null,
    SUSPEND_CODE varchar2(2) null,
    MDS_SUSPEND_REASON varchar2(44) null,
    PENDING_CODE varchar2(2) null,
    PENDING_DESC varchar2(44) null,
    PENDING_SUPPL_DESC varchar2(255) null,
    PENDING_USER varchar2(12) null,
    PENDING_DATE timestamp null,
    UNIT_OF_CHARGE number(10) null,
    CHARGE_FLAG varchar2(1) not null,
    STAT_FLAG number(1) default 0 not null,
    PPMI_FLAG number(1) default 0 not null,
    ALERT_FLAG number(1) default 0 not null,
    FM_FLAG number(1) default 0 not null,
    DOCTOR_CODE varchar2(12) null,
    DOCTOR_NAME varchar2(48) null,
    DOCTOR_RANK_CODE varchar2(10) null,
    FIRST_PRINT_DATE timestamp null,
    URGENT_FLAG number(1) default 0 not null,
    PRINT_INSTRUCTION_FLAG number(1) default 0 not null,
    STATUS varchar2(1) not null,
    CMM_ITEM_FLAG number(1) default 0 not null,
    CMM_ITEM_STATUS varchar2(1) not null,
    OVERRIDE_DESC varchar2(255) null,
    OVERRIDE_DOCTOR_CODE varchar2(12) null,
    OVERRIDE_DOCTOR_RANK_CODE varchar2(10) null,
    OVERRIDE_DOCTOR_NAME varchar2(48) null,
    OVERRIDE_DATE timestamp null,
    RESUME_PENDING_REMARK varchar2(255) null,
    URGENT_NUM number(10) null,
    ORDER_DATE timestamp not null,
    MED_PROFILE_ITEM_ID number(19) null,
    MED_PROFILE_ORDER_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE_MO_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_MO_ITEM is 'Medication order items of medication profiles';
comment on column MED_PROFILE_MO_ITEM.ID is 'Medication order item id (medication profile)';
comment on column MED_PROFILE_MO_ITEM.ORG_ITEM_NUM is 'Original item number';
comment on column MED_PROFILE_MO_ITEM.ITEM_NUM is 'Item number';
comment on column MED_PROFILE_MO_ITEM.RX_ITEM_XML is 'Medication order item in XML format';
comment on column MED_PROFILE_MO_ITEM.RX_ITEM_TYPE is 'Medication order item type';
comment on column MED_PROFILE_MO_ITEM.START_DATE is 'Start date of administration';
comment on column MED_PROFILE_MO_ITEM.END_DATE is 'End date of administration';
comment on column MED_PROFILE_MO_ITEM.SUSPEND_CODE is 'Suspend code';
comment on column MED_PROFILE_MO_ITEM.MDS_SUSPEND_REASON is 'MDS suspend reason';
comment on column MED_PROFILE_MO_ITEM.PENDING_CODE is 'Pending code';
comment on column MED_PROFILE_MO_ITEM.PENDING_DESC is 'Pending description';
comment on column MED_PROFILE_MO_ITEM.PENDING_SUPPL_DESC is 'Pending supplementary description';
comment on column MED_PROFILE_MO_ITEM.PENDING_USER is 'User who pended item';
comment on column MED_PROFILE_MO_ITEM.PENDING_DATE is 'Pending date';
comment on column MED_PROFILE_MO_ITEM.UNIT_OF_CHARGE is 'Unit(s) of charge';
comment on column MED_PROFILE_MO_ITEM.CHARGE_FLAG is 'Charging required';
comment on column MED_PROFILE_MO_ITEM.STAT_FLAG is 'STAT then iten';
comment on column MED_PROFILE_MO_ITEM.PPMI_FLAG is 'PPMI item';
comment on column MED_PROFILE_MO_ITEM.ALERT_FLAG is 'Alerted';
comment on column MED_PROFILE_MO_ITEM.FM_FLAG is 'Contain formulary management';
comment on column MED_PROFILE_MO_ITEM.DOCTOR_CODE is 'Doctor code';
comment on column MED_PROFILE_MO_ITEM.DOCTOR_NAME is 'Doctor name';
comment on column MED_PROFILE_MO_ITEM.DOCTOR_RANK_CODE is 'Doctor rank code';
comment on column MED_PROFILE_MO_ITEM.FIRST_PRINT_DATE is 'First printed date';
comment on column MED_PROFILE_MO_ITEM.URGENT_FLAG is 'Urgent item';
comment on column MED_PROFILE_MO_ITEM.PRINT_INSTRUCTION_FLAG is 'Printing instruction required';
comment on column MED_PROFILE_MO_ITEM.STATUS is 'Record status';
comment on column MED_PROFILE_MO_ITEM.CMM_ITEM_FLAG is 'CMM item';
comment on column MED_PROFILE_MO_ITEM.CMM_ITEM_STATUS is 'CMM item status';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DESC is 'Override pending description';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DOCTOR_CODE is 'Override pending doctor code';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DOCTOR_RANK_CODE is 'Override pending doctor rank code';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DOCTOR_NAME is 'Override pending doctor name';
comment on column MED_PROFILE_MO_ITEM.OVERRIDE_DATE is 'Override date';
comment on column MED_PROFILE_MO_ITEM.RESUME_PENDING_REMARK is 'Resume pending remark';
comment on column MED_PROFILE_MO_ITEM.URGENT_NUM is 'Urgent Dispense Number';    
comment on column MED_PROFILE_MO_ITEM.ORDER_DATE is 'Order Date';
comment on column MED_PROFILE_MO_ITEM.MED_PROFILE_ITEM_ID is 'Medication profile item id';
comment on column MED_PROFILE_MO_ITEM.MED_PROFILE_ORDER_ID is 'Medication profile order id';
comment on column MED_PROFILE_MO_ITEM.CREATE_USER is 'Created user';
comment on column MED_PROFILE_MO_ITEM.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_MO_ITEM.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE_MO_ITEM.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE_MO_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_MO_ITEM_ALERT (
    ID number(19) not null,
    ADDITIVE_NUM number(10) null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    ALERT_TYPE varchar2(8) not null,
    ALERT_XML clob null,
    OVERRIDE_REASON varchar2(500) null,
    ACK_USER varchar2(12) null,
    ACK_DATE timestamp null,
    SORT_SEQ number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE_MO_ITEM_ALERT primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_MO_ITEM_ALERT is 'Alerts of medication order items';
comment on column MED_PROFILE_MO_ITEM_ALERT.ID is 'Medication order item alert id (medication profile)';
comment on column MED_PROFILE_MO_ITEM_ALERT.ADDITIVE_NUM is 'Additive number';
comment on column MED_PROFILE_MO_ITEM_ALERT.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column MED_PROFILE_MO_ITEM_ALERT.ALERT_TYPE is 'Alert type';
comment on column MED_PROFILE_MO_ITEM_ALERT.ALERT_XML is 'Alert content in XML format';
comment on column MED_PROFILE_MO_ITEM_ALERT.OVERRIDE_REASON is 'Overriding reason';
comment on column MED_PROFILE_MO_ITEM_ALERT.ACK_USER is 'Acknowledged user';
comment on column MED_PROFILE_MO_ITEM_ALERT.ACK_DATE is 'Acknowledged date';
comment on column MED_PROFILE_MO_ITEM_ALERT.SORT_SEQ is 'Sort sequence';    
comment on column MED_PROFILE_MO_ITEM_ALERT.CREATE_USER is 'Created user';
comment on column MED_PROFILE_MO_ITEM_ALERT.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_MO_ITEM_ALERT.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE_MO_ITEM_ALERT.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE_MO_ITEM_ALERT.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_MO_ITEM_FM (
    ID number(19) not null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    ITEM_CODE varchar2(6) null,
    DISPLAY_NAME varchar2(70) not null,
    FORM_CODE varchar2(3) not null,
    SALT_PROPERTY varchar2(35) null,
    CORP_IND_CODE varchar2(10) null,
    SUPPL_IND_CODE varchar2(10) null,
    OTHER_MESSAGE varchar2(100) null,
    AUTH_DOCTOR_NAME varchar2(70) null,
    FM_CREATE_USER varchar2(12) null,
    FM_CREATE_DATE timestamp null,
    IND_VALID_PERIOD number(10) null,
    PRESC_OPTION varchar2(2) not null,
    REF_ORDER_NUM varchar2(16) not null,
    REF_ITEM_NUM number(10) not null,
    FM_STATUS varchar2(1) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE_MO_ITEM_FM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_MO_ITEM_FM is 'Formulary management of medication order items';
comment on column MED_PROFILE_MO_ITEM_FM.ID is 'Formulary management id of medication order item (medication profile)';
comment on column MED_PROFILE_MO_ITEM_FM.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column MED_PROFILE_MO_ITEM_FM.ITEM_CODE is 'Item code';
comment on column MED_PROFILE_MO_ITEM_FM.DISPLAY_NAME is 'Drug display name';
comment on column MED_PROFILE_MO_ITEM_FM.FORM_CODE is 'Form code';
comment on column MED_PROFILE_MO_ITEM_FM.SALT_PROPERTY is 'Salt property';
comment on column MED_PROFILE_MO_ITEM_FM.CORP_IND_CODE is 'Corporate indication code';
comment on column MED_PROFILE_MO_ITEM_FM.SUPPL_IND_CODE is 'Supplementary indication code';
comment on column MED_PROFILE_MO_ITEM_FM.OTHER_MESSAGE is 'Other message';
comment on column MED_PROFILE_MO_ITEM_FM.AUTH_DOCTOR_NAME is 'Authorized doctor name';
comment on column MED_PROFILE_MO_ITEM_FM.FM_CREATE_USER is 'Created user of formulary management';
comment on column MED_PROFILE_MO_ITEM_FM.FM_CREATE_DATE is 'Created date of formulary management';
comment on column MED_PROFILE_MO_ITEM_FM.IND_VALID_PERIOD is 'Indication valid period';
comment on column MED_PROFILE_MO_ITEM_FM.PRESC_OPTION is 'Prescription option';
comment on column MED_PROFILE_MO_ITEM_FM.REF_ORDER_NUM is 'Reference order number';
comment on column MED_PROFILE_MO_ITEM_FM.REF_ITEM_NUM is 'Reference item number';
comment on column MED_PROFILE_MO_ITEM_FM.FM_STATUS is 'Formulary management status';
comment on column MED_PROFILE_MO_ITEM_FM.CREATE_USER is 'Created user';
comment on column MED_PROFILE_MO_ITEM_FM.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_MO_ITEM_FM.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE_MO_ITEM_FM.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE_MO_ITEM_FM.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_ORDER (
    ID number(19) not null,
    ORDER_NUM varchar2(12) not null,
    SEQ_NUM number(10) null,
    TYPE varchar2(2) not null,
    PAT_HOSP_CODE varchar2(3) not null,
    CASE_NUM varchar2(12) null,
    PAT_KEY varchar2(8) null,
    ADMIN_DATE timestamp null,
    STATUS varchar2(1) not null,
    DISCONTINUE_REASON varchar2(255) null,
    DISCONTINUE_HOSP_CODE varchar2(3) null,
    DISCONTINUE_DATE timestamp null,
    ALLOW_REPEAT_FLAG number(1) default 0 null,
    PREGNANCY_CHECK_FLAG number(1) default 0 null,
    PAS_SPEC_CODE varchar2(4) null,
    PAS_SUB_SPEC_CODE varchar2(4) null,
    PAS_BED_NUM varchar2(5) null,
    PAS_WARD_CODE varchar2(4) null,
    ORDER_DIGEST varchar2(100) null,
    HOSP_CODE varchar2(3) null,
    WORKSTORE_CODE varchar2(4) null,
    WARD_CODE varchar2(4) null,
    SPEC_CODE varchar2(4) null,
    PAT_CAT_CODE varchar2(2) null,
    PRIVATE_FLAG number(1) default 0 not null,
    BODY_WEIGHT number(4,1) null,
    BODY_HEIGHT number(4,1) null,
    BODY_INFO_UPDATE_DATE timestamp null,    
    ORDER_DATE timestamp not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE_ORDER primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_ORDER is 'Staging order of medication profile pending for processing';
comment on column MED_PROFILE_ORDER.ID is 'Medication profile order id';
comment on column MED_PROFILE_ORDER.ORDER_NUM is 'Medication order number';
comment on column MED_PROFILE_ORDER.SEQ_NUM is 'Sequence number';
comment on column MED_PROFILE_ORDER.TYPE is 'Medication profile order type';
comment on column MED_PROFILE_ORDER.PAT_HOSP_CODE is 'Patient hospital code';
comment on column MED_PROFILE_ORDER.CASE_NUM is 'Case number';
comment on column MED_PROFILE_ORDER.PAT_KEY is 'Patient key';
comment on column MED_PROFILE_ORDER.ADMIN_DATE is 'Administration date';
comment on column MED_PROFILE_ORDER.STATUS is 'Record status';
comment on column MED_PROFILE_ORDER.DISCONTINUE_REASON is 'Discontinued reason';
comment on column MED_PROFILE_ORDER.DISCONTINUE_HOSP_CODE is 'Discontinue Hospital Code';
comment on column MED_PROFILE_ORDER.DISCONTINUE_DATE is 'Discontinue Date';
comment on column MED_PROFILE_ORDER.ALLOW_REPEAT_FLAG is 'Allow repeat';
comment on column MED_PROFILE_ORDER.PREGNANCY_CHECK_FLAG is 'Checked pregnancy';
comment on column MED_PROFILE_ORDER.PAS_SPEC_CODE is 'PAS specialty code';
comment on column MED_PROFILE_ORDER.PAS_SUB_SPEC_CODE is 'PAS sub-specialty code';
comment on column MED_PROFILE_ORDER.PAS_BED_NUM is 'PAS bed number';
comment on column MED_PROFILE_ORDER.PAS_WARD_CODE is 'PAS ward code';
comment on column MED_PROFILE_ORDER.ORDER_DIGEST is 'Message digest of CMS order';
comment on column MED_PROFILE_ORDER.HOSP_CODE is 'Hospital code';
comment on column MED_PROFILE_ORDER.WORKSTORE_CODE is 'Workstore code';
comment on column MED_PROFILE_ORDER.WARD_CODE is 'Ward code';
comment on column MED_PROFILE_ORDER.SPEC_CODE is 'Specialty code';
comment on column MED_PROFILE_ORDER.PAT_CAT_CODE is 'Patient category code';
comment on column MED_PROFILE_ORDER.PRIVATE_FLAG is 'Private flag';
comment on column MED_PROFILE_ORDER.BODY_WEIGHT is 'Body weight';
comment on column MED_PROFILE_ORDER.BODY_HEIGHT is 'Body height';
comment on column MED_PROFILE_ORDER.BODY_INFO_UPDATE_DATE is 'Last update date of body information';
comment on column MED_PROFILE_ORDER.ORDER_DATE is 'Order Date';    
comment on column MED_PROFILE_ORDER.CREATE_USER is 'Created user';
comment on column MED_PROFILE_ORDER.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_ORDER.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE_ORDER.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE_ORDER.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_PO_ITEM (
    ID number(19) not null,
    ITEM_CODE varchar2(6) not null,
    REGIMEN_XML clob null,
    DOSE_QTY number(19,4) null,
    DOSE_UNIT varchar2(15) null,
    CAL_QTY number(19,4) not null,
    ADJ_QTY number(19,4) not null,
    ISSUE_QTY number(19,4) not null,
    BASE_UNIT varchar2(4) null,
    DUE_DATE timestamp null,
    LAST_DUE_DATE timestamp null,
    REFILL_DURATION number(10) null,
    REFILL_DURATION_UNIT varchar2(1) null,
    DRUG_NAME varchar2(57) null,
    FORM_CODE varchar2(3) null,
    FORM_LABEL_DESC varchar2(30) null,
    STRENGTH varchar2(59) null,
    VOLUME_TEXT varchar2(23) null,
    DANGER_DRUG_FLAG number(1) default 0 not null,
    WARD_STOCK_FLAG number(1) default 0 not null,
    WARN_CODE_1 varchar2(2) null,
    WARN_CODE_2 varchar2(2) null,
    WARN_CODE_3 varchar2(2) null,
    WARN_CODE_4 varchar2(2) null,
    REMARK_TEXT varchar2(125) null,
    DRUG_SYNONYM varchar2(46) null,
    RE_DISP_FLAG number(1) default 0 not null,
    STAT_FLAG number(1) default 0 not null,
    CHARGE_SPEC_CODE varchar2(4) null,
    SINGLE_DISP_FLAG number(1) default 0 not null,
    DIRECT_PRINT_FLAG number(1) default 0 not null,
    ACTION_STATUS varchar2(255) not null,
    FM_STATUS varchar2(1) null,
    ITEM_NUM number(10) null,
    LAST_PRINT_DATE timestamp null,
    NUM_OF_LABEL number(1) not null,
    MO_ISSUE_QTY number(10) null,
    MO_BASE_UNIT varchar2(4) null,
    LAST_ADJ_QTY number(19,4) null,
    ADDITIVE_NUM number(10) null,
    FLUID_NUM number(10) null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    HOSP_CODE varchar2(3) not null,
    WORKSTORE_CODE varchar2(4) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_MED_PROFILE_PO_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  MED_PROFILE_PO_ITEM is 'Pharmacy order items of medication profiles';
comment on column MED_PROFILE_PO_ITEM.ID is 'Pharmacy order item id (medication profile)';
comment on column MED_PROFILE_PO_ITEM.ITEM_CODE is 'Item code';
comment on column MED_PROFILE_PO_ITEM.REGIMEN_XML is 'Regimen content in XML format';
comment on column MED_PROFILE_PO_ITEM.DOSE_QTY is 'Dose quantity';
comment on column MED_PROFILE_PO_ITEM.DOSE_UNIT is 'Dose unit';
comment on column MED_PROFILE_PO_ITEM.CAL_QTY is 'Calculated quantity';
comment on column MED_PROFILE_PO_ITEM.ADJ_QTY is 'Adjusted quantity';
comment on column MED_PROFILE_PO_ITEM.ISSUE_QTY is 'Issued quantity';
comment on column MED_PROFILE_PO_ITEM.BASE_UNIT is 'Base unit';
comment on column MED_PROFILE_PO_ITEM.DUE_DATE is 'Due date';
comment on column MED_PROFILE_PO_ITEM.LAST_DUE_DATE is 'Last due date';
comment on column MED_PROFILE_PO_ITEM.REFILL_DURATION is 'Refill duration';
comment on column MED_PROFILE_PO_ITEM.REFILL_DURATION_UNIT is 'Unit of refill duration';
comment on column MED_PROFILE_PO_ITEM.DRUG_NAME is 'Drug name';
comment on column MED_PROFILE_PO_ITEM.FORM_CODE is 'Form code';
comment on column MED_PROFILE_PO_ITEM.FORM_LABEL_DESC is 'Form description shown on label';
comment on column MED_PROFILE_PO_ITEM.STRENGTH is 'Strength';
comment on column MED_PROFILE_PO_ITEM.VOLUME_TEXT is 'Volume text';
comment on column MED_PROFILE_PO_ITEM.DANGER_DRUG_FLAG is 'Dangerous drug';
comment on column MED_PROFILE_PO_ITEM.WARD_STOCK_FLAG is 'Ward stock item';
comment on column MED_PROFILE_PO_ITEM.WARN_CODE_1 is 'Warning code 1';
comment on column MED_PROFILE_PO_ITEM.WARN_CODE_2 is 'Warning code 2';
comment on column MED_PROFILE_PO_ITEM.WARN_CODE_3 is 'Warning code 3';
comment on column MED_PROFILE_PO_ITEM.WARN_CODE_4 is 'Warning code 4';
comment on column MED_PROFILE_PO_ITEM.REMARK_TEXT is 'Remark';
comment on column MED_PROFILE_PO_ITEM.DRUG_SYNONYM is 'Drug synonym';
comment on column MED_PROFILE_PO_ITEM.RE_DISP_FLAG is 'Re-dispensed';
comment on column MED_PROFILE_PO_ITEM.STAT_FLAG is 'STAT then item';
comment on column MED_PROFILE_PO_ITEM.CHARGE_SPEC_CODE is 'Charging specialty code';
comment on column MED_PROFILE_PO_ITEM.SINGLE_DISP_FLAG is 'Single dispensed';
comment on column MED_PROFILE_PO_ITEM.DIRECT_PRINT_FLAG is 'Direct printed';
comment on column MED_PROFILE_PO_ITEM.ACTION_STATUS is 'Action status';
comment on column MED_PROFILE_PO_ITEM.FM_STATUS is 'Formulary management status';
comment on column MED_PROFILE_PO_ITEM.ITEM_NUM is 'Item number';
comment on column MED_PROFILE_PO_ITEM.LAST_PRINT_DATE is 'Last printed date';
comment on column MED_PROFILE_PO_ITEM.NUM_OF_LABEL is 'Number of label(s)';
comment on column MED_PROFILE_PO_ITEM.MO_ISSUE_QTY is 'Issued quantity of medication order';
comment on column MED_PROFILE_PO_ITEM.MO_BASE_UNIT is 'Base unit of medication order';
comment on column MED_PROFILE_PO_ITEM.LAST_ADJ_QTY is 'Last adjusted quantity';
comment on column MED_PROFILE_PO_ITEM.ADDITIVE_NUM is 'Additive Number';
comment on column MED_PROFILE_PO_ITEM.FLUID_NUM is 'Fluid Number';
comment on column MED_PROFILE_PO_ITEM.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column MED_PROFILE_PO_ITEM.HOSP_CODE is 'Hospital code';
comment on column MED_PROFILE_PO_ITEM.WORKSTORE_CODE is 'Workstore code';
comment on column MED_PROFILE_PO_ITEM.CREATE_USER is 'Created user';
comment on column MED_PROFILE_PO_ITEM.CREATE_DATE is 'Created date';
comment on column MED_PROFILE_PO_ITEM.UPDATE_USER is 'Last updated user';
comment on column MED_PROFILE_PO_ITEM.UPDATE_DATE is 'Last updated date';
comment on column MED_PROFILE_PO_ITEM.VERSION is 'Version to serve as optimistic lock value';


create table MED_PROFILE_STAT (
    ID number(19) not null,
    ORDER_TYPE varchar2(1) not null,
    ADMIN_TYPE varchar2(1) not null,
    DUE_DATE timestamp null,
    FIRST_ITEM_DATE timestamp null,
    LAST_ITEM_DATE timestamp null,
    URGENT_FLAG number(1) default 0 not null,
    CHARGE_TYPE varchar2(1) not null,
    REPLENISH_FLAG number(1) default 0 not null,
    AMEND_FLAG number(1) default 0 not null,
    CMM_FLAG number(1) default 0 not null,
    SUSPEND_FLAG number(1) default 0 not null,
    PENDING_FLAG number(1) default 0 not null,
    MDS_ERROR_FLAG number(1) default 0 not null,
    WARD_STOCK_FLAG number(1) default 0 not null,
    SELF_OWN_FLAG number(1) default 0 not null,
    REFILL_EXCEPT_FLAG number(1) default 0 not null,
    MED_PROFILE_ID number(19) not null,
    constraint PK_MED_PROFILE_STAT primary key (ID) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  MED_PROFILE_STAT is 'Consolidated statistics of medication profile statuses';
comment on column MED_PROFILE_STAT.MED_PROFILE_ID is 'Medication profile id';
comment on column MED_PROFILE_STAT.ID is 'Medication profile statistic';
comment on column MED_PROFILE_STAT.ORDER_TYPE is 'Order type';
comment on column MED_PROFILE_STAT.ADMIN_TYPE is 'Pharmacy admin type';
comment on column MED_PROFILE_STAT.DUE_DATE is 'Due date';
comment on column MED_PROFILE_STAT.FIRST_ITEM_DATE is 'First item arrival date';
comment on column MED_PROFILE_STAT.LAST_ITEM_DATE is 'Last item arrival date';
comment on column MED_PROFILE_STAT.URGENT_FLAG is 'Contain urgent item(s)';
comment on column MED_PROFILE_STAT.CHARGE_TYPE is 'Charge type';
comment on column MED_PROFILE_STAT.REPLENISH_FLAG is 'Contain replenishment item(s)';
comment on column MED_PROFILE_STAT.AMEND_FLAG is 'Contain amended item(s)';
comment on column MED_PROFILE_STAT.CMM_FLAG is 'Contain CMM item(s)';
comment on column MED_PROFILE_STAT.SUSPEND_FLAG is 'Contain suspend item(s)';
comment on column MED_PROFILE_STAT.PENDING_FLAG is 'Contain pending item(s)';
comment on column MED_PROFILE_STAT.MDS_ERROR_FLAG is 'Contain MDS error item(s)';
comment on column MED_PROFILE_STAT.WARD_STOCK_FLAG is 'Contain ward stock item(s)';
comment on column MED_PROFILE_STAT.SELF_OWN_FLAG is 'Contain self-own item(s)';
comment on column MED_PROFILE_STAT.REFILL_EXCEPT_FLAG is 'Contain refill exception';


create table REPLENISHMENT (
    ID number(19) not null,
    REPLENISH_NUM number(10) not null,
    REQUEST_DATE timestamp not null,
    REASON varchar2(255) null,
    STATUS varchar2(1) not null,
    MED_PROFILE_ID number(19) null,
    MED_PROFILE_MO_ITEM_ID number(19) not null,
    MED_PROFILE_ORDER_ID number(19) null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REPLENISHMENT primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  REPLENISHMENT is 'Replenishment requests of medication profiles';
comment on column REPLENISHMENT.ID is 'Replenishment id';
comment on column REPLENISHMENT.REPLENISH_NUM is 'Replenishment number';
comment on column REPLENISHMENT.REQUEST_DATE is 'Request date';
comment on column REPLENISHMENT.REASON is 'Reason';
comment on column REPLENISHMENT.STATUS is 'Record status';
comment on column REPLENISHMENT.MED_PROFILE_ID is 'Medication profile id';
comment on column REPLENISHMENT.MED_PROFILE_MO_ITEM_ID is 'Medication order item id (medication profile)';
comment on column REPLENISHMENT.MED_PROFILE_ORDER_ID is 'Medication profile order id';
comment on column REPLENISHMENT.CREATE_USER is 'Created user';
comment on column REPLENISHMENT.CREATE_DATE is 'Created date';
comment on column REPLENISHMENT.UPDATE_USER is 'Last updated user';
comment on column REPLENISHMENT.UPDATE_DATE is 'Last updated date';
comment on column REPLENISHMENT.VERSION is 'Version to serve as optimistic lock value';


create table REPLENISHMENT_ITEM (
    ID number(19) not null,
    ISSUE_QTY number(19,4) null,
    DIRECT_PRINT_FLAG number(1) default 0 not null,
    DUE_DATE timestamp null,
    PRINT_DATE timestamp null,
    REPLENISHMENT_ID number(19) not null,
    MED_PROFILE_PO_ITEM_ID number(19) not null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_REPLENISHMENT_ITEM primary key (ID) using index tablespace PMS_INDX_01)
partition by range(CREATE_DATE) (
    partition P_PMS_MP_DATA_2012 values less than (to_date('20130101','YYYYMMDD')) tablespace P_PMS_MP_DATA_2012);

comment on table  REPLENISHMENT_ITEM is 'Replenishment request items (medication profile)';
comment on column REPLENISHMENT_ITEM.ID is 'Replenishment item id';
comment on column REPLENISHMENT_ITEM.ISSUE_QTY is 'Issued quantity';
comment on column REPLENISHMENT_ITEM.DIRECT_PRINT_FLAG is 'Direct printed';
comment on column REPLENISHMENT_ITEM.DUE_DATE is 'Due date';
comment on column REPLENISHMENT_ITEM.PRINT_DATE is 'Printed date';
comment on column REPLENISHMENT_ITEM.REPLENISHMENT_ID is 'Replenishment id';
comment on column REPLENISHMENT_ITEM.MED_PROFILE_PO_ITEM_ID is 'Pharmacy order item id (medication profile)';
comment on column REPLENISHMENT_ITEM.CREATE_USER is 'Created user';
comment on column REPLENISHMENT_ITEM.CREATE_DATE is 'Created date';
comment on column REPLENISHMENT_ITEM.UPDATE_USER is 'Last updated user';
comment on column REPLENISHMENT_ITEM.UPDATE_DATE is 'Last updated date';
comment on column REPLENISHMENT_ITEM.VERSION is 'Version to serve as optimistic lock value';

--//@UNDO
drop table DELIVERY;
drop table DELIVERY_ITEM;
drop table DELIVERY_REQUEST;
drop table DELIVERY_REQUEST_ALERT;
drop table MED_PROFILE;
drop table MED_PROFILE_ERROR_LOG;
drop table MED_PROFILE_ERROR_STAT;
drop table MED_PROFILE_ITEM;
drop table MED_PROFILE_MO_ITEM;
drop table MED_PROFILE_MO_ITEM_ALERT;
drop table MED_PROFILE_MO_ITEM_FM;
drop table MED_PROFILE_ORDER;
drop table MED_PROFILE_PO_ITEM;
drop table MED_PROFILE_STAT;
drop table REPLENISHMENT;
drop table REPLENISHMENT_ITEM;
--//