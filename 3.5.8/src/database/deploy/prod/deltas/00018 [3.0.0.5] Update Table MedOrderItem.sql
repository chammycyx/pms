alter table MED_ORDER_ITEM modify UNIT_OF_CHARGE not null;

alter table MED_ORDER_ITEM add CMS_CONFIRM_CHARGE_FLAG varchar2(1) null;
alter table MED_ORDER_ITEM add CMS_CONFIRM_UNIT_OF_CHARGE number(10) null;

comment on column MED_ORDER_ITEM.CMS_CONFIRM_CHARGE_FLAG is 'CMS confirm charging required';
comment on column MED_ORDER_ITEM.CMS_CONFIRM_UNIT_OF_CHARGE is 'CMS confirm unit(s) of charge';

update MED_ORDER_ITEM set CMS_CONFIRM_CHARGE_FLAG = 'N'; 
update MED_ORDER_ITEM set CMS_CONFIRM_UNIT_OF_CHARGE = 0; 

alter table MED_ORDER_ITEM modify CMS_CONFIRM_CHARGE_FLAG not null;
alter table MED_ORDER_ITEM modify CMS_CONFIRM_UNIT_OF_CHARGE not null;


--//@UNDO
alter table MED_ORDER_ITEM modify UNIT_OF_CHARGE null;

alter table MED_ORDER_ITEM drop column CMS_CONFIRM_CHARGE_FLAG;
alter table MED_ORDER_ITEM drop column CMS_CONFIRM_UNIT_OF_CHARGE;
--//