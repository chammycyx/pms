insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG,REMINDER_MESSAGE) 
values (32039,'vetting.oneStopData.captured','Capture one stop entry data in advance during Vetting save','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,0,null,'S',null,0,null);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,32039,'Y',32039,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');


--//@UNDO
delete from WORKSTORE_PROP where PROP_ID=32039;

delete from PROP where ID=32039;
--//