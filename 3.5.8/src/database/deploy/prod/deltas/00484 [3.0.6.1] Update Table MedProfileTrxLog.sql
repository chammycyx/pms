alter table MED_PROFILE_TRX_LOG add CREATE_USER varchar(12);
alter table MED_PROFILE_TRX_LOG add WORKSTATION_ID number(19);
alter table MED_PROFILE_TRX_LOG modify REF_NUM null;

comment on column MED_PROFILE_TRX_LOG.CREATE_USER is 'Created user';
comment on column MED_PROFILE_TRX_LOG.WORKSTATION_ID is 'Workstation id';


--//@UNDO

alter table MED_PROFILE_TRX_LOG drop column CREATE_USER;
alter table MED_PROFILE_TRX_LOG drop column WORKSTATION_ID;
delete from MED_PROFILE_TRX_LOG where REF_NUM is null;
alter table MED_PROFILE_TRX_LOG modify REF_NUM not null;
--//