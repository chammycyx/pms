alter table WARD_CONFIG add ATDPS_WARD_FLAG number(1);
update WARD_CONFIG set ATDPS_WARD_FLAG = 0;
alter table WARD_CONFIG modify ATDPS_WARD_FLAG number(1) not null;

comment on column WARD_CONFIG.ATDPS_WARD_FLAG is 'Atdps Ward Flag';

alter table DELIVERY_ITEM add BIN_NUM varchar2(4);
comment on column DELIVERY_ITEM.BIN_NUM is 'Bin number';

alter table MED_PROFILE_ITEM add VERIFY_USER varchar2(12);
comment on column MED_PROFILE_ITEM.VERIFY_USER is 'Verify user';

--//@UNDO
alter table WARD_CONFIG drop column ATDPS_WARD_FLAG;
alter table DELIVERY_ITEM drop column BIN_NUM;
alter table MED_PROFILE_ITEM drop column VERIFY_USER;
--//