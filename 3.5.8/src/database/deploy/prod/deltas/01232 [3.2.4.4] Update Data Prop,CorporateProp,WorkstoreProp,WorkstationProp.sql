update PROP set DESCRIPTION='Issue window number of fast queue in Checking', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=15005;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (16005,'checkIssue.uncollect.issueWindowNum','Issue window number for Uncollect in Checking and Issuing','0-20','I',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'O',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,16005,'0',16005,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (16006,'checkIssue.uncollect.reverse.popup','Popup reverse Uncollect in Checking and Issuing','Y,N','B',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,'Y','S',null,0);

insert all into WORKSTATION_PROP (ID,PROP_ID,VALUE,SORT_SEQ,WORKSTATION_ID,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTATION_PROP.nextval,16006,'Y',16006,ID,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select ID from WORKSTATION);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (16007,'checkIssue.uncollect.order.suspendCode','Suspend code of order for Uncollect in Checking and Issuing','0-2','S',1,0,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,16007,'UC',16007,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

update PROP set DESCRIPTION='Suspend code of order for Uncollect in One Stop Entry', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=22014;
update PROP set DESCRIPTION='Require suspend code of order for Uncollect in One Stop Entry', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=22015;

update PROP set TYPE='kk', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=27004;
update PROP set NAME='report.waitingTime.current.criteria.order.max', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=27015;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27016,'report.waitingTime.duration.exclusion','Excluded duration (in minutes) for Waiting Time calculation','180-480','mm',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (27016,27016,'180',27016,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27017,'report.waitingTime.audit.startHour','Start hour for EDS Waiting Time Audit Report','0-23','HH',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (27017,27017,'9',27017,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27018,'report.waitingTime.audit.endHour','End hour for EDS Waiting Time Audit Report','1-24','kk',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (27018,27018,'17',27018,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27019,'report.waitingTime.audit.startDayOfWeek','Start day number of the week for EDS Waiting Time Audit Report','1-7','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (27019,27019,'1',27019,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27020,'report.waitingTime.audit.endDayOfWeek','End day number of the week for EDS Waiting Time Audit Report','1-7','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert into CORPORATE_PROP (ID,SORT_SEQ,VALUE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (27020,27020,'5',27020,'pmsadmin',sysdate,'pmsadmin',sysdate,1);

update PROP set DESCRIPTION='Duration (in days) of day end orders to alert in Uncollect for Drug Charging', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=31001;
update PROP set DESCRIPTION='Duration (in days) of day end orders for Uncollect for Drug Charging', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=31002;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (32046,'vetting.medProfile.atdps.refill.duration','Refill duration (in days) for ATDPS item in IP Vetting','1-7','dd',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'I',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,32046,'1',32046,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS='A');

--//@UNDO
update PROP set DESCRIPTION='Issue window number of fast queue for Checking', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=15005;

update PROP set DESCRIPTION='Suspend code of order for Uncollect', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=22014;
update PROP set DESCRIPTION='Require suspend code of order for Uncollect', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=22015;

update PROP set TYPE='HH', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=27004;
update PROP set NAME='report.currentWaitingTime.current.criteria.order.max', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=27015;

update PROP set DESCRIPTION='Duration (in days) of day end orders to alert in Uncollect', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=31001;
update PROP set DESCRIPTION='Duration (in days) of day end orders for Uncollect', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=31002;

delete from WORKSTORE_PROP where PROP_ID in (16005, 16007, 32046);
delete from WORKSTATION_PROP where PROP_ID in (16006);
delete from CORPORATE_PROP where PROP_ID in (27016, 27017, 27018, 27019, 27020);

delete from PROP where ID in (16005, 16006, 16007, 27016, 27017, 27018, 27019, 27020, 32046);
--//