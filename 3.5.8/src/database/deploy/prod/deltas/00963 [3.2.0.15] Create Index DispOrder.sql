create index I_DISP_ORDER_16 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, TICKET_CREATE_DATE) local online;
create index I_DISP_ORDER_17 on DISP_ORDER (HOSP_CODE, WORKSTORE_CODE, VET_DATE) local online;

--//@UNDO
drop index I_DISP_ORDER_16;
drop index I_DISP_ORDER_17;
--//