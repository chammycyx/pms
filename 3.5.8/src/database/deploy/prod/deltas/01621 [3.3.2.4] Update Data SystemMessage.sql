insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0984',null,'en_US',null,1007,'I',null,null,0,null,null,null,0,sysdate,null,null,null,'eHR patient alert profile enquiry service is not available.','Please exercise your professional judgement during the downtime period and contact IT system support for assistance.',sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0985',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Please input item code or manufacturer code if batch no. is specified.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

update SYSTEM_MESSAGE set MAIN_MSG = 'HA patient alert profile enquiry service is not available.  Therefore, the system cannot perform Allergy, ADR and G6PD Deficiency Contraindication checking.' where MESSAGE_CODE = '0592';
update SYSTEM_MESSAGE set MAIN_MSG = 'PIVAS satellite option is off, it is not allow to set satellite supply schedule.' where MESSAGE_CODE = '0813';

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0984';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0985';

update SYSTEM_MESSAGE set MAIN_MSG = 'Patient alert profile enquiry service is not available.  Therefore, the system cannot perform Allergy, ADR and G6PD Deficiency Contraindication checking.' where MESSAGE_CODE = '0592';
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid solvent code.' where MESSAGE_CODE = '0813';
--//