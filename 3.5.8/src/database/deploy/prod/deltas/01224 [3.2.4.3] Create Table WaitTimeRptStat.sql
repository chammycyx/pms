create table WAIT_TIME_RPT_STAT (
                ID number(19) not null,
                HOSP_CODE varchar2(3) not null,
                WORKSTORE_CODE varchar2(4) not null,
                YEAR number(10) not null,
                MONTH number(10) not null,
  DAY number(10) not null,
  HOUR number(10) not null,
  DAY_OF_WEEK number(10) not null,
  WAIT_TIME number(10) not null,
                CREATE_DATE timestamp not null,
                constraint PK_WAIT_TIME_RPT_STAT primary key (ID) using index tablespace PMS_INDX_01,
                constraint UI_WAIT_TIME_RPT_STAT_01 unique (HOSP_CODE, WORKSTORE_CODE, YEAR, MONTH, DAY, HOUR) using index tablespace PMS_INDX_01)
tablespace PMS_DATA_01;

comment on table  WAIT_TIME_RPT_STAT is 'Consolidated waiting time for report usage';
comment on column WAIT_TIME_RPT_STAT.ID is 'Wait time report statistic id';
comment on column WAIT_TIME_RPT_STAT.HOSP_CODE is 'Hospital code';
comment on column WAIT_TIME_RPT_STAT.WORKSTORE_CODE is 'Workstore code';
comment on column WAIT_TIME_RPT_STAT.YEAR is 'Statistical year';
comment on column WAIT_TIME_RPT_STAT.MONTH is 'Statistical month';
comment on column WAIT_TIME_RPT_STAT.DAY is 'Statistical day';
comment on column WAIT_TIME_RPT_STAT.HOUR is 'Statistical hour';
comment on column WAIT_TIME_RPT_STAT.DAY_OF_WEEK is 'Statistical day of week';
comment on column WAIT_TIME_RPT_STAT.WAIT_TIME is 'Waiting time';

create sequence SQ_WAIT_TIME_RPT_STAT increment by 50 start with 10000000000049;

--//@UNDO
drop sequence SQ_WAIT_TIME_RPT_STAT;

drop table WAIT_TIME_RPT_STAT;
--//
