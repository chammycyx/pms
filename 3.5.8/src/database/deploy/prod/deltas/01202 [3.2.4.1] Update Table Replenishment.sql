alter table REPLENISHMENT add DOSE_COUNT number(10) null;

comment on column REPLENISHMENT.DOSE_COUNT is 'Dose count';

--//@UNDO
alter table REPLENISHMENT drop column DOSE_COUNT;
--//