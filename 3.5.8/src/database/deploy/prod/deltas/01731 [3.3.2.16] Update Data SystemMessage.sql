update SYSTEM_MESSAGE set MAIN_MSG = 'Due date is set before supply / current time.  Confirm?', SEVERITY_CODE = 'Q' where MESSAGE_CODE = '0935';
update SYSTEM_MESSAGE set MAIN_MSG = 'Due date is set before supply / current time' where MESSAGE_CODE = '0961';
update SYSTEM_MESSAGE set MAIN_MSG = 'Pivas Batch Preparation|Reprint|LotNum [#0]|ReprintNumOfLabel [#1]|ReprintType [#2]|' where MESSAGE_CODE = '0898';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('1007',null,'en_US',null,1007,'W',null,null,0,null,null,null,0,sysdate,null,null,null,'Number of label must be greater than zero.',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG = 'Due date should be later than or equal to supply buffer date', SEVERITY_CODE = 'W' where MESSAGE_CODE = '0935';
update SYSTEM_MESSAGE set MAIN_MSG = 'Supply date is set after the due date' where MESSAGE_CODE = '0961';
update SYSTEM_MESSAGE set MAIN_MSG = 'Pivas Batch Preparation|Reprint|LotNum[#0]|' where MESSAGE_CODE = '0898';

delete SYSTEM_MESSAGE where MESSAGE_CODE = '1007';
--//