alter table DELIVERY_ITEM add PRE_ISSUE_QTY number(19,4) null;

comment on column DELIVERY_ITEM.PRE_ISSUE_QTY is 'Preliminary issue quantity';

--//@UNDO
alter table DELIVERY_ITEM drop column PRE_ISSUE_QTY;
--//

