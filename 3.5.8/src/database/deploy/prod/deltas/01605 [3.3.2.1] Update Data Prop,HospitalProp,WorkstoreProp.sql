update PROP set DESCRIPTION='Check MDS in IP Vetting, IP Manual Profile Entry', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32035;

update PROP set DESCRIPTION='Current batch number for normal Delivery', VALIDATION='1-7999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37003;
update PROP set DESCRIPTION='Maximum batch number for normal Delivery', VALIDATION='1-7999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37004;
update PROP set DESCRIPTION='Minimum batch number for normal Delivery', VALIDATION='1-7999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37005;

update HOSPITAL_PROP set VALUE='7999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37004;

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27023,'report.drugRefillList.dosageInstruction.visible','Show dosage instruction in Drug Refill List','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,27023,VALUE,27023,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE,VALUE from WORKSTORE_PROP where PROP_ID=27022);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (27024,'report.patientDrugProfileList.dosageInstruction.visible','Show dosage instruction in Patient Drug Profile List','Y,N','B',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,27024,VALUE,27024,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE,VALUE from WORKSTORE_PROP where PROP_ID=27022);

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37013,'delivery.purchaseRequest.batchNum','Current batch number for purchase request Delivery','8000-8999','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37013,'8000',HOSP_CODE,37013,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37014,'delivery.purchaseRequest.batchNum.min','Minimum batch number for purchase request Delivery','8000-8999','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37014,'8000',HOSP_CODE,37014,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37015,'delivery.purchaseRequest.batchNum.max','Maximum batch number for purchase request Delivery','8000-8999','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37015,'8999',HOSP_CODE,37015,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37016,'delivery.pivas.batchNum','Current batch number for PIVAS Delivery','9000-9999','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'-',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37016,'9000',HOSP_CODE,37016,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37017,'delivery.pivas.batchNum.min','Minimum batch number for PIVAS Delivery','9000-9999','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37017,'9000',HOSP_CODE,37017,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) 
values (37018,'delivery.pivas.batchNum.max','Maximum batch number for PIVAS Delivery','9000-9999','I',0,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into HOSPITAL_PROP (ID,SORT_SEQ,VALUE,HOSP_CODE,PROP_ID,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION) 
values (SQ_HOSPITAL_PROP.nextval,37018,'9999',HOSP_CODE,37018,'pmsadmin',sysdate,'pmsadmin',sysdate,1) 
(select HOSP_CODE from HOSPITAL where STATUS = 'A');

--//@UNDO
update PROP set DESCRIPTION='Check MDS in IP Vetting', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=32035;

update PROP set DESCRIPTION='Current batch number for Delivery', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37003;
update PROP set DESCRIPTION='Maximum batch number for Delivery', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37004;
update PROP set DESCRIPTION='Minimum batch number for Delivery', VALIDATION='0-9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37005;

update HOSPITAL_PROP set VALUE='9999', UPDATE_USER='pmsadmin', UPDATE_DATE=sysdate where ID=37004;

delete from WORKSTORE_PROP where PROP_ID in (27023, 27024);

delete from HOSPITAL_PROP where PROP_ID in (37013, 37014, 37015, 37016, 37017, 37018);

delete from PROP where ID in (27023, 27024, 37013, 37014, 37015, 37016, 37017, 37018);
--//