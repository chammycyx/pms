alter table BATCH_ISSUE_LOG_DETAIL add SORT_SEQ number(10) null;

comment on column BATCH_ISSUE_LOG_DETAIL.SORT_SEQ is 'Sort sequence';

--//@UNDO
alter table BATCH_ISSUE_LOG_DETAIL drop column SORT_SEQ;
--//
