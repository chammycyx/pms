alter table MED_PROFILE_ITEM add GROUP_NUM number(10);

comment on column MED_PROFILE_ITEM.GROUP_NUM is 'Group Number';

--//@UNDO
alter table MED_PROFILE_ITEM drop column GROUP_NUM;
--//