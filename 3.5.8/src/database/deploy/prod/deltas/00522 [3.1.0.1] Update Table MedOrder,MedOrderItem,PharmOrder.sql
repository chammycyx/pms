alter table MED_ORDER add URGENT_FLAG number(1) default 0;
alter table MED_ORDER add PRE_VET_USER varchar2(12);
alter table MED_ORDER add PRE_VET_DATE timestamp;

alter table MED_ORDER_ITEM add PRE_VET_NOTE_TEXT varchar2(255);
alter table MED_ORDER_ITEM add PRE_VET_NOTE_USER varchar2(12);
alter table MED_ORDER_ITEM add PRE_VET_NOTE_DATE timestamp;

alter table PHARM_ORDER add URGENT_FLAG number(1) default 0;


comment on column MED_ORDER.URGENT_FLAG is 'Urgent flag for PreVet';
comment on column MED_ORDER.PRE_VET_USER is 'PreVet User';
comment on column MED_ORDER.PRE_VET_DATE is 'PreVet Date';

comment on column MED_ORDER_ITEM.PRE_VET_NOTE_TEXT is 'PreVet Note';
comment on column MED_ORDER_ITEM.PRE_VET_NOTE_USER is 'PreVet Note User';
comment on column MED_ORDER_ITEM.PRE_VET_NOTE_DATE is 'PreVet Note Date';

comment on column PHARM_ORDER.URGENT_FLAG is 'Urgent flag for PreVet';



--//@UNDO

alter table MED_ORDER drop column URGENT_FLAG;
alter table MED_ORDER drop column PRE_VET_USER;
alter table MED_ORDER drop column PRE_VET_DATE;

alter table MED_ORDER_ITEM drop column PRE_VET_NOTE_TEXT;
alter table MED_ORDER_ITEM drop column PRE_VET_NOTE_USER;
alter table MED_ORDER_ITEM drop column PRE_VET_NOTE_DATE; 

alter table PHARM_ORDER drop column URGENT_FLAG;
--//

