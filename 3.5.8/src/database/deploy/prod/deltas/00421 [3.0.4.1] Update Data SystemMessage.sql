update SYSTEM_MESSAGE set MAIN_MSG = 'The unsaved replenishment request processing of the same order will be cancelled after saving pharmacy item(s), continue to save?' where MESSAGE_CODE = '0308';
update SYSTEM_MESSAGE set SUPPL_MSG = 'Please call IT support hotline (9038 2477) for handling the error case(s).<br><br>No label will be generated for this process.<br><br>Please exclude the error cases in your generation criteria and generate the non-problematic cases again.' where MESSAGE_CODE = '0667';

insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0669',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'You are discarding all your changes and going back to Pharmacy Inbox.  Confirm?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');
insert into SYSTEM_MESSAGE (MESSAGE_CODE,DETAIL,LOCALE,BTN_YES_SIZE,APPLICATION_ID,SEVERITY_CODE,EFFECTIVE,EXPIRY,VERSION,BTN_NO_SHORTCUT,BTN_NO_SIZE,BTN_CANCEL_SIZE,FUNCTION_ID,CREATE_DATE,BTN_CANCEL_SHORTCUT,BTN_YES_SHORTCUT,BTN_CANCEL_CAPTION,MAIN_MSG,SUPPL_MSG,UPDATE_DATE,OPERATION_ID,CREATE_USER,BTN_YES_CAPTION,BTN_NO_CAPTION,UPDATE_USER) values ('0670',null,'en_US',null,1007,'Q',null,null,0,null,null,null,0,sysdate,null,null,null,'Annotation created by pharmacy will be visible to clinician.  Do you want to proceed?',null,sysdate,null,'pmsadmin',null,null,'pmsadmin');

--//@UNDO
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0669';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0670';
--//