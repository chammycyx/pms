insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (30015,'ticketGen.queue1.name','Name of Ticket Generation queue 1','1-20','S',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (30016,'ticketGen.queue2.name','Name of Ticket Generation queue 2','1-20','S',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);
insert into PROP (ID,NAME,DESCRIPTION,VALIDATION,TYPE,CACHE_IN_SESSION_FLAG,CACHE_EXPIRE_TIME,CREATE_USER,CREATE_DATE,UPDATE_USER,UPDATE_DATE,VERSION,DEF_VALUE,MAINT_SCREEN,HIDE_IN_OPERATION_PROFILE_TYPE,SYNC_FLAG) values (30017,'ticketGen.queue3.name','Name of Ticket Generation queue 3','1-20','S',1,60,'pmsadmin',sysdate,'pmsadmin',sysdate,1,null,'S',null,0);

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,30015,'Public Patient',30015,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,30016,'Private Patient',30016,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

insert all into WORKSTORE_PROP (ID,HOSP_CODE,WORKSTORE_CODE,PROP_ID,VALUE,SORT_SEQ,CREATE_USER,CREATE_DATE,UPDATE_DATE,UPDATE_USER,VERSION) 
values (SQ_WORKSTORE_PROP.nextval,HOSP_CODE,WORKSTORE_CODE,30017,'SFI Patient',30017,'pmsadmin',sysdate,sysdate,'pmsadmin',1) 
(select HOSP_CODE,WORKSTORE_CODE from WORKSTORE where STATUS = 'A');

update PROP set NAME = 'ticketGen.queue1.ticket.ticketNum', DESCRIPTION = 'Current ticket number for Ticket Generation queue 1' where ID = 30005;
update PROP set NAME = 'ticketGen.queue1.ticket.ticketNum.max', DESCRIPTION = 'Maximum ticket number for Ticket Generation queue 1' where ID = 30006;
update PROP set NAME = 'ticketGen.queue1.ticket.ticketNum.min', DESCRIPTION = 'Minimum ticket number for Ticket Generation queue 1' where ID = 30007;
update PROP set NAME = 'ticketGen.queue2.ticket.ticketNum', DESCRIPTION = 'Current ticket number for Ticket Generation queue 2' where ID = 30008;
update PROP set NAME = 'ticketGen.queue2.ticket.ticketNum.max', DESCRIPTION = 'Maximum ticket number for Ticket Generation queue 2' where ID = 30009;
update PROP set NAME = 'ticketGen.queue2.ticket.ticketNum.min', DESCRIPTION = 'Minimum ticket number for Ticket Generation queue 2' where ID = 30010;
update PROP set NAME = 'ticketGen.queue3.ticket.ticketNum', DESCRIPTION = 'Current ticket number for Ticket Generation queue 3' where ID = 30011;
update PROP set NAME = 'ticketGen.queue3.ticket.ticketNum.max', DESCRIPTION = 'Maximum ticket number for Ticket Generation queue 3' where ID = 30012;
update PROP set NAME = 'ticketGen.queue3.ticket.ticketNum.min', DESCRIPTION = 'Minimum ticket number for Ticket Generation queue 3' where ID = 30013;

--Swap Data BETWEEN public and private patient
alter table WORKSTORE_PROP add TEMP_VALUE varchar2(1000) null;

update WORKSTORE_PROP set TEMP_VALUE = VALUE where PROP_ID between 30005 and 30010;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30008 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30005;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30009 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30006;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30010 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30007;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30005 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30008;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30006 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30009;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30007 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30010;

alter table WORKSTORE_PROP drop column TEMP_VALUE;

--//@UNDO
delete from WORKSTORE_PROP where PROP_ID=30015;
delete from WORKSTORE_PROP where PROP_ID=30016;
delete from WORKSTORE_PROP where PROP_ID=30017;

delete from PROP where ID=30015;
delete from PROP where ID=30016;
delete from PROP where ID=30017;

update PROP set NAME = 'ticketGen.private.ticket.ticketNum', DESCRIPTION = 'Current ticket number for private patient' where ID = 30005;
update PROP set NAME = 'ticketGen.private.ticket.ticketNum.max', DESCRIPTION = 'Maximum ticket number for private patient' where ID = 30006;
update PROP set NAME = 'ticketGen.private.ticket.ticketNum.min', DESCRIPTION = 'Minimum ticket number for private patient' where ID = 30007;
update PROP set NAME = 'ticketGen.public.ticket.ticketNum', DESCRIPTION = 'Current ticket number for public patient' where ID = 30008;
update PROP set NAME = 'ticketGen.public.ticket.ticketNum.max', DESCRIPTION = 'Maximum ticket number for public patient' where ID = 30009;
update PROP set NAME = 'ticketGen.public.ticket.ticketNum.min', DESCRIPTION = 'Minimum ticket number for public patient' where ID = 30010;
update PROP set NAME = 'ticketGen.sfi.ticket.ticketNum', DESCRIPTION = 'Current ticket number for SFI patient' where ID = 30011;
update PROP set NAME = 'ticketGen.sfi.ticket.ticketNum.max', DESCRIPTION = 'Maximum ticket number for SFI patient' where ID = 30012;
update PROP set NAME = 'ticketGen.sfi.ticket.ticketNum.min', DESCRIPTION = 'Minimum ticket number for SFI patient' where ID = 30013;

alter table WORKSTORE_PROP add TEMP_VALUE varchar2(1000) null;

update WORKSTORE_PROP set TEMP_VALUE = VALUE where PROP_ID between 30005 and 30010;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30008 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30005;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30009 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30006;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30010 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30007;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30005 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30008;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30006 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30009;

update WORKSTORE_PROP wp1
set wp1.VALUE = ( select wp2.TEMP_VALUE from WORKSTORE_PROP wp2 where wp2.PROP_ID = 30007 and wp2.WORKSTORE_CODE = wp1.WORKSTORE_CODE and wp2.HOSP_CODE = wp1.HOSP_CODE )
where wp1.PROP_ID = 30010;

alter table WORKSTORE_PROP drop column TEMP_VALUE;
--//