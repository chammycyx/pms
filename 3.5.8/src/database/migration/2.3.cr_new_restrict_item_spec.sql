create table new_restrict_item_spec(
	hospcode         varchar2( 3 ) NULL, 
	itemcode         varchar2( 6 ) not NULL, 
	formulary_status varchar2( 1 ) not NULL, 
	specialty        varchar2( 4 ) not NULL, 
	sub_specialty    varchar2( 4 ) NULL, 
	spec_type        varchar2( 1 ) NULL 
);

exit;
