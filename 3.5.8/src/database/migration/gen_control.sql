set echo off ver off feed off pages 0 
set line 1000;

accept tname prompt 'Enter Name of Table: ' 

spool .\control\&tname..ctl

select 'LOAD DATA'|| chr (10) ||
	   'BADFILE ''.\log\' || lower (table_name) || '.bad''' || chr (10) ||
       'INFILE ''.\data\' || lower (table_name) || '.txt''' || chr (10) ||
       'INTO TABLE '|| table_name || chr (10)||
       'FIELDS TERMINATED BY ''``'''||chr (10)||
       'TRAILING NULLCOLS' || chr (10) || '(' 
from   user_tables
where  table_name = upper ('&tname');

select decode( rownum, 1, '', ',' ) || line from (
  select rpad (column_name, 33, ' ')      ||
         decode (data_type,
             'VARCHAR2', 'CHAR "NVL( rtrim( :' || column_name || ' ), '' '' )"', 
             'FLOAT',    'DECIMAL EXTERNAL NULLIF('||column_name||'=BLANKS)',
             'NUMBER',   decode (data_precision, 0, 
                         'INTEGER EXTERNAL NULLIF ('||column_name||
                         '=BLANKS)', decode (data_scale, 0, 
                         'INTEGER EXTERNAL NULLIF ('||
                         column_name||'=BLANKS)', 
                         'DECIMAL EXTERNAL NULLIF ('||
                         column_name||'=BLANKS)')), 
          'TIMESTAMP(6)','"TO_TIMESTAMPE( :'|| column_name || ', ''Mon dd yyyy hh:mi:ss:ffAM'' )"',
             'DATE',     '"TO_DATE(SUBSTR(:' || column_name || ',1,20),''Mon dd yyyy HH:MI:SS'')"' ) as line,
             column_id
  from   user_tab_columns
  where  table_name = upper ('&tname') 
  order  by to_number( column_id )
);

select ')'  
from dual;
spool off

 