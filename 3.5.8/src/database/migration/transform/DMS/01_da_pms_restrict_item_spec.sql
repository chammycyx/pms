set serveroutput on size 1000000;
declare 
  
  i int;
  id_value long;
  hosp_code_begin varchar2(10);
  
  cursor c_src ( in_code_begin varchar2 ) is
    select * 
    from  new_restrict_item_spec
    where hospcode like in_code_begin;
    
begin

  select nvl( max( record_id ), 0 )
  into   id_value
  from   pms_restrict_item_spec;
 
  for i in ascii( 'P' )..ascii( 'Z' ) loop
       hosp_code_begin := chr( i ) || '%';
       dbms_output.put_line( 'Processing hospital code starting with : ' || hosp_code_begin );
       
       for rec in c_src ( hosp_code_begin ) loop
       
          insert into pms_restrict_item_spec ( 
            RECORD_ID,
            DISP_HOSP_CODE, DISP_WORKSTORE, ITEM_CODE, PMS_FM_STATUS,
            PAS_SPECIALTY, PAS_SUB_SPECIALTY, SPECIALTY_TYPE,
            UPDATE_DATE, UPDATE_USER, UPDATE_VERSION
          )
          select id_value + rownum, m.disp_hosp_code, m.disp_workstore, rec.ITEMCODE, rec.FORMULARY_STATUS,
                 rec.SPECIALTY, rec.SUB_SPECIALTY, rec.SPEC_TYPE,
                 sysdate, 'LEGACY-PMS', 0
          from   pms_pcu_mapping m
          where  m.pseudo_hosp_code = rec.HOSPCODE;
          
          select nvl( max( record_id ), 0 )
          into   id_value
          from   pms_restrict_item_spec;
          
       end loop;
       
       commit;
       
  end loop;
end;
/
