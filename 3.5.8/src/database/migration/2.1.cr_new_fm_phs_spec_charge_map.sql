create table new_fm_phs_spec_charge_map ( 
	hospcode         varchar2( 3 ) not null, 
	drugclass        varchar2( 1 ) not null, 
	pharm_specialty  varchar2( 4 ) not null, 
	charge_specialty varchar2( 4 ) not null
);

exit;

