echo off

REM ******************
REM * Usage: bcp_out.bat [Database login] [Database password] [Database server]
REM ******************

if "%1" == "" (
goto usage
)
if "%2" == "" (
goto usage
)

if "%3" == "" (
goto usage
)

echo .|date |find "current"
echo .|time |find "current"

for /f "tokens=1-4 delims=/ " %%a in ('date /t') do (set date=%%a& set month=%%b& set year=%%c& set weekday=%%d)
for /f "tokens=1-3 delims=: " %%e in ('time /t') do (set hour=%%e& set min=%%f& set m=%%g)

REM -------------- File Maintenance Tables --------------
bcp dls2_db..new_fm_phs_spec_charge_map out .\data\new_fm_phs_spec_charge_map.txt -c -Jcp850 -t"``" -U%1 -P%2  -S%3 > .\log\new_fm_phs_spec_charge_map.%year%%month%%date%_%hour%%min%%m%.log

bcp dls2_db..new_nonformul_specialty out .\data\new_nonformul_specialty.txt -c -Jcp850 -t"``" -U%1 -P%2  -S%3 > .\log\new_nonformul_specialty.%year%%month%%date%_%hour%%min%%m%.log

bcp dls2_db..new_all_drugs out .\data\new_all_drugs.txt -c -Jcp850 -t"``" -U%1 -P%2  -S%3 > .\log\new_all_drugs.%year%%month%%date%_%hour%%min%%m%.log

bcp dls2_db..new_restrict_item_spec out .\data\new_restrict_item_spec.txt -c -Jcp850 -t"``" -U%1 -P%2  -S%3 > .\log\new_restrict_item_spec.%year%%month%%date%_%hour%%min%%m%.log





goto end


:usage
echo Usage :  bcp_out.bat [Database login] [Database password] [Database server]
echo ........................................................
echo ........................................................
goto endend

:end
echo ...... Check Error ....................
echo ...... Check for Fail .................
find "Fail" *.log > _temp_log.txt
find "fail" *.log >> _temp_log.txt
find "FAIL" *.log >> _temp_log.txt
echo ...... Check for Error .................
find "Error" *.log >> _temp_log.txt
find "error" *.log >> _temp_log.txt
find "ERROR" *.log >> _temp_log.txt
echo ...... Check for Exception .................
find "Invalid" *.log >> _temp_log.txt

notepad _temp_log.txt

:endend

