echo off

REM ******************
REM * Usage: 2.0.cr_temp_table_pms_revamp.bat [Database login] [Database password] [Database server]
REM ******************

if "%1" == "" (
goto usage
)
if "%2" == "" (
goto usage
)

if "%3" == "" (
goto usage
)

sqlplus %1/%2@%3 @2.1.cr_new_fm_phs_spec_charge_map.sql

sqlplus %1/%2@%3 @2.2.cr_new_nonformul_specialty.sql

sqlplus %1/%2@%3 @2.3.cr_new_all_drugs.sql
