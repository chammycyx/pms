echo off

REM ******************
REM * Usage: 2.0.cr_temp_table_pms_revamp.bat [Database login] [Database password] [Database server]
REM ******************

if "%1" == "" (
goto usage
)
if "%2" == "" (
goto usage
)

if "%3" == "" (
goto usage
)

sqlldr userid=%1/%2@%3 control=.\control\new_all_drugs.ctl

sqlldr userid=%1/%2@%3 control=.\control\new_restrict_item.ctl