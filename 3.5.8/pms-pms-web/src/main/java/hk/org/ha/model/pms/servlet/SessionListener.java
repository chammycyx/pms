package hk.org.ha.model.pms.servlet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@SuppressWarnings("unchecked")
public class SessionListener implements ServletContextListener, HttpSessionListener {

    public static final String SESSION_LIST_KEY = "hk.org.ha.pms.web.sessionList";

    public void contextInitialized(ServletContextEvent e) {
        e.getServletContext().setAttribute(SESSION_LIST_KEY, Collections.synchronizedList(new ArrayList<HttpSession>()));
    }

    public void contextDestroyed(ServletContextEvent e) {}

	public void sessionCreated(HttpSessionEvent e) {
    	this.getSessionList(e.getSession().getServletContext()).add(e.getSession());
    }

    public void sessionDestroyed(HttpSessionEvent e) {
    	this.getSessionList(e.getSession().getServletContext()).remove(e.getSession());
    }
    
    private List<HttpSession> getSessionList(ServletContext sc) {
    	return (List<HttpSession>) sc.getAttribute(SESSION_LIST_KEY);
    }
}