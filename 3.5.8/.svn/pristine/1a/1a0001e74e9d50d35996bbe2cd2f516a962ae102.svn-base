<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="1000" height="600"
	keyUp="lookupKeyUpHandler(event)">
	
	<fx:Metadata>
		[Name("drugCheckIssueDocListPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.checkissue.UncollectPrescEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.utils.BarcodeReader;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.model.pms.vo.checkissue.CheckIssueUncollectSummary;
			import hk.org.ha.model.pms.vo.security.PropMap;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;	
			
			[In]
			public var propMap:PropMap;
			
			public var closeFunc:Function;
			
			private var uncollectPrescList:ArrayCollection;
			
			private const CHECKISSUE_UNCOLLECT_ISSUEWINDOWNUM:String = "checkIssue.uncollect.issueWindowNum";
			private static var LABEL_BARCODESUBMIT_DELAY:String = "label.barcodeSubmit.delay";
			
			public function initPopup(inUncollectPrescList:ArrayCollection):void {
				uncollectPrescList = inUncollectPrescList;
				uncollectedPrescGrid.dataProvider = uncollectPrescList;
				ticketNoTxt.setFocus();
				
				var issueWindNumStr:String = propMap.getValue(CHECKISSUE_UNCOLLECT_ISSUEWINDOWNUM);
				if ( issueWindNumStr.length > 0 ) {
					issueWinNumTxt.text = issueWindNumStr;
				} else {
					issueWinNumTxt.text = "0";
				}
				reAssignCbx.selected = ( issueWinNumTxt.text != "0" );
			}
			
			public function showFocus():void {
				focusManager.showFocus();
			}
			
			private function clearErrorString():void {
				ticketDateField.errorString = "";
				ticketNoTxt.errorString = "";
				issueWinNumTxt.errorString = "";
			}
			
			private function checkValidInput():Boolean 
			{
				if ( ticketNoTxt.text.length == 12 ) 
				{
					ticketDateField.text = ticketNoTxt.text.substring(0, 4);
					ticketNoTxt.text = ticketNoTxt.text.substring(8, 12);
					ticketDateField.selectedDate = new Date(new Date().fullYear, Number(ticketDateField.text.substring(0, 2))-1, Number(ticketDateField.text.substring(2, 4)));
				} 
				else if ( ticketNoTxt.text.length == 14 ) 
				{
					if ( ticketNoTxt.text.substring(0,1) == "9" ) {
						ticketDateField.text = ticketNoTxt.text.substring(2, 6);
						ticketNoTxt.text = ticketNoTxt.text.substring(10, 14);
						ticketDateField.selectedDate = new Date(new Date().fullYear, Number(ticketDateField.text.substring(0, 2))-1, Number(ticketDateField.text.substring(2, 4)));
					} else {
						ticketDateField.text = ticketNoTxt.text.substring(0, 8);
						ticketNoTxt.text = ticketNoTxt.text.substring(8, 12);
						ticketDateField.selectedDate = new Date(Number(ticketDateField.text.substring(4, 8)), Number(ticketDateField.text.substring(2, 4))-1, Number(ticketDateField.text.substring(0, 2)));
					}
				}
				
				if ( dateFormatter.format(ticketDateField.selectedDate).toString() != ticketDateField.text ) {
					ticketDateField.text = dateFormatter.format(ticketDateField.selectedDate).toString();
				}
				
				if ( !ExtendedDateFieldValidator.isValidDate(ticketDateField) ){
					showSystemMessage("0001", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							ticketDateField.setFocus();
						});//Invalid date format [DDMMYYYY]
					return false;
					ticketDateField.setFocus();
					return false;
				}
				
				if ( !ticketDateField.selectedDateValid() ) {
					ticketDateField.setFocus();
					return false;
				}
				
				if (ticketNoTxt.text == "" || ticketNoTxt.text.length != 4) {
					showSystemMessage("0002", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							ticketNoTxt.setFocus();
						});//Invalid ticket number.
					return false;
				}
				
				return true;
			}
			
			public function uncollectPresc():void {
				clearErrorString();
				if ( !checkValidInput() ) {
					return;
				}
				var issueWinNum:Number = 0;
				if ( reAssignCbx.selected ) {
					if ( issueWinNumTxt.text.length == 0 || issueWinNumTxt.text == "0" ) {
						showSystemMessage("0458", 
							function(evt:MouseEvent):void {
								PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
								issueWinNumTxt.setFocus();
							});//Issue window number should be greater than zero.
						return;
					}
					if ( issueWinNumTxt.text.length > 0 && Number(issueWinNumTxt.text) > 20){
						showSystemMessage("0459", 
							function(evt:MouseEvent):void {
								PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
								issueWinNumTxt.setFocus();
							});//Issue window number should be smaller than or equal to 20.
						return;
					}
					issueWinNum = Number(issueWinNumTxt.text);
				}
				dispatchEvent(new UncollectPrescEvent(ticketDateField.selectedDate, ticketNoTxt.text, issueWinNum));
			}
			
			public function refreshPopup(checkIssueUncollectSummary:CheckIssueUncollectSummary):void {
				ticketDateField.selectedDate = new Date();
				ticketNoTxt.text = "";
				uncollectPrescList.addItemAt(checkIssueUncollectSummary, 0);
				uncollectedPrescGrid.selectedIndex = 0;
				uncollectPrescList.refresh();
			}
			
			private function close():void {
				PopUpManager.removePopUp(this);
				if ( closeFunc != null ) {
					closeFunc(uncollectPrescList);
				}
				uncollectPrescList = null;
				uncollectedPrescGrid.dataProvider = null;
			}
			
			private function lookupKeyUpHandler(evt:KeyboardEvent):void {
				if ( evt.ctrlKey && evt.charCode == 48 ) { // Ctrl + 0 - Close
					closeBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					evt.stopImmediatePropagation();
					return;
				}
			}
			
			private function ticketDateLabelFunc(checkIssueUncollectSummary:CheckIssueUncollectSummary, column:DataGridColumn):String{
				if ( checkIssueUncollectSummary.ticketDate == null ) {
					return "";
				} 
				
				return dateFormatter.format(checkIssueUncollectSummary.ticketDate);
			}
			
			private function refrigItemFlagLabelFunc(checkIssueUncollectSummary:CheckIssueUncollectSummary, column:DataGridColumn):String{
				return (checkIssueUncollectSummary.refrigItemFlag?"Y":"");
			}
			
			private function issueWindowNumLabelFunc(checkIssueUncollectSummary:CheckIssueUncollectSummary, column:DataGridColumn):String{
				if ( isNaN(checkIssueUncollectSummary.issueWindowNum) || checkIssueUncollectSummary.issueWindowNum == 0) {
					return "";
				} 
				
				return checkIssueUncollectSummary.issueWindowNum.toString();
			}
			
			private function showSystemMessage(errorCode:String, okFunc:Function):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.setOkButtonOnly = true;
				msgProp.okHandler = okFunc;
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function ticketDateFieldKeyBoardFunc(event:KeyboardEvent):void{
				if( event.keyCode == Keyboard.ENTER  ){
					uncollectPresc();
				} else {
					BarcodeReader.scanBarcodeFromCheckIssue(event, propMap.getValueAsInteger(LABEL_BARCODESUBMIT_DELAY), uncollectPresc);
				}
			}

		]]>
	</fx:Script>
	
	<s:Panel width="100%" height="100%" title="Uncollect">
		<s:VGroup width="100%" height="100%" paddingLeft="10" paddingTop="10" gap="10" paddingRight="10" paddingBottom="10">
			<s:HGroup width="100%" verticalAlign="middle">
				<s:Label text="To mark Uncollected" paddingTop="3"/>
				<fc:ExtendedCheckBox id="reAssignCbx" label="Re-assign to issue window"/>
				<fc:NumericInput id="issueWinNumTxt" enabled="{reAssignCbx.selected}" width="30" numberFormat="2.0"/>
				<s:Label text="(applicable to ticket of current date)"/>
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" >
				<s:Label text="Ticket Date"/>
				<fc:ExtendedAbbDateField id="ticketDateField" selectedDate="{new Date()}"/>
				<s:Label text="Ticket No."/>
				<fc:ExtendedTextInput id="ticketNoTxt" maxChars="14" restrict="0-9" width="63" keyUp="ticketDateFieldKeyBoardFunc(event)"/>
			</s:HGroup>
			<s:Panel width="100%" height="100%" title="Uncollected Prescription">
				<s:VGroup width="100%" height="100%" paddingLeft="0" paddingTop="0" gap="0" paddingRight="0" paddingBottom="0">
				<fc:ExtendedDataGrid id="uncollectedPrescGrid" horizontalScrollPolicy="auto"
									 width="100%" height="100%" draggableColumns="false" sortableColumns="false" resizableColumns="false">
					<fc:columns>
						<mx:DataGridColumn headerText="" dataField="number" minWidth="20" width="{uncollectedPrescGrid.width*0.03}">
							<mx:itemRenderer>
								<fx:Component>
									<s:MXDataGridItemRenderer autoDrawBackground="false">
										<s:layout>
											<s:HorizontalLayout verticalAlign="middle" paddingLeft="5" paddingRight="5"/>
										</s:layout>
										<s:Label id="seqNo" text="{itemIndex+1}" textAlign="right" width="100%"/>
									</s:MXDataGridItemRenderer>
								</fx:Component>
							</mx:itemRenderer>
						</mx:DataGridColumn>
						<mx:DataGridColumn headerText="Ticket Date" dataField="ticketDate" labelFunction="ticketDateLabelFunc" width="{uncollectedPrescGrid.width*0.09}"/>
						<mx:DataGridColumn headerText="Ticket No." dataField="ticketNum" width="{uncollectedPrescGrid.width*0.07}"/>
						<mx:DataGridColumn headerText="Re-assign to" dataField="issueWindowNum" textAlign="right" labelFunction="issueWindowNumLabelFunc" width="{uncollectedPrescGrid.width*0.09}"/>
						<mx:DataGridColumn headerText="Contains Refrigerated Item" dataField="refrigItemFlag" labelFunction="refrigItemFlagLabelFunc" headerWordWrap="true" width="{uncollectedPrescGrid.width*0.17}"/>
						<mx:DataGridColumn headerText="Message" dataField="message"/>
					</fc:columns>
				</fc:ExtendedDataGrid>
				</s:VGroup>
			</s:Panel>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="right">
				<fc:ExtendedButton label="Close" id="closeBtn" click="close()"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>