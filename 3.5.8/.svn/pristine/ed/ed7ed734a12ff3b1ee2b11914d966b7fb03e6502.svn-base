<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RefillWorkloadForecastRpt" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="812" leftMargin="15" rightMargin="15" topMargin="15" bottomMargin="15">
	<property name="ireport.zoom" value="1.3310000000000022"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Row">
		<conditionalStyle>
			<conditionExpression><![CDATA[$V{REPORT_COUNT}%2==0]]></conditionExpression>
			<style mode="Opaque" backcolor="#DEDEDE" fontName="Arial" fontSize="8"/>
		</conditionalStyle>
	</style>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<parameter name="monthEndReportDate" class="java.util.Date"/>
	<field name="workstoreCode" class="java.lang.String"/>
	<field name="specCode" class="java.lang.String"/>
	<field name="outstandRefillCount" class="java.lang.Integer"/>
	<field name="outstandRefillDrugCost" class="java.lang.Double"/>
	<field name="processedRefillCount" class="java.lang.Integer"/>
	<field name="processedRefillDrugCost" class="java.lang.Double"/>
	<variable name="totalRefillCount" class="java.lang.Integer">
		<variableExpression><![CDATA[$F{outstandRefillCount}+$F{processedRefillCount}]]></variableExpression>
	</variable>
	<variable name="totalRefillCost" class="java.lang.Double">
		<variableExpression><![CDATA[$F{outstandRefillDrugCost}.doubleValue()+$F{processedRefillDrugCost}.doubleValue()]]></variableExpression>
	</variable>
	<variable name="outstandRefillDrugCost_1" class="java.lang.Double" resetType="Group" resetGroup="line" calculation="Sum">
		<variableExpression><![CDATA[$F{outstandRefillDrugCost}]]></variableExpression>
	</variable>
	<variable name="outstandRefillCount_1" class="java.lang.Integer" resetType="Group" resetGroup="line" calculation="Sum">
		<variableExpression><![CDATA[$F{outstandRefillCount}]]></variableExpression>
	</variable>
	<variable name="totalRefillCount_1" class="java.lang.Integer" resetType="Group" resetGroup="line" calculation="Sum">
		<variableExpression><![CDATA[$V{totalRefillCount}]]></variableExpression>
	</variable>
	<variable name="totalDrugCost_1" class="java.lang.Double" resetType="Group" resetGroup="line" calculation="Sum">
		<variableExpression><![CDATA[$V{totalRefillCost}]]></variableExpression>
	</variable>
	<variable name="processedCount_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{processedRefillCount}]]></variableExpression>
	</variable>
	<variable name="processedCost_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{processedRefillDrugCost}]]></variableExpression>
	</variable>
	<group name="line">
		<groupExpression><![CDATA[$F{workstoreCode}]]></groupExpression>
		<groupFooter>
			<band height="207">
				<staticText>
					<reportElement x="398" y="0" width="20" height="12"/>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[End]]></text>
				</staticText>
				<line>
					<reportElement x="0" y="0" width="812" height="1"/>
				</line>
				<staticText>
					<reportElement x="0" y="31" width="199" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total No. of Refill Coupons]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="49" width="199" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Drug Cost for all Refill Coupons]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="169" width="70" height="11"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Remarks:]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="180" width="438" height="11"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[- The drug cost is calculated from the Average Unit Cost from PHS at the time of dispensing.]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="191" width="438" height="11"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[- Refill Coupons processed as "Force Completed", "Abort" or "Suspend" also contribute to the drug cost.]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="67" width="199" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total No. of Refill Coupons (Processed)]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="85" width="199" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Drug Cost for Refill Coupons (Processed)]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="103" width="199" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total No. of Refill Coupons (Not Processed)]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="119" width="199" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Drug Cost for Refill Coupons (Not Processed)]]></text>
				</staticText>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement x="199" y="31" width="80" height="12"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{totalRefillCount_1}]]></textFieldExpression>
				</textField>
				<textField pattern="¤ #,##0.0000" isBlankWhenNull="true">
					<reportElement x="199" y="49" width="80" height="12"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.Double"><![CDATA[$V{totalDrugCost_1}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="199" y="67" width="80" height="12"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{processedCount_1}]]></textFieldExpression>
				</textField>
				<textField pattern="¤ #,##0.0000" isBlankWhenNull="true">
					<reportElement x="199" y="85" width="80" height="12"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.Double"><![CDATA[$V{processedCost_1}]]></textFieldExpression>
				</textField>
				<textField pattern="¤ #,##0.0000" isBlankWhenNull="true">
					<reportElement x="199" y="119" width="80" height="12"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.Double"><![CDATA[$V{outstandRefillDrugCost_1}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="199" y="103" width="80" height="12"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{outstandRefillCount_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="49" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="635" height="20"/>
				<textElement>
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Month End Report - Refill Statistics for                   (" + $P{hospCode} +" - "+ $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="495" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[The following Refill Coupon(s) with end date in the specified period and have not been processed by pharmacy.]]></text>
			</staticText>
			<textField pattern="MMM yyyy" isBlankWhenNull="true">
				<reportElement x="261" y="0" width="67" height="20"/>
				<textElement>
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$P{monthEndReportDate}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="22" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="59" height="20"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Working Store ]]></text>
			</staticText>
			<staticText>
				<reportElement x="59" y="0" width="65" height="20"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Specialty]]></text>
			</staticText>
			<staticText>
				<reportElement x="254" y="0" width="100" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total Drug Cost]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="20" width="812" height="1"/>
			</line>
			<staticText>
				<reportElement stretchType="RelativeToBandHeight" x="124" y="0" width="155" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total No. of Refill Coupon                    (with due date in specified period)]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToBandHeight" x="380" y="0" width="86" height="20"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[No. of Refill Coupon (Processed)]]></text>
			</staticText>
			<staticText>
				<reportElement x="466" y="0" width="98" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Drug Cost]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToBandHeight" x="607" y="0" width="100" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[No. of Refill Coupon        (Not Processed)]]></text>
			</staticText>
			<staticText>
				<reportElement x="721" y="0" width="74" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Drug Cost]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="12" splitType="Stretch">
			<frame>
				<reportElement style="Row" x="0" y="0" width="812" height="12"/>
			</frame>
			<textField isBlankWhenNull="true">
				<reportElement x="59" y="-1" width="65" height="12"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{specCode}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy" isBlankWhenNull="true">
				<reportElement x="0" y="-1" width="59" height="12"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{workstoreCode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="124" y="0" width="100" height="12"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$V{totalRefillCount}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.0000" isBlankWhenNull="true">
				<reportElement x="254" y="0" width="100" height="12"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$V{totalRefillCost}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="607" y="0" width="100" height="12"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{outstandRefillCount}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.0000" isBlankWhenNull="true">
				<reportElement x="721" y="0" width="74" height="12"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{outstandRefillDrugCost}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="380" y="0" width="76" height="12"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{processedRefillCount}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.0000" isBlankWhenNull="true">
				<reportElement x="466" y="0" width="100" height="12"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{processedRefillDrugCost}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="15" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="663" y="0" width="132" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="798" y="0" width="28" height="15"/>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="&apos;Printed on&apos; dd-MMM-yyyy &apos;at&apos; HH:mm ">
				<reportElement x="0" y="0" width="220" height="15"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
