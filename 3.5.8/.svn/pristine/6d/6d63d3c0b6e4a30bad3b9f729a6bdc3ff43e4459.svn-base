package hk.org.ha.control.pms.main.onestop
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiClearanceWarningPopupEvent;
	import hk.org.ha.event.pms.main.charging.popup.ShowSfiForceProceedPopupEvent;
	import hk.org.ha.event.pms.main.onestop.AllowModifyPostCommentPrescEvent;
	import hk.org.ha.event.pms.main.onestop.AllowModifyPrescEvent;
	import hk.org.ha.event.pms.main.onestop.CheckAllowModifyOrderStatusEvent;
	import hk.org.ha.event.pms.main.onestop.ReleaseScreenLockEvent;
	import hk.org.ha.event.pms.main.onestop.ReversePrescAllowModifyEvent;
	import hk.org.ha.event.pms.main.onestop.SfiPrescriptionDeferReverseAllowModifySuspendProceedEvent;
	import hk.org.ha.event.pms.main.onestop.SfiPrescriptionReverseAllowModifyForceProceedEvent;
	import hk.org.ha.event.pms.main.onestop.ShowInfomationMessageEvent;
	import hk.org.ha.event.pms.main.onestop.SuspendAllowModifyUncollectValidateOrderStatusFollowUpEvent;
	import hk.org.ha.model.pms.biz.onestop.AllowModifyPrescServiceBean;
	import hk.org.ha.model.pms.vo.charging.FcsPaymentStatus;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("allowModifyPrescServiceCtl", restrict="true")]
	public class AllowModifyPrescServiceCtl
	{		
		[In]
		public var allowModifyPrescService:AllowModifyPrescServiceBean;
		
		private var resultEvent:Event;
		private var dispOrderId:Number;
		private var checkAllowModifyOrderStatusEvent:CheckAllowModifyOrderStatusEvent;
		
		[Observer]
		public function checkAllowModifyOrderStatus(evt:CheckAllowModifyOrderStatusEvent):void {
			In(Object(allowModifyPrescService).statusErrorMsg);
			checkAllowModifyOrderStatusEvent = evt;
			allowModifyPrescService.checkAllowModifyOrderStatus(evt.dispOrderId, evt.dispOrderVersion, checkAllowModifyOrderStatusResult);
		}
		
		private function checkAllowModifyOrderStatusResult(evt:TideResultEvent):void {
			if (Object(allowModifyPrescService).statusErrorMsg != null) 
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(allowModifyPrescService).statusErrorMsg), true));
			}
			else
			{
				evt.context.dispatchEvent(new SuspendAllowModifyUncollectValidateOrderStatusFollowUpEvent(
					checkAllowModifyOrderStatusEvent.followUpFunc));
			}
		}
		
		[Observer]
		public function allowModifyPresc(evt:AllowModifyPrescEvent):void {
			resultEvent = evt.resultEvent;
			allowModifyPrescService.allowModifyWithSuspend(evt.dispOrderId, evt.pharmOrderVersion, evt.suspendCode, allowModifyPrescResult, allowModifyPrescFaultResult);			
		}
		
		private function allowModifyPrescResult(evt:TideResultEvent):void {
			if (resultEvent!=null) {
				evt.context.dispatchEvent(resultEvent);
			}
		}
		
		private function allowModifyPrescFaultResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new ReleaseScreenLockEvent());
		}
		
		[Observer]
		public function allowModifyPostCommentPresc(evt:AllowModifyPostCommentPrescEvent):void {
			resultEvent = evt.resultEvent;
			allowModifyPrescService.allowModifyWithPostComment(evt.dispOrderId, evt.pharmOrderVersion, evt.allowCommentType, allowModifyPostCommentPrescResult, allowModifyPostCommentPrescFaultResult);
		}
		
		private function allowModifyPostCommentPrescResult(evt:TideResultEvent):void {
			if (resultEvent!=null) {
				evt.context.dispatchEvent(resultEvent);
			}
		}
		
		private function allowModifyPostCommentPrescFaultResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new ReleaseScreenLockEvent());
		}
		
		[Observer]
		public function reversePrescAllowModify(evt:ReversePrescAllowModifyEvent):void {
			In(Object(allowModifyPrescService).fcsPaymentStatus);
			In(Object(allowModifyPrescService).sfiForceProceed);
			resultEvent = evt.resultEvent;
			dispOrderId = evt.dispOrderId;
			allowModifyPrescService.reverseAllowModifyPrescription(evt.dispOrderId, evt.dispOrderVersion, reversePrescAllowModifyResult, reversePrescAllowModifyFaultResult);
		}
		
		private function reversePrescAllowModifyResult(evt:TideResultEvent):void {
			if (Object(allowModifyPrescService).fcsPaymentStatus == null || Object(allowModifyPrescService).sfiForceProceed) {
				evt.context.dispatchEvent(resultEvent);
			} else {
				evt.context.dispatchEvent(new ShowSfiClearanceWarningPopupEvent(Object(allowModifyPrescService).fcsPaymentStatus.clearanceStatus, "revAllowModify", dispOrderId));
			}
		}
		
		private function reversePrescAllowModifyFaultResult(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new ReleaseScreenLockEvent());
		}
		
		[Observer]
		public function sfiPrescriptionDeferReverseSuspendProceed(evt:SfiPrescriptionDeferReverseAllowModifySuspendProceedEvent):void {
			resultEvent = evt.resultEvent;
			In(Object(allowModifyPrescService).statusErrorMsg);
			allowModifyPrescService.sfiPrescriptionAllowModifyDeferProceed(evt.dispOrderId, evt.dispOrderVersion, sfiPrescriptionReverseAllowModifyResult);
		}
		
		[Observer]
		public function sfiPrescriptionReverseAllowModifyForceProceed(evt:SfiPrescriptionReverseAllowModifyForceProceedEvent):void {
			resultEvent = evt.resultEvent;
			In(Object(allowModifyPrescService).statusErrorMsg);
			allowModifyPrescService.sfiPrescriptionAllowModifyForceProceed(evt.dispOrderId, evt.dispOrderVersion, sfiPrescriptionReverseAllowModifyResult);
		}
		
		private function sfiPrescriptionReverseAllowModifyResult(evt:TideResultEvent):void {
			if (Object(allowModifyPrescService).statusErrorMsg != null) 
			{
				evt.context.dispatchEvent(new ShowInfomationMessageEvent(new Array(Object(allowModifyPrescService).statusErrorMsg), false));
			}
			
			evt.context.dispatchEvent(resultEvent);
		}
	}
}
