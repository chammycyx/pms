<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent 
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx" 
	xmlns:vetting="hk.org.ha.view.pms.main.vetting.*"
	xmlns:drug="hk.org.ha.view.pms.main.drug.*"
	width="100%" height="100%"
	manualDashboardOperation="true">
	
	<fx:Metadata>
		[Name("drugSearchVettingView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.drug.popup.FreeTextEntryPopupProp;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.window.Window;
			import hk.org.ha.model.pms.dms.vo.PreparationSearchCriteria;
			import hk.org.ha.model.pms.dms.vo.RelatedDrugSearchCriteria;
			import hk.org.ha.model.pms.persistence.corp.Workstore;
			import hk.org.ha.model.pms.persistence.disp.PharmOrder;
			import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
			import hk.org.ha.model.pms.persistence.reftable.RefillConfig;
			import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
			import hk.org.ha.view.pms.main.vetting.OrderEditView;
			
			import mx.collections.ArrayCollection;
			// ------------------------------------------------			
			// inject for drug search
			// ------------------------------------------------			
			[In] [Bindable]
			public var propMap:PropMap;
			
			[In] [Bindable]
			public var orderViewInfo:OrderViewInfo;
			
			[In] [Bindable]
			public var pharmOrder:PharmOrder;			
			
			[In] [Bindable]
			public var workstore:Workstore;
			
			[In] [Bindable]
			public var window:Window;
			
			[In] [Bindable]
			public var orderEditView:OrderEditView;
			// ------------------------------------------------			
			
			// ------------------------------------------------			
			// inject for patient and order component of vetting
			// ------------------------------------------------			
			[In] [Bindable]
			public var vettingRefillConfig:RefillConfig;
			
			[In] [Bindable]
			public var vettingDrsConfig:DrsConfig;
			
			[In] [Bindable]
			public var formVerbMap:PropMap;
			
			[In] [Bindable]
			public var exemptChargeReasonList:ArrayCollection;
			
			[In] [Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[In] [Bindable]
			public var oneStopSpecialtyList:ArrayCollection;			
			
			[In] [Bindable]
			public var oneStopWardList:ArrayCollection;			
			
			[In] [Bindable]
			public var oneStopPatientCatList:ArrayCollection;			
			
			[In] [Bindable]
			public var oneStopMedicalOfficerList:ArrayCollection;
			
			[In] [Bindable]
			public var closeMenuItem:MenuItem;
			
			[In] [Bindable]
			public var rootMenu:MenuItem;
			// ------------------------------------------------
			
			
			public override function onShow():void {
				drugSearchComponent.properties = this.properties;
				drugSearchComponent.onShow();					
			}
			
			public override function onShowLater():void {
				drugSearchComponent.properties = this.properties;
				drugSearchComponent.onShowLater();
			}
			
			public override function onHide() : void {
				drugSearchComponent.properties = this.properties;
				drugSearchComponent.onHide();
			}

			
			public function createDrugNameTree(drugSearchNameMapList:ArrayCollection, itemCodeByItemCodeSearch:String):void {
				drugSearchComponent.createDrugNameTree(drugSearchNameMapList, itemCodeByItemCodeSearch);
			}
			
			public function createBnfTree(drugSearchBnfMapList:ArrayCollection):void {
				drugSearchComponent.createBnfTree(drugSearchBnfMapList);
			}
			
			public function addBnfNode(drugSearchBnfNodeMapList:ArrayCollection):void {
				drugSearchComponent.addBnfNode(drugSearchBnfNodeMapList);
			}
			
			public function addDrugDetailNode(drugSearchDetailMapList:ArrayCollection):void {
				drugSearchComponent.addDrugDetailNode(drugSearchDetailMapList);
			}
			
			public function setDrugSearchBnf(drugSearchDrugBnfList:ArrayCollection):void {
				drugSearchComponent.setDrugSearchBnf(drugSearchDrugBnfList);
			}
			
			public function setDrugSearchIngredient(drugSearchDrugIngredientList:ArrayCollection):void {
				drugSearchComponent.setDrugSearchIngredient(drugSearchDrugIngredientList);
			}

			public function resumeAddDrugLoading():void {
				drugSearchComponent.resumeAddDrugLoading();
			}			
			
			public function showDrugPriceEnquiryPopup(drugSearchSource:DrugSearchSource,
													  preparationSearchCriteria:PreparationSearchCriteria, 
													  relatedDrugSearchCriteria:RelatedDrugSearchCriteria, 
													  popupCancelHandler:Function):void {
				drugSearchComponent.showDrugPriceEnquiryPopup(drugSearchSource, preparationSearchCriteria, relatedDrugSearchCriteria, popupCancelHandler);
			}
			
			public function setRelatedDrugPrice():void {
				drugSearchComponent.setRelatedDrugPrice();
			}
			
			public function showFreeTextEntryPopup(freeTextEntryPopupProp:FreeTextEntryPopupProp):void {
				drugSearchComponent.showFreeTextEntryPopup(freeTextEntryPopupProp);
			}
			
			public function enableVetting(_savingFlag:Boolean):void {
				drugSearchComponent.enableVetting(_savingFlag);
			}
			
			public function enableVettingByBarcode(_barcodeFlag:Boolean):void {
				if (drugSearchComponent != null) {
					drugSearchComponent.enableVettingByBarcode(_barcodeFlag);
				}
			}
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<s:VGroup width="100%" height="100%">
		<drug:DrugSearchComponent id="drugSearchComponent"
								  ctx="{ctx}"
								  currentShown="{currentShown}"
								  propMap="{propMap}"
								  orderViewInfo="{orderViewInfo}"
								  pharmOrder="{pharmOrder}"
								  workstore="{workstore}"
								  window="{window}"
								  orderEditView="{orderEditView}"
								  vettingRefillConfig="{vettingRefillConfig}"
								  vettingDrsConfig="{vettingDrsConfig}"
								  formVerbMap="{formVerbMap}"
								  exemptChargeReasonList="{exemptChargeReasonList}"
								  sysMsgMap="{sysMsgMap}"
								  oneStopSpecialtyList="{oneStopSpecialtyList}"
								  oneStopWardList="{oneStopWardList}"
								  oneStopPatientCatList="{oneStopPatientCatList}"
								  oneStopMedicalOfficerList="{oneStopMedicalOfficerList}"
								  closeMenuItem="{closeMenuItem}"
								  rootMenu="{rootMenu}"
								  />
	</s:VGroup>		
</fc:ExtendedNavigatorContent>