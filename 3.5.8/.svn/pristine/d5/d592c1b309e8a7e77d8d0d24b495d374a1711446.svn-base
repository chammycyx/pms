package hk.org.ha.model.pms.biz.reftable.test;

import hk.org.ha.model.pms.biz.reftable.mp.WardGroupListServiceLocal;
import hk.org.ha.model.pms.biz.security.PostLogonServiceLocal;
import hk.org.ha.model.pms.persistence.phs.Ward;
import hk.org.ha.model.pms.persistence.reftable.WardGroup;
import hk.org.ha.model.pms.persistence.reftable.WardGroupItem;
import hk.org.ha.model.pms.vo.reftable.mp.WardGroupInfo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class WardGroupListTest extends SeamTest {

	private int totalWardGroup;
	@Test
	public void testWardGroupListComponent() throws Exception {
		
		// Login
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				assert getValue("#{identity.loggedIn}").equals(false);
	            setValue("#{identity.username}", "admin");
	            setValue("#{identity.password}", "admin");
	            invokeMethod("#{identity.login}");
	            assert getValue("#{identity.loggedIn}").equals(true);
	            PostLogonServiceLocal postLogonService = (PostLogonServiceLocal) getValue("#{postLogonService}");
	            postLogonService.postLogon("QMH", "WKS1");
			}
		}.run();
		
		//retrieve wardGroupList
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				WardGroupListServiceLocal wardGroupListService = (WardGroupListServiceLocal) getValue("#{wardGroupListService}");
				WardGroupInfo wardGroupInfo = wardGroupListService.retrieveWardGroupList();
				
				assert wardGroupInfo.getWardGroupList().size() > 0;
				totalWardGroup = wardGroupInfo.getWardGroupList().size();
			}
		}.run();

		//create wardGroup
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				WardGroupListServiceLocal wardGroupListService = (WardGroupListServiceLocal) getValue("#{wardGroupListService}");
				wardGroupListService.createWardGroup("GRP1");

				WardGroupInfo wardGroupInfo = wardGroupListService.retrieveWardGroupList();
				
				assert wardGroupListService.isSaveSuccess();
				assert wardGroupInfo.getWardGroupList().size() == totalWardGroup+1;
			}
		}.run();		
		
		//Duplicate wardGroup
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{
				WardGroupListServiceLocal wardGroupListService = (WardGroupListServiceLocal) getValue("#{wardGroupListService}");
				wardGroupListService.createWardGroup("GRP1");

				WardGroupInfo wardGroupInfo = wardGroupListService.retrieveWardGroupList();

				assert !wardGroupListService.isSaveSuccess();
				assert "0006".equals(wardGroupListService.getErrCode());
				assert wardGroupInfo.getWardGroupList().size() == totalWardGroup+1;
			}
		}.run();
		
		//remove wardGroup
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{

				WardGroupListServiceLocal wardGroupListService = (WardGroupListServiceLocal) getValue("#{wardGroupListService}");
				WardGroupInfo wardGroupInfo = wardGroupListService.retrieveWardGroupList();
				
				assert wardGroupInfo.getWardGroupList().size() == totalWardGroup+1;
				
				for (WardGroup wardGroup : wardGroupInfo.getWardGroupList()) {
					if ( StringUtils.equals(wardGroup.getWardGroupCode(), "GRP1")) {
						wardGroupListService.removeWardGroup(wardGroup);
						break;
					}
				}

				wardGroupInfo = wardGroupListService.retrieveWardGroupList();
				
				assert wardGroupInfo.getWardGroupList().size() == totalWardGroup;
				
			}
		}.run();
		
		//update wardGroupItem
		new ComponentTest() 
		{
			protected void testComponents() throws Exception 
			{

				WardGroupListServiceLocal wardGroupListService = (WardGroupListServiceLocal) getValue("#{wardGroupListService}");
				WardGroupInfo wardGroupInfo = wardGroupListService.retrieveWardGroupList();
				List<WardGroup> wardGroupList = wardGroupInfo.getWardGroupList();
				List<Ward> wardGroupWardList = wardGroupInfo.getWardGroupWardList();
				
				assert wardGroupList.size() == totalWardGroup;
				assert wardGroupWardList != null;
				
				List<Ward> selectedWardList = new ArrayList<Ward>();
				
				for ( Ward ward : wardGroupWardList ) {
					if ( "5D".equals(ward.getWardCode()) || "A1".equals(ward.getWardCode()) ) {
						selectedWardList.add(ward);
					}
				}

				wardGroupListService.updateWardGroup(wardGroupList.get(0), selectedWardList);

				wardGroupInfo = wardGroupListService.retrieveWardGroupList();
				wardGroupList = wardGroupInfo.getWardGroupList();
				
				assert wardGroupList.size() == totalWardGroup;
				assert wardGroupList.get(0).getWardGroupItemList().size() == 2;
				
				for ( WardGroupItem item : wardGroupList.get(0).getWardGroupItemList() ) {
					if ( "5D".equals(item.getWardCode()) || "A1".equals(item.getWardCode())) {
						assert true;
					} else {
						assert false;
					}
				}
			}
		}.run();
		
		
	}	
}
