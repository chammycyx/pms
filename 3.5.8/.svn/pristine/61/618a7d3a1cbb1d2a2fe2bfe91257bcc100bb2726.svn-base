<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
				 xmlns:s="library://ns.adobe.com/flex/spark" 
				 xmlns:mx="library://ns.adobe.com/flex/mx"
				 xmlns:core="hk.org.ha.fmk.pms.flex.components.core.*"
				 xmlns="hk.org.ha.view.pms.main.vetting.po.*" 
				 xmlns:mp="hk.org.ha.view.pms.main.medprofile.*" 
				 focusIn="pharmLineEditFocusInHandler(event)">
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.events.IndexChangeEvent;
			
			import hk.org.ha.event.pms.main.drug.RetrieveDefaultFormVerbByFormCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDmFormVerbMappingListBySiteCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDmWarningListForOrderEditEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDosageUnitListByFormCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveIpSiteListByItemCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveSiteListEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForDhOrderEditEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForMpOrderEditEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForOrderEditEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByDhRxDrugEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListDrugKeyEvent;
			import hk.org.ha.event.pms.main.vetting.PharmOrderItemChangeEvent;
			import hk.org.ha.event.pms.main.vetting.RecalPharmOrderItemEvent;
			import hk.org.ha.event.pms.main.vetting.popup.ShowFormVerbPopupEvent;
			import hk.org.ha.event.pms.main.vetting.popup.ShowOrderEditItemPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.dms.persistence.DmDrug;
			import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
			import hk.org.ha.model.pms.dms.persistence.DmWarning;
			import hk.org.ha.model.pms.dms.vo.FormVerb;
			import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
			import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
			import hk.org.ha.model.pms.udt.vetting.ActionStatus;
			import hk.org.ha.model.pms.vo.drug.DmDrugLite;
			import hk.org.ha.model.pms.vo.drug.DmFormLite;
			import hk.org.ha.model.pms.vo.drug.DmWarningLite;
			import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
			import hk.org.ha.model.pms.vo.rx.Dose;
			import hk.org.ha.model.pms.vo.rx.DoseGroup;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewInfo;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewUtil;
			import hk.org.ha.model.pms.vo.vetting.PharmOrderEditUiInfo;
			import hk.org.ha.view.pms.main.vetting.OrderEditView;
			import hk.org.ha.view.pms.main.vetting.popup.FormVerbPopup;
			import hk.org.ha.view.pms.main.vetting.popup.OrderEditItemPopup;
			
			[Bindable]
			public var orderEditViewInfo:OrderEditViewInfo;														
			
			[Bindable]
			private var pharmOrderItemList:ListCollectionView;
			
			private var formCodeChangeFlag:Boolean=false;
			private var isSfiFlag:Boolean;
			private var lastCurrentDosageUnit:String;
			
			private var recalCallBack:Function; //pms-3102
			
			[Bindable]
			public var itemCodeTabIndex:int;
			[Bindable]
			public var drugNameTabIndex:int;
			[Bindable]
			public var formDescTabIndex:int;
			[Bindable]
			public var strengthTabIndex:int;
			[Bindable]
			public var volumeTabIndex:int;
			[Bindable]
			public var totalQtyTabIndex:int;
			[Bindable]
			public var issueQtyTabIndex:int;
			[Bindable]
			public var drugNameTabFocusEnabled:Boolean = false;
			[Bindable]
			public var formDescTabFocusEnabled:Boolean = false;
			[Bindable]
			public var strengthTabFocusEnabled:Boolean = false;
			[Bindable]
			public var volumeTabFocusEnabled:Boolean = false;
			
			public var haveItemCodeChangeFlag:Boolean = false;
			
			private function retrieveNewDrugItem():void
			{
				if (!orderEditViewInfo.readOnlyFlag && !orderEditViewInfo.isMoiEditFlag && data) {
					
					var poi:PharmOrderItem = data as PharmOrderItem;
					if (poi.checkDrugValidity(orderEditViewInfo.sysMsgMap,false)) {
						orderEditViewInfo.backendProcessFlag = true;
						poi.dirtyFlag = true;
						if (orderEditViewInfo.medOrder.isDhOrder()) {
							dispatchEvent(new RetrieveWorkstoreDrugForDhOrderEditEvent(poi.medOrderItem.dhRxDrug, poi.itemCode, updatePharmOrderDrugItem));
						}
						else if (orderEditViewInfo.medOrder.isMpDischarge()) {
							dispatchEvent(new RetrieveWorkstoreDrugForMpOrderEditEvent(poi.medOrderItem.rxItem, poi.itemCode, updatePharmOrderDrugItem));
						}
						else {
							dispatchEvent(new RetrieveWorkstoreDrugForOrderEditEvent(orderEditViewInfo.medOrderItem.displayName, orderEditViewInfo.medOrderItem.fmStatus, data.itemCode, updatePharmOrderDrugItem));
						}
					}
					else {//pms-3406
						callLater(orderEditViewInfo.setFocusToPoDataRowAt,[pharmOrderItemList.getItemIndex(data),0,0]);
					}
				}
				else
				{
					callLater(orderEditViewInfo.setFocusToPoDataRowAt,[pharmOrderItemList.getItemIndex(data),0,0]);//pms-3406
				}
			}
			
			protected function itemCodeFocusChangeHandler(event:FocusEvent):void
			{
				try {
					if ( StringUtil.trim(itemCodeTextInput.text) != data.itemCode ) { //pms-3406
						event.preventDefault();
						stage.focus=null;			
					}
					var poi:PharmOrderItem = data as PharmOrderItem;
					var orderEditView:OrderEditView = orderEditViewInfo.orderEditView as OrderEditView;
					itemCodeChangeHandler();
					if ( poi.itemCodeErrorString ) {
						haveItemCodeChangeFlag = true;
						poi.itemCodeErrorString = null;
						orderEditView.manualErrorString = null;
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function itemCodeChangeHandler():void
			{
				try {
					var poi:PharmOrderItem = data as PharmOrderItem;
					
					if ( StringUtil.trim(itemCodeTextInput.text) != data.itemCode) {
						data.itemCode = StringUtil.trim(itemCodeTextInput.text);
						retrieveNewDrugItem();
					}
					
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			public function itemCodeKeyDownHandler(event:KeyboardEvent):void
			{				
				if ( event.keyCode == Keyboard.ENTER ) {
					itemCodeChangeHandler();
				} 
			}
			
			private function postItemCodeKeyUpHandler():void {
				var okHandler:Function = function(evt:Event):void {							
					var popup:OrderEditItemPopup = UIComponent(evt.target).parentDocument as OrderEditItemPopup;
					var drug:WorkstoreDrug = null;
					callLater(
						function():void {
							stage.focus = null;
						}
					);
					
					if( popup.itemListGrid.selectedIndex != -1 ) {
						drug = popup.itemListGrid.selectedItem as WorkstoreDrug;
					} else if ( popup.solventListGrid.selectedIndex != -1 ) {
						drug = popup.solventListGrid.selectedItem as WorkstoreDrug;
					}
					
					if(drug != null && drug.msWorkstoreDrug.suspend != 'Y') {
						updatePharmOrderDrugItem(drug);
						PopUpManager.removePopUp(popup);
					}
				}
				
				var doubleClickHandler:Function = function(evt:ListEvent):void {
					
					var popup:OrderEditItemPopup = UIComponent(evt.target).parentDocument as OrderEditItemPopup;
					var drug:WorkstoreDrug = evt.itemRenderer.data as WorkstoreDrug;						
					callLater(
						function():void {
							stage.focus = null;
						}
					);
					if(drug != null && drug.msWorkstoreDrug.suspend != 'Y') {
						updatePharmOrderDrugItem(drug);	
						PopUpManager.removePopUp(popup);
					}
				}																	
				
				var cancelClickHandler:Function = function(evt:Event):void {
					var popup:OrderEditItemPopup = UIComponent(evt.target).parentDocument as OrderEditItemPopup;
					PopUpManager.removePopUp(popup);
					callLater(validateItemCodeTextInput);
					callLater(itemCodeTextInput.setFocus);
					callLater(itemCodeTextInput.focusManager.showFocus);
				}
				dispatchEvent(
					new ShowOrderEditItemPopupEvent(
						orderEditViewInfo.medOrderItem, 
						okHandler, 
						doubleClickHandler, 
						cancelClickHandler
					)
				);
				if (orderEditViewInfo.medOrder.isDhOrder()) {
					dispatchEvent(new RetrieveWorkstoreDrugListByDhRxDrugEvent(orderEditViewInfo.medOrderItem.dhRxDrug, data.itemCode));
				}
				else {
					dispatchEvent(new RetrieveWorkstoreDrugListDrugKeyEvent(orderEditViewInfo.medOrderItem.drugKey));
				}
			}
			
			public function itemCodeKeyUpHandler(event:KeyboardEvent):void {
				
				if ( PopupUtil.isAnyVisiblePopupExists() ) {
					return;
				} else if ( orderEditViewInfo.readOnlyFlag || orderEditViewInfo.isMoiEditFlag ) {
					return;
				} else {
					if (event.charCode == 53 && event.ctrlKey ) {
						if (orderEditViewInfo.medOrderItem.isDhRxDrug && orderEditViewInfo.latestMappingStatus == "N" && itemCodeTextInput.text.length < 4) {
							var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0904");
							msgProp.setOkButtonOnly = true;
							dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
							return;
						}
						
						data.itemCode = StringUtil.trim(itemCodeTextInput.text);
						if (orderEditViewInfo.medOrder.isMpDischarge()) {
							OrderEditViewUtil.postItemCodeKeyUpHandlerForMp(
								orderEditViewInfo.orderEditView.ctx,
								orderEditViewInfo.medOrderItem.rxItem,
								false,//not manual profile
								updatePharmOrderDrugItem);
						}						
						else {
							postItemCodeKeyUpHandler();
						}
					}
				}
			}
			
			private function validateItemCodeTextInput():void
			{
				var pharmOrderItem:PharmOrderItem = data as PharmOrderItem;
				if ( orderEditViewInfo && !orderEditViewInfo.isOnHide ) {
					
					if(itemCodeTextInput.text == "")
					{
						pharmOrderItem.itemCodeErrorString = orderEditViewInfo.sysMsgMap.getMessage("0375");
					}
					else 
					{
						pharmOrderItem.checkDrugValidity(orderEditViewInfo.sysMsgMap,true);
					}
					orderEditViewInfo.orderEditView['manualErrorString'] = pharmOrderItem.itemCodeErrorString;
				}
			}
			
			protected function itemCodeTextInputFocusIn(event:FocusEvent):void
			{
				try {
					//PMSB-2372
					var orderEditView:OrderEditView = orderEditViewInfo.orderEditView as OrderEditView;  
					orderEditView.poView.list.selectedItem = data as PharmOrderItem;
					if( haveItemCodeChangeFlag) // update item code error string 
					{
						validateItemCodeTextInput();
						haveItemCodeChangeFlag = false;
					}
					orderEditViewInfo.itemCodeFocusFlag=true;
					orderEditViewInfo.resetPoViewTabSeqAndInvalidDesc(itemCodeTabIndex);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function formDescKeyUpHandler(event:KeyboardEvent):void
			{
				if ( PopupUtil.isAnyVisiblePopupExists() ) {
					return;
				}
				if ( orderEditViewInfo.readOnlyFlag || orderEditViewInfo.isMoiEditFlag ) {
					return;
				}
				if ( !event.ctrlKey || event.charCode != 53 ) { 
					return;
				}
				
				try {
					
					lastCurrentDosageUnit = PharmOrderItem(data).regimen.firstDose.dosageUnit;
					
					var okHandler:Function = function(value:Object):void {	
						var popup:FormVerbPopup;
						if ( !(value is FormVerbPopup) ) {
							popup = UIComponent(value.target).parentDocument as FormVerbPopup;
						} else {
							popup = value as FormVerbPopup;
						}
						var dmFormVerbMapping:DmFormVerbMapping = null;								
						if( popup.formVerbGrid.selectedIndex != -1 ) {									
							dmFormVerbMapping = popup.formVerbGrid.selectedItem as DmFormVerbMapping;
							if ( data.formCode != dmFormVerbMapping.dmForm.formCode ) {
								formCodeChangeFlag = true;
								dispatchEvent( new RetrieveDosageUnitListByFormCodeEvent(dmFormVerbMapping.dmForm.formCode, updateDosageUnitList) );
							}
							data.dmFormLite = DmFormLite.objValueOf(dmFormVerbMapping.dmForm);							
							
							data.formLabelDesc = DmFormLite(data.dmFormLite).labelDesc;									
							data.formCode = dmFormVerbMapping.dmForm.formCode;								
							data.checkFormVerb(orderEditViewInfo.formVerbMap,orderEditViewInfo.sysMsgMap.getMessage("0172"));
							PharmOrderItem(data).routeCode = dmFormVerbMapping.dmForm.dmRoute.routeCode;
							PharmOrderItem(data).routeDesc = dmFormVerbMapping.dmForm.dmRoute.fullRouteDesc;
							PharmOrderItem(data).markDirty();
							
							orderEditViewInfo.clearDispQtyFromAllPharmDoseForDangerDrug(PharmOrderItem(data));	//PMSU-4533
							
							updatePharmOrderItemDispQty(0,0);//PMSU-1013, PMSU-3402
							
							PopUpManager.removePopUp(popup);
						}
					}
					//PMS-2914
					var cancelHandler:Function = function(event:MouseEvent):void
					{
						PopUpManager.removePopUp(((event.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						callLater(formInput.setFocus);
					}
					dispatchEvent( new ShowFormVerbPopupEvent(okHandler, orderEditViewInfo.medOrder.isMpDischarge(), cancelHandler) );
					dispatchEvent( new RetrieveDmFormVerbMappingListBySiteCodeEvent(PharmOrderItem(data).regimen.firstDose.siteCode) );
					
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function updateDosageUnitList(value:ArrayCollection):void
			{				
				try {
					var dosageUnit:String = null;
					
					PharmOrderItem(data).regimen.firstDose.dosageUnitList = value;
					
					if ( lastCurrentDosageUnit ) {
						for each ( var dosageUnitValue:String in value ) {
							if ( dosageUnitValue == lastCurrentDosageUnit ) {
								dosageUnit = dosageUnitValue;
								break;
							}
						}				
					}
					
					if ( !dosageUnit ) {
						if ( formCodeChangeFlag ) {
							if ( value.length == 1 ) {						
								dosageUnit  = value.getItemAt(0) as String;
							} else {						
								dosageUnit  = null;
							}
						} else {
							dosageUnit = PharmOrderItem(data).dmDrugLite.dispenseDosageUnit;
						}
					}
					
					PharmOrderItem(data).regimen.updateDosage(dosageUnit?"0":"");				
					PharmOrderItem(data).regimen.updateDosageUnit(dosageUnit);
					PharmOrderItem(data).regimen.firstDose.dosageSetFocusFlag = true;				
					PharmOrderItem(data).doseQty = 0;
					PharmOrderItem(data).doseUnit = dosageUnit;
					
					if ( PharmOrderItem(data).regimen.firstDose.dispQtyText != "" || formCodeChangeFlag ) {//PMSU-4157
						recalHandler();
					} 
					formCodeChangeFlag = false;
					
					callLater(orderEditViewInfo.setFocusToPoDataRowAt,[pharmOrderItemList.getItemIndex(data),0,0]);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}												
			
			protected function updatePharmOrderItemByDmWarningList(dmWarningList:ArrayCollection):void
			{
				try {
					var pharmOrderItem:PharmOrderItem = PharmOrderItem(data);																	
					for ( var i:int=0; i<dmWarningList.length; i++ ) {
						var dmWarning:DmWarning = dmWarningList.getItemAt(i) as DmWarning;
						if ( dmWarning ) {
							pharmOrderItem.setDmWarningLite(i,DmWarningLite.objValueOf(dmWarning));
							pharmOrderItem.setWarnCode(i,dmWarning.warnCode);
						} else {
							pharmOrderItem.setDmWarningLite(i,null);
							pharmOrderItem.setWarnCode(i,null);
						}
					}
					orderEditViewInfo.warnDesc = "";
					if ( pharmOrderItem.dmWarningLite1 != null ) {
						orderEditViewInfo.warnDesc = pharmOrderItem.dmWarningLite1.warnMessageEng;
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function updateDefaultFormVerb(formVerbValue:FormVerb, overrideSiteCode:String=null, overrideSupplSiteDesc:String=null):void
			{
				var formVerbSiteCode:String = formVerbValue == null ? null : formVerbValue.siteCode;
				var supplementarySite:String = formVerbValue == null ? null : formVerbValue.supplementarySite;
				
				try {
					var pharmOrderItem:PharmOrderItem = PharmOrderItem(data);
					for each ( var doseGroup:DoseGroup in pharmOrderItem.regimen.doseGroupList ) {
						for each ( var dose:Dose in doseGroup.doseList ) {
							dose.defaultFormVerb = formVerbValue;
							dose.siteCode		 = overrideSiteCode == null ? formVerbSiteCode : overrideSiteCode;
							dose.supplSiteDesc	 = overrideSupplSiteDesc == null ? supplementarySite : overrideSupplSiteDesc;
							dose.dmSite			 = orderEditViewInfo.dmSiteDictionary[dose.siteCode];
							dose.dmSupplSite	 = orderEditViewInfo.dmSupplSiteDictionary[dose.siteCode+dose.supplSiteDesc];
							
							dose.siteDesc		 = dose.dmSite == null ? null : dose.dmSite.siteDescEng;
						}
					}
					recalHandler();
				} catch ( e:Error ) {
					trace(e.getStackTrace());
				}
			}
			
			private function checkDrugScope(dmDrugIn:DmDrug, drugScope:Number):Boolean {
				return drugScope >= 200 || (drugScope >= 100 && dmDrugIn.dmMoeProperty.solventItem == "Y");
			}
			
			protected function updatePharmOrderDrugItem(drug:WorkstoreDrug):void
			{							
				try {
					orderEditViewInfo.saveItemInProgressFlag = false;
					orderEditViewInfo.backendProcessFlag	 = false;
					lastCurrentDosageUnit					 = null;
					
					var pharmOrderItem:PharmOrderItem = PharmOrderItem(data);	
					
					if (orderEditViewInfo.isMoiEditFlag) {
						return;
					}
					else {
						if( drug != null && 
							drug.msWorkstoreDrug != null &&
							drug.msWorkstoreDrug.suspend != "Y" && 
							checkDrugScope(drug.dmDrug, drug.msWorkstoreDrug.drugScope) ) {
							
							var dmDrugValue:DmDrug = drug.dmDrug;																			
							
							pharmOrderItem.itemCodeErrorString 	= "";
							pharmOrderItem.dmDrugLite 			= DmDrugLite.objValueOf(dmDrugValue);
							pharmOrderItem.itemCode 			= dmDrugValue.itemCode;
							pharmOrderItem.baseUnit 			= dmDrugValue.baseUnit;										
							pharmOrderItem.dmFormLite 			= DmFormLite.objValueOf(dmDrugValue.dmForm);
							pharmOrderItem.drugKey 				= dmDrugValue.drugKey;
							pharmOrderItem.drugName 			= dmDrugValue.drugName;
							pharmOrderItem.tradeName			= dmDrugValue.dmMoeProperty.tradename;
							pharmOrderItem.dupChkDisplayName	= dmDrugValue.dmDrugProperty.displayname;					
							pharmOrderItem.strength 			= dmDrugValue.strength;
							pharmOrderItem.volumeText 			= dmDrugValue.volumeText;
							pharmOrderItem.dangerDrugFlag		= dmDrugValue.dangerousDrug == 'Y';
							pharmOrderItem.formCode 			= dmDrugValue.dmForm.formCode;
							pharmOrderItem.orgFormCode			= dmDrugValue.dmForm.formCode;
							pharmOrderItem.formLabelDesc 		= dmDrugValue.dmForm.labelDesc;
							pharmOrderItem.routeCode			= dmDrugValue.dmForm.routeCode;
							pharmOrderItem.routeDesc			= orderEditViewInfo.dmRouteDictionary[pharmOrderItem.routeCode];
							pharmOrderItem.fmStatus				= orderEditViewInfo.medOrderItem.fmStatus;
							pharmOrderItem.doseUnit 			= "";
							pharmOrderItem.dirtyFlag 			= true;
							pharmOrderItem.moIssueQty			= Number.NaN;
							pharmOrderItem.moBaseUnit			= "";
							pharmOrderItem.regimen 				= orderEditViewInfo.createRegimen();
							pharmOrderItem.regimen.updateBaseUnit(dmDrugValue.baseUnit);
							//PMS-2790
							pharmOrderItem.issueQty				= Number.NaN;
							pharmOrderItem.issueQtyText 		= "0";
							pharmOrderItem.medOrderItem			= orderEditViewInfo.medOrderItem;
							pharmOrderItem.actionStatus			= orderEditViewInfo.medOrderItem.actionStatus;
							
							formCodeChangeFlag = false;					
							pharmOrderItem.itemCodeErrorString = null;
							orderEditViewInfo.orderEditView['manualErrorString'] = null;
							
							dispatchEvent( new RetrieveDosageUnitListByFormCodeEvent(dmDrugValue.dmForm.formCode, updateDosageUnitList) );
							dispatchEvent( new RetrieveDmWarningListForOrderEditEvent(dmDrugValue.itemCode,updatePharmOrderItemByDmWarningList) );
							
							if (orderEditViewInfo.medOrder.isMpDischarge()) {
								
								OrderEditViewUtil.updateFmAndActionStatus(pharmOrderItem.medOrderItem.rxItem, pharmOrderItem);//update fmStatus and actionStatus
								if (!OrderEditViewUtil.checkIsParenteralSite(pharmOrderItem.medOrderItem.rxItem, parentApplication.dmSiteParenteralMap)) {
									dispatchEvent( new RetrieveDefaultFormVerbByFormCodeEvent(dmDrugValue.dmForm.formCode, updateDefaultFormVerb, pharmOrderItem.itemCode) );
									dispatchEvent( new RetrieveIpSiteListByItemCodeEvent(pharmOrderItem.itemCode, updatePoiRegimenSiteList) );
								} else {
									OrderEditViewUtil.assignParenteralSiteToDose(
										orderEditViewInfo.dmSiteDictionary, 
										orderEditViewInfo.dmSupplSiteDictionary, 
										pharmOrderItem.medOrderItem.rxItem, 
										pharmOrderItem.regimen.firstDose);
								}
							}
							else {
								dispatchEvent( new RetrieveDefaultFormVerbByFormCodeEvent(dmDrugValue.dmForm.formCode,function(formVerb:FormVerb):void {
									var overrideSiteCode:String = orderEditViewInfo.medOrder.isDhOrder() ? drug.siteCode : null;
									var overrideSupplSiteDesc:String = orderEditViewInfo.medOrder.isDhOrder() ? drug.supplSiteDesc : null;
									updateDefaultFormVerb(formVerb, overrideSiteCode, overrideSupplSiteDesc);
								}) );
								dispatchEvent( new RetrieveSiteListEvent(dmDrugValue.dmForm.formCode,updatePoiRegimenSiteList) );
							}
							
							orderEditViewInfo.resetPoViewTabSeqAndInvalidDesc(-1,10,true);
							
						} else {		
							if ( orderEditViewInfo && !orderEditViewInfo.isOnHide ) {
								itemCodeTextInput.setFocus();
								if(itemCodeTextInput.text == "")
								{
									pharmOrderItem.itemCodeErrorString = orderEditViewInfo.sysMsgMap.getMessage("0375");
								}
								else
								{
									pharmOrderItem.itemCodeErrorString = orderEditViewInfo.sysMsgMap.getMessage("0030");
								}
								orderEditViewInfo.orderEditView['manualErrorString'] = pharmOrderItem.itemCodeErrorString;
							} else {
								pharmOrderItem.itemCodeErrorString = null;
								orderEditViewInfo.orderEditView['manualErrorString'] = null;
							}
						}
					}
				} catch ( e:Error ) {
					trace(e.getStackTrace());
				}
			}												
			
			private function updatePoiRegimenSiteList(siteListValue:ListCollectionView):void
			{
				try {
					var poi:PharmOrderItem = data as PharmOrderItem;
					poi.regimen.setAllSiteList(siteListValue);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function formInputClickHandler(event:Event):void
			{
				formInput.focusManager.showFocus();				
			}
			
			private function copyMedOrderItem(inMedOrderItem:MedOrderItem):MedOrderItem
			{
				var outMedOrderItem:MedOrderItem = new MedOrderItem();								
				outMedOrderItem.actionStatus 	 = inMedOrderItem.actionStatus;
				outMedOrderItem.baseUnit 		 = inMedOrderItem.baseUnit;
				outMedOrderItem.formCode 		 = inMedOrderItem.formCode;	
				outMedOrderItem.rxDrug			 = inMedOrderItem.rxDrug;
				outMedOrderItem.ivRxDrug		 = inMedOrderItem.ivRxDrug;
				outMedOrderItem.regimen 		 = inMedOrderItem.regimen;
				
				return outMedOrderItem;
			}
			
			private function copyPharmOrderItem(inPharmOrderItem:PharmOrderItem):PharmOrderItem
			{
				var outPharmOrderItem:PharmOrderItem = new PharmOrderItem;
				outPharmOrderItem.doseQty 			 = inPharmOrderItem.doseQty;
				outPharmOrderItem.doseUnit 			 = inPharmOrderItem.doseUnit;
				outPharmOrderItem.itemCode			 = inPharmOrderItem.itemCode;
				outPharmOrderItem.formCode			 = inPharmOrderItem.formCode;
				outPharmOrderItem.regimen			 = inPharmOrderItem.regimen;
				outPharmOrderItem.moIssueQty		 = inPharmOrderItem.moIssueQty;
				outPharmOrderItem.moBaseUnit		 = inPharmOrderItem.moBaseUnit;
				outPharmOrderItem.additiveNum		 = inPharmOrderItem.additiveNum;
				outPharmOrderItem.fluidNum			 = inPharmOrderItem.fluidNum;
				outPharmOrderItem.actionStatus		 = inPharmOrderItem.actionStatus; //PMSB-3047
				
				return outPharmOrderItem;
			}
			
			private function updatePharmOrderItemDispQty(inCalQty:Number,inDoseQty:Number):void 
			{					
				orderEditViewInfo.backendProcessFlag = false;				
				if (!data) {				
					return;
				} else {
				
					var orderEditView:OrderEditView = orderEditViewInfo.orderEditView as OrderEditView;
					var poi:PharmOrderItem = data as PharmOrderItem;
					
					//PMSU-3404
					poi.calQty 			   = inCalQty;
					poi.issueQtyText   	   = String(inCalQty);	
					poi.doseQty 		   = inDoseQty;				
					
					if (isSfiFlag) {
						if ( poi.sfiQtyText == "" ) {
							poi.sfiQtyText = "0";
						}
					}
					
					if ( poi.recalRequestFlag ) {
						poi.recalRequestFlag = false;
						recalHandler(recalCallBack);
					} else if ( orderEditViewInfo.saveItemInProgressFlag ) {
						orderEditView.saveItem();
					}
					
					// pms-3102
					if(recalCallBack != null)
					{
						recalCallBack();
					}
				}
			}
			
			public function recalHandler(callBack:Function = null):void
			{				
				var orderEditView:OrderEditView = orderEditViewInfo.orderEditView as OrderEditView;
				if ( !data || orderEditViewInfo.orderEditView.properties["readOnly"] == true || orderEditViewInfo.isMoiEditFlag ) {
					return;
				}
				
				var poi:PharmOrderItem = data as PharmOrderItem;
				poi.markDirty();
				
				if ( poi.regimen.checkFreqParam(orderEditViewInfo.sysMsgMap,false) ) {
					return;
				}
				
				if( !orderEditViewInfo.backendProcessFlag ) {	
					recalFunc();
				} else {
					poi.recalRequestFlag = true;
				}
				
				recalCallBack = callBack;
			}						
			
			private function getNumOfSystemDeducedItem():Number
			{
				var count:Number = 0;
				for each ( var poi:PharmOrderItem in orderEditViewInfo.medOrderItem.pharmOrderItemList ) {
					if ( poi.systemDeducedFlag ) {
						++count;
					}
				}
				return count;
			}
			
			private function validateSendRecalPharmOrderItem(poiValue:PharmOrderItem):Boolean
			{
				if ( poiValue == null || poiValue.dmDrugLite == null ) {
					return false;				
				}
				if ( !orderEditViewInfo.validateRegimenForPharmRecal(poiValue.regimen) ) {
					return false;
				}
				if ( poiValue.regimen.checkFreqParam(orderEditViewInfo.sysMsgMap,false) ) {
					return false;
				}
				for each (var doseGroup:DoseGroup in poiValue.regimen.doseGroupList ) {
					if ( doseGroup.checkDurationForRecal() ) {
						return false;
					}
					if ( doseGroup.checkDurationUnitForRecal() ) {
						return false;
					}
				}
				orderEditViewInfo.backendProcessFlag = true;
				return true;
			}
			
			private function recalFunc():void
			{				
				var orderEditView:OrderEditView = orderEditViewInfo.orderEditView as OrderEditView;
				if ( orderEditViewInfo.orderEditView.properties["readOnly"] || orderEditViewInfo.isMoiEditFlag ) {
					return;
				}
				var poi:PharmOrderItem = data as PharmOrderItem;
				
				//PMS-3010
				poi.clearErrorString();
				if ( orderEditViewInfo.poDurationDateFieldFlag ) {
					poi.regimen.determineDuration(orderEditViewInfo.dmRegimenDictionary);
				}
				
				if (!validateSendRecalPharmOrderItem(poi)) {
					return;
				}
				
				var outPharmOrderItem:PharmOrderItem 				  = copyPharmOrderItem(poi);				
				outPharmOrderItem.medOrderItem 						  = copyMedOrderItem(poi.medOrderItem);
				outPharmOrderItem.medOrderItem.numOfSystemDeducedItem = getNumOfSystemDeducedItem();
				dispatchEvent( new PharmOrderItemChangeEvent(poi, new RecalPharmOrderItemEvent(outPharmOrderItem, updatePharmOrderItemDispQty)) );				
			}
			
			public function getDoseGroupItemRendererAt(index:int):DoseGroupEditItemRenderer
			{
				if ( !doseGroupDataGroup || !doseGroupDataGroup.dataProvider ) {
					return null;
				}
				if ( index < doseGroupDataGroup.dataProvider.length && index >= 0 ) {
					return doseGroupDataGroup.getElementAt(index) as DoseGroupEditItemRenderer;
				} else {
					return null;
				}
			}
			
			public function checkFocusToError(compulFocusFlag:Boolean=false):void
			{
				if ( orderEditViewInfo.hasFocusFlag && !compulFocusFlag ) {
					return;
				}
				
				if ( itemCodeTextInput.errorString ) {
					orderEditViewInfo.hasFocusFlag = true;
					itemCodeTextInput.setFocus();
					orderEditViewInfo.orderEditView['manualErrorString'] = itemCodeTextInput.errorString;
					return;
				}
			}
			
			public function resetScrollPosition(evt:FocusEvent, obj:Object):void
			{
				try {
					var poiIndex:int = ListCollectionView(pharmOrderItemList).getItemIndex(data);
					callLater(orderEditViewInfo.resetScrollPosition, [poiIndex]);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			public function drugNameTextInputFocusOutHandler(event:FocusEvent):void 
			{
				if(data != null)
				{
					data.drugName=StringUtil.trim(data.drugName);
				}
				orderEditViewInfo.drugNameFocusFlag=false;
				resetScrollPosition(event, data);
			}
			
			public function strengthFocusOutHandler():void 
			{
				orderEditViewInfo.strengthFocusFlag=false;
				if(data != null)
				{
					data.strength=StringUtil.trim(data.strength);
				}
			}
			
			public function volumeFocusOutHandler():void
			{
				orderEditViewInfo.volumeFocusFlag=false;
				if(data != null)
				{
					data.volumeText=StringUtil.trim(data.volumeText);
				}
			}

			protected function pharmLineEditFocusInHandler(event:FocusEvent):void
			{
				var poView:OrderLineEdit = orderEditViewInfo.orderEditView.poView;
				var oldIndex:int = poView.list.selectedIndex;
				var newIndex:int = pharmOrderItemList.getItemIndex(data);
				if (oldIndex != newIndex) {
					poView.list.selectedIndex = newIndex;
					poView.list.dispatchEvent(new IndexChangeEvent(IndexChangeEvent.CHANGE,false,false,oldIndex,newIndex));
				}
			}
			
			protected function shortcutKey(evt:KeyboardEvent):void
			{	
				if (orderEditViewInfo.isMoiEditFlag) {
					return;
				}
				if (orderEditViewInfo.readOnlyFlag) {
					return;
				}
				if (data == null) {
					return;
				}
				var poi:PharmOrderItem = data as PharmOrderItem;
				var lastDoseGroupIndex:int = poi.regimen.doseGroupList.length - 1;
				var lastDoseIndex:int = DoseGroup(poi.regimen.doseGroupList[lastDoseGroupIndex]).doseList.length - 1;
				var firstDoseItemRenderer:DoseEditItemRenderer = getDoseGroupItemRendererAt(0).getDoseItemRendererAt(0);
				var lastDoseItemRenderer:DoseEditItemRenderer = getDoseGroupItemRendererAt(lastDoseGroupIndex).getDoseItemRendererAt(lastDoseIndex);
				// meal time, Ctrl + 2
				if ( evt.charCode == 50 && evt.ctrlKey){
					if (poi.regimen.getFullDoseList().length != 1) {
						return;
					}
					if (!firstDoseItemRenderer.adminTimeBtn.enabled) {
						return;
					}
					firstDoseItemRenderer.adminTimeBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}//add, Ctrl + 3
				else if (evt.charCode == 51 && evt.ctrlKey && lastDoseItemRenderer.addBtn.enabled && lastDoseItemRenderer.addBtn.visible) {
					lastDoseItemRenderer.addBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
				else if (evt.charCode == 52 && evt.ctrlKey && lastDoseItemRenderer.removeBtn.enabled && lastDoseItemRenderer.removeBtn.visible) {
					lastDoseItemRenderer.removeBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
			}

		]]>
	</fx:Script>
	
	<fx:Binding source="{orderEditViewInfo.medOrderItem.pharmOrderItemList}" destination="pharmOrderItemList"/>
	<fx:Binding source="{orderEditViewInfo.medOrderItem.actionStatus == ActionStatus.PurchaseByPatient || orderEditViewInfo.medOrderItem.actionStatus == ActionStatus.SafetyNet}" destination="isSfiFlag"/>
	
	<mp:layout>
		<s:VerticalLayout paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0"/>
	</mp:layout>	
	
	<s:HGroup id="pharmLineGroup"
			  gap="{PharmOrderEditUiInfo.paddingSpace}"
			  verticalAlign="middle" styleName="vettingPoDrugLineStyle"
			  paddingTop="{PharmOrderEditUiInfo.minPaddingSpace}"
			  paddingLeft="{PharmOrderEditUiInfo.paddingSpace}" paddingRight="{PharmOrderEditUiInfo.paddingSpace}"
			  keyUp="shortcutKey(event)">
		
		<core:UppercaseTextInput id="itemCodeTextInput"
								 tabIndex="{itemCodeTabIndex}"
								 text="{data.itemCode}" 
								 width="{PharmOrderEditUiInfo.itemCodeColWidth - PharmOrderEditUiInfo.paddingSpace}"
								 keyDown="itemCodeKeyDownHandler(event)"
								 keyUp="itemCodeKeyUpHandler(event)"
								 errorString="{data.itemCodeErrorString}"
								 restrict="A-Z0-9a-z "
								 maxChars="6"	
								 focusIn="itemCodeTextInputFocusIn(event);
								 resetScrollPosition(event, data);"
								 keyFocusChange="itemCodeFocusChangeHandler(event)"
								 mouseFocusChange="itemCodeFocusChangeHandler(event)"
								 focusOut="orderEditViewInfo.itemCodeFocusFlag=false;"								  
								 editable="{!orderEditViewInfo.readOnlyFlag}"
								 enabled="{!orderEditViewInfo.isMoiEditFlag &amp;&amp; !orderEditViewInfo.readOnlyFlag}"
								 tabFocusEnabled="true"/>
		<core:UppercaseTextInput text="@{data.drugName}" id="drugNameTextInput"
								 tabIndex="{drugNameTabIndex}"
								 width="{PharmOrderEditUiInfo.drugNameColWidth - PharmOrderEditUiInfo.paddingSpace}" 									 
								 maxChars="57"
								 restrict="\u0020-\u007E"
								 change="data.dirtyFlag=true"
								 focusIn="orderEditViewInfo.drugNameFocusFlag=true;orderEditViewInfo.resetPoViewTabSeqAndInvalidDesc()"
								 focusOut="drugNameTextInputFocusOutHandler(event)"
								 editable="{!orderEditViewInfo.readOnlyFlag}"
								 enabled="{!orderEditViewInfo.isMoiEditFlag &amp;&amp; !orderEditViewInfo.readOnlyFlag}"
								 tabFocusEnabled="{drugNameTabFocusEnabled}"/>
		<core:UppercaseTextInput id="formInput"
								 tabIndex="{formDescTabIndex}"
								 text="{data.formLabelDesc}" 
								 width="{PharmOrderEditUiInfo.formDescColWidth - PharmOrderEditUiInfo.paddingSpace}" 
								 keyUp="formDescKeyUpHandler(event)"
								 change="data.dirtyFlag=true"
								 focusIn="orderEditViewInfo.formDescFocusFlag=true;orderEditViewInfo.resetPoViewTabSeqAndInvalidDesc();
								 resetScrollPosition(event, data);"
								 focusOut="orderEditViewInfo.formDescFocusFlag=false;"
								 click="formInputClickHandler(event)"
								 doubleClick="formInputClickHandler(event)"
								 editable="false"
								 toolTip="{data.formLabelDesc}"
								 enabled="{!orderEditViewInfo.isMoiEditFlag &amp;&amp; !orderEditViewInfo.readOnlyFlag}"
								 tabFocusEnabled="{formDescTabFocusEnabled}"
								 styleName="{!orderEditViewInfo.isMoiEditFlag &amp;&amp; !orderEditViewInfo.readOnlyFlag?'formInputEnabledStyle':'formInputDisabledStyle'}"/>
		<core:UppercaseTextInput text="@{data.strength}"
								 tabIndex="{strengthTabIndex}"
								 width="{PharmOrderEditUiInfo.strengthColWidth - PharmOrderEditUiInfo.paddingSpace}" 									 
								 restrict="\u0020-\u007E"
								 maxChars="12"
								 change="data.dirtyFlag=true"
								 focusIn="orderEditViewInfo.strengthFocusFlag=true;orderEditViewInfo.resetPoViewTabSeqAndInvalidDesc();
								 resetScrollPosition(event, data);"
								 focusOut="strengthFocusOutHandler()"
								 editable="{!orderEditViewInfo.readOnlyFlag}"								 
								 enabled="{!orderEditViewInfo.isMoiEditFlag &amp;&amp; !orderEditViewInfo.readOnlyFlag}"
								 tabFocusEnabled="{strengthTabFocusEnabled}"/>
		<core:UppercaseTextInput id="volume"
								 tabIndex="{volumeTabIndex}"
								 text="@{data.volumeText}"	  
								 width="{PharmOrderEditUiInfo.volumeColWidth - PharmOrderEditUiInfo.paddingSpace}"
								 restrict="\u0020-\u007E"
								 maxChars="23"
								 change="data.dirtyFlag=true"
								 focusIn="orderEditViewInfo.volumeFocusFlag=true;orderEditViewInfo.resetPoViewTabSeqAndInvalidDesc();
								 resetScrollPosition(event, data);"
								 focusOut="volumeFocusOutHandler()"
								 editable="{!orderEditViewInfo.readOnlyFlag}"								 
								 enabled="{!orderEditViewInfo.isMoiEditFlag &amp;&amp; !orderEditViewInfo.readOnlyFlag}"
								 tabFocusEnabled="{volumeTabFocusEnabled}"/>
		<core:NumericInput text="{data.doseQtyText}"
						   textAlign="right" 
						   enabled = "false"
						   width="{PharmOrderEditUiInfo.doseQtyTextInputWidth}" 
						   styleName="poRegimenTotalDoseStyle"
						   toolTip="{data.doseQtyText}"
						   tabFocusEnabled="false"/>
		<s:Label text="{data.doseUnit}" 
				 verticalAlign="middle" textAlign="left" 
				 width="100%"/>	<!--PMSU-2037-->	
	</s:HGroup>						
	
	<s:VGroup id="vGroup"
			  height="100%" 			  
			  paddingTop="0" paddingBottom="0"
			  paddingLeft="0" paddingRight="0"
			  gap="0">
		<s:DataGroup id="doseGroupDataGroup"
					 width="100%"
					 dataProvider="{data.regimen.doseGroupList}"
					 tabFocusEnabled="false">
			<s:layout>
				<s:VerticalLayout gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0"/>
			</s:layout>
			
			<s:itemRenderer>
				<fx:Component>					
					<DoseGroupEditItemRenderer pharmOrderItem="{outerDocument.data as PharmOrderItem}" 
											   doseGroupList="{outerDocument.data.regimen.doseGroupList}"
											   orderEditViewInfo="{outerDocument.orderEditViewInfo}"
											   recalHandler="{outerDocument.recalHandler}"
											   tabFocusEnabled="true">
						<fx:Script>
							<![CDATA[
								import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
							]]>
						</fx:Script>
					</DoseGroupEditItemRenderer>
					
				</fx:Component>
			</s:itemRenderer>
		</s:DataGroup>	
	</s:VGroup>
	
	<s:Line xFrom="0" xTo="{vGroup.width}" 
			yFrom="0" yTo="0" 
			visible="{ListCollectionView(pharmOrderItemList).getItemIndex(data) != (pharmOrderItemList.length - 1)}"
			includeInLayout="{ListCollectionView(pharmOrderItemList).getItemIndex(data) != (pharmOrderItemList.length - 1)}"
			width="100%">
		<s:stroke>
			<s:SolidColorStroke color="0x000000" weight="2" joints="round"/>
		</s:stroke>
	</s:Line>
	
</mp:ItemRenderer>
