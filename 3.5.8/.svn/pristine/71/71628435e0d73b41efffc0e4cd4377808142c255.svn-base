<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="560" 
	height="{!pivasPrinterEnabledFlag?400:465}"
	creationComplete="creationCompleteSetFocus()">
	
	<fx:Metadata>
		[Name("operationModeMaintEditWorkstationSettingPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.reftable.UpdateWorkstationSettingEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.persistence.reftable.Workstation;
			import hk.org.ha.model.pms.persistence.reftable.WorkstationPrint;
			import hk.org.ha.model.pms.udt.reftable.PrintDocType;
			import hk.org.ha.model.pms.udt.reftable.PrintType;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.components.DropDownList;
			
			[Bindable]
			private var workstation:Workstation;
			
			[Bindable]
			private var pivasPrinterEnabledFlag:Boolean;
			
			private var addWorkstationFlag:Boolean;
			
			[In][Bindable]
			public var adminLevel:Number;
			
			private function creationCompleteSetFocus():void{
				if ( stationCodeTxt.enabled ) {
					stationCodeTxt.setFocus();
				} else {
					computerName.setFocus();
				}
			}
			
			public function initPopup(inWorkstation:Workstation, redirectList:ArrayCollection, inPivasPrinterEnabledFlag:Boolean, inAddWorkstationFlag:Boolean):void{
				workstation = inWorkstation;
				pivasPrinterEnabledFlag = inPivasPrinterEnabledFlag;
				addWorkstationFlag = inAddWorkstationFlag;
				initDropDownListDataProvider(redirectList);
				initPrinterSetting(workstation);	
			}
			
			private function listValueToSelectedIndex(value:Object, list:ArrayCollection):int 
			{
				var i:int = 0;
				for each (var listItem:Object in list) {
					if (value == listItem.workstationCode) {
						return i;
					}
					i++;	
				}
				return -1;
			}
			
			private function initPrinterSetting(workstation:Workstation):void{
				if ( workstation.otherLabelPrint.type == PrintType.Printer ) {
					otherLabelCbx.selected = true;
					otherLabelPrinterRadioBtn.selected = true;
					otherLabelPrinterTextInput.text = workstation.otherLabelPrint.value;
				} else if ( workstation.otherLabelPrint.type == PrintType.Redirect ) {
					otherLabelCbx.selected = true;
					otherLabelRedirectRadioBtn.selected = true;
					otherLabelDropDownList.selectedIndex = listValueToSelectedIndex(workstation.otherLabelPrint.value, otherLabelDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.sfiInvoicePrint.type == PrintType.Printer ) {
					sfiInvoiceCbx.selected = true;
					sfiInvoicePrinterRadioBtn.selected = true;
					sfiInvoicePrinterTextInput.text = workstation.sfiInvoicePrint.value;
				} else if ( workstation.sfiInvoicePrint.type == PrintType.Redirect ) {
					sfiInvoiceCbx.selected = true;
					sfiInvoiceRedirectRadioBtn.selected = true;
					sfiInvoiceDropDownList.selectedIndex = listValueToSelectedIndex(workstation.sfiInvoicePrint.value, sfiInvoiceDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.sfiRefillCouponPrint.type == PrintType.Printer ) {
					sfiRefillCouponCbx.selected = true;
					sfiRefillCouponPrinterRadioBtn.selected = true;
					sfiRefillCouponPrinterTextInput.text = workstation.sfiRefillCouponPrint.value;
				} else if ( workstation.sfiRefillCouponPrint.type == PrintType.Redirect ) {
					sfiRefillCouponCbx.selected = true;
					sfiRefillCouponRedirectRadioBtn.selected = true;
					sfiRefillCouponDropDownList.selectedIndex = listValueToSelectedIndex(workstation.sfiRefillCouponPrint.value, sfiRefillCouponDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.refillCouponPrint.type == PrintType.Printer ) {
					refillCouponCbx.selected = true;
					refillCouponPrinterRadioBtn.selected = true;
					refillCouponPrinterTextInput.text = workstation.refillCouponPrint.value;
				} else if ( workstation.refillCouponPrint.type == PrintType.Redirect ) {
					refillCouponCbx.selected = true;
					refillCouponRedirectRadioBtn.selected = true;
					refillCouponDropDownList.selectedIndex = listValueToSelectedIndex(workstation.refillCouponPrint.value, refillCouponDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.capdVoucherPrint.type == PrintType.Printer ) {
					capdVoucherCbx.selected = true;
					capdVoucherPrinterRadioBtn.selected = true;
					capdVoucherPrinterTextInput.text = workstation.capdVoucherPrint.value;
				} else if ( workstation.capdVoucherPrint.type == PrintType.Redirect ) {
					capdVoucherCbx.selected = true;
					capdVoucherRedirectRadioBtn.selected = true;
					capdVoucherDropDownList.selectedIndex = listValueToSelectedIndex(workstation.capdVoucherPrint.value, capdVoucherDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.reportPrint.type == PrintType.Printer ) {
					reportCbx.selected = true;
					reportPrinterRadioBtn.selected = true;
					reportPrinterTextInput.text = workstation.reportPrint.value;
				} else if ( workstation.reportPrint.type == PrintType.Redirect ) {
					reportCbx.selected = true;
					reportRedirectRadioBtn.selected = true;
					reportDropDownList.selectedIndex = listValueToSelectedIndex(workstation.reportPrint.value, reportDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.pivasLargeLabelPrint.type == PrintType.Printer ) {
					pivasLargeLabelCbx.selected = true;
					pivasLargeLabelPrinterRadioBtn.selected = true;
					pivasLargeLabelPrinterTextInput.text = workstation.pivasLargeLabelPrint.value;
				} else if ( workstation.pivasLargeLabelPrint.type == PrintType.Redirect ) {
					pivasLargeLabelCbx.selected = true;
					pivasLargeLabelRedirectRadioBtn.selected = true;
					pivasLargeLabelDropDownList.selectedIndex = listValueToSelectedIndex(workstation.pivasLargeLabelPrint.value, pivasLargeLabelDropDownList.dataProvider as ArrayCollection);
				}
				if ( workstation.pivasColorLabelPrint.type == PrintType.Printer ) {
					pivasColorLabelCbx.selected = true;
					pivasColorLabelPrinterRadioBtn.selected = true;
					pivasColorLabelPrinterTextInput.text = workstation.pivasColorLabelPrint.value;
				} else if ( workstation.pivasColorLabelPrint.type == PrintType.Redirect ) {
					pivasColorLabelCbx.selected = true;
					pivasColorLabelRedirectRadioBtn.selected = true;
					pivasColorLabelDropDownList.selectedIndex = listValueToSelectedIndex(workstation.pivasColorLabelPrint.value, pivasColorLabelDropDownList.dataProvider as ArrayCollection);
				}
			}
			
			private function initDropDownListDataProvider(redirectList:ArrayCollection):void{
				for each ( var item:Object in redirectList ) {
					switch ( item.type ) {
						case PrintDocType.OtherLabel:
							otherLabelDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.SfiInvoice:
							sfiInvoiceDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.SfiRefillCoupon:
							sfiRefillCouponDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.RefillCoupon:
							refillCouponDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.CapdVoucher:
							capdVoucherDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.Report:
							reportDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.PivasLargeLabel:
							pivasLargeLabelDropDownList.dataProvider = item.list;
							break;
						case PrintDocType.PivasColorLabel:
							pivasColorLabelDropDownList.dataProvider = item.list;
							break;
					}
				}
			}
			
			private function close(evt:MouseEvent):void {
				PopUpManager.removePopUp(this);
			}
			
			private function okFunc(evt:MouseEvent):void {
				if ( !validation() ) {
					return;
				}
				var updateWorkstation:Workstation = new Workstation();
				updateWorkstation.editable = workstation.editable;
				updateWorkstation.edited = true;
				updateWorkstation.workstationCode = stationCodeTxt.text;
				updateWorkstation.hostName = computerName.text;
				updateWorkstation.commonAccountFlag = commonAccountCbx.selected;
				updateWorkstation.chemoFlag = chemoCbx.selected;
				updateWorkstation.adminLevel = itdWorkstationCbx.selected?1:0;
				
				if ( otherLabelCbx.selected ) {
					if ( otherLabelPrinterRadioBtn.selected ) {
						updateWorkstation.otherLabelPrint = setWorkstationPrint(PrintDocType.OtherLabel, PrintType.Printer, StringUtil.trim(otherLabelPrinterTextInput.text));
					} else {
						updateWorkstation.otherLabelPrint = setWorkstationPrint(PrintDocType.OtherLabel, PrintType.Redirect, otherLabelDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.otherLabelPrint = setWorkstationPrint(PrintDocType.OtherLabel, PrintType.None, "");
				}
				
				if ( sfiInvoiceCbx.selected ) {
					if ( sfiInvoicePrinterRadioBtn.selected ) {
						updateWorkstation.sfiInvoicePrint = setWorkstationPrint(PrintDocType.SfiInvoice, PrintType.Printer, StringUtil.trim(sfiInvoicePrinterTextInput.text));
					} else {
						updateWorkstation.sfiInvoicePrint = setWorkstationPrint(PrintDocType.SfiInvoice, PrintType.Redirect, sfiInvoiceDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.sfiInvoicePrint = setWorkstationPrint(PrintDocType.SfiInvoice, PrintType.None, "");
				}
				
				if ( sfiRefillCouponCbx.selected ) {
					if ( sfiRefillCouponPrinterRadioBtn.selected ) {
						updateWorkstation.sfiRefillCouponPrint = setWorkstationPrint(PrintDocType.SfiRefillCoupon, PrintType.Printer, StringUtil.trim(sfiRefillCouponPrinterTextInput.text));
					} else {
						updateWorkstation.sfiRefillCouponPrint = setWorkstationPrint(PrintDocType.SfiRefillCoupon, PrintType.Redirect, sfiRefillCouponDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.sfiRefillCouponPrint = setWorkstationPrint(PrintDocType.SfiRefillCoupon, PrintType.None, "");
				}
				
				if ( refillCouponCbx.selected ) {
					if ( refillCouponPrinterRadioBtn.selected ) {
						updateWorkstation.refillCouponPrint = setWorkstationPrint(PrintDocType.RefillCoupon, PrintType.Printer, StringUtil.trim(refillCouponPrinterTextInput.text));
					} else {
						updateWorkstation.refillCouponPrint = setWorkstationPrint(PrintDocType.RefillCoupon, PrintType.Redirect, refillCouponDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.refillCouponPrint = setWorkstationPrint(PrintDocType.RefillCoupon, PrintType.None, "");
				}
				
				if ( capdVoucherCbx.selected ) {
					if ( capdVoucherPrinterRadioBtn.selected ) {
						updateWorkstation.capdVoucherPrint = setWorkstationPrint(PrintDocType.CapdVoucher, PrintType.Printer, StringUtil.trim(capdVoucherPrinterTextInput.text));
					} else {
						updateWorkstation.capdVoucherPrint = setWorkstationPrint(PrintDocType.CapdVoucher, PrintType.Redirect, capdVoucherDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.capdVoucherPrint = setWorkstationPrint(PrintDocType.CapdVoucher, PrintType.None, "");
				}
				
				if ( reportCbx.selected ) {
					if ( reportPrinterRadioBtn.selected ) {
						updateWorkstation.reportPrint = setWorkstationPrint(PrintDocType.Report, PrintType.Printer, StringUtil.trim(reportPrinterTextInput.text));
					} else {
						updateWorkstation.reportPrint = setWorkstationPrint(PrintDocType.Report, PrintType.Redirect, reportDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.reportPrint = setWorkstationPrint(PrintDocType.Report, PrintType.None, "");
				}
				
				if ( pivasLargeLabelCbx.selected ) {
					if ( pivasLargeLabelPrinterRadioBtn.selected ) {
						updateWorkstation.pivasLargeLabelPrint = setWorkstationPrint(PrintDocType.PivasLargeLabel, PrintType.Printer, StringUtil.trim(pivasLargeLabelPrinterTextInput.text));
					} else {
						updateWorkstation.pivasLargeLabelPrint = setWorkstationPrint(PrintDocType.PivasLargeLabel, PrintType.Redirect, pivasLargeLabelDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.pivasLargeLabelPrint = setWorkstationPrint(PrintDocType.PivasLargeLabel, PrintType.None, "");
				}
				
				if ( pivasColorLabelCbx.selected ) {
					if ( pivasColorLabelPrinterRadioBtn.selected ) {
						updateWorkstation.pivasColorLabelPrint = setWorkstationPrint(PrintDocType.PivasColorLabel, PrintType.Printer, StringUtil.trim(pivasColorLabelPrinterTextInput.text));
					} else {
						updateWorkstation.pivasColorLabelPrint = setWorkstationPrint(PrintDocType.PivasColorLabel, PrintType.Redirect, pivasColorLabelDropDownList.selectedItem.workstationCode);
					}
				} else {
					updateWorkstation.pivasColorLabelPrint = setWorkstationPrint(PrintDocType.PivasColorLabel, PrintType.None, "");
				}
				
				dispatchEvent(new UpdateWorkstationSettingEvent(updateWorkstation, addWorkstationFlag));
				PopUpManager.removePopUp(this);
			}
			
			private function setWorkstationPrint(docType:PrintDocType, printType:PrintType, printValue:String):WorkstationPrint{
				var workstationPrint:WorkstationPrint = new WorkstationPrint();
				workstationPrint.docType = docType;
				workstationPrint.type = printType;
				workstationPrint.value = printValue;
				return workstationPrint;
			}
			
			private function printerSettingValidation(radioBtn:RadioButton, textInput:TextInput, dropDownList:AdvancedDropDownList):Boolean {
				
				if ( radioBtn.selected ) {
					if ( StringUtil.trim(textInput.text) == "" ){
						this.showSystemMessage("0484", 
							function(evt:MouseEvent):void {
								PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
								textInput.setFocus();
							});//Please input printer name.
						return false;
					}
				} else {
					if ( dropDownList.selectedIndex<0 ){
						this.showSystemMessage("0485", 
							function(evt:MouseEvent):void {
								PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
								dropDownList.setFocus();
								focusManager.showFocus();
							});//Please select redirect printer.
						return false;
					}
				}
				return true;
			}
			
			private function validation():Boolean {
				var stationCodeValid:Boolean = true;
				
				if ( StringUtil.trim(stationCodeTxt.text) == "" ){
					this.showSystemMessage("0481", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							stationCodeTxt.setFocus();
						});//Please input station code.
					return false;
				}
				if ( StringUtil.trim(computerName.text) == "" ){
					this.showSystemMessage("0482", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							computerName.setFocus();
						});//Please input computer name.
					return false;
				}
				
				if ( otherLabelCbx.selected ) {
					if ( !printerSettingValidation(otherLabelPrinterRadioBtn, otherLabelPrinterTextInput, otherLabelDropDownList) ) {
						return false;
					}
				}
				
				if ( sfiInvoiceCbx.selected ) {
					if ( !printerSettingValidation(sfiInvoicePrinterRadioBtn, sfiInvoicePrinterTextInput, sfiInvoiceDropDownList) ) {
						return false;
					}
				}
				
				if ( sfiRefillCouponCbx.selected ) {
					if ( !printerSettingValidation(sfiRefillCouponPrinterRadioBtn, sfiRefillCouponPrinterTextInput, sfiRefillCouponDropDownList) ) {
						return false;
					}
				}
				
				if ( refillCouponCbx.selected ) {
					if ( !printerSettingValidation(refillCouponPrinterRadioBtn, refillCouponPrinterTextInput, refillCouponDropDownList) ) {
						return false;
					}
				}
				
				if ( capdVoucherCbx.selected ) {
					if ( !printerSettingValidation(capdVoucherPrinterRadioBtn, capdVoucherPrinterTextInput, capdVoucherDropDownList) ) {
						return false;
					}
				}
				
				if ( reportCbx.selected ) {
					if ( !printerSettingValidation(reportPrinterRadioBtn, reportPrinterTextInput, reportDropDownList) ) {
						return false;
					}
				}
				
				if ( pivasLargeLabelCbx.selected ) {
					if ( !printerSettingValidation(pivasLargeLabelPrinterRadioBtn, pivasLargeLabelPrinterTextInput, pivasLargeLabelDropDownList) ) {
						return false;
					}
				}
				
				if ( pivasColorLabelCbx.selected ) {
					if ( !printerSettingValidation(pivasColorLabelPrinterRadioBtn, pivasColorLabelPrinterTextInput, pivasColorLabelDropDownList) ) {
						return false;
					}
				}
				
				return true;
			}
			
			private function showSystemMessage(errorCode:String, okFunc:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if ( okFunc != null ) {
					msgProp.okHandler = okFunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function clearDataByType(textInput:TextInput, dropDownList:DropDownList):void{
				textInput.text = "";
				dropDownList.selectedIndex = -1;
			}
			
			private function focusNextItem(evt:FocusEvent):void {
				switch (evt.currentTarget) {
					case otherLabelPrinterTextInput:
					case otherLabelDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								sfiInvoiceCbx.setFocus();
							}
						}
						break;
					case sfiInvoicePrinterTextInput:
					case sfiInvoiceDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								sfiRefillCouponCbx.setFocus();
							}
						}
						break;
					case sfiRefillCouponPrinterTextInput:
					case sfiRefillCouponDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								refillCouponCbx.setFocus();
							}
						}
						break;
					case refillCouponPrinterTextInput:
					case refillCouponDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								capdVoucherCbx.setFocus();
							}
						}
						break;
					case capdVoucherPrinterTextInput:
					case capdVoucherDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								reportCbx.setFocus();
							}
						}
						break;
					case reportPrinterTextInput:
					case reportDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								if (!pivasPrinterEnabledFlag) {
									okButton.setFocus();
								} else {
									pivasLargeLabelCbx.setFocus();
								}
							}
						}
						break;
					case pivasLargeLabelPrinterTextInput:
					case pivasLargeLabelDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								pivasColorLabelCbx.setFocus();
							}
						}
						break;
					case pivasColorLabelPrinterTextInput:
					case pivasColorLabelDropDownList:
						if ( !evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								okButton.setFocus();
							}
						}
						break;
				}
			}
		]]>
	</fx:Script>	
	
	<fx:Declarations>
		<s:RadioButtonGroup id="otherLabelRadioGroup" change="clearDataByType(otherLabelPrinterTextInput, otherLabelDropDownList)"/>
		<s:RadioButtonGroup id="sfiInvouceRadioGroup" change="clearDataByType(sfiInvoicePrinterTextInput, sfiInvoiceDropDownList)"/>
		<s:RadioButtonGroup id="sfiRefillCouponRadioGroup" change="clearDataByType(sfiRefillCouponPrinterTextInput, sfiRefillCouponDropDownList)"/>
		<s:RadioButtonGroup id="refillCouponRadioGroup" change="clearDataByType(refillCouponPrinterTextInput, refillCouponDropDownList)"/>
		<s:RadioButtonGroup id="capdVoucherRadioGroup" change="clearDataByType(capdVoucherPrinterTextInput, capdVoucherDropDownList)"/>
		<s:RadioButtonGroup id="reportRadioGroup" change="clearDataByType(reportPrinterTextInput, reportDropDownList)"/>
		<s:RadioButtonGroup id="pivasLargeLabelRadioGroup" change="clearDataByType(pivasLargeLabelPrinterTextInput, pivasLargeLabelDropDownList)"/>
		<s:RadioButtonGroup id="pivasColorLabelRadioGroup" change="clearDataByType(pivasColorLabelPrinterTextInput, pivasColorLabelDropDownList)"/>
	</fx:Declarations>
	
	<s:Panel width="100%" height="100%" title="Edit Workstation Setting">
		<s:VGroup width="100%" height="100%" paddingLeft="10" paddingTop="10" gap="10" paddingRight="10" paddingBottom="10">
			<s:HGroup>
				<s:Label text="Station Code" paddingLeft="0" paddingTop="6" width="125"/>
				<fc:UppercaseTextInput id="stationCodeTxt" text="{workstation.workstationCode}" enabled="{workstation.editable}" maxChars="4" width="100"/>
			</s:HGroup>
			<s:HGroup>
				<s:Label text="Computer Name" paddingTop="6" width="125"/>
				<fc:LowercaseTextInput id="computerName" text="{workstation.hostName}" maxChars="15" width="150"/>
			</s:HGroup>
			<s:HGroup>
				<s:CheckBox id="commonAccountCbx" label="Enable Common Account" selected="{workstation.commonAccountFlag}"/>
				<s:CheckBox id="itdWorkstationCbx" label="ITD Workstation" selected="{workstation.adminLevel==1}" visible="{adminLevel==1}"/>
				<s:CheckBox id="chemoCbx" label="Chemo Workstation" selected="{workstation.chemoFlag}"/>
			</s:HGroup>
			<s:HGroup>
				<s:Panel title="Printer Setting" width="100%" height="100%">
					<s:VGroup width="100%" height="100%" paddingBottom="10" paddingLeft="10" paddingRight="10" paddingTop="10" gap="10" verticalAlign="middle" horizontalAlign="left">
						<s:HGroup width="100%" height="100%">
							<s:CheckBox id="otherLabelCbx" label="Other Label" width="130"
										change="{otherLabelCbx.selected?otherLabelPrinterRadioBtn.selected=true:null;clearDataByType(otherLabelPrinterTextInput, otherLabelDropDownList)}"/>
							<s:RadioButton id="otherLabelPrinterRadioBtn" groupName="otherLabelRadioGroup" label="Printer" enabled="{otherLabelCbx.selected}"/>
							<s:TextInput id="otherLabelPrinterTextInput" maxChars="100" enabled="{otherLabelCbx.selected &amp;&amp; otherLabelPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="otherLabelRedirectRadioBtn" groupName="otherLabelRadioGroup" label="Redirect" enabled="{otherLabelCbx.selected}"/>
							<fc:AdvancedDropDownList id="otherLabelDropDownList"
													 width="112"
													 labelField="workstationCode"
													 enabled="{otherLabelCbx.selected &amp;&amp; otherLabelRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%">
							<s:CheckBox id="sfiInvoiceCbx" label="SFI Invoice" width="130"
										change="{sfiInvoiceCbx.selected?sfiInvoicePrinterRadioBtn.selected=true:null;clearDataByType(sfiInvoicePrinterTextInput, sfiInvoiceDropDownList)}"/>
							<s:RadioButton id="sfiInvoicePrinterRadioBtn" groupName="sfiInvouceRadioGroup" label="Printer" enabled="{sfiInvoiceCbx.selected}"/>
							<s:TextInput id="sfiInvoicePrinterTextInput" maxChars="100" enabled="{sfiInvoiceCbx.selected &amp;&amp; sfiInvoicePrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="sfiInvoiceRedirectRadioBtn" groupName="sfiInvouceRadioGroup" label="Redirect" enabled="{sfiInvoiceCbx.selected}"/>
							<fc:AdvancedDropDownList id="sfiInvoiceDropDownList" 
													 width="112"
													 labelField="workstationCode"
													 enabled="{sfiInvoiceCbx.selected &amp;&amp; sfiInvoiceRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%">
							<s:CheckBox id="sfiRefillCouponCbx" label="SFI Refill Coupon" width="130"
										change="{sfiRefillCouponCbx.selected?sfiRefillCouponPrinterRadioBtn.selected=true:null;clearDataByType(sfiRefillCouponPrinterTextInput, sfiRefillCouponDropDownList)}"/>
							<s:RadioButton id="sfiRefillCouponPrinterRadioBtn" groupName="sfiRefillCouponRadioGroup" label="Printer" enabled="{sfiRefillCouponCbx.selected}"/>
							<s:TextInput id="sfiRefillCouponPrinterTextInput" maxChars="100" enabled="{sfiRefillCouponCbx.selected &amp;&amp; sfiRefillCouponPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="sfiRefillCouponRedirectRadioBtn" groupName="sfiRefillCouponRadioGroup" label="Redirect" enabled="{sfiRefillCouponCbx.selected}"/>
							<fc:AdvancedDropDownList id="sfiRefillCouponDropDownList" 
													 width="112"
													 labelField="workstationCode"
													 enabled="{sfiRefillCouponCbx.selected &amp;&amp; sfiRefillCouponRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%">
							<s:CheckBox id="refillCouponCbx" label="Refill Coupon" width="130"
										change="{refillCouponCbx.selected?refillCouponPrinterRadioBtn.selected=true:null;clearDataByType(refillCouponPrinterTextInput, refillCouponDropDownList)}"/>
							<s:RadioButton id="refillCouponPrinterRadioBtn" groupName="refillCouponRadioGroup" label="Printer" enabled="{refillCouponCbx.selected}"/>
							<s:TextInput id="refillCouponPrinterTextInput" maxChars="100" enabled="{refillCouponCbx.selected &amp;&amp; refillCouponPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="refillCouponRedirectRadioBtn" groupName="refillCouponRadioGroup" label="Redirect" enabled="{refillCouponCbx.selected}"/>
							<fc:AdvancedDropDownList id="refillCouponDropDownList" 
													 width="112"
													 labelField="workstationCode"
													 enabled="{refillCouponCbx.selected &amp;&amp; refillCouponRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%">
							<s:CheckBox id="capdVoucherCbx" label="CAPD Voucher" width="130"
										change="{capdVoucherCbx.selected?capdVoucherPrinterRadioBtn.selected=true:null;clearDataByType(capdVoucherPrinterTextInput, capdVoucherDropDownList)}"/>
							<s:RadioButton id="capdVoucherPrinterRadioBtn" groupName="capdVoucherRadioGroup" label="Printer" enabled="{capdVoucherCbx.selected}"/>
							<s:TextInput id="capdVoucherPrinterTextInput" maxChars="100" enabled="{capdVoucherCbx.selected &amp;&amp; capdVoucherPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="capdVoucherRedirectRadioBtn" groupName="capdVoucherRadioGroup" label="Redirect" enabled="{capdVoucherCbx.selected}"/>
							<fc:AdvancedDropDownList id="capdVoucherDropDownList" 
													 width="112"
													 labelField="workstationCode"
													 enabled="{capdVoucherCbx.selected &amp;&amp; capdVoucherRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%">
							<s:CheckBox id="reportCbx" label="Report" width="130"
										change="{reportCbx.selected?reportPrinterRadioBtn.selected=true:null;clearDataByType(reportPrinterTextInput, reportDropDownList)}"/>
							<s:RadioButton id="reportPrinterRadioBtn" groupName="reportRadioGroup" label="Printer" enabled="{reportCbx.selected}"/>
							<s:TextInput id="reportPrinterTextInput" maxChars="100" enabled="{reportCbx.selected &amp;&amp; reportPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="reportRedirectRadioBtn" groupName="reportRadioGroup" label="Redirect" enabled="{reportCbx.selected}"/>
							<fc:AdvancedDropDownList id="reportDropDownList"
													 width="112"											
													 labelField="workstationCode"
													 enabled="{reportCbx.selected &amp;&amp; reportRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%" visible="{pivasPrinterEnabledFlag}" includeInLayout="{pivasPrinterEnabledFlag}">
							<s:CheckBox id="pivasLargeLabelCbx" label="PIVAS Large Label" width="130"
										change="{pivasLargeLabelCbx.selected?pivasLargeLabelPrinterRadioBtn.selected=true:null;clearDataByType(pivasLargeLabelPrinterTextInput, pivasLargeLabelDropDownList)}"/>
							<s:RadioButton id="pivasLargeLabelPrinterRadioBtn" groupName="pivasLargeLabelRadioGroup" label="Printer" enabled="{pivasLargeLabelCbx.selected}"/>
							<s:TextInput id="pivasLargeLabelPrinterTextInput" maxChars="100" enabled="{pivasLargeLabelCbx.selected &amp;&amp; pivasLargeLabelPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="pivasLargeLabelRedirectRadioBtn" groupName="pivasLargeLabelRadioGroup" label="Redirect" enabled="{pivasLargeLabelCbx.selected}"/>
							<fc:AdvancedDropDownList id="pivasLargeLabelDropDownList"
													 width="112"
													 labelField="workstationCode"
													 enabled="{pivasLargeLabelCbx.selected &amp;&amp; pivasLargeLabelRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
						<s:HGroup width="100%" height="100%" visible="{pivasPrinterEnabledFlag}" includeInLayout="{pivasPrinterEnabledFlag}">
							<s:CheckBox id="pivasColorLabelCbx" label="PIVAS Colour Label" width="130"
										change="{pivasColorLabelCbx.selected?pivasColorLabelPrinterRadioBtn.selected=true:null;clearDataByType(pivasColorLabelPrinterTextInput, pivasColorLabelDropDownList)}"/>
							<s:RadioButton id="pivasColorLabelPrinterRadioBtn" groupName="pivasColorLabelRadioGroup" label="Printer" enabled="{pivasColorLabelCbx.selected}"/>
							<s:TextInput id="pivasColorLabelPrinterTextInput" maxChars="100" enabled="{pivasColorLabelCbx.selected &amp;&amp; pivasColorLabelPrinterRadioBtn.selected}" keyFocusChange="focusNextItem(event)"/>
							<s:RadioButton id="pivasColorLabelRedirectRadioBtn" groupName="pivasColorLabelRadioGroup" label="Redirect" enabled="{pivasColorLabelCbx.selected}"/>
							<fc:AdvancedDropDownList id="pivasColorLabelDropDownList"
													 width="112"
													 labelField="workstationCode"
													 enabled="{pivasColorLabelCbx.selected &amp;&amp; pivasColorLabelRedirectRadioBtn.selected}"
													 keyFocusChange="focusNextItem(event)"/>
						</s:HGroup>
					</s:VGroup>
				</s:Panel>
			</s:HGroup>
			<s:HGroup width="100%" height="100%" horizontalAlign="right">
				<fc:ExtendedButton id="okButton" label="OK" click="okFunc(event)"/>
				<fc:ExtendedButton id="cancelButton" label="Cancel" click="close(event)"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>