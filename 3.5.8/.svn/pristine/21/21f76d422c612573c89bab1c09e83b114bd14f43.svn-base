<?xml version="1.0" encoding="utf-8"?>
<s:NavigatorContent xmlns:fx="http://ns.adobe.com/mxml/2009" 
					xmlns:s="library://ns.adobe.com/flex/spark" 
					xmlns:mx="library://ns.adobe.com/flex/mx"
					xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
					xmlns:m="hk.org.ha.fmk.pms.flex.components.message.*"
					xmlns="hk.org.ha.view.pms.main.inbox.*"
					implements="hk.org.ha.view.pms.main.inbox.TabViewSupport">
	
	<fx:Metadata>
		[Name("pharmInboxFindPatientView")]
	</fx:Metadata>
	
	<fx:Declarations>
		<m:SystemMessagePopupProp id="recordNotFoundMsgPopupProp" messageCode="0005" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="recordNotFoundForManualProfileMsgPopupProp" messageCode="0743" setOkButtonOnly="true"/>
		<fx:Component id="specialtyMappingMissingMsgPopupPropFactory">
			<m:SystemMessagePopupProp messageCode="0712" setOkButtonOnly="true"/>
		</fx:Component>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.Sort;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			import mx.utils.StringUtil;
			
			import hk.org.ha.event.pms.main.inbox.ConstructManualProfileEvent;
			import hk.org.ha.event.pms.main.inbox.CountCategoriesEvent;
			import hk.org.ha.event.pms.main.inbox.FindMedProfileListEvent;
			import hk.org.ha.event.pms.main.inbox.FinishConstructManualProfileEvent;
			import hk.org.ha.event.pms.main.inbox.ForceProcessPendingOrdersEvent;
			import hk.org.ha.event.pms.main.inbox.ReceiveMedProfileListEvent;
			import hk.org.ha.event.pms.main.inbox.UpdateShowChemoProfileOnlyEvent;
			import hk.org.ha.event.pms.main.vetting.mp.VetMedProfileEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.utils.BarcodeReader;
			import hk.org.ha.fmk.pms.flex.utils.CaseValidator;
			import hk.org.ha.fmk.pms.flex.utils.HkidValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.fmk.pms.sys.SysProfile;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
			import hk.org.ha.model.pms.udt.medprofile.FindMedProfileListResult;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileType;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.inbox.popup.ManualProfilePopup;
			import hk.org.ha.view.pms.main.inbox.popup.ManualTransferPopup;
			import hk.org.ha.view.pms.main.medprofile.MedProfileConstants;
			
			import org.granite.tide.seam.Context;
			

			private static const LABEL_BARCODESUBMIT_DELAY:String = "label.barcodeSubmit.delay";
			
			[In]
			public var ctx:Context;
			
			[In]
			public var sysMsgMap:SysMsgMap;			

			[In] [Bindable]
			public var sysProfile:SysProfile;
			
			[In] [Bindable]
			public var propMap:PropMap;
			
			[In] [Bindable]
			public var permissionMap:PropMap;
						
			[In] [Out] [Bindable]
			public var medProfileList:ArrayCollection;
						
			[Bindable]
			public var createManualProfileEnabled:Boolean;
			
			[Bindable]
			public var hasItdSupportPermission:Boolean;
			
			[Bindable]
			private var _showChemoProfileOnly:Boolean;
			
			public function showView(tabIndex:Number, fromVetting:Boolean, triggerByTabBar:Boolean):void
			{
				clear();
				reload(tabIndex);
				_showChemoProfileOnly = ctx.showChemoProfileOnly;
			}
			
			public function hideView(triggerByTabBar:Boolean):void
			{
				if (triggerByTabBar) {
					clear();
				}
			}
			
			public function reload(tabIndex:Number):void
			{
				dispatchEvent(new CountCategoriesEvent());
			}
			
			public function get reloadOnReceiveNotification():Boolean
			{
				return false;
			}			
			
			public function get supportShowAllWards():Boolean
			{
				return false;
			}
			
			public function showAllWards(showAllWards:Boolean):void
			{
			}
			
			public function get supportShowChemoProfileOnly():Boolean
			{
				return true;
			}
			
			public function showChemoProfileOnly(showChemoProfileOnly:Boolean):void
			{
				dispatchEvent(new UpdateShowChemoProfileOnlyEvent(showChemoProfileOnly));
				_showChemoProfileOnly = showChemoProfileOnly;
				if (medProfileList != null) {
					medProfileList.refresh();
				}
			}
			
			public function get supportSorting():Boolean
			{
				return false;
			}
			
			public function get sortPopupState():String
			{
				return null;
			}
			
			public function sort(sortType:String):void
			{
			}
			
			public function get supportRetrieve():Boolean
			{
				return true;
			}
			
			public function retrieve():void
			{
				findMedProfileList();
			}
			
			public function get supportClear():Boolean
			{
				return true;
			}
			
			public function clear():void
			{
				createManualProfileEnabled = false;
				medProfileList = null;
				if (dataGrid != null) {
					callLater(dataGrid.drawFocus, [false]);
				}
				callLater(clearHkidCaseNumInput);
			}
			
			public function clearHkidCaseNumInput():void
			{
				if (hkidCaseNumInput != null) {
					hkidCaseNumInput.text = "";
					hkidCaseNumInput.errorString = "";
					callLater(hkidCaseNumInput.setFocus);
				} else {
					callLater(clearHkidCaseNumInput);
				}
			}
			
			public function findMedProfileList():void
			{
				medProfileList = null;
				hkidCaseNumInput.errorString = "";
				var inputStr:String = StringUtil.trim(hkidCaseNumInput.text);
				if (!HkidValidator.isHkidValid(inputStr) && !CaseValidator.isCaseNumValid(inputStr)) {
					hkidCaseNumInput.errorString = sysMsgMap.getMessage("0034");
				} else {
					dispatchEvent(new FindMedProfileListEvent(inputStr));
				}
			}
			
			public function applySorting():void {
				
				medProfileList.sort = new Sort();
				medProfileList.sort.compareFunction = function(a:Object, b:Object, fields:Array = null):int {
					return ObjectUtil.numericCompare(getStatusIndex(a.status), getStatusIndex(b.status));
				}
				medProfileList.filterFunction = function(item:Object):Boolean {
					return !_showChemoProfileOnly || item.type == MedProfileType.Chemo;
				}
				medProfileList.refresh();
			}
			
			private function getStatusIndex(status:MedProfileStatus):int {
				switch (status) {
					case MedProfileStatus.Active:
						return 0;
					case MedProfileStatus.HomeLeave:
						return 1;
					case MedProfileStatus.PendingDischarge:
						return 2;
					case MedProfileStatus.Inactive:
						return 3;
				}
				return 4;
			}
			
			[Observer]
			public function receiveMedProfileList(evt:ReceiveMedProfileListEvent):void
			{
				applySorting();
				
				var searchByCaseNum:Boolean = CaseValidator.isCaseNumValid(StringUtil.trim(hkidCaseNumInput.text));
				var firstMedProfile:MedProfile = (medProfileList.length <= 0 ? null : medProfileList.getItemAt(0)) as MedProfile;
				
				if (evt.result == FindMedProfileListResult.NoProfileFound) {
					createManualProfileEnabled = true;
					if (searchByCaseNum) {
						createManualProfile();
					} else {
						dispatchEvent(new RetrieveSystemMessageEvent(recordNotFoundForManualProfileMsgPopupProp));
					}
				} else if (medProfileList.length == 1 && firstMedProfile.status != MedProfileStatus.Inactive &&
					(searchByCaseNum || firstMedProfile.medCase.caseNum == null)) {
					createManualProfileEnabled = false;
					if (searchByCaseNum && !hasItdSupportPermission) {
						vetting(firstMedProfile);
					} else {
						selectFirstMedProfile();
					}
				} else {
					createManualProfileEnabled = !searchByCaseNum && isAllCaseNumBasedProfile();
					if (medProfileList.length == 1) {
						selectFirstMedProfile();
					}								
				}
			}
			
			private function isAllCaseNumBasedProfile():Boolean {
				
				for each (var medProfile:MedProfile in medProfileList) {
					if (medProfile.medCase.caseNum == null) {
						return false;
					}
				}
				return true;
			}
			
			public function createManualProfile():void 
			{
				var inputStr:String = StringUtil.trim(hkidCaseNumInput.text);
				var hkid:String = HkidValidator.isHkidValid(inputStr) ? inputStr : null;
				var caseNum:String = CaseValidator.isCaseNumValid(inputStr) ? inputStr : null;

				if (hkid == null && caseNum == null) {
					return;
				}
					
				dispatchEvent(new ConstructManualProfileEvent(hkid, caseNum));
			}
			
			private function selectFirstMedProfile():void
			{
				dataGrid.selectedIndex = 0;
				callLater(function():void {
					dataGrid.setFocus();
				});
			}
			
			[Observer]
			public function finishConstructManualProfile(evt:FinishConstructManualProfileEvent):void
			{
				var medProfile:MedProfile = evt.medProfile;
				
				if (medProfile.medCase.pasWardCode != null) {
					if (medProfile.wardCode == null) {
						var popupProp:SystemMessagePopupProp = specialtyMappingMissingMsgPopupPropFactory.newInstance();
						popupProp.messageParams = [medProfile.medCase.pasSpecCode, medProfile.medCase.pasWardCode];
						dispatchEvent(new RetrieveSystemMessageEvent(popupProp));
					} else {
						vetting(medProfile);
					}
				} else {
					var manualProfilePopup:ManualProfilePopup = PopUpManager.createPopUp(
						parentApplication as DisplayObject, ManualProfilePopup, true) as ManualProfilePopup;
					manualProfilePopup.setupUI(medProfile, vetting, clear);
				}
			}
			
			public function vetSelectedItem(evt:ListEvent):void 
			{
				var medProfile:MedProfile = evt.currentTarget.selectedItem;
				evt.currentTarget.selectedItem = null;
				vetting(medProfile);
			}
			
			public function vetting(medProfile:MedProfile):void {
				dispatchEvent(new VetMedProfileEvent(medProfile));
			}
			
			public function manualTransfer():void
			{
				var medProfile:MedProfile = dataGrid.selectedItem as MedProfile;
				var manualTransferPopup:ManualTransferPopup = PopUpManager.createPopUp(
					parentApplication as DisplayObject, ManualTransferPopup, true) as ManualTransferPopup;
				manualTransferPopup.setupUI(medProfile, clear);
			}
			
			public function forceProcessPendingOrders():void
			{
				dispatchEvent(new ForceProcessPendingOrdersEvent(dataGrid.selectedItem as MedProfile, clear));
			}
			
			public function hkidCaseNumInput_keyUpHandler(evt:KeyboardEvent):void
			{
				if (!PopupUtil.isAnyVisiblePopupExists() && hkidCaseNumInput.enabled && evt.charCode == Keyboard.ENTER) {
					retrieve();
				}
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="{permissionMap.getValueAsBoolean(MedProfileConstants.PERMISSION_ITD_SUPPORT)}" destination="hasItdSupportPermission"/>

	<s:VGroup gap="5" width="100%" height="100%">
		<s:Panel title="Search Criteria" width="100%" height="80">
			<s:HGroup width="100%" gap="10" paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" verticalAlign="bottom">
				<s:Label text="HKID / Case No."/>
				<fc:UppercaseTextInput id="hkidCaseNumInput" maxChars="12" enabled="{!createManualProfileEnabled}"
									   keyUp="BarcodeReader.scanEnterBarcodeWithCallbackParam(event,propMap.getValueAsInteger(LABEL_BARCODESUBMIT_DELAY),hkidCaseNumInput_keyUpHandler)"/>
				<s:Label width="100%"/>
				<s:HGroup visible="{hasItdSupportPermission}" includeInLayout="{hasItdSupportPermission}"
						  enabled="{dataGrid.selectedItem != null}">
					<fc:ExtendedButton label="Force process Pending Order(s)" click="forceProcessPendingOrders()"/>
					<fc:ExtendedButton label="Refresh Profile Setting" click="manualTransfer()"/>
				</s:HGroup>					
			</s:HGroup>
		</s:Panel>
		<PharmInboxFindPatientDataGrid id="dataGrid" width="100%" height="100%" dataProvider="{medProfileList}"
									   doubleClickEnabled="true" itemDoubleClick="vetSelectedItem(event)"/>
	</s:VGroup>
</s:NavigatorContent>
