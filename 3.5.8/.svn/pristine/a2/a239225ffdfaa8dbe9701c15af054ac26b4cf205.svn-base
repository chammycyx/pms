package hk.org.ha.model.pms.udt.medprofile;

import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum ActionLogType implements StringValuedEnum {

	Suspend("S", "Suspend"),
	ResumeSuspend("RS", "ResumeSuspend"),
	Pending("P", "Pending"),
	Override("O", "Override"),
	ResumePending("RP", "ResumePending");
	
    private final String dataValue;
    private final String displayValue;
    
    public static final List<ActionLogType> suspend_Status = Arrays.asList(Suspend,ResumeSuspend);
    public static final List<ActionLogType> pending_Status = Arrays.asList(Pending,Override,ResumePending);
    
	private ActionLogType(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
    public static ActionLogType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ActionLogType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ActionLogType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ActionLogType> getEnumClass() {
    		return ActionLogType.class;
    	}
    }
}
