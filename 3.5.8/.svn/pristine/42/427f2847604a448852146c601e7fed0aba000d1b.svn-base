package hk.org.ha.model.pms.vo.rx;

import hk.org.ha.model.pms.udt.disp.DupChkDrugType;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PharmDrug implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String itemCode;
	
	@XmlElement
	private BigDecimal doseQty;

	@XmlElement
	private String doseUnit;
	
	@XmlElement
	private BigDecimal calQty;

	@XmlElement
	private Integer calDuration;

	@XmlElement
	private String baseUnit;

	@XmlElement(name="Regimen")
	private Regimen regimen;

	@XmlElement
	private Integer drugKey;

	@XmlElement
	private String drugName;

	@XmlElement
	private String formCode;

	@XmlElement
	private String formLabelDesc;

	@XmlElement
	private String formVerb;
	
	@XmlElement
	private String strength;

	@XmlElement
	private String volumeText;

	@XmlElement
	private String warnCode1;

	@XmlElement
	private String warnCode2;
	
	@XmlElement
	private String warnCode3;

	@XmlElement
	private String warnCode4;
	
	@XmlElement
	private Boolean dangerDrugFlag;
	
	@XmlElement
	private Integer additiveNum;
	
	@XmlElement
	private Integer fluidNum;
	
	// for duplicate drug check
	
	@XmlElement
	private DupChkDrugType drugType;
	
	@XmlElement
	private String displayName;

	// only for transfer

	@XmlElement
	private Integer moIssueQty;

	@XmlElement
	private String moBaseUnit;

	// for IPMOE only
	
	@XmlElement
	private Integer itemNum;	
	
	// for EPR
	
	@XmlElement
	private String routeCode;
	
	@XmlElement
	private String routeDesc;
	
	// for CDDH
	@XmlElement
	private String tradeName;
	
	// for calculation of insulin items' issue Qty.
	@XmlElement
	private Boolean dispDurationModified;
	
	// for insulin qty. conversion disp. duration calculation
	@XmlElement
	private Boolean dispDurationCalcFlag;
	
	// for supporting refill order with number of doses specified by prescriber
	@XmlElement
	private BigDecimal totalIssueQty;
	
	@XmlElement//from CARS2 IPMOE or Non-IPMOE
	private Boolean calBothIssueQtyAndTotalQtyForDoseSpecified;
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public BigDecimal getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(BigDecimal doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public BigDecimal getCalQty() {
		return calQty;
	}

	public void setCalQty(BigDecimal calQty) {
		this.calQty = calQty;
	}
	
	public Integer getCalDuration() {
		return calDuration;
	}

	public void setCalDuration(Integer calDuration) {
		this.calDuration = calDuration;
	}	

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Regimen getRegimen() {
		return regimen;
	}

	public void setRegimen(Regimen regimen) {
		this.regimen = regimen;
	}

	public Integer getDrugKey() {
		return drugKey;
	}

	public void setDrugKey(Integer drugKey) {
		this.drugKey = drugKey;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getFormVerb() {
		return formVerb;
	}

	public void setFormVerb(String formVerb) {
		this.formVerb = formVerb;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}

	public String getWarnCode1() {
		return warnCode1;
	}

	public void setWarnCode1(String warnCode1) {
		this.warnCode1 = warnCode1;
	}

	public String getWarnCode2() {
		return warnCode2;
	}

	public void setWarnCode2(String warnCode2) {
		this.warnCode2 = warnCode2;
	}

	public String getWarnCode3() {
		return warnCode3;
	}

	public void setWarnCode3(String warnCode3) {
		this.warnCode3 = warnCode3;
	}

	public String getWarnCode4() {
		return warnCode4;
	}

	public void setWarnCode4(String warnCode4) {
		this.warnCode4 = warnCode4;
	}

	public Boolean getDangerDrugFlag() {
		return dangerDrugFlag;
	}

	public void setDangerDrugFlag(Boolean dangerDrugFlag) {
		this.dangerDrugFlag = dangerDrugFlag;
	}

	public Integer getAdditiveNum() {
		return additiveNum;
	}

	public void setAdditiveNum(Integer additiveNum) {
		this.additiveNum = additiveNum;
	}

	public Integer getFluidNum() {
		return fluidNum;
	}

	public void setFluidNum(Integer fluidNum) {
		this.fluidNum = fluidNum;
	}

	public DupChkDrugType getDrugType() {
		return drugType;
	}

	public void setDrugType(DupChkDrugType drugType) {
		this.drugType = drugType;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getMoIssueQty() {
		return moIssueQty;
	}

	public void setMoIssueQty(Integer moIssueQty) {
		this.moIssueQty = moIssueQty;
	}

	public String getMoBaseUnit() {
		return moBaseUnit;
	}

	public void setMoBaseUnit(String moBaseUnit) {
		this.moBaseUnit = moBaseUnit;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public void setDispDurationModified(Boolean dispDurationModified) {
		this.dispDurationModified = dispDurationModified;
	}

	public Boolean getDispDurationModified() {
		return dispDurationModified;
	}

	public void setDispDurationCalcFlag(Boolean dispDurationCalcFlag) {
		this.dispDurationCalcFlag = dispDurationCalcFlag;
	}

	public Boolean getDispDurationCalcFlag() {
		return dispDurationCalcFlag;
	}

	public void setTotalIssueQty(BigDecimal totalIssueQty) {
		this.totalIssueQty = totalIssueQty;
	}

	public BigDecimal getTotalIssueQty() {
		return totalIssueQty;
	}

	public void setCalBothIssueQtyAndTotalQtyForDoseSpecified(
			Boolean calBothIssueQtyAndTotalQtyForDoseSpecified) {
		this.calBothIssueQtyAndTotalQtyForDoseSpecified = calBothIssueQtyAndTotalQtyForDoseSpecified;
	}

	public Boolean getCalBothIssueQtyAndTotalQtyForDoseSpecified() {
		return calBothIssueQtyAndTotalQtyForDoseSpecified;
	}
}