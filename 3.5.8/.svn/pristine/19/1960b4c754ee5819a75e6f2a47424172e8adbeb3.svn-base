<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="340" height="130">
	
	<fx:Metadata>
		[Name("dispHistoryEnqRemarkPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.cddh.CheckDeleteCddhRemarkStatusEvent;
			import hk.org.ha.event.pms.main.cddh.RefreshCddhEnquiryMedicationEvent;
			import hk.org.ha.event.pms.main.cddh.UpdateDispOrderItemRemarkEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.udt.disp.DispOrderItemRemarkType;
			import hk.org.ha.model.pms.vo.cddh.CddhDispOrderItem;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[Bindable]
			private var cddhDispOrderItem:CddhDispOrderItem;
			
			[Bindable]
			public var remarkTypeList:ArrayCollection;
			
			private var REMARK_TEXT_TOTAL_LENGTH:int = 40;
			
			public function showSystemMessage(errorCode:String, oKfunc:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if (oKfunc != null)
				{
					msgProp.okHandler = oKfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function initRemarkTypeList():void {
				remarkTypeList = new ArrayCollection(DispOrderItemRemarkType.constants);
				remarkTypeList.removeItemAt(0);
			}
			
			private function close(evt:Event):void {
				if (remarkType.selectedItem == DispOrderItemRemarkType.Others) {
					if (remarkText.text == null || StringUtil.trim(remarkText.text) == "") {
						showSystemMessage("0476", function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							remarkText.setFocus();
						});
						return;
					}
				}
				
				if (remarkType.selectedItem == null) {
					showSystemMessage("0472", function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						remarkType.setFocus();
						focusManager.showFocus();
					});
				} else {
					cddhDispOrderItem.doiRemarkType = remarkType.selectedItem;
					cddhDispOrderItem.doiRemarkText = remarkText.text;
					this.dispatchEvent(new UpdateDispOrderItemRemarkEvent(cddhDispOrderItem));
					PopUpManager.removePopUp(this);
				}
			}
			
			private function cancelEvent(evt:Event):void{
				dispatchEvent(new RefreshCddhEnquiryMedicationEvent(null, false));
				PopUpManager.removePopUp(this);
			}
			
			public function setCddhDispOrderItem(inCddhDispOrderItem:CddhDispOrderItem):void{
				this.cddhDispOrderItem = inCddhDispOrderItem;
				
				updateRemarkTextProperty();
				
				focusManager.showFocus();
			}
			
			public function onRemarkTypeChange():void {
				remarkText.text = "";
				updateRemarkTextProperty();
			}
			
			private function updateRemarkTextProperty():void {
				if (remarkType.selectedItem != null) {
					if (remarkType.selectedItem == DispOrderItemRemarkType.Empty){
						remarkText.enabled = false;
					} else {
						remarkText.enabled = true;
						if (remarkType.selectedItem == DispOrderItemRemarkType.Others){
							remarkText.maxChars = 34;
						} else {
							remarkText.maxChars = REMARK_TEXT_TOTAL_LENGTH - remarkType.selectedItem.displayValue.toString().length;
						}
					}
				}
			}
			
			public function setInitFocus():void {
				remarkType.setFocus();
			}
			
			private function deleteRemarkClickHandler(event:Event):void {
				this.dispatchEvent(new CheckDeleteCddhRemarkStatusEvent(cddhDispOrderItem));
				PopUpManager.removePopUp(this);
			}
			
			private function pressEnterShortcutKey(evt:KeyboardEvent):void {
				if (evt.keyCode == Keyboard.ENTER) {
					close(evt);
				}
			}
			
		]]>
	</fx:Script>
	<s:Panel width="100%" height="100%" title="CDDH Remark">
		<s:layout>
			<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
		</s:layout>
		<fc:AdvancedDropDownList id="remarkType" width="100%" dataProvider="{remarkTypeList}" labelField="displayValue" selectedItem="{cddhDispOrderItem.doiRemarkType}" change="onRemarkTypeChange()" keyUp="pressEnterShortcutKey(event)"/>
		<fc:UppercaseTextInput id="remarkText" width="100%" restrict="\u0020-\u007E" text="{cddhDispOrderItem.doiRemarkText}" maxChars="34" keyUp="pressEnterShortcutKey(event)"/>
		<s:HGroup width="100%" height="5%" verticalAlign="middle" horizontalAlign="right">
			<fc:ExtendedButton label="OK" id="btnOk" click="close(event)"/>
			<fc:ExtendedButton label="Delete" id="btnDelete" click="deleteRemarkClickHandler(event)" enabled="{cddhDispOrderItem.doiRemarkType!=DispOrderItemRemarkType.Empty}"/>
			<fc:ExtendedButton label="Cancel" id="btnCancel" click="cancelEvent(event)"/>
		</s:HGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>