<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:flexiframe="http://code.google.com/p/flex-iframe/"
	width="100%" height="100%">

	<fx:Metadata>
		[Name("renalAdjEnqView")]
	</fx:Metadata>
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.enquiry.PrintRenalPdfEvent;
			import hk.org.ha.event.pms.main.enquiry.RetrieveRenalDmDrugEvent;
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.lookup.popup.ItemCodeLookupPopupProp;
			import hk.org.ha.event.pms.main.lookup.popup.ShowItemCodeLookupPopupEvent;
			import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveRenalPdfByItemCodeEvent;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.dms.persistence.DmDrug;
			import hk.org.ha.model.pms.vo.drug.DmDrugLite;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.lookup.popup.ItemCodeLookupPopup;
			
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.messaging.config.ServerConfig;
			import mx.utils.StringUtil;
			
			[Bindable]
			private var reportUri:String = null;
			
			[Bindable]
			private var renalCodeDesc:String = null;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			private function clearHandler():void {
				reportUri = null;
				renalCodeDesc = null;
				itemCodeTxt.text = "";
				itemDescTxt.text = "";
				callLater(itemCodeTxt.setFocus);
			}
			
			private function closeHandler():void {
				clearHandler();
				dispatchEvent(new CloseActiveWindowEvent());
			}
			
			public override function onShowLater():void {
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.printFunc = printHandler;
					pmsToolbar.init();
					pmsToolbar.printButton.enabled = false;
				}
				if (itemCodeTxt.enabled) {
					callLater(itemCodeTxt.setFocus);
				}
				callLater(disbaleMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(keyGuideHandler);
			}
			
			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			private function keyGuideHandler():void {
				dispatchEvent(new ShowKeyGuidePopupEvent("RenalAdjEnqView"));
			}
			
			private function disbaleMenuBarCloseButton():void {
				dispatchEvent(new UpdateCloseMenuItemEvent(false));
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if (!PopupUtil.isAnyVisiblePopupExists() && evt.ctrlKey) {
					if (evt.charCode == 52 && pmsToolbar.clearButton.enabled) {	
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
					}
					else if (evt.charCode == 48 && pmsToolbar.closeButton.enabled) {
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
					else if (evt.charCode == 56 && pmsToolbar.printButton.enabled) {
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.printButton));
					}
				}
			}
			
			private function itemLookupHandler(evt:ListEvent):void {
				var itemCodeLookupPopup:ItemCodeLookupPopup = (UIComponent(evt.currentTarget).parentDocument) as ItemCodeLookupPopup;
				var selectedItem:DmDrugLite = evt.itemRenderer.data as DmDrugLite;
				itemCodeTxt.text = selectedItem.itemCode;
				itemDescTxt.text = selectedItem.fullDrugDesc;
				itemCodeTxt.errorString = null;
				retrieveRenalPdf();
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			}
			
			private function showItemCodeLookupPopup():void {
				var itemCodeLookupPopupProp:ItemCodeLookupPopupProp = new ItemCodeLookupPopupProp();
				itemCodeLookupPopupProp.doubleClickHandler = itemLookupHandler;
				itemCodeLookupPopupProp.cancelHandler = function (evt:MouseEvent):void {
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					itemCodeTxt.setFocus();
				};
				itemCodeLookupPopupProp.prefixItemCode = itemCodeTxt.text;
				dispatchEvent(new ShowItemCodeLookupPopupEvent(itemCodeLookupPopupProp));
			}
			
			private function retrieveHandler():void {
				if (StringUtil.trim(itemCodeTxt.text).length == 0) {
					itemCodeTxt.errorString = sysMsgMap.getMessage("0375");
					itemCodeTxt.setFocus();
					return;
				}
				
				if (StringUtil.trim(itemCodeTxt.text).length != 6) {
					itemCodeTxt.errorString = sysMsgMap.getMessage("0030");
					itemCodeTxt.setFocus();
					return;
				}
				
				dispatchEvent(new RetrieveRenalDmDrugEvent(StringUtil.trim(itemCodeTxt.text), function(dmDrug:DmDrug):void {
					if (dmDrug == null) {
						itemCodeTxt.errorString = sysMsgMap.getMessage("0030");
						return;
					}
					itemDescTxt.text = dmDrug.fullDrugDesc;
					retrieveRenalPdf();
				}));
			}
			
			private function retrieveRenalPdf():void {
				
				dispatchEvent(new RetrieveRenalPdfByItemCodeEvent(StringUtil.trim(itemCodeTxt.text), function(renalCode:String, reviewDate:Date):void{

					reportUri = null;
					renalCodeDesc = getRenalCodeDesc(renalCode, reviewDate);
					if (renalCode == "01" || renalCode == "03") {
						refreshPdf();
					}

				}));
			}
			
			private function refreshPdf():void {
				var url:String = "document.seam?actionMethod=report.xhtml%3ArenalAdjService.generateRenalPdf";
				if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
					url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
				}
				reportUri = url;
			}
			
			private function getRenalCodeDesc(code:String, date:Date):String {
				var txt:String;
				if (code == "01") {
					txt = "Adjustment Information Available";
				}
				else if (code == "02") {
					txt = "No Adjustment Necessary";
				}
				else if (code == "03") {
					txt = "See Monograph";
				}
				else if (code == "04") {
					txt = "Adjustment Information not Available";
				}
				else {
					return "N/A";
				}
				if (date != null) {
					txt += " (Review Date: " + dateFormatter.format(date) + ")";
				}
				return txt;
			}
			
			private function lookupShortcutHandler(evt:KeyboardEvent):void {
				if (evt.charCode == 53 && evt.ctrlKey && itemCodeLookupBtn.enabled) {
					showItemCodeLookupPopup();
				}
			}
			
			private function printHandler():void {
				dispatchEvent(new PrintRenalPdfEvent());
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="{reportUri != null}"
				destination="pmsToolbar.printButton.enabled"/>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>

	<fc:Toolbar id="pmsToolbar" width="100%"/>

	<s:VGroup width="100%" height="100%"
			  gap="10" paddingLeft="10" paddingRight="10" paddingBottom="10" paddingTop="10">
		
		<s:HGroup width="100%" verticalAlign="middle">
			<s:Label text="Item Code" width="130"/>
			<fc:UppercaseTextInput id="itemCodeTxt" maxChars="6" width="100"
								   enabled="{renalCodeDesc == null}"
								   enter="retrieveHandler()"
								   keyUp="lookupShortcutHandler(event)"
								   change="itemCodeTxt.errorString = null"/>
			<fc:LookupButton id="itemCodeLookupBtn"
							 enabled="{StringUtil.trim(itemCodeTxt.text).length > 0 &amp;&amp; renalCodeDesc == null}"
							 click="showItemCodeLookupPopup()"/>
		</s:HGroup>
	
		<s:HGroup width="100%" verticalAlign="middle">
			<s:Label width="130" text="Item Description"/>
			<fc:UppercaseTextInput id="itemDescTxt" maxChars="59" width="584" enabled="false"/>
		</s:HGroup>
		
		<s:HGroup width="100%" verticalAlign="middle">
			<s:Label width="130" text="Adjustment Code"/>
			<s:TextInput enabled="false" width="584" text="{renalCodeDesc}"/>
		</s:HGroup>
		
		<flexiframe:IFrame id="reportFrame" width="100%" height="100%" overlayDetection="true"
						   loadIndicatorClass="hk.org.ha.view.pms.main.report.LoadingLabel"
						   visible="{reportUri != null}" source="{reportUri}"/>
		
	</s:VGroup>

</fc:ExtendedNavigatorContent>
