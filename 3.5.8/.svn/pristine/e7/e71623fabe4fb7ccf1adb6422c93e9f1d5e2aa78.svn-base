package hk.org.ha.model.pms.udt.onestop;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OneStopOrderStatusIndicator implements StringValuedEnum {

	Suspend("S", "S"),
	PharmacyRemark("RR", "R"),
	PharmacyremarkConfirm("RG", "R"),
	AllowModify("AM", "A"),
	AllowModifyPostComment("AP", "A"),
	AllowModifyChangePrescType("ACP", "A"),
	AllowModifyAll("ALP", "A"),
	Uncollect("U", "U");
	
    private final String dataValue;
    private final String displayValue;
        
    OneStopOrderStatusIndicator(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public static OneStopOrderStatusIndicator dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OneStopOrderStatusIndicator.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<OneStopOrderStatusIndicator> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<OneStopOrderStatusIndicator> getEnumClass() {
    		return OneStopOrderStatusIndicator.class;
    	}
    }
}
