<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MedSummaryRptDtl" language="groovy" pageWidth="480" pageHeight="802" columnWidth="480" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="hk.org.ha.model.pms.util.QrCodeHelper"/>
	<field name="itemCode" class="java.lang.String"/>
	<field name="labelDesc" class="java.lang.String"/>
	<field name="labelInstructionText" class="java.lang.String"/>
	<field name="issueQty" class="java.lang.Number"/>
	<field name="baseUnit" class="java.lang.String"/>
	<field name="warningText" class="java.lang.String"/>
	<field name="startDate" class="java.util.Date"/>
	<field name="itemXmlBarCode" class="java.lang.String"/>
	<field name="barCodeEnable" class="java.lang.Boolean"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="187" splitType="Prevent">
			<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}!=null]]></printWhenExpression>
			<line>
				<reportElement positionType="Float" x="0" y="0" width="480" height="1"/>
			</line>
			<staticText>
				<reportElement positionType="Float" x="0" y="33" width="70" height="19">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Start Date : ]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="52" width="480" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{warningText}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="380" y="67" width="100" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{issueQty}+" "+$F{baseUnit}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy" isBlankWhenNull="true">
				<reportElement positionType="Float" x="65" y="33" width="415" height="19" isPrintWhenDetailOverflows="true"/>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{startDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="17" width="480" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="DFKai-SB" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelInstructionText}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="380" y="1" width="100" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="1" width="391" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<image scaleImage="Clip" hAlign="Center" vAlign="Middle">
				<reportElement positionType="Float" x="11" y="72" width="90" height="90">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}!=null]]></printWhenExpression>
				</reportElement>
				<imageExpression class="java.io.InputStream"><![CDATA[QrCodeHelper.createQrCodeImageStream($F{itemXmlBarCode}, 90, 90)]]></imageExpression>
			</image>
			<line>
				<reportElement positionType="Float" x="0" y="170" width="480" height="1">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}!=null]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
		</band>
		<band height="101" splitType="Prevent">
			<printWhenExpression><![CDATA[!$F{barCodeEnable}]]></printWhenExpression>
			<line>
				<reportElement positionType="Float" x="0" y="1" width="480" height="1"/>
			</line>
			<staticText>
				<reportElement positionType="Float" x="0" y="34" width="70" height="19">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Start Date : ]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="53" width="480" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{warningText}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="380" y="68" width="100" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{issueQty}+" "+$F{baseUnit}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy" isBlankWhenNull="true">
				<reportElement positionType="Float" x="65" y="34" width="415" height="19" isPrintWhenDetailOverflows="true"/>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{startDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="18" width="480" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="DFKai-SB" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelInstructionText}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="380" y="2" width="100" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="2" width="391" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="85" width="480" height="1">
					<printWhenExpression><![CDATA[!$F{barCodeEnable}]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
		</band>
		<band height="143" splitType="Prevent">
			<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="17" width="480" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="DFKai-SB" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelInstructionText}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="124" width="480" height="1">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField pattern="dd-MMM-yyyy" isBlankWhenNull="true">
				<reportElement positionType="Float" x="65" y="33" width="415" height="19" isPrintWhenDetailOverflows="true"/>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{startDate}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="380" y="67" width="100" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{issueQty}+" "+$F{baseUnit}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="1" width="391" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{labelDesc}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement positionType="Float" x="380" y="1" width="100" height="16" isPrintWhenDetailOverflows="true"/>
				<textElement textAlignment="Right">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="52" width="480" height="15" isPrintWhenDetailOverflows="true"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{warningText}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="0" width="480" height="1"/>
			</line>
			<staticText>
				<reportElement positionType="Float" x="0" y="33" width="70" height="19">
					<printWhenExpression><![CDATA[$F{startDate}!=null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Start Date : ]]></text>
			</staticText>
			<rectangle>
				<reportElement positionType="Float" x="0" y="100" width="219" height="14">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement positionType="Float" x="1" y="100" width="218" height="14">
					<printWhenExpression><![CDATA[$F{barCodeEnable} && $F{itemXmlBarCode}==null]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font isItalic="true"/>
				</textElement>
				<text><![CDATA[No barcode information is available for this item.]]></text>
			</staticText>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
