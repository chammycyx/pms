<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="100%" height="100%" 
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*">
	
	<fx:Metadata>
		[Name("sendOutView")]
	</fx:Metadata>
	
	
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.DropdownEvent;
			import mx.managers.PopUpManager;
			import mx.messaging.events.MessageEvent;
			import mx.messaging.messages.AsyncMessage;
			import mx.utils.ObjectUtil;
			
			import hk.org.ha.event.pms.main.checkissue.mp.BatchSendDeliveryListEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.ReadyToSendDeliveryListEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.RetrieveSentListEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.SendDeliveryEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryDateEvent;
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.sys.RetrieveCurrentServerTimeEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.Window;
			import hk.org.ha.fmk.pms.flex.utils.BarcodeReader;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.biz.checkissue.mp.CheckIssueViewUtil;
			import hk.org.ha.model.pms.persistence.corp.Workstore;
			import hk.org.ha.model.pms.udt.medprofile.PrintMode;
			import hk.org.ha.model.pms.vo.checkissue.mp.SentDelivery;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import org.granite.gravity.Consumer;
		
			private var initView:Boolean = true;
			
			private var autoRefreshID:uint;
			
			[In]
			public var window:Window;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[In]
			public var sentDelivery:SentDelivery;
			
			[In][Bindable]
			public var sentDeliveryList:ArrayCollection;
			
			[In][Bindable]
			public var readyToSendDeliveryList:ArrayCollection;
			
			[In][Bindable]
			public var sentDeliveryFromBatchSendList:ArrayCollection;
			[In]
			public var closeMenuItem:MenuItem;
			
			[In] [Bindable]
			public var workstore:Workstore;
			
			[In]
			public var propMap:PropMap;
			
			private static var LABEL_BARCODESUBMIT_DELAY:String = "label.barcodeSubmit.delay";
			
			private var pendingReload:Boolean;
			private var consumer:Consumer = null;
			
			
			public override function onShow():void {
				dispatchEvent(new RetrieveCurrentServerTimeEvent(function(now:Date):void {
					connectGravity();
				}));					
			}
			
			public override function onShowLater():void {
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.batchSendFunc = sendBatchHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					
					var tomorrowDate:Date = new Date();
					tomorrowDate.setDate(tomorrowDate.getDate() + 1);
					batchDate.disabledRanges = [{rangeStart:tomorrowDate}]; 
				}
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				
				
				
				if ( initView ) {
					retrieveHandler();
					clearHandler();
					initView = false;
				}
				
				delayProcessTimer = new Timer(300,1);
				delayProcessTimer.addEventListener("timer",delayProcessTimerHandler);
				
				
				customizedCallLater(batchDate.setFocus);
				callLater(disableMenuBarCloseButton);
				parentApplication.setKeyGuideHandler(showSendOutShortcutKeyHandler);
				
				//force screen to reload 
				reloadView();
			}
			
			private function delayProcessTimerHandler(event:TimerEvent):void
			{
				if ( delayProcessFunction != null ) {
					delayProcessFunction();
					delayProcessFunction = null;
				}
			}
			
			private var delayProcessTimer:Timer;
			
			private var delayProcessFunction:Function;
			public function customizedCallLater(value:Function, delayValue:int=50):void
			{
				delayProcessTimer.reset();
				delayProcessFunction 			= value;
				delayProcessTimer.delay 		= delayValue;
				delayProcessTimer.repeatCount 	= 1;
				delayProcessTimer.start();
			}
			
			public function connectGravity():void 
			{
				disconnectGravity();
				if (consumer == null) {
					consumer = new Consumer();
					consumer.destination = "pharm-send-out-gravity";
					consumer.selector = "HOSP_CODE = '" + workstore.hospCode + "'";
					consumer.addEventListener(MessageEvent.MESSAGE, updateNotificationHandler);
				}
				consumer.subscribe();
			}
			
			public function disconnectGravity():void 
			{
				if (consumer != null && consumer.subscribed) {
					consumer.unsubscribe();
				}
			}			
			
			private function updateNotificationHandler(event:MessageEvent):void 
			{
				var msg:AsyncMessage = event.message as AsyncMessage;
				
				if (!pendingReload)
				{
					pendingReload = true;
					setTimeout(reloadView, Math.random() * 2000);
				}
			}
			
			private function reloadView():void 
			{
				refreshReadySendOutList();
				pendingReload = false;
			}
			
			
			private function showSendOutShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("SendOutView"));
			}
			
			private function disableMenuBarCloseButton():void {
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
			}
			
			private function closeHandler():void {
				clearHandler();
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			public override function onHide():void 
			{
				stage.focus = null;
				dispatchEvent(new RetrieveCurrentServerTimeEvent(function(now:Date):void {disconnectGravity()}));
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				delayProcessTimer.removeEventListener("timer",delayProcessTimerHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			public function updateReadySendOutListCounterHandler():void{
				readyToSendDeliveryList.refresh();
				updateCounterFunction();
			}
			
			
			private function retrieveHandler():void{
				currentBatchNum.text ="";
				currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerNoColorStyle'
				dispatchEvent(new RetrieveSentListEvent() );
				refreshReadySendOutList();
			}
			
			private function sendHandler():void {
				
				if ( searchCriteriaValidation() ) {
					dispatchEvent(new SendDeliveryEvent(batchDate.selectedDate, batchCode.text.substring(2,6) ));
					
				} else if (batchDate.errorString != null && batchDate.errorString != "" ){
					this.batchCode.setFocus();//for select whole batchDate text
					callLater(batchDate.setFocus);
				}
			}
			
			private function searchCriteriaValidation():Boolean {
				clearErrorString();
				if ( !CheckIssueViewUtil.searchCriteriaValid(batchDate,batchCode, sysMsgMap, workstore) ) {
					return false;
				}
				
				var inputformat:String = CheckIssueViewUtil.extractInputFormatValidation(batchDate,batchCode);
				if ( inputformat != "barCode" && inputformat != "batchCode" ) {
					batchCode.errorString = sysMsgMap.getMessage("0650"); 
					batchCode.selectAll();
					return false;
				}
				return true;
			}
			
			
			private function sendBatchHandler():void {
				
				stage.focus = null;
				if(readyToSendDeliveryList != null && readyToSendDeliveryList.length > 0){
					dispatchEvent(new BatchSendDeliveryListEvent(readyToSendDeliveryList) );
				}
				
				
			}
			
			public function sentBatchResult():void{
				currentBatchNum.text = "";
				currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerNoColorStyle'
				updateSendOutHistoryFromBatchSend();
			}
			

			
			private function clearErrorString():void {
				batchDate.errorString = "";
				batchCode.errorString = "";
			}
			
			public function clearHandler():void{
				batchCode.setFocus(); //for select whole batchDate text
				updateCounterFunction();
				clearErrorString();
				batchDate.selectedDate = new Date();
				batchCode.text = "";
				currentBatchNum.text ="";
				currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerNoColorStyle';
				if (readyToSendDeliveryList != null){	
					
					for each(var sentDeliveryToUpdate :SentDelivery  in readyToSendDeliveryList){
						sentDeliveryToUpdate.checkedToSend = false;
					}
					readySendOutHistoryGrid.selectedIndex = 0;
					readyToSendDeliveryList.sort = null;
					readyToSendDeliveryList.refresh();
					
				}
				callLater(batchDate.setFocus);
			}
			
			public function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( evt.charCode == 52 && evt.ctrlKey &&  !PopupUtil.isAnyVisiblePopupExists() ){	
					dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
				} else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled && !PopupUtil.isAnyVisiblePopupExists() ){	//ctrl + 0 - close
					dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
				}
			}
			
			public function sendResult():void{
				currentBatchNum.text =   batchCode.text;

				if (focusManager.getFocus()== batchDate) {	
					this.stage.focus = null;
					batchDate.setFocus();
				} else {
					this.stage.focus = null;
					batchCode.setFocus();
				}
				
				if ( sentDelivery == null || sentDelivery.msgCode == "0005" ) {
					clearHandler();
					showSystemMessage("0005");
					return;
				}
				
				if ( sentDelivery.msgCode == "0286" ) {
					currentBatchNum.text ="";
					currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerPurpleStyle';
					showSystemMessage("0287", updateDeliveryDateHandler, updateSendOutHistoryHandler);
					return;
				}
				
				//KEEP RECORD / DELETED BATCH
				if ( sentDelivery.msgCode == "0659" ) {
					currentBatchNum.text ="";
					currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerRedStyle';
				}else if(sentDelivery.msgCode == "0285" ){
					currentBatchNum.text ="";
					currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerAquaStyle';
				}else{
					currentBatchNumBorderContainer.styleName ='mpSendOutViewCurrentBatchNumBorderContainerGreenStyle';
				}
				
				updateSendOutHistory(sentDelivery);
				
			}
			
			
			private function updateSendOutHistoryFromBatchSend():void{
				clearErrorString();
				
				if(sentDeliveryFromBatchSendList != null){
					for each( var sentDeliveryToUpdate :SentDelivery  in sentDeliveryFromBatchSendList ) {
						sentDeliveryList.addItemAt(sentDeliveryToUpdate, 0);
					}
				}
				
				sentDeliveryList.sort = null;
				sentDeliveryList.refresh();
				
				readyToSendDeliveryList.refresh();
				refreshReadySendOutList();
				updateCounterFunction();
				sendOutHistoryGrid.selectedIndex = -1;
				
			}
			
			private function updateSendOutHistory(sentDelivery:SentDelivery):void{
				clearErrorString();
				
				sentDeliveryList.addItemAt(sentDelivery, 0);
				sentDeliveryList.sort = null;
				sentDeliveryList.refresh();
				
				readyToSendDeliveryList.refresh();
				refreshReadySendOutList();
				updateCounterFunction();
				sendOutHistoryGrid.selectedIndex = -1;
				
			}
			
			private function updateDeliveryDateHandler(evt:MouseEvent):void{
				dispatchEvent( new UpdateDeliveryDateEvent( sentDelivery ) );
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			}
			
			private function updateSendOutHistoryHandler(evt:MouseEvent):void{
				clearHandler();
				updateSendOutHistory(sentDelivery);
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			}
			
			public function showSystemMessage(errorCode:String, yesHandler:Function=null, noHandler:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if ( yesHandler != null ) {
					msgProp.setYesNoButton = true;
					msgProp.yesHandler = yesHandler;
					if ( noHandler != null ) {
						//PMS-3534
						msgProp.setYesDefaultButton = false;
						msgProp.setNoDefaultButton = true;
						msgProp.noHandler = noHandler;
					}
					
				} else {
					
					msgProp.okHandler = function(mouseEvent:MouseEvent):void{
						clearHandler();
						PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					}
						
					msgProp.setOkButtonOnly = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function batchModeLabelFunc(item:SentDelivery, column:DataGridColumn):String {
				if ( item == null || item.printMode == null ) {
					return "";
				}
				if ( item.printMode == PrintMode.Batch ) {
					return "Y";
				} else {
					return "N";
				}
			}
			
			public function batchDateLabelFuncForReadyList(batchDate:Date):String{
				return dateFormatter.format(batchDate);
			}
			
			private function batchDateLabelFunc(item:Object, column:DataGridColumn):String {
				
				return dateFormatter.format(item[column.dataField]);
			}
			
			private function dateTimeLabelFunc(item:Object, column:DataGridColumn):String {
				return dateTimeFormatter.format(item[column.dataField]);
			}
			
			private function criteriaFieldKeyBoardFunc(event:KeyboardEvent):void{
				if(event.keyCode==13 && batchCode.text =="" && batchDate.text ==""){
					batchCode.errorString = sysMsgMap.getMessage("0486");
					batchCode.setFocus();
					return;
				}
			}
			
			
			private function validateDateField(evt:Event):void {
				
				
				batchDate.errorString = null;
				if ( !ExtendedDateFieldValidator.isValidDate(batchDate) ) {
					
					if(  batchDate.selectedDate == null){
						batchDate.errorString = sysMsgMap.getMessage("0001");
						return; 
					}
				}else {
					var todayDateVal : Date = new Date();
					if(CheckIssueViewUtil.compareDate( batchDate.selectedDate, todayDateVal ) > 0 ){
						batchDate.errorString = sysMsgMap.getMessage("0649");
						return;
					}
				}
				
				
			}
			
			
			private function refreshReadySendOutList():void{
				dispatchEvent(new ReadyToSendDeliveryListEvent() );
			}
			
			
			public function updateCounterFunction():void{
				
				if(readyToSendDeliveryList != null){
					readyForSendingOutListPanel.title = "Ready for Sending Out List (" + readyToSendDeliveryList.length + ")";
				}
				
			}
			
			private function batchNumLabelFunc(data:SentDelivery, column:DataGridColumn):String{
				var dateString : String = data.batchDate.date.toString();
				
				if(dateString.length == 1){
					dateString = '0' + dateString;
				}
				var todayDate: Date = new Date();
				
				return dateString + data.batchNum;
				
			}
			
			private function batchDateKeyBoardFunc(event:KeyboardEvent):void {
				if(event.keyCode==13){
					readBarcodeHandler(event);
				}
				
			}
			

			public function readBarcodeHandler(evt:KeyboardEvent):void{
				
				BarcodeReader.scanBarcode(evt, propMap.getValueAsInteger(LABEL_BARCODESUBMIT_DELAY), sendHandler);
			}
			

			protected function batchDateSortFunc(object1:Object, object2:Object):int
			{
				var batchDate1:String = object1.batchDate.getTime().toString();
				var batchDate2:String = object2.batchDate.getTime().toString();
				
				var result:int = compareFunc(batchDate1, batchDate2);
				if ( result == 0 ) {	
					if( bDate.sortDescending ) {
						return (-1)*compareFunc(object1.batchNum, object2.batchNum);
					} else {
						return compareFunc(object1.batchNum, object2.batchNum);
					}
				} else {
					return result;
				}					
				return 0;
			}
			
			protected function batchNumSortFunc(object1:Object, object2:Object):int
			{
				var batchNum1:String = bNum.labelFunction(object1, bNum);
				var batchNum2:String = bNum.labelFunction(object2, bNum);
				
				var result:int = compareFunc(batchNum1, batchNum2);
				if ( result == 0 ) {
					if( bNum.sortDescending ) {
						return (-1)*compareFunc(object1.batchDate, object2.batchDate);
					} else {
						return compareFunc(object1.batchDate, object2.batchDate);
					}
				} else {
					return result;
				}
				return 0;
			}
			
			protected function printModeSortFunc(object1:Object, object2:Object):int
			{
				var printMode1:String = pMode.labelFunction(object1, pMode);
				var printMode2:String = pMode.labelFunction(object2, pMode);
				var batchNum1:String = bNum.labelFunction(object1, bNum);
				var batchNum2:String = bNum.labelFunction(object2, bNum);
				
				var result:int = compareFunc(printMode1, printMode2);
				if ( result == 0 ) {
					if( pMode.sortDescending ) {
						return (-1)*batchDateSortFunc(object1,object2);
					} else {
						return batchDateSortFunc(object1,object2);
					}
				} else {
					return result;
				}
				return 0;
			}
			
			protected function wardSortFunc(object1:Object, object2:Object):int
			{
				var ward1:String = object1.wardCode;
				var ward2:String = object2.wardCode;
				var batchNum1:String = bNum.labelFunction(object1, bNum);
				var batchNum2:String = bNum.labelFunction(object2, bNum);
				
				var result:int = compareFunc(ward1, ward2);
				if ( result == 0 ) {
					if( wardId.sortDescending ) {
						return (-1)*batchDateSortFunc(object1,object2);
					} else {
						return batchDateSortFunc(object1,object2);
					}
				} else {
					return result;
				}

			}
			
			protected function dateTimeSortFunc(object1:Object, object2:Object):int
			{
				
				var dateTime1:String = dateTimeId.labelFunction(object1, dateTimeId);
				var dateTime2:String = dateTimeId.labelFunction(object2, dateTimeId);
				var batchNum1:String = bNum.labelFunction(object1, bNum);
				var batchNum2:String = bNum.labelFunction(object2, bNum);


				dateTime1 = convertMilliSecondTimeString(dateTime1);
				dateTime2 = convertMilliSecondTimeString(dateTime2);
				var result:int = compareFunc(dateTime1, dateTime2);
				if ( result == 0 ) {
					if( dateTimeId.sortDescending ) {
						return (-1)*batchDateSortFunc(object1,object2);
					} else {
						return batchDateSortFunc(object1,object2);
					}
				} else {
					return result;
				}
				return 0;
			}
			
			
			private function parsingDateToTime(i:int, dateStr:String):String{
				return new Date( dateStr.substring(7, 11) ,i,dateStr.substring(0, 2), dateStr.substring(12, 14), dateStr.substring(15, 17)).getTime().toString();
				
			}
			
			private function convertMilliSecondTimeString(str1:String):String{
				//01234567890123456789
				//DD-MMM-YYYY HH:NN
				str1 = str1.toUpperCase();
				if(str1.substring(3,6) =="JAN"){
					return parsingDateToTime(0,str1);
				}else if(str1.substring(3,6) =="FEB"){
					return parsingDateToTime(1,str1);
				}else if(str1.substring(3,6) =="MAR"){
					return parsingDateToTime(2,str1);
				}else if(str1.substring(3,6) =="APR"){
					return parsingDateToTime(3,str1);
				}else if(str1.substring(3,6) =="MAY"){
					return parsingDateToTime(4,str1);
				}else if(str1.substring(3,6) =="JUN"){
					return parsingDateToTime(5,str1);
				}else if(str1.substring(3,6) =="JUL"){
					return parsingDateToTime(6,str1);
				}else if(str1.substring(3,6) =="AUG"){
					return parsingDateToTime(7,str1);
				}else if(str1.substring(3,6) =="SEP"){
					return parsingDateToTime(8,str1);
				}else if(str1.substring(3,6) =="OCT"){
					return parsingDateToTime(9,str1);
				}else if(str1.substring(3,6) =="NOV"){
					return parsingDateToTime(10,str1);
				}else if(str1.substring(3,6) =="DEC"){
					return parsingDateToTime(11,str1);
				}
				return "";
			}
			
			private function compareFunc(str1:String, str2:String):int 
			{
				return ObjectUtil.compare(str1, str2);
			}
			
			
			public function batchDateStyleFunc(batchDate:Date):String{
				
				var toDayDate:Date = new Date();
				batchDate.month != toDayDate.month
				if ( !(batchDate.fullYear == toDayDate.fullYear && batchDate.month == toDayDate.month && batchDate.date ==  toDayDate.date) ) {
					return 'drugCheckLabelStyle';
					
				}else{
					return '';
				}
				
			}

			protected function batchDate_closeHandler(event:DropdownEvent):void
			{
				customizedCallLater(function():void {
					batchCode.setFocus();
					batchDate.setFocus();
				});
			}

		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	<fc:Toolbar id="pmsToolbar" width="100%"/>
	<s:HGroup width="100%" height="100%" gap="10" paddingBottom="10" paddingLeft="10" paddingRight="10" paddingTop="10">
		<s:VGroup width="40%" height="100%" gap="10" paddingBottom="10" paddingLeft="5" paddingRight="0" paddingTop="10">
			<s:Panel id="readyForSendingOutListPanel" title="Ready for Sending Out List (0)" width="100%" height="100%" >
				<fc:ExtendedDataGrid id="readySendOutHistoryGrid" dataProvider="{readyToSendDeliveryList}" width="100%" height="100%"
									 draggableColumns="false" resizableColumns="false" horizontalScrollPolicy="auto" >
					<fc:columns>
						<mx:DataGridColumn headerText=" " dataField="checkedToSend" width="{readySendOutHistoryGrid.width*0.0700}">							
							<mx:itemRenderer >
								<fx:Component>
									
									<s:MXDataGridItemRenderer>
										<s:layout>
											<s:HorizontalLayout verticalAlign="bottom" horizontalAlign="center" />
										</s:layout>
										<s:CheckBox  selected="@{data.checkedToSend}" />
									</s:MXDataGridItemRenderer>
								</fx:Component>
							</mx:itemRenderer>
						</mx:DataGridColumn>
						<mx:DataGridColumn headerText="Batch Date" dataField="batchDate" id="bDate" labelFunction="batchDateLabelFunc" sortCompareFunction="batchDateSortFunc" headerWordWrap="true" width="{readySendOutHistoryGrid.width*0.22}" >
						<mx:itemRenderer>
							<fx:Component>
								<s:MXDataGridItemRenderer autoDrawBackground="false">
									<s:layout>
										<s:HorizontalLayout verticalAlign="middle" paddingLeft="5"  />
									</s:layout>
									<s:Label text="{outerDocument.batchDateLabelFuncForReadyList(data.batchDate)}"   width="100%" styleName="{outerDocument.batchDateStyleFunc(data.batchDate)}"/>
								</s:MXDataGridItemRenderer>
							</fx:Component>
						</mx:itemRenderer>
						</mx:DataGridColumn>
						<fc:ExtendedDataGridColumn headerText="Batch Code" dataField="batchNum" id="bNum" labelFunction="batchNumLabelFunc" sortCompareFunction="batchNumSortFunc" headerWordWrap="true"  width="{readySendOutHistoryGrid.width*0.13}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn headerText="Batch Mode" dataField="printMode" id="pMode" headerWordWrap="true" labelFunction="batchModeLabelFunc" sortCompareFunction="printModeSortFunc" width="{readySendOutHistoryGrid.width*0.13}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn headerText="Ward" dataField="wardCode"  headerWordWrap="true"  width="{readySendOutHistoryGrid.width*0.12}" enableDynamicColumnWidth="true" id="wardId" sortCompareFunction="wardSortFunc"/>
						<mx:DataGridColumn headerText="Update Date / Time" dataField="checkDate"  headerWordWrap="true"  labelFunction="dateTimeLabelFunc" id="dateTimeId" sortCompareFunction="dateTimeSortFunc" width="{readySendOutHistoryGrid.width*0.22}"/>
					</fc:columns>
				</fc:ExtendedDataGrid>
				<s:controlBarContent>
					<s:HGroup width="100%">
						<s:Label width="70%" text=" " />
						<s:Button id="refresh" label="Refresh"  click="refreshReadySendOutList()"  />
					</s:HGroup>
				</s:controlBarContent>
			</s:Panel>
			
		</s:VGroup>
		<s:VGroup width="60%" height="100%" gap="10" paddingBottom="10" paddingLeft="5" paddingRight="5" paddingTop="10">
			<s:Panel title="Search Criteria" width="100%" height="70" keyDown="criteriaFieldKeyBoardFunc(event)">
				<s:layout>
					<s:HorizontalLayout verticalAlign="middle" horizontalAlign="left" paddingBottom="10" paddingLeft="10" paddingRight="10" paddingTop="10" gap="5"/>
				</s:layout>
				<s:Label text="Batch Date"/>
				<fc:ExtendedAbbDateField id="batchDate" selectedDate="{new Date()}"  focusOut="validateDateField(event)"
										 creationComplete="batchDate.addEventListener(KeyboardEvent.KEY_UP, batchDateKeyBoardFunc)" maxChars="22"
										 close="batchDate_closeHandler(event)"/>
				<s:Label text="Batch Code"/>
				<s:TextInput id="batchCode" keyUp="readBarcodeHandler(event)" />
				<s:Button label="Send" click="sendHandler()" />
			</s:Panel>
			<s:BorderContainer id="currentBatchNumBorderContainer" width="100%"  height="160">
				<s:layout>
					<s:VerticalLayout horizontalAlign="center" verticalAlign="bottom" paddingTop="5" paddingBottom="5"/>
				</s:layout>		
				<s:Label id="currentBatchNum" fontSize="150" verticalAlign="middle" height="100%"/>			
			</s:BorderContainer >
			<s:Panel title="Send Out History" width="100%" height="100%" >
				<fc:ExtendedDataGrid id="sendOutHistoryGrid" dataProvider="{sentDeliveryList}" width="100%" height="100%"
									 draggableColumns="false" resizableColumns="false" horizontalScrollPolicy="on" variableRowHeight="true">
					<fc:columns> 
						<fc:ExtendedDataGridColumn headerText="Batch Date" dataField="batchDate"  headerWordWrap="true"  labelFunction="batchDateLabelFunc" width="{sendOutHistoryGrid.width*0.15}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn headerText="Batch Code" dataField="batchNum" labelFunction="batchNumLabelFunc"   headerWordWrap="true"  width="{sendOutHistoryGrid.width*0.09}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn headerText="Batch Mode" dataField="printMode"  headerWordWrap="true"  labelFunction="batchModeLabelFunc"  width="{sendOutHistoryGrid.width*0.09}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn  headerText="Ward" dataField="wardCode"  headerWordWrap="true"  width="{sendOutHistoryGrid.width*0.08}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn headerText="Update Date / Time"  headerWordWrap="true"  dataField="deliveryDate" labelFunction="dateTimeLabelFunc" width="{sendOutHistoryGrid.width*0.22}" enableDynamicColumnWidth="true"/>
						<fc:ExtendedDataGridColumn  headerText="Status" dataField="status" wordWrap="false"  headerWordWrap="true"  width="{sendOutHistoryGrid.width*0.27+100}" enableDynamicColumnWidth="true"/>
					</fc:columns>
				</fc:ExtendedDataGrid>
			</s:Panel>
		</s:VGroup>
	</s:HGroup>
</fc:ExtendedNavigatorContent>