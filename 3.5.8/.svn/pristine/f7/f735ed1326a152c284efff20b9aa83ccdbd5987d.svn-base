package hk.org.ha.event.pms.main.cddh.popup
{
	import hk.org.ha.model.pms.udt.cddh.FilterPatType;
	
	import mx.collections.ArrayCollection;
	import mx.utils.UIDUtil;
	
	public class FilterPopupProp
	{
		private var _filterHospCode:Boolean = false;
		private var _filterSpecCode:Boolean = false;
		private var _filterCddhSpecCode:Boolean = false;
		private var _filterItemCode:Boolean = false;
		private var _filterDispDate:Boolean = false;
		private var _filterPatType:Boolean = false;
		
		private var _hospCode:String;
		private var _specCode:String;
		private var _cddhSpecCode:String;
		private var _itemCode:String;
		private var _dispDateStart:Date;
		private var _dispDateEnd:Date;
		private var _patType:FilterPatType = FilterPatType.All;
		
		private var _proceedCallbackFunc:Function;
		private var _resetCallbackFunc:Function;
		private var _cancelCallbackFunc:Function;

		private var _dataMinimum:Date;	// min date allowed in filter popup (date of search criteria or earliest record) 
		private var _dataMaximum:Date; // max date allowed in filter popup (date of search criteria or latest record)
		
		private var _filterPopupPropHospCodeList:ArrayCollection = null;
		private var _filterPopupPropSpecCodeList:ArrayCollection = null;
		private var _filterPopupPropCddhSpecCodeList:ArrayCollection = null;
		private var _filterPopupPropItemCodeList:ArrayCollection = null;

		private var _searchDispDateStart:Date;	// disp start date in search criteria
		private var _searchDispDateEnd:Date;	// disp end date in search criteria
		
		
		public function set filterHospCode(value:Boolean):void {
			_filterHospCode = value;
		}
		[Bindable]
		public function get filterHospCode():Boolean {
			return _filterHospCode;
		}

		
		public function set filterSpecCode(value:Boolean):void {
			_filterSpecCode = value;
		}
		[Bindable]
		public function get filterSpecCode():Boolean {
			return _filterSpecCode;
		}

		
		public function set filterCddhSpecCode(value:Boolean):void {
			_filterCddhSpecCode = value;
		}
		[Bindable]
		public function get filterCddhSpecCode():Boolean {
			return _filterCddhSpecCode;
		}

		
		public function set filterItemCode(value:Boolean):void {
			_filterItemCode = value;
		}
		[Bindable]
		public function get filterItemCode():Boolean {
			return _filterItemCode;
		}

		
		public function set filterDispDate(value:Boolean):void {
			_filterDispDate = value;
		}
		[Bindable]
		public function get filterDispDate():Boolean {
			return _filterDispDate;
		}

		
		public function set filterPatType(value:Boolean):void {
			_filterPatType = value;
		}
		[Bindable]
		public function get filterPatType():Boolean {
			return _filterPatType;
		}
		
		
		public function set hospCode(value:String):void {
			_hospCode = value;
		}
		[Bindable]
		public function get hospCode():String {
			return _hospCode;
		}
		
		
		public function set specCode(value:String):void {
			_specCode = value;
		}
		[Bindable]
		public function get specCode():String {
			return _specCode;
		}
		
		
		public function set cddhSpecCode(value:String):void {
			_cddhSpecCode = value;
		}
		[Bindable]
		public function get cddhSpecCode():String {
			return _cddhSpecCode;
		}

		
		public function set itemCode(value:String):void {
			_itemCode = value;
		}
		[Bindable]
		public function get itemCode():String {
			return _itemCode;
		}
		
		
		public function set dispDateStart(value:Date):void {
			_dispDateStart = value;
		}
		[Bindable]
		public function get dispDateStart():Date {
			return _dispDateStart;
		}
		
		
		public function set dispDateEnd(value:Date):void {
			_dispDateEnd = value;
		}
		[Bindable]
		public function get dispDateEnd():Date {
			return _dispDateEnd;
		}
		
		
		public function set patType(value:FilterPatType):void {
			_patType = value;
		}
		[Bindable]
		public function get patType():FilterPatType {
			return _patType;
		}
		
		
		public function set proceedCallbackFunc(value:Function):void {
			_proceedCallbackFunc = value;
		}
		[Bindable]
		public function get proceedCallbackFunc():Function {
			return _proceedCallbackFunc;
		}
		
		
		public function set resetCallbackFunc(value:Function):void {
			_resetCallbackFunc = value;
		}
		[Bindable]
		public function get resetCallbackFunc():Function {
			return _resetCallbackFunc;
		}

		
		public function set cancelCallbackFunc(value:Function):void {
			_cancelCallbackFunc = value;
		}
		[Bindable]
		public function get cancelCallbackFunc():Function {
			return _cancelCallbackFunc;
		}
		
		
		public function set dataMinimum(value:Date):void {
			_dataMinimum = value;
		}
		[Bindable]
		public function get dataMinimum():Date {
			return _dataMinimum;
		}
		
		
		public function set dataMaximum(value:Date):void {
			_dataMaximum = value;
		}
		[Bindable]
		public function get dataMaximum():Date {
			return _dataMaximum;
		}
		
		
		public function set filterPopupPropHospCodeList(value:ArrayCollection):void {
			_filterPopupPropHospCodeList = value;
		}
		[Bindable]
		public function get filterPopupPropHospCodeList():ArrayCollection {
			return _filterPopupPropHospCodeList;
		}
		
		
		public function set filterPopupPropSpecCodeList(value:ArrayCollection):void {
			_filterPopupPropSpecCodeList = value;
		}
		[Bindable]
		public function get filterPopupPropSpecCodeList():ArrayCollection {
			return _filterPopupPropSpecCodeList;
		}
		
		
		public function set filterPopupPropCddhSpecCodeList(value:ArrayCollection):void {
			_filterPopupPropCddhSpecCodeList = value;
		}
		[Bindable]
		public function get filterPopupPropCddhSpecCodeList():ArrayCollection {
			return _filterPopupPropCddhSpecCodeList;
		}
		
		
		public function set filterPopupPropItemCodeList(value:ArrayCollection):void {
			_filterPopupPropItemCodeList = value;
		}
		[Bindable]
		public function get filterPopupPropItemCodeList():ArrayCollection {
			return _filterPopupPropItemCodeList;
		}
		
		
		public function set searchDispDateStart(value:Date):void {
			_searchDispDateStart = value;
		}
		[Bindable]
		public function get searchDispDateStart():Date {
			return _searchDispDateStart;
		}
		
		
		public function set searchDispDateEnd(value:Date):void {
			_searchDispDateEnd = value;
		}
		[Bindable]
		public function get searchDispDateEnd():Date {
			return _searchDispDateEnd;
		}
	}
}