<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*" >
	
	<fx:Metadata>
		[Name("workstationPropMaintPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.udt.DataType;
			import hk.org.ha.model.pms.udt.YesNoFlag;
			import hk.org.ha.model.pms.vo.support.PropInfo;
			import hk.org.ha.model.pms.vo.support.PropInfoItem;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ValidationResultEvent;
			import mx.managers.PopUpManager;
			import mx.validators.DateValidator;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			public var popupPropInfo:PropInfo;
			public var popupOkHandler:Function;
			public var popupCancelHandler:Function;
			
			[Bindable]
			public var popupPropInfoItem:PropInfoItem;
			
			private const MAX_LINE_NUM:int = 8;
			
			private var dateTimeRegExp:RegExp;
			
			[Bindable]
			private var booleanTypeList:ArrayCollection= new ArrayCollection([
				{display:YesNoFlag.Yes, value:YesNoFlag.Yes.dataValue},
				{display:YesNoFlag.No, value:YesNoFlag.No.dataValue}
			]);
			
			public function initPopup():void{
				popupPropInfoItem = new PropInfoItem;
				setDataByType();
				initPopupSize();
				callLater(setInputFocus);
				callLater(focusManager.showFocus);	
			}
			
			private function initPopupSize():void {
				if ( valueDdl.visible ) {
					this.height = 80;
					this.width = 300;
				} else if ( valueTextArea.visible ) {
					this.height = 200;
					this.width = 500;
					group.verticalAlign = "top";
				} else if ( valueTxt.visible ) {
					this.height = 80;
					this.width = 300;
				}
			}
			
			public function okHandler(evt:MouseEvent):void
			{
				if ( popupPropInfoItem.displayValue == null || popupPropInfoItem.displayValue == "" ) {
					this.showSystemMessage("0488", 
											function(evt:MouseEvent):void {
												PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
												setInputFocus();
											});//Please input property value
					
					return;
				}
				if ( popupPropInfo.type == DataType.BooleanType ) {
					popupPropInfoItem.value = (popupPropInfoItem.displayValue==YesNoFlag.Yes.displayValue)?YesNoFlag.Yes.dataValue:YesNoFlag.No.dataValue;
				} else {
					popupPropInfoItem.value = popupPropInfoItem.displayValue;
				}
				popupOkHandler(evt);
			}
			
			private function showSystemMessage(errorCode:String, okFunc:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				msgProp.okHandler = okFunc;
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function cancelHandler(evt:MouseEvent):void
			{
				popupCancelHandler(evt);
				removePopup();
			}
			
			public function removePopup():void {
				PopUpManager.removePopUp(this);
			}
			
			public function setInputFocus():void {
				if ( valueDdl.visible ) {
					valueDdl.setFocus();
				} else if ( valueTextArea.visible ) {
					valueTextArea.setFocus();
				} else if ( valueTxt.visible ) {
					valueTxt.setFocus();
				}
				focusManager.showFocus();
			}
			
			private function setDataByType():void {
				var type:DataType = popupPropInfo.type;
				var validation:String = popupPropInfo.validation;
				
				if ( type == DataType.BooleanType ) 
				{
					valueDdl.visible = true;
					valueDdl.includeInLayout = true;
					valueDdl.dataProvider = booleanTypeList;
					valueDdl.labelField = "display";
					listValueToSelectedIndex(popupPropInfoItem.displayValue, booleanTypeList);
					
				} else if ( type == DataType.TextAreaType ) 
				{
					valueTextArea.visible = true;
					valueTextArea.includeInLayout = true;
					valueTextArea.text = popupPropInfoItem.displayValue;
					valueTextArea.setFocus();
					
				} else if ( type == DataType.DateTimeType ) {
					var strRegExp:String = setDateTimeRegExp(validation);
					dateTimeRegExp = new RegExp(strRegExp);
					valueTxt.maxChars = validation.length;
					valueTxt.visible = true;
					valueTxt.includeInLayout = true;
					valueTxt.text = popupPropInfoItem.displayValue;
					valueTxt.setFocus();
				} else {
					var str:String = validation;
					if ( str.split('-').length > 1 ){
						valueTxt.visible = true;
						valueTxt.includeInLayout = true;
						valueTxt.text = popupPropInfoItem.displayValue;
						if (type == DataType.StringType) {
							valueTxt.maxChars = Number(str.split('-')[1]);
						} else {
							valueTxt.restrict= "0-9"
						}
						valueTxt.setFocus();
					} else {
						var array:Array = str.split(',');
						var list:ArrayCollection = new ArrayCollection();
						for each ( var item:String in array ) {
							list.addItem(item);
						}
						valueDdl.dataProvider = list;
						valueDdl.visible = true;
						valueDdl.includeInLayout = true;
						valueDdl.setFocus();
					}
				}
			}
			
			private function updatePropValue(value:Object):void {
				if ( popupPropInfo.type == DataType.BooleanType ) {
					value = value.display;
				}else if ( popupPropInfo.type == DataType.DateTimeType ) {
					if ( dateTimeRegExp.test(value.toString())) {
						popupPropInfoItem.errorString = "";
					} else {
						popupPropInfoItem.errorString = sysMsgMap.getMessage("0034", new Array([popupPropInfo.validation]));
					}
				}
				popupPropInfoItem.displayValue = String(value);
			}
			
			private function listValueToSelectedIndex(value:String, list:ArrayCollection):int {
				if ( popupPropInfo.type == DataType.BooleanType ) {
					return listValueToSelectedIndexInBooleanTypeList(value);
				} else {
					var i:int = 0;
					for each (var listItem:Object in list) {
						if (value == listItem) {
							return i;
						}
						i++;	
					}
					return -1;
				}
			}
			
			private function listValueToSelectedIndexInBooleanTypeList(value:String):int 
			{
				var i:int = 0;
				for each (var listItem:Object in booleanTypeList) {
					if (value == listItem.display) {
						return i;
					}
					i++;	
				}
				return -1;
			}
			
			private function checkTextAreaLineNum(value:String):void{
				var array:Array = popupPropInfo.validation.split(',');
				var maxRow:int = array[0].toString().substring(1);;
				var maxChar:int = array[1].toString().substring(1);
				var errorParm:Array = new Array(maxRow, maxChar);
				popupPropInfoItem.errorString = "";
				if ( valueTextArea.text.split("\n").length > maxRow ) {
					popupPropInfoItem.errorString  = sysMsgMap.getMessage("0252", errorParm);
				} else {
					for each ( var str:String in valueTextArea.text.split("\n") ) {
						if ( str.length > maxChar ) {
							popupPropInfoItem.errorString  = sysMsgMap.getMessage("0252", errorParm);
							break;
						}
					} 
				}
				updatePropValue(value);
			}
			
			private function setDateTimeRegExp(validation:String):String {
				var strRegExp:String = "^";
				validation = validation.toLocaleLowerCase();
				for ( var i:int=0; i<validation.length; i++ ) {
					var str:String = validation.substr(i, 1);
					
					if ( str == "y" ||
						str == "m" ||
						str == "d" ||
						str == "h" ||
						str == "s" ) {
						strRegExp+="[0-9]";
					} else {
						strRegExp += str;
					}
				}
				strRegExp+="$";														
				return strRegExp;
			}		
			
			
			private function isValidDateTime(value:String, format:String):Boolean {
				var yrIndex:int = format.indexOf("yyyy");
				var monthIndex:int = format.indexOf("MM");
				var dayIndex:int = format.indexOf("dd");
				var hourIndex:int = format.indexOf("HH");
				var minIndex:int = format.indexOf("mm");
				var secondIndex:int = format.indexOf("ss");
				var milliSecondIndex:int = format.indexOf("SSS");
				
				var date:String = value.substr(yrIndex, 4)+value.substr(monthIndex, 2)+value.substr(dayIndex, 2);
				if ( !isValidDate(date) ) {
					return false;
				} 
				
				if ( hourIndex >=0 ) {
					if ( !isValidHour(Number(value.substr(hourIndex, 2))) ) {
						return false;
					}
				}
				
				if ( minIndex >= 0 ) {
					if ( !isValidMinSec(Number(value.substr(minIndex, 2))) ) {
						return false;
					}
				}
				
				if ( secondIndex >= 0 ) {
					if ( !isValidMinSec(Number(value.substr(secondIndex, 2))) ) {
						return false;
					}
				}
				
				if ( milliSecondIndex >= 0 ) {
					if ( !isValidMilliSec(Number(value.substr(milliSecondIndex, 3))) ) {
						return false;
					}
				}
				return true;
			}
			
			private function isValidHour(value:Number):Boolean {
				return ( value >= 0 && value <=23);
			}
			
			private function isValidMinSec(value:Number):Boolean {
				return ( value >= 0 && value <=59);
			}
			
			private function isValidMilliSec(value:Number):Boolean {
				return ( value >= 0 && value <=999);
			}
			
			private function isValidDate(date:String):Boolean {
				var dateValidator:DateValidator = new DateValidator();
				dateValidator.inputFormat = "yyyymmdd";
				
				if ( dateValidator.validate(date).type ==  ValidationResultEvent.INVALID ) {
					return false;
				}
				return true;
			}
		]]>
	</fx:Script>
	<s:Panel title="Apply to All Working Store" width="100%" height="100%">
		<s:VGroup width="100%" height="100%" paddingLeft="10" paddingTop="10" gap="10" paddingRight="10" paddingBottom="10">
			<s:HGroup id="group" verticalAlign="middle" width="100%">
				<s:Label text="Property Value " width="90"/>
				
				<fc:AdvancedDropDownList id="valueDdl" 
								selectedIndex="{listValueToSelectedIndex(popupPropInfoItem.displayValue, valueDdl.dataProvider as ArrayCollection)}"
								visible="false" width="100%" includeInLayout="false"
								change="updatePropValue(valueDdl.selectedItem)"
								errorString="{popupPropInfoItem.errorString}"/>
				
				<s:TextInput id="valueTxt" width="100%" 
							 visible="false" includeInLayout="false"
							 errorString="{popupPropInfoItem.errorString}" 
							 change="updatePropValue(valueTxt.text)"/>		
				
				<s:TextArea id="valueTextArea" editable="true" heightInLines="8"  
							width="100%" visible="false" includeInLayout="false"
							change="checkTextAreaLineNum(valueTextArea.text)" 
							errorString="{popupPropInfoItem.errorString}"/>
				
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="right">
				<fc:ExtendedButton id="okButton" label="OK" click="okHandler(event)"/>
				<fc:ExtendedButton id="cancelButton" label="Cancel" click="cancelHandler(event)"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>