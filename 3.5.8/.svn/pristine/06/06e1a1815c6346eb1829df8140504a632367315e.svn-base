package hk.org.ha.model.pms.vo.checkissue.mp;

import java.io.Serializable;
import java.math.BigDecimal;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AssignBatchSummaryItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long deliveryItemId;
	
	private String caseNum;
	
	private String wardCode;
	
	private String bedNum;
	
	private String itemCode;
	
	private BigDecimal issueQty;
	
	private String destination;
	
	private String baseUnit;
	
	private String doseUnit;
	
	private String exception;
	
	private String drugName;
	
	private String formLabelDesc;
	
	private String strength;
	
	private String volumeText;
	
	private String patientName;
	
	private boolean havePatientChiName;
	
	private Long medProfileId;

	public Long getDeliveryItemId() {
		return deliveryItemId;
	}

	public void setDeliveryItemId(Long deliveryItemId) {
		this.deliveryItemId = deliveryItemId;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public BigDecimal getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(BigDecimal issueQty) {
		this.issueQty = issueQty;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getException() {
		return exception;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFormLabelDesc() {
		return formLabelDesc;
	}

	public void setFormLabelDesc(String formLabelDesc) {
		this.formLabelDesc = formLabelDesc;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getVolumeText() {
		return volumeText;
	}

	public void setVolumeText(String volumeText) {
		this.volumeText = volumeText;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestination() {
		return destination;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public void setHavePatientChiName(boolean havePatientChiName) {
		this.havePatientChiName = havePatientChiName;
	}

	public boolean isHavePatientChiName() {
		return havePatientChiName;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setMedProfileId(Long medProfileId) {
		this.medProfileId = medProfileId;
	}

	public Long getMedProfileId() {
		return medProfileId;
	}
	
}