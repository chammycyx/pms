<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
				 xmlns:s="library://ns.adobe.com/flex/spark" 
				 xmlns:mx="library://ns.adobe.com/flex/mx"
				 xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
				 xmlns="hk.org.ha.view.pms.main.vetting.mp.*"
				 styleName="vettingMpMoStyle" autoDrawBackground="true">
	
	<fx:Declarations>
		<mx:DateFormatter id="dateTimeFormatter" formatString="DD-MMM-YYYY JJ:NN"/>		
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[			
			import hk.org.ha.event.pms.main.vetting.mp.RetrieveDeliveryItemListByIdListEvent;
			import hk.org.ha.event.pms.main.vetting.mp.popup.ShowPendItemPopupEvent;
			import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
			import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
			import hk.org.ha.model.pms.persistence.medprofile.ReplenishmentItem;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
			import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;
			import hk.org.ha.model.pms.vo.medprofile.ReplenishmentRequestUiInfo;
			import hk.org.ha.model.pms.vo.rx.IvRxDrug;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			
			import mx.collections.ArrayCollection;
			import mx.utils.ObjectUtil;						
			
			[Bindable]
			public var readOnly:Boolean;
			[Bindable]
			public var sysMsgMap:SysMsgMap;
			[Bindable]
			public var propMap:PropMap;
			
			[Bindable]
			public var privateFlag:Boolean;			
			[Bindable]
			public var activeListDictionary:Dictionary;
			
			[Bindable]
			private var replenishmentStatus:String;
			[Bindable]
			private var dispenseEnabled:Boolean = true;
			[Bindable]
			public var showSfiPurchaseRequestForReplenishment:Function;
			[Bindable]
			public var clearPurchaseRequestForReplenishment:Function;						
			
			public override function set data(value:Object):void
			{
				super.data = value;				
				if (value) {
					var medProfileMoItem:MedProfileMoItem = Replenishment(value).medProfileMoItem;
					
					dispenseEnabled = checkDispenseEnabled() && !readOnly;
					
					if (medProfileMoItem 
						&& medProfileMoItem.isIvRxDrug 
						&& (IvRxDrug(medProfileMoItem.rxItem).ivAdditiveList == null || IvRxDrug(medProfileMoItem.rxItem).ivAdditiveList.length == 0)
						&& (medProfileMoItem.medProfilePoItemList == null || medProfileMoItem.medProfilePoItemList.length == 0)) {
						if (Replenishment(value).status == ReplenishmentStatus.Processed) {
							replenishmentStatus = "Processed by\nsystem";
							Replenishment(value).dispFlag = true;
						} else {
							replenishmentStatus = Replenishment(value).status.displayValue;							
						}
					} else {
						replenishmentStatus = Replenishment(value).status.displayValue;
					}
				}
			}
			
			private function checkOrderModified():Boolean
			{
				//PMS-3260 
				var replenishment:Replenishment = data as Replenishment;
				try {
					var mpi:MedProfileItem = activeListDictionary[replenishment.medProfileMoItem.medProfileItem.id];
					if (isNaN(replenishment.medProfileMoItem.id)
						|| isNaN(mpi.medProfileMoItem.id)
						|| replenishment.medProfileMoItem.itemNum != mpi.medProfileMoItem.itemNum 
						|| replenishment.medProfileMoItem.medProfileItem.status != MedProfileItemStatus.Verified) {
						return true;
					} else {
						return false;
					}
				} catch (e:Error) {
					trace("Hit inactive item | medProfileMoItem.id:"+replenishment.medProfileMoItem.id);
				}
				return true;
			}
			
			public function checkDispenseEnabled():Boolean
			{
				var replenishment:Replenishment = data as Replenishment;
				if (replenishment.replenishmentItemList == null || replenishment.replenishmentItemList.length == 0) {
					return false;
				}
				
				if (!replenishment.isOutstanding) {
					return false;
				}
				
				if (checkOrderModified()) {
					return false;
				}
				return true;
			}
			
			public function getDoseTlf(dataIn:*):String {							
				return dataIn.rxItem.formattedDrugOrderTlf;				
			}
			
			private function associateDeliveryItemToMedProfilePoItem(medProfileMoItem:MedProfileMoItem):void
			{
				var deliveryItemIdList:ArrayCollection = new ArrayCollection();
				
				for each (var medProfilePoItem:MedProfilePoItem in medProfileMoItem.medProfilePoItemList) {
					if (!isNaN(medProfilePoItem.lastDeliveryItemId)) {
						deliveryItemIdList.addItem(medProfilePoItem.lastDeliveryItemId);
					}
				}
				
				dispatchEvent(new RetrieveDeliveryItemListByIdListEvent(medProfileMoItem, deliveryItemIdList,
					postAssociateDeliveryItemToMedProfilePoItem));
			}
			
			public function postAssociateDeliveryItemToMedProfilePoItem(medProfileMoItem:MedProfileMoItem, deliveryItemList:ArrayCollection):void
			{
				var deliveryItemMap:Dictionary = new Dictionary();
				for each (var deliveryItem:DeliveryItem in deliveryItemList) {
					deliveryItemMap[deliveryItem.id] = deliveryItem;
				}
				
				for each (var medProfilePoItem:MedProfilePoItem in medProfileMoItem.medProfilePoItemList) {
					if (!isNaN(medProfilePoItem.lastDeliveryItemId) && deliveryItemMap[medProfilePoItem.lastDeliveryItemId] != null) {
						medProfilePoItem.deliveryItem = deliveryItemMap[medProfilePoItem.lastDeliveryItemId];
					}
				}
			}
			
			private function checkExistOfDeliveryInfo(medProfileMoItem:MedProfileMoItem):Boolean {
				for each (var poi:MedProfilePoItem in medProfileMoItem.medProfilePoItemList) {
					if (poi.deliveryItem != null) {
						return true;
					}
				}
				return false;
			}
			
			protected function dispClickHandler(event:MouseEvent):void
			{
				handleDispFlagUpdated();
				checkSfi();
				if (data.dispFlag && !checkExistOfDeliveryInfo(data.medProfileMoItem as MedProfileMoItem)) {
					associateDeliveryItemToMedProfilePoItem(data.medProfileMoItem as MedProfileMoItem);
				}
			}
			
			protected function handleDispFlagUpdated():void {
				var replenishment:Replenishment;
				var replenishmentItem:ReplenishmentItem;
				if ( !data.dispFlag ) {
					Replenishment(data).updateIssueQtyToEmpty();
					data.clearErrorString();
					
					clearPurchaseRequestForReplenishment(data);
				} else {
					replenishment = data as Replenishment;
					var mpi:MedProfileItem = activeListDictionary[replenishment.medProfileMoItem.medProfileItem.id];
					replenishment.replenishmentItemList.removeAll();
					for each (var poi:MedProfilePoItem in mpi.medProfileMoItem.medProfilePoItemList) {
						replenishmentItem = new ReplenishmentItem;
						replenishmentItem.medProfilePoItem = poi;
						replenishment.replenishmentItemList.addItem(replenishmentItem);
					}
					replenishment.medProfileMoItem = mpi.medProfileMoItem;
				}
				Replenishment(data).updateDirectPrintFlag();
			}
			
			protected function checkSfi():void {
				var replenishment:Replenishment = Replenishment(data);
				var medProfileItem:MedProfileItem = replenishment.medProfileMoItem.medProfileItem;
				
				if (!replenishment.dispFlag || privateFlag) {
					return;
				}
				
				var paymentRequired:Boolean = false;
				for each (var replenishmentItem:ReplenishmentItem in replenishment.replenishmentItemList) {
					paymentRequired ||= replenishmentItem.medProfilePoItem.isPublicSfi(privateFlag);
				}
				if (!paymentRequired) {
					return;
				}
				showSfiPurchaseRequestForReplenishment(replenishment,cancelDispense);
			}
			
			public function cancelDispense():void {
				data.dispFlag = false;
				handleDispFlagUpdated();
			}
		]]>
	</fx:Script>
	
	<mp:states>
		<s:State name="normal"/>
		<s:State name="selected"/>
	</mp:states>
	
	<mp:layout>
		<s:VerticalLayout gap="0"/>
	</mp:layout>
	
	<s:HGroup width="100%" gap="0">
		
		<!-- Dispense -->
		<s:HGroup width="{ReplenishmentRequestUiInfo.dispColWidth}" styleName="none"
				  verticalAlign="middle" horizontalAlign="center" gap="0" paddingTop="{ReplenishmentRequestUiInfo.paddingSpace}">
			<s:CheckBox id="dispChkBox" selected="@{data.dispFlag}" click="dispClickHandler(event)" enabled="{dispenseEnabled}"/>	
		</s:HGroup>		
		
		<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		<!-- Drug -->
		<s:VGroup width="{ReplenishmentRequestUiInfo.drugColWidth - 1}" gap="0">
			<s:RichText width="100%" 
						paddingLeft="{ReplenishmentRequestUiInfo.paddingSpace}" 
						paddingRight="{ReplenishmentRequestUiInfo.paddingSpace}" 
						paddingTop="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}" 
						paddingBottom="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}"						
						textFlow="{MedProfileUtils.importFromTlfString(getDoseTlf(data.medProfileMoItem))}"/>
			<s:HGroup gap="0">
				<s:Label fontWeight="bold" fontStyle="italic" text="M" includeInLayout="{data.medProfileMoItem.mealFlag}"/>
			</s:HGroup>
			<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		</s:VGroup>
		
		<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		
		<!-- Issue Qty -->
		<s:HGroup width="{ReplenishmentRequestUiInfo.issueQtyColWidth - 1}" gap="0" paddingLeft="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingRight="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingTop="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}" 
				  paddingBottom="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}">
			<s:Label text="{(!isNaN(data.doseCount) &amp;&amp; data.doseCount!=0)?data.doseCount+' Dose(s)':''}" width="100%"/>
		</s:HGroup>
		
		<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		
		<!-- Reason -->
		<s:HGroup width="{ReplenishmentRequestUiInfo.reasonColWidth - 1}" gap="0" paddingLeft="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingRight="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingTop="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}" 
				  paddingBottom="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}" >
			<s:Label text="{data.reason}" width="100%"/>
		</s:HGroup>
		
		<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		
		<!-- Request Date -->
		<s:HGroup width="{ReplenishmentRequestUiInfo.requestDateColWidth - 1}" gap="0" paddingLeft="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingRight="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingTop="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}" 
				  paddingBottom="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}">
			<s:Label text="{dateTimeFormatter.format(data.requestDate)}"/>	
		</s:HGroup>
		
		<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		
		<!-- Status -->
		<s:HGroup width="{ReplenishmentRequestUiInfo.statusColWidth - ReplenishmentRequestUiInfo.scrollBarWidth - 1}" gap="0" paddingLeft="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingRight="{ReplenishmentRequestUiInfo.paddingSpace}" 
				  paddingTop="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}" 
				  paddingBottom="{ReplenishmentRequestUiInfo.paddingSpaceTopBottom}">
			<s:Label text="{replenishmentStatus}"/>
		</s:HGroup>
		
	</s:HGroup>
	<s:DataGroup includeInLayout="{data.dispFlag}" visible="{data.dispFlag}" width="100%" dataProvider="{data.replenishmentItemList}">
		<s:layout>
			<s:VerticalLayout gap="0"/>
		</s:layout>
		<s:itemRenderer>
			<fx:Component>
				<ReplenishmentItemRenderer styleName="vettingMpPoStyle" width="100%" 
										   autoDrawBackground="true" currentState="{outerDocument.currentState}"											   
										   dataChange="outerDocument.dispatchEvent(event)"
										   sysMsgMap="{outerDocument.sysMsgMap}" replenishmentStatus="{outerDocument.data.status}" readOnly="{!outerDocument.data.isOutstanding || outerDocument.readOnly}"
										   privateFlag="{outerDocument.privateFlag}"
										   propMap="{outerDocument.propMap}"/>
			</fx:Component>
		</s:itemRenderer>
	</s:DataGroup>
	<mx:HRule styleName="vettingMpRuleStyle" width="100%"/>						
</mp:ItemRenderer>
