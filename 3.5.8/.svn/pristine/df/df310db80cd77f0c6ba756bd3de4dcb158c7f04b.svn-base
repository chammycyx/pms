<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
				xmlns:s="library://ns.adobe.com/flex/spark" 
				xmlns:mx="library://ns.adobe.com/flex/mx"
				xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
				xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
				xmlns="hk.org.ha.view.pms.main.vetting.mp.*" >
	
	<fx:Script>
		<![CDATA[
			import mx.utils.StringUtil;
			
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			
			[Bindable]
			public var dosageColumnWidth:Number;
			
			[Bindable]
			public var freqColumnWidth:Number;
			
			[Bindable]
			public var treatmentDayColumnWidth:Number;
			
			[Bindable]
			public var durationColumnWidth:Number;
			
			[Bindable]
			public var qtyColumnWidth:Number;
			
			[Bindable]
			public var showDurationAndQty:Boolean;
			
			[Bindable]
			public var showFluidDesc:Boolean;

			[Bindable]
			public var isManualItem:Boolean;
			
			[Bindable]
			public var isChemoItem:Boolean;
			
			[Bindable]
			protected var fluidDesc:String;
		]]>
	</fx:Script>
	
	<fx:Binding source="{showFluidDesc ? MedProfileUtils.formatDoseFluidDesc(data) : ''}" destination="fluidDesc"/>
	
	<mp:layout>
		<s:VerticalLayout gap="0"/>
	</mp:layout>
	
	<s:HGroup includeInLayout="{itemIndex != 0}" visible="{itemIndex != 0}" gap="0">
		<mx:HRule styleName="vettingMpRuleStyle" width="{dosageColumnWidth + freqColumnWidth - (isChemoItem ? 0 : 1)}"/>
		<s:HGroup includeInLayout="{showDurationAndQty}" visible="{showDurationAndQty}" gap="0">
			<mx:HRule styleName="vettingMpRuleStyle" width="1" includeInLayout="{isChemoItem}" visible="{isChemoItem}"/>
			<mx:HRule styleName="vettingMpInvisibleRuleStyle" width="{treatmentDayColumnWidth - 1}" includeInLayout="{isChemoItem}" visible="{isChemoItem}"/>
			<mx:HRule styleName="vettingMpInvisibleRuleStyle" width="{durationColumnWidth - 1}"/>
			<mx:HRule styleName="vettingMpRuleStyle" width="{qtyColumnWidth - 15}"/>
		</s:HGroup>
	</s:HGroup>
	<s:HGroup width="100%" height="100%" gap="0">
		<s:VGroup width="{dosageColumnWidth - 1}" paddingLeft="5" paddingTop="5" paddingRight="5" paddingBottom="5" gap="2">
			<s:RichText width="100%"
						visible="{data.dosage || data.dosageEnd}"
						includeInLayout="{data.dosage || data.dosageEnd}">
				<s:textFlow>
					<s:TextFlow>
						<s:span>{isNaN(data.dosage) ? '' : data.dosage}</s:span>
						<s:span>{isNaN(data.dosageEnd) ? ' ' : ' - ' + data.dosageEnd + ' '}</s:span>
						<s:span>{MedProfileUtils.formatUnit(isNaN(data.dosageEnd) ? data.dosage : data.dosageEnd, data.dosageUnit.toLowerCase())}</s:span>
					</s:TextFlow>
				</s:textFlow>
			</s:RichText>
			<s:Label width="100%" includeInLayout="{StringUtil.trim(fluidDesc).length > 0}" visible="{StringUtil.trim(fluidDesc).length > 0}"
					 text="{fluidDesc}"/>
		</s:VGroup>
		<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		<s:Label width="{freqColumnWidth - 1}"
				 paddingLeft="5" paddingTop="5" paddingRight="5" paddingBottom="5"
				 text="{data.freqFullDesc.toLowerCase()}{data.prn ? ' prn' : ''}{(isManualItem || !data.isParenteralSite) &amp;&amp; !data.isDefaultSite ?
				       MedProfileUtils.formatBrackets((StringUtil.trim(data.supplSiteDesc).length > 0 ? data.supplSiteDesc : data.ipmoeDesc),' [',']') : ''}"/>
		<mp:VRule styleName="vettingMpRuleStyle" height="100%" includeInLayout="{isChemoItem}" visible="{isChemoItem}"/>
		<s:Spacer width="{treatmentDayColumnWidth - 1}" includeInLayout="{isChemoItem}" visible="{isChemoItem}"/>
		<s:HGroup height="100%" gap="0" includeInLayout="{showDurationAndQty}" visible="{showDurationAndQty}">
			<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
			<s:Label width="{durationColumnWidth - 1}"/>
			<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
			<s:HGroup width="{qtyColumnWidth - 17}">
				<s:Label width="100%" paddingLeft="5" paddingTop="5" paddingBottom="5"
						 text="{data.dispQty ? data.dispQty + ' ' + data.baseUnit.toLowerCase() : ''}"/>
				<s:CheckBox styleName="vettingMpCheckBoxStyle" visible="false"/>
			</s:HGroup>
			<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
		</s:HGroup>
	</s:HGroup>
</mp:ItemRenderer>
