<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009" 
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	width="440"
	keyUp="keyUpEventHandler(event)">

	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.components.Button;
			import spark.components.DataRenderer;
			
			import hk.org.ha.event.pms.main.alert.mds.EditItemReasonEvent;
			import hk.org.ha.event.pms.main.alert.mds.EditPrescReasonEvent;
			import hk.org.ha.event.pms.main.alert.mds.popup.ShowOverrideReasonOpPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.persistence.AlertEntity;
			import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
			import hk.org.ha.model.pms.vo.alert.mds.MedProfileAlertMsg;
			
			import org.granite.util.Enum;
			
			private var overrideReasonPopup:OverrideReasonBasePopup;

			protected var ddiAlertIndex:Number;
			protected var alertMsg:AlertMsg;
			
			protected var patOverrideReason:Array;
			protected var ddiOverrideReason:Array;
			
			[Bindable]
			private var presetReasonList:ArrayCollection;
			
			[Bindable]
			public var overrideReasonList:ListCollectionView;
			
			[Bindable]
			private var remarkReason:String;
			
			private function constructPresetReasonList(state:String):ArrayCollection {

				var array:Array = new Array();
				if (state == "pat") {
					array = patOverrideReason;
				}
				else if (state == "ddi") {
					array = ddiOverrideReason;
				}

				var list:ArrayCollection = new ArrayCollection();
				for each (var reason:Enum in array) {
					list.addItem(reason.displayValue);
				}
				return list;
			}
			
			private function constructOverrideReasonListFromAlertMsg(alertMsg:AlertMsg, ddiIndex:Number):ListCollectionView {
				var alertEntity:AlertEntity;
				if (isNaN(ddiIndex)) {
					if (alertMsg.patAlertList.length > 0) {
						alertEntity = alertMsg.patAlertList.getItemAt(0) as AlertEntity;
					}
				}
				else {
					alertEntity = alertMsg.ddiAlertList.getItemAt(ddiIndex) as AlertEntity;
				}

				if (alertEntity == null) {
					return new ArrayCollection();
				}
				else {
					return alertEntity.overrideReason;
				}
			}
			
			private function getRemarkFromOverrideReasonList(overrideReasonList:ListCollectionView, presetReasonList:ListCollectionView):String {
				for each (var overrideReason:String in overrideReasonList) {
					if (!presetReasonList.contains(overrideReason)) {
						return overrideReason;
					}
				}
				return null;
			}
			
			private function removeLastAsterisk(str:String):String {
				if (lastCharIsAsterisk(str)) {
					return str.substr(0, str.length-1);
				}
				else {
					return str;
				}
			}
			
			private function lastCharIsAsterisk(str:String):Boolean {
				if (str == null || str.length == 0) {
					return false;
				}
				var lastChar:String = str.charAt(str.length-1);
				return lastChar == "*";
			}
			
			protected function constructOverrideReasonListFromUI():ListCollectionView {
				
				var list:ArrayCollection = new ArrayCollection();
				
				for (var i:int=0; i < presetReasonGroup.dataProvider.length; i++) {
					var checkBox:CheckBox = (presetReasonGroup.getElementAt(i) as DataRenderer).getElementAt(0) as CheckBox;
					if (checkBox.selected) {
						list.addItem(checkBox.label);
					}
				}
				
				if (StringUtil.trim(remark.text) != "") {
					if (other.selected) {
						list.addItem(remark.text + "*");
					}
					else {
						list.addItem(remark.text);
					}
				}
				
				return list;
			}
			
			private function proceedBtnHander():void {
				
				if (!validate()) {
					return;
				}
				
				var reasonList:ListCollectionView = constructOverrideReasonListFromUI();

				overrideReasonList.removeAll();
				overrideReasonList.addAll(reasonList);
				
				proceedCallback(reasonList);
				
				closePopup();
			}
			
			protected function proceedCallback(reasonList:ListCollectionView):void {
			}
			
			private function cancelBtnHandler():void {
				cancelCallback();
				closePopup();
			}

			protected function cancelCallback():void {
			}
			
			private function closePopup():void {
				PopUpManager.removePopUp(overrideReasonPopup);
			}
			
			protected function baseInit(alertMsg:AlertMsg, ddiAlertIndex:Number):void {

				if (isNaN(ddiAlertIndex)) {
					currentState = "pat";
				}
				else {
					currentState = "ddi";
				}
				
				this.alertMsg      = alertMsg;
				this.ddiAlertIndex = ddiAlertIndex;

				presetReasonList = constructPresetReasonList(currentState);
				overrideReasonList = constructOverrideReasonListFromAlertMsg(alertMsg, ddiAlertIndex);
				remarkReason = getRemarkFromOverrideReasonList(overrideReasonList, presetReasonList);

				callLater(proceedBtn.setFocus);
				overrideReasonPopup = this;
				focusManager.showFocus();
			}

			private function validate():Boolean {
				
				var sysMsgProp:SystemMessagePopupProp;

				if (!isAnyReasonSelected()) {
					sysMsgProp = new SystemMessagePopupProp("0238");
					sysMsgProp.setOkButtonOnly = true;
					dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
					return false;
				}
				
				if (isOtherSelectedWithoutRemark()) {
					sysMsgProp = new SystemMessagePopupProp("0350");
					sysMsgProp.setOkButtonOnly = true;
					sysMsgProp.okHandler = function(evt:MouseEvent):void {
						callLater(remark.setFocus);
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					};
					dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
					return false;
				}
				
				if (calcSelectedReasonLength(constructOverrideReasonListFromUI()) > 255) {
					sysMsgProp = new SystemMessagePopupProp("0466");
					sysMsgProp.setOkButtonOnly = true;
					dispatchEvent(new RetrieveSystemMessageEvent(sysMsgProp));
					return false;
				}
				
				return true;
			}
			
			private function isAnyReasonSelected():Boolean {
				if (other.selected) {
					return true;
				}
				for (var i:int=0; i < presetReasonGroup.dataProvider.length; i++) {
					var checkBox:CheckBox = (presetReasonGroup.getElementAt(i) as DataRenderer).getElementAt(0) as CheckBox;
					if (checkBox.selected) {
						return true;
					}
				}
				return false;
			}
			
			private function isOtherSelectedWithoutRemark():Boolean {
				if (other.selected && StringUtil.trim(remark.text) == "") {
					return true;
				}
				else {
					return false;
				}
			}
			
			private function otherReasonBoxClickHandler():void {
				if (other.selected) {
					remark.setFocus();
				}
				else {
					remark.errorString = null;
				}
			}
			
			private function calcSelectedReasonLength(reasonList:ListCollectionView):int {
				var length:int = 0;
				for each (var reason:String in reasonList) {
					if (length > 0) {
						length++;
					}
					length += reason.length;
				}
				return length;
			}

			private function keyUpEventHandler(evt:KeyboardEvent):void {
				
				if (evt.keyCode == 13) { // enter
					triggerButton(proceedBtn);
					evt.stopPropagation();
					return;
				}
				
				if (evt.ctrlKey) {
					switch (evt.charCode) {
						case 48: // Ctrl + 0
							triggerButton(cancelBtn);
							evt.stopPropagation();
							break;
					}
				}
			}

			private function triggerButton(button:Button):void {
				if (button != null && button.enabled && button.visible) {
					button.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
			}

			private function otherReasonKeyUpHandler(evt:KeyboardEvent):void {
				if (evt.keyCode == 13) { // enter
					evt.stopPropagation();
				}
			}

			private function proceedBtnKeyUpHandler(evt:KeyboardEvent):void {
				if (evt.keyCode == 13) { // enter
					evt.stopPropagation();
				}
			}

		]]>		
	</fx:Script>
	
	<fc:states>
		<s:State name="pat"/>
		<s:State name="ddi"/>
	</fc:states>
	
	<s:Panel width="100%"
			 title.pat="Override MDS alert"
			 title.ddi="Override Drug-Drug Interaction alert">

		<s:layout>
			<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" gap="10"/>
		</s:layout>
		
		<s:Panel title="Please enter reason(s) of overriding of MDS alert" width="100%">

			<s:layout>
				<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10"/>
			</s:layout>
			
			<s:DataGroup id="presetReasonGroup"
						 dataProvider="{presetReasonList}"
						 width="100%">
	
				<s:layout>
					<s:VerticalLayout paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0"/>
				</s:layout>
				
				<s:itemRenderer>
					<fx:Component>
						<s:ItemRenderer autoDrawBackground="false" width="100%">
	
							<fx:Script>
								<![CDATA[
									
									[Bindable]
									private var overrideReason:String;
									
									public override function set data(value:Object):void {
										super.data = value;
										if (value == null) {
											overrideReason = null;
										}
										else {
											overrideReason = data as String;
										}
									}
								]]>
							</fx:Script>
							
							<s:CheckBox label="{overrideReason}"
										tabFocusEnabled="false"
										selected="{outerDocument.overrideReasonList.contains(overrideReason)}"/>
						</s:ItemRenderer>
					</fx:Component>
				</s:itemRenderer>
	
			</s:DataGroup>
			
			<s:CheckBox id="other"
						label="Other (Please specify reason below)"
						tabFocusEnabled="false"
						selected="{lastCharIsAsterisk(remarkReason)}"
						click="otherReasonBoxClickHandler()"/>

		</s:Panel>

		<s:Panel title="Remarks" width="100%" height="145">
			<mx:TextArea id="remark"
						 width="100%" height="100%"
						 maxChars="255"
						 text="{removeLastAsterisk(remarkReason)}"
						 keyUp="otherReasonKeyUpHandler(event)"/>
		</s:Panel>
		
		<s:HGroup horizontalAlign="right" width="100%">
			<fc:ExtendedButton id="proceedBtn" label="Proceed" click="proceedBtnHander()" keyUp="proceedBtnKeyUpHandler(event)"/>
			<fc:ExtendedButton id="cancelBtn"  label="Cancel"  click="cancelBtnHandler()"/>
		</s:HGroup>
	</s:Panel>
	
</fc:ExtendedNavigatorContent>
