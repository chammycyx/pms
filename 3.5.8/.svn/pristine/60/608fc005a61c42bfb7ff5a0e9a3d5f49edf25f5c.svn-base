package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.reftable.MpWardManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
import hk.org.ha.model.pms.vo.delivery.ScheduleTemplateInfo;
import hk.org.ha.model.pms.vo.medprofile.Ward;
import hk.org.ha.model.pms.vo.medprofile.WardGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("dueOrderScheduleService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DueOrderScheduleServiceBean implements DueOrderScheduleServiceLocal {
	@Logger
	private Log logger;
	
	@In
	private DueOrderPrintManagerLocal dueOrderPrintManager;
	
	@In
	private DeliveryScheduleManagerLocal deliveryScheduleManager;
	
	@In
	private MpWardManagerLocal mpWardManager;
	
	@In
	private Workstore workstore;
	
	@Out(required = false)
	private List<Ward> scheduleWardList;
	
	@Out(required = false)
	private List<WardGroup> scheduleWardGroupList;
	
    public DueOrderScheduleServiceBean() {
    }
    
    @Override
    public ScheduleTemplateInfo retrieveDeliveryScheduleList() {
    	retrieveScheduleWardInfo();
    	
    	ScheduleTemplateInfo templateInfo = new ScheduleTemplateInfo();
    	
    	List<DeliverySchedule> deliveryScheduleList = deliveryScheduleManager.retrieveDeliveryScheduleList(workstore.getHospital());
    	templateInfo.setDeliveryScheduleList(deliveryScheduleList);
    	
    	String resetTemplateName = null;
    	if( deliveryScheduleList.size() == 1 ){
    		resetTemplateName = deliveryScheduleList.get(0).getName();
    	}
    	deliveryScheduleManager.setDeliveryScheduleTemplate(workstore, resetTemplateName, true);
    	
    	retrieveDefaultScheduleTemplateInfo(templateInfo);

    	return templateInfo;
    }
        
    public ScheduleTemplateInfo changeDeliveryScheduleTemplate(String templateName){
    	retrieveScheduleWardInfo();
    	
    	ScheduleTemplateInfo templateInfo = new ScheduleTemplateInfo();
    	
    	List<DeliverySchedule> deliveryScheduleList = deliveryScheduleManager.retrieveDeliveryScheduleList(workstore.getHospital());
    	templateInfo.setDeliveryScheduleList(deliveryScheduleList);
    	
    	deliveryScheduleManager.setDeliveryScheduleTemplate(workstore, templateName, false);
    	
    	retrieveDefaultScheduleTemplateInfo(templateInfo);
    	
    	return templateInfo;
    }
    
    private void retrieveScheduleWardInfo(){
    	scheduleWardList = mpWardManager.retrieveWardList(workstore.getWorkstoreGroup());
    	scheduleWardGroupList = mpWardManager.retrieveWardGroupList(workstore.getWorkstoreGroup());    	
    }
    
    private void retrieveDefaultScheduleTemplateInfo(ScheduleTemplateInfo scheduleInfoIn){
    	DeliverySchedule defaultTemplate = deliveryScheduleManager.retrieveDeliveryScheduleTemplate(workstore, scheduleInfoIn);
    	if( defaultTemplate != null ){
    		List<DeliveryScheduleItem> dueOrderDeliveryScheduleItemList = deliveryScheduleManager.retrieveDeliveryScheduleItemList(workstore, defaultTemplate);
    		if( dueOrderDeliveryScheduleItemList != null ){
    			sortDeliveryScheduleItemList(dueOrderDeliveryScheduleItemList);
    		}
    		scheduleInfoIn.setCurrentDeliverySchedule(defaultTemplate);
    		scheduleInfoIn.setDefaultDeliverySchedule(defaultTemplate);
    		scheduleInfoIn.setDeliveryScheduleItemList(dueOrderDeliveryScheduleItemList);
    	}
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void sortDeliveryScheduleItemList(List<DeliveryScheduleItem> deliveryScheduleItemList){
		Collections.sort(deliveryScheduleItemList, new Comparator(){
			public int compare(Object o1, Object o2) {
				DeliveryScheduleItem item1 = (DeliveryScheduleItem) o1;
				DeliveryScheduleItem item2 = (DeliveryScheduleItem) o2;
				
				if( item1.getScheduleTime().compareTo(item2.getScheduleTime()) != 0 ){
					return item1.getScheduleTime().compareTo(item2.getScheduleTime());
				}
				
				if( !item1.getDueOnType().equals(item2.getDueOnType()) ){
					return item1.getDueOnType().compareTo(item2.getDueOnType());			
				}
				
				if( item1.getDueHour().compareTo(item2.getDueHour()) != 0 ){
					return item1.getDueHour().compareTo(item2.getDueHour());
				}
				
				return item1.getId().compareTo(item2.getId());
			}
		});	
	}
    
    @Remove
	@Override
	public void destroy() {
	}
}
