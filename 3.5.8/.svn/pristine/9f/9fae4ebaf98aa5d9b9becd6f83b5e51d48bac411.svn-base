<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	manualDashboardOperation="true">
	
	<fx:Metadata>
		[Name("appointmentPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[			
			import hk.org.ha.event.pms.main.refill.popup.EditRefillScheduleAppointmentDateEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.model.pms.persistence.disp.RefillSchedule;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ValidationResultEvent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			import mx.validators.DateValidator;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			private var prevRefillSchedule:RefillSchedule;
			private var currRefillSchedule:RefillSchedule;
						
			public function updateAppointmentPopupDisplay(prevRs:RefillSchedule, currRs:RefillSchedule):void{								
				prevRefillSchedule = prevRs;
				currRefillSchedule = currRs;
				
				nextPharmApptDateAbb.text = dateFormatter.format(currRefillSchedule.refillDate).toString();
				nextPharmApptDateAbb.setFocus();
			}
			
			public function showPopupFocus():void {
				focusManager.showFocus();
			}
						
			protected function apptOkFunc(event:MouseEvent):void{
				if( !validApptDateInput() ){
					return;
				}
				
				var pharmApptDate:Date = nextPharmApptDateAbb.selectedDate;
				pharmApptDate = new Date( pharmApptDate.getFullYear(), pharmApptDate.getMonth(), pharmApptDate.getDate(), 0,0,0,0 );
				
				if( isValidPharmClinicApptDate(pharmApptDate) ){
					currRefillSchedule.pharmApptDateEnable = true;					
					currRefillSchedule.nextPharmApptDate = pharmApptDate;
					dispatchEvent(new EditRefillScheduleAppointmentDateEvent(currRefillSchedule));
					PopUpManager.removePopUp(this);
				}
			}
			
			protected function apptCancelFunc(event:MouseEvent):void{
				currRefillSchedule.nextPharmApptDate = null;
				currRefillSchedule.pharmApptDateEnable = false;
				dispatchEvent(new EditRefillScheduleAppointmentDateEvent(currRefillSchedule));
				PopUpManager.removePopUp(this);
			}

			private function isValidPharmClinicApptDate(pharmApptDate:Date):Boolean{
				if( ObjectUtil.compare(pharmApptDate, currRefillSchedule.refillDate) > 0 ){
					showSystemMessage("0008");
					return false;
				}else if( ObjectUtil.compare(prevRefillSchedule.refillDate, pharmApptDate) >= 0 ){
					showSystemMessage("0039");
					return false;
				}
				return true;
			}
			
			private function showSystemMessage(errorCode:String):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				var okHandler:Function = function(evt:Event):void{
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					nextPharmApptDateAbb.setFocus();
					focusManager.showFocus();
				};
				msgProp.okHandler = okHandler;
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			protected function focusNextItem(event:FocusEvent):void{
				if( event.shiftKey ){
					if( event.keyCode == Keyboard.TAB ){
						event.preventDefault();
						switch( event.currentTarget ){
							case nextPharmApptDateAbb:
								if( validApptDateInput() ){
									callLater( apptCancel.setFocus );
								}
								break;
							case apptOk:
								callLater( nextPharmApptDateAbb.setFocus );
								break;
							case apptCancel:
								callLater( apptOk.setFocus );
								break;
						}
					}
				}else if( event.keyCode == Keyboard.TAB ){
					event.preventDefault();
					switch( event.currentTarget ){
						case nextPharmApptDateAbb:
							if( validApptDateInput() ){
								callLater( apptOk.setFocus );
							}
							break;
						case apptOk:
							callLater( apptCancel.setFocus );
							break;
						case apptCancel:
							callLater( nextPharmApptDateAbb.setFocus );
							break;
					}					
				}
			}
			
			private function validApptDateInput():Boolean {
				if( !ExtendedDateFieldValidator.isValidDate(nextPharmApptDateAbb) ){
					if( nextPharmApptDateAbb.text == "" ){
						showSystemMessage("0508");
					}else{
						showSystemMessage("0001");
					}
					return false;
				}
				return true;
			}
		]]>
	</fx:Script>
	<s:Panel width="100%" height="100" title="Refill">
		<s:VGroup width="100%" height="100%" left="10" right="10" bottom="10" top="10" gap="10">
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="left">
				<s:Label text="Patient should attend Pharmacist Clinic on" width="240"/>
				<fc:ExtendedAbbDateField id="nextPharmApptDateAbb" selectedDate="{new Date()}" keyFocusChange="focusNextItem(event)"/>
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="right">
				<fc:ExtendedButton label="OK" id="apptOk" click="apptOkFunc(event)" keyFocusChange="focusNextItem(event)"/>
				<fc:ExtendedButton label="Cancel" id="apptCancel" click="apptCancelFunc(event)" keyFocusChange="focusNextItem(event)"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>