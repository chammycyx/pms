package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.corp.cache.DmDrugCacherInf;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.MsWorkstoreDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.corp.WorkstorePK;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.ItemLocationRpt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("itemLocationRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ItemLocationRptServiceBean implements ItemLocationRptServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<String> itemCodeList;

	@Out(required = false)
	private List<String> itemLocationRptPickStationList;
	
	@Out(required = false)
	private List<String> bakerCellList;
	
	@Out(required = false)
	private List<String> binNumList;

	@In
	private Workstore workstore;
	
	@In
	private PrintAgentInf printAgent; 
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private PrinterSelectorLocal printerSelector;
    
    @Out
    public List<ItemLocationRpt> itemLocationRptList;
    
    private Map<String, Object> parameters = new HashMap<String, Object>();
    
    private List<ItemLocationRpt> exportItemLocationRptList;
    

	public void printItemLocationRpt(List<ItemLocationRpt> printItemLocationList){
		parameters.put("hospCode", workstore.getHospCode());
		
		printAgent.renderAndPrint(new RenderAndPrintJob(
				"ItemLocationRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				printItemLocationList));
		
	}

	public void retrieveItemLocationRpt(ItemLocationRpt itemLocationRpt){
		List<ItemLocation> itemLocationList = retrieveItemLocationList(itemLocationRpt);
		
		itemLocationRptList = constructItemLocationRptList(itemLocationRpt, itemLocationList);

		itemLocationRptPickStationList = new ArrayList<String>();
		bakerCellList = new ArrayList<String>();
		itemCodeList = new ArrayList<String>();
		binNumList = new ArrayList<String>();
		
		retrievePickStationList(itemLocationRptList);
		retrieveBakerCellList(itemLocationRptList);
		retrieveItemCodeList(itemLocationRptList);
		retrieveBinNumList(itemLocationRptList);
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemLocation> retrieveItemLocationList(ItemLocationRpt itemLocationRpt) {
		Workstore ws = em.find(Workstore.class, new WorkstorePK(workstore.getHospCode(), itemLocationRpt.getWorkstoreCode()));
		
		StringBuilder sb = new StringBuilder( 
				"select o from ItemLocation o" + // 20120305 index check : ItemLocation.workstore : FK_ITEM_LOCATION_01
				" where o.workstore = :workstore" + 
				" order by o.itemCode");

		
		Query query = em.createQuery(sb.toString());
		query.setParameter("workstore", ws);
		
		List<ItemLocation> itemLocationList = query.getResultList();

		return itemLocationList;
	}

	@SuppressWarnings("unchecked")
	public List<ItemLocationRpt> constructItemLocationRptList (ItemLocationRpt itemLocationRpt, List<ItemLocation> itemLocationList) {
		List<ItemLocationRpt> reportList = new ArrayList<ItemLocationRpt>();
		
		for(ItemLocation itemLocation:itemLocationList){
			
			boolean isFilter = false;

			DmDrug dmDrug = dmDrugCacher.getDmDrug(itemLocation.getItemCode());
			
			MsWorkstoreDrug msWorkstoreDrug = null;

			if (dmDrug != null ) {
				msWorkstoreDrug = dmDrugCacher.getMsWorkstoreDrug(workstore, dmDrug.getItemCode());
			}
			
			if (itemLocationRpt.getSuspend() == null) {

				isFilter = false;
				
			}else{
				if (msWorkstoreDrug != null){
					if ( !itemLocationRpt.getSuspend().equals(msWorkstoreDrug.getSuspend()) ) {
						isFilter = true;
					}
				}else{
					if (StringUtils.equals(itemLocationRpt.getSuspend(), "Y")) {
						isFilter = false;
					}else{
						isFilter = true;
					}
				}
			}

			if (!isFilter) {
				ItemLocationRpt rpt = new ItemLocationRpt();
				rpt.setItemCode(itemLocation.getItemCode());
				
				rpt.setSuspend((msWorkstoreDrug != null) ? msWorkstoreDrug.getSuspend() : "Y");
				rpt.setFullDrugDesc((dmDrug != null) ? dmDrug.getFullDrugDesc() : "");
				rpt.setBaseUnit((dmDrug != null) ? dmDrug.getBaseUnit() : "");
	
				rpt.setPickStationNum(itemLocation.getMachine().getPickStation().getPickStationNum());
				rpt.setMachineType(itemLocation.getMachine().getType().getDisplayValue());
				
				rpt.setBakerCellNum(itemLocation.getBakerCellNum());
				rpt.setBinNum(itemLocation.getBinNum());
				rpt.setWorkstoreCode(itemLocationRpt.getWorkstoreCode());
				
				if (dmDrug !=null) {
					rpt.setDrugName(dmDrug.getDrugName());
					rpt.setTradeName(dmDrug.getDmMoeProperty().getTradename());
					rpt.setStrength(dmDrug.getStrength());
					rpt.setFormDesc(dmDrug.getDmForm().getLabelDesc());
				}
				rpt.setMaxQty(itemLocation.getMaxQty());
				rpt.setHostName(itemLocation.getMachine().getHostName());
				reportList.add(rpt);
			}
		}
		
		return reportList;
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveItemCodeList(List<ItemLocationRpt> itemLocationRptObjList){
		List<String> fullItemList = new ArrayList();
		for(ItemLocationRpt rpt: itemLocationRptObjList){	
			fullItemList.add(rpt.getItemCode());
		}

		HashSet hashSet = new HashSet(fullItemList);
		itemCodeList = new ArrayList(hashSet);
		Collections.sort(itemCodeList);
		itemCodeList.add(0, "All");
	}
	
	@SuppressWarnings("unchecked")
	private void retrievePickStationList(List<ItemLocationRpt> itemLocationRptObjList){
		List<String> fullPickList = new ArrayList();
		for(ItemLocationRpt rpt: itemLocationRptObjList){	
			fullPickList.add(rpt.getPickStationNum().toString());
		}

		HashSet hashSet = new HashSet(fullPickList);
		itemLocationRptPickStationList = new ArrayList(hashSet);
		Collections.sort(itemLocationRptPickStationList);
		itemLocationRptPickStationList.add(0, "All");
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveBakerCellList(List<ItemLocationRpt> itemLocationRptObjList){
		List<String> fullBakerList = new ArrayList();
		for(ItemLocationRpt rpt: itemLocationRptObjList){
			if(rpt.getBakerCellNum()!=null){
				fullBakerList.add(rpt.getBakerCellNum());
			}
		}

		HashSet hashSet = new HashSet(fullBakerList);
		bakerCellList = new ArrayList(hashSet);
		Collections.sort(bakerCellList);
		bakerCellList.add(0, "All");
		bakerCellList.add(1, "");
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveBinNumList(List<ItemLocationRpt> itemLocationRptObjList){
		List<String> fullBinList = new ArrayList();
		for(ItemLocationRpt rpt: itemLocationRptObjList){	
			fullBinList.add(rpt.getBinNum());
		}

		HashSet hashSet = new HashSet(fullBinList);
		binNumList = new ArrayList(hashSet);
		Collections.sort(binNumList);
		binNumList.add(0, "All");
	}
	
	public void clearCbxList() {
		itemCodeList = null;
		itemLocationRptPickStationList = null;
		bakerCellList = null;
		binNumList = null;
	}
	
	public void sendExportItemLocationRpt(List<ItemLocationRpt> exportItemLocationList){
		exportItemLocationRptList = new ArrayList<ItemLocationRpt>();
		exportItemLocationRptList.addAll(exportItemLocationList);
	}
	
	public void exportItemLocationRpt() {	
		parameters.put("hospCode", workstore.getHospCode());
		PrintOption po = new PrintOption();
		po.setDocType(ReportProvider.XLS);
		printAgent.renderAndRedirect(new RenderJob(
				"ItemLocationXls", 
				po, 
				new HashMap<String, Object>(), 
				exportItemLocationRptList));
	}

	@Remove
	public void destroy() 
	{
		if (itemCodeList != null) {
			itemCodeList = null;
		}
		
		if (itemLocationRptPickStationList != null) {
			itemLocationRptPickStationList = null;
		}
		
		if (bakerCellList != null) {
			bakerCellList = null;
		}
		
		if (binNumList != null) {
			binNumList = null;
		}
	}

	
}
