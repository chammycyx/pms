<?xml version="1.0" encoding="utf-8"?>

<s:NavigatorContent xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
					xmlns:fx="http://ns.adobe.com/mxml/2009" 
					xmlns:s="library://ns.adobe.com/flex/spark" 
					xmlns:mx="library://ns.adobe.com/flex/mx"
					xmlns:pivas="hk.org.ha.view.pms.main.pivas.*">
	
	<fx:Metadata>
		[Name("pivasInboxView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.Sort;
			import mx.containers.TabNavigator;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			import mx.utils.StringUtil;
			
			import hk.org.ha.event.pms.main.pivas.AddToWorklistEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasInboxListEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasPrepDetailInfoEvent;
			import hk.org.ha.event.pms.main.pivas.popup.ShowPivasWorklistSortPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.Toolbar;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
			import hk.org.ha.model.pms.udt.pivas.PivasInboxSortColumn;
			import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
			import hk.org.ha.model.pms.vo.pivas.worklist.PivasWorklistResponse;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			
			public static const PIVAS_WORKLIST_INBOX_SORT:String = "pivas.worklist.inbox.sort";
			
			[In]
			public var closeMenuItem:MenuItem;	
			
			[In] [Bindable]
			public var propMap:PropMap;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			// --------------------------------------
			// Variable passed by parent tab
			// --------------------------------------
			public var pmsToolbar:Toolbar;
			
			public var pivasWorklistTab:TabNavigator;

			public var pivasDueOrderView:PivasDueOrderView;
			
			public var pivasWorklistContentView:PivasWorklistContentView;
			
			public var reloadAllTab:Function;
			// --------------------------------------
			
			[Bindable]
			public var pivasWorklistList:ArrayCollection;
			
			[Bindable]
			public var addAllToWorklistFlag:Boolean = false;
			
			public var sortColumnDataValue:String = null;
			
			public function initToolbar():void {
				pmsToolbar.addToWorklistButton.enabled = true;
				pmsToolbar.retrieveButton.enabled = false;
				pmsToolbar.deleteButton.enabled = false;
				pmsToolbar.clearButton.enabled = false;
				pmsToolbar.sortButton.enabled = true;
				pmsToolbar.assignBatchPrintButton.enabled = false;
			}

			public function showSystemMessageAndFocus(errorCode:String, focusUiComponent:UIComponent, params:Array=null):void {
				showSystemMessageAndFocusWithVariableHeight(errorCode, 200, focusUiComponent, params);
			}
			
			public function showSystemMessageAndFocusWithVariableHeight(errorCode:String, messageWinHeight:int, focusUiComponent:UIComponent, params:Array=null):void {
				var okHandler:Function = function(evt:MouseEvent):void {
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					focusUiComponent.setFocus();
				}
				
				showSystemMessageWithVariableHeight(errorCode, messageWinHeight, null, null, okHandler, params);
			}

			private function showSystemMessage(errorCode:String, yesFunc:Function=null, noFunc:Function=null, okfunc:Function=null, params:Array=null):void {
				showSystemMessageWithVariableHeight(errorCode, 200, yesFunc, noFunc, okfunc, params);
			}
			
			private function showSystemMessageWithVariableHeight(errorCode:String, messageWinHeight:int, yesFunc:Function=null, noFunc:Function=null, okfunc:Function=null, params:Array=null):void {	
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = messageWinHeight;
				msgProp.setOkButtonOnly = true;
				
				if( params != null ){
					msgProp.messageParams = params;
				}
				
				if (okfunc != null) {
					msgProp.setOkButtonOnly = true;
					msgProp.setYesNoButton = false;
					msgProp.okHandler = okfunc;
				} else if(noFunc!=null) {
					msgProp.setYesNoButton = true;
					msgProp.setOkButtonOnly = false;
					msgProp.noHandler = noFunc;
				} else if(yesFunc!=null) {
					msgProp.setYesNoButton = true;
					msgProp.setOkButtonOnly = false;
					msgProp.yesHandler = yesFunc;
					msgProp.setYesDefaultButton = false;
					msgProp.setNoDefaultButton = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function addToWorklistHandler():void {
				var addedPivasWorklistList:ArrayCollection = new ArrayCollection();
				for each (var _pivasWorklist:PivasWorklist in pivasWorklistList) {
					if (_pivasWorklist.addToWorklistFlag) {
						addedPivasWorklistList.addItem(_pivasWorklist);
					}
				}

				if (addedPivasWorklistList.length <= 0) {
					return;
				}
				
				var supplyDate:Date = (addedPivasWorklistList[0] as PivasWorklist).supplyDate;
				for each (var addedPivasWorklist:PivasWorklist in addedPivasWorklistList) {
					if (ObjectUtil.compare(addedPivasWorklist.supplyDate, supplyDate) != 0) {
						showSystemMessageAndFocus("0971", pivasInboxGrid);
						return;
					}
				}

				if (PivasWorklistUiInfo.isWorklistProcessing()) {
					return;
				}
				
				PivasWorklistUiInfo.processingInboxTabFlag = true;
				
				dispatchEvent(new AddToWorklistEvent(addedPivasWorklistList, PivasWorklistType.Inbox, addToWorklistCallbackFunc));
				
				// clear add to worklist flag to prevent refresh being skipped
				for each (var pivasWorklist:PivasWorklist in pivasWorklistList) {
					pivasWorklist.addToWorklistFlag = false;
				}
				addAllToWorklistFlag = false;
			}
			
			private function addToWorklistCallbackFunc(pivasWorklistResponse:PivasWorklistResponse):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingInboxTabFlag = false;
				
				// refresh worklist screen if any reload request is pending or supply date not match with worklist tab error occurs
				if (PivasWorklistUiInfo.pendingReloadAllTab || pivasWorklistResponse.errorCode == "0919") {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
				
				if (StringUtil.trim(pivasWorklistResponse.errorCode) != "") {
					if (pivasWorklistResponse.errorParamList != null) {
						if (pivasWorklistResponse.errorCode == "0919") {
							showSystemMessageAndFocusWithVariableHeight(pivasWorklistResponse.errorCode, 250, pivasInboxGrid, pivasWorklistResponse.errorParamList.toArray());
						} else {
							showSystemMessageAndFocus(pivasWorklistResponse.errorCode, pivasInboxGrid, pivasWorklistResponse.errorParamList.toArray());
						}
					} else {
						showSystemMessageAndFocus(pivasWorklistResponse.errorCode, pivasInboxGrid);
					}
				}
			}
			
			public function retrieveHandler():void {
				// skip reload if any add to worklist checkbox is checked
				for each (var pivasWorklist:PivasWorklist in pivasWorklistList) {
					if (pivasWorklist.addToWorklistFlag) {
						return;
					}
				}
				
				// note: because some of processing flag is already set true in PivasWorklistView.loadAllTab() when calling other retrieve function, 
				//       processing flag cannot be checked here and needed to be check in PivasWorklistView.loadAllTab()
				
				PivasWorklistUiInfo.processingInboxTabFlag = true;
				
				dispatchEvent(new RetrievePivasInboxListEvent(updatePivasInboxList));
			}
			
			private function updatePivasInboxList(_pivasWorklistList:ArrayCollection):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingInboxTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
					return;		// no need to proceed when tab will be reloaded since this function is called again
				}
				
				pivasWorklistList = _pivasWorklistList;
				this.label = PivasWorklistType.Inbox.displayValue + " (" + pivasWorklistList.length + ")";
				
				addAllToWorklistFlag = false;
				
				sortPivasWorklistList();
				
				PivasWorklistUiInfo.updatePivasInfoErrorMessage(pivasWorklistList, sysMsgMap);
				
				if (pivasWorklistTab != null && pivasWorklistTab.selectedChild == this) {
					pivasInboxGrid.setFocus();
				}
			}
			
			public function sortHandler():void {
				if (PivasWorklistUiInfo.isWorklistProcessing()) {
					return;
				}
				
				PivasWorklistUiInfo.processingInboxTabFlag = true;
				
				dispatchEvent(new ShowPivasWorklistSortPopupEvent(PivasWorklistType.Inbox, sortOkCallbackFunc, sortCancelCallbackFunc));
			}
			
			private function sortOkCallbackFunc(_sortColumnDataValue:String):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingInboxTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
				
				sortColumnDataValue = _sortColumnDataValue;
				sortPivasWorklistList();
			}

			private function sortCancelCallbackFunc():void {
				// resume procesing flag
				PivasWorklistUiInfo.processingInboxTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
			}
			
			private function sortPivasWorklistList():void {
				var pivasInboxSortColumn:PivasInboxSortColumn = null;
				if (sortColumnDataValue == null) {
					// set default sort column
					var defaultSortColumnDataValue:String = propMap.getValue(PIVAS_WORKLIST_INBOX_SORT);
					pivasInboxSortColumn = PivasInboxSortColumn.dataValueOf(defaultSortColumnDataValue);
				} else {
					pivasInboxSortColumn = PivasInboxSortColumn.dataValueOf(sortColumnDataValue);
				}
				
				pivasWorklistList.sort = new Sort();
				
				if (pivasInboxSortColumn != null) {
					switch (pivasInboxSortColumn) {
						case PivasInboxSortColumn.DueDate:
							pivasWorklistList.sort.compareFunction = dueDateCompareFunc;
							break;
						case PivasInboxSortColumn.ReceivedDateDueDate:
							pivasWorklistList.sort.compareFunction = receivedDateDueDateCompareFunc;
							break;
						case PivasInboxSortColumn.SupplyDateDueDate:
							pivasWorklistList.sort.compareFunction = supplyDateDueDateCompareFunc;
							break;
						case PivasInboxSortColumn.PhsWardBedNumPatName:
							pivasWorklistList.sort.compareFunction = phsWardBedNumPatNameCompareFunc;
							break;
					}
				}
				
				pivasWorklistList.refresh();
			}
			
			private function dueDateCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var dueDateResult:int = ObjectUtil.compare(pivasWorklist1.dueDate, pivasWorklist2.dueDate);				
				if (dueDateResult != 0) {
					return dueDateResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}			

			private function receivedDateDueDateCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var receivedDate1:Date;
				if (ObjectUtil.compare(pivasWorklist1.modifyDate, pivasWorklist1.medProfileMoItem.medProfileItem.verifyDate) > 0) {
					receivedDate1 = pivasWorklist1.modifyDate;
				} else {
					receivedDate1 = pivasWorklist1.medProfileMoItem.medProfileItem.verifyDate;
				}

				var receivedDate2:Date;
				if (ObjectUtil.compare(pivasWorklist2.modifyDate, pivasWorklist2.medProfileMoItem.medProfileItem.verifyDate) > 0) {
					receivedDate2 = pivasWorklist2.modifyDate;
				} else {
					receivedDate2 = pivasWorklist2.medProfileMoItem.medProfileItem.verifyDate;
				}
				
				var receivedDateResult:int = ObjectUtil.compare(receivedDate1, receivedDate2);
				if (receivedDateResult != 0) {
					return receivedDateResult;
				}
				
				var dueDateResult:int = ObjectUtil.compare(pivasWorklist1.dueDate, pivasWorklist2.dueDate);				
				if (dueDateResult != 0) {
					return dueDateResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}
			
			private function supplyDateDueDateCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var supplyDateResult:int = ObjectUtil.compare(pivasWorklist1.supplyDate, pivasWorklist2.supplyDate);
				if (supplyDateResult != 0) {
					return supplyDateResult;
				}
				
				var dueDateResult:int = ObjectUtil.compare(pivasWorklist1.dueDate, pivasWorklist2.dueDate);				
				if (dueDateResult != 0) {
					return dueDateResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}
			
			private function phsWardBedNumPatNameCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var wardCodeResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.wardCode, pivasWorklist2.medProfile.wardCode);
				if (wardCodeResult != 0) {
					return wardCodeResult;
				}
				
				var bedNumResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.medCase.pasBedNum, pivasWorklist2.medProfile.medCase.pasBedNum);
				if (bedNumResult != 0) {
					return bedNumResult;
				}

				var patNameResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.patient.name, pivasWorklist2.medProfile.patient.name);
				if (patNameResult != 0) {
					return patNameResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}

			protected function pivasInboxGridItemRendererFunction(data:Object):IFactory {
				return pivasInboxRenderer;
			}
			
			public function addAllToWorklistFlagChkBoxChangeHandler():void {
				// clear sorting for performance
				pivasWorklistList.sort = null;	
				
				// update checkbox
				for each (var pivasWorklist:PivasWorklist in pivasWorklistList) {
					if (PivasWorklistUiInfo.getAddToWorklistFlagChkBoxVisible(pivasWorklist)) {
						pivasWorklist.addToWorklistFlag = addAllToWorklistFlag;
					}
				}
				
				// re-run sorting after clearing at beginning
				sortPivasWorklistList();
				
				// reload list if no add to worklist checkbox is checked
				// (because reload is skipped if any add to worklist checkbox is checked)
				if ( ! addAllToWorklistFlag) {
					reloadAllTab();
				}
			}
			
			public function updateAddAllToWorklistFlagChkBox():void {
				var selectAll:Boolean = true;
				for each (var pivasWorklist:PivasWorklist in pivasWorklistList) {
					if (PivasWorklistUiInfo.getAddToWorklistFlagChkBoxVisible(pivasWorklist) && ! pivasWorklist.addToWorklistFlag){
						selectAll = false;
						break;
					}
				}
				
				addAllToWorklistFlag = selectAll;
				
				// reload list if no add to worklist checkbox is checked
				// (because reload is skipped if any add to worklist checkbox is checked)
				var deselectAll:Boolean = true;
				for each (var pivasWorklist:PivasWorklist in pivasWorklistList) {
					if (PivasWorklistUiInfo.getAddToWorklistFlagChkBoxVisible(pivasWorklist) && pivasWorklist.addToWorklistFlag){
						deselectAll = false;
						break;
					}
				}
				
				if (deselectAll) {
					reloadAllTab();
				}
			}
			
			private function pivasInboxGridItemDoubleClickHandler(evt:MouseEvent):void {
				if (evt.target == pivasInboxGrid.dataGroup) {
					return;
				}
				
				if (pivasInboxGrid.selectedItem == null) {
					return;
				}
				
				var _pivasWorklist:PivasWorklist = pivasInboxGrid.selectedItem as PivasWorklist;
				
				if ( ! _pivasWorklist.allowEditFlag) {
					return;
				}
				
				if (PivasWorklistUiInfo.isWorklistProcessing()) {
					return;
				}
				
				PivasWorklistUiInfo.processingInboxTabFlag = true;	// note: processing flag is cleared in onShowLater of PivasWorklistView
				
				
				// proceed to edit screen
				var retrievePivasPrepDetailInfoEvent:RetrievePivasPrepDetailInfoEvent = new RetrievePivasPrepDetailInfoEvent();
				
				var copyPivasWorklist:PivasWorklist = ObjectUtil.copy(_pivasWorklist) as PivasWorklist;	// clone object to allow fallback data when cancel in popup
				
				retrievePivasPrepDetailInfoEvent.sourceType = PivasWorklistType.Inbox;
				retrievePivasPrepDetailInfoEvent.pivasWorklist = copyPivasWorklist;
				
				dispatchEvent(retrievePivasPrepDetailInfoEvent);
				
				// clear add to worklist flag to prevent refresh being skipped
				for each (var pivasWorklist:PivasWorklist in pivasWorklistList) {
					pivasWorklist.addToWorklistFlag = false;
				}
				addAllToWorklistFlag = false;
			}
		]]>
		
	</fx:Script>
	
	<fx:Declarations>
		<!-- Place non-visual elements (e.g., services, value objects) here -->
		<fx:Component id="pivasInboxRenderer">
			<pivas:PivasInboxRenderer updateAddAllToWorklistFlagChkBoxFunc="{outerDocument.updateAddAllToWorklistFlagChkBox}"/>
		</fx:Component>
	</fx:Declarations>
	
	<s:layout>
		<s:VerticalLayout paddingTop="0" paddingLeft="10" paddingRight="10" paddingBottom="10" gap="10"/>
	</s:layout>
	
	<s:VGroup width="100%" height="100%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">
		<fc:ExtendedDataGrid id="pivasInboxHeaderGrid" width="100%" height="0%" wordWrap="true"
							 sortableColumns="false" resizableColumns="false" draggableColumns="false">
			<fc:columns>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxAddToWorklistFlagColWidth}" editable="false" sortable="false" resizable="false">
					<mx:headerRenderer>
						<fx:Component>
							<s:MXDataGridItemRenderer autoDrawBackground="false">
								<s:layout>
									<s:HorizontalLayout horizontalAlign="left" paddingLeft="5"/>
								</s:layout>
								<s:CheckBox id="addAllToWorklistFlagChkBox"  enabled="{outerDocument.pivasWorklistList != null &amp;&amp; outerDocument.pivasWorklistList.length > 0}" 
											selected="@{outerDocument.addAllToWorklistFlag}" 
											change="outerDocument.addAllToWorklistFlagChkBoxChangeHandler()"/>
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:headerRenderer>
				</mx:DataGridColumn>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxIndicatorsColWidth}" headerText="Indicators"/>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxDueDateColWidth}" headerText="Due Date"/>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxSupplyDateColWidth}" headerText="Supply Time&#10;(Next Refill)"/>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxPatientInfoColWidth}" headerText="Name&#10;Case No. / PHS Ward (Bed)"/>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxOrderDetailsColWidth}" headerText="Order Details"/>
				<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasInboxReceivedTimeColWidth}" headerText="Received Time"/>
			</fc:columns>
		</fc:ExtendedDataGrid>		
		<s:List id="pivasInboxGrid" width="100%" height="100%" dataProvider="{pivasWorklistList}"				 
				useVirtualLayout="false" hasFocusableChildren="true" horizontalScrollPolicy="off" verticalScrollPolicy="on"
				itemRendererFunction="{pivasInboxGridItemRendererFunction}"
				doubleClickEnabled="true" doubleClick="pivasInboxGridItemDoubleClickHandler(event)"/>		
	</s:VGroup>
</s:NavigatorContent>