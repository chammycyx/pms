<?xml version="1.0" encoding="utf-8"?>

<s:NavigatorContent xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
					xmlns:fx="http://ns.adobe.com/mxml/2009" 
					xmlns:s="library://ns.adobe.com/flex/spark" 
					xmlns:mx="library://ns.adobe.com/flex/mx">
	
	<fx:Metadata>
		[Name("batchPreparationView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			
			import spark.events.TextOperationEvent;
			
			import flexlib.scheduling.util.DateUtil;
			
			import hk.org.ha.event.pms.main.pivas.DeletePivasBatchPrepEvent;
			import hk.org.ha.event.pms.main.pivas.DeletePivasBatchPrepResultEvent;
			import hk.org.ha.event.pms.main.pivas.QuickAddPivasBatchPrepEvent;
			import hk.org.ha.event.pms.main.pivas.RefreshPivasProductLabelPreviewEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasBatchPrepEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasBatchPrepListEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaDrugListByPrefixDisplayNameEvent;
			import hk.org.ha.event.pms.main.pivas.SetPivasBatchPrepListEvent;
			import hk.org.ha.event.pms.main.pivas.SetPivasFormulaDrugListEvent;
			import hk.org.ha.event.pms.main.pivas.SetPivasFormulaListEvent;
			import hk.org.ha.event.pms.main.pivas.UpdatePivasBatchPrepResultEvent;
			import hk.org.ha.event.pms.main.pivas.popup.ShowBatchPrepFormulaSelectionPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.Toolbar;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
			import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
			import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;
			import hk.org.ha.model.pms.udt.pivas.PivasBatchPrepStatus;
			import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheet;
			import hk.org.ha.model.pms.vo.label.PivasMergeFormulaProductLabel;
			import hk.org.ha.model.pms.vo.label.PivasMergeFormulaProductLabelDrugDtl;
			import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetDrugInstructionDtl;
			import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetRawMaterialDtl;
			import hk.org.ha.model.pms.vo.label.PivasMergeFormulaWorksheetSolventInstructionDtl;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.pivas.popup.BatchPrepAddBatchPrepPopup;
			import hk.org.ha.view.pms.main.pivas.popup.BatchPrepFormulaSelectionPopup;
			
			
			
			
			
			
			
			
			
			
			[In]
			public var closeMenuItem:MenuItem;	
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			// --------------------------------------
			// Variable passed by parent tab
			// --------------------------------------
			public var pmsToolbar:Toolbar;
			// --------------------------------------
			
			private var batchPrepFormulaSelectionPopup:BatchPrepFormulaSelectionPopup;
			
			private var batchPrepAddBatchPrepPopup:BatchPrepAddBatchPrepPopup;
			
			// declare labelXml classes to fix exception when frontend search and retrieve PivasBatchPrep Merge Formula printed records
			private var pivasMergeFormulaWorksheet:PivasMergeFormulaWorksheet;
			private var pivasMergeFormulaProductLabel:PivasMergeFormulaProductLabel;
			private var pivasMergeFormulaProductLabelDrugDtl:PivasMergeFormulaProductLabelDrugDtl;
			private var pivasMergeFormulaWorksheetSolventInstructionDtl:PivasMergeFormulaWorksheetSolventInstructionDtl;
			private var pivasMergeFormulaWorksheetDrugInstructionDtl:PivasMergeFormulaWorksheetDrugInstructionDtl;
			private var pivasMergeFormulaWorksheetRawMaterialDtl:PivasMergeFormulaWorksheetRawMaterialDtl;
			
			[Bindable]
			private var pivasBatchPrepList:ArrayCollection;
			
			[Bindable]
			public var batchPreparationContextMenu:ContextMenu = new ContextMenu();
			protected var batchPreparationAddMenuItem:ContextMenuItem = new ContextMenuItem("Add Batch Preparation");
			protected var batchPreparationQuickAddMenuItem:ContextMenuItem = new ContextMenuItem("Quick Add Batch Preparation");
			
			private var initScreen:Boolean = true;
			
			private function batchPreparationVGroupCreationCompleteHandler():void {
				// since createAll policy is not used in TabNavigator, this tab content page is not rendered at beginning and init 
				// context menu cannot be called by onShowLater of main page.   
				initContextMenu();
			}
			
			public function initToolbar():void {
				pmsToolbar.addToWorklistButton.enabled = false;
				pmsToolbar.retrieveButton.enabled = manufFromDate.enabled;
				pmsToolbar.deleteButton.enabled = false;
				pmsToolbar.clearButton.enabled = true;
				pmsToolbar.sortButton.enabled = false;
				pmsToolbar.assignBatchPrintButton.enabled = false;
			}
			
			public function clearHandler():void {
				displayNameTxt.errorString = "";
				displayNameTxt.text = "";
				saltProperyTxt.text = "";
				drugKeyTxt.text = "";
				formCodeTxt.text = "";
				manufFromDate.selectedDate = new Date();
				manufToDate.selectedDate = new Date();
				manufFromDate.enabled = true;
				manufToDate.enabled = true;
				displayNameTxt.enabled = true;
				pivasBatchPrepList = null;
				pmsToolbar.retrieveButton.enabled = true;
			}		
			
			public function retrieveHandler():void 
			{
				displayNameTxt.errorString = "";
				if ( displayNameTxt.text.length > 0 &&  drugKeyTxt.text.length == 0) {
					displayNameTxt.errorString = sysMsgMap.getMessage("0791");//Please input drug.
					return;
				}
				var drugKey:Number = NaN;
				if ( drugKeyTxt.text.length > 0 ) {
					drugKey = Number(drugKeyTxt.text);
				}
				dispatchEvent(new RetrievePivasBatchPrepListEvent(manufFromDate.selectedDate, manufToDate.selectedDate, drugKey));
			}
			
			[Observer]
			public function setPivasBatchPrepList(evt:SetPivasBatchPrepListEvent):void {
				pivasBatchPrepList = evt.pivasBatchPrepList;
				manufFromDate.enabled = false;
				manufToDate.enabled = false;
				displayNameTxt.enabled = false;
				pmsToolbar.retrieveButton.enabled = false;
			}
			
			public function initContextMenu():void 
			{ 
				batchPreparationContextMenu.customItems = [batchPreparationAddMenuItem, batchPreparationQuickAddMenuItem];				
				batchPreparationContextMenu.hideBuiltInItems();	
				batchPreparationContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, updateContextMenu);
				batchPreparationAddMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, addBatchPreparation);	
				batchPreparationQuickAddMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, quickAddBatchPreparation);	
				batchPreparationVGroup.contextMenu = batchPreparationContextMenu;
			}
			
			protected function updateContextMenu(evt:ContextMenuEvent):void
			{
				batchPreparationQuickAddMenuItem.enabled = batchPreparationGrid.selectedItem != null && batchPreparationGrid.selectedItem.status == PivasBatchPrepStatus.Printed;
			}
			
			private function addBatchPreparation(evt:ContextMenuEvent):void
			{
				dispatchEvent(new ShowBatchPrepFormulaSelectionPopupEvent(displayNameTxt.text));
			}
			
			private function quickAddBatchPreparation(evt:ContextMenuEvent):void
			{
				if ( batchPreparationGrid.selectedItem != null ) {
					dispatchEvent(new QuickAddPivasBatchPrepEvent(batchPreparationGrid.selectedItem.lotNum));
				}
			}
			
			public function showSystemMessage(errorCode:String, okfunc:Function=null, param:Array=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				if ( param != null ) {
					msgProp.messageParams = param;
				}
				if ( okfunc != null ) {
					msgProp.okHandler = okfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function showLookupPopup(pivasFormulaDrugList:ArrayCollection):void {
				var prop:LookupPopupProp = new LookupPopupProp();
				prop.doubleClickHandler = function(evt:ListEvent):void {
					var selectedItem:PivasFormulaDrug = evt.itemRenderer.data as PivasFormulaDrug;
					if ( selectedItem != null ) {
						displayNameTxt.text = selectedItem.displayname;
						saltProperyTxt.text = selectedItem.saltProperty;
						formCodeTxt.text = selectedItem.formCode;
						drugKeyTxt.text = selectedItem.drugKey.toString();
						PopUpManager.removePopUp((evt.currentTarget as UIComponent).parentDocument as IFlexDisplayObject);
						retrieveHandler();
					}
				};
				prop.lookupWinHeight = 230;
				prop.lookupWinWidth = 622;
				prop.title = "PIVAS Drugs";
				var displaynameColumn:DataGridColumn = LookupPopupProp.getColumn("Display Name", "displayname");
				var saltColumn:DataGridColumn = LookupPopupProp.getColumn("Salt Property", "saltProperty");
				var formColumn:DataGridColumn = LookupPopupProp.getColumn("Form Code", "formCode");
				displaynameColumn.sortable = false;
				saltColumn.sortable = false;
				formColumn.sortable = false;
				prop.columns = [displaynameColumn, saltColumn, formColumn];
				prop.dataProvider = pivasFormulaDrugList;
				
				dispatchEvent(new ShowLookupPopupEvent(prop));
			}
			

			protected function displayNameTxt_changeHandler(event:TextOperationEvent):void
			{
				if ( displayNameTxt.text.length < 4 ) {
					return;
				}
				dispatchEvent(new RetrievePivasFormulaDrugListByPrefixDisplayNameEvent(displayNameTxt.text));
			}
			
			private function manufactureDateLabelFunction(item:PivasBatchPrep, column:DataGridColumn):String 
			{
				return batchPrepDateFormatter.format(item.manufactureDate);
			}
			
			private function expiryDateLabelFunction(item:PivasBatchPrep, column:DataGridColumn):String 
			{
				return batchPrepDateFormatter.format(item.prepExpiryDate);
			}
			
			private function labelDescLabelFunction(item:PivasBatchPrep, column:DataGridColumn):String 
			{
				return orderDetailDataTipFunction(item);
			}
			
			private function qtyAndNoOfLabelLabelFunction(item:PivasBatchPrep, column:DataGridColumn):String 
			{
				if ( item.prepQty == item.numOfLabel ) {
					return item.prepQty.toString();
				} 
				return item.prepQty +" ("+item.numOfLabel+")";
			}
			
			private function orderDetailDataTipFunction(item:PivasBatchPrep):String 
			{
				if (item.pivasFormulaType == PivasFormulaType.Merge) {
					prepDetail = item.printName.concat("\n", item.dmSite.ipmoeDesc);
				} else {
					var prepDetail:String = item.printName + "\n";
					var siteDesc:String = "";
					
					if(item.dmSite != null){
						siteDesc = item.dmSite.ipmoeDesc;
					}			
					
					var pdu:String = item.pivasBatchPrepItemList.getItemAt(0).pivasDosageUnit;
					
					if(pdu == "ML"){
						prepDetail = prepDetail + siteDesc;
					}else{
						prepDetail = prepDetail + item.concn + pdu + "/ML" + " - " + siteDesc;
					}
				}
				
				return prepDetail;
			}
			
			protected function batchPreparationGrid_itemDoubleClickHandler(event:ListEvent):void
			{
				if ( batchPreparationGrid.selectedItem != null && batchPreparationGrid.selectedItem.status == PivasBatchPrepStatus.Printed) {
					dispatchEvent(new RetrievePivasBatchPrepEvent(batchPreparationGrid.selectedItem.lotNum));
				}
			}
			
			public function batchPreparationGrid_keyUpHandler(evt:KeyboardEvent):void
			{
				if (evt.ctrlKey) {
					if ( evt.keyCode == Keyboard.INSERT ) {
						dispatchEvent(new ShowBatchPrepFormulaSelectionPopupEvent(displayNameTxt.text));
					}	
					else if ( evt.keyCode == Keyboard.DELETE ) {
						if ( batchPreparationGrid.selectedItem != null && batchPreparationGrid.selectedItem.status == PivasBatchPrepStatus.Printed) {
							var today:Date = new Date();
							DateUtil.clearTime(today);
							if ( ObjectUtil.dateCompare(batchPreparationGrid.selectedItem.manufactureDate, today) == -1) {
								//Cannot delete the past record [#0].
								showSystemMessage("0910", null, new Array(batchPreparationGrid.selectedItem.lotNum));
							}
							else {
								//Delete [#0] batch preparation record, please confirm if you want to proceed.
								var drugKey:Number = NaN;
								if ( drugKeyTxt.text.length > 0 ) {
									drugKey = Number(drugKeyTxt.text);
								}
								showSystemMessageYesNoBtn("0876", 
									function(evt:MouseEvent):void {
										PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
										dispatchEvent(new DeletePivasBatchPrepEvent(batchPreparationGrid.selectedItem.lotNum, 
																						manufFromDate.selectedDate, 
																						manufToDate.selectedDate, 
																						drugKey));
									}, new Array(batchPreparationGrid.selectedItem.lotNum));
							}
						}
					}
				}
			}
			
			public function showSystemMessageYesNoBtn(errorCode:String, yesfunc:Function=null, param:Array=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setYesNoButton = true;
				msgProp.setNoDefaultButton = true;
				if ( param != null ) {
					msgProp.messageParams = param;
				}
				
				if (yesfunc != null)
				{
					msgProp.yesHandler = yesfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function showBatchPrepFormulaSelectionPopup(_batchPrepFormulaSelectionPopup:BatchPrepFormulaSelectionPopup):void {
				batchPrepFormulaSelectionPopup = _batchPrepFormulaSelectionPopup;
			}
			
			public function showBatchPrepAddBatchPrepPopup(_batchPrepAddBatchPrepPopup:BatchPrepAddBatchPrepPopup):void {
				batchPrepAddBatchPrepPopup = _batchPrepAddBatchPrepPopup;
			}			
			
			[Observer]
			public function setPivasFormulaList(evt:SetPivasFormulaListEvent):void {
				batchPrepFormulaSelectionPopup.setFormulaList(evt.pivasFormulaList);
			}
			
			[Observer]
			public function setPivasFormulaDrugList(evt:SetPivasFormulaDrugListEvent):void {
				showLookupPopup(evt.pivasFormulaDrugList);
			}
			
			[Observer]
			public function updatePivasBatchPrepResult(evt:UpdatePivasBatchPrepResultEvent):void {
				batchPrepAddBatchPrepPopup.saveResult(evt.pivasPrepValidateResponse);
			}
			
			[Observer]
			public function deletePivasBatchPrepResult(evt:DeletePivasBatchPrepResultEvent):void {
				showSystemMessage("0307");
			}
			
			[Observer]
			public function refreshPivasProductLabelPreview(evt:RefreshPivasProductLabelPreviewEvent):void {
				batchPrepAddBatchPrepPopup.genPivasBatchPrepLabelPreview();
			}
		]]>
		
	</fx:Script>
	
	<fx:Declarations>
		<mx:DateFormatter id="batchPrepDateFormatter" formatString="DD-MMM-YYYY JJ:NN"/>
	</fx:Declarations>
	
	<s:layout>
		<s:VerticalLayout paddingTop="0" paddingLeft="10" paddingRight="10" paddingBottom="10" gap="10"/>
	</s:layout>
	
	<s:Panel title="Search Criteria" width="100%" height="100" >
		<s:layout>
			<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10" verticalAlign="middle"/>
		</s:layout>
		<s:HGroup width="100%" verticalAlign="middle">
			<s:Label text="Manufacture Date"/>
			<fc:ExtendedAbbDateField id="manufFromDate" 
									 selectedDate="{new Date()}"/>
			<s:Label text="to"/>
			<fc:ExtendedAbbDateField id="manufToDate" 
									 selectedDate="{new Date()}"/>
		</s:HGroup>
		<s:HGroup width="100%" verticalAlign="middle">
			<fc:ExtendedTextInput id="drugKeyTxt" includeInLayout="false" visible="false"/>
			<s:Label text="Display Name"/>
			<fc:UppercaseTextInput id="displayNameTxt" width="355" change="displayNameTxt_changeHandler(event)"/>
			<s:Label text="Salt Property"/>
			<fc:ExtendedTextInput id="saltProperyTxt" enabled="false"/>
			<s:Label text="Form Code"/>
			<fc:ExtendedTextInput id="formCodeTxt" enabled="false"/>
		</s:HGroup>
	</s:Panel>
	<s:VGroup id="batchPreparationVGroup" width="100%" height="100%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0"
			  mouseEnabled="true" creationComplete="batchPreparationVGroupCreationCompleteHandler()">
		<s:Panel title="Batch Preparation" width="100%" height="100%"  >
			<fc:ExtendedDataGrid id="batchPreparationGrid" width="100%" height="100%"
								 dataProvider="{pivasBatchPrepList}"
								 draggableColumns="false" horizontalScrollPolicy="auto" sortableColumns="false" resizableColumns="false"
								 doubleClickEnabled="true" variableRowHeight="true" wordWrap="true" 
								 itemDoubleClick="batchPreparationGrid_itemDoubleClickHandler(event)">
				<fc:columns>
					<mx:DataGridColumn headerText="Lot No." dataField="lotNum" width="{batchPreparationGrid.width*0.12}"/>
					<mx:DataGridColumn headerText="Manufacture Date" labelFunction="manufactureDateLabelFunction" width="{batchPreparationGrid.width*0.126}"/>
					<mx:DataGridColumn headerText="Preparation Detail" labelFunction="labelDescLabelFunction" 
									   width="{batchPreparationGrid.width*0.389}"/>
					<mx:DataGridColumn headerText="Qty (No. of Label)" labelFunction="qtyAndNoOfLabelLabelFunction" width="{batchPreparationGrid.width*0.115}"/>
					<mx:DataGridColumn headerText="Expiry Date" labelFunction="expiryDateLabelFunction" width="{batchPreparationGrid.width*0.125}"/>
					<mx:DataGridColumn headerText="Status" dataField="status"/>
				</fc:columns>
			</fc:ExtendedDataGrid>
		</s:Panel>
		<s:BorderContainer width="100%" height="20">
			<s:HGroup width="100%" horizontalAlign="left" paddingLeft="5" paddingTop="5">
				<s:Label text="&lt;Ctrl+Insert&gt; to add record; &lt;Ctrl+Delete&gt; to delete record" styleName="footNoteLabelStyle"/>
			</s:HGroup>
			<s:HGroup width="100%"  paddingRight="5" paddingTop="4" horizontalAlign="right" verticalAlign="middle">
				<s:Label text="{pivasBatchPrepList.length} record(s)" />
			</s:HGroup>
		</s:BorderContainer>
	</s:VGroup>
</s:NavigatorContent>