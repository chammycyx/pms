package hk.org.ha.model.pms.biz.vetting;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.conversion.ConvertParam;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.vo.rx.RxItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("prevPrescService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PrevPrescServiceBean implements PrevPrescServiceLocal {

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.vo.rx";

	@PersistenceContext
	private EntityManager em;

	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@SuppressWarnings("unchecked")
	public MedOrder retrievePrevPresc(String orderNum) {

		List<MedOrder> medOrderList = (List<MedOrder>) em.createQuery(
				"select o from MedOrder o" + // 20120307 index check : MedOrder.orderNum : I_MED_ORDER_01
				" where o.orderNum = :orderNum" +
				" and o.status = :status")
				.setParameter("orderNum", orderNum)
				.setParameter("status", MedOrderStatus.Deleted)
				.getResultList();
		
		if (medOrderList.isEmpty()) {
			return null;
		}

		MedOrder prevMedOrder = medOrderList.get(0);
		prevMedOrder.loadChild();

		if (prevMedOrder.isMpDischarge()) {
			formatDrugOrder(prevMedOrder);
		}

		for (MedOrderItem moi : prevMedOrder.getMedOrderItemList()) {
			moi.loadDmInfo(prevMedOrder.isMpDischarge());
		}
		
		Collections.sort(prevMedOrder.getMedOrderItemList(), new MedOrderItemComparator());
		return prevMedOrder;

	}

	private void formatDrugOrder(MedOrder medOrder) {
		
		JaxbWrapper<RxItem> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
		
		List<String> rxItemXmlList = new ArrayList<String>();

		for (MedOrderItem moi : medOrder.getMedOrderItemList()) {
			RxItem rxItem = moi.getRxItem();
			rxItem.setItemNum(moi.getItemNum());
			rxItemXmlList.add(rxJaxbWrapper.marshall(rxItem));
		}
		
		ConvertParam convertParam = new ConvertParam();
		convertParam.setOrderType("D");
		//Format for Discharge only
		rxItemXmlList = dmsPmsServiceProxy.formatDrugOrderXml(rxItemXmlList, convertParam);
		
		for (String rxItemXml : rxItemXmlList) {
			RxItem rxItem = rxJaxbWrapper.unmarshall(rxItemXml);
			rxItem.buildTlf();
			medOrder.getMedOrderItemByItemNum(rxItem.getItemNum()).setRxItem(rxItem);
		}
	}

	@Remove
	public void destroy() {
	}
}
