<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009" 
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	manualDashboardOperation="true"
	maxHeight="22"
	click="focusManager.showFocus()">

	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.prevet.RetrieveDischargeMedOrderEvent;
			import hk.org.ha.event.pms.main.vetting.show.ShowOrderViewEvent;
			import hk.org.ha.model.pms.persistence.disp.MedOrder;
			import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
			import hk.org.ha.model.pms.udt.onestop.OneStopOrderType;
			import hk.org.ha.model.pms.udt.prevet.PreVetMode;
			import hk.org.ha.model.pms.vo.alert.SteroidTag;
			import hk.org.ha.model.pms.vo.alert.mds.MdsUtils;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
			
			import mx.collections.ArrayCollection;
			import mx.events.FlexEvent;
			import mx.managers.IFocusManagerComponent;
			
			import spark.components.DropDownList;
			import spark.events.IndexChangeEvent;

			[Bindable]
			public var oneStopSpecialtyList:ArrayCollection;
			
			[Bindable]
			public var oneStopPatientCatList:ArrayCollection;
			
			[Bindable]
			public var oneStopWardList:ArrayCollection;
			
			private var _specCode:String;

			[Bindable]
			private function get specCode():String {
				return _specCode == null ? "" : _specCode;
			}
			private function set specCode(specCode:String):void {
				_specCode = specCode;
			}

			private var _ward:String;

			[Bindable]
			private function get ward():String {
				return _ward == null ? "" : _ward;
			}
			private function set ward(ward:String):void {
				_ward = ward;
			}
			
			private var _patCatCode:String;
			
			[Bindable]
			private function get patCatCode():String {
				return _patCatCode == null ? "" : _patCatCode;
			}
			
			private function set patCatCode(patCatCode:String):void {
				_patCatCode = patCatCode;
			}

			[Bindable]
			public var orderViewInfo:OrderViewInfo;
			
			[Bindable]
			public var medOrder:MedOrder;

			[Bindable]
			private var allowEditFlag:Boolean;

			[Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[Bindable]
			public var propMap:PropMap;
			
			[Bindable]
			public var screenEnableFlag:Boolean = true;
			
			[Bindable]
			public var errorMsg:String;
			
			[Bindable]
			public var showCurrMedButton:Boolean = false;
			
			[Bindable]
			public var steroidTag:SteroidTag;
			
			private static var PRE_VET:String = "Discharge Rx Reconciliation";
			private static var PHARMACY_INBOX:String = "Inbox";
			
			public var pregnancyFunc:Function = function():void {};
			
			private function specCodeChangingHandler(evt:IndexChangeEvent):void {
				if (evt.newIndex == -1 || !screenEnableFlag) {
					evt.preventDefault();
				}
			}

			private function specCodeChangeHandler(evt:IndexChangeEvent):void {
				specCode = specCodeDropDown.selectedItem as String;
				specCodeDropDown.errorString = null;
			}
			
			private function wardChangingHandler(evt:IndexChangeEvent):void {
				if (!screenEnableFlag) {
					evt.preventDefault();
				}
			}
			
			private function wardChangeHandler(evt:IndexChangeEvent):void {
				ward = wardDropDown.selectedItem as String;
			}
			
			private function patCatChangingHandler(evt:IndexChangeEvent):void {
				if ((evt.newIndex == -1 && propMap.getValueAsBoolean("oneStop.patient.patCatCode.required")) || !screenEnableFlag) {
					evt.preventDefault();
				}
			}

			private function patCatChangeHandler(evt:IndexChangeEvent):void {
				patCatCode = patCatDropDown.selectedItem as String;
				patCatDropDown.errorString = null;
			}
			
			private function dropdownCreationCompleteHandler(dropDown:DropDownList):void {
				var openHandler:Function = function(evt:FlexEvent):void {
					if (!screenEnableFlag) {
						evt.stopImmediatePropagation();
					}
				};
				dropDown.openButton.addEventListener(FlexEvent.BUTTON_DOWN, openHandler, false, 1);
			}
			
			public function validate(successHandler:Function):Boolean {
				if (specCode == null || specCode == '') {
					specCodeDropDown.errorString = sysMsgMap.getMessage("0362");
					specCodeDropDown.setFocus();
					return false;
				}
				if (propMap.getValueAsBoolean("oneStop.patient.patCatCode.required") && (patCatCode == null || patCatCode == '')) {
					patCatDropDown.errorString = sysMsgMap.getMessage("0363");
					patCatDropDown.setFocus();
					return false;
				}
				if (!oneStopPatientCatList.contains(patCatCode) && patCatCode != null && patCatCode != '') {
					patCatDropDown.errorString = sysMsgMap.getMessage("0208");
					patCatDropDown.setFocus();
					return false;
				}
				successHandler();
				return true;
			}
			
			private function getErrorString(... args):String {
				for (var i:int=0; i<args.length; i++) {
					if (args[i] != null && args[i] != "") {
						return args[i] as String;
					}
				}
				return null;
			}

			private function keyFocusChangeHandler(evt:FocusEvent):void {
				if (!screenEnableFlag) {
					evt.preventDefault();
				}
			}

			public var checkBeforeCurrentMedFunc:Function = function():Boolean {
				return true;
			}
			
			private function currentMedFunc():void {
				if (checkBeforeCurrentMedFunc()) {
					if (parentApplication.preVetInstance != null)
					{
						var showOrderViewEvent:ShowOrderViewEvent = new ShowOrderViewEvent();
						showOrderViewEvent.errorString = sysMsgMap.getMessage("0689", new Array(PHARMACY_INBOX, PRE_VET));
						dispatchEvent(showOrderViewEvent);
						return;
					}
					
					dispatchEvent(new RetrieveDischargeMedOrderEvent(medOrder.orderNum, PreVetMode.Vetting, medOrder.patient));
				}
			}
			
			private function buttonFocusInHandler(evt:FocusEvent):void {
				if (evt.relatedObject != null && evt.relatedObject is IFocusManagerComponent && evt.relatedObject != evt.target) {
					(evt.relatedObject as IFocusManagerComponent).setFocus();
				}
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="orderViewInfo.specCode"
				destination="specCode"
				twoWay="true"/>

	<fx:Binding source="orderViewInfo.wardCode"
				destination="ward"
				twoWay="true"/>
	
	<fx:Binding source="orderViewInfo.patCatCode"
				destination="patCatCode"
				twoWay="true"/>
	
	<fx:Binding source="{orderViewInfo.allowEditFlag &amp;&amp; orderViewInfo.oneStopOrderType != OneStopOrderType.SfiOrder}"
				destination="allowEditFlag"/>
	
	<fx:Binding source="{getErrorString(specCodeDropDown.errorString, patCatDropDown.errorString)}"
				destination="errorMsg"/>
	
	<fc:states>
		<s:State name="default"/>
		<s:State name="vettingList"/>
		<s:State name="orderEdit"/>
	</fc:states>
	
	<fc:layout>
		<s:HorizontalLayout verticalAlign="middle"/>
	</fc:layout>

		<s:Label text="PHS Spec." alignmentBaseline="descent"/>
		<fc:AdvancedDropDownList id="specCodeDropDown"
								 tabIndex.default="-1"
								 tabIndex.orderEdit="-1"
								 tabFocusEnabled="false"
								 width="74"
								 dataProvider="{oneStopSpecialtyList}"
								 selectedItem="{specCode}"
								 changing="specCodeChangingHandler(event)"
								 change="specCodeChangeHandler(event)"
								 creationComplete="dropdownCreationCompleteHandler(specCodeDropDown)"
								 keyFocusChange="keyFocusChangeHandler(event)"
								 visible="{allowEditFlag}" includeInLayout="{allowEditFlag}"/>
		<s:TextInput width="74"
					 text="{specCode}"
					 enabled="false"
					 visible="{!allowEditFlag}" includeInLayout="{!allowEditFlag}"/>

		<s:Label text="PHS Ward" alignmentBaseline="descent"/>
		<fc:AdvancedDropDownList id="wardDropDown"
								 tabIndex.default="-1"
								 tabIndex.orderEdit="-1"
								 tabFocusEnabled="false"
								 width="74"					
								 dataProvider="{oneStopWardList}"
								 selectedItem="{ward}"
								 changing="wardChangingHandler(event)"
								 change="wardChangeHandler(event)"
								 creationComplete="dropdownCreationCompleteHandler(wardDropDown)"
								 keyFocusChange="keyFocusChangeHandler(event)"
								 visible="{allowEditFlag}" includeInLayout="{allowEditFlag}">
			<fc:itemRenderer>
				<fx:Component>
					<s:ItemRenderer height="22">
						<s:layout>
							<s:HorizontalLayout verticalAlign="middle" paddingLeft="3"/>
						</s:layout>
						<s:Label text="{data == '' ? '(Blank)' : data}" width="100%"/>
					</s:ItemRenderer>
				</fx:Component>
			</fc:itemRenderer>
		</fc:AdvancedDropDownList>
		<s:TextInput width="74"
					 text="{ward}"
					 enabled="false"
					 visible="{!allowEditFlag}" includeInLayout="{!allowEditFlag}"/>

		<s:Label text="Patient Cat." alignmentBaseline="descent"/>
		<fc:AdvancedDropDownList id="patCatDropDown"
								 tabIndex.default="-1"
								 tabIndex.orderEdit="-1"
								 tabFocusEnabled="false"
								 width="74"						
								 dataProvider="{oneStopPatientCatList}"
								 selectedItem="{patCatCode}"
								 changing="patCatChangingHandler(event)"
								 change="patCatChangeHandler(event)"
								 creationComplete="dropdownCreationCompleteHandler(patCatDropDown)"
								 keyFocusChange="keyFocusChangeHandler(event)"
								 visible="{allowEditFlag}" includeInLayout="{allowEditFlag}">
			<fc:itemRenderer>
				<fx:Component>
					<s:ItemRenderer height="22">
						<s:layout>
							<s:HorizontalLayout verticalAlign="middle" paddingLeft="3"/>
						</s:layout>
						<s:Label text="{data == '' ? '(Blank)' : data}" width="100%"/>
					</s:ItemRenderer>
				</fx:Component>
			</fc:itemRenderer>
		</fc:AdvancedDropDownList>
		<s:TextInput width="74"
					 text="{patCatCode}"
					 enabled="false"
					 visible="{!allowEditFlag}" includeInLayout="{!allowEditFlag}"/>

		<s:Label text="Rx Type" alignmentBaseline="descent"/>
		<s:TextInput width="74" text="{medOrder.prescType.displayValue}" enabled="false"/>
	
		<s:Button id="currentMedBtn" label="Current Medication"
				  width="120"
				  click="currentMedFunc()"
				  focusIn="buttonFocusInHandler(event)"
				  tabFocusEnabled="false"
				  visible="{showCurrMedButton &amp;&amp; medOrder.prescType == MedOrderPrescType.MpDischarge || medOrder.prescType == MedOrderPrescType.MpHomeLeave}"
				  includeInLayout="{showCurrMedButton &amp;&amp; medOrder.prescType == MedOrderPrescType.MpDischarge || medOrder.prescType == MedOrderPrescType.MpHomeLeave}"/>
	
		<mx:Spacer width="100%"/>

	<s:VGroup gap="0">
		<s:CheckBox label="Pregnancy Contraindication Checking"
					selected="@{orderViewInfo.pregnancyCheckFlag}"
					visible="{orderViewInfo.pregnancyCheckVisible(medOrder.patient.sex, medOrder.docType, propMap)}"
					includeInLayout="{orderViewInfo.pregnancyCheckVisible(medOrder.patient.sex, medOrder.docType, propMap)}"
					tabFocusEnabled="false"
					click="pregnancyFunc()"
					enabled="{allowEditFlag &amp;&amp; orderViewInfo.pregnancyCheckEditableFlag &amp;&amp; screenEnableFlag}"/>
		<s:Group visible="{steroidTag != null}"
				 includeInLayout="{steroidTag != null}"
				 toolTip="{MdsUtils.constructSteroidTagInfo(steroidTag)}">
			 <s:CheckBox label="LTHD Steroid"
						 selected="true"
						 tabFocusEnabled="false"
						 enabled="false"/>
		</s:Group>
	</s:VGroup>

</fc:ExtendedNavigatorContent>
