package hk.org.ha.model.pms.udt.printing;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrintLang implements StringValuedEnum {

	Eng("en", "English", "E"),
	Chi("zh_TW", "Chinese", "C");
	
    private final String dataValue;
    private final String displayValue;
    private final String shortFormValue;

    PrintLang(final String dataValue, final String displayValue, final String shortFormValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
        this.shortFormValue = shortFormValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public String getShortFormValue() {
        return this.shortFormValue;
    }  
    
    
    public static PrintLang dataValueOf(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(PrintLang.class, dataValue);
    }
    
	public static class Converter extends StringValuedEnumConverter<PrintLang> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PrintLang> getEnumClass() {
    		return PrintLang.class;
    	}
    }
}



