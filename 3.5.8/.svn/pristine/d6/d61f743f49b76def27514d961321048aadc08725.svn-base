/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (MaterialRequest.as).
 */

package hk.org.ha.model.pms.persistence.pivas {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.persistence.corp.Hospital;
    import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
    import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class MaterialRequestBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _firstDoseSupplyQty:Number;
        private var _hospCode:String;
        private var _hospital:Hospital;
        private var _id:Number;
        private var _materialRequestItemList:ListCollectionView;
        private var _medProfile:MedProfile;
        private var _medProfileId:Number;
        private var _medProfileMoItem:MedProfileMoItem;
        private var _medProfileMoItemId:Number;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is MaterialRequest) || (property as MaterialRequest).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set firstDoseSupplyQty(value:Number):void {
            _firstDoseSupplyQty = value;
        }
        public function get firstDoseSupplyQty():Number {
            return _firstDoseSupplyQty;
        }

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        public function get hospCode():String {
            return _hospCode;
        }

        public function set hospital(value:Hospital):void {
            _hospital = value;
        }
        public function get hospital():Hospital {
            return _hospital;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set materialRequestItemList(value:ListCollectionView):void {
            _materialRequestItemList = value;
        }
        public function get materialRequestItemList():ListCollectionView {
            return _materialRequestItemList;
        }

        public function set medProfile(value:MedProfile):void {
            _medProfile = value;
        }
        public function get medProfile():MedProfile {
            return _medProfile;
        }

        public function set medProfileId(value:Number):void {
            _medProfileId = value;
        }
        public function get medProfileId():Number {
            return _medProfileId;
        }

        public function set medProfileMoItem(value:MedProfileMoItem):void {
            _medProfileMoItem = value;
        }
        public function get medProfileMoItem():MedProfileMoItem {
            return _medProfileMoItem;
        }

        public function set medProfileMoItemId(value:Number):void {
            _medProfileMoItemId = value;
        }
        public function get medProfileMoItemId():Number {
            return _medProfileMoItemId;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:MaterialRequestBase = MaterialRequestBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._firstDoseSupplyQty, _firstDoseSupplyQty, null, this, 'firstDoseSupplyQty', function setter(o:*):void{_firstDoseSupplyQty = o as Number}, false);
               em.meta_mergeExternal(src._hospCode, _hospCode, null, this, 'hospCode', function setter(o:*):void{_hospCode = o as String}, false);
               em.meta_mergeExternal(src._hospital, _hospital, null, this, 'hospital', function setter(o:*):void{_hospital = o as Hospital}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._materialRequestItemList, _materialRequestItemList, null, this, 'materialRequestItemList', function setter(o:*):void{_materialRequestItemList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._medProfile, _medProfile, null, this, 'medProfile', function setter(o:*):void{_medProfile = o as MedProfile}, false);
               em.meta_mergeExternal(src._medProfileId, _medProfileId, null, this, 'medProfileId', function setter(o:*):void{_medProfileId = o as Number}, false);
               em.meta_mergeExternal(src._medProfileMoItem, _medProfileMoItem, null, this, 'medProfileMoItem', function setter(o:*):void{_medProfileMoItem = o as MedProfileMoItem}, false);
               em.meta_mergeExternal(src._medProfileMoItemId, _medProfileMoItemId, null, this, 'medProfileMoItemId', function setter(o:*):void{_medProfileMoItemId = o as Number}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _firstDoseSupplyQty = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _hospCode = input.readObject() as String;
                _hospital = input.readObject() as Hospital;
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _materialRequestItemList = input.readObject() as ListCollectionView;
                _medProfile = input.readObject() as MedProfile;
                _medProfileId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _medProfileMoItem = input.readObject() as MedProfileMoItem;
                _medProfileMoItemId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_firstDoseSupplyQty is IPropertyHolder) ? IPropertyHolder(_firstDoseSupplyQty).object : _firstDoseSupplyQty);
                output.writeObject((_hospCode is IPropertyHolder) ? IPropertyHolder(_hospCode).object : _hospCode);
                output.writeObject((_hospital is IPropertyHolder) ? IPropertyHolder(_hospital).object : _hospital);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_materialRequestItemList is IPropertyHolder) ? IPropertyHolder(_materialRequestItemList).object : _materialRequestItemList);
                output.writeObject((_medProfile is IPropertyHolder) ? IPropertyHolder(_medProfile).object : _medProfile);
                output.writeObject((_medProfileId is IPropertyHolder) ? IPropertyHolder(_medProfileId).object : _medProfileId);
                output.writeObject((_medProfileMoItem is IPropertyHolder) ? IPropertyHolder(_medProfileMoItem).object : _medProfileMoItem);
                output.writeObject((_medProfileMoItemId is IPropertyHolder) ? IPropertyHolder(_medProfileMoItemId).object : _medProfileMoItemId);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
