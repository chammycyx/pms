<?xml version="1.0" encoding="utf-8"?>
<s:MXDataGridItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
						  xmlns:s="library://ns.adobe.com/flex/spark" 
						  xmlns:mx="library://ns.adobe.com/flex/mx"
						  xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
						  autoDrawBackground="false">		
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.model.pms.udt.DataType;
			import hk.org.ha.model.pms.udt.YesNoFlag;
			import hk.org.ha.model.pms.vo.support.PropMaintUtil;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.collections.ArrayCollection;
			import mx.core.mx_internal;
			
			[Bindable]
			public var ruleValue:Object;
			
			[Bindable]
			public var rulValueChange:Boolean;
			
			[Bindable]
			public var popupFlag:Boolean;
			
			public var sysMsgMap:SysMsgMap;
			
			public var showCheckBoxPopup:Function;
			
			public var setProfileTabEnable:Function;
			
			public var currentScreen:String; /*operationModeMaint, corporatePropMaint, propMaint, workstationPropMaint*/
			
			private var dateTimeRegExp:RegExp;
			
			[Bindable]
			private var booleanTypeList:ArrayCollection= new ArrayCollection(YesNoFlag.constants);
			
			[Bindable]
			public var showRelogonMsg:Boolean = false;
			
			public override function set data(value:Object):void {
				
				super.data = value;
				if (value == null) {
					return;
				}
				
				setDataByType();
			}
			
			private function setDataByType():void {
				if ( data.prop.type == DataType.ListIntegerType || 
					data.prop.type == DataType.ListStringType ) 
				{
					valueLabel.visible = true;
					valueLabel.includeInLayout = true;
					borderContainer.visible = true;
					borderContainer.includeInLayout = true;
					valueLabel.text = data.displayValue;
					valueLabel.setFocus();
				} 
				else if ( data.prop.type == DataType.BooleanType ) 
				{
					valueDdl.visible = true;
					valueDdl.includeInLayout = true;
					valueDdl.dataProvider = booleanTypeList;
					valueDdl.labelField = "display";
					listValueToSelectedIndex(data.displayValue, booleanTypeList);
					valueDdl.setFocus();
					
				} 
				else if ( data.prop.type == DataType.TextAreaType ) 
				{
					var textAreaProp:Object = PropMaintUtil.calculateTextAreaProp(data.prop.validation);
					valueTextArea.lineMaxChars = textAreaProp.maxChar;
					valueTextArea.maxLines = textAreaProp.maxRow;
					valueTextArea.visible = true;
					valueTextArea.includeInLayout = true;
					valueTextArea.height = textAreaProp.height+9;
					valueTextArea.text = data.displayValue;
					valueTextArea.setFocus();
					
				}  
				else if ( data.prop.type == DataType.DateTimeType) 
				{
					var strRegExp:String = setDateTimeRegExp(data.prop.validation);
					dateTimeRegExp = new RegExp(strRegExp);
					valueTxt.maxChars = data.prop.validation.length;
					valueTxt.visible = true;
					valueTxt.includeInLayout = true;
					valueTxt.text = data.displayValue;
					valueTxt.setFocus();
				} 
				else 
				{
					var str:String = data.prop.validation;
					if ( PropMaintUtil.isNumericInput(data.prop.type) ) {
						if ( str.split('-').length > 1 ) {
							valueNumInput.visible = true;
							valueNumInput.includeInLayout = true;
							valueNumInput.text = data.displayValue;
							var format:String = str.split('-')[1];
							valueNumInput.numberFormat = format.length+".0";
							valueNumInput.setFocus();
						} else {
							var array:Array = str.split(',');
							var list:ArrayCollection = new ArrayCollection();
							for each ( var item:String in array ) {
								list.addItem(item);
							}
							valueDdl.dataProvider = list;
							valueDdl.visible = true;
							valueDdl.includeInLayout = true;
							valueDdl.setFocus();
						}
					} else {
						if ( str.split('-').length > 1 &&  str.split(',').length == 1 ) {
							valueTxt.visible = true;
							valueTxt.includeInLayout = true;
							valueTxt.text = data.displayValue;
							if (data.prop.type == DataType.StringType) {
								valueTxt.maxChars = Number(str.split('-')[1]);
							} else {
								valueTxt.restrict= "0-9"
							}
							valueTxt.setFocus();
						} else {
							var array:Array = str.split(',');
							var list:ArrayCollection = new ArrayCollection();
							for each ( var item:String in array ) {
								list.addItem(item);
							}
							valueDdl.dataProvider = list;
							valueDdl.visible = true;
							valueDdl.includeInLayout = true;
							valueDdl.setFocus();
						}
					}
				}
				
				if ( currentScreen == "operationModeMaint" ) {
					setProfileTabEnable(false);
				}
			}
			
			private function updateListData(value:Object):void 
			{
				if ( data.prop.type == DataType.BooleanType ) {
					data.value = value.dataValue;
					data.displayValue = value.displayValue;
				} else if ( data.prop.type == DataType.DateTimeType ) {
					if ( dateTimeRegExp.test(value.toString())) {
						data.errorString = "";
					} else {
						data.errorString = sysMsgMap.getMessage("0034", new Array([data.prop.validation]));
					}
				} else {
					data.displayValue = value;
					data.value = value;
				}
				data.edited = true;
				rulValueChange = true;
				showRelogonMsg = true;
			}
			
			private function listValueToSelectedIndex(value:String, list:ArrayCollection):int 
			{
				if ( data.prop.type == DataType.BooleanType ) {
					return listValueToSelectedIndexInBooleanTypeList(value);
				} else {
					var i:int = 0;
					for each (var listItem:Object in list) {
						if (value == listItem) {
							return i;
						}
						i++;	
					}
					return -1;
				}
			}
			
			private function listValueToSelectedIndexInBooleanTypeList(value:String):int 
			{
				var i:int = 0;
				for each (var item:YesNoFlag in booleanTypeList) {
					if (value == item.displayValue) {
						return i;
					}
					i++;	
				}
				return -1;
			}
			
			private function checkTextAreaLineNum(value:String):void
			{
				var array:Array = data.prop.validation.split(',');
				var maxRow:int = array[0].toString().substring(1);;
				var maxChar:int = array[1].toString().substring(1);
				var errorParm:Array = new Array(maxRow, maxChar);
				data.errorString = "";
				if ( valueTextArea.text.split("\n").length > maxRow ) {
					data.errorString  = sysMsgMap.getMessage("0252", errorParm);
				} else {
					for each ( var str:String in valueTextArea.text.split("\n") ) {
						if ( str.length > maxChar ) {
							data.errorString  = sysMsgMap.getMessage("0252", errorParm);
							break;
						}
					} 
				}
				updateListData(value);
			}
			
			private function setDateTimeRegExp(validation:String):String {
				var strRegExp:String = "^";
				validation = validation.toLocaleLowerCase();
				for ( var i:int=0; i<validation.length; i++ ) {
					var str:String = validation.substr(i, 1);
					
					if ( str == "y" ||
						str == "m" ||
						str == "d" ||
						str == "h" ||
						str == "s" ) {
						strRegExp+="[0-9]";
					} else {
						strRegExp += str;
					}
				}
				strRegExp+="$";														
				return strRegExp;
			}
			
			private function showPropup():void 
			{
				if ( !popupFlag ) {
					return;
				}
				popupFlag = false;
				showCheckBoxPopup(data.prop.validation, data.displayValue);
			}
			
		]]>
	</fx:Script>
	<fc:AdvancedDropDownList id="valueDdl" 
							 selectedIndex="{listValueToSelectedIndex(data.displayValue, valueDdl.dataProvider as ArrayCollection)}"
							 visible="false" includeInLayout="false"
							 width="100%" open="valueDdl.skin['dropDown'].owner=this"  
							 change="updateListData(valueDdl.selectedItem)"
							 errorString="{data.errorString}"/>
	<fc:ExtendedTextInput id="valueTxt" width="100%" 
						  visible="false" includeInLayout="false"
						  errorString="{data.errorString}" 
						  change="updateListData(valueTxt.text)"/>		
	<fc:NumericInput id="valueNumInput"
					 width="100%" textAlign="left"
					 visible="false" includeInLayout="false"
					 errorString="{data.errorString}" 
					 change="updateListData(valueNumInput.text)"/>
	<fc:ExtendedTextArea id="valueTextArea" 
						 editable="true" 
						 width="100%" height="130"
						 visible="false" includeInLayout="false"
						 errorString="{data.errorString}"
						 change="updateListData(valueTextArea.text)"/>
	<s:BorderContainer id="borderContainer" width="100%" height="23" visible="false" includeInLayout="false" 
					   styleName="propListTypeBorderStyle">
		<s:layout>
			<s:VerticalLayout horizontalAlign="left" verticalAlign="middle"/>
		</s:layout>
		<s:Label id="valueLabel" 
				 visible="false" includeInLayout="false" 
				 width="100%" height="100%"
				 click="showPropup()"
				 verticalAlign="middle"
				 paddingLeft="7"
				 styleName="{data.edited?'boldTextStyle':''}"/>
	</s:BorderContainer>
	
	
</s:MXDataGridItemRenderer>
