package hk.org.ha.model.pms.udt.onestop;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum MoeSortOption implements StringValuedEnum {
	
	RefNum("R", "Ref Num"),
	OrderDate("O", "Order Date"),
	Spec("S", "Specialty"),
	Ward("W", "Ward"),
	TicketNum("T", "Ticket Num");
	
    private final String dataValue;
    private final String displayValue;
        
    MoeSortOption(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static MoeSortOption dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(MoeSortOption.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<MoeSortOption> {

		private static final long serialVersionUID = 1L;

		public Class<MoeSortOption> getEnumClass() {
    		return MoeSortOption.class;
    	}
    }
}
