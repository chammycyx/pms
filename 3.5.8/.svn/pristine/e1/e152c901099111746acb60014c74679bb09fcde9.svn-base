<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:message="hk.org.ha.fmk.pms.flex.components.message.*">
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ListCollectionView;
			import mx.containers.TabNavigator;
			import mx.controls.DataGrid;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.DataGridEvent;
			import mx.managers.PopUpManager;
			
			import hk.org.ha.event.pms.main.reftable.cmm.RetrieveCmmSpecialtyListEvent;
			import hk.org.ha.event.pms.main.reftable.cmm.RetrieveSpecialtyCodeListEvent;
			import hk.org.ha.event.pms.main.reftable.cmm.UpdateCmmSpecialtyListEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.core.Toolbar;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.WindowCloseEvent;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.dms.persistence.cmm.CmmSpecialty;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			private static const backgroundColor:uint = 0xE7E5E5; //DataGridColumn non-editable backgroundColor

			[Bindable]
			public var sysMsgMap:SysMsgMap;

			[Bindable]
			public var readonly:Boolean = false;

			[Bindable]
			public var selectAll:Boolean = false;
			
			[Bindable]
			public var cmmSpecialtyList:ListCollectionView;

			[Bindable]
			public var specialtyCodeList:ListCollectionView;

			[Bindable]
			public var isFullAccess:Boolean = false;
			
			public var closeMenuItem:MenuItem;
			
			public var screenActive:Boolean;
			
			public var pmsToolbar:Toolbar;
			
			public var tab:TabNavigator;
			
			public var permissionMap:PropMap;	
			
			public var isModified:Boolean;
			
			[Bindable]
			private var specialtyContextMenu:ContextMenu;
			
			private var shortcutKeySave:Boolean = false;
			
			private var addSpecialtyMenuItem:ContextMenuItem;
			
			public function init():void {
				
				isFullAccess = permissionMap.getValueAsBool("chemoModuleMaintFullAccess", false);

				if (!pmsToolbar.hasInited()) {
					
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.saveYesFunc = saveHandler;
					pmsToolbar.closeFunc = closeHandler;
					
					pmsToolbar.init();
					
					pmsToolbar.saveButton.enabled = isFullAccess;
				}

				if (specialtyCodeList == null || cmmSpecialtyList == null) {
					dispatchEvent(new RetrieveSpecialtyCodeListEvent(function(codeList:ListCollectionView):void {
						specialtyCodeList = codeList;
						retrieveHandler();
					}));
				}
				
				if (addSpecialtyMenuItem == null) {
					addSpecialtyMenuItem = new ContextMenuItem("Add Specialty Code");
					addSpecialtyMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, addSpecialtyMenuHandler);
					addSpecialtyMenuItem.enabled = isFullAccess;
				}
				
				if (specialtyContextMenu == null) {
					specialtyContextMenu = new ContextMenu();
					specialtyContextMenu.hideBuiltInItems();
					specialtyContextMenu.customItems = [addSpecialtyMenuItem];
					specialtyGrid.contextMenu = specialtyContextMenu;
				}
			}
			
			private function saveHandler(callback:Function=null):void {

				if (!validateSpecialtyList()) {
					return;
				}
				
				readonly = true;
				dispatchEvent(new UpdateCmmSpecialtyListEvent(cmmSpecialtyList, function(list:ListCollectionView):void {
					
					cmmSpecialtyList = list;
					readonly = false;
					isModified = false;
					
					showSystemMessage("0446", null, function(evt:Event):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						if (callback != null) {
							callback();
						}
					});

				}));
			}

			private function validateSpecialtyList():Boolean {
				
				var specCodeMap:Dictionary = new Dictionary();
				
				for each (var cmmSpecialty:CmmSpecialty in cmmSpecialtyList) {
					
					if (cmmSpecialty.markDelete) {
						continue;
					}
					
					var specCode:String = cmmSpecialty.specCode;
					if (specCodeMap[specCode]) {
						showSystemMessage("0007", [specCode], function(evt:Event):void{
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							specialtyGrid.editedItemPosition = {
								rowIndex : cmmSpecialtyList.getItemIndex(cmmSpecialty),
								columnIndex : 1
							};
						});
						return false;
					}
					specCodeMap[specCode] = true;
					
					if (specCode == null || specCode == "" || !specialtyCodeList.contains(specCode)) {
						cmmSpecialty.invalidErrorString = sysMsgMap.getMessage("0206");
						specialtyGrid.editedItemPosition = {
							rowIndex : cmmSpecialtyList.getItemIndex(cmmSpecialty),
							columnIndex : 1
						};
						return false;
					}
				}
				
				return true;
			}
			
			private function showSystemMessage(messageCode:String, params:Array=null, oKfunc:Function=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = messageCode;
				msgProp.messageParams = params;
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				if (oKfunc != null) {
					msgProp.okHandler = oKfunc;
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function closeHandler():void {
				dispatchEvent(new WindowSwitchEvent(closeMenuItem, this));
			}
			
			public function windowCloseHandler(evt:WindowCloseEvent=null):void {
				if (isModified) {
					evt.preventDefault();
					showExitConfirmation(closeHandler);
				}
				else {
					cleanUp();
				}
			}
			
			private function cleanUp():void {
				specialtyCodeList = null;
				cmmSpecialtyList = null;
				isModified = false;
			}
			
			private function addSpecialtyMenuHandler(evt:ContextMenuEvent):void {
				addSpecialty();
			}
			
			private function addSpecialty():void {
				specialtyGrid.destroyItemEditor();
				var cmmSpecialty:CmmSpecialty = new CmmSpecialty();
				cmmSpecialtyList.addItem(cmmSpecialty);
				specialtyGrid.editedItemPosition = {rowIndex: cmmSpecialtyList.length -1, columnIndex: 1};
				updateSpecSelectAll();
				isModified = true;
			}
			
			private function retrieveHandler():void {
				
				isModified = false;
				cmmSpecialtyList = null;
				updateSpecSelectAll();
				
				dispatchEvent(new RetrieveCmmSpecialtyListEvent(function(list:ListCollectionView):void {
					cmmSpecialtyList = list;
					updateSpecSelectAll();
				}));
			}
			
			private function specialtyGridItemEditBeginningHandler(evt:DataGridEvent):void {
				if (!screenActive || readonly || shortcutKeySave || !isFullAccess) {
					shortcutKeySave = false;
					evt.preventDefault();
					return;
				}
				if (evt.rowIndex >= 0) {
					var cmmSpecialty:CmmSpecialty = evt.currentTarget.dataProvider.getItemAt(evt.rowIndex) as CmmSpecialty;
					if( cmmSpecialty.markDelete ){
						evt.preventDefault();
					}
				}
			}
			
			private function gridRowColorFunc(cmmSpecialty:CmmSpecialty, rowIndex:int, dataIndex:int, color:uint):uint {
				if (cmmSpecialty.markDelete) {
					return backgroundColor;
				}
				else {
					return color;
				}
			}
			
			private function destroySpecialtyGridEditor(evt:DataGridEvent):void {
				var dataGrid:DataGrid = evt.currentTarget as DataGrid;
				switch (evt.dataField) {
					case "specCode":
						dataGrid.destroyItemEditor();
						break;
				}
			}
			
			private function specialtyGridKeyFocusChangeHandler(evt:FocusEvent):void {
				if (cmmSpecialtyList == null || cmmSpecialtyList.length == 0) {
					return;
				}
				if (specialtyGrid.selectedIndex == -1 || specialtyGrid.editedItemPosition == null) {
					return;
				}
				var currentRowIndex:Number = specialtyGrid.selectedIndex;
				if (evt.shiftKey) {
					focusLastIsEditLine(currentRowIndex-1);
				}
				else {
					focusNextIsEditLine(currentRowIndex+1);
				}
			}
			
			private function focusLastIsEditLine(targetRowIndex:Number):void {
				if (cmmSpecialtyList == null || cmmSpecialtyList.length == 0) {
					return;
				}
				for (var i:Number=targetRowIndex; i>=0; i--) {
					var cmmSpecialty:CmmSpecialty = cmmSpecialtyList.getItemAt(i) as CmmSpecialty;
					if( cmmSpecialty.markDelete ){
						continue;
					}
					specialtyGrid.destroyItemEditor();
					specialtyGrid.editedItemPosition = {rowIndex:i, columnIndex:1};
					return;
				}
				specialtyGrid.destroyItemEditor();
				callLater(tab.setFocus);
			}
			
			private function focusNextIsEditLine(targetRowIndex:Number):void {
				if (cmmSpecialtyList == null || cmmSpecialtyList.length == 0) {
					return;
				}				
				for (var i:Number=targetRowIndex; i<cmmSpecialtyList.length; i++) {
					var cmmSpecialty:CmmSpecialty = cmmSpecialtyList.getItemAt(i) as CmmSpecialty;
					if( cmmSpecialty.markDelete ){
						continue;
					}
					specialtyGrid.destroyItemEditor();
					specialtyGrid.editedItemPosition = {rowIndex:i, columnIndex:1};	
					return;
				}
				specialtyGrid.destroyItemEditor();
				callLater(tab.setFocus);
			}
			
			public function updateSpecSelectAll():void {
				selectAll = true;
				for each (var cmmSpecialty:CmmSpecialty in cmmSpecialtyList) {
					if (cmmSpecialty.markDelete == false) {
						selectAll = false;
						break;
					}
				}
			}

			public function stagekeyUpHandler(evt:KeyboardEvent):void {
				
				if (PopupUtil.isAnyVisiblePopupExists() || readonly || !evt.ctrlKey) {
					return;
				}
				
				switch (evt.charCode) {
					case 55:
						if (isFullAccess) {
							shortcutKeySave = true;
							specialtyGrid.destroyItemEditor();
							dispatchEvent(new ShortcutKeyEvent(pmsToolbar.saveButton));
						}
						return;
					
					case 48:
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
						return;
					
				}
				
				switch (evt.keyCode) {
					case Keyboard.INSERT:
						if (addSpecialtyMenuItem.enabled) {
							addSpecialty();
						}
						return;
				}
			}
			
			public function stagekeyDownHandler(evt:KeyboardEvent):void {
			}
			
			public function showExitConfirmation(callback:Function):void {

				var yesfunc:Function = function(yesEvt:MouseEvent):void {
					PopUpManager.removePopUp(((yesEvt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					saveHandler(callback);
				};

				var noFunc:Function = function (noEvt:MouseEvent):void {
					PopUpManager.removePopUp(((noEvt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					cleanUp();
					callback();
				};

				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = "0542";  
				msgProp.messageWinHeight = 200;
				msgProp.setYesNoButton = true;

				if (yesfunc != null) {
					msgProp.yesHandler = yesfunc;
				}

				if (noFunc != null) {
					msgProp.noHandler = noFunc;
				}

				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}

		]]>
	</fx:Script>
	
	<fx:Binding source="{!readonly}" destination="pmsToolbar.enabled"/>

	<fc:layout>
		<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="0" paddingBottom="10" gap="0"/>
	</fc:layout>
	
	<fc:ExtendedDataGrid id="specialtyGrid" width="250" height="100%" dataProvider="{cmmSpecialtyList}"
						 itemEditBeginning="specialtyGridItemEditBeginningHandler(event)" rowColorFunction="gridRowColorFunc"
						 sortableColumns="false" itemEditEnd="destroySpecialtyGridEditor(event)"
						 editable="{!readonly}" horizontalScrollPolicy="auto" draggableColumns="false"
						 keyFocusChange="specialtyGridKeyFocusChangeHandler(event)">
		<fc:columns>
			<mx:DataGridColumn headerText="Delete" dataField="markDelete" width="70" editable="false" sortable="false" 
							   resizable="false" rendererIsEditor="true" >
				<mx:headerRenderer>
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false">
							<fx:Script>
								<![CDATA[
									import hk.org.ha.model.pms.dms.persistence.cmm.CmmSpecialty;
									
									private function selectAllUpdate():void {
										for each (var cmmSpecialty:CmmSpecialty in outerDocument.cmmSpecialtyList) {
											cmmSpecialty.markDelete = deleteAll.selected;
										}
										outerDocument.selectAll = deleteAll.selected;
										outerDocument.isModified = true;
									}
								]]>
							</fx:Script>
							
							<s:layout>
								<s:HorizontalLayout horizontalAlign="left" paddingLeft="5"/>
							</s:layout>
							<s:CheckBox label="Delete" id="deleteAll" selected="{outerDocument.selectAll}" click="selectAllUpdate()" 
										enabled="{outerDocument.cmmSpecialtyList != null &amp;&amp; outerDocument.cmmSpecialtyList.length > 0 &amp;&amp; !outerDocument.readonly &amp;&amp; outerDocument.isFullAccess}"/>
							
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:headerRenderer>
				<mx:itemRenderer>
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false">
							<fx:Binding source="{deleteCheckbox.selected}" destination="data.markDelete"/>
							<fx:Script>
								<![CDATA[
									import hk.org.ha.model.pms.dms.persistence.cmm.CmmSpecialty;
									
									private function deleteCheckboxHandler():void {
										outerDocument.isModified = true;
										outerDocument.updateSpecSelectAll();
									}
								]]>
							</fx:Script>
							<s:layout>
								<s:HorizontalLayout horizontalAlign="center"/>
							</s:layout>
							<s:VGroup>
								<s:CheckBox id="deleteCheckbox" selected="{(data as CmmSpecialty).markDelete}"
											enabled="{!outerDocument.readonly &amp;&amp; outerDocument.isFullAccess}"
											click="deleteCheckboxHandler()"/>
							</s:VGroup>
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:itemRenderer>
			</mx:DataGridColumn>
			<mx:DataGridColumn headerText="PHS Specialty" dataField="specCode" editorUsesEnterKey="true">
				<mx:itemEditor>
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false">
							<fx:Script>
								<![CDATA[
									import hk.org.ha.model.pms.dms.persistence.cmm.CmmSpecialty;
									import spark.events.IndexChangeEvent;
									
									private function specCbxChangeHandler(evt:IndexChangeEvent):void{
										if (evt.newIndex >= 0) {
											var specCode:String = outerDocument.specialtyCodeList.getItemAt(evt.newIndex) as String;
											specDropDown.selectedItem = specCode;
											outerDocument.isModified = true;
										}
									}
									
									private function setSelectedIndex(specCode:String):void{
										var i:int = 0;
										if (specCode == null || specCode == '') {
											return;
										}
										for each (var code:String in outerDocument.specialtyCodeList) {
											if (specCode == code) {
												specDropDown.selectedIndex = i;
												return;
											}
											i++;	
										}
									}
									
									private function specDropDownFocusOutHandler(evt:FocusEvent):void {
										var cmmSpecialty:CmmSpecialty = data as CmmSpecialty;
										if (specDropDown.textInput.text != '') {
											for each (var code:String in outerDocument.specialtyCodeList) {
												if (specDropDown.textInput.text == code) {
													cmmSpecialty.specCode = code;
													cmmSpecialty.invalidErrorString = '';
													return;
												}
											}
										}
										cmmSpecialty.specCode = specDropDown.textInput.text.toUpperCase();
										cmmSpecialty.invalidErrorString = outerDocument.sysMsgMap.getMessage("0206");
									}
								]]>
							</fx:Script>
							<s:VGroup width="100%" height="100%" clipAndEnableScrolling="true">                           
								<fc:AdvancedComboBox 
									id="specDropDown" 
									maxChars="4"
									open="specDropDown.skin['dropDown'].owner = this"
									dataProvider="{outerDocument.specialtyCodeList}"
									typographicCase="uppercase"
									selectedItem="{(data as CmmSpecialty).specCode==null?'':(data as CmmSpecialty).specCode}"
									width="100%"
									caretChange="specCbxChangeHandler(event)"
									errorString="@{(data as CmmSpecialty).invalidErrorString}" 
									focusOut="specDropDownFocusOutHandler(event)"
									creationComplete="specDropDown.setFocus(), setSelectedIndex((data as CmmSpecialty).specCode)">
								</fc:AdvancedComboBox>
							</s:VGroup>
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:itemEditor>
			</mx:DataGridColumn>
		</fc:columns>
	</fc:ExtendedDataGrid>

	<s:BorderContainer width="250" height="20">
		<s:HGroup width="100%" horizontalAlign="left" paddingLeft="5" paddingTop="5">
			<s:Label text="&lt;Ctrl+Insert&gt; to add record" styleName="footNoteLabelStyle"/>
		</s:HGroup>
	</s:BorderContainer>
	
</fc:ExtendedNavigatorContent>
