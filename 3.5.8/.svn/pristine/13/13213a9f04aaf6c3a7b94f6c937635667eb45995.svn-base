<?xml version="1.0" encoding="utf-8"?>
<s:TitleWindow xmlns:fx="http://ns.adobe.com/mxml/2009"
			   xmlns:s="library://ns.adobe.com/flex/spark" 
			   xmlns:mx="library://ns.adobe.com/flex/mx"
			   xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
			   width="500" height="250"
			   creationComplete="creationCompleteHandler()"
			   windowMoving="titleWindowMovingHandler(event);">
	
	<fx:Metadata>
		[Name("discontinueGroupPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.managers.PopUpManager;
			
			import spark.events.TitleWindowBoundsEvent;
			
			import hk.org.ha.event.pms.main.vetting.mp.popup.ShowDiscontinueGroupPopupEvent;
			import hk.org.ha.fmk.pms.flex.utils.SystemMessageBuilder;
			import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
			import hk.org.ha.model.pms.vo.alert.mds.MdsUtils;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			import hk.org.ha.view.pms.main.vetting.mp.VettingUtils;

			private var cancelCallback:Function;

			private var callback:Function;
			
			private var medProfileItem:MedProfileItem;
			
			private var medProfileItemList:ListCollectionView;
			
			private var purchaseRequestList:ListCollectionView;
			
			private var modifyListFlag:Boolean;

			[In]
			public var inactiveList:ListCollectionView;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[Bindable]
			private var message:String;
			
			private function creationCompleteHandler():void {
				sysMsgTextArea.setStyle("borderVisible", false);
				sysMsgTextArea.setStyle("focusColor", "white");
			}
			
			protected function titleWindowMovingHandler(evt:TitleWindowBoundsEvent):void {
				if (evt.afterBounds.left < 0) {
					evt.afterBounds.left = 0;
				} else if (evt.afterBounds.right > systemManager.stage.stageWidth) {
					evt.afterBounds.left = systemManager.stage.stageWidth - evt.afterBounds.width;
				}
				if (evt.afterBounds.top < 0) {
					evt.afterBounds.top = 0;
				} else if (evt.afterBounds.bottom > systemManager.stage.stageHeight) {
					evt.afterBounds.top = systemManager.stage.stageHeight - evt.afterBounds.height;
				}
			}
			
			private function cancelHandler():void {
				cancelCallback();
				PopUpManager.removePopUp(this);
			}				
			
			private function discontinueOneHandler():void {
				var groupNum:Number = medProfileItem.groupNum;
				medProfileItem.status = MedProfileItemStatus.Discontinued;
				medProfileItem.groupNum = Number.NaN;
				medProfileItem.modified = true;
				medProfileItem.medProfileMoItem.endDate = new Date;
				medProfileItem.medProfileMoItem.mdsSuspendReason = null;
				MdsUtils.removeRelatedDdi(medProfileItem.medProfileMoItem, medProfileItemList);
				if (modifyListFlag && !inactiveList.contains(medProfileItem.medProfileMoItem)) {
					VettingUtils.addToInactiveList(inactiveList, medProfileItem.medProfileMoItem);
				}
				MedProfileUtils.dissociateRemainingMedProfileItem(medProfileItemList, groupNum);
				if(modifyListFlag){
					MedProfileUtils.findAndDeletePurchaseRequestList(purchaseRequestList, medProfileItemList, false);
				}
				callback();				
				PopUpManager.removePopUp(this);
			}
			
			private function discontinueAllHandler():void {
				var groupNum:Number = medProfileItem.groupNum;
				for each (var mpi:MedProfileItem in new ArrayCollection(medProfileItemList.toArray())) {
					if (mpi.groupNum == groupNum) {						
						mpi.status = MedProfileItemStatus.Discontinued;						
						mpi.groupNum = Number.NaN;
						mpi.modified = true;
						mpi.medProfileMoItem.endDate = new Date;
						mpi.medProfileMoItem.mdsSuspendReason = null;
						MdsUtils.removeRelatedDdi(mpi.medProfileMoItem, medProfileItemList);
						if (modifyListFlag && !inactiveList.contains(mpi.medProfileMoItem)) {
							VettingUtils.addToInactiveList(inactiveList, mpi.medProfileMoItem);
						}
						if(modifyListFlag){
							MedProfileUtils.findAndDeletePurchaseRequestList(purchaseRequestList, medProfileItemList, false);
						}
					}
				}
				callback();
				PopUpManager.removePopUp(this);
			}
			
			public function init(evt:ShowDiscontinueGroupPopupEvent):void {
				callback = evt.callback;
				cancelCallback = evt.cancelCallback;
				medProfileItem = evt.medProfileItem;
				medProfileItemList = evt.medProfileItemList;
				purchaseRequestList = evt.purchaseRequestList;
				modifyListFlag = evt.modifyListFlag;
				constructMessage();
				callLater(discontinueOneBtn.setFocus);
				focusManager.showFocus();
			}
			
			private function constructMessage():void {				
				var sysMsg:SystemMessage  = sysMsgMap.getSystemMessage("0721");
				var param:String = "";
				for each (var mpi:MedProfileItem in medProfileItemList) {
					if (mpi == medProfileItem) {
						continue;
					}
					if (isNaN(mpi.groupNum) || mpi.groupNum != medProfileItem.groupNum) {
						continue;
					}
					param += MedProfilePoItem(mpi.medProfileMoItem.medProfilePoItemList[0]).fullDrugDesc(false,false,true) + "\n";
				}
				message = SystemMessageBuilder.createPopupSystemMessage(sysMsg, [MedProfilePoItem(medProfileItem.medProfileMoItem.medProfilePoItemList[0]).fullDrugDesc(false,false,true), "<font size='16'><b>" + param + "</b></font>"]);
			}
		]]>
	</fx:Script>
	
	<s:VGroup left="10" right="10" bottom="10" top="10" width="100%" height="100%">
		<s:HGroup width="100%" height="100%" gap="10">
			<mx:Image source="@Embed(source='/assets/msg_alert.png')"/>
			<s:VGroup width="85%" height="100%" gap="10" paddingTop="5">
				<mx:TextArea id="sysMsgTextArea" tabFocusEnabled="false" width="100%" height="100%" editable="false" htmlText="{message}"/>
			</s:VGroup>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="right">
			<fc:ExtendedButton label="Discontinue A only" id="discontinueOneBtn" click="discontinueOneHandler()"/>
			<fc:ExtendedButton label="Discontinue All"    id="discontinueAllBtn" click="discontinueAllHandler()"/>
			<fc:ExtendedButton label="Cancel"             id="cancelBtn"         click="cancelHandler()"/>
		</s:HGroup>
	</s:VGroup>
</s:TitleWindow>
