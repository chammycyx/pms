package hk.org.ha.model.pms.vo.patient;

import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
import hk.org.ha.model.pms.udt.reftable.HomeLeaveMappingType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type = DefaultExternalizer.class)
public class PasSpecialty implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pasHospCode;
	
	private String pasSpecCode;
	
	private String pasDescription;
	
	private PasSpecialtyType type;
	
	private List<PasSubSpecialty> pasSubSpecialtyList;
	
	private HomeLeaveMappingType defaultType = HomeLeaveMappingType.Discharge;
	
	private Boolean allowPrescribe;

	public PasSpecialty() {
	}

	public String getPasHospCode() {
		return pasHospCode;
	}

	public void setPasHospCode(String pasHospCode) {
		this.pasHospCode = pasHospCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasDescription() {
		return pasDescription;
	}

	public void setPasDescription(String pasDescription) {
		this.pasDescription = pasDescription;
	}

	public PasSpecialtyType getType() {
		return type;
	}

	public void setType(PasSpecialtyType type) {
		this.type = type;
	}

	public List<PasSubSpecialty> getPasSubSpecialtyList() {
		return pasSubSpecialtyList;
	}

	public void setPasSubSpecialtyList(List<PasSubSpecialty> pasSubSpecialtyList) {
		if (pasSubSpecialtyList == null) {
			pasSubSpecialtyList = new ArrayList<PasSubSpecialty>();
		}
		this.pasSubSpecialtyList = pasSubSpecialtyList;
	}
	
	public void setDefaultType(HomeLeaveMappingType defaultType) {
		this.defaultType = defaultType;
	}

	public HomeLeaveMappingType getDefaultType() {
		return defaultType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pasHospCode == null) ? 0 : pasHospCode.hashCode());
		result = prime * result
				+ ((pasSpecCode == null) ? 0 : pasSpecCode.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PasSpecialty other = (PasSpecialty) obj;
		if (pasHospCode == null) {
			if (other.pasHospCode != null)
				return false;
		} else if (!pasHospCode.equals(other.pasHospCode))
			return false;
		if (pasSpecCode == null) {
			if (other.pasSpecCode != null)
				return false;
		} else if (!pasSpecCode.equals(other.pasSpecCode))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public void setAllowPrescribe(Boolean allowPrescribe) {
		this.allowPrescribe = allowPrescribe;
	}

	public Boolean getAllowPrescribe() {
		return allowPrescribe;
	}

	@Override
	public String toString() {
		return "PasSpecialty [allowPrescribe=" + allowPrescribe
				+ ", defaultType=" + defaultType + ", pasHospCode="
				+ pasHospCode + ", pasSpecCode=" + pasSpecCode
				+ ", pasSubSpecialtyList=" + pasSubSpecialtyList + ", type="
				+ type + "]";
	}
}

