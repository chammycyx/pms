<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
				xmlns:s="library://ns.adobe.com/flex/spark" 
				xmlns:mx="library://ns.adobe.com/flex/mx"
				xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
				xmlns:i="hk.org.ha.view.pms.main.vetting.mp.indicator.*"
				autoDrawBackground="true">
	<fx:Script>
		<![CDATA[
			import mx.collections.ListCollectionView;
			
			import hk.org.ha.event.pms.main.alert.mds.RetrieveMedProfileAlertMsgEvent;
			import hk.org.ha.event.pms.main.alert.mds.popup.ShowMedProfileAlertMsgPopupEvent;
			import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveFmIndicationListByMedProfileMoItemEvent;
			import hk.org.ha.event.pms.main.vetting.mp.popup.ShowOrderAmendmentHistoryPopupEvent;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItemAlert;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
			import hk.org.ha.model.pms.udt.alert.mds.AlertType;
			import hk.org.ha.model.pms.udt.medprofile.EndType;
			import hk.org.ha.model.pms.udt.vetting.ActionStatus;
			import hk.org.ha.view.pms.main.medprofile.MedProfileConstants;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			
			private var _backgroundColor:int;
			
			private var i:int;
			
			[Bindable]
			public var medProfileMoItem:MedProfileMoItem;
			
			[Bindable]
			public var medProfilePoItem:MedProfilePoItem;
			
			[Bindable]
			public function set backgroundColor(n:int):void {
				_backgroundColor = n;
				iconContainer.setStyle("backgroundColor", n);
			}
			
			public function get backgroundColor():int {
				return _backgroundColor;
			}
			
			public function processMdsAlert():void {
				dispatchEvent(new RetrieveMedProfileAlertMsgEvent(medProfileMoItem, new ShowMedProfileAlertMsgPopupEvent()));
			}
			
			public function processFmIndication():void {
				dispatchEvent(new RetrieveFmIndicationListByMedProfileMoItemEvent(medProfileMoItem));
			}
			
			public function areAllWardStocks(medProfilePoItemList:ListCollectionView):Boolean {
				return areWardStocks(medProfilePoItemList, false);
			}
			
			public function arePartialWardStocks(medProfilePoItemList:ListCollectionView):Boolean {
				return areWardStocks(medProfilePoItemList, true);
			}
			
			public function areWardStocks(medProfilePoItemList:ListCollectionView, partial:Boolean):Boolean {
				
				var wardStockCount:Number = 0;
				var normalCount:Number = 0;

				for each (var medProfilePoItem:MedProfilePoItem in medProfilePoItemList) {
					if (medProfilePoItem.wardStockFlag) {
						wardStockCount++;
					} else {
						normalCount++;
					}
				}
				
				return wardStockCount > 0 && (partial ? normalCount > 0 : normalCount == 0);
			}
			
			public function hasRedispenseItem(medProfilePoItemList:ListCollectionView):Boolean {
				
				for each (var medProfilePoItem:MedProfilePoItem in medProfilePoItemList) {
					if (medProfilePoItem.reDispFlag) {
						return true;
					}
				}
				return false;
			}
			
			public function areHlaAlert(medProfileMoItemAlertList:ListCollectionView):Boolean {
				for each (var medProfileMoItemAlert:MedProfileMoItemAlert in medProfileMoItemAlertList) {
					if (medProfileMoItemAlert.alertType == AlertType.IpHla) {
						return true;
					}
				}
				return false;
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="{(data is MedProfileMoItem ? data : data.medProfileMoItem) as MedProfileMoItem}"
				destination="medProfileMoItem"/>
	
	<fx:Binding source="{medProfileMoItem.medProfilePoItemList.getItemAt(0) as MedProfilePoItem}"
				destination="medProfilePoItem"/>
	
	<fx:Binding source="{currentState == 'selected' ? getStyle('selectionColor') : (data.hasOwnProperty('modified') &amp;&amp; data.modified ? 0xFFFFFF : 0xE7E5E5)}"
				destination="backgroundColor"/>

	<mp:states>
		<s:State name="normal"/>
		<s:State name="selected"/>
	</mp:states>
	
	<mp:layout>
		<s:VerticalLayout gap="0"/>
	</mp:layout>
	<s:BorderContainer styleName="mpVettingManualProfileHeaderStyle" width="100%" height="20" visible="{medProfileMoItem.firstManualItem}" includeInLayout="{medProfileMoItem.firstManualItem}"> 
		<s:layout>
			<s:HorizontalLayout horizontalAlign="left" gap="0" paddingLeft="5" paddingTop="5" paddingRight="5"/>
		</s:layout>
		<s:Label text="Manual Profile" width="100%"/>
	</s:BorderContainer>
	<s:RichText id="drugDescText" width="100%" paddingLeft="5" paddingRight="5" paddingTop="5" paddingBottom="0"
				backgroundColor="{backgroundColor}"
				textFlow="{medProfilePoItem.fullDrugDescTlf(false)}"
				mouseMove="MedProfileUtils.toggleTooltip(event, drugDescText.textFlow)" mouseOut="MedProfileUtils.destroyTooltip()"
				click="MedProfileUtils.processDrugOrderMouseClick(event, drugDescText.textFlow, processMdsAlert, processFmIndication)"/>
	<s:RichText id="druginstructionText" width="100%" paddingLeft="5" paddingRight="5" paddingTop="5" paddingBottom="0"
				backgroundColor="{backgroundColor}"
				textFlow="{medProfilePoItem.regimen.getManualEntryDrugInstructionTlf(false)}"
				mouseMove="MedProfileUtils.toggleTooltip(event, druginstructionText.textFlow)" mouseOut="MedProfileUtils.destroyTooltip()"
				click="MedProfileUtils.processDrugOrderMouseClick(event, druginstructionText.textFlow, processMdsAlert, processFmIndication)"/>
	
	<s:SkinnableContainer id="iconContainer" width="100%">
		<s:layout>
			<s:VerticalLayout paddingLeft="5" paddingRight="5" paddingTop="0" paddingBottom="5" gap="2"/>
		</s:layout>
		
		<s:HGroup gap="2">
			<s:Label paddingTop="2" paddingLeft="2" paddingBottom="0" paddingRight="2" 
					 styleName="{data is MedProfileItem ? 'modifiedOrderLabelStyle' : 'inactiveModifiedOrderLabelStyle'}"
					 visible="{medProfileMoItem.medProfileItem.amendFlag &amp;&amp; !medProfileMoItem.firstItemFlag}"
					 includeInLayout="{medProfileMoItem.medProfileItem.amendFlag &amp;&amp; !medProfileMoItem.firstItemFlag}"
					 toolTip="{MedProfileConstants.TOOLTIP_MODIFIED_ORDER}" text="Modified"
					 click="if (data is MedProfileItem) dispatchEvent(new ShowOrderAmendmentHistoryPopupEvent(medProfileMoItem))"/>
			<s:Group visible="{medProfileMoItem.alertFlag}" includeInLayout="{medProfileMoItem.alertFlag}"
					 toolTip="{MedProfileConstants.TOOLTIP_MDS_ALERT}" click="dispatchEvent(new RetrieveMedProfileAlertMsgEvent(medProfileMoItem, new ShowMedProfileAlertMsgPopupEvent()))">
				<s:BitmapImage source="@Embed(source='/assets/mds_alert_12.png')"/>
			</s:Group>
			<s:Group visible="{medProfileMoItem.fmFlag}" includeInLayout="{medProfileMoItem.fmFlag}"
					 toolTip="{MedProfileConstants.TOOLTIP_FM_INDICATION}" mouseEnabled="true" click="dispatchEvent(new RetrieveFmIndicationListByMedProfileMoItemEvent(medProfileMoItem))">
				<s:BitmapImage source="@Embed(source='/assets/fm.png')"/>
			</s:Group>
			<s:Group visible="{medProfileMoItem.urgentFlag &amp;&amp; MedProfileUtils.isToday(medProfileMoItem.firstPrintDate, true)}"
					 includeInLayout="{medProfileMoItem.urgentFlag &amp;&amp; MedProfileUtils.isToday(medProfileMoItem.firstPrintDate, true)}"
					 toolTip="Urgent">
				<s:BitmapImage source="@Embed(source='/assets/urgent.png')"/>
			</s:Group>
			<s:Group visible="{data is MedProfileMoItem &amp;&amp; medProfileMoItem.pendingCode == null &amp;&amp; medProfileMoItem.suspendCode != null}"
					 includeInLayout="{data is MedProfileMoItem &amp;&amp; medProfileMoItem.pendingCode == null &amp;&amp; medProfileMoItem.suspendCode != null}"
					 toolTip="Suspended" mouseEnabled="true">
				<s:BitmapImage source="@Embed(source='/assets/ip_suspend.png')"/>
			</s:Group>
			<s:Group visible="{medProfileMoItem.mdsSuspendReason != null}"
					 includeInLayout="{medProfileMoItem.mdsSuspendReason != null}"
					 toolTip="{MedProfileConstants.TOOLTIP_MDS_EXCEPTION}">
				<s:BitmapImage source="@Embed(source='/assets/mds_exception.png')"/>
			</s:Group>
			<s:Group visible="{data is MedProfileMoItem &amp;&amp; medProfileMoItem.unitDoseExceptFlag}"
					 includeInLayout="{data is MedProfileMoItem &amp;&amp; medProfileMoItem.unitDoseExceptFlag}"
					 toolTip="{MedProfileConstants.TOOLTIP_UNIT_DOSE_EXCEPTION}">
				<s:BitmapImage source="@Embed(source='/assets/unit_dose_exception.png')"/>
			</s:Group>
			<s:Group visible="{medProfileMoItem.medProfileItem.isItemSuspend}"
					 includeInLayout="{medProfileMoItem.medProfileItem.isItemSuspend}"
					 toolTip="{MedProfileConstants.TOOLTIP_ITEM_SUSPENDED}">
				<s:BitmapImage source="@Embed(source='/assets/item_suspended.png')"/>
			</s:Group>
			
			<s:Group visible="{!medProfilePoItem.dangerDrugFlag &amp;&amp; medProfilePoItem.wardStockFlag}"
					 includeInLayout="{!medProfilePoItem.dangerDrugFlag &amp;&amp; medProfilePoItem.wardStockFlag}"
					 toolTip="{MedProfileConstants.TOOLTIP_WARD_STOCK}">
				<s:BitmapImage source="@Embed(source='/assets/ward_stock.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.dangerDrugFlag}" includeInLayout="{medProfilePoItem.dangerDrugFlag}"
					 toolTip="{MedProfileConstants.TOOLTIP_DANGER_DRUG}">
				<s:BitmapImage source="@Embed(source='/assets/danger_drug_12.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.reDispFlag}" includeInLayout="{medProfilePoItem.reDispFlag}" toolTip="{MedProfileConstants.TOOLTIP_REDISPENSE}">
				<s:BitmapImage source="@Embed(source='/assets/redisp.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.singleDispFlag}" includeInLayout="{medProfilePoItem.singleDispFlag}" toolTip="Single Dispense">
				<s:BitmapImage source="@Embed(source='/assets/single_use_ind.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.unitDoseFlag}" includeInLayout="{medProfilePoItem.unitDoseFlag}" toolTip="Unit Dose">
				<s:BitmapImage source="@Embed(source='/assets/unit_dose.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.directPrintFlag}" includeInLayout="{medProfilePoItem.directPrintFlag}" toolTip="Direct Label Print">
				<s:BitmapImage source="@Embed(source='/assets/direct_label_print.png')"/>
			</s:Group>
			<s:Group visible="{medProfileMoItem.pivasFlag}" includeInLayout="{medProfileMoItem.pivasFlag}" toolTip="PIVAS">
				<s:BitmapImage source="@Embed(source='/assets/pivas.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.actionStatus == ActionStatus.PurchaseByPatient}"
					 includeInLayout="{medProfilePoItem.actionStatus == ActionStatus.PurchaseByPatient}"
					 toolTip="{medProfilePoItem.actionStatus.displayValue}">
				<s:BitmapImage source="@Embed(source='/assets/purchase_by_pat.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.actionStatus == ActionStatus.SafetyNet}" 
					 includeInLayout="{medProfilePoItem.actionStatus == ActionStatus.SafetyNet}"
					 toolTip="{medProfilePoItem.actionStatus.displayValue}">
				<s:BitmapImage source="@Embed(source='/assets/purchase_by_pat_safety_net.png')"/>
			</s:Group>
			<s:Group visible="{medProfilePoItem.actionStatus == ActionStatus.ContinueWithOwnStock}"
					 includeInLayout="{medProfilePoItem.actionStatus == ActionStatus.ContinueWithOwnStock}"
					 toolTip="{MedProfileConstants.TOOLTIP_CONTINUE_WITH_OWN_STOCK}">
				<s:BitmapImage source="@Embed(source='/assets/own_stock.png')"/>
			</s:Group>
		</s:HGroup>
		
		<s:Label styleName="wardTransferStyle" 
				 visible="{medProfileMoItem.medProfileItem.medProfileMoItem == medProfileMoItem &amp;&amp; medProfileMoItem.transferFlag == true}"
				 includeInLayout="{medProfileMoItem.medProfileItem.medProfileMoItem == medProfileMoItem &amp;&amp; medProfileMoItem.transferFlag == true}"
				 text="Transfer{medProfileMoItem.transferDate == null ? '' : ' on: ' + MedProfileUtils.formatDateTime(medProfileMoItem.transferDate, false)}"/>

		<s:Label styleName="wardTransferStyle" 
				 visible="{data is MedProfileMoItem &amp;&amp; medProfileMoItem.medProfileItem.endType == EndType.WardTransfered}"
				 includeInLayout="{data is MedProfileMoItem &amp;&amp; medProfileMoItem.medProfileItem.endType == EndType.WardTransfered}"
				 text="Ward Transfer"/>
	</s:SkinnableContainer>
	
	<mx:HRule styleName="vettingMpRuleStyle" width="100%"/>
		
</mp:ItemRenderer>
