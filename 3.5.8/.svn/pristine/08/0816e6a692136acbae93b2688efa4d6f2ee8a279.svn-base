<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="WorkloadStatRpt" language="groovy" pageWidth="595" pageHeight="842" columnWidth="565" leftMargin="15" rightMargin="15" topMargin="15" bottomMargin="15">
	<property name="ireport.zoom" value="1.8181818181818306"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Row">
		<box>
			<pen lineWidth="0.0"/>
			<topPen lineWidth="0.0"/>
			<leftPen lineWidth="0.0"/>
			<bottomPen lineWidth="0.0"/>
			<rightPen lineWidth="0.0"/>
		</box>
		<conditionalStyle>
			<conditionExpression><![CDATA[$V{REPORT_COUNT}%2==0]]></conditionExpression>
			<style mode="Opaque" backcolor="#DEDEDE" fontName="Arial" fontSize="8"/>
		</conditionalStyle>
	</style>
	<style name="Border" mode="Transparent" pattern="">
		<pen lineWidth="0.5"/>
		<box>
			<pen lineWidth="0.75"/>
			<topPen lineWidth="0.75"/>
			<leftPen lineWidth="0.75"/>
			<bottomPen lineWidth="0.75"/>
			<rightPen lineWidth="0.75"/>
		</box>
	</style>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<parameter name="activeProfileName" class="java.lang.String"/>
	<field name="dayEndBatchDate" class="java.util.Date"/>
	<field name="opOrderCount" class="java.lang.Integer"/>
	<field name="opOrderItemCount" class="java.lang.Integer"/>
	<field name="opRefillCount" class="java.lang.Integer"/>
	<field name="opRefillItemCount" class="java.lang.Integer"/>
	<field name="mpItemCount" class="java.lang.Integer"/>
	<field name="mpSfiItemCount" class="java.lang.Integer"/>
	<field name="mpSafetyNetItemCount" class="java.lang.Integer"/>
	<field name="mpWardStockItemCount" class="java.lang.Integer"/>
	<field name="mpDangerDrugItemCount" class="java.lang.Integer"/>
	<variable name="sumOpOrderCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{opOrderCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumOpOrderItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{opOrderItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumOpRefillCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{opRefillCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumOpRefillItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{opRefillItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumMpItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{mpItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumMpSfiItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{mpSfiItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumMpSafetyNetItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{mpSafetyNetItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumMpWardStockItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{mpWardStockItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="sumMpDangerDrugItemCount" class="java.lang.Integer" calculation="Sum">
		<variableExpression><![CDATA[$F{mpDangerDrugItemCount}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<variable name="totalOpItem" class="java.lang.Integer">
		<variableExpression><![CDATA[$V{sumOpOrderItemCount}+$V{sumOpRefillItemCount}]]></variableExpression>
	</variable>
	<variable name="totalMpItem" class="java.lang.Integer">
		<variableExpression><![CDATA[$V{sumMpItemCount}+$V{sumMpSfiItemCount}+$V{sumMpSafetyNetItemCount}]]></variableExpression>
	</variable>
	<variable name="totalWardStockDangerDrugItem" class="java.lang.Integer">
		<variableExpression><![CDATA[$V{sumMpWardStockItemCount}+$V{sumMpDangerDrugItemCount}]]></variableExpression>
	</variable>
	<group name="Summary">
		<groupFooter>
			<band height="121">
				<line>
					<reportElement x="0" y="0" width="565" height="1"/>
				</line>
				<textField isBlankWhenNull="true">
					<reportElement x="100" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumOpOrderCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="150" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumOpOrderItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="210" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumOpRefillCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="260" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumOpRefillItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="320" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumMpItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="370" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumMpSfiItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="420" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumMpSafetyNetItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="470" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumMpWardStockItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="520" y="1" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{sumMpDangerDrugItemCount}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="37" width="210" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[Total Out-Patient Item (Including Refill):]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="210" y="37" width="60" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{totalOpItem}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="49" width="210" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[Total In-Patient Item (Excluding Ward Stock & DD):]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="210" y="49" width="60" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{totalMpItem}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="61" width="210" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[Total (Ward Stock & DD)]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="210" y="61" width="60" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{totalWardStockDangerDrugItem}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="109" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[* Remark: ]]></text>
				</staticText>
				<staticText>
					<reportElement x="40" y="109" width="400" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[The workload count for the ward stock and dangerous drugs included new orders only]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="40" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="466" height="20"/>
				<textElement>
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Workload Inquiry  (" + $P{hospCode} +" - "+ $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="388" y="0" width="177" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Operation Mode: " + $P{activeProfileName}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="49" splitType="Stretch">
			<staticText>
				<reportElement style="Border" mode="Transparent" x="100" y="2" width="90" height="24"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[ Out-Patient
(Excluding Refill)]]></text>
			</staticText>
			<staticText>
				<reportElement style="Border" mode="Transparent" x="210" y="2" width="90" height="24"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="9" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[ Out-Patient
Refill]]></text>
			</staticText>
			<staticText>
				<reportElement style="Border" x="320" y="2" width="240" height="24"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[In-Patient]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="28" width="90" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Transaction Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Rx]]></text>
			</staticText>
			<staticText>
				<reportElement x="150" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Item No.]]></text>
			</staticText>
			<staticText>
				<reportElement x="210" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Coupon]]></text>
			</staticText>
			<staticText>
				<reportElement x="260" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Item No.]]></text>
			</staticText>
			<staticText>
				<reportElement x="320" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[General]]></text>
			</staticText>
			<staticText>
				<reportElement x="370" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[SFI]]></text>
			</staticText>
			<staticText>
				<reportElement x="420" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[SN]]></text>
			</staticText>
			<staticText>
				<reportElement x="470" y="29" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Ward Stock*]]></text>
			</staticText>
			<staticText>
				<reportElement x="520" y="28" width="40" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[DD*]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="48" width="565" height="1"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="12" splitType="Stretch">
			<frame>
				<reportElement style="Row" mode="Opaque" x="0" y="0" width="565" height="12"/>
				<textField pattern="dd-MMM-yyyy" isBlankWhenNull="true">
					<reportElement x="0" y="0" width="90" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.util.Date"><![CDATA[$F{dayEndBatchDate}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="100" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{opOrderCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="150" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{opOrderItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="210" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{opRefillCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="260" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{opRefillItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="320" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{mpItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="370" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{mpSfiItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="420" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{mpSafetyNetItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="470" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{mpWardStockItemCount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="520" y="0" width="40" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$F{mpDangerDrugItemCount}]]></textFieldExpression>
				</textField>
			</frame>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="15" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="425" y="0" width="132" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="539" y="0" width="28" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="&apos;Printed on&apos; dd-MMM-yyyy &apos;at&apos; HH:mm ">
				<reportElement x="0" y="0" width="220" height="15"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
