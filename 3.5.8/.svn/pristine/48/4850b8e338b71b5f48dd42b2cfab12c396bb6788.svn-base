package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderStatus;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;


@Entity
@Table(name = "MED_PROFILE_ORDER")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MedProfileOrder")
public class MedProfileOrder extends MedProfileOrderEntity {
	
	private static final long serialVersionUID = 1L;
	
	public static final List<Integer> ADMIN_STATUS_GIVEN_LIST = Arrays.asList(9999, 0, 2, 3, 4, 5, 6, 8, 9, 10, 12, 13);
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medProfileOrderSeq")
	@SequenceGenerator(name = "medProfileOrderSeq", sequenceName = "SQ_MED_PROFILE_ORDER", allocationSize = 1,
			initialValue = 100000000)
	@XmlElement
	private Long id;

	@Column(name = "ORDER_NUM", nullable = false, length = 36)
	@XmlElement
	private String orderNum;
	
	@Column(name = "ORDER_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@XmlElement
	private Date orderDate;

	@Column(name = "CMS_CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    @XmlTransient
	private Date cmsCreateDate;
	
	@Column(name = "SEQ_NUM")
	@XmlElement
	private Integer seqNum;

    @Converter(name = "MedProfileOrder.type", converterClass = MedProfileOrderType.Converter.class)
    @Convert("MedProfileOrder.type")
	@Column(name = "TYPE", nullable = false, length = 2)
	private MedProfileOrderType type;
	
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	@XmlElement
	private String patHospCode;
	
	@Column(name = "CASE_NUM", length = 12)
	@XmlElement
	private String caseNum;
	
	@Column(name = "PAT_KEY", length = 8)
	@XmlElement
	private String patKey;
	
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ADMIN_DATE")
    @XmlTransient
	private Date adminDate;

    @Converter(name = "MedProfileOrder.status", converterClass = MedProfileOrderStatus.Converter.class)
    @Convert("MedProfileOrder.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	@XmlElement
	private MedProfileOrderStatus status;

	@Column(name = "DISCONTINUE_REASON", length = 255)
    @XmlTransient
	private String discontinueReason;

	@Column(name = "DISCONTINUE_HOSP_CODE", length = 3)
    @XmlTransient
	private String discontinueHospCode;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DISCONTINUE_DATE")
    @XmlTransient
	private Date discontinueDate;
	
	@Column(name = "ALLOW_REPEAT_FLAG", nullable = false)
    @XmlTransient
	private Boolean allowRepeatFlag;
	
    @Column(name = "PRIVATE_FLAG")
	@XmlElement
    private Boolean privateFlag;	
	
	@Column(name = "PREGNANCY_CHECK_FLAG")
    @XmlTransient
	private Boolean pregnancyCheckFlag;
	
	@Column(name = "PAS_SPEC_CODE", length = 4)
	@XmlElement
	private String pasSpecCode;
	
	@Column(name = "PAS_SUB_SPEC_CODE", length = 4)
	@XmlElement
	private String pasSubSpecCode;
	
	@Column(name = "PAS_BED_NUM", length = 5)
	@XmlElement
	private String pasBedNum;
	
	@Column(name = "PAS_WARD_CODE", length = 4)
	@XmlElement
	private String pasWardCode;

	@Column(name = "PREV_PAS_WARD_CODE", length = 4)
    @XmlTransient
	private String prevPasWardCode;
	
	@Column(name = "ORDER_DIGEST", length = 100)
    @XmlTransient
	private String orderDigest;

    @Column(name = "MR_ANNOTATION", length = 255)
    @XmlTransient
    private String mrAnnotation;

	@Column(name = "CMS_ADMIN_STATUS")
    @XmlTransient
	private Integer cmsAdminStatus;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REVIEW_DATE")
    @XmlTransient
    private Date reviewDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DISCHARGE_DATE")
    @XmlTransient
    private Date dischargeDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TRANSFER_DATE")
    @XmlTransient
    private Date transferDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CANCEL_DISCHARGE_DATE")
    @XmlTransient
    private Date cancelDischargeDate;

	@Column(name = "ITEM_NUM")
	@XmlElement
	private Integer itemNum;
    
	@Column(name = "ORG_ITEM_NUM")
	@XmlElement
	private Integer orgItemNum;

    @Column(name = "PROTOCOL_CODE", length = 20)
	@XmlElement
    private String protocolCode;
    
    @Column(name = "PROTOCOL_VERSION", length = 20)
	@XmlElement
    private Integer protocolVersion;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE",      referencedColumnName = "HOSP_CODE"      ),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE" )
	})
	@XmlElement
	private Workstore workstore;
	
	@Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
    @XmlTransient
	private String hospCode;	
    
	@OrderBy("itemNum")
    @OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "MED_PROFILE_ORDER_ID")
	@XmlElement(name="MedProfileMoItem")
    private List<MedProfileMoItem> medProfileMoItemList;
    
    @OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "MED_PROFILE_ORDER_ID")
    @XmlTransient
    private List<Replenishment> replenishmentList;
    	
	@Transient
	@XmlElement
	private String typeString;
    
    public MedProfileOrder() {
    	super();
    	status = MedProfileOrderStatus.Pending;
    	allowRepeatFlag = Boolean.FALSE;
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getCmsCreateDate() {
		return cmsCreateDate;
	}

	public void setCmsCreateDate(Date cmsCreateDate) {
		this.cmsCreateDate = cmsCreateDate;
	}

	public Integer getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

	public MedProfileOrderType getType() {
		return type;
	}

	public void setType(MedProfileOrderType type) {
		this.type = type;
	}
	
	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}
	
	public Date getAdminDate() {
		return adminDate;
	}

	public void setAdminDate(Date adminDate) {
		this.adminDate = adminDate;
	}
	
	public MedProfileOrderStatus getStatus() {
		return status;
	}	

	public void setStatus(MedProfileOrderStatus status) {
		this.status = status;
	}
	
	public String getDiscontinueReason() {
		return discontinueReason;
	}

	public void setDiscontinueReason(String discontinueReason) {
		this.discontinueReason = discontinueReason;
	}

	public String getDiscontinueHospCode() {
		return discontinueHospCode;
	}

	public void setDiscontinueHospCode(String discontinueHospCode) {
		this.discontinueHospCode = discontinueHospCode;
	}

	public Date getDiscontinueDate() {
		return discontinueDate;
	}

	public void setDiscontinueDate(Date discontinueDate) {
		this.discontinueDate = discontinueDate;
	}

	public Boolean getAllowRepeatFlag() {
		return allowRepeatFlag;
	}

	public void setAllowRepeatFlag(Boolean allowRepeatFlag) {
		this.allowRepeatFlag = allowRepeatFlag;
	}

	public Boolean getPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(Boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public Boolean getPregnancyCheckFlag() {
		return pregnancyCheckFlag;
	}

	public void setPregnancyCheckFlag(Boolean pregnancyCheckFlag) {
		this.pregnancyCheckFlag = pregnancyCheckFlag;
	}
	
	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasSubSpecCode() {
		return pasSubSpecCode;
	}

	public void setPasSubSpecCode(String pasSubSpecCode) {
		this.pasSubSpecCode = pasSubSpecCode;
	}
	
	public String getPasBedNum() {
		return pasBedNum;
	}

	public void setPasBedNum(String pasBedNum) {
		this.pasBedNum = pasBedNum;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public void setPrevPasWardCode(String prevPasWardCode) {
		this.prevPasWardCode = prevPasWardCode;
	}

	public String getPrevPasWardCode() {
		return prevPasWardCode;
	}

	public String getOrderDigest() {
		return orderDigest;
	}

	public void setOrderDigest(String orderDigest) {
		this.orderDigest = orderDigest;
	}

	public String getMrAnnotation() {
		return mrAnnotation;
	}

	public void setMrAnnotation(String mrAnnotation) {
		this.mrAnnotation = mrAnnotation;
	}

	public Integer getCmsAdminStatus() {
		return cmsAdminStatus;
	}

	public void setCmsAdminStatus(Integer cmsAdminStatus) {
		this.cmsAdminStatus = cmsAdminStatus;
	}

	public Date getReviewDate() {
		return (reviewDate == null)? null : new Date(reviewDate.getTime());
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = (reviewDate == null) ? null : new Date(reviewDate.getTime());
	}
	
	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public Date getCancelDischargeDate() {
		return cancelDischargeDate;
	}

	public void setCancelDischargeDate(Date cancelDischargeDate) {
		this.cancelDischargeDate = cancelDischargeDate;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public Integer getOrgItemNum() {
		return orgItemNum;
	}

	public void setOrgItemNum(Integer orgItemNum) {
		this.orgItemNum = orgItemNum;
	}

	public String getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}

	public Integer getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(Integer protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public List<MedProfileMoItem> getMedProfileMoItemList() {
		if (medProfileMoItemList == null) {
			medProfileMoItemList = new ArrayList<MedProfileMoItem>();
		}
		return medProfileMoItemList;
	}

	public void setMedProfileMoItemList(List<MedProfileMoItem> medProfileMoItemList) {
		this.medProfileMoItemList = medProfileMoItemList;
	}
	
	public void addMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		getMedProfileMoItemList().add(medProfileMoItem);
	}
	
	public void clearMedProfileMoItemList() {
		medProfileMoItemList = new ArrayList<MedProfileMoItem>();
	}

	public List<Replenishment> getReplenishmentList() {
		return replenishmentList;
	}

	public void setReplenishmentList(
			List<Replenishment> replenishmentList) {
		this.replenishmentList = replenishmentList;
	}

	public String getTypeString() {
		return typeString;
	}

	public void setTypeString(String typeString) {
		this.typeString = typeString;
	}	
}
