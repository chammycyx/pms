<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:flexiframe="http://code.google.com/p/flex-iframe/"	
	width="100%" height="100%" 
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*">
	
	<fx:Metadata>
		[Name("capdVoucherRpt")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.report.PrintCapdVoucherRptEvent;
			import hk.org.ha.event.pms.main.report.RetrieveCapdVoucherRptEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.vo.report.CapdVoucherDtl;
			import hk.org.ha.model.pms.vo.report.CapdVoucherRpt;
			import hk.org.ha.model.pms.vo.report.CapdVoucherRptDtl;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.messaging.config.ServerConfig;
			import mx.utils.ObjectUtil;
			
			private var capdVoucherRpt:CapdVoucherRpt;
			private var capdVoucherRptDtl:CapdVoucherRptDtl;
			private var capdVoucherDtl:CapdVoucherDtl;
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In] [Bindable]
			public var sysMsgMap:SysMsgMap;
			
			public override function onShowLater():void{
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.printFunc = printHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					
					clearHandler();
					reportFrame.visible = false;
				}
				
				callLater(disbaleMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showCapdVoucherRptShortcutKeyHandler);
			}
			
			private function showCapdVoucherRptShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("CapdVoucherRptView"));
			}
			
			private function disbaleMenuBarCloseButton():void
			{
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
				
			}
			
			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				this.infoString = "";
				parentApplication.clearKeyGuideHandler();
			}
			
			private function closeHandler():void {
				clearHandler();
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			private function printHandler():void{
				dispatchEvent(new PrintCapdVoucherRptEvent());
			}
			
			private function retrieveHandler():void {
				hkid.errorString = "";
				patName.errorString = "";
				voucherNum.errorString = "";
				dateFrom.errorString = "";
				dateTo.errorString = "";
				this.infoString = ""; 
				stage.focus = null;
				var validDateFrom:Boolean = ExtendedDateFieldValidator.isValidDate(dateFrom);
				var validDateTo:Boolean = ExtendedDateFieldValidator.isValidDate(dateTo);
				
				if ( !validDateFrom)
				{
					dateFrom.setFocus();
					if(dateFrom.text == "")
					{
						dateFrom.errorString = sysMsgMap.getMessage("0496");
					}
					else
					{
						dateFrom.errorString = sysMsgMap.getMessage("0001");
					}
				}
				else if (!validDateTo)
				{
					dateTo.setFocus();
					if (dateTo.text == "") 
					{
						dateTo.errorString = sysMsgMap.getMessage("0496");
					}
					else 
					{
						dateTo.errorString = sysMsgMap.getMessage("0001");
					}
				}
				else if (ObjectUtil.dateCompare(dateTo.selectedDate,dateFrom.selectedDate)== -1)
				{
					dateTo.errorString = sysMsgMap.getMessage("0584");
					dateTo.setFocus();
				}
				else 
				{
					if(hkidrb.selected && hkid.text == ""){
						hkid.errorString = sysMsgMap.getMessage("0398");
						hkid.setFocus();
					}
					else if(voucherNumrb.selected && voucherNum.text ==""){
						voucherNum.errorString = sysMsgMap.getMessage("0412");
						voucherNum.setFocus();
					}
					else if(patNamerb.selected && patName.text == ""){
						patName.errorString = sysMsgMap.getMessage("0385");
						patName.setFocus();
					}else{
						dispatchEvent(new RetrieveCapdVoucherRptEvent(dateFrom.selectedDate, dateTo.selectedDate, hkid.text, patName.text, voucherNum.text));
					}
				}
			}
			
			private function clearHandler():void {
				capdVoucherrg.selectedValue = "HKID";
				hkid.setFocus();
				hkid.enabled = true;
				dateFrom.selectedDate = new Date();
				dateTo.selectedDate = new Date();
				hkid.text = "";
				patName.text = "";
				voucherNum.text = "";
				capdVoucherrg.enabled = true;
				reportFrame.visible = false;
				
				hkid.errorString = "";
				patName.errorString = "";
				voucherNum.errorString = "";
				dateFrom.errorString = "";
				dateTo.errorString = "";
				
				pmsToolbar.printButton.enabled = false;
				pmsToolbar.retrieveButton.enabled = true;
				dateFrom.enabled = true;
				dateTo.enabled = true;

				this.infoString = "";
			}
			
			public function generateReport():void 
			{
				var urlReq:String = "report.seam?actionMethod=report.xhtml%3AcapdVoucherRptService.generateCapdVoucherRpt";			
				if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
					urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
				}		
				
				reportFrame.visible = false;
				reportFrame.source = urlReq;
				reportFrame.visible = true;
				
				pmsToolbar.retrieveButton.enabled = false;
				pmsToolbar.printButton.enabled = true;
				
				dateFrom.enabled = false;
				dateTo.enabled = false;
				hkid.enabled = false;
				patName.enabled = false;
				voucherNum.enabled = false;
				capdVoucherrg.enabled = false;
				
				if (hkidrb.selected) {
					hkidrb.setFocus();
				}else if (voucherNumrb.selected) {
					voucherNumrb.setFocus();
				}else if (patNamerb.selected) {
					patNamerb.setFocus();
				}
			
			}
			
			public function showInformationDashboard():void {
				this.infoString = sysMsgMap.getMessage("0005");
			}
			
			private function pressEnterShortcutKey(evt:KeyboardEvent):void {
				if (evt.keyCode == Keyboard.ENTER ) {
					retrieveHandler();
				}
			}
			
			private function changeRb():void {
				hkid.text = "";
				patName.text = "";
				voucherNum.text = "";
				
				hkid.errorString = "";
				patName.errorString = "";
				voucherNum.errorString = "";
				dateFrom.errorString = "";
				dateTo.errorString = "";
				
				this.infoString = "";
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if (!PopupUtil.isAnyVisiblePopupExists())
				{
					if ( evt.charCode == 52 && evt.ctrlKey ){	
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
					}	
					else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled){  //ctrl + 0 - close
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
				}
			}

			private function focusNextItem(evt:FocusEvent):void {
				if ( evt.keyCode == Keyboard.TAB && pmsToolbar.retrieveButton.enabled) {
					switch ( evt.currentTarget ) {
						case dateFrom:
							if (!evt.shiftKey) {
								evt.preventDefault();
								callLater(dateTo.setFocus);
							}else {
								evt.preventDefault();
								if (hkidrb.selected && hkid.text != "") {
									callLater(hkidrb.setFocus);
								}else if (voucherNumrb.selected && voucherNum.text != "") {
									callLater(voucherNumrb.setFocus);
								}else if (patNamerb.selected && patName.text != "") {
									callLater(patNamerb.setFocus);
								}else {
									hkidrb.selected = true;
									callLater(hkidrb.setFocus);
									changeRb();
								}
							}
							break;
						
						case dateTo:
							if (!evt.shiftKey) {
								evt.preventDefault();
								if (hkidrb.selected && hkid.text != "") {
									callLater(hkidrb.setFocus);
								}else if (voucherNumrb.selected && voucherNum.text != "") {
									callLater(voucherNumrb.setFocus);
								}else if (patNamerb.selected && patName.text != "") {
									callLater(patNamerb.setFocus);
								}else {
									hkidrb.selected = true;
									callLater(hkidrb.setFocus);
									changeRb();
								}
								
							}else {
								evt.preventDefault();
								callLater(dateFrom.setFocus);
							}
							break;
						
						case hkidrb:
							if (!evt.shiftKey) {
								evt.preventDefault();
								callLater(hkid.setFocus);
							}else {
								evt.preventDefault();
								callLater(dateTo.setFocus);
							}
							break;
						
						case hkid:
							evt.preventDefault();
							callLater(hkidrb.setFocus);
							break;
						
						case voucherNumrb:
							evt.preventDefault();
							callLater(voucherNum.setFocus);
							break;
						
						case voucherNum:
							evt.preventDefault();
							callLater(voucherNumrb.setFocus);
							break;
						
						case patNamerb:
							evt.preventDefault();
							callLater(patName.setFocus);
							break;
						
						case patName:
							evt.preventDefault();
							callLater(patNamerb.setFocus);
							break;
					}
				}
			}
			
			public function setHkidErrorString():void {
				hkid.errorString = sysMsgMap.getMessage("0033");
				hkid.setFocus();
			}
			
			private function validateDateField(evt:FocusEvent):void 
			{
				dateFrom.errorString = "";
				dateTo.errorString = "";

				if ( !ExtendedDateFieldValidator.isValidDate(dateFrom)&& dateFrom.text != "") 
				{
					dateFrom.errorString = sysMsgMap.getMessage("0001");
				}
				
				if ( !ExtendedDateFieldValidator.isValidDate(dateTo) && dateTo.text != "") 
				{
					dateTo.errorString = sysMsgMap.getMessage("0001");
				}
			}
			
		]]>
	</fx:Script>
	
	
	
	<fx:Declarations>
		
		<s:RadioButtonGroup id="capdVoucherrg" change="changeRb()"/>
	</fx:Declarations>
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	<fc:Toolbar id="pmsToolbar" width="100%"/>

	<s:VGroup width="100%" height="100%" gap="10" paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10">
		<s:Panel title="Report Criteria" width="1005" height="102">
			<s:layout> 
				<s:HorizontalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
			</s:layout>

			<s:VGroup width="300" height="100%" verticalAlign="middle">
				<s:HGroup width="100%" height="100%" verticalAlign="middle">
					<s:Label text="Date"/>
					<fc:ExtendedAbbDateField id="dateFrom" keyFocusChange="focusNextItem(event);validateDateField(event)" keyUp="pressEnterShortcutKey(event)" mouseFocusChange="validateDateField(event)" /> 
					<s:Label text="to"/>
					<fc:ExtendedAbbDateField id="dateTo" keyFocusChange="focusNextItem(event);validateDateField(event)" keyUp="pressEnterShortcutKey(event)" mouseFocusChange="validateDateField(event)"/> 
				</s:HGroup>
				<s:HGroup width="100%" height="100%"/>
			</s:VGroup>
			<s:VGroup width="100%" height="100%" verticalAlign="top">
				<s:HGroup width="100%" verticalAlign="middle">
					<s:RadioButton id="hkidrb" label="HKID" groupName="capdVoucherrg" width="60" selected="true" click="focusManager.setFocus(hkid);changeRb()" keyFocusChange="focusNextItem(event)"/>
					<fc:UppercaseTextInput id="hkid" enabled="{hkidrb.selected}" width="100" maxChars="12" keyFocusChange="focusNextItem(event)" keyUp="pressEnterShortcutKey(event)"/>
					<s:Label width="20"/>
					<s:RadioButton id="voucherNumrb" label="Voucher No." groupName="capdVoucherrg" width="110" click="focusManager.setFocus(voucherNum);changeRb()" keyFocusChange="focusNextItem(event)"/>
					<fc:UppercaseTextInput id="voucherNum" width="200" enabled="{voucherNumrb.selected}" maxChars="11" keyFocusChange="focusNextItem(event)" keyUp="pressEnterShortcutKey(event)"/>
				</s:HGroup>

				<s:HGroup width="100%" verticalAlign="middle">
					<s:RadioButton id="patNamerb" label="Name" groupName="capdVoucherrg" width="60" click="focusManager.setFocus(patName);changeRb()" keyFocusChange="focusNextItem(event)"/>
					<fc:UppercaseTextInput id="patName" width="550" enabled="{patNamerb.selected}" maxChars="48" keyFocusChange="focusNextItem(event)" keyUp="pressEnterShortcutKey(event)"/>
					
				</s:HGroup>
			</s:VGroup>
			
		</s:Panel>
		<flexiframe:IFrame id="reportFrame" width="100%" height="100%" overlayDetection="true"
						   loadIndicatorClass="hk.org.ha.view.pms.main.report.LoadingLabel"/>
	</s:VGroup>

	
</fc:ExtendedNavigatorContent>