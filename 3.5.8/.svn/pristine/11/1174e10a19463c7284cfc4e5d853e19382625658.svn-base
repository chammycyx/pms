<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:flexiframe="http://code.google.com/p/flex-iframe/"
	width="100%" height="100%" xmlns:calendar="hk.org.ha.service.app.util.calendar.*">
	
	<fx:Metadata>
		[Name("outstandSfiPaymentRptView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.report.PrintOutstandSfiPaymentRptEvent;
			import hk.org.ha.event.pms.main.report.RetrieveOutstandSfiPaymentRptEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ItemClickEvent;
			import mx.managers.PopUpManager;
			import mx.messaging.config.ServerConfig;
			import mx.utils.ObjectUtil;
			
			[Bindable]
			private var today:Date = new Date();
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In] [Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[In]
			public var propMap:PropMap;
			
			public override function onShowLater():void{
				if (!pmsToolbar.hasInited()) 
				{
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.printFunc = printHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					clearHandler();
				}
				
				callLater(disbaleMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showOutstandSfiPaymentRptShortcutKeyHandler);
			}
			
			private function disbaleMenuBarCloseButton():void
			{
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
				
			}
			
			private function showOutstandSfiPaymentRptShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("OutstandSfiPaymentRptView"));
			}

			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				this.infoString = "";
				parentApplication.clearKeyGuideHandler();
			}
			
			private function closeHandler():void {
				clearHandler();
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			public function stateChange(state:String):void{
				switch (state){
					case 'Clear':
						reportFrame.visible = false;
						dispDate.enabled = true;
						dispDate.selectedDate = new Date();
						dispDate.validateNow();
						dispDate.setFocus();
						
						pmsToolbar.retrieveButton.enabled = true;
						pmsToolbar.printButton.enabled = false;
						dispDate.errorString = "";
						this.infoString = "";
						break;
					case 'Retrieve':	
						reportFrame.visible = true;
						dispDate.enabled = false;
						
						pmsToolbar.retrieveButton.enabled = false;
						pmsToolbar.printButton.enabled = true;
						break;
				}
			}
						
			private function retrieveHandler():void {
				dispDate.errorString = "";
				this.infoString = "";
				stage.focus = null;
				if ( !ExtendedDateFieldValidator.isValidDate(dispDate) ) 
				{
					if (dispDate.text == "" ) 
					{
						dispDate.errorString = sysMsgMap.getMessage("0496");
					}
					else 
					{
						dispDate.errorString = sysMsgMap.getMessage("0001");
					}
					dispDate.setFocus();
				} 
				else if (ObjectUtil.compare(dispDate.selectedDate, today) > 0) 
				{
					dispDate.errorString = sysMsgMap.getMessage("0422");
					dispDate.setFocus();
				} 
				else 
				{
					reportFrame.visible = false;
					dispatchEvent(new RetrieveOutstandSfiPaymentRptEvent(dispDate.selectedDate));
				}
			}
			
			private function clearHandler():void 
			{
				stage.focus = null;
				stateChange('Clear');
			}
			
			private function printHandler():void {
				
				dispatchEvent(new PrintOutstandSfiPaymentRptEvent());
			}
			
			public function generateReport():void {
				
				var urlReq:String = "report.seam?actionMethod=report.xhtml%3AoutstandSfiPaymentRptService.generateOutstandSfiPaymentRpt";
				
				if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
					urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
				}		
				
				reportFrame.source = urlReq;

				stateChange('Retrieve');
				
				outstandingPanel.setFocus();
				
				showFcsClearanceErrorMessage();
			}
			
			public function showFcsClearanceErrorMessage():void{
				if( !propMap.getValueAsBoolean("charging.fcs.enabled") ){
					showSystemMessage("0014");
				}
			}
			
			public function showSystemMessage(errorCode:String, oKfunc:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if (oKfunc != null)
				{
					msgProp.okHandler = oKfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function showInformationDashboard():void {
				this.infoString = sysMsgMap.getMessage("0005");
				stage.focus = null;
				dispDate.setFocus();
			}
	
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( !PopupUtil.isAnyVisiblePopupExists() )
				{
					if ( evt.charCode == 52 && evt.ctrlKey && pmsToolbar.clearButton.enabled){	
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
					}
					else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled){  //ctrl + 0 - close
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
				}
			} 
			
			private function pressEnter(evt:KeyboardEvent):void {
				if ( evt.keyCode == 13 ){
					retrieveHandler();
				}
			}
			
			private function focusNextItem(evt:FocusEvent):void {
				if ( evt.keyCode == Keyboard.TAB && pmsToolbar.retrieveButton.enabled) {
					switch ( evt.currentTarget ) {
						case dispDate:
							evt.preventDefault();
							callLater(dispDate.setFocus);
							break;
					}
				}
			}
			
			private function validateDate():void
			{
				dispDate.errorString = "";
				
				if(!ExtendedDateFieldValidator.isValidDate(dispDate) && dispDate.text != "")
				{
					dispDate.errorString = sysMsgMap.getMessage("0001");
				}
			}
		]]>
	</fx:Script>
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	<fc:Toolbar id="pmsToolbar" width="100%"/>

	<s:VGroup width="100%" height="100%" gap="10" paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10">
		<s:Panel id="outstandingPanel" title="Report Criteria" height="79" width="100%">
			<s:layout>
				<s:HorizontalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10" verticalAlign="middle"/>
			</s:layout>
			<s:Label text="Dispensing Date"/>
			<fc:ExtendedAbbDateField id="dispDate" selectedDate="{today}" keyUp="pressEnter(event)"
									 keyFocusChange="focusNextItem(event);validateDate()" mouseFocusChange="validateDate()"/>
		</s:Panel>
		<flexiframe:IFrame id="reportFrame" width="100%" height="100%" overlayDetection="true"
						   />
	</s:VGroup>
</fc:ExtendedNavigatorContent>