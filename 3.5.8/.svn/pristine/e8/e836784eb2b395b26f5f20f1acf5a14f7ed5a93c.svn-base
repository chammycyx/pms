<?xml version="1.0" encoding="utf-8"?>

<s:NavigatorContent xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
					xmlns:fx="http://ns.adobe.com/mxml/2009" 
					xmlns:s="library://ns.adobe.com/flex/spark" 
					xmlns:mx="library://ns.adobe.com/flex/mx"
					xmlns:pivas="hk.org.ha.view.pms.main.pivas.*">
	
	<fx:Metadata>
		[Name("pivasWorklistContentView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.Sort;
			import mx.containers.TabNavigator;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			import mx.utils.StringUtil;
			
			import hk.org.ha.event.pms.main.delivery.AssignBatchAndPrintEvent;
			import hk.org.ha.event.pms.main.delivery.FinishAssignBatchAndPrintForPivasEvent;
			import hk.org.ha.event.pms.main.pivas.DeleteFromWorklistEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasWorklistListEvent;
			import hk.org.ha.event.pms.main.pivas.popup.ShowPivasWorklistSortPopupEvent;
			import hk.org.ha.event.pms.main.report.PrintPivasWorklistSummaryRptEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.Toolbar;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
			import hk.org.ha.model.pms.persistence.pivas.PivasWorklist;
			import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestOrderType;
			import hk.org.ha.model.pms.udt.pivas.PivasWorklistSortColumn;
			import hk.org.ha.model.pms.udt.pivas.PivasWorklistType;
			import hk.org.ha.model.pms.vo.pivas.worklist.PivasWorklistResponse;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			import hk.org.ha.view.pms.main.pivas.PivasWorklistUiInfo;
			
			
			public static const PIVAS_WORKLIST_WORKLIST_SORT:String = "pivas.worklist.worklist.sort";
			
			[In]
			public var closeMenuItem:MenuItem;	
			
			[In] [Bindable]
			public var propMap:PropMap;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[In] [Bindable]
			public var dmDailyFrequencyList:ArrayCollection;
			
			// --------------------------------------
			// Variable passed by parent tab
			// --------------------------------------
			public var pmsToolbar:Toolbar;
			
			public var pivasWorklistTab:TabNavigator;
			
			public var pivasDueOrderView:PivasDueOrderView;
			
			public var reloadAllTab:Function;
			// --------------------------------------
			
			[Bindable]
			private var worklistSupplyDate:Date;
			
			[Bindable]
			private var pivasWorklistList:ArrayCollection;
			
			private var assignBatchPrintButtonEnabled:Boolean = true;
			
			private var pendingConfirmDeleteWorklist:Boolean = false;
			
			public var sortColumnDataValue:String = null;
			
			public function initToolbar():void {
				pmsToolbar.addToWorklistButton.enabled = false;
				pmsToolbar.retrieveButton.enabled = false;
				if (pivasWorklistGrid.selectedItem != null) {
					pmsToolbar.deleteButton.enabled = true;
				} else {
					pmsToolbar.deleteButton.enabled = false;
				}
				pmsToolbar.clearButton.enabled = true;
				pmsToolbar.sortButton.enabled = true;
				pmsToolbar.assignBatchPrintButton.enabled = assignBatchPrintButtonEnabled;
			}
			
			private function showSystemMessage(errorCode:String, yesFunc:Function=null, noFunc:Function=null, okfunc:Function=null, params:Array=null):void {	
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if( params != null ){
					msgProp.messageParams = params;
				}
				
				if (okfunc != null) {
					msgProp.setOkButtonOnly = true;
					msgProp.setYesNoButton = false;
					msgProp.okHandler = okfunc;
				} else if (yesFunc!=null && noFunc!=null) {
					msgProp.setYesNoButton = true;
					msgProp.yesHandler = yesFunc;
					msgProp.noHandler = noFunc;
					msgProp.setYesDefaultButton = false;
					msgProp.setNoDefaultButton = true;
				} else if(noFunc!=null) {
					msgProp.setYesNoButton = true;
					msgProp.setOkButtonOnly = false;
					msgProp.noHandler = noFunc;
				} else if(yesFunc!=null) {
					msgProp.setYesNoButton = true;
					msgProp.setOkButtonOnly = false;
					msgProp.yesHandler = yesFunc;
					msgProp.setYesDefaultButton = false;
					msgProp.setNoDefaultButton = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function retrieveHandler():void {
				// note: because some of processing flag is already set true in PivasWorklistView.loadAllTab() when calling other retrieve function, 
				//       processing flag cannot be checked here and needed to be check in PivasWorklistView.loadAllTab()
				
				PivasWorklistUiInfo.processingWorklistTabFlag = true;
				
				dispatchEvent(new RetrievePivasWorklistListEvent(updatePivasWorklistList));
			}

			private function updatePivasWorklistList(_pivasWorklistList:ArrayCollection):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingWorklistTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
					return;		// no need to proceed when tab will be reloaded since this function is called again
				}
				
				pendingConfirmDeleteWorklist = false;
				
				assignBatchPrintButtonEnabled = true;
				if (pivasWorklistTab != null && pivasWorklistTab.selectedChild == this) {
					pmsToolbar.assignBatchPrintButton.enabled = true;					
				}
				
				pivasWorklistList = _pivasWorklistList;
				this.label = PivasWorklistType.Worklist + " (" + pivasWorklistList.length + ")";
				
				if (_pivasWorklistList != null && _pivasWorklistList.length > 0) {
					var firstPivasWorklist:PivasWorklist = _pivasWorklistList.getItemAt(0) as PivasWorklist;
					worklistSupplyDate = firstPivasWorklist.supplyDate;
				} else {
					worklistSupplyDate = null;
				}
					
				if (checkConfirmDeleteWorklist()) {
					if (pivasWorklistTab != null && pivasWorklistTab.selectedChild == this) {
						confirmDeleteWorklist(true);
					} else {
						// do not prompt message after retrieve when PIVAS worklist screen is initiated
						pendingConfirmDeleteWorklist = true;						
					}
				}
				
				sortPivasWorklistList();
					
				PivasWorklistUiInfo.updatePivasInfoErrorMessage(pivasWorklistList, sysMsgMap);
				
				if (pivasWorklistTab != null && pivasWorklistTab.selectedChild == this) {
					pmsToolbar.deleteButton.enabled = false;
					
					// do not prompt focus after retrieve when PIVAS worklist screen is initiated
					pivasWorklistGrid.setFocus();
				}
			}
			
			public function sortHandler():void {
				if (PivasWorklistUiInfo.isWorklistProcessing()) {
					return;
				}
				
				PivasWorklistUiInfo.processingWorklistTabFlag = true;
				
				dispatchEvent(new ShowPivasWorklistSortPopupEvent(PivasWorklistType.Worklist, sortOkCallbackFunc, sortCancelCallbackFunc));
			}
			
			private function sortOkCallbackFunc(_sortColumnDataValue:String):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingWorklistTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
				
				sortColumnDataValue = _sortColumnDataValue;
				sortPivasWorklistList();
			}
			
			private function sortCancelCallbackFunc():void {
				// resume procesing flag
				PivasWorklistUiInfo.processingWorklistTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
			}
			
			private function sortPivasWorklistList(sortType:String=null):void {
				var pivasWorklistSortColumn:PivasWorklistSortColumn = null;
				if (sortColumnDataValue == null) {
					// set default sort column
					var defaultSortColumnDataValue:String = propMap.getValue(PIVAS_WORKLIST_WORKLIST_SORT);
					pivasWorklistSortColumn = PivasWorklistSortColumn.dataValueOf(defaultSortColumnDataValue);
				} else {
					pivasWorklistSortColumn = PivasWorklistSortColumn.dataValueOf(sortColumnDataValue);
				}
				
				pivasWorklistList.sort = new Sort();
				
				if (pivasWorklistSortColumn != null) {
					switch (pivasWorklistSortColumn) {
						case PivasWorklistSortColumn.DueDate:
							pivasWorklistList.sort.compareFunction = dueDateCompareFunc;
							break;
						case PivasWorklistSortColumn.PreparationDtlDueDate:
							pivasWorklistList.sort.compareFunction = preparationDtlDueDateCompareFunc;
							break;
						case PivasWorklistSortColumn.PreparationDtlPhsWardBedNumPatName:
							pivasWorklistList.sort.compareFunction = preparationDtlPhsWardBedNumPatNameCompareFunc;
							break;
						case PivasWorklistSortColumn.PatNamePreparationDtl:
							pivasWorklistList.sort.compareFunction = patNamePreparationDtlCompareFunc;
							break;
						case PivasWorklistSortColumn.PhsWardBedNumPatNamePreparationDtl:
							pivasWorklistList.sort.compareFunction = phsWardBedNumPatNamePreparationDtlCompareFunc;
							break;
					}
				}
				
				pivasWorklistList.refresh();
			}

			private function dueDateCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var dueDateResult:int = ObjectUtil.compare(pivasWorklist1.dueDate, pivasWorklist2.dueDate);
				if (dueDateResult != 0) {
					return dueDateResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}			
			
			private function preparationDtlDueDateCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var preparationDetail1:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist1, dmDailyFrequencyList);
				var preparationDetail2:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist2, dmDailyFrequencyList);
				
				var preparationDetailResult:int = ObjectUtil.compare(preparationDetail1, preparationDetail2);
				if (preparationDetailResult != 0) {
					return preparationDetailResult;
				}
				
				var dueDateResult:int = ObjectUtil.compare(pivasWorklist1.dueDate, pivasWorklist2.dueDate);
				if (dueDateResult != 0) {
					return dueDateResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}
			
			private function preparationDtlPhsWardBedNumPatNameCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var preparatationDetail1:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist1, dmDailyFrequencyList);
				var preparationDetail2:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist2, dmDailyFrequencyList);
				
				var preparationDetailResult:int = ObjectUtil.compare(preparatationDetail1, preparationDetail2);
				if (preparationDetailResult != 0) {
					return preparationDetailResult;
				}
				
				var wardCodeResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.wardCode, pivasWorklist2.medProfile.wardCode);
				if (wardCodeResult != 0) {
					return wardCodeResult;
				}
				
				var bedNumResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.medCase.pasBedNum, pivasWorklist2.medProfile.medCase.pasBedNum);
				if (bedNumResult != 0) {
					return bedNumResult;
				}
				
				var patNameResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.patient.name, pivasWorklist2.medProfile.patient.name);
				if (patNameResult != 0) {
					return patNameResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}

			private function patNamePreparationDtlCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var patNameResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.patient.name, pivasWorklist2.medProfile.patient.name);
				if (patNameResult != 0) {
					return patNameResult;
				}
				
				var preparationDetail1:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist1, dmDailyFrequencyList);
				var preparationDetail2:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist2, dmDailyFrequencyList);
				
				var preparationDetailResult:int = ObjectUtil.compare(preparationDetail1, preparationDetail2);
				if (preparationDetailResult != 0) {
					return preparationDetailResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}
			
			private function phsWardBedNumPatNamePreparationDtlCompareFunc(pivasWorklist1:PivasWorklist, pivasWorklist2:PivasWorklist, fields:Array = null):int {
				var wardCodeResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.wardCode, pivasWorklist2.medProfile.wardCode);
				if (wardCodeResult != 0) {
					return wardCodeResult;
				}
				
				var bedNumResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.medCase.pasBedNum, pivasWorklist2.medProfile.medCase.pasBedNum);
				if (bedNumResult != 0) {
					return bedNumResult;
				}
				
				var patNameResult:int = ObjectUtil.compare(pivasWorklist1.medProfile.patient.name, pivasWorklist2.medProfile.patient.name);
				if (patNameResult != 0) {
					return patNameResult;
				}
				
				var preparationDetail1:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist1, dmDailyFrequencyList);
				var preparationDetail2:String = PivasWorklistUiInfo.getPreparationDetailsText(pivasWorklist2, dmDailyFrequencyList);
				
				var preparationDetailResult:int = ObjectUtil.compare(preparationDetail1, preparationDetail2);
				if (preparationDetailResult != 0) {
					return preparationDetailResult;
				}
				
				// add ID sorting to guarantee all records are distinct 
				// (to fix problem if there are multiple records with same ordering, not all checkbox is checked when user check add all to worklist checkbox)   
				return ObjectUtil.compare(pivasWorklist1.id, pivasWorklist2.id);				
			}
			
			private function checkConfirmDeleteWorklist():Boolean {
				if (worklistSupplyDate == null) {
					return false;
				}
				
				var supplyDateWithoutTime:Date = new Date(worklistSupplyDate.getFullYear(), worklistSupplyDate.getMonth(), worklistSupplyDate.getDate());
				
				var today:Date = new Date();
				var todayWithoutTime:Date = new Date(today.getFullYear(), today.getMonth(), today.getDate());
				
				if (ObjectUtil.dateCompare(supplyDateWithoutTime, todayWithoutTime) < 0) {
					return true;
				} else {
					return false;
				}
			}
			
			public function confirmDeleteWorklist(forceConfirm:Boolean):void {
				if (pivasWorklistTab != null && pivasWorklistTab.selectedChild == this) {
					// for safety only to check active tab is worklist tab
					if (forceConfirm || pendingConfirmDeleteWorklist) {
						var yesHandler:Function = function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							deleteFromWorklist(pivasWorklistList);
						}
						
						var noHandler:Function = function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							assignBatchPrintButtonEnabled = false;
							pmsToolbar.assignBatchPrintButton.enabled = false;
						}
						
						showSystemMessage("0913", yesHandler, noHandler, null);
						
						pendingConfirmDeleteWorklist = false;
					}
				}
			}
			
			public function deleteHandler():void {
				if (pivasWorklistGrid.selectedItem == null) {
					return;
				}
				
				var _pivasWorklistList:ArrayCollection = new ArrayCollection(new Array(pivasWorklistGrid.selectedItem));
				deleteFromWorklist(_pivasWorklistList);
			}
			
			private function deleteAllFromWorklist():void {
				if (pivasWorklistList.length == 0) {
					return;
				}
				
				var yesHandler:Function = function(evt:MouseEvent):void {
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
					deleteFromWorklist(pivasWorklistList);
				}
				
				showSystemMessage("0950", yesHandler, null, null);
			}
			
			private function deleteFromWorklist(_pivasWorklistList:ArrayCollection):void {
				if (PivasWorklistUiInfo.isWorklistProcessing()) {
					return;
				}
				
				PivasWorklistUiInfo.processingWorklistTabFlag = true;
				
				dispatchEvent(new DeleteFromWorklistEvent(_pivasWorklistList, deleteFromWorklistCallbackFunc));
			}
			
			private function deleteFromWorklistCallbackFunc(pivasWorklistResponse:PivasWorklistResponse):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingWorklistTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
				
				if (StringUtil.trim(pivasWorklistResponse.errorCode) != "") {
					var okHandler:Function = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						pivasWorklistGrid.setFocus();
					}
					
					showSystemMessage(pivasWorklistResponse.errorCode, null, null, okHandler);
				}
			}
			
			public function clearHandler():void {
				deleteAllFromWorklist();
			}
			
			public function assignBatchPrintHandler():void {
				if (pivasWorklistList.length <= 0) {
					return;
				}
				
				if (checkConfirmDeleteWorklist()) {
					confirmDeleteWorklist(true);
					return;
				}
				
				if (PivasWorklistUiInfo.isWorklistProcessing()) {
					return;
				}
				
				PivasWorklistUiInfo.processingWorklistTabFlag = true;
				
				var request:DeliveryRequest = new DeliveryRequest();
				request.orderType = DeliveryRequestOrderType.None;
				request.pivasFlag = true;
				request.supplyDate = worklistSupplyDate;
				dispatchEvent(new AssignBatchAndPrintEvent(request));				
			}
			
			[Observer]
			public function finishAssignBatchAndPrintForPivas(evt:FinishAssignBatchAndPrintForPivasEvent):void {
				// resume procesing flag
				PivasWorklistUiInfo.processingWorklistTabFlag = false;
				
				// refresh worklist screen
				if (PivasWorklistUiInfo.pendingReloadAllTab) {
					reloadAllTab();
					PivasWorklistUiInfo.pendingReloadAllTab = false;
				}
			}
			
			private function worklistSummaryBtnHandler(evt:MouseEvent):void {
				if (pivasWorklistList.length > 0) {
					dispatchEvent(new PrintPivasWorklistSummaryRptEvent(pivasWorklistList));
				}
			}
			
			protected function pivasWorklistGridItemRendererFunction(data:Object):IFactory {
				return pivasWorklistContentRenderer;
			}

			private function pivasWorklistGridChangeHandler():void {
				if (pivasWorklistGrid.selectedItem != null) {
					pmsToolbar.deleteButton.enabled = true;
				} else {
					pmsToolbar.deleteButton.enabled = false;
				}
			}
		]]>
		
	</fx:Script>
	
	<fx:Declarations>
		<!-- Place non-visual elements (e.g., services, value objects) here -->
		<fx:Component id="pivasWorklistContentRenderer">
			<pivas:PivasWorklistContentRenderer dmDailyFrequencyList="{outerDocument.dmDailyFrequencyList}"/>
		</fx:Component>
	</fx:Declarations>
	
	<s:layout>
		<s:VerticalLayout paddingTop="0" paddingLeft="10" paddingRight="10" paddingBottom="10" gap="10"/>
	</s:layout>
	
	<s:VGroup width="100%" height="100%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="10">
		<s:HGroup width="100%" verticalAlign="middle">
			<s:Label text="PIVAS Supply Date"/>
			<s:TextInput width="120" enabled="false" text="{MedProfileUtils.formatDateTime(worklistSupplyDate, false)}"/>
			<s:VGroup width="100%" horizontalAlign="right">
				<fc:ExtendedButton label="Worklist Summary" id="worklistSummaryBtn" click="worklistSummaryBtnHandler(event)"/>
			</s:VGroup>
		</s:HGroup>
		<s:VGroup width="100%" height="100%" gap="0">
			<fc:ExtendedDataGrid id="pivasWorklistHeaderGrid" width="100%" height="0%" wordWrap="true"
								 sortableColumns="false" resizableColumns="false" draggableColumns="false">
				<fc:columns>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistDueDateColWidth}" headerText="Due Date"/>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistPatientInfoColWidth}" headerText="Name&#10;Case No. / PHS Ward (Bed)"/>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistPreparationDetailsColWidth}" headerText="Preparation Details"/>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistDrugItemDetailsColWidth}" headerText="Drug Item Details"/>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistSolventDiluentDetailsColWidth}" headerText="Solvent / Diluent Details"/>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistSupplyColWidth}" headerText="Supply"/>
					<mx:DataGridColumn width="@{PivasWorklistUiInfo.pivasWorklistPivasRequestColWidth}" headerText="PIVAS Request"/>
				</fc:columns>
			</fc:ExtendedDataGrid>		
			<s:List id="pivasWorklistGrid" width="100%" height="100%" dataProvider="{pivasWorklistList}"				 
					useVirtualLayout="false" hasFocusableChildren="true" horizontalScrollPolicy="off" verticalScrollPolicy="on"
					itemRendererFunction="{pivasWorklistGridItemRendererFunction}"
					change="pivasWorklistGridChangeHandler()"/>
		</s:VGroup>
	</s:VGroup>
</s:NavigatorContent>