<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:flexiframe="http://code.google.com/p/flex-iframe/"
	width="100%" height="100%" xmlns:calendar="hk.org.ha.service.app.util.calendar.*">
	
	<fx:Metadata>
		[Name("pivasRptView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.messaging.config.ServerConfig;
			import mx.utils.ObjectUtil;
			import mx.utils.StringUtil;
			
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.lookup.popup.ItemCodeLookupPopupProp;
			import hk.org.ha.event.pms.main.lookup.popup.ManufCodeLookupPopupProp;
			import hk.org.ha.event.pms.main.lookup.popup.ShowItemCodeLookupPopupEvent;
			import hk.org.ha.event.pms.main.lookup.popup.ShowManufCodeLookupPopupEvent;
			import hk.org.ha.event.pms.main.report.ExportPivasRptEvent;
			import hk.org.ha.event.pms.main.report.PrintPivasRptEvent;
			import hk.org.ha.event.pms.main.report.RetrievePivasRptEvent;
			import hk.org.ha.event.pms.main.report.popup.ShowFilePasswordPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.persistence.corp.Workstore;
			import hk.org.ha.model.pms.udt.YesNoBlankFlag;
			import hk.org.ha.model.pms.udt.report.PivasReportType;
			import hk.org.ha.model.pms.udt.report.PivasRptDispMethod;
			import hk.org.ha.model.pms.udt.report.PivasRptItemCatFilterOption;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			[Bindable]
			public var pivasReportTypeList:ArrayCollection = new ArrayCollection(PivasReportType.constants);
			
			[Bindable]
			public var pivasRptDispMethodList:ArrayCollection = new ArrayCollection(PivasRptDispMethod.constants);
			
			[Bindable]
			public var pivasRptItemCatList:ArrayCollection = new ArrayCollection(PivasRptItemCatFilterOption.constants);
			
			[Bindable]
			public var satelliteList:ArrayCollection = new ArrayCollection(YesNoBlankFlag.constants);

			[In]
			public var workstore:Workstore;
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In][Bindable]
			public var propMap:PropMap;
			
			[In] [Bindable]
			public var sysMsgMap:SysMsgMap;
			
			public override function onShow():void {
			}
			
			public override function onShowLater():void {
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.exportFunc = exportHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.printFunc = printHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();

					reportFrame.visible = false;
					clearHandler();

					rptTypeDdl.setFocus();
					focusManager.showFocus();
				}
				
				callLater(disbaleMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showPivasRptShortcutKeyHandler);
			}
			
			private function showPivasRptShortcutKeyHandler():void {
				dispatchEvent(new ShowKeyGuidePopupEvent("PivasRptView"));
			}
			
			private function disbaleMenuBarCloseButton():void {
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
			}
			
			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				this.infoString = "";
				parentApplication.clearKeyGuideHandler();
			}
			
			private function closeHandler():void {
				clearHandler();
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			private function displayDdlDataValueFunc(item:Object):String {
				return item.dataValue;
			}
			
			private function showItemLookupPopup(event:Event):void {
				var itemCodeLookupPopProp:ItemCodeLookupPopupProp = new ItemCodeLookupPopupProp();
				itemCodeLookupPopProp.doubleClickHandler = itemCodeLookupDoubleClickFunc;
				itemCodeLookupPopProp.prefixItemCode = itemCodeTxt.text;
				dispatchEvent(new ShowItemCodeLookupPopupEvent(itemCodeLookupPopProp));
			}

			private function itemCodeLookupDoubleClickFunc(evt:ListEvent):void {
				itemCodeTxt.text = (evt.itemRenderer.data).itemCode;
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				itemCodeTxt.setFocus();
			}
			
			private function showManufCodeLookupPopup(event:Event):void {
				var manufCodeLookupPopProp:ManufCodeLookupPopupProp = new ManufCodeLookupPopupProp();
				manufCodeLookupPopProp.doubleClickHandler = manufCodeLookupDoubleClickFunc;
				manufCodeLookupPopProp.prefixManufCode = manufCodeTxt.text;
				dispatchEvent(new ShowManufCodeLookupPopupEvent(manufCodeLookupPopProp));
			}
			
			private function manufCodeLookupDoubleClickFunc(evt:ListEvent):void {
				manufCodeTxt.text = (evt.itemRenderer.data).companyCode;
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				manufCodeTxt.setFocus();
			}
			
			private function retrieveHandler():void {
				//clear ErrorString before calling validateSearchCriteria()
				clearErrorString(); 
				stage.focus = null;

				var resultSupplyDateFrom:Date = null;
				var resultSupplyDateTo:Date = null;
				
				if (validateSearchCriteria()) {
					if (supplyTimeRb.selected) { 
						// search by supply date with specific supply time
						resultSupplyDateFrom = calculateDateTime(supplyDateFrom.selectedDate, supplyTimeTxt.text.toString());
						resultSupplyDateTo = null;
					} else { 
						// search by supply date range
						resultSupplyDateFrom = supplyDateFrom.selectedDate;
						resultSupplyDateTo = supplyDateTo.selectedDate;
					}
					
					dispatchEvent(new RetrievePivasRptEvent(rptTypeDdl.selectedItem, resultSupplyDateFrom, resultSupplyDateTo, satelliteDdl.selectedItem, 
															hkidCaseNumTxt.text, pivasRptDispMethodDdl.selectedItem, pivasRptItemCatDdl.selectedItem, 
															itemCodeTxt.text, drugKeyCbx.selected, manufCodeTxt.text, batchNumTxt.text));
				}	
			}

			private function clearErrorString():void {
				supplyDateFrom.errorString = "";
				supplyTimeTxt.errorString = "";
				supplyDateTo.errorString = "";
				hkidCaseNumTxt.errorString = "";
				itemCodeTxt.errorString = "";
				manufCodeTxt.errorString = "";
				batchNumTxt.errorString = "";
				
				this.infoString = "";
			}
			
			public static function calculateDateTime(inputDate:Date, inputTime:String):Date {
				var resultDateTime:Date = new Date(inputDate.fullYear, inputDate.month, inputDate.date, 
					Number(inputTime.substr(0, 2)), Number(inputTime.substr(2)), 0, 0);
				return resultDateTime;
			}
			
			private function validateSearchCriteria():Boolean {
				// Both date range and specific date time search needs to be checked
				if(!ExtendedDateFieldValidator.isValidDate(supplyDateFrom)) {
					if ( supplyDateFrom.text == "" ) {
						// Please input report date
						supplyDateFrom.errorString = sysMsgMap.getMessage("0496");
					} else {	
						// Invalid date format [DDMMYYYY]
						supplyDateFrom.errorString = sysMsgMap.getMessage("0001");
					}
					supplyDateFrom.setFocus();
					return false;	
				}
				
				// 	supply date time search only
				if (supplyTimeRb.selected) {
					if (StringUtil.trim(supplyTimeTxt.text) == "") {
						// Please input supply time.
						supplyTimeTxt.errorString = sysMsgMap.getMessage("0794");
						supplyTimeTxt.setFocus();
						return false;
					} else if (! isValidTimeStr(supplyTimeTxt.text)) {
						// Invalid supply time.
						supplyTimeTxt.errorString = sysMsgMap.getMessage("0800");
						supplyTimeTxt.setFocus();
						return false;
					}
				}
				
				// supply date range search only
				if (supplyDateToRb.selected) {
					if (!ExtendedDateFieldValidator.isValidDate(supplyDateTo)) {
						if (supplyDateTo.text == "" ) {
							// Please input report date
							supplyDateTo.errorString = sysMsgMap.getMessage("0496");
						} else {
							// Invalid date format [DDMMYYYY]
							supplyDateTo.errorString = sysMsgMap.getMessage("0001");
						}
						supplyDateTo.setFocus();
						return false;
					} else if (ObjectUtil.dateCompare(supplyDateTo.selectedDate, supplyDateFrom.selectedDate)== -1) { 
						// Report end date should be later than start date
						supplyDateTo.errorString = sysMsgMap.getMessage("0584");
						supplyDateTo.setFocus();
						return false;
					} else if (!isValidDateRange()) {
						supplyDateFrom.setFocus();
						return false;
					}
				}
				
				if (rptTypeDdl.selectedIndex != 0) {
					if (drugKeyCbx.selected && isItemCodeEmpty()) {
						// Please input item code
						itemCodeTxt.errorString = sysMsgMap.getMessage("0375");
						itemCodeTxt.setFocus();
						return false;
					}
				}

				// forcing input of itemCode or manufCode if batchNum is used to search
				if (StringUtil.trim(batchNumTxt.text) != "" && StringUtil.trim(itemCodeTxt.text) == "" && StringUtil.trim(manufCodeTxt.text) == "") {
					// Please input item code or manufacturer code if batch no. is specified.
					itemCodeTxt.errorString = sysMsgMap.getMessage("0985");
					itemCodeTxt.setFocus();
					return false;
				}
				return true;
			}
			
			public function exportHandler():void {
				// export with password for PivasPatientDispRpt only
				if (rptTypeDdl.selectedIndex == 1) {
					dispatchEvent(new ShowFilePasswordPopupEvent("PivasRptView", null, null));
				} else {
					dispatchEvent(new ExportPivasRptEvent(null));
				}
			}
			
			private function printHandler():void {
				// export with password for PivasPatientDispRpt only
				if( rptTypeDdl.selectedIndex == 1) {
					// This report contains patient's HKID and Name, please handle according to local guideline and policy.
					showSystemMessage("0352", printOkHandler);
				} else {
					dispatchEvent(new PrintPivasRptEvent());					
				}
			}
			
			private function printOkHandler(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				dispatchEvent(new PrintPivasRptEvent());
			}
			
			private function clearHandler():void {
				satelliteDdl.dataProvider = satelliteList;
				pivasRptDispMethodDdl.dataProvider = pivasRptDispMethodList;
				pivasRptItemCatDdl.dataProvider = pivasRptItemCatList;
				
				rptTypeDdl.selectedIndex = 0;
				rptTypeDdl.enabled = true;
				supplyTimeRb.enabled = true;
				supplyDateToRb.enabled = true;
				
				supplyDateToRb.selected = true;
				
				supplyTimeTxt.enabled = false;
				supplyDateTo.enabled = true;
				
				supplyDateFrom.selectedDate = new Date();
				supplyTimeTxt.text = "";
				supplyDateTo.selectedDate = new Date();
				hkidCaseNumTxt.text = "";
				itemCodeTxt.text = "";
				manufCodeTxt.text = "";
				batchNumTxt.text = "";
				
				satelliteDdl.selectedIndex = 0;
				pivasRptDispMethodDdl.selectedIndex = 0;
				pivasRptItemCatDdl.selectedIndex = 0;

				stage.focus = null;
				rptTypeDdl.setFocus();
				focusManager.showFocus();
				
				reportFrame.visible = false;
				
				rptTypeDdlChangeHandler();
			
				pmsToolbar.retrieveButton.enabled = true;
				pmsToolbar.printButton.enabled = false;
				pmsToolbar.exportButton.enabled = false;
			}

			public function generateReport():void  {
				var urlReq:String = "report.seam?actionMethod=report.xhtml%3ApivasRptService.generatePivasRpt";			
				if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
					urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
				}		
				
				reportFrame.visible = false;
				reportFrame.source = urlReq;
				reportFrame.visible = true;
				
				pmsToolbar.retrieveButton.enabled = false;
				pmsToolbar.printButton.enabled = true;
				pmsToolbar.exportButton.enabled = true;
				
				rptTypeDdl.enabled = false;
				supplyTimeRb.enabled = false;
				supplyDateToRb.enabled = false;
				
				supplyDateFrom.enabled = false;
				supplyTimeTxt.enabled = false;
				supplyDateTo.enabled = false;
				satelliteDdl.enabled = false;
				
				hkidCaseNumTxt.enabled = false;
				pivasRptDispMethodDdl.enabled = false;
				pivasRptItemCatDdl.enabled = false;
				
				itemCodeTxt.enabled = false;
				itemCodeLookupBtn.enabled = false;
				drugKeyCbx.enabled = false;
				
				manufCodeTxt.enabled = false;
				manufCodeLookupBtn.enabled = false;
				batchNumTxt.enabled = false;

				pivasRptPanel.setFocus();
			}
			
			public function exportReport():void {
				var urlReq:String = "report.seam?actionMethod=report.xhtml%3ApivasRptService.exportPivasRpt";			
				if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
					urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
				}		
				exportFrame.source = urlReq;
			}
			
			public function showInformationDashboard():void {
				// Record not found
				this.infoString = sysMsgMap.getMessage("0005");
				stage.focus = null;
				
				if (supplyTimeRb.selected) {
					supplyTimeTxt.setFocus();
				} else { //supplyDateToRb.selected
					supplyDateFrom.setFocus();
				}
			}
			
			public function showSystemMessage(errorCode:String, oKfunc:Function=null, params:Array=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.messageParams = params;
				
				if (oKfunc != null) {
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				} else  {
					msgProp.setOkButtonOnly = true;
					msgProp.okHandler = okPopupHandler;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function okPopupHandler(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
			}
			
			public function rptTypeDdlChangeHandler():void{
				supplyDateFrom.selectedDate = new Date();
				supplyDateTo.selectedDate = new Date();
				supplyTimeTxt.text = "";
				hkidCaseNumTxt.text = "";
				itemCodeTxt.text = "";
				manufCodeTxt.text = "";
				batchNumTxt.text = "";
				
				supplyDateFrom.enabled = true;
				supplyDateTo.enabled = true;
	
				supplyDateToRb.selected = true;
				
				supplyTimeTxt.enabled = false;
				
				drugKeyCbx.selected = false;
				
				satelliteDdl.selectedIndex = 0;
				//reset PivasRptDispMethod dropdown
				pivasRptDispMethodDdl.selectedIndex = 0;
				pivasRptItemCatDdl.selectedIndex = 0;
				
				clearErrorString();
				
				itemCodeLookupBtn.enabled = false;
				drugKeyCbx.enabled = false;
				manufCodeLookupBtn.enabled = false;

				if (rptTypeDdl.selectedIndex == 0) { // PivasConsumptionRpt
					satelliteDdl.enabled = true;
					
					hkidCaseNumTxt.enabled = false;
					pivasRptDispMethodDdl.enabled = true;
					pivasRptItemCatDdl.enabled = true;
					
					itemCodeTxt.enabled = false;
					itemCodeLookupBtn.enabled = false;
					drugKeyCbx.enabled = false;
					manufCodeTxt.enabled = false;
					manufCodeLookupBtn.enabled = false;
					batchNumTxt.enabled = false;
					
				} else if (rptTypeDdl.selectedIndex == 1) { // PivasPatientDispRpt
					satelliteDdl.enabled = true;
					
					hkidCaseNumTxt.enabled = true;
					pivasRptDispMethodDdl.enabled = false;
					pivasRptItemCatDdl.enabled = true;
					
					itemCodeTxt.enabled = true;
					itemCodeLookupBtn.enabled = false;
					drugKeyCbx.enabled = true;
					manufCodeTxt.enabled = true;
					manufCodeLookupBtn.enabled = true;
					batchNumTxt.enabled = true;
					
					// PivasRptDispMethod, Patient Dispensing
					pivasRptDispMethodDdl.selectedIndex = 1;
					
				} else if (rptTypeDdl.selectedIndex == 2) { // PivasBatchDispRpt
					satelliteDdl.enabled = false;
					
					hkidCaseNumTxt.enabled = false;
					pivasRptDispMethodDdl.enabled = false;
					pivasRptItemCatDdl.enabled = false;
					
					itemCodeTxt.enabled = true;
					itemCodeLookupBtn.enabled = false;
					drugKeyCbx.enabled = true;
					manufCodeTxt.enabled = true;
					manufCodeLookupBtn.enabled = true;
					batchNumTxt.enabled = true;
					
					// PivasRptDispMethod, Batch Dispensing
					pivasRptDispMethodDdl.selectedIndex = 2;
				} else if (rptTypeDdl.selectedIndex == 3) { // PivasWorkloadSummaryPatientRpt
					satelliteDdl.enabled = true;
					
					hkidCaseNumTxt.enabled = false;;
					pivasRptDispMethodDdl.enabled = false;
					pivasRptItemCatDdl.enabled = false;
					
					itemCodeTxt.enabled = true;
					itemCodeLookupBtn.enabled = false;
					drugKeyCbx.enabled = true;
					manufCodeTxt.enabled = false;
					manufCodeLookupBtn.enabled = false;
					batchNumTxt.enabled = false;
					
					// PivasRptDispMethod, Patient Dispensing
					pivasRptDispMethodDdl.selectedIndex = 1;
					// Drug search
					pivasRptItemCatDdl.selectedIndex = 1;
				} else if (rptTypeDdl.selectedIndex == 4) { // PivasWorkloadSummaryBatchRpt
					satelliteDdl.enabled = false;
					
					hkidCaseNumTxt.enabled = false;
					pivasRptDispMethodDdl.enabled = false;
					pivasRptItemCatDdl.enabled = false;
					
					itemCodeTxt.enabled = true;
					itemCodeLookupBtn.enabled = false;
					drugKeyCbx.enabled = true;
					manufCodeTxt.enabled = false;
					manufCodeLookupBtn.enabled = false;
					batchNumTxt.enabled = false;
					
					// PivasRptDispMethod, Batch Dispensing
					pivasRptDispMethodDdl.selectedIndex = 2;
					// Drug search
					pivasRptItemCatDdl.selectedIndex = 1;
				}
			}
			
			public function pivasRptDispMethodDdlChangeHandler():void {
				// PivasConsumptionRpt or PivasPatientDispRpt or PivasWorkloadSummaryPatientRpt selected
				if (pivasRptDispMethodDdl.selectedIndex == 0 || pivasRptDispMethodDdl.selectedIndex == 1) {
					satelliteDdl.enabled = true;
				} else { // PivasBatchDispRpt or PivasWorkloadSummaryBatchRpt
					satelliteDdl.selectedIndex = 0;
					satelliteDdl.enabled = false;
				}
			}
			
			private function drugKeyCbxChangeHandler():void {
				itemCodeTxt.setFocus();
				
				// PivasWorkloadSummaryPatientRpt and PivasWorkloadSummaryBatchRpt only supports Drug Search
				if (!(rptTypeDdl.selectedIndex == 3 || rptTypeDdl.selectedIndex == 4)) {
					if ( drugKeyCbx.selected ) {
						pivasRptItemCatDdl.enabled = false;
						pivasRptItemCatDdl.selectedIndex = 1;
					} else {
						pivasRptItemCatDdl.enabled = rptTypeDdl.selectedIndex != 2;
						pivasRptItemCatDdl.selectedIndex = 0;
					}
				}
			}
			
			private function pivasRptRbGroupChangeHandler():void {
				clearErrorString();
				
				if (supplyTimeRb.selected) {
					if (supplyDateFrom.selectedDate == null) {
						supplyDateFrom.text = "";
					}
					if (supplyDateTo.selectedDate == null) {
						supplyDateTo.text = "";
					}
				} else {
					supplyTimeTxt.text = "";
				}
			}	

			//check rule value days for report range
			private function isValidDateRange():Boolean {
				if (supplyDateToRb.selected) { 
					var duration:String = propMap.getValue("report.pivas.criteria.supplyDate.duration");
					
					var durationValue:Number;
					if (parseInt(duration) != 0) {
						durationValue = parseInt(duration);
							
						var dateDiff:Number = supplyDateTo.selectedDate.getTime() - supplyDateFrom.selectedDate.getTime();
						dateDiff = Math.floor(dateDiff/(1000*3600*24)) + 1;
						if (dateDiff > durationValue) {
							var params:Array = new Array(duration);
							// The reporting period cannot exceed [#0] days
							supplyDateFrom.errorString = sysMsgMap.getMessage("0237", params);
							supplyDateFrom.setFocus();
							return false;
						}
					}
				}

				return true;			
			}
			
			private function isValidTimeStr(value:String):Boolean {	
				if ( value.length < 4 ) {
					return false;
				}
				
				var hour:Number = Number( value.substr(0, 2) );
				var minute:Number = Number( value.substr(2, 2) );
				if( hour > 23 || minute > 59 ){
					return false;
				}
				return true;
			}
			
			public function isSatelliteCriteriaVisible():Boolean {
				if (propMap.getValue("pivas.satellite.enabled")=='Y'){
					return true;
				} else {
					return false;
				}
			}

			private function isItemCodeEmpty():Boolean{
				itemCodeTxt.errorString = "";
				if (itemCodeTxt.text == null || StringUtil.trim(itemCodeTxt.text) == "") {
					itemCodeTxt.text = "";
					return true;	
				} else {
					return false;
				}
			}
			
			private function pressEnterShortcutKey(evt:KeyboardEvent):void {
				if (evt.keyCode == Keyboard.ENTER) {
					retrieveHandler();
				}
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( !PopupUtil.isAnyVisiblePopupExists() ) {
					if ( evt.charCode == 52 && evt.ctrlKey ) {	// Ctrl + 4 - Clear
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
					} else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled) {  // Ctrl + 0 - Close
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
				}
			}
			
			private function isItemCodeBtnEnabled():void {
				if ( (rptTypeDdl.selectedIndex != 0) && (StringUtil.trim(itemCodeTxt.text).length > 0) ) {
					itemCodeLookupBtn.enabled = true;
				}else{
					itemCodeLookupBtn.enabled = false;
				}	
			}
			
			private function isManufCodeBtnEnabled():void {
				if (rptTypeDdl.selectedIndex == 1 || rptTypeDdl.selectedIndex == 2) {
					manufCodeLookupBtn.enabled = true;
				}else{
					manufCodeLookupBtn.enabled = false;
				}	
			}
			
			private function validateDateField(evt:FocusEvent):void {
				if (!ExtendedDateFieldValidator.isValidDate(supplyDateFrom) && supplyDateFrom.text != "") {
					clearErrorString();
					// Invalid date format [DDMMYYYY]
					supplyDateFrom.errorString = sysMsgMap.getMessage("0001");	
				}
				
				if (!ExtendedDateFieldValidator.isValidDate(supplyDateTo) && supplyDateTo.text != "") {
					clearErrorString();
					// Invalid date format [DDMMYYYY]
					supplyDateTo.errorString = sysMsgMap.getMessage("0001");
				}
			}

			private function validateSupplyTimeField(evt:FocusEvent):void {
				if (! isValidTimeStr(supplyTimeTxt.text) && supplyTimeTxt.text != "") {
					clearErrorString();
					// Invalid supply time.
					supplyTimeTxt.errorString = sysMsgMap.getMessage("0800");
				}
			}
			
		]]>
	</fx:Script>
	
	<fx:Declarations>
		<s:RadioButtonGroup id="pivasRptRbGroup" change="pivasRptRbGroupChangeHandler()"/>	
	</fx:Declarations>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<fc:Toolbar width="100%" id="pmsToolbar"/>
	<s:VGroup width="100%" height="100%" gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10" mouseEnabled="true">
		<s:Panel id="pivasRptPanel" width="100%" title="Report Criteria" >
			<s:layout>
				<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
			</s:layout>
			
			<s:HGroup verticalAlign="middle">
				<s:Label text="Report Type" verticalAlign="middle" width="100"/>
				<fc:AdvancedDropDownList id="rptTypeDdl" dataProvider="{pivasReportTypeList}" keyUp="pressEnterShortcutKey(event)" enabled="false" 
										 labelField="displayValue" selectedIndex="0" width="283" change="rptTypeDdlChangeHandler()"/>
			</s:HGroup>
			
			<s:HGroup verticalAlign="middle">
				<s:Label text="Supply Date" verticalAlign="middle" width="100"/>
				<fc:ExtendedAbbDateField id="supplyDateFrom" enabled="true" keyUp="pressEnterShortcutKey(event)" 
										 keyFocusChange="validateDateField(event)" mouseFocusChange="validateDateField(event)"/>
				
				<s:Label width="6"/>
				
				<s:RadioButton id="supplyTimeRb" label="Supply Time" groupName="pivasRptRbGroup" width="90" selected="true" click="focusManager.setFocus(supplyTimeTxt)"/>
				<fc:UppercaseTextInput id="supplyTimeTxt" textAlign="left" enabled="{supplyTimeRb.selected}" keyUp="pressEnterShortcutKey(event)" 
									   width="60" maxChars="4" restrict="0-9" keyFocusChange="validateSupplyTimeField(event)" mouseFocusChange="validateSupplyTimeField(event)"/>
				
				<s:Label width="33"/>
				
				<s:RadioButton id="supplyDateToRb" label="To" groupName="pivasRptRbGroup" width="36" selected="true" click="focusManager.setFocus(supplyDateTo)"/>
				<fc:ExtendedAbbDateField id="supplyDateTo" enabled="{supplyDateToRb.selected}" keyUp="pressEnterShortcutKey(event)" 
										 keyFocusChange="validateDateField(event)" mouseFocusChange="validateDateField(event)"/>
				
				<s:Label width="105"/>
				
				<s:Label text="Satellite" verticalAlign="middle" width="80" visible="{isSatelliteCriteriaVisible()}" includeInLayout="{isSatelliteCriteriaVisible()}" />
				<fc:AdvancedDropDownList id="satelliteDdl" dataProvider="{satelliteList}" labelFunction="displayDdlDataValueFunc" labelField="displayValue" enabled="false"
										 visible="{isSatelliteCriteriaVisible()}" includeInLayout="{isSatelliteCriteriaVisible()}" 
										 selectedIndex="0" width="50" keyUp="pressEnterShortcutKey(event)"/>
			</s:HGroup>
			
			<s:HGroup verticalAlign="middle">
				<s:Label text="HKID / Case No." verticalAlign="middle" width="100"/>
				<fc:UppercaseTextInput id="hkidCaseNumTxt" textAlign="left" enabled="false" keyUp="pressEnterShortcutKey(event)" width="108" maxChars="12" />
				
				<s:Label width="169"/>
				
				<s:Label text="Data Source" verticalAlign="middle" width="76"/>
				<fc:AdvancedDropDownList id="pivasRptDispMethodDdl" dataProvider="{pivasRptDispMethodList}" labelField="displayValue" 
										 selectedIndex="0" width="190" change="pivasRptDispMethodDdlChangeHandler()" keyUp="pressEnterShortcutKey(event)"/>
				
				<s:Label width="25"/>
				
				<s:Label text="Item Category" verticalAlign="middle" width="80"/>
				<fc:AdvancedDropDownList id="pivasRptItemCatDdl" dataProvider="{pivasRptItemCatList}" labelField="displayValue" selectedIndex="0" width="168" keyUp="pressEnterShortcutKey(event)"/>	
			</s:HGroup>
			
			<s:HGroup verticalAlign="middle">
				<s:Label text="Item Code" verticalAlign="middle" width="100"/>
				<fc:UppercaseTextInput id="itemCodeTxt" width="73" maxChars="6" keyUp="isItemCodeBtnEnabled();pressEnterShortcutKey(event)"/>
				<fc:LookupButton id="itemCodeLookupBtn" click="showItemLookupPopup(event)" />
				
				<s:Label width="7"/>
				
				<s:CheckBox id="drugKeyCbx" label="Drug Key" change="drugKeyCbxChangeHandler()" />
				
				<s:Label width="80"/>
				
				<s:Label text="Manufacturer" verticalAlign="middle" width="76"/>
				<fc:UppercaseTextInput id="manufCodeTxt" width="73" maxChars="4" keyUp="isManufCodeBtnEnabled();pressEnterShortcutKey(event)"/>
				<fc:LookupButton id="manufCodeLookupBtn" click="showManufCodeLookupPopup(event)" />

				<s:Label width="106"/>
				
				<s:Label text="Batch No." verticalAlign="middle" width="80"/>
				<fc:ExtendedTextInput id="batchNumTxt" width="168" maxChars="20" keyUp="isItemCodeBtnEnabled();pressEnterShortcutKey(event)"/>
			</s:HGroup>
			
		</s:Panel>
		<flexiframe:IFrame id="reportFrame" width="100%" height="100%" overlayDetection="true"
						   loadIndicatorClass="hk.org.ha.view.pms.main.report.LoadingLabel"/>
		<flexiframe:IFrame id="exportFrame" width="0" height="0" overlayDetection="true"
						   loadIndicatorClass="hk.org.ha.view.pms.main.report.LoadingLabel"/>
	</s:VGroup>
</fc:ExtendedNavigatorContent>