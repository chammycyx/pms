package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.LedOperationLog;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.LedOperationLogStatus;
import hk.org.ha.model.pms.udt.machine.IqdsAction;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.checkissue.CheckIssueOrder;
import hk.org.ha.model.pms.vo.machine.IqdsMessageContent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("ledOperationListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LedOperationListServiceBean implements LedOperationListServiceLocal {

	private static final String CHECK_ISSUE_ORDER_CLASS_NAME = CheckIssueOrder.class.getName();
	
	@In
	private Workstore workstore;  
	
	@In
	private Workstation workstation;

	@In
	private OperationProfile activeProfile;  

	@In
	private CheckIssueManagerLocal checkIssueManager;

	@Out(required = false)
	private List<Integer> issueWindowList;
	
	@Out(required = false)
	private List<CheckIssueOrder> ledList;
	
	@Out(required = false)
	private List<LedOperationLog> ledOperationLogList;
	
	@Out(required = false)
	private List<LedOperationLog> ledOperationLogOutstandingList;
	
	private Map<String, LedOperationLog> ledOperationLogOutstandingMap;

	@In
	private IqdsSenderLocal iqdsSender;
		
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public void retrieveIssueWindowList() {		
		
		List<OperationWorkstation> operationWorkstationList = activeProfile.getOperationWorkstationList();
		
		List<Integer> fullIssueWindowList = new ArrayList<Integer>();
		
		for(OperationWorkstation w : operationWorkstationList){
			fullIssueWindowList.add(w.getIssueWindowNum());
		}
		
		HashSet hashSet = new HashSet(fullIssueWindowList);
		issueWindowList = new ArrayList(hashSet);
		Collections.sort(issueWindowList);	
	}
	
	public void sendTicketListToIqds(List<IqdsMessageContent> iqdsMessageContentList, Collection<CheckIssueOrder> ledCheckedList, Integer issueWindowNum) {
		
		for ( CheckIssueOrder checkIssueOrder : ledCheckedList ) {
			DispOrder dispOrder = em.find(DispOrder.class, checkIssueOrder.getDispOrderId());
			if ( dispOrder != null ) {
				dispOrder.setIssueWindowNum(issueWindowNum);
				em.merge(dispOrder);
				em.flush();
			}
		}
		sendTicketToIqds(iqdsMessageContentList);
		retrieveLedList();
	}
	
	public void sendTicketToIqds(List<IqdsMessageContent> iqdsMessageContentList) {
		List<LedOperationLog> newLedOperationLogList = new ArrayList<LedOperationLog>(); 
		
		for (IqdsMessageContent i: iqdsMessageContentList) {
			LedOperationLog ledOperationLog = new LedOperationLog();
			ledOperationLog.setTicketDate(new DateTime().toDate());
			ledOperationLog.setTicketNum(StringUtils.leftPad(String.valueOf(i.getTicketNum()),4,'0'));
			if (i.getIqdsAction().equals(IqdsAction.SendTicketNum)){
				ledOperationLog.setStatus(LedOperationLogStatus.Send);
			}else {
				ledOperationLog.setStatus(LedOperationLogStatus.Withdraw);
			}
			ledOperationLog.setWindowNum(Integer.parseInt(i.getIssueWindowNum()));
			
			newLedOperationLogList.add(ledOperationLog);
			
			i.setTicketNum(StringUtils.leftPad(String.valueOf(i.getTicketNum()),4,'0'));
			i.setHospCode(workstore.getHospCode());
			i.setWorkstoreCode(workstore.getWorkstoreCode());
		}

		iqdsSender.sendTicketToIqds(iqdsMessageContentList);
		
		updateLedOperationLogList(newLedOperationLogList);
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveLedList() {

		ledList = em.createQuery(
				"select new " + CHECK_ISSUE_ORDER_CLASS_NAME + // 20120225 index check : Ticket.workstore,orderType,ticketDate : UI_TICKET_01
				"  (o.id, o.ticket.ticketDate, o.ticket.ticketNum," +
				"  o.issueWindowNum, o.urgentFlag, o.pharmOrder.medOrder.docType," +
				"  o.checkWorkstationCode) " +
				" from DispOrder o" +
				" where o.ticket.workstore = :workstore" +
				" and o.ticket.orderType = :orderType" +
				" and o.ticket.ticketDate = :today " +
				" and o.status = :status" +
				" and o.issueWindowNum = 0")	
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("today", new DateTime().toDate())
				.setParameter("status", DispOrderStatus.Checked)
				.getResultList();
	}
	
	public Integer retrieveIssueWindowNum(){ 
		return checkIssueManager.retrieveIssueWindowNum(workstore, workstation);
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveLedOperationLogList(IqdsAction iqdsAction) {

		ledOperationLogList = em.createQuery(
				"select o from LedOperationLog o"+ // 20140718 index check : LedOperationLog.workstore, ticketDate : I_LED_OPERATION_LOG_01
				" where o.workstore = :workstore" +
				" and o.ticketDate = :today " + 
				" order by o.createDate, o.ticketNum asc")	
				.setParameter("workstore", workstore)
				.setParameter("today", new DateTime().toDate())
				.getResultList();
		
		retrieveLedOperationLogOutstandingList(ledOperationLogList);

	}
	
	private void retrieveLedOperationLogOutstandingList(List<LedOperationLog> ledOperationLogList)
	{
		ledOperationLogOutstandingMap = new HashMap<String, LedOperationLog>();
		
		for ( LedOperationLog ledOperationLog : ledOperationLogList ) {
			if (ledOperationLog.getStatus().equals(LedOperationLogStatus.Send)) {
				ledOperationLogOutstandingMap.put(ledOperationLog.getTicketNum(), ledOperationLog);
			}else {
				if (ledOperationLogOutstandingMap.containsKey(ledOperationLog.getTicketNum())) {
					ledOperationLogOutstandingMap.remove(ledOperationLog.getTicketNum());
				}
			}
		}

		ledOperationLogOutstandingList = new ArrayList<LedOperationLog>(ledOperationLogOutstandingMap.values());
		
		Collections.sort(ledOperationLogOutstandingList, new Comparator<LedOperationLog>() {
			@Override
			public int compare(LedOperationLog o1, LedOperationLog o2) {
				return new CompareToBuilder().append(o1.getCreateDate(), o2.getCreateDate())
				.append(o1.getTicketNum(), o2.getTicketNum())
				.toComparison();
			}
		}); 
	}
	
	public void updateLedOperationLogList(Collection<LedOperationLog> newLedOperationLogList) {

		for (LedOperationLog ledOperationLog : newLedOperationLogList) {
			ledOperationLog.setWorkstore(workstore);
			em.persist(ledOperationLog);					
		}
		em.flush();		
	}
	
	@Remove
	public void destroy() 
	{
		if (issueWindowList != null) {
			issueWindowList = null;
		}
		
		if (ledList != null) {
			ledList = null;
		}
		
		if (ledOperationLogList != null) {
			ledOperationLogList = null;
		}
		
		if (ledOperationLogOutstandingList != null) {
			ledOperationLogOutstandingList = null;
		}

	}
	
}
