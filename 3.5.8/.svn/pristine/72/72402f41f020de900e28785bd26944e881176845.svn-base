<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="100%" height="100%" 
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*">
	
	<fx:Metadata>
		[Name("drugCheckView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[			
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import hk.org.ha.event.pms.main.checkissue.mp.RetrieveCheckDeliveryEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.RetrieveDrugCheckViewListEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.UpdateDeliveryStatusToCheckedEvent;
			import hk.org.ha.event.pms.main.checkissue.mp.popup.ShowDrugCheckDeleteDelinkPopupEvent;
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.Window;
			import hk.org.ha.fmk.pms.flex.utils.BarcodeReader;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.biz.checkissue.mp.CheckIssueViewUtil;
			import hk.org.ha.model.pms.persistence.corp.Workstore;
			import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestAlert;
			import hk.org.ha.model.pms.udt.medprofile.PrintMode;
			import hk.org.ha.model.pms.vo.checkissue.mp.CheckDelivery;
			import hk.org.ha.model.pms.vo.checkissue.mp.CheckDeliveryInfo;
			import hk.org.ha.model.pms.vo.checkissue.mp.DeliveryProblemItem;
			import hk.org.ha.model.pms.vo.checkissue.mp.DrugCheckViewListSummary;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			[In]
			public var window:Window;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In][Bindable]
			public var checkDelivery:CheckDelivery;
			
			[Bindable]
			public var drugCheckViewListSummary:DrugCheckViewListSummary;
			
			[Bindable]
			public var clearFlag:Boolean = false;
			
			
			[In] [Bindable]
			public var workstore:Workstore;
			
			[Bindable]
			private var printRptFlag:Boolean=false;
			
			[In]
			public var propMap:PropMap;
			
			private static var LABEL_BARCODESUBMIT_DELAY:String = "label.barcodeSubmit.delay";
			
			private var onShowRefreshList:Boolean = true;
			
			[Bindable]
			private var processingFlag:Boolean = false;
			
			// for frontend refresh call only
			private var refreshingFlag:Boolean = false;
			
			// declare to fix flex exception when save for checking
			private var deliveryRequestAlert:DeliveryRequestAlert;
			
			public override function onShowLater():void {
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					var tomorrowDate:Date = new Date();
					tomorrowDate.setDate(tomorrowDate.getDate() + 1);
					batchDate.disabledRanges = [{rangeStart:tomorrowDate}]; 
					
					clearHandler();
				}
				
				if ( onShowRefreshList ) {
					refreshHandler();
					onShowRefreshList = false
				}
				
				callLater(disableMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showDrugCheckShortcutKeyHandler);
			}
			
			
			private function showDrugCheckShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("DrugCheckView"));
			}
			
			private function disableMenuBarCloseButton():void {
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
			}
			
			private function closeHandler():void {
				if (checkDelivery != null && checkDelivery.deliveryProblemItemList != null) {
					for each ( var deliveryProblemItem:DeliveryProblemItem in checkDelivery.deliveryProblemItemList ) {
						if ( deliveryProblemItem.markUnLink || deliveryProblemItem.markDelete ) {
							showYesNoButtonSystemMessage("0655", unsavedCloseHandler);
							return;
						}
					}
				}
				this.dispatchEvent(new CloseActiveWindowEvent());
				confirmClear();	
				onShowRefreshList = true;
			}
			
			private function unsavedCloseHandler(evt:MouseEvent):void{
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				confirmClear();	
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			public override function onHide():void 
			{
				stage.focus = null;
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			
			
			private function retrieveHandler():void{				
				processingFlag = true;
				if ( searchCriteriaValidation() ) {
					dispatchEvent( new RetrieveCheckDeliveryEvent(batchDate.selectedDate, batchCodeTextInput.text.substring(2,6)) );
				} else {
					processingFlag = false;
					//PMS-3441
					refreshHandler();
				}
			}
			
			private function clearErrorString():void{
				batchDate.errorString = "";
				batchCodeTextInput.errorString = "";
			}
			
			
			private function searchCriteriaValidation():Boolean {
				clearErrorString();
				return CheckIssueViewUtil.searchCriteriaValid(batchDate,batchCodeTextInput, sysMsgMap, workstore);
			}
			
			
			private function saveHandler():void{
				if ( processingFlag ) {
					return;
				}
				processingFlag = true;
				printRptFlag = false;
				setSaveMsg(printRptFlag);
			}
			
			
			private function savePrintHandler():void{
				if ( processingFlag ) {
					return;
				}
				processingFlag = true;
				printRptFlag = true;
				setSaveMsg(printRptFlag);
			}
			
			
			private function UpdateDeliveryStatusToCheckedHandler(evt:MouseEvent):void{
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);			
				dispatchEvent( new UpdateDeliveryStatusToCheckedEvent(checkDelivery, printRptFlag) ) ;
			}
			
			//			
			public function updateDeliveryStatusToCheckedResultAfterHandler(updateDrugCheckViewListSummary:DrugCheckViewListSummary):void{
				confirmClear();
				setDrugCheckViewListSummary(updateDrugCheckViewListSummary);
			}
			
			
			public function clearHandler():void{		
				
				if (checkDelivery != null && checkDelivery.deliveryProblemItemList != null) {
					for each ( var deliveryProblemItem:DeliveryProblemItem in checkDelivery.deliveryProblemItemList ) {
						if ( deliveryProblemItem.markUnLink || deliveryProblemItem.markDelete ) {
							showYesNoButtonSystemMessage("0655", unsavedYesHandler);
							return;
						}
					}
				}
				confirmClear();
			}
			
			private function unsavedYesHandler(evt:MouseEvent):void{
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				confirmClear();
			}
			
			private function confirmClear():void{
				clearFlag = false;
				
				clearErrorString();
				printRptFlag = false;
				pmsToolbar.retrieveButton.enabled = true;			
				
				trxIdTextInput.enabled = false;
				trxIdTextInput.text = "";
				batchDate.enabled = true;
				saveButton.enabled = false;
				savePrintButton.enabled = false;
				batchDate.selectedDate = new Date();
				batchDate.validateNow();
				batchDate.setFocus();
				batchCodeTextInput.enabled = true;
				checkButton.enabled = true;
				batchCodeTextInput.text = "";
				callLater(function():void {
					batchCodeTextInput.setFocus();
					processingFlag = false;
				});
				
				this.callLater(clearproblemItemGridDataForDrugCheckSource);
			}
			
			private function clearproblemItemGridDataForDrugCheckSource():void{
				ctx.checkDelivery = null;
				checkDelivery = null;
				problemItemGridDataForDrugCheck.source = null;
			}
			
			
			private function refreshHandler():void {
				if (!refreshingFlag) {
					setRefreshingFlag(true);
					dispatchEvent(new RetrieveDrugCheckViewListEvent());
				}
			}
			
			public function setRefreshingFlag(inRefreshingFlag:Boolean):void {
				refreshingFlag = inRefreshingFlag;
			}
			
			public function setDrugCheckViewListSummary(inDrugCheckViewListSummary:DrugCheckViewListSummary):void {
				drugCheckViewListSummary = inDrugCheckViewListSummary;
			}
			
			public function retrieveCheckOrderResult():void{				
				showSystemMessage("0654", "okButtonOnly", null, exceptionYesHandler);
				
				problemItemGridDataForDrugCheck.source=checkDelivery.deliveryProblemItemList;
				
				pmsToolbar.retrieveButton.enabled = false;
				saveButton.enabled = true;
				savePrintButton.enabled = true;
				batchDate.enabled = false;
				batchDate.selectedDate = checkDelivery.batchDate;
				batchCodeTextInput.enabled = false;
				checkButton.enabled = false;
				var dateStringCheck:String = checkDelivery.batchDate.toDateString().substring(8,10);
				if(StringUtil.trim(dateStringCheck).length == 1){
					dateStringCheck = '0'+ StringUtil.trim(dateStringCheck)
				}
				
				batchCodeTextInput.text = dateStringCheck +checkDelivery.batchNum;
				
				this.callLater(displayProblemItemGridDataForDrugCheckSource); 
				processingFlag = false;
			}
			
			private function displayProblemItemGridDataForDrugCheckSource():void{
				clearFlag = true;
			}
			
			private function exceptionYesHandler(evt:MouseEvent):void{
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				this.callLater(setFocusToTrxIdTextInput);
			}
			
			
			private function setFocusToTrxIdTextInput():void{
				trxIdTextInput.enabled = true;
				trxIdTextInput.setFocus();
			}
			
			
			private var currentDrugItem:DeliveryProblemItem;
			
			private function getTrxId(mpDispLabelXml:String):void{
				currentDrugItem = null;
				mpDispLabelXml = mpDispLabelXml.toUpperCase();
				if(mpDispLabelXml.length > 14 && mpDispLabelXml.search("<E>") > -1 ){
					var fromIndex:int = mpDispLabelXml.indexOf("<E>");
					var toIndex:int = mpDispLabelXml.indexOf("</E>");
					
					if ( fromIndex > -1 ) {
						trxIdTextInput.text = mpDispLabelXml.substring(fromIndex+3, toIndex);
					}
				}
				
				if(isNaN(Number(trxIdTextInput.text))){
					showSystemMessage("0261", "okButtonOnly");
				}else{
					currentDrugItem = getDeliveryProblemItemByTrxId(Number(trxIdTextInput.text));
					
					if ( currentDrugItem == null ) {
						//PMS-3441
						showSystemMessage("0606", "okButtonOnly" );
						//PMS-2780
						trxIdTextInput.text = "";
					} else {
						if ( !currentDrugItem.markUnLink ) {
							dispatchEvent( new ShowDrugCheckDeleteDelinkPopupEvent()) ;
						} else {
							showSystemMessage("0216", "okButtonOnly" );//Drug item is already unlink.
						}
					}
				}
			}
			
			public function markUnlinkHandler():void{
				currentDrugItem.markUnLink = true;
				trxIdTextInput.text = "";
				trxIdTextInput.setFocus();
			}
			
			public function markDeleteHandler():void{
				currentDrugItem.uiUnlinkFlag = true;
				currentDrugItem.markUnLink = true;
				currentDrugItem.markDelete = true;
				trxIdTextInput.text = "";
				trxIdTextInput.setFocus();
			}
			
			private function getDeliveryProblemItemByTrxId(trxId:Number):DeliveryProblemItem {
				for each ( var deliveryProblemItem:DeliveryProblemItem in checkDelivery.deliveryProblemItemList ) {
					if ( deliveryProblemItem.deliveryItemId == trxId ) {
						return deliveryProblemItem;
					}
				}
				return null;
			}
			
			private function setSaveMsg(isPrintButton:Boolean):void{
				for each ( var deliveryProblemItem:DeliveryProblemItem in checkDelivery.deliveryProblemItemList ) {
					if ( !deliveryProblemItem.markUnLink && !deliveryProblemItem.markDelete) {
						if ( isPrintButton ) {		
							//new
							showSystemMessage("0657", "yesNoButton", null, UpdateDeliveryStatusToCheckedHandler); 
						} else {
							
							//213
							showSystemMessage("0213", "yesNoButton", null, UpdateDeliveryStatusToCheckedHandler); 
						}
						return;
					}
				}
				if ( isPrintButton ) {
					showSystemMessage("0214", "yesNoButton", null, UpdateDeliveryStatusToCheckedHandler); 
				} else {
					showSystemMessage("0304", "yesNoButton", null, UpdateDeliveryStatusToCheckedHandler); 
				}
			}
			
			public function showYesNoButtonSystemMessage(errorCode:String, yesHandler:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				msgProp.setYesNoButton = true;
				msgProp.setYesDefaultButton = false;
				msgProp.setNoDefaultButton = true;
				if ( yesHandler != null ) {
					msgProp.yesHandler = yesHandler;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			
			public function showSystemMessage(errorCode:String, buttonType:String, paramList:Array=null, yesHandler:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if ( buttonType == "yesNoButton") {
					msgProp.setYesNoButton = true;
					if ( yesHandler != null ) {
						msgProp.yesHandler = yesHandler;
					}
					msgProp.noHandler = removePopUpHandler;
				} else {
					msgProp.setOkButtonOnly = true;
					
					if ( yesHandler != null ) {
						msgProp.okHandler = yesHandler;
					}else{
						msgProp.okHandler = removePopUpHandler;
					}
				}
				
				if ( paramList != null ) {
					msgProp.messageParams = paramList;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function removePopUpHandler(mouseEvent:MouseEvent):void {
				PopUpManager.removePopUp(((mouseEvent.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				processingFlag = false;
			}
			
			//-------------shortcut key function-----------------
			public function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( evt.charCode == 52 && evt.ctrlKey && pmsToolbar.clearButton.enabled && !PopupUtil.isAnyVisiblePopupExists() ){	
					dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
				} else if ( evt.charCode == 55 && evt.ctrlKey && saveButton.enabled && !PopupUtil.isAnyVisiblePopupExists() ) {
					dispatchEvent(new ShortcutKeyEvent(saveButton));
				} else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled && !PopupUtil.isAnyVisiblePopupExists() ){	//ctrl + 0 - close
					dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
				}
			}
			
			private function batchDateKeyBoardFunc(event:KeyboardEvent):void{
				if( event.keyCode==13 && !PopupUtil.isAnyVisiblePopupExists() ){
					readBarcodeHandler(event);
				}
			}
			
			//---------DataGrid label function----------
			private function updateDateLabelFunc(checkDeliveryInfo:CheckDeliveryInfo, column:DataGridColumn):String {
				return dateTimeFormatter.format(checkDeliveryInfo.checkDate);
			}
			
			private function readyForCheckUpdateDateLabelFunc(checkDeliveryInfo:CheckDeliveryInfo, column:DataGridColumn):String {
				return dateTimeFormatter.format(checkDeliveryInfo.updateDate);
			}
			
			public function batchModeLabelFunc(checkDeliveryInfo:CheckDeliveryInfo, column:DataGridColumn):String {
				if ( checkDeliveryInfo.printMode == PrintMode.Batch ) {
					return "Y";
				} else {
					return "N";
				}
			}
			
			public function wardBedNumLabelFunc(obj:*, column:DataGridColumn):String {
				var str:String = "";
				var deliveryProblemItem:DeliveryProblemItem = obj as DeliveryProblemItem;
				
				if ( deliveryProblemItem.wardCode != null ) {
					str = deliveryProblemItem.wardCode;
				}
				str += " / ";
				if ( deliveryProblemItem.bedNum != null ) {
					str += deliveryProblemItem.bedNum;
				}
				if ( str == " / " ) {
					str = "";
				}
				return str;
			}
			
			public function qtyLabelFunc(obj:*,column:DataGridColumn):String {
				var deliveryProblemItem:DeliveryProblemItem = obj as DeliveryProblemItem;
				
				if(deliveryProblemItem.baseUnit != null && deliveryProblemItem.baseUnit != ""){
					return deliveryProblemItem.issueQty+" "+deliveryProblemItem.baseUnit;
				}else if ( deliveryProblemItem.issueQty == 0 ) {
					return "";
				} else {
					return deliveryProblemItem.issueQty+" ";
				}
			}
			
			private function validateDateField(evt:Event):void {
				batchDate.errorString = null;
				if ( !ExtendedDateFieldValidator.isValidDate(batchDate) ) {
					
					if(  batchDate.selectedDate == null){
						batchDate.errorString = sysMsgMap.getMessage("0001");
						return; 
					}
				}else {
					var todayDateVal : Date = new Date();
					if(CheckIssueViewUtil.compareDate( batchDate.selectedDate, todayDateVal ) > 0 ){
						batchDate.errorString = sysMsgMap.getMessage("0649");
						return;
					}
				}
			}
			
			private function markUnLinkCbxFunction(data:DeliveryProblemItem, event:Event):void{
				data.markUnLink = event.currentTarget
			}
			
			public function batchNumLabelFunc(batchNum:String, batchDate:Date):String{
				var dayPerfix : String = batchDate.date.toString();
				if(dayPerfix.length == 1){
					dayPerfix = '0' + dayPerfix;
				}
				return dayPerfix + batchNum;
			}
			
			public function batchNumStyleFunc(batchDate:Date):String{
				
				var toDayDate:Date = new Date();
				if ( !(batchDate.fullYear == toDayDate.fullYear && batchDate.month == toDayDate.month && batchDate.date ==  toDayDate.date) ) {
					return 'drugCheckLabelStyle';
					
				}else{
					return '';
				}
			}
			
			public function disableUnlinkSelectionFormDelete(data:Object, selected:Boolean):void {
				
				if(selected){
					DeliveryProblemItem(data).markUnLink = selected;
					DeliveryProblemItem(data).uiUnlinkFlag= true;
				}else{
					DeliveryProblemItem(data).uiUnlinkFlag= false;
				}
				
				
			}
			public function checkEnableUnlink(keepRecorded:Boolean, unlinked:Boolean, pivas:Boolean):Boolean {
				if (keepRecorded || unlinked || pivas) {
					return false;
				} else {
					return true;
				}	
			}
			
			public function checkEnableDelete(dayEndProcessed:Boolean, deleted:Boolean):Boolean {
				if (dayEndProcessed || deleted) {
					return false;
				} else {
					return true;
				}	
			}
			
			public function readBarcodeHandler(evt:KeyboardEvent):void{
				if (evt.keyCode == Keyboard.ENTER && !processingFlag) {
					processingFlag = true;
					BarcodeReader.scanBarcode(evt, propMap.getValueAsInteger(LABEL_BARCODESUBMIT_DELAY), retrieveHandler);
				}
			}
			
			public function checkButtonClickHandler():void {
				if (!processingFlag) {
					processingFlag = true;
					retrieveHandler()
				}
			}
			
			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	<fc:Toolbar id="pmsToolbar" width="100%"/>
	<s:HGroup width="100%" height="100%" gap="10" paddingBottom="10" paddingLeft="10" paddingRight="10" paddingTop="10">
		<s:VGroup width="28%" height="100%">
			<s:Panel title="Search Criteria" width="100%" height="100">
				<s:layout>
					<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
				</s:layout>
				<s:HGroup width="100%" verticalAlign="middle">
					<s:Label text="Batch Date" width="80"/>
					<fc:ExtendedAbbDateField id="batchDate" selectedDate="{new Date()}" maxChars="22" 
											 focusOut="validateDateField(event)"
											 creationComplete="batchDate.addEventListener(KeyboardEvent.KEY_UP, batchDateKeyBoardFunc);"/>
				</s:HGroup>
				<s:HGroup width="100%" verticalAlign="middle">
					<s:Label text="Batch Code" width="80"/>
					<s:TextInput id="batchCodeTextInput" keyUp="readBarcodeHandler(event)" width="85"/>
					<s:Button id="checkButton" label="Check" click="checkButtonClickHandler()"/>
				</s:HGroup>
			</s:Panel>
			<s:VGroup gap="0" width="100%" height="100%">
				<s:Panel title="Ready for Checking List" width="100%" height="45%">
					<fc:ExtendedDataGrid id="readyForCheckGrid" dataProvider="{drugCheckViewListSummary.readyForCheckDeliveryList}" width="100%" height="100%" 
										 horizontalScrollPolicy="auto" draggableColumns="false" resizableColumns="false" sortableColumns="false">
						<fc:columns>
							<mx:DataGridColumn headerText="Batch Code" dataField="batchNum" headerWordWrap="true" width="{readyForCheckGrid.width*0.187}" >
								<mx:itemRenderer>
									<fx:Component>
										<s:MXDataGridItemRenderer autoDrawBackground="false">
											<s:layout>
												<s:HorizontalLayout verticalAlign="middle" paddingLeft="5"  />
											</s:layout>
											<s:Label text="{outerDocument.batchNumLabelFunc(data.batchNum,data.batchDate)}" width="100%" styleName="{outerDocument.batchNumStyleFunc(data.batchDate)}"/>
										</s:MXDataGridItemRenderer>
									</fx:Component>
								</mx:itemRenderer>
							</mx:DataGridColumn>
							<mx:DataGridColumn headerText="Batch Mode" dataField="printMode" headerWordWrap="true" labelFunction="batchModeLabelFunc" width="{readyForCheckGrid.width*0.187}"/>
							<mx:DataGridColumn headerText="Ward" dataField="wardCode" headerWordWrap="true" width="{readyForCheckGrid.width*0.161}"/>
							<mx:DataGridColumn headerText="Update Date / Time" dataField="updateDate" labelFunction="readyForCheckUpdateDateLabelFunc" headerWordWrap="true"/>
						</fc:columns>
					</fc:ExtendedDataGrid>
				</s:Panel>
				<s:Panel title="Drug Checking History" width="100%" height="55%">
					<fc:ExtendedDataGrid id="drugCheckHistory" dataProvider="{drugCheckViewListSummary.checkedDeliveryList}" width="100%" height="100%" 
										 horizontalScrollPolicy="auto" draggableColumns="false" resizableColumns="false" sortableColumns="false">
						<fc:columns>
							<mx:DataGridColumn headerText="Batch Code" dataField="batchNum" headerWordWrap="true" width="{drugCheckHistory.width*0.187}" >
								<mx:itemRenderer>
									<fx:Component>
										<s:MXDataGridItemRenderer autoDrawBackground="false">
											<s:layout>
												<s:HorizontalLayout verticalAlign="middle" paddingLeft="5"  />
											</s:layout>
											<s:Label text="{outerDocument.batchNumLabelFunc(data.batchNum,data.batchDate)}" width="100%" styleName="{outerDocument.batchNumStyleFunc(data.batchDate)}"/>
										</s:MXDataGridItemRenderer>
									</fx:Component>
								</mx:itemRenderer>
							</mx:DataGridColumn>
							<mx:DataGridColumn headerText="Batch Mode" dataField="printMode" headerWordWrap="true" labelFunction="batchModeLabelFunc" width="{drugCheckHistory.width*0.187}"/>
							<mx:DataGridColumn headerText="Ward" dataField="wardCode" headerWordWrap="true" width="{drugCheckHistory.width*0.161}"/>
							<mx:DataGridColumn headerText="Update Date / Time" dataField="checkDate" labelFunction="updateDateLabelFunc" headerWordWrap="true"/>
						</fc:columns>
					</fc:ExtendedDataGrid>
					<s:controlBarContent>
						<s:HGroup width="100%">
							<s:Label text=" " width="80%"/>
							<s:Button id="refresh" label="Refresh"  click="refreshHandler()"/>
						</s:HGroup>
					</s:controlBarContent>
				</s:Panel>
			</s:VGroup>
		</s:VGroup>
		<s:VGroup width="72%" height="100%">
			<s:HGroup width="100%" height="70">
				<s:Panel title="Label Delink" width="500" height="100%" id="delinkPanel" styleName="{clearFlag ? 'mpCheckExceptionIdentifiedPanelStyle' :''}">
					<s:layout>
						<s:HorizontalLayout verticalAlign="middle" horizontalAlign="left" gap="10" paddingBottom="10" paddingLeft="10" paddingRight="10" paddingTop="10"/>
					</s:layout>
					<s:Label text="Transaction ID &#xd;(2D code on Drug Label)"/>
					<s:TextInput id="trxIdTextInput" enter="getTrxId(trxIdTextInput.text)" width="100%"/>
				</s:Panel>
			</s:HGroup>
			<s:Panel title="Problems Identified from Drug Checking" width="100%" height="500" id="exceptionPanel" styleName="{clearFlag ? 'mpCheckExceptionIdentifiedPanelStyle' :''}" >
				
				<fc:ExtendedAdvancedDataGrid useRollOver="false" id="problemItemGridForDrugCheck" 
											 width="100%" height="100%" 
											 draggableColumns="false" resizableColumns="false" horizontalScrollPolicy="auto" sortExpertMode="true"
											 variableRowHeight="true" displayItemsExpanded="true" sortableColumns="false" editable="false"
											 disclosureClosedIcon="{null}"  paddingTop="0"
											 disclosureOpenIcon="{null}"  paddingBottom="0"
											 folderClosedIcon="{null}" folderOpenIcon="{null}" defaultLeafIcon="{null}">
					
					<fc:dataProvider>
						<mx:HierarchicalData id="problemItemGridDataForDrugCheck" childrenField="deliveryProblemItem"/>
					</fc:dataProvider>
					<fc:groupedColumns>
						
						<mx:AdvancedDataGridColumn  id="deleteColumn"  headerText="Label Delete" dataField="markDelete"  resizable="true"  headerWordWrap="true" width="{problemItemGridForDrugCheck.width*0.07}"/>
						<mx:AdvancedDataGridColumn  id="unLinkColumn" headerText="Label Delink" dataField="markUnLink" resizable="true"  headerWordWrap="true" width="{problemItemGridForDrugCheck.width*0.07}"/> 
						<mx:AdvancedDataGridColumnGroup headerText="Case No." width="{problemItemGridForDrugCheck.width*0.157}" headerWordWrap="true">	
							<mx:AdvancedDataGridColumn id="wardBedNumColumn"  headerText="Ward / Bed" resizable="true"  headerWordWrap="true" width="{problemItemGridForDrugCheck.width*0.157}" />
						</mx:AdvancedDataGridColumnGroup>
						<mx:AdvancedDataGridColumnGroup  headerText="Patient Name" resizable="false" width="180" headerWordWrap="true">
							<mx:AdvancedDataGridColumn id="itemCodeColumn"   headerText="Item Code" width="70"  headerWordWrap="true" />
							<mx:AdvancedDataGridColumn id="dispQtyColumn"   headerText="Disp. Qty"   width="110" headerWordWrap="true"  />
						</mx:AdvancedDataGridColumnGroup>
						
						<mx:AdvancedDataGridColumn  id="drugColumn" headerText="Drug" resizable="true"  headerWordWrap="true" width="{problemItemGridForDrugCheck.width*0.23}" wordWrap="true" />
						<mx:AdvancedDataGridColumn  id="exceptionColumn" headerText="Exception" resizable="true"   headerWordWrap="true" wordWrap="true" width="{problemItemGridForDrugCheck.width*0.207}" />
					</fc:groupedColumns>
					<fc:rendererProviders>
						<mx:AdvancedDataGridRendererProvider column="{deleteColumn}" >
							
							<mx:renderer>
								<fx:Component>
									<s:MXAdvancedDataGridItemRenderer >
										<s:SkinnableContainer width="100%" height="100%" styleName="{ data.keepRecorded ? 'mpCheckKeepRecordDayEndProcessStyle' : ''}">
											<s:layout>
												<s:HorizontalLayout horizontalAlign="center"/>
											</s:layout>
											<s:VGroup>
												<s:CheckBox id="markDeleteCbx" selected="@{data.markDelete}"  enabled="{outerDocument.checkEnableDelete(data.dayendProcess, data.uiMarkDeleteFlag) }" change="outerDocument.disableUnlinkSelectionFormDelete(data, event.target.selected)"  />
											</s:VGroup>
										</s:SkinnableContainer>
									</s:MXAdvancedDataGridItemRenderer>
								</fx:Component>
							</mx:renderer>
							
						</mx:AdvancedDataGridRendererProvider>
						<mx:AdvancedDataGridRendererProvider column="{unLinkColumn}" >
							
							<mx:renderer>
								<fx:Component>
									<s:MXAdvancedDataGridItemRenderer  >
										<s:SkinnableContainer width="100%" height="100%" styleName="{ data.keepRecorded ? 'mpCheckKeepRecordDayEndProcessStyle' : ''}">
											<s:layout>
												<s:HorizontalLayout horizontalAlign="center"/>
											</s:layout>
											<s:CheckBox id="markUnLinkCbx" selected="@{data.markUnLink}" enabled="{outerDocument.checkEnableUnlink(data.keepRecorded, data.uiUnlinkFlag, data.drugName == null)}"  />
										</s:SkinnableContainer>
									</s:MXAdvancedDataGridItemRenderer>
								</fx:Component>
							</mx:renderer>
							
						</mx:AdvancedDataGridRendererProvider> 
						<mx:AdvancedDataGridRendererProvider column="{wardBedNumColumn}" columnSpan="2"  >
							
							<mx:renderer>
								<fx:Component>
									<s:MXAdvancedDataGridItemRenderer >
										<s:SkinnableContainer width="100%" height="100%" styleName="{ data.keepRecorded ? 'mpCheckKeepRecordDayEndProcessStyle' : ''}">
											<s:layout>
												<s:VerticalLayout paddingLeft="5" paddingRight="5" paddingTop="5" paddingBottom="5" gap="10"/>
											</s:layout>
											<s:Label text="{data.caseNum}" paddingLeft="5" paddingRight="5" />
											<s:VGroup>
												<s:Label text="{outerDocument.wardBedNumLabelFunc(data,null)}" paddingLeft="5" paddingRight="5"/>
											</s:VGroup>
										</s:SkinnableContainer>
									</s:MXAdvancedDataGridItemRenderer>
								</fx:Component>
							</mx:renderer>
							
						</mx:AdvancedDataGridRendererProvider>
						<mx:AdvancedDataGridRendererProvider column="{itemCodeColumn}" columnSpan="2"  >
							<mx:renderer>
								<fx:Component>
									<s:MXAdvancedDataGridItemRenderer autoDrawBackground="false" >
										<s:SkinnableContainer width="100%" height="100%" styleName="{ data.keepRecorded ? 'mpCheckKeepRecordDayEndProcessStyle' : ''}">
											<s:layout>
												<s:VerticalLayout paddingLeft="5" paddingRight="5" paddingTop="5" paddingBottom="5" gap="10"/>
											</s:layout>
											<s:Label text="{data.patientName}" showTruncationTip="true" maxDisplayedLines="1" width="180" fontFamily="{data.havePatientChiName ?'HA_MingLiu':'Arial'}"/>
											<s:HGroup>
												<s:Label text="{data.itemCode}"   width="70"/>
												<s:Label text="{outerDocument.qtyLabelFunc(data,null)}" width="110"/>
											</s:HGroup>
										</s:SkinnableContainer>
									</s:MXAdvancedDataGridItemRenderer>
								</fx:Component>
							</mx:renderer>
							
						</mx:AdvancedDataGridRendererProvider>
						
						<mx:AdvancedDataGridRendererProvider column="{drugColumn}"  >
							
							<mx:renderer>								
								<fx:Component>
									<s:MXAdvancedDataGridItemRenderer >
										<fx:Script>
											<![CDATA[
												import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
											]]>
										</fx:Script>
										<s:SkinnableContainer width="100%" height="100%" styleName="{ data.keepRecorded ? 'mpCheckKeepRecordDayEndProcessStyle' : ''}">
											<s:VGroup id="vgroup">
												<s:RichText width="{outerDocument.drugColumn.width}" paddingLeft="5" paddingBottom="3" paddingTop="3" paddingRight="5"
															visible="{data.itemDescTlf != null}" includeInLayout="{data.itemDescTlf != null}" textFlow="{MedProfileUtils.importFromTlfString(data.itemDescTlf)}"/>
												<s:RichText width="{outerDocument.drugColumn.width}" paddingLeft="5" paddingBottom="3" paddingTop="3" paddingRight="5"
															visible="{data.itemDescTlf == null}" includeInLayout="{data.itemDescTlf == null}">
													<s:textFlow>
														<s:TextFlow>
															<fx:Script>
																<![CDATA[
																	import flash.text.engine.BreakOpportunity;
																	import hk.org.ha.fmk.pms.flex.utils.WordUtil;
																]]>
															</fx:Script>
															<s:span breakOpportunity="{BreakOpportunity.AUTO}">{WordUtil.capitalize(data.drugName)}</s:span>
															<s:span> </s:span>
															<s:span breakOpportunity="{BreakOpportunity.AUTO}">{WordUtil.capitalize(data.formLabelDesc)}</s:span>
															<s:span> </s:span>
															<s:span breakOpportunity="{BreakOpportunity.AUTO}">{data.strength.toLowerCase()}</s:span>
															<s:span> </s:span>
															<s:span breakOpportunity="{BreakOpportunity.AUTO}">{data.volumeText.toLowerCase()}</s:span>
														</s:TextFlow>
													</s:textFlow>
												</s:RichText>
											</s:VGroup>
										</s:SkinnableContainer>
									</s:MXAdvancedDataGridItemRenderer>
								</fx:Component>
							</mx:renderer>
							
						</mx:AdvancedDataGridRendererProvider>
						<mx:AdvancedDataGridRendererProvider column="{exceptionColumn}"  >
							
							<mx:renderer>
								<fx:Component>
									<s:MXAdvancedDataGridItemRenderer >
										<s:SkinnableContainer width="100%" height="100%"  styleName="{ data.keepRecorded ? 'mpCheckKeepRecordDayEndProcessStyle' : ''}">
											<s:layout>
												<s:VerticalLayout />
											</s:layout>
											<s:VGroup >
												<s:RichText id="exceptionRichText" width="{outerDocument.exceptionColumn.width}" paddingLeft="5" paddingBottom="3" paddingTop="3" paddingRight="5" >
													<s:textFlow>
														<s:TextFlow id="textflow" >
															<fx:Script>
																<![CDATA[
																	import flash.text.engine.BreakOpportunity;
																	import hk.org.ha.fmk.pms.flex.utils.WordUtil;
																]]>
															</fx:Script>
															<s:span>{data.exception}</s:span>
														</s:TextFlow>
													</s:textFlow>
												</s:RichText>
											</s:VGroup>
										</s:SkinnableContainer>
									</s:MXAdvancedDataGridItemRenderer>
								</fx:Component>
							</mx:renderer>
						</mx:AdvancedDataGridRendererProvider>
					</fc:rendererProviders>
				</fc:ExtendedAdvancedDataGrid>
				
			</s:Panel>
			<s:Label fontStyle="italic" fontWeight="bold" text="* IPAS ward is shown, no IPMOE specialty mapping record can be found."/>
			<s:HGroup width="100%" horizontalAlign="right">
				<s:Button id="savePrintButton" label="Save for Checking &amp; Print Updated Report" click="savePrintHandler()" enabled="false"/>
				<s:Label width="10"/>
				<s:Button id="saveButton" label="Save for Checking" click="saveHandler()" enabled="false"/>
			</s:HGroup>
			
		</s:VGroup>
	</s:HGroup>
</fc:ExtendedNavigatorContent>