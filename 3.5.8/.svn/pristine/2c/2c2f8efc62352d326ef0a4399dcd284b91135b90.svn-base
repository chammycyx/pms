<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent xmlns:fx="http://ns.adobe.com/mxml/2009"
							 xmlns:s="library://ns.adobe.com/flex/spark"
							 xmlns:mx="library://ns.adobe.com/flex/mx"
							 xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
							 xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
							 xmlns="hk.org.ha.view.pms.main.dashboard.mp.*">
	
	<fx:Metadata>
		[Name("medProfileDashboardView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.messaging.events.MessageEvent;
			import mx.messaging.messages.AsyncMessage;
			
			import hk.org.ha.event.pms.main.dashboard.mp.CountAllDashboardItems;
			import hk.org.ha.event.pms.main.dashboard.mp.RecalculateDashboardRelatedStatEvent;
			import hk.org.ha.event.pms.main.sys.RetrieveCurrentServerTimeEvent;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.model.pms.persistence.corp.Workstore;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.view.pms.main.medprofile.MedProfileConstants;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			
			import org.granite.gravity.Consumer;
			
			public static const ITEM_STATUS_LABEL_WIDTH:Number = 150;
			
			public static const OTHER_LABEL_WIDTH:Number = 220;
			
			private var consumer:Consumer = null;
			
			[In] [Bindable]
			public var permissionMap:PropMap;
			
			[In] [Bindable]
			public var workstore:Workstore;

			[In] [Bindable]
			public var dashboardUpdateDate:Date;
			
			[In] [Bindable]
			public var newCount:Number;
			
			[In] [Bindable]
			public var verifyCount:Number;
			
			[In] [Bindable]
			public var newNormalCount:Number;
			
			[In] [Bindable]
			public var newUrgentCount:Number;
			
			[In] [Bindable]
			public var newPendingCount:Number;
			
			[In] [Bindable]
			public var newOverridePendingCount:Number;
			
			[In] [Bindable]
			public var newSuspendCount:Number;
			
			[In] [Bindable]
			public var verifyNormalCount:Number;
			
			[In] [Bindable]
			public var verifyUrgentCount:Number;
			
			[In] [Bindable]
			public var verifyPendingCount:Number;
			
			[In] [Bindable]
			public var verifyOverridePendingCount:Number;
			
			[In] [Bindable]
			public var verifySuspendCount:Number;
			
			[In] [Bindable]
			public var exceptionCount:Number;
			
			[In] [Bindable]
			public var followUpCount:Number;
			
			[In] [Bindable]
			public var dashboardOverdueNewCount:Number;
			
			[In] [Bindable]
			public var dashboardOverdueRefillCount:Number;
			
			[In] [Bindable]
			public var dashboardForecastNewCount:Number;
			
			[In] [Bindable]
			public var dashboardForecastRefillCount:Number;
			
			[In] [Bindable]
			public var paidCount:Number;
			
			[In] [Bindable]
			public var awaitPaymentCount:Number;
			
			[In] [Bindable]
			public var uncheckedCount:Number;
			
			[In] [Bindable]
			public var unsentCount:Number;
			
			[Bindable]
			public var toolbarInitialized:Boolean = false;
			
			public var pendingReload:Boolean = false;
			
			private var autoReloadCountsProcessId:Number;
			
			public override function onShow():void
			{
				dispatchEvent(new RetrieveCurrentServerTimeEvent(function(now:Date):void {
					connectGravity();
					reloadCounts();
				}));
				setNextAutoReloadCountsTrigger();
			}
			
			public override function onShowLater():void
			{
				callLater(disableMenuBarCloseButton);
			}
			
			public override function onHide():void
			{
				dispatchEvent(new RetrieveCurrentServerTimeEvent(function(now:Date):void {
					disconnectGravity();
				}));
				clearNextAutoReloadCountsTrigger();
			}
			
			public function connectGravity():void 
			{
				disconnectGravity();
				if (consumer == null) {
					consumer = new Consumer();
					consumer.destination = "pharm-inbox-gravity";
					consumer.selector = "HOSP_CODE = '" + workstore.hospCode + "'";
					consumer.addEventListener(MessageEvent.MESSAGE, updateNotificationHandler);
				}
				consumer.subscribe();
			}
			
			public function disconnectGravity():void 
			{
				if (consumer != null && consumer.subscribed) {
					consumer.unsubscribe();
				}
			}
			
			private function updateNotificationHandler(event:MessageEvent):void 
			{
				var msg:AsyncMessage = event.message as AsyncMessage;
				
				if (!pendingReload) {
					pendingReload = true;
					setTimeout(reloadCounts, Math.random() * 2000);
					resetNextAutoReloadCountsTrigger();
				}
			}
			
			private function disableMenuBarCloseButton():void
			{
				dispatchEvent(new UpdateCloseMenuItemEvent(false));
			}
			
			public function autoReloadCounts():void
			{
				reloadCounts();
				setNextAutoReloadCountsTrigger();
			}
			
			private function setNextAutoReloadCountsTrigger():void
			{
				autoReloadCountsProcessId = setTimeout(autoReloadCounts, 900000);
			}
			
			private function clearNextAutoReloadCountsTrigger():void
			{
				if (!isNaN(autoReloadCountsProcessId)) {
					clearTimeout(autoReloadCountsProcessId);
				}
				autoReloadCountsProcessId = NaN;
			}
			
			private function resetNextAutoReloadCountsTrigger():void
			{
				clearNextAutoReloadCountsTrigger();
				setNextAutoReloadCountsTrigger();
			}
			
			public function reloadCounts():void
			{
				dispatchEvent(new CountAllDashboardItems());
				pendingReload = false;
			}
			
			public function recalculateDashboardRelatedStat():void
			{
				dispatchEvent(new RecalculateDashboardRelatedStatEvent());
			}
			
			public function close():void
			{
				dispatchEvent(new CloseActiveWindowEvent());
			}
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout/>
	</fc:layout>
	
	<fx:Binding source="{toolbarInitialized &amp;&amp; permissionMap.getValueAsBoolean(MedProfileConstants.PERMISSION_ITD_SUPPORT)}"
				destination="toolbar.refreshButton.visible"/>
	<fx:Binding source="{toolbarInitialized &amp;&amp; permissionMap.getValueAsBoolean(MedProfileConstants.PERMISSION_ITD_SUPPORT)}"
				destination="toolbar.refreshButton.includeInLayout"/>
	
	<fc:Toolbar id="toolbar" width="100%" 
				refreshFunc="{recalculateDashboardRelatedStat}"
				closeFunc="{close}"
				initialize="toolbar.init(); toolbarInitialized = true"/>

	<s:HGroup width="100%" height="100%" paddingLeft="10" paddingTop="10" paddingRight="10" gap="10">
		
		<s:VGroup width="100%" height="100%">
			<CategoryCountPanel width="100%" height="100%" styleName="dashboardNewPanelStyle"
								title="New"
								totalCount="{newCount}"
								normalCount="{newNormalCount}"
								urgentCount="{newUrgentCount}"
								pendingCount="{newPendingCount}"
								overridePendingCount="{newOverridePendingCount}"
								suspendCount="{newSuspendCount}"/>
			<GeneralCountPanel width="100%" title="Unchecked / Unsent Batches" visible="false"/>
		</s:VGroup>

		<s:VGroup width="100%" height="100%">
			<CategoryCountPanel width="100%" height="100%" styleName="dashboardVerifyPanelStyle"
								title="Pending Verify"
								totalCount="{verifyCount}"
								normalCount="{verifyNormalCount}"
								urgentCount="{verifyUrgentCount}"
								pendingCount="{verifyPendingCount}"
								overridePendingCount="{verifyOverridePendingCount}"
								suspendCount="{verifySuspendCount}"/>
			<GeneralCountPanel width="100%" title="Unchecked / Unsent Batches"
							   firstCount="{uncheckedCount}" secondCount="{unsentCount}"/>
		</s:VGroup>
		
		<s:VGroup width="100%" height="100%">
			<GeneralCountPanel width="100%" title="Exception"
							   firstCount="{exceptionCount}"/>
			<s:Spacer height="100%"/>
			<GeneralCountPanel width="100%" title="Follow Up"
							   firstCount="{followUpCount}"/>
			<s:Spacer height="100%"/>
			<GeneralCountPanel width="100%" title="Overdue (New / Refill)"
							   firstCount="{dashboardOverdueNewCount}" secondCount="{dashboardOverdueRefillCount}"/>
			<s:Spacer height="100%"/>
			<GeneralCountPanel width="100%" title="2-hr Forecast (New / Refill)"
							   firstCount="{dashboardForecastNewCount}" secondCount="{dashboardForecastRefillCount}"/>
			<s:Spacer height="100%"/>
			<GeneralCountPanel width="100%" title="SFI (Paid / Awaiting $)"
							   firstCount="{paidCount}" secondCount="{awaitPaymentCount}"/>
		</s:VGroup>
	</s:HGroup>
	<s:Label width="100%" paddingRight="10" textAlign="right" text="Last Updated: {MedProfileUtils.formatDateTime(dashboardUpdateDate, false)}"/>
	
</fc:ExtendedNavigatorContent>
