<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
				 xmlns:s="library://ns.adobe.com/flex/spark" 
				 xmlns:mx="library://ns.adobe.com/flex/mx"
				 xmlns:m="hk.org.ha.fmk.pms.flex.components.message.*"
				 xmlns="hk.org.ha.view.pms.main.vetting.mp.*"
				 xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
				 xmlns:core="hk.org.ha.fmk.pms.flex.components.core.*"
				 autoDrawBackground="true">		
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			
			import spark.events.TextOperationEvent;
			
			import hk.org.ha.event.pms.main.alert.druginfo.RetrieveDrugMdsByItemCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDefaultFormVerbByFormCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDmFormVerbMappingListBySiteCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDmWarningListForOrderEditEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveDosageUnitListByFormCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveIpSiteListByItemCodeEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugForMpOrderEditEvent;
			import hk.org.ha.event.pms.main.reftable.RetrieveFloatingDrugNameEvent;
			import hk.org.ha.event.pms.main.reftable.RetrieveItemDispConfigEvent;
			import hk.org.ha.event.pms.main.vetting.mp.ChangeItemCodeFromManualEntryItemEditEvent;
			import hk.org.ha.event.pms.main.vetting.mp.CheckAtdpsItemLocationByItemCodeEvent;
			import hk.org.ha.event.pms.main.vetting.popup.ShowFormVerbPopupEvent;
			import hk.org.ha.model.pms.dms.persistence.DmDrug;
			import hk.org.ha.model.pms.dms.persistence.DmFormVerbMapping;
			import hk.org.ha.model.pms.dms.persistence.DmWarning;
			import hk.org.ha.model.pms.dms.vo.DrugMds;
			import hk.org.ha.model.pms.dms.vo.FormVerb;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
			import hk.org.ha.model.pms.persistence.reftable.ItemDispConfig;
			import hk.org.ha.model.pms.udt.vetting.FmStatus;
			import hk.org.ha.model.pms.udt.vetting.OrderType;
			import hk.org.ha.model.pms.vo.drug.DmDrugLite;
			import hk.org.ha.model.pms.vo.drug.DmWarningLite;
			import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
			import hk.org.ha.model.pms.vo.medprofile.VettingEditItemViewInfo;
			import hk.org.ha.model.pms.vo.medprofile.VettingEditItemViewUiInfo;
			import hk.org.ha.model.pms.vo.rx.Dose;
			import hk.org.ha.model.pms.vo.rx.DoseGroup;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewUtil;
			import hk.org.ha.view.pms.main.vetting.popup.FormVerbPopup;
			
			[Bindable]
			public var vettingEditItemViewInfo:VettingEditItemViewInfo;
			[Bindable]
			public var medProfilePoItemList:ListCollectionView;
			[Bindable]
			public var itemCodeTabIndex:int;
			[Bindable]
			public var drugNameTabIndex:int;
			[Bindable]
			public var formDescTabIndex:int;
			[Bindable]
			public var strengthTabIndex:int;
			[Bindable]
			public var volumeTabIndex:int;
			[Bindable]
			public var drugNameTabFocusEnabled:Boolean=false;
			[Bindable]
			public var formDescTabFocusEnabled:Boolean=false;
			[Bindable]
			public var strengthTabFocusEnabled:Boolean=false;
			[Bindable]
			public var volumeTabFocusEnabled:Boolean=false;
			private var formCodeChangeFlag:Boolean=false;						
			
			public override function set data(value:Object):void {
				super.data = value;
				if ( vettingEditItemViewInfo.itemCodeFocusFlag ) {
					vettingEditItemViewInfo.itemCodeFocusFlag = false;
					itemCodeTextInput.setFocus();
				}
			}						
			
			private function resetFirstDose(firstDose:Dose, dosageUnit:String):void
			{
				if(dosageUnit != null){
					firstDose.dosageSetFocusFlag = true;		
				}else{
					firstDose.dosageUnitFocusFlag = true;
				}
				firstDose.dosageEnd = 0;
				firstDose.dosageEndText ="";
				firstDose.dosageEndErrorString = null;
				firstDose.dosage = 0;
				firstDose.dosageText ="";				
				firstDose.dosageErrorString = null;
			}
			
			private function updateManualEntryFreeTextBaseUnit(medProfilePoItem:MedProfilePoItem, dosageUnit:String):void {
				if (vettingEditItemViewInfo.isManualProfile) {
					if (medProfilePoItem.isFreeTextEntry) {
						if (dosageUnit) {
							medProfilePoItem.baseUnit = medProfilePoItem.doseUnit.substr(0,4);
						}
					}
				}
			}
			
			private function updateDosageUnitList(value:ArrayCollection):void
			{
				var medProfilePoItem:MedProfilePoItem = MedProfilePoItem(data);				
				var dosageUnit:String;
				
				vettingEditItemViewInfo.dosageUnitList = value;
				//PMS-1561
				medProfilePoItem.regimen.firstDose.dosageUnitList = value;					
				//end-PMS-1561
				medProfilePoItem.addBlankDosageUnitForNonDosageCompul();
				
				if ( formCodeChangeFlag ) {
					if ( value.length == 1 ) {
						dosageUnit = value.getItemAt(0) as String;
					} else {
						dosageUnit = null;
					}					
				} else {
					dosageUnit = medProfilePoItem.dmDrugLite.dispenseDosageUnit;
				}
				
				medProfilePoItem.doseQty = 0;
				medProfilePoItem.doseUnit = dosageUnit;
				
				medProfilePoItem.regimen.updateDosage("");
				medProfilePoItem.regimen.updateDosageUnit(dosageUnit);		
				
				resetFirstDose(medProfilePoItem.regimen.firstDose, dosageUnit);
				updateManualEntryFreeTextBaseUnit(medProfilePoItem, dosageUnit);

				if (formCodeChangeFlag && medProfilePoItem.fmStatus != FmStatus.FreeTextEntryItem.dataValue) {
					vettingEditItemViewInfo.recalHandler(medProfilePoItem);
				}
				
				formCodeChangeFlag = false;
			}
			
			protected function formDescKeyUpHandler(event:KeyboardEvent):void
			{							
				if (MedProfilePoItem(data).readOnlyFlag || !event.ctrlKey || event.charCode != 53) { 
					return;
				} 
				else if (vettingEditItemViewInfo.isManualProfile && vettingEditItemViewInfo.isCapd) {
					return;
				} else {
					var firstDose:Dose = MedProfilePoItem(data).regimen.firstDose;
					var okHandler:Function = function(value:Object):void {	
						vettingEditItemViewInfo.backendProcessFlag = true;
						var popup:FormVerbPopup = value as FormVerbPopup;
						var dmFormVerbMapping:DmFormVerbMapping = null;				
						if( popup.formVerbGrid.selectedIndex != -1 ) {				
							dmFormVerbMapping = popup.formVerbGrid.selectedItem as DmFormVerbMapping;
							if ( data.formCode != dmFormVerbMapping.dmForm.formCode ) {
								formCodeChangeFlag = true;
								dispatchEvent( new RetrieveDosageUnitListByFormCodeEvent(dmFormVerbMapping.dmForm.formCode, updateDosageUnitList) );
							}
							data.formLabelDesc = dmFormVerbMapping.dmForm.labelDesc;							
							data.formCode = dmFormVerbMapping.dmForm.formCode;
							data.checkFormVerb(vettingEditItemViewInfo.mpFormVerbMap,vettingEditItemViewInfo.sysMsgMap.getMessage("0172"));
							//PMS-1782
							data.issueQtyText = "0";
							firstDose.checkDosageLargerThanDosageEnd(vettingEditItemViewInfo.sysMsgMap);
							
							if( MedProfilePoItem(data).dmDrugLite.dispenseDosageUnit != null){
								firstDose.dosageSetFocusFlag = true;		
							}else{
								firstDose.dosageUnitFocusFlag = true;
							}
							MedProfilePoItem(data).routeCode = dmFormVerbMapping.dmForm.dmRoute.routeCode;
							MedProfilePoItem(data).routeDesc = dmFormVerbMapping.dmForm.dmRoute.fullRouteDesc;
							vettingEditItemViewInfo.clearMoIdToNanAndUpdateMoStatus();
							markModifyFlag(MedProfilePoItem(data));
							
							PopUpManager.removePopUp(popup);
						}
					}
					
					var dose:Dose = DoseGroup(MedProfilePoItem(data).regimen.doseGroupList.getItemAt(0)).doseList.getItemAt(0) as Dose;
					
					dispatchEvent( new RetrieveDmFormVerbMappingListBySiteCodeEvent(
						dose.siteCode,
						new ShowFormVerbPopupEvent(okHandler,true)
					));
				}
			}
			
			private function postChangeItemCodeHandler(medProfileMoItemIn:MedProfileMoItem):void {
				var medProfilePoItem:MedProfilePoItem = data as MedProfilePoItem;
				var mpi:MedProfileItem = medProfilePoItem.medProfileMoItem.medProfileItem;
				if (medProfileMoItemIn && mpi) {							
					var poi:MedProfilePoItem = medProfileMoItemIn.medProfilePoItemList[0];
					var poiIndex:int = medProfilePoItemList.getItemIndex(medProfilePoItem);
					
					mpi.medProfileMoItem = medProfileMoItemIn;
					poi.medProfileMoItem = medProfileMoItemIn;
					medProfileMoItemIn.medProfileItem = mpi;
					medProfileMoItemIn.itemNum = mpi.orgItemNum;
					medProfileMoItemIn.orgItemNum = mpi.orgItemNum;
					
					mpi.newItemFlag = true;
					
					if (medProfilePoItemList.length > 1) {
						poi.regimen.type = medProfilePoItem.regimen.type;
						poi.regimen = vettingEditItemViewInfo.createRegimen(poi);
						vettingEditItemViewInfo.refreshMedProfilePoItemRegimenInfo(poi);
					}
					
					data = poi;
					
					medProfilePoItemList.setItemAt(poi,poiIndex);
					medProfilePoItemList.refresh();
					
					vettingEditItemViewInfo.resetPoViewTabSeq(-1,10);
					vettingEditItemViewInfo.updateChargeSpecialty(poi);
					vettingEditItemViewInfo.refreshSelectedItemFromAssociatedItems(poi);
					
					if (medProfileMoItemIn.isCapdRxDrug) {
						poi.regimen.updateDosageUnit(poi.regimen.firstDose.dosageUnitList[0]);
					}
					
					poi.addBlankDosageUnitForNonDosageCompul();
					
					vettingEditItemViewInfo.context.dispatchEvent( new RetrieveItemDispConfigEvent(poi.itemCode, 
						function(itemDispConfig:ItemDispConfig):void {
							checkSingleDispFlagByItemDispConfig(itemDispConfig,poi);
						}
					));							
					
					if (poi.itemCode != null && poi.itemCode != "" && medProfileMoItemIn.rxItem.isRxDrug) {
						vettingEditItemViewInfo.context.dispatchEvent(new RetrieveDrugMdsByItemCodeEvent(poi.itemCode, true, true, retrieveDrugInfoCodeHandler));
					}
					
					//for ATDPS
					vettingEditItemViewInfo.context.dispatchEvent( 
						new CheckAtdpsItemLocationByItemCodeEvent(
							poi.itemCode, 
							vettingEditItemViewInfo.atdpsPickStationsNumList, 
							function(atdpsItemLocationList:ArrayCollection):void {
								poi.unitDoseItem = ( atdpsItemLocationList.length > 0 );	
								poi.checkHideDispDuration();
								poi.checkUnitDoseEnable(vettingEditItemViewInfo.propMap, vettingEditItemViewInfo.medProfile.atdpsWardFlag, true);//PMSIPU-1951,PMSIPU-1899
								if (poi.unitDoseItem) {
									poi.refillDurationText = String(vettingEditItemViewInfo.propMap.getValueAsInt("vetting.medProfile.atdps.refill.duration",1));
								}
							}
						)
					);
				} else {
					medProfilePoItem.itemCodeErrorString = vettingEditItemViewInfo.sysMsgMap.getMessage("0030");
				}
			}
			
			//For manual profile use only
			private function changeItemCodeHandler(itemCodeIn:String):void {
				vettingEditItemViewInfo.gcnSeqNum = null;
				vettingEditItemViewInfo.rdfgenId = null;
				vettingEditItemViewInfo.rgenId = null;

				vettingEditItemViewInfo.backendProcessFlag = true;
				vettingEditItemViewInfo.markDirtyWhenItemCodeChange();
				
				dispatchEvent(
					new ChangeItemCodeFromManualEntryItemEditEvent(
					itemCodeIn, 
					vettingEditItemViewInfo.previousDmDrug.displayname,
					postChangeItemCodeHandler
				));
			}
			
			//PMS2750
			protected function itemCodeFocusInHandler(event:FocusEvent):void{
				try{
					vettingEditItemViewInfo.resetPoViewTabSeq(itemCodeTabIndex);
				}catch(e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function itemCodeFocusOutHandler(event:FocusEvent):void
			{
				if ( !data || data.checkItemCode(vettingEditItemViewInfo.sysMsgMap) ) {								
					return;
				}				
				if ( data.checkDrugValidity(vettingEditItemViewInfo.sysMsgMap, false) ) {
					if (!vettingEditItemViewInfo.isManualProfile) {
						dispatchEvent(new RetrieveWorkstoreDrugForMpOrderEditEvent(
							data.medProfileMoItem.rxItem, 
							data.itemCode, 
							updatePharmDrugItem
						));
					} else {
						changeItemCodeHandler(itemCodeTextInput.text);
					}
				}				
			}
			
			public function drugItemKeyboardHandler(event:KeyboardEvent):void {
				if (event.charCode == 53 && event.ctrlKey ) {
					OrderEditViewUtil.postItemCodeKeyUpHandlerForMp(
						parentApplication.ctx, 
						MedProfilePoItem(data).medProfileMoItem.rxItem, 
						vettingEditItemViewInfo.isManualProfile, 
						vettingEditItemViewInfo.isManualProfile?changeItemCodeHandler:updatePharmDrugItem
					);				
				}
			}						
			
			//it executes in CARS2 only
			private function postUpdatePharmDrugItem(medProfilePoItem:MedProfilePoItem):void {
				var dmDrugValue:DmDrugLite = medProfilePoItem.dmDrugLite;
				
				OrderEditViewUtil.updateFmAndActionStatus(medProfilePoItem.medProfileMoItem.rxItem, medProfilePoItem);
				vettingEditItemViewInfo.updateChargeSpecialty(medProfilePoItem);
				
				medProfilePoItem.checkSingleDispenseCompul();
				medProfilePoItem.medProfileMoItem.updateDoctorSpecifiedQtyToPharmLineRegimen();											
				
				formCodeChangeFlag = false;
				dispatchEvent( new RetrieveDosageUnitListByFormCodeEvent(dmDrugValue.formCode, updateDosageUnitList) );
				dispatchEvent( new RetrieveDmWarningListForOrderEditEvent(dmDrugValue.itemCode, updateMedProfilePoItemByDmWarningList) );					
				dispatchEvent( new RetrieveFloatingDrugNameEvent(dmDrugValue.itemCode, OrderType.InPatient, updateSynonym) );					
				
				if ( !vettingEditItemViewInfo.isParenteral || vettingEditItemViewInfo.isManualProfile ) {
					dispatchEvent( new RetrieveIpSiteListByItemCodeEvent(medProfilePoItem.itemCode, updateSiteList) );
					dispatchEvent( new RetrieveDefaultFormVerbByFormCodeEvent(dmDrugValue.formCode,updateDefaultFormVerb) );
				}
				
				vettingEditItemViewInfo.context.dispatchEvent( new RetrieveItemDispConfigEvent(medProfilePoItem.itemCode, 
					function(itemDispConfig:ItemDispConfig):void {
						checkSingleDispFlagByItemDispConfig(itemDispConfig,medProfilePoItem);
					}
				));
				
				//for ATDPS
				vettingEditItemViewInfo.context.dispatchEvent(
					new CheckAtdpsItemLocationByItemCodeEvent(
						dmDrugValue.itemCode, 
						vettingEditItemViewInfo.atdpsPickStationsNumList, 
						function(atdpsItemLocationList:ArrayCollection):void {
							medProfilePoItem.unitDoseItem = ( atdpsItemLocationList.length > 0 );
							medProfilePoItem.checkHideDispDuration();
							medProfilePoItem.checkUnitDoseEnable(vettingEditItemViewInfo.propMap, vettingEditItemViewInfo.medProfile.atdpsWardFlag, true);//PMSIPU-1951,PMSIPU-1899
							if (medProfilePoItem.unitDoseItem) {
								medProfilePoItem.refillDurationText = String(vettingEditItemViewInfo.propMap.getValueAsInt("vetting.medProfile.atdps.refill.duration",1));
							}
						}
					)
				);
				
				medProfilePoItem.clearDuration();
				
				vettingEditItemViewInfo.resetPoViewTabSeq(-1,10);				
			}
			
			private function updatePharmDrugItem(drug:WorkstoreDrug):void
			{							
				var medProfilePoItem:MedProfilePoItem = MedProfilePoItem(data); 
				var medProfileMoItem:MedProfileMoItem = vettingEditItemViewInfo.medProfileMoItem;
				if (medProfilePoItem != null) {
					if( drug != null ) {
						if ( drug.msWorkstoreDrug.suspend == 'Y' ) {
							medProfilePoItem.resetStateForSuspendedItem();
							medProfilePoItem.regimen			 = vettingEditItemViewInfo.createRegimen();
							medProfilePoItem.itemCodeErrorString = vettingEditItemViewInfo.sysMsgMap.getMessage("0095");						
						} else {
							var dmDrugValue:DmDrug = drug.dmDrug;			
							medProfilePoItem.clearErrorString();
							medProfilePoItem.dmDrugLite 			= DmDrugLite.objValueOf(dmDrugValue);
							medProfilePoItem.itemCode 				= dmDrugValue.itemCode;
							medProfilePoItem.baseUnit 				= dmDrugValue.baseUnit;						
							medProfilePoItem.drugName 				= dmDrugValue.drugName;
							medProfilePoItem.tradeName				= dmDrugValue.dmMoeProperty.tradename;
							medProfilePoItem.formCode 				= dmDrugValue.dmForm.formCode;
							medProfilePoItem.formLabelDesc	 		= dmDrugValue.dmForm.labelDesc;
							medProfilePoItem.strength 				= dmDrugValue.strength;
							medProfilePoItem.volumeText				= dmDrugValue.volumeText;
							medProfilePoItem.routeCode				= dmDrugValue.dmForm.routeCode;
							medProfilePoItem.routeDesc				= vettingEditItemViewInfo.dmRouteDictionary[medProfilePoItem.routeCode];
							medProfilePoItem.doseUnit 				= "";
							medProfilePoItem.refillDurationText		= getDefaultRefillDuration(medProfilePoItem.medProfileMoItem);
							medProfilePoItem.numOfLabel				= 1;					
							medProfilePoItem.regimen				= vettingEditItemViewInfo.createRegimen();
							medProfilePoItem.regimen.updateBaseUnit( dmDrugValue.baseUnit );
							medProfilePoItem.dangerDrugFlag			= dmDrugValue.dangerousDrug == 'Y';
							medProfilePoItem.wardStockFlag			= vettingEditItemViewInfo.wardStockMap.getWardStock(dmDrugValue.itemCode) != null;					
							medProfilePoItem.unitDoseFlag			= false;
							medProfilePoItem.reDispFlag				= false;
							medProfilePoItem.directPrintFlag		= false;
							medProfilePoItem.remarkText				= "";
							
							medProfilePoItem.setSingleDispFlagWithoutMarkDirty(medProfilePoItem.medProfileMoItem.isPrnExist);
							
							//bug fix on change item code clear issueQty
							medProfilePoItem.adjQtyText				= "0";
							medProfilePoItem.issueQtyText			= "0";
							
							postUpdatePharmDrugItem(medProfilePoItem);
							
							markModifyFlag(medProfilePoItem);
						}
					} else {	
						medProfilePoItem.itemCodeErrorString = vettingEditItemViewInfo.sysMsgMap.getMessage("0030");
					}
					vettingEditItemViewInfo.markDirtyWhenItemCodeChange();
					vettingEditItemViewInfo.resetPoViewTabSeq();
					vettingEditItemViewInfo.clearMoIdToNanAndUpdateMoStatus();
				}
			}
			
			private function retrieveDrugInfoCodeHandler(drugMds:DrugMds, renalAdjCode:String, reviewDate:Date):void {
				
				if (drugMds != null) {
					if (drugMds.moeCheckFlag == "Y") {
						vettingEditItemViewInfo.gcnSeqNum = drugMds.gcnSeqno;
						vettingEditItemViewInfo.rdfgenId = drugMds.routeformGeneric;
						vettingEditItemViewInfo.rgenId = drugMds.routedGeneric;
					}
					else if (drugMds.groupMoeCheckFlag == "Y") {
						vettingEditItemViewInfo.gcnSeqNum = drugMds.groupGcnSeqno;
						vettingEditItemViewInfo.rdfgenId = drugMds.groupRouteformGeneric;
						vettingEditItemViewInfo.rgenId = drugMds.groupRoutedGeneric;
					}
				}
				
				vettingEditItemViewInfo.renalAdjCode = renalAdjCode;
				vettingEditItemViewInfo.renalReviewDate = reviewDate;
			}
			
			private function getDefaultRefillDuration(medProfileMoItem:MedProfileMoItem):String {
				
				for each (var poi:MedProfilePoItem in medProfileMoItem.medProfilePoItemList) {
					if (!isNaN(poi.refillDuration)) {
						return String(poi.refillDuration);
					}
				}
				return "";
			}
			
			private function updateDefaultFormVerb(formVerbValue:FormVerb):void
			{
				var medProfilePoItem:MedProfilePoItem = MedProfilePoItem(data); 
				for each ( var doseGroup:DoseGroup in medProfilePoItem.regimen.doseGroupList ) {
					for each ( var dose:Dose in doseGroup.doseList ) {
						if (formVerbValue) {
							dose.updateDefaultSite(formVerbValue, vettingEditItemViewInfo);
						}
					}
				}
			}
			
			private function updateSynonym(value:String):void
			{
				MedProfilePoItem(data).drugSynonym = value;
			}
			
			private function updateSiteList(val:ArrayCollection):void
			{
				MedProfilePoItem(data).regimen.setAllSiteList(val);				
			}
			
			private function updateMedProfilePoItemByDmWarningList(dmWarningList:ArrayCollection):void
			{
				var medProfilePoItem:MedProfilePoItem = MedProfilePoItem(data);			
				for ( var i:int=0; i<dmWarningList.length; i++ ) {
					var dmWarning:DmWarning = dmWarningList.getItemAt(i) as DmWarning;
					if ( dmWarning ) {
						medProfilePoItem.setDmWarningLite(i,DmWarningLite.objValueOf(dmWarning));
						medProfilePoItem.setWarnCode(i,dmWarning.warnCode);
					} else {
						medProfilePoItem.setDmWarningLite(i,null);
						medProfilePoItem.setWarnCode(i,null);
					}
				}				
			}
			
			private function checkSingleDispFlagByItemDispConfig(itemDispConfig:ItemDispConfig, poi:MedProfilePoItem):void
			{
				poi.itemDispConfig = itemDispConfig;
				if ( itemDispConfig ) {
					poi.singleDispFlag ||= itemDispConfig.singleDispFlag;
				}				
				vettingEditItemViewInfo.updateSingleDispenseAndDirectLabelPrint(poi);
				vettingEditItemViewInfo.checkItemEndorsement(
					poi,
					function(evt:Event):void
					{
						PopUpManager.removePopUp((UIComponent(evt.currentTarget).parentDocument) as IFlexDisplayObject)
						poi.itemCode = "";	
						poi.checkItemCode(vettingEditItemViewInfo.sysMsgMap);
						vettingEditItemViewInfo.checkErrorFocus()
					},
					function():void {}
				);
			}
			
			private function formInputFocusHandler(evt:Event):void
			{																
				formInput.focusManager.showFocus();
			}
			
			public function getDoseGroupEditItemRendererAt(index:int):DoseGroupEditItemRenderer
			{		
				var dataGroup:DataGroup = regimenGroup;
				if ( !dataGroup || !dataGroup.dataProvider ) {
					return null;
				}
				if ( index < dataGroup.dataProvider.length && index >= 0 ) {
					return dataGroup.getElementAt(index) as DoseGroupEditItemRenderer;
				} else {
					return null;
				}				
			}
			
			public function checkFocusToError():void
			{
				if ( !vettingEditItemViewInfo.isFocusFlag ) {
					
					if ( itemCodeTextInput.errorString ) {
						vettingEditItemViewInfo.isFocusFlag = true;
						vettingEditItemViewInfo.itemEditManualErrorString = itemCodeTextInput.errorString;
						itemCodeTextInput.setFocus();
					}
				}
				
				for (var i:int=0; i<regimenGroup.dataProvider.length && !vettingEditItemViewInfo.isFocusFlag; i++) {
					getDoseGroupEditItemRendererAt(i).checkFocusToError();
				}
			}
			
			private function markModifyFlag(poi:MedProfilePoItem):void {
				if (vettingEditItemViewInfo.isManualProfile || vettingEditItemViewInfo.pivasItemFlag) {
					return;
				}
				poi.medProfileMoItem.modifyFlag = true;
			}
			
		]]>
	</fx:Script>
	
	<mp:layout>
		<s:VerticalLayout gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0"/>
	</mp:layout>
	
	<s:HGroup gap="{VettingEditItemViewUiInfo.paddingSpace}"
			  verticalAlign="middle" styleName="vettingPoDrugLineStyle"
			  paddingLeft="{VettingEditItemViewUiInfo.paddingSpace}" paddingRight="0">		
		<core:UppercaseTextInput id="itemCodeTextInput"
								 tabIndex="{itemCodeTabIndex}"
								 name="itemCodeTextInput"
								 text="@{data.itemCode}" 
								 keyUp="drugItemKeyboardHandler(event)"		 
								 focusIn="itemCodeFocusInHandler(event)"
								 focusOut="itemCodeFocusOutHandler(event)"
								 change="markModifyFlag(MedProfilePoItem(data))"
								 width="{VettingEditItemViewUiInfo.itemCodeColWidth - VettingEditItemViewUiInfo.paddingSpace}"
								 errorString="{data.itemCodeErrorString}"
								 restrict="A-Z0-9a-z "
								 maxChars="6"
								 styleName="iPItemEditPOIComponentStyle"
								 enabled="{!MedProfilePoItem(data).readOnlyFlag &amp;&amp; !MedProfilePoItem(data).isFreeTextEntry}"
								 editable="true"/>
		<core:UppercaseTextInput text="@{data.drugName}"
								 tabIndex="{drugNameTabIndex}"
								 focusIn="vettingEditItemViewInfo.resetPoViewTabSeq()"
								 change="vettingEditItemViewInfo.clearMoIdToNanAndUpdateMoStatus();markModifyFlag(MedProfilePoItem(data));"
								 width="{VettingEditItemViewUiInfo.drugNameColWidth - VettingEditItemViewUiInfo.paddingSpace}" 
								 editable="{!MedProfilePoItem(data).readOnlyFlag}"									 
								 maxChars="57"
								 restrict="\u0020-\u007E"
								 tabFocusEnabled="{drugNameTabFocusEnabled}"
								 styleName="iPItemEditPOIComponentStyle"
								 enabled="{!MedProfilePoItem(data).readOnlyFlag}"
								 toolTip="{data.drugName}"/>					
		<core:UppercaseTextInput id="formInput"
								 tabIndex="{formDescTabIndex}"
								 focusIn="vettingEditItemViewInfo.resetPoViewTabSeq()"
								 text="{data.formLabelDesc}" 									 
								 width="{VettingEditItemViewUiInfo.formDescColWidth - VettingEditItemViewUiInfo.paddingSpace}" 	
								 keyUp="formDescKeyUpHandler(event)"									 									
								 editable="false"
								 click="formInputFocusHandler(event)"
								 doubleClick="formInputFocusHandler(event)"
								 selectable="true"
								 enabled="{!MedProfilePoItem(data).readOnlyFlag}"
								 tabFocusEnabled="{formDescTabFocusEnabled}"
								 styleName="iPItemEditPOIComponentStyle"
								 toolTip="{data.formLabelDesc}"/>
		<core:UppercaseTextInput text="@{data.strength}"
								 tabIndex="{strengthTabIndex}"
								 focusIn="vettingEditItemViewInfo.resetPoViewTabSeq()"
								 change="vettingEditItemViewInfo.clearMoIdToNanAndUpdateMoStatus();markModifyFlag(MedProfilePoItem(data));"
								 width="{VettingEditItemViewUiInfo.strengthColWidth - VettingEditItemViewUiInfo.paddingSpace}" 
								 editable="{!MedProfilePoItem(data).readOnlyFlag}"									 
								 maxChars="12"
								 enabled="{!MedProfilePoItem(data).readOnlyFlag}"
								 tabFocusEnabled="{strengthTabFocusEnabled}"
								 styleName="iPItemEditPOIComponentStyle"
								 toolTip="{data.strength}"/>
		<core:UppercaseTextInput text="@{data.volumeText}"
								 tabIndex="{volumeTabIndex}"
								 focusIn="vettingEditItemViewInfo.resetPoViewTabSeq()"
								 change="vettingEditItemViewInfo.clearMoIdToNanAndUpdateMoStatus();markModifyFlag(MedProfilePoItem(data));"
								 width="{VettingEditItemViewUiInfo.volumeColWidth - VettingEditItemViewUiInfo.paddingSpace}" 
								 editable="{!MedProfilePoItem(data).readOnlyFlag}"	
								 enabled="{!MedProfilePoItem(data).readOnlyFlag}"
								 styleName="iPItemEditPOIComponentStyle"
								 maxChars="23"
								 tabFocusEnabled="{volumeTabFocusEnabled}"
								 toolTip="{data.volumeText}"/>
		<s:Group width="{VettingEditItemViewUiInfo.synonymColWidth - VettingEditItemViewUiInfo.paddingSpace}">
			<core:UppercaseTextInput text="@{data.drugSynonym}"
									 change="vettingEditItemViewInfo.clearMoIdToNanAndUpdateMoStatus();"
									 width="100%" 
									 tabFocusEnabled="false"
									 editable="false"										 
									 maxChars="46"
									 enabled="{!MedProfilePoItem(data).readOnlyFlag}"
									 styleName="iPItemEditPOIComponentStyle"
									 toolTip="{data.drugSynonym}"/>	
		</s:Group>			
		<s:HGroup width="100%" paddingLeft="0" paddingRight="0" gap="{VettingEditItemViewUiInfo.paddingSpace}" verticalAlign="middle">
			<core:NumericInput text="{data.doseQtyText}"									 
							   numberFormat="6.0"
							   tabFocusEnabled="false"
							   width="{VettingEditItemViewUiInfo.adjQtyColWidth - VettingEditItemViewUiInfo.paddingSpace}" 
							   textAlign="right"	
							   editable="false"
							   enabled="{!MedProfilePoItem(data).readOnlyFlag}"
							   styleName="iPItemEditDoseComponentStyle"/>						
			<s:Label text="{data.doseUnit}"
					 styleName="iPItemEditPOIComponentStyle"
					 verticalAlign="middle"/>				
		</s:HGroup>		
	</s:HGroup>		
	
	<s:VGroup id="vGroup" height="100%" gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0">		
		<s:DataGroup id="regimenGroup" width="100%"					 
					 dataProvider="{data.regimen.doseGroupList}">
			<s:layout>
				<s:VerticalLayout gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0"/>
			</s:layout>
			
			<s:itemRenderer>
				<fx:Component>
					<DoseGroupEditItemRenderer medProfilePoItem="{outerDocument.data as MedProfilePoItem}"
											   doseGroupList="{outerDocument.data.regimen.doseGroupList}"
											   vettingEditItemViewInfo="{outerDocument.vettingEditItemViewInfo}"
											   width="100%">
						<fx:Script>
							<![CDATA[
								import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;									
								import mx.collections.ListCollectionView;
							]]>
						</fx:Script>
					</DoseGroupEditItemRenderer>
				</fx:Component>
			</s:itemRenderer>
		</s:DataGroup>	
	</s:VGroup>
	
	<s:Line xFrom="0" xTo="{vGroup.width}" 
			yFrom="0" yTo="0" 
			visible="{ListCollectionView(medProfilePoItemList).getItemIndex(data) != (medProfilePoItemList.length - 1)}"
			includeInLayout="{ListCollectionView(medProfilePoItemList).getItemIndex(data) != (medProfilePoItemList.length - 1)}"
			width="100%">
		<s:stroke>
			<s:SolidColorStroke color="0x000000" weight="2" joints="round"/>
		</s:stroke>
	</s:Line>
	
</mp:ItemRenderer>
