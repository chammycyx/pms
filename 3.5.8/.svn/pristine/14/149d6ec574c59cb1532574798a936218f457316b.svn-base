package hk.org.ha.model.pms.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DataType implements StringValuedEnum {

	StringType("S", "String"),
	BooleanType("B", "Boolean"), 
	TextAreaType("T", "TextArea"),
	IntegerType("I", "Integer"),
	DayType("dd", "Day"),
	MonthType("MM", "Month"),
	YearType("yy", "Year"),
	HourType("HH", "Hour"),
	MinuteType("mm", "Minute"),
	SecondType("ss", "Second"),
	MillisecondType("SS", "Millisecond"),
	WeekType("ww", "Week"),
	DateTimeType("DT", "DateTime"),
	ListStringType("LS", "ListOfString"),
	ListIntegerType("LI", "ListOfInteger"), 
	Hour1To24Type("kk", "Hour"),
	DayOfWeekType("u", "DayOfWeek"),
	DoubleType("D", "Double");
	
    private final String dataValue;
    private final String displayValue;

    DataType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DataType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DataType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DataType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DataType> getEnumClass() {
    		return DataType.class;
    	}
    }
}



