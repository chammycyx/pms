<?xml version="1.0" encoding="utf-8"?>
<s:TitleWindow xmlns:fx="http://ns.adobe.com/mxml/2009" 
			   xmlns:s="library://ns.adobe.com/flex/spark" 
			   xmlns:mx="library://ns.adobe.com/flex/mx"
			   xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
			   title="Select Treatment Day for dispensing">
	
	<fx:Metadata>
		[Name("treatmentDaySelectionPopup")]
	</fx:Metadata>
	
	<fx:Declarations>
		<mx:DateFormatter id="treatmentDateFormatter" formatString="DD MMM YYYY (EEE)"/>
		<s:ArrayCollection id="selectedTreatmentDaySelectionListView" list="{treatmentDaySelectionList}"
						   filterFunction="{MedProfileUtils.selectedListFilterFunction}"/>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.managers.PopUpManager;
			
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.vo.chemo.ChemoItem;
			import hk.org.ha.model.pms.vo.medprofile.SelectionItem;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;

			private var medProfileItem:MedProfileItem;
			
			private var okFunction:Function;
			
			[Bindable]
			public var treatmentDaySelectionList:ArrayCollection;
			
			[Bindable]
			public var allSelected:Boolean;
			
			public function setupUI(medProfileItem:MedProfileItem, okFunction:Function):void
			{
				this.medProfileItem = medProfileItem;
				this.okFunction = okFunction;
				treatmentDaySelectionList = constructTreatmentDaySelectionList(medProfileItem);
				PopUpManager.centerPopUp(this);
			}
			
			private function constructTreatmentDaySelectionList(medProfileItem:MedProfileItem):ArrayCollection
			{
				var resultList:ArrayCollection = new ArrayCollection();
				for each (var chemoItem:ChemoItem in medProfileItem.medProfileMoItem.chemoInfo.chemoItemList) {
					var selectionItem:SelectionItem = new SelectionItem();
					selectionItem.selected = false;
					selectionItem.data = chemoItem;
					resultList.addItem(selectionItem);
				}
				return resultList;
			}
			
			private function dayLabelFunction(item:Object, column:DataGridColumn):String
			{
				return "D" + item.data.treatmentDay;
			}
			
			private function treatmentDateLabelFunction(item:Object, column:DataGridColumn):String
			{
				return treatmentDateFormatter.format(item.data.startDate);
			}
			
			public function selectAll(selected:Boolean):void
			{
				for each (var selectionItem:SelectionItem in treatmentDaySelectionList) {
					selectionItem.selected = selected;
				}
			}
			
			public function ok():void
			{
				var chemoItemList:ArrayCollection = new ArrayCollection();
				for each (var selectionItem:SelectionItem in selectedTreatmentDaySelectionListView) {
					chemoItemList.addItem(selectionItem.data);
				}
				okFunction(medProfileItem, chemoItemList);
				PopUpManager.removePopUp(this);
			}
			
			public function cancel():void
			{
				PopUpManager.removePopUp(this);
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="{selectedTreatmentDaySelectionListView.length != treatmentDaySelectionList.length || treatmentDaySelectionList.length == 0 ? false : true}"
				destination="allSelected"/>	
	
	<fc:ExtendedNavigatorContent>
		<fc:layout>
			<s:VerticalLayout paddingLeft="10" paddingTop="10" paddingRight="10" paddingBottom="10"/>
		</fc:layout>
		
		<fc:ExtendedDataGrid height="250" draggableColumns="false" resizableColumns="false" sortableColumns="false"
							 verticalScrollPolicy="on" horizontalScrollPolicy="off"
							 editable="false" dataProvider="{treatmentDaySelectionList}">
			<fc:columns>
				<mx:DataGridColumn width="70">
					<mx:headerRenderer>
						<fx:Component>
							<s:MXDataGridItemRenderer width="100%" autoDrawBackground="false">
								<s:layout>
									<s:HorizontalLayout horizontalAlign="center"/>
								</s:layout>
								<s:CheckBox id="selectAllCheckBox" label="Select" enabled="{outerDocument.treatmentDaySelectionList.length > 0}"
											selected="@{outerDocument.allSelected}"
											change="outerDocument.selectAll(selectAllCheckBox.selected)"/>
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:headerRenderer>
					<mx:itemRenderer>
						<fx:Component>
							<s:MXDataGridItemRenderer width="100%" autoDrawBackground="false">
								<s:layout>
									<s:HorizontalLayout horizontalAlign="center"/>
								</s:layout>
								<s:CheckBox selected="@{data.selected}"/>
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:itemRenderer>
				</mx:DataGridColumn>
				<mx:DataGridColumn width="50" headerText="Day" labelFunction="dayLabelFunction"/>
				<mx:DataGridColumn width="130" headerText="Treatment Date" labelFunction="treatmentDateLabelFunction"/>
			</fc:columns>
			
		</fc:ExtendedDataGrid>
		
		<s:HGroup width="100%" horizontalAlign="right">
			<fc:ExtendedButton label="OK" enabled="{selectedTreatmentDaySelectionListView.length > 0}"
							   click="ok()"/>
			<fc:ExtendedButton label="Cancel" click="cancel()"/>
		</s:HGroup>
		
	</fc:ExtendedNavigatorContent>
	
</s:TitleWindow>
