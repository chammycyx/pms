package hk.org.ha.model.pms.vo.alert.mds;

import hk.org.ha.model.pms.udt.alert.Certainty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AlertAllergen")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertAllergen implements Alert, Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final char NEWLINE = '\n';
	
	private static final char[] DELIMITER = new char[] {' ', '-', ',', '(', ')', '[', ']', '{', '}', '.', ':'};
	
	private static final String MANIFESTATION_SEPARATOR = ";";
	
	private static final int ADDITIVE_NUM_MULTIPLIER = 10000;
	
	private static final int MANUAL_ITEM_MULTIPLIER = 10000;

	private static final int MANUAL_ITEM_RANGE = 100000000; //100010000
	
	@XmlElement(name="HospCode", required = true)
	private String hospCode;

	@XmlElement(name="OrdNo")
	private Long ordNo;
	
	@XmlElement(name="ItemNum", required = true)
	private Long itemNum;

	@XmlElement(name="Allergen", required = true)
	private String allergen;
	
	@XmlElement(name="MatchType", required = true)
	private String matchType;
 
	@XmlElement(name="ScreenMsg", required = true)
	private String screenMsg;
 
	@XmlElement(name="Certainty", required = true)
	private Certainty certainty;
	
	@XmlElement(name="AdditionInfo")
	private String additionInfo;

	@XmlElement(name="Manifestation")
	private String manifestation;

	@XmlTransient
	private String drugName;

	@XmlTransient
	private String drugDesc;
	
	@XmlTransient
	private Boolean suppressFlag;
	
	@XmlTransient
	private Long sortSeq;
	
	public AlertAllergen() {
		suppressFlag = Boolean.FALSE;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public Long getOrdNo() {
		return ordNo;
	}

	public void setOrdNo(Long ordNo) {
		this.ordNo = ordNo;
	}

	public Long getItemNum() {
		return itemNum;
	}

	public void setItemNum(Long itemNum) {
		this.itemNum = itemNum;
	}

	public String getAllergen() {
		return allergen;
	}

	public void setAllergen(String allergen) {
		this.allergen = allergen;
	}

	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public String getScreenMsg() {
		return screenMsg;
	}

	public void setScreenMsg(String screenMsg) {
		this.screenMsg = screenMsg;
	}

	public Certainty getCertainty() {
		return certainty;
	}

	public void setCertainty(Certainty certainty) {
		this.certainty = certainty;
	}

	public String getAdditionInfo() {
		return additionInfo;
	}

	public void setAdditionInfo(String additionInfo) {
		this.additionInfo = additionInfo;
	}

	public String getManifestation() {
		return manifestation;
	}

	public void setManifestation(String manifestation) {
		this.manifestation = manifestation;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugDesc() {
		return drugDesc;
	}

	public void setDrugDesc(String drugDesc) {
		this.drugDesc = drugDesc;
	}

	public Boolean getSuppressFlag() {
		return suppressFlag;
	}

	public void setSuppressFlag(Boolean suppressFlag) {
		this.suppressFlag = suppressFlag;
	}

	public Long getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Long sortSeq) {
		this.sortSeq = sortSeq;
	}

	@ExternalizedProperty
	public String getAlertTitle() {
		return "Allergy Checking";
	}

	@ExternalizedProperty
	public String getAlertDesc() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(allergen).append(" - Allergy history reported").append(NEWLINE);

		List<String> manifestationList = new ArrayList<String>();
		if (StringUtils.isNotBlank(manifestation)) {
			sb.append("Clinical Manifestation: ").append(WordUtils.capitalizeFully(manifestation, DELIMITER)).append(NEWLINE);
			manifestationList = Arrays.asList(StringUtils.splitByWholeSeparator(manifestation, MANIFESTATION_SEPARATOR));
		}

		boolean lastManifestationEqualAdditionInfo = !manifestationList.isEmpty()
			&& StringUtils.equalsIgnoreCase(additionInfo, StringUtils.trimToNull(manifestationList.get(manifestationList.size()-1)));

		if (StringUtils.isNotBlank(additionInfo) && !lastManifestationEqualAdditionInfo) {
			sb.append("Additional Information: ").append(additionInfo).append(NEWLINE);
		}
		
		if (certainty != null) {
			sb.append("Level of Certainty: ").append(certainty.getDisplayValue()).append(NEWLINE);
		}
		
		if (StringUtils.contains(StringUtils.lowerCase(screenMsg), "an allergic/a cross-sensitivity")) {
			sb.append("Use of " + StringUtils.upperCase(drugDesc) + " may result in allergic/cross-sensitivity reaction.");
		}
		else if (StringUtils.contains(StringUtils.lowerCase(screenMsg), "an idiosyncratic")) {
			sb.append("Use of " + StringUtils.upperCase(drugDesc) + " may result in idiosyncratic reaction.");
		}
		else if (StringUtils.contains(StringUtils.lowerCase(screenMsg), "an allergic")) {
			sb.append("Use of " + StringUtils.upperCase(drugDesc) + " may result in allergic reaction.");
		}
		else if (StringUtils.contains(StringUtils.lowerCase(screenMsg), "a cross-sensitivity")) {
			sb.append("Use of " + StringUtils.upperCase(drugDesc) + " may result in cross-sensitivity reaction.");
		}
		else {
			sb.append(screenMsg);
		}
		
		return sb.toString();
	}
	
	public boolean equals(Object object) {
		
		if (!(object instanceof AlertAllergen)) {
			return false;
		}
		if (this == object) {
			return true;
		}

		AlertAllergen otherAlert = (AlertAllergen) object;
		
		return new EqualsBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode)),      StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getHospCode())))
			.append(ordNo,                                                        otherAlert.getOrdNo())
			.append(itemNum,                                                      otherAlert.getItemNum())
			.append(StringUtils.trimToNull(StringUtils.lowerCase(allergen)),      StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getAllergen())))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(screenMsg)),     StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getScreenMsg())))
			.append(certainty,                                                    otherAlert.getCertainty())
			.append(StringUtils.trimToNull(StringUtils.lowerCase(additionInfo)),  StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getAdditionInfo())))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(manifestation)), StringUtils.trimToNull(StringUtils.lowerCase(otherAlert.getManifestation())))
			.isEquals();
	}
	
	public int hashCode() {
		return new HashCodeBuilder()
			.append(StringUtils.trimToNull(StringUtils.lowerCase(hospCode)))
			.append(ordNo)
			.append(itemNum)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(allergen)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(screenMsg)))
			.append(certainty)
			.append(StringUtils.trimToNull(StringUtils.lowerCase(additionInfo)))
			.append(StringUtils.trimToNull(StringUtils.lowerCase(manifestation)))
			.toHashCode();
	}

	public void loadDrugDesc(String drugDesc) {
		this.drugDesc = drugDesc;
	}

	public void replaceItemNum(Integer oldItemNum, Integer newItemNum, String orderNum) {
		itemNum = Long.valueOf(newItemNum.intValue());
	}

	public void replaceOrderNum(String oldOrderNum, String newOrderNum) {
		hospCode = StringUtils.trimToNull(newOrderNum.substring(0, 3));
		ordNo = Long.valueOf(newOrderNum.substring(3));		
	}

	public void restoreItemNum() {
		if( itemNum.longValue() > MANUAL_ITEM_RANGE ){
			itemNum = Long.valueOf(itemNum.longValue() / MANUAL_ITEM_MULTIPLIER);
		}else if (itemNum.longValue() >= ADDITIVE_NUM_MULTIPLIER) {
			itemNum = Long.valueOf(itemNum.longValue() / ADDITIVE_NUM_MULTIPLIER);
		}
	}

	@ExternalizedProperty
	public String getKeys() {
		return AlertAllergen.class.getName()
				+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(allergen))
				+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(screenMsg))
				+ NEWLINE + certainty.getDisplayValue()
				+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(additionInfo))
				+ NEWLINE + StringUtils.trimToEmpty(StringUtils.lowerCase(manifestation));
	}
}
