package hk.org.ha.model.pms.persistence.corp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.RecordStatus;

@Entity
@Table(name = "WARD_ADMIN_FREQ")
@IdClass(WardAdminFreqPK.class)
public class WardAdminFreq extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PAT_HOSP_CODE", nullable = false, length = 3)
	private String patHospCode;
	
	@Id
	@Column(name = "PAS_WARD_CODE", nullable = false, length = 4)
	private String pasWardCode;
	
	@Id
	@Column(name = "FREQ_CODE", nullable = false, length = 5)
	private String freqCode;
	
	@Converter(name = "WardAdminFreq.status", converterClass = RecordStatus.Converter.class)
    @Convert("WardAdminFreq.status")
	@Column(name="STATUS", nullable = false, length = 1)
	private RecordStatus status;
	
	public WardAdminFreq() {
		status = RecordStatus.Active;
	}

	public WardAdminFreq(String patHospCode, String pasWardCode, String freqCode) {
		this();
		this.patHospCode = patHospCode;
		this.pasWardCode = pasWardCode;
		this.freqCode    = freqCode;
	}
	
	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public String getFreqCode() {
		return freqCode;
	}

	public void setFreqCode(String freqCode) {
		this.freqCode = freqCode;
	}

	public RecordStatus getStatus() {
		return status;
	}

	public void setStatus(RecordStatus status) {
		this.status = status;
	}
	
	public WardAdminFreqPK getId(){
		return new WardAdminFreqPK(this.patHospCode, this.pasWardCode, this.freqCode);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(patHospCode)
			.append(pasWardCode)
			.append(freqCode)
			.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		WardAdminFreq other = (WardAdminFreq) obj;
		return new EqualsBuilder()
			.append(patHospCode, other.getPatHospCode())
			.append(pasWardCode, other.getPasWardCode())
			.append(freqCode,    other.getFreqCode())
			.isEquals();
	}
}
