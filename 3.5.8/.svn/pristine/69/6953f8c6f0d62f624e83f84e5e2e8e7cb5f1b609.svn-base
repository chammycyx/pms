<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="720" height="240">

	<fx:Metadata>
		[Name("fmIndicationPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.model.pms.vo.vetting.FmIndication;
			
			import mx.collections.ArrayCollection;
			import mx.managers.PopUpManager;
			
			import spark.events.IndexChangeEvent;

			[Bindable]
			private var fmIndicationList:ArrayCollection = new ArrayCollection();

			public function init(inFmIndicationList:ArrayCollection):void {
				fmIndicationList.removeAll();
				fmIndicationList.addAll(inFmIndicationList);
				callLater(closeBtn.setFocus);
				focusManager.showFocus();
			}
			
			private function fmListChangingHandler(evt:IndexChangeEvent):void {
				evt.preventDefault();
			}
			
			private function cancelHandler(event:MouseEvent):void {
				PopUpManager.removePopUp(this);
			}
		]]>
		
	</fx:Script>
	
	<s:Panel title="Formulary Management Indication" width="100%" height="100%">

		<s:layout>
			<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" gap="10"/>
		</s:layout>

		<s:List id="fmList" width="100%" height="100%" dataProvider="{fmIndicationList}" changing="fmListChangingHandler(event)">
			
			<s:itemRenderer>
				<fx:Component>
					<s:ItemRenderer autoDrawBackground="true" width="100%" styleName="fmIndicationStyle">
						<fx:Script>
							<![CDATA[
								import flexlib.scheduling.util.DateUtil;
								
								import hk.org.ha.fmk.pms.flex.utils.WordUtil;
								import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
								import hk.org.ha.model.pms.udt.vetting.FmStatus;
								import hk.org.ha.model.pms.udt.vetting.NameType;
								import hk.org.ha.model.pms.vo.rx.RxDrug;
								import hk.org.ha.model.pms.vo.vetting.FmIndication;
								
								import mx.utils.StringUtil;
								
								override public function set data(value:Object):void
								{
									if(value != null)
									{ 
										super.data = value;
										setStyle("fontWeight", 'bold');
									}
								}	 
								
								private function getFmDesc(data:Object):String {
									if (data == null) {
										return "";
									}
									
									var fmDesc:String = "";
									
									// set drug desc
									fmDesc += getMoeDrugDesc(data);
									fmDesc += "\n";
									
									// set indication
									if (data.fmEntity.otherMessage != null || data.fmEntity.dmCorpIndication != null) {
										fmDesc += "Indication: ";
										if (data.fmEntity.otherMessage != null) {
											fmDesc += data.fmEntity.otherMessage;
										} else if (data.fmEntity.dmCorpIndication != null) {
											fmDesc += data.fmEntity.dmCorpIndication.corpIndicationDesc;
										}
										fmDesc += "\n";
									}
									
									// set prescribing doctor
									fmDesc += "Prescribing doctor: ";
									fmDesc += data.prescibingDoctorName;
									fmDesc += "\n";
									
									// set authorized doctor
									if (data.fmEntity.authDoctorName != null) {
										fmDesc += "Authorized doctor: ";
										fmDesc += data.fmEntity.authDoctorName;
										fmDesc += "\n";
									}
									
									// set order date
									fmDesc += "Order date: ";
									fmDesc += outerDocument.dateFormatter.format(data.fmEntity.fmCreateDate);
									fmDesc += "\n\n";
									
									// set authorized date
									if ( ! isAuthExpired(data as FmIndication)) {
										fmDesc += "The authorization is valid until ";
										fmDesc += outerDocument.dateFormatter.format(data.authDate);
										fmDesc += ".";
										fmDesc += "\n";
									} else {
										fmDesc += "The valid period of the authorization has expired."
										fmDesc += "\n";
									}
									
									return fmDesc;
								}
								
								private function getMoeDrugDesc(data:Object):String {
									var rxDrug:RxDrug = data.rxDrug;
									
									// Medical Line Display Name
									var moeDrugDesc:String = "";
									
									var volumnStr:String = "";
									var isIncludeAlias:Boolean = false;
									
									if (StringUtil.trim(rxDrug.aliasName) != "" && rxDrug.nameType == NameType.TradeName) {
										isIncludeAlias = true;
									}
									
									// alias name
									if (isIncludeAlias) {
										moeDrugDesc += WordUtil.capitalize(rxDrug.aliasName) + " (";
									}
									
									// MOE display name
									moeDrugDesc += WordUtil.capitalize(rxDrug.displayName);
									
									// Salt / special property
									if (StringUtil.trim(rxDrug.saltProperty) != "") {
										moeDrugDesc += " " + WordUtil.capitalize(rxDrug.saltProperty);
									}
									
									if (isIncludeAlias) {
										moeDrugDesc += ")";
									}
									
									// Form description
									moeDrugDesc += " " + StringUtil.trim(rxDrug.formDesc).toLowerCase();
									
									// Strength
									if ( rxDrug.strength != null ) {
										moeDrugDesc += " " + rxDrug.strength.toLowerCase();
									}
									
									// Volume value & Volume unit
									if ( !isNaN(rxDrug.volume) && rxDrug.volume > 0 ) {
										volumnStr += rxDrug.volume;
										if (rxDrug.volumeUnit != null) {
											volumnStr += rxDrug.volumeUnit;
										}
										moeDrugDesc += " " + volumnStr.toLowerCase();
									}
									
									if ( rxDrug.extraInfo != null && (StringUtil.trim(rxDrug.aliasName) == "" || rxDrug.extraInfo != rxDrug.aliasName) ) {
										moeDrugDesc += " " + rxDrug.extraInfo.toLowerCase();
									}
									
									return moeDrugDesc + ' - prescribed as a ' + data.fmEntity.prescOption.displayValue.toLowerCase();
								}
								
								private function isAuthExpired(fmIndication:FmIndication):Boolean {
									if (fmIndication.authDate == null || ((new Date()).time > fmIndication.authDate.time)) {
										return true;
									} else {
										return false;
									}
								}
							]]>
						</fx:Script>
						<s:layout>
							<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10"/>
						</s:layout>
						<s:Label width="660" text="{getFmDesc(data)}"/>
					</s:ItemRenderer>
				</fx:Component>
			</s:itemRenderer>				
		</s:List>

		<s:HGroup horizontalAlign="right" width="100%">
			<fc:ExtendedButton id="closeBtn" label="Close" click="cancelHandler(event)"/>
		</s:HGroup>

	</s:Panel>

</fc:ExtendedNavigatorContent>
