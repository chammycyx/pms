package hk.org.ha.model.pms.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public class RemarkEntity extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "REMARK_TEXT", length = 100)
	@XmlTransient
	private String remarkText;
	
	@Column(name = "REMARK_CREATE_USER", length = 12)
	@XmlElement
	private String remarkCreateUser;
	
	@Column(name = "REMARK_CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlElement
	private Date remarkCreateDate;
	
	@Column(name = "REMARK_CONFIRM_USER", length = 12)
	@XmlElement
	private String remarkConfirmUser;
	
	@Column(name = "REMARK_CONFIRM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	@XmlElement
	private Date remarkConfirmDate;

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getRemarkCreateUser() {
		return remarkCreateUser;
	}

	public void setRemarkCreateUser(String remarkCreateUser) {
		this.remarkCreateUser = remarkCreateUser;
	}

	public Date getRemarkCreateDate() {
		if (remarkCreateDate == null) {
			return null;
		}
		else {
			return new Date(remarkCreateDate.getTime());
		}
	}

	public void setRemarkCreateDate(Date remarkCreateDate) {
		if (remarkCreateDate == null) {
			this.remarkCreateDate = null;
		}
		else {
			this.remarkCreateDate = new Date(remarkCreateDate.getTime());
		}
	}

	public String getRemarkConfirmUser() {
		return remarkConfirmUser;
	}

	public void setRemarkConfirmUser(String remarkConfirmUser) {
		this.remarkConfirmUser = remarkConfirmUser;
	}

	public Date getRemarkConfirmDate() {
		if (remarkConfirmDate == null) {
			return null;
		}
		else {
			return new Date(remarkConfirmDate.getTime());
		}
	}

	public void setRemarkConfirmDate(Date remarkConfirmDate) {
		if (remarkConfirmDate == null) {
			this.remarkConfirmDate = null;
		}
		else {
			this.remarkConfirmDate = new Date(remarkConfirmDate.getTime());
		}
	}

}
