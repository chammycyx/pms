<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:mx="library://ns.adobe.com/flex/mx">
	
	<fx:Metadata>
		[Name("mpCapdDrugPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.drug.popup.FreeTextEntryPopupProp;
			import hk.org.ha.event.pms.main.drug.popup.RetrieveMpCapdDrugCalciumEvent;
			import hk.org.ha.event.pms.main.drug.popup.RetrieveMpCapdDrugConcentrationEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
			import hk.org.ha.model.pms.dms.persistence.DmForm;
			import hk.org.ha.model.pms.dms.persistence.DmRoute;
			import hk.org.ha.model.pms.persistence.disp.MedOrder;
			import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
			import hk.org.ha.model.pms.udt.drug.DrugSearchSource;
			import hk.org.ha.model.pms.udt.vetting.ActionStatus;
			import hk.org.ha.model.pms.vo.rx.Dose;
			import hk.org.ha.model.pms.vo.rx.DoseGroup;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.events.IndexChangeEvent;

			// ------------------------------------------------			
			// drug search loading flag
			// ------------------------------------------------
			[Bindable]
			private var capdListLoading:Boolean = false;
			// ------------------------------------------------
			
			[Bindable]
			private var validInput:Boolean = false;
			
			private var drugSearchSource:DrugSearchSource;
			
			private var popupOkHandler:Function;

			private var popupCancelHandler:Function;
			
			[In] [Bindable]
			public var drugSearchCapdSupplierSystemList:ArrayCollection;

			[In] [Bindable]
			public var drugSearchCapdCalciumStrengthList:ArrayCollection;

			[In] [Bindable]
			public var drugSearchCapdConcentrationList:ArrayCollection;
			
			private var lastPopupDismissTime:Number;
			
			
			public function initMpCapdDrugPopup(_drugSearchSource:DrugSearchSource, _popupOkHandler:Function, _popupCancelHandler:Function):void {
				drugSearchSource = _drugSearchSource;
				popupOkHandler = _popupOkHandler;
				popupCancelHandler = _popupCancelHandler;
				
				capdListLoading = false;
				validInput = false;
				
				callLater(function():void {
					supplierSystemComboBox.setFocus();
					focusManager.showFocus();
				});
			}

			public function refreshMpCapdDrugPopup():void {
				capdListLoading = false;
			}
			
			private function concentrationCbxLabelFunction(item:Object):String {
				if (item != null && item.hasOwnProperty('concentration') ) {
					return item.concentration;
				} else {
					return "";
				}
			}			
			
			private function supplierSystemCbxCloseHandler():void {
				if (capdListLoading) {
					return;
				}
				
				calciumContentComboBox.selectedIndex = -1;
				concentrationComboBox.selectedIndex = -1;
				validInput = false;
				
				capdListLoading = true;
				dispatchEvent(new RetrieveMpCapdDrugCalciumEvent(drugSearchSource, supplierSystemComboBox.selectedItem));
			}
			
			private function supplierSystemCbxKeyDownHandler(event:KeyboardEvent):void {
				if ( event.keyCode == Keyboard.UP || event.keyCode == Keyboard.DOWN ) {
					supplierSystemComboBox.openDropDown();
				}
			}

			private function calciumContentCbxCloseHandler():void {
				if (capdListLoading) {
					return;
				}
				
				concentrationComboBox.selectedIndex = -1;
				validInput = false;
				
				capdListLoading = true;
				dispatchEvent(new RetrieveMpCapdDrugConcentrationEvent(drugSearchSource, supplierSystemComboBox.selectedItem, calciumContentComboBox.selectedItem));
			}
			
			private function calciumContentCbxKeyDownHandler(event:KeyboardEvent):void {
				if ( event.keyCode == Keyboard.UP || event.keyCode == Keyboard.DOWN ) {
					calciumContentComboBox.openDropDown();
				}
			}

			private function concentrationCbxKeyDownHandler(event:KeyboardEvent):void {
				if ( event.keyCode == Keyboard.UP || event.keyCode == Keyboard.DOWN ) {
					concentrationComboBox.openDropDown();
				}
			}
			
			private function concentrationCbxKeyUpHandler(evt:KeyboardEvent):void {
				if (evt.keyCode == Keyboard.ENTER) {
					if ( ! PopupUtil.isDiscardKeyEventForPopup(lastPopupDismissTime) ) {
						if (okBtn.enabled) {
							okHandler();
						}
					}
				}
			}

			private function concentrationCbxChangeHandler(event:IndexChangeEvent):void {
				validInput = validateInput();
			}
			
			private function validateInput():Boolean {
				// this function is used to refresh enable flag of concentrationComboBox once its selected index is changed
				
				if (supplierSystemComboBox.selectedItem == null || supplierSystemComboBox.selectedIndex < 0) {
					return false;
				}
				
				if (calciumContentComboBox.selectedItem == null || calciumContentComboBox.selectedIndex < 0) {
					return false;
				}
				
				if (concentrationComboBox.selectedItem == null || concentrationComboBox.selectedIndex < 0) {
					return false;
				}
				
				return true;
			}
			
			private function okHandler():void {
				if (capdListLoading) {
					return;
				}
				
				PopUpManager.removePopUp(this);
				
				popupOkHandler(supplierSystemComboBox.selectedItem, calciumContentComboBox.selectedItem, concentrationComboBox.selectedItem);
			}
			
			private function cancelHandler():void {
				if (capdListLoading) {
					return;
				}
				
				PopUpManager.removePopUp(this);
				
				if (popupCancelHandler != null) {
					popupCancelHandler();
				}					
			}
		]]>
		
	</fx:Script>
	
	<fx:Declarations>
		<s:RadioButtonGroup id="actionStatus"/>
	</fx:Declarations>
	
	<s:Panel title="CAPD" width="600">
		<s:layout>
			<s:VerticalLayout paddingTop="10" paddingBottom="10" paddingLeft="10" paddingRight="10" gap="10"/>			
		</s:layout>					
		
		<s:HGroup verticalAlign="middle" width="100%">
			<s:Label width="200" text="Manufacturer and Type"/>
			<fc:AdvancedDropDownList id="supplierSystemComboBox" dataProvider="{drugSearchCapdSupplierSystemList}" width="250"  
									 enabled="{ ! capdListLoading}"  typographicCase="uppercase" 
									 close="supplierSystemCbxCloseHandler()"
									 keyDown="supplierSystemCbxKeyDownHandler(event)" />	
		</s:HGroup>
		
		<s:HGroup verticalAlign="middle" width="100%">
			<s:Label width="200" text="Calcium Content"/>
			<fc:AdvancedDropDownList id="calciumContentComboBox" dataProvider="{drugSearchCapdCalciumStrengthList}" width="250"
									 enabled="{ ! capdListLoading}"  typographicCase="uppercase"
									 close="calciumContentCbxCloseHandler()"
									 keyDown="calciumContentCbxKeyDownHandler(event)"/>		
		</s:HGroup>
		
		<s:HGroup verticalAlign="middle" width="100%">
			<s:Label width="200" text="Concentration / Ingredient / Volume"/>
			<fc:AdvancedDropDownList id="concentrationComboBox" dataProvider="{drugSearchCapdConcentrationList}" width="370"
									 enabled="{ ! capdListLoading}"   
									 labelFunction="concentrationCbxLabelFunction"
									 keyDown="concentrationCbxKeyDownHandler(event)"
									 keyUp="concentrationCbxKeyUpHandler(event)"
									 change="concentrationCbxChangeHandler(event)"/>
		</s:HGroup>
		
		<s:HGroup width="100%" height="100%" verticalAlign="bottom" horizontalAlign="right"
				  paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="10">						
			<fc:ExtendedButton id="okBtn" label="OK"  enabled="{ ! capdListLoading &amp;&amp; validInput}" click="okHandler()"/>
			<fc:ExtendedButton id="cancelBtn" label="Cancel"  enabled="{ ! capdListLoading}" click="cancelHandler()"/>								
		</s:HGroup>		
		
	</s:Panel>	
	
</fc:ExtendedNavigatorContent>
