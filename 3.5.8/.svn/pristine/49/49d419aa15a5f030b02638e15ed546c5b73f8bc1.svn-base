package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.DataType;
import hk.org.ha.model.pms.udt.reftable.MaintScreen;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "PROP")
public class Prop extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propSeq")
	@SequenceGenerator(name = "propSeq", sequenceName = "SQ_PROP", initialValue = 100000000)
	private Long id;
	
	@Column(name = "NAME", nullable = false, length = 100)
	private String name;
	
	@Column(name = "DESCRIPTION", nullable = false, length = 1000)
	private String description;
	
	@Column(name = "VALIDATION", nullable = false, length = 100)
	private String validation;
	
	@Converter(name = "Prop.type", converterClass = DataType.Converter.class)
    @Convert("Prop.type")
	@Column(name = "TYPE", nullable = false, length = 2)
	private DataType type;
	
	@Column(name = "DEF_VALUE", length = 1000)
	private String defValue;
	
	@Column(name = "CACHE_IN_SESSION_FLAG", nullable = false)
	private Boolean cacheInSessionFlag;
	
	@Column(name = "CACHE_EXPIRE_TIME", nullable = false)
	private Integer cacheExpireTime;
	
	@Converter(name = "Prop.maintScreen", converterClass = MaintScreen.Converter.class)
    @Convert("Prop.maintScreen")
	@Column(name = "MAINT_SCREEN", nullable = false, length = 1)
	private MaintScreen maintScreen;
	
	@Converter(name = "Prop.hideInOperationProfileType", converterClass = StringCollectionConverter.class)
	@Convert("Prop.hideInOperationProfileType")
	@Column(name = "HIDE_IN_OPERATION_PROFILE_TYPE", length = 100)
	private List<String> hideInOperationProfileType;
	
	@Column(name = "SYNC_FLAG", nullable = false)
	private Boolean syncFlag;
	
	@Column(name = "REMINDER_MESSAGE", length = 1000)
	private String reminderMessage;

	@Transient
	private List<OperationProfileType> hideInOperationProfileTypeList;
	
	@PostLoad
	public void postLoad() {
		if ( !hideInOperationProfileType.isEmpty() ) {
			hideInOperationProfileTypeList = new ArrayList<OperationProfileType>();
			for (String type : hideInOperationProfileType){
				hideInOperationProfileTypeList.add(OperationProfileType.dataValueOf(type));
			}
		}
	}
	
	public Prop() {
		cacheInSessionFlag = Boolean.TRUE;
		cacheExpireTime = Integer.valueOf(60);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DataType getType() {
		return type;
	}

	public void setType(DataType type) {
		this.type = type;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}
	
	public String getDefValue() {
		return defValue;
	}

	public void setDefValue(String defValue) {
		this.defValue = defValue;
	}

	public Boolean getCacheInSessionFlag() {
		return cacheInSessionFlag;
	}

	public void setCacheInSessionFlag(Boolean cacheInSessionFlag) {
		this.cacheInSessionFlag = cacheInSessionFlag;
	}

	public Integer getCacheExpireTime() {
		return cacheExpireTime;
	}

	public void setCacheExpireTime(Integer cacheExpireTime) {
		this.cacheExpireTime = cacheExpireTime;
	}

	public void setHideInOperationProfileType(
			List<String> hideInOperationProfileType) {
		this.hideInOperationProfileType = hideInOperationProfileType;
	}

	public List<String> getHideInOperationProfileType() {
		return hideInOperationProfileType;
	}

	public void setMaintScreen(MaintScreen maintScreen) {
		this.maintScreen = maintScreen;
	}

	public MaintScreen getMaintScreen() {
		return maintScreen;
	}

	public void setHideInOperationProfileTypeList(
			List<OperationProfileType> hideInOperationProfileTypeList) {
		this.hideInOperationProfileTypeList = hideInOperationProfileTypeList;
	}

	public List<OperationProfileType> getHideInOperationProfileTypeList() {
		return hideInOperationProfileTypeList;
	}

	public void setSyncFlag(Boolean syncFlag) {
		this.syncFlag = syncFlag;
	}

	public Boolean getSyncFlag() {
		return syncFlag;
	}

	public String getReminderMessage() {
		return reminderMessage;
	}

	public void setReminderMessage(String reminderMessage) {
		this.reminderMessage = reminderMessage;
	}
}
