<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
							 xmlns:fx="http://ns.adobe.com/mxml/2009"
							 xmlns:s="library://ns.adobe.com/flex/spark"
							 xmlns:mx="library://ns.adobe.com/flex/mx"
							 xmlns:mdcs="com.iwobanas.controls.*"
							 xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
							 width="420" height="300">

	<fx:Metadata>
		[Name("RefillModuleMaintDrsPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.controls.dataGridClasses.DataGridColumn;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			
			import spark.events.IndexChangeEvent;
			
			import hk.org.ha.event.pms.main.reftable.popup.RefillModuleMaintDrsPopupProp;
			import hk.org.ha.event.pms.main.reftable.popup.RetrievePasSpecialtyListForDrsConfigEvent
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopup;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.persistence.reftable.DrsConfig;
			import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
			import hk.org.ha.model.pms.vo.patient.PasSpecialty;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[Bindable]
			public var drsConfig:DrsConfig;
			
			private var otherDrsConfigList:ArrayCollection = new ArrayCollection();
			
			[Bindable]
			public var pasSpecialtyList:ArrayCollection = new ArrayCollection();
			
			[Bindable]
			public var patientTypeList:ArrayCollection = new ArrayCollection([PasSpecialtyType.OutPatient, PasSpecialtyType.InPatient]);
			
			private var _okHandler:Function; 
			
			private function okHandler(evt:MouseEvent):void {											
				drsConfig.pasSpecCode = specialtyTextInput.text;
				if (!drsConfigValidation()) {
					return;
				} else {
					_okHandler(drsConfig);
					PopUpManager.removePopUp(this);
					
				}
			}

			private function cancelHandler(evt:MouseEvent):void {
				PopUpManager.removePopUp(this);
			}
			
			private function lookupShortcutHandler(evt:KeyboardEvent):void {
				if (evt.charCode == 53 && evt.ctrlKey && specialtyLookupBtn.enabled){
					initLookupPopup();
				}
			}
			
			private function initLookupPopup():void {
				dispatchEvent(new RetrievePasSpecialtyListForDrsConfigEvent(specialtyTextInput.text, patientTypeDropDownList.selectedItem, updatePasSpecialtyList));
			}
			
			private function updatePasSpecialtyList(pasSpecialtyList:ArrayCollection):void {
				this.pasSpecialtyList = pasSpecialtyList;
				initLookupPopupCallBack();
			}
			
			public function initLookupPopupCallBack():void {
				var lookupPopProp:LookupPopupProp = new LookupPopupProp();	
				
				lookupPopProp.doubleClickHandler = function(evt:ListEvent):void {
					var lookupPopup:LookupPopup = (UIComponent(evt.currentTarget).parentDocument) as LookupPopup;
					var selectedItem:PasSpecialty = evt.itemRenderer.data as PasSpecialty;
					
					specialtyTextInput.text = selectedItem.pasSpecCode;
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				};
				
				lookupPopProp.title = "PAS Specialty Lookup";
				lookupPopProp.lookupWinHeight=350;
				lookupPopProp.lookupWinWidth=600;
				var typeCol:DataGridColumn = LookupPopupProp.getColumn("Type","type");
				var specCodeCol:DataGridColumn = LookupPopupProp.getColumn("Spec. Code","pasSpecCode");
				var specDescCol:DataGridColumn = LookupPopupProp.getColumn("Description","pasDescription");
				typeCol.width = 48;
				typeCol.minWidth = 48;
				specCodeCol.width = 72;
				specCodeCol.minWidth = 75;
				specDescCol.minWidth = 145;
				
				typeCol.sortable = false;
				specCodeCol.sortable = false;
				specDescCol.sortable = false;
				
				typeCol.resizable = false;
				specCodeCol.resizable = false;
				
				typeCol.labelFunction = pasSpecTypeLabelFunc;
				lookupPopProp.columns = [typeCol, specCodeCol, specDescCol];
				lookupPopProp.dataProvider = pasSpecialtyList;	
				
				dispatchEvent(new ShowLookupPopupEvent(lookupPopProp));
			}
			
			public function updateDrsConfigPopupDisplay(popupProp:RefillModuleMaintDrsPopupProp):void {
				this.drsConfig = popupProp.drsConfig;
				this.pasSpecialtyList = popupProp.pasSpecialtyForDrsConfigList;
				this.otherDrsConfigList = popupProp.otherDrsConfigList;
				_okHandler = popupProp.okHandler;
				if (this.drsConfig.pasSpecCode != null) {
					drsConfigPopupPanel.title = "Edit DRS Setting";
				} else {
					drsConfigPopupPanel.title = "Add DRS Setting";
				}
				callLater(patientTypeDropDownList.setFocus);
				callLater(focusManager.showFocus);
			}
			
			private function pasSpecTypeLabelFunc(item:Object, column:DataGridColumn):String 
			{
				return item[column.dataField].displayValue;
			}
			
			private function patientTypeDropDownListLabelFunc(item:Object):String 
			{
				var displayName:String = null;
				if (item.displayValue == "O") {
					displayName = "Out-Patient";
				} else if (item.displayValue == "I") {
					displayName = "In-Patient";
				}
				return displayName;
			}
			

			private function patientTypeDropDownListChangeHandler(event:IndexChangeEvent):void {
				if (patientTypeDropDownList.selectedItem != null) {
					drsConfig.type = patientTypeDropDownList.selectedItem.displayValue;
					specialtyTextInput.text = "";
				}
			}
			
			private function drsConfigValidation():Boolean {
				var specCodePass:Boolean = false;
				var okHandler:Function;
				if (specialtyTextInput.text == '' || specialtyTextInput.text == null) {			
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						specialtyTextInput.setFocus();
					}					
					showSystemMessage("0420", null, null, null, okHandler);
					return false;
				}
				
				for each ( var pasSpecialty:PasSpecialty in pasSpecialtyList) {
					if (specialtyTextInput.text == pasSpecialty.pasSpecCode && drsConfig.type == pasSpecialty.type.displayValue) {
						specCodePass = true;
						break;
					}
				}
				if (!specCodePass) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						specialtyTextInput.setFocus();
					}				
					showSystemMessage("0025", null, null, null, okHandler);
					return false;
				}								
				if (itemCountDrsTextInput.text=='' || isNaN(Number(itemCountDrsTextInput.text))) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						itemCountDrsTextInput.setFocus();
					}				
					showSystemMessage("1058", null, null, null, okHandler);
					return false;
				}
				if (itemCountDrsTextInput.text=="0") {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						itemCountDrsTextInput.setFocus();
					}			
					showSystemMessage("1062", null, null, null, okHandler);
					return false;
				}
				if (splitDrugItemAfterDrsTextInput.text=='' || isNaN(Number(splitDrugItemAfterDrsTextInput.text))) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						splitDrugItemAfterDrsTextInput.setFocus();
					}				
					showSystemMessage("1060", null, null, null, okHandler);
					return false;
				}
				if (splitDrugItemAfterDrsTextInput.text=="0") {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						splitDrugItemAfterDrsTextInput.setFocus();
					}			
					showSystemMessage("1063", null, null, null, okHandler);
					return false;
				}
				if (refillIntervalDrsTextInput.text=='' || isNaN(Number(refillIntervalDrsTextInput.text))) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						refillIntervalDrsTextInput.setFocus();
					}					
					showSystemMessage("1059", null, null, null, okHandler);
					return false;
				}
				if (refillIntervalDrsTextInput.text=="0") {		
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						refillIntervalDrsTextInput.setFocus();
					}					
					showSystemMessage("1064", null, null, null, okHandler);
					return false;
				}
				if (lastIntervalMaxDayDrsTextInput.text=='' || isNaN(Number(lastIntervalMaxDayDrsTextInput.text))) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						lastIntervalMaxDayDrsTextInput.setFocus();
					}					
					showSystemMessage("1061", null, null, null, okHandler);
					return false;
				}
				if (lastIntervalMaxDayDrsTextInput.text=="0") {		
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						lastIntervalMaxDayDrsTextInput.setFocus();
					}					
					showSystemMessage("1065", null, null, null, okHandler);
					return false;
				}
				
				if (Number(refillIntervalDrsTextInput.text) > Number(splitDrugItemAfterDrsTextInput.text)) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						refillIntervalDrsTextInput.setFocus();
					}					
					showSystemMessage("1066", null, null, null, okHandler);
					return false;
				}
				if (Number(refillIntervalDrsTextInput.text) > Number(lastIntervalMaxDayDrsTextInput.text)) {
					okHandler = function(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						refillIntervalDrsTextInput.setFocus();
					}					
					showSystemMessage("1067", null, null, null, okHandler);
					return false;
				}
							
				for each (var tmpDrsConfig:DrsConfig in otherDrsConfigList){
					if (tmpDrsConfig.pasSpecCode == drsConfig.pasSpecCode && tmpDrsConfig.type == drsConfig.type){
						showSystemMessage("1068", null, null, null, null);
						callLater(specialtyTextInput.setFocus);
						return false;
					}		
				}
				return true;
			}
			
			public function showSystemMessage(errorCode:String, yesfunc:Function=null, noFunc:Function=null, paramList:Array=null, okFunc:Function=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				if (paramList!=null) {
					msgProp.messageParams = paramList;
				}		
				if (noFunc != null) {
					msgProp.noHandler = noFunc;
				}
				
				if (yesfunc != null) {
					msgProp.setYesDefaultButton = false;
					msgProp.setNoDefaultButton = true;
					msgProp.setYesNoButton = true;
					msgProp.yesHandler = yesfunc;				
				} else {
					msgProp.setOkButtonOnly = true;
					if (okFunc != null) {
						msgProp.okHandler = okFunc;
					}
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}			
			
		]]>
	</fx:Script>

	<s:Panel id="drsConfigPopupPanel" width="100%" height="100%">
		<s:layout>
			<s:VerticalLayout gap="10" paddingBottom="10" paddingLeft="10" paddingRight="10"
							  paddingTop="10"/>
		</s:layout>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="Patient Type"/>
			<fc:AdvancedDropDownList id="patientTypeDropDownList" width="112"
									 change="patientTypeDropDownListChangeHandler(event)"
									 dataProvider="{patientTypeList}"
									 labelFunction="patientTypeDropDownListLabelFunc"
									 selectedItem="{PasSpecialtyType.dataValueOf(drsConfig.type)}"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="PAS Specialty"/>
			<fc:UppercaseTextInput id="specialtyTextInput" width="79"
								   keyUp="lookupShortcutHandler(event)" maxChars="4"
								   text="@{drsConfig.pasSpecCode}"/>		
			<fc:LookupButton id="specialtyLookupBtn" width="28" click="initLookupPopup()"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="MR Reminder"/>
			<s:CheckBox id="mrReminderCheckBox" selected="@{drsConfig.mrReminderFlag}"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="Number of Item Prescribed"/>
			<fc:NumericInput id="itemCountDrsTextInput" width="50" allowNegative="false"
							 numberFormat="3.0" value="@{drsConfig.itemCount}"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="Split Drug Item after (DRS)"/>
			<fc:NumericInput id="splitDrugItemAfterDrsTextInput" width="50" allowNegative="false"
							 numberFormat="3.0" value="@{drsConfig.threshold}"/>
			<s:Label text="day(s)"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="Refill Interval (DRS)"/>
			<fc:NumericInput id="refillIntervalDrsTextInput" width="50" allowNegative="false"
							 numberFormat="3.0" value="@{drsConfig.interval}"/>
			<s:Label text="day(s)"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label width="250" text="Maximum Interval for the Last Refill (DRS)"/>
			<fc:NumericInput id="lastIntervalMaxDayDrsTextInput" width="50" allowNegative="false"
							 numberFormat="3.0" value="@{drsConfig.lastIntervalMaxDay}"/>
			<s:Label text="day(s)"/>
		</s:HGroup>

		
		<s:HGroup width="100%" horizontalAlign="right">
			<fc:ExtendedButton id="okBtn" label="OK" click="okHandler(event)"/>
			<fc:ExtendedButton id="cancelBtn" label="Cancel" click="cancelHandler(event)"/>
		</s:HGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>
