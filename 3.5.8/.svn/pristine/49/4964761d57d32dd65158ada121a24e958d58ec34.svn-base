package hk.org.ha.model.pms.persistence.pivas;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "PIVAS_PREP_METHOD_ITEM")
public class PivasPrepMethodItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pivasPrepMethodItemSeq")
	@SequenceGenerator(name = "pivasPrepMethodItemSeq", sequenceName = "SQ_PIVAS_PREP_METHOD_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "DRUG_ITEM_CODE", length = 20)
	private String drugItemCode;

	@Column(name = "DRUG_MANUF_CODE", length = 20)
	private String drugManufCode;
	
	@Column(name = "DRUG_BATCH_NUM", length = 20)
	private String drugBatchNum;
	
	@Column(name = "DRUG_EXPIRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date drugExpiryDate;

	@Column(name = "SOLVENT_CODE", length = 25)
	private String solventCode;
	
	@Column(name = "SOLVENT_DESC", length = 100)
	private String solventDesc;
	
	@Column(name = "SOLVENT_ITEM_CODE", length = 20)
	private String solventItemCode;

	@Column(name = "SOLVENT_MANUF_CODE", length = 20)
	private String solventManufCode;
	
	@Column(name = "SOLVENT_BATCH_NUM", length = 20)
	private String solventBatchNum;
	
	@Column(name = "SOLVENT_EXPIRY_DATE")
	@Temporal(TemporalType.DATE)
	private Date solventExpiryDate;
	
	@Column(name = "DRUG_CAL_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal drugCalQty;
	
	@Column(name = "DRUG_DISP_QTY", nullable = false, precision = 19, scale = 4)
	private BigDecimal drugDispQty;
	
	@ManyToOne
	@JoinColumn(name = "PIVAS_PREP_METHOD_ID", nullable = false)
	private PivasPrepMethod pivasPrepMethod;

	@Column(name = "PIVAS_PREP_METHOD_ID", insertable = false, updatable = false)
	private Long pivasPrepMethodId;
	
	@Column(name = "PIVAS_FORMULA_METHOD_DRUG_ID", nullable = false)
	private Long pivasFormulaMethodDrugId;
	
	@Transient
	private DmDrug drugDmDrug;

	@Transient
	private PivasDrugManuf solventPivasDrugManuf;
	
	public PivasPrepMethodItem() {
		drugCalQty = BigDecimal.valueOf(0);
		drugDispQty = BigDecimal.valueOf(0);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDrugItemCode() {
		return drugItemCode;
	}

	public void setDrugItemCode(String drugItemCode) {
		this.drugItemCode = drugItemCode;
	}

	public String getDrugManufCode() {
		return drugManufCode;
	}

	public void setDrugManufCode(String drugManufCode) {
		this.drugManufCode = drugManufCode;
	}

	public String getDrugBatchNum() {
		return drugBatchNum;
	}

	public void setDrugBatchNum(String drugBatchNum) {
		this.drugBatchNum = drugBatchNum;
	}

	public Date getDrugExpiryDate() {
		return drugExpiryDate == null ? null : new Date(drugExpiryDate.getTime());
	}

	public void setDrugExpiryDate(Date drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate == null ? null : new Date(drugExpiryDate.getTime());
	}

	public String getSolventCode() {
		return solventCode;
	}

	public void setSolventCode(String solventCode) {
		this.solventCode = solventCode;
	}

	public String getSolventDesc() {
		return solventDesc;
	}

	public void setSolventDesc(String solventDesc) {
		this.solventDesc = solventDesc;
	}

	public String getSolventItemCode() {
		return solventItemCode;
	}

	public void setSolventItemCode(String solventItemCode) {
		this.solventItemCode = solventItemCode;
	}

	public String getSolventManufCode() {
		return solventManufCode;
	}

	public void setSolventManufCode(String solventManufCode) {
		this.solventManufCode = solventManufCode;
	}

	public String getSolventBatchNum() {
		return solventBatchNum;
	}

	public void setSolventBatchNum(String solventBatchNum) {
		this.solventBatchNum = solventBatchNum;
	}

	public Date getSolventExpiryDate() {
		return solventExpiryDate == null ? null : new Date(solventExpiryDate.getTime());
	}

	public void setSolventExpiryDate(Date solventExpiryDate) {
		this.solventExpiryDate = solventExpiryDate == null ? null : new Date(solventExpiryDate.getTime());
	}

	public BigDecimal getDrugCalQty() {
		return drugCalQty;
	}

	public void setDrugCalQty(BigDecimal drugCalQty) {
		this.drugCalQty = drugCalQty;
	}

	public BigDecimal getDrugDispQty() {
		return drugDispQty;
	}

	public void setDrugDispQty(BigDecimal drugDispQty) {
		this.drugDispQty = drugDispQty;
	}

	public PivasPrepMethod getPivasPrepMethod() {
		return pivasPrepMethod;
	}

	public void setPivasPrepMethod(PivasPrepMethod pivasPrepMethod) {
		this.pivasPrepMethod = pivasPrepMethod;
	}

	public Long getPivasPrepMethodId() {
		return pivasPrepMethodId;
	}

	public void setPivasPrepMethodId(Long pivasPrepMethodId) {
		this.pivasPrepMethodId = pivasPrepMethodId;
	}

	public Long getPivasFormulaMethodDrugId() {
		return pivasFormulaMethodDrugId;
	}

	public void setPivasFormulaMethodDrugId(Long pivasFormulaMethodDrugId) {
		this.pivasFormulaMethodDrugId = pivasFormulaMethodDrugId;
	}

	public DmDrug getDrugDmDrug() {
		return drugDmDrug;
	}

	public void setDrugDmDrug(DmDrug drugDmDrug) {
		this.drugDmDrug = drugDmDrug;
	}

	public PivasDrugManuf getSolventPivasDrugManuf() {
		return solventPivasDrugManuf;
	}

	public void setSolventPivasDrugManuf(PivasDrugManuf solventPivasDrugManuf) {
		this.solventPivasDrugManuf = solventPivasDrugManuf;
	}	
}
