package hk.org.ha.model.pms.udt.reftable;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum HomeLeaveMappingAction implements StringValuedEnum {
	
	Insert("A", "Insert"),
	Update("C", "Update"),
	Delete("D", "Delete");	
	
    private final String dataValue;
    private final String displayValue;
        
    HomeLeaveMappingAction(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static HomeLeaveMappingAction dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(HomeLeaveMappingAction.class, dataValue);
	}
        
	public static class Converter extends StringValuedEnumConverter<HomeLeaveMappingAction> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<HomeLeaveMappingAction> getEnumClass() {
    		return HomeLeaveMappingAction.class;
    	}
    }
}
