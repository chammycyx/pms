package hk.org.ha.model.pms.biz.inbox;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;

@AutoCreate
@Stateless
@Name("categorySummary")
@MeasureCalls
public class CategorySummaryBean implements CategorySummaryLocal {

	@In
	private CategorySummarySupportLocal categorySummarySupport; 

	@Out(scope=ScopeType.SESSION, required=false)
	protected Long newCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long verifyCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long exceptionCount;

	@Out(scope=ScopeType.SESSION, required=false)
	protected Long dischargeMedOrderCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long cancelDischargeTransferCount;

	@Out(scope=ScopeType.SESSION, required=false)
	protected Long normalCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long urgentCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long pendingSuspendCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long pendingCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long suspendCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Long overridePendingCount;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Date firstUrgentDueDate;
	
	@Out(scope=ScopeType.SESSION, required=false)
	protected Date firstPendingSuspendDueDate;

    @Override
	public void countCategories(MedProfileStatAdminType adminType, Workstore workstore, Boolean showChemoProfileOnly, List<String> wardCodeList) {

    	Date now = new Date();
    	
		List<CategoryCount> orderTypeCounts = categorySummarySupport.retrieveOrderTypeCategoryCountList(workstore, showChemoProfileOnly, wardCodeList, now);
		List<CategoryCount> adminTypeCounts = categorySummarySupport.retrieveAdminTypeCategoryCountList(workstore, showChemoProfileOnly, wardCodeList, now);
		
		newCount = 0L;
		verifyCount = 0L;
		exceptionCount = categorySummarySupport.countErrorStat(workstore, wardCodeList, now);
		dischargeMedOrderCount = categorySummarySupport.countDischargeMedOrder(workstore, wardCodeList, now);
		cancelDischargeTransferCount = categorySummarySupport.countCancelDischargeTransferCount(workstore);
		normalCount = 0L;
		urgentCount = 0L;
		pendingSuspendCount = 0L;
		pendingCount = 0L;
		suspendCount = 0L;
		overridePendingCount = 0L;
		firstUrgentDueDate = null;
		firstPendingSuspendDueDate = null;

		for (CategoryCount count: adminTypeCounts) {
			assignAdminTypeCategoryCount(count);
		}
		for (CategoryCount count: orderTypeCounts) {
			assignOrderTypeCategoryCount(adminType, count);
		}
	}
    	
	private void assignAdminTypeCategoryCount(CategoryCount count) {
		
		if (count.getAdminType() == MedProfileStatAdminType.NewArrival) {
			newCount = count.getCount();
		} else if (count.getAdminType() == MedProfileStatAdminType.Verify) {
			verifyCount = count.getCount();
		}
	}
	
	private void assignOrderTypeCategoryCount(MedProfileStatAdminType adminType, CategoryCount count) {
		
		if (count.getAdminType() == adminType) {
			if (count.getOrderType() == MedProfileStatOrderType.Normal) {
				normalCount = count.getCount();
			} else if (count.getOrderType() == MedProfileStatOrderType.Urgent) {
				urgentCount = count.getCount();
				if (count.getStatCount() <= 0) {
					firstUrgentDueDate = count.getFirstDueDate();
				}
			} else if (count.getOrderType() == MedProfileStatOrderType.PendingSuspend) {
				pendingSuspendCount = count.getCount();
				pendingCount = count.getPendingCount();
				suspendCount = count.getSuspendCount();
				overridePendingCount = count.getOverridePendingCount();
				if (count.getStatCount() <= 0) {
					firstPendingSuspendDueDate = count.getFirstDueDate();
				}
			}
		}
	}
    
}
