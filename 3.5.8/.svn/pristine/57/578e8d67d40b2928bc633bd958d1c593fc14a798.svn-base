<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009"
				 xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
				 xmlns:s="library://ns.adobe.com/flex/spark" 		
				 xmlns:mx="library://ns.adobe.com/flex/mx"
				 xmlns="hk.org.ha.view.pms.main.vetting.mo.*"
				 xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
				 keyUp="shortcutKey(event)">		
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.vetting.CalSubTotalDosageForDangerousDrugEvent;
			import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
			import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
			import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
			import hk.org.ha.model.pms.vo.rx.Dose;
			import hk.org.ha.model.pms.vo.rx.DoseGroup;
			import hk.org.ha.model.pms.vo.rx.RxItem;
			import hk.org.ha.model.pms.vo.rx.Site;
			import hk.org.ha.model.pms.vo.vetting.MedOrderEditUiInfo;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewInfo;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewUtil;
			
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.core.UIComponent;
			import mx.utils.StringUtil;
			
			import spark.events.DropDownEvent;
			import spark.events.IndexChangeEvent;
			
			[Bindable]
			public var orderEditViewInfo:OrderEditViewInfo;			
			[Bindable]
			public var doseGroupList:ListCollectionView;			
			[Bindable]
			public var doseGroup:DoseGroup;					
			[Bindable]
			public var doseList:ListCollectionView;				
			[Bindable]
			public var dosageTabIndex:int;
			[Bindable]
			public var dosageStartTabIndex:int;
			[Bindable]
			public var dosageEndTabIndex:int;
			[Bindable]
			public var dosageUnitTabIndex:int;
			[Bindable]
			public var dailyFreqTabIndex:int;
			[Bindable]
			public var supplFreqTabIndex:int;
			[Bindable]
			public var adminTimeTabIndex:int;
			[Bindable]
			public var prnTabIndex:int;
			[Bindable]
			public var prnPercentTabIndex:int;
			[Bindable]
			public var siteTabIndex:int;
			
			public var retainDescFlag:Boolean 	= false;
			public var dailyFreqDispText:String = "";					
			public var supplFreqDispText:String = "";
			public var siteDispText:String		= "";
			
			[Bindable]
			private var isFirstDoseFlag:Boolean;
			[Bindable]
			private var doseGroupIndex:Number;
			[Bindable]
			private var doseIndex:Number;			
			[Bindable]
			private var dosageTextStyle:String;
			[Bindable]
			private var prnCbxFocusFlag:Boolean = false;
			[Bindable]
			private var siteToolTip:String;
			
			public override function set data(value:Object):void
			{		
				try {
					if ( !value ) {
						return;
					}
					
					super.data = value;
					var dose:Dose = value as Dose;
					if (orderEditViewInfo && orderEditViewInfo.medOrder) {
						if (orderEditViewInfo.medOrder.isMpDischarge()) {
							siteToolTip = dose.ipSite.siteLabelDesc;
						} else {
							siteToolTip = dose.site.siteLabelDesc;
						}
					}
					if ( !data.dmDailyFrequency ) {
						data.dmDailyFrequency = new DmDailyFrequency;
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			/* Common function handler*/
			public function commonUpdate():void
			{			
				try {					
					if ( orderEditViewInfo.medOrderItem.dangerDrugFlag ) {
						if ( data.prn && (data.prnPercent == null || data.prnPercent == PrnPercentage.Zero) ) {
							OrderEditViewUtil.updatePrnPercentage(orderEditViewInfo, data as Dose, doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary));
						}
						calDispQtyForDangerDrug();
					}
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			public function calDispQtyForDangerDrug():void
			{				
				try {
					if ( !orderEditViewInfo.medOrderItem.dangerDrugFlag || isNaN(doseGroupIndex) || isNaN(doseIndex) ) {
						return;
					}
					if ( (data as Dose).checkFreqParam(orderEditViewInfo.sysMsgMap,false) ) {
						return;
					}
					dispatchEvent( new CalSubTotalDosageForDangerousDrugEvent(orderEditViewInfo.medOrderItem.rxDrug as RxItem, doseGroupIndex, doseIndex, updateMoIssueQty) );
				} catch (e:Error) {
					trace(e);
				}
			}
			
			private function updateMoIssueQty(value:ListCollectionView):void
			{				
				var dispQtyText:String = String(value.getItemAt(0) as Number);
				if ( dispQtyText.length > 4 ) {
					data.dispQtyText = "";
				} else {
					data.dispQtyText = dispQtyText;
				}
				if ( data.baseUnit != "DOSE" ) {
					data.baseUnit = "DOSE";
				}
				
				orderEditViewInfo.updateMoView();
			}
			
			protected function changingHandler(event:IndexChangeEvent):void
			{							
				if ( event.newIndex < 0 ) {
					event.preventDefault();
				}
			}
			
			/* Dosage function handler PMSU-2838, PMSU-2464*/
			protected function dosageTextInputFocusChangeHandler(event:FocusEvent):void
			{
				var dose:Dose = data as Dose;
				if ( data == null ) {
					return;
				}
				if ( StringUtil.trim(dosageTextInput.text) != dose.dosageText ) {
					
					dose.dosageText = dosageTextInput.text;
					if ( orderEditViewInfo.medOrderItem.isFreeTextEntryFlag ) {
						dose.outstandingDosageFlag = dose.checkDosageForFreeTextEntry(orderEditViewInfo.isMoiEditFlag,true,orderEditViewInfo.sysMsgMap, true);
					} else {
						dose.outstandingDosageFlag = dose.checkDosage(true,orderEditViewInfo.medOrderItem.dosageCompulsory,orderEditViewInfo.sysMsgMap, true);
					}
					orderEditViewInfo.orderEditView.manualErrorString = dose.dosageErrorString;
					if ( dose.dosageErrorString != null ) {
						orderEditViewInfo.customizedCallLater(dosageTextInput.setFocus,200);
					}
					orderEditViewInfo.updateMoView();
				} else {
					if ( dose.dosageErrorString != null ) {
						dose.dosageErrorString = null;
						orderEditViewInfo.orderEditView.manualErrorString = null;
					}
				}
			}
			
			protected function dosageTextInputKeyDownHandler(event:KeyboardEvent):void
			{
				if ( event.keyCode == Keyboard.ENTER ) {
					orderEditViewInfo.focusedObject = dosageTextInput;
					orderEditViewInfo.orderEditView.stage.focus = null;
					dosageTextInputFocusChangeHandler(new FocusEvent(FocusEvent.KEY_FOCUS_CHANGE));
				}
			}
			
			protected function dosageUnitFocusChangeHandler(evt:FocusEvent):void 
			{
				var dose:Dose = data as Dose;
				if ( dose.dosageUnit != StringUtil.trim(dosageUnitTextInput.text) ) {
					orderEditViewInfo.moDosageUnit = StringUtil.trim(dosageUnitTextInput.text);
					dose.dosageUnit = orderEditViewInfo.moDosageUnit;
					orderEditViewInfo.medOrderItem.regimen.updateDosageUnit(orderEditViewInfo.moDosageUnit);
					orderEditViewInfo.updateMoView();					
				}
				dose.dosageUnitErrorString = null;
				orderEditViewInfo.orderEditView.manualErrorString = null;
			}
			
			protected function dosageUnitKeyDownHandler(event:KeyboardEvent):void
			{
				if ( event.keyCode == Keyboard.ENTER ) {
					orderEditViewInfo.focusedObject = dosageUnitTextInput;
					orderEditViewInfo.orderEditView.stage.focus = null;
					dosageUnitFocusChangeHandler(new FocusEvent(FocusEvent.KEY_FOCUS_CHANGE));
				}
			}
			
			/* DailyFreq function handler */
			protected function dailyFreqLabelFunction(item:Object):String
			{
				try {
					return OrderEditViewUtil.dailyFreqLabelFunc(item, data as Dose, dailyFreqCbx, this);
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			protected function dailyFreqEqualFunction(dailyFreq1:Object, dailyFreq2:Object):Boolean
			{				
				try {
					return DmDailyFrequency(dailyFreq1).dailyFreqCode == DmDailyFrequency(dailyFreq2).dailyFreqCode;
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
				return false;
			}
			
			protected function dailyFreqCbxKeyDownHandler(event:KeyboardEvent):void
			{
				OrderEditViewUtil.dailyFreqCbxKeyDownHandler(event,this,commonUpdate);
			}
			
			protected function dailyFreqCbxDoubleClickHandler(event:MouseEvent):void
			{
				OrderEditViewUtil.dailyFreqCbxDoubleClickHandler(event,this,commonUpdate);
			}
			
			protected function dailyFreqCbxFocusOutHandler():void
			{
				OrderEditViewUtil.dailyFreqCbxFocusOutHandler(this);
				orderEditViewInfo.updateMoView();
			}												
			
			protected function dailyFreqCbxChangeHandler(event:IndexChangeEvent):void 
			{	
				try {
					OrderEditViewUtil.dailyFreqChangeFunc(event,commonUpdate,this);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function dailyFreqCbxOpenHandler(event:DropDownEvent):void
			{
				OrderEditViewUtil.dailyFreqCbxOpenHandler(this);
			}			
			
			protected function dailyFreqCbxCloseHandler(event:DropDownEvent):void
			{
				OrderEditViewUtil.dailyFreqCbxCloseHandler(this);
			}
			
			/* Supplmentary Frequency function handler */					
			protected function supplFreqLabelFunction(item:Object):String
			{			
				try {
					return OrderEditViewUtil.supplFreqLabelFunc(item,data as Dose,supplFreqCbx,this);
				} catch(e:Error) {					
					trace(e.getStackTrace());
				}
				return "";
			}
			
			protected function supplFreqEqualFunction(supplFreq1:Object, supplFreq2:Object):Boolean
			{
				try {
					return DmSupplFrequency(supplFreq1).supplFreqCode == DmSupplFrequency(supplFreq2).supplFreqCode;
				} catch(e:Error) {
					trace(e.getStackTrace());
				} 
				return false;
			}
			
			protected function supplFreqCbxKeyDownHandler(event:KeyboardEvent):void
			{
				OrderEditViewUtil.supplFreqCbxKeyDownHandler(event,this,commonUpdate);
			}
			
			protected function supplFreqCbxDoubleClickHandler(event:MouseEvent):void
			{
				OrderEditViewUtil.supplFreqCbxDoubleClickHandler(this);
			}
			
			protected function supplFreqCbxFocusOutHandler():void
			{
				OrderEditViewUtil.supplFreqCbxFocusOutHandler(this);
				orderEditViewInfo.updateMoView();
			}												
			
			protected function supplFreqCbxChangeHandler(event:IndexChangeEvent):void
			{
				try {
					OrderEditViewUtil.supplFreqChangeFunc(event,this,commonUpdate);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function supplFreqCbxOpenHandler(event:DropDownEvent):void
			{
				OrderEditViewUtil.supplFreqCbxOpenHandler(this);
			}			
			
			protected function supplFreqCbxCloseHandler(event:DropDownEvent):void
			{			
				OrderEditViewUtil.supplFreqCbxCloseHandler(this,commonUpdate);
			}
			
			/* PRN function handler */
			public function clearDispQty():void
			{
				try {
					if ( orderEditViewInfo.medOrderItem.dangerDrugFlag ) {
						data.dispQtyText = "";
						data.baseUnit = "DOSE";
					} else {
						orderEditViewInfo.medOrderItem.regimen.firstDose.dispQtyText = "";
						data.baseUnit = orderEditViewInfo.medOrderItem.baseUnit;
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			public function refreshToDefaultDoseGroupPrn():void{
				//PMSU-4806 - include stepup down
				var dangerousDrugFlag:Boolean = orderEditViewInfo.medOrderItem.dangerDrugFlag;
				if( dangerousDrugFlag || orderEditViewInfo.medOrderItem.regimen.firstDose.dispQtyText == "" ){
					for each ( var refreshDoseGroup:DoseGroup in orderEditViewInfo.medOrderItem.regimen.doseGroupList ){
						for each ( var dose:Dose in refreshDoseGroup.doseList ) {
							if ( dose.prn && (dose.prnPercent == PrnPercentage.Zero || dose.prnPercent == null) ) {
								OrderEditViewUtil.updatePrnPercentage(orderEditViewInfo,dose,doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary));
							}
						}
					}
				}
			}

			protected function prnChkboxChangeHandler(event:Event):void
			{		
				try {					
					
					if ( !prnChkbox.selected ) {
						data.prnPercent = PrnPercentage.Hundred;
					} else {
						data.prnPercent = null;
						OrderEditViewUtil.updatePrnPercentage(
							orderEditViewInfo, 
							data as Dose, 
							doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary)
						);
					}
					
					if ( orderEditViewInfo.medOrderItem.dangerDrugFlag ) {
						calDispQtyForDangerDrug();
					}
					
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}						
			
			protected function prnCbxLabelFunction(item:Object):String
			{			
				return OrderEditViewUtil.prnPercentLabelFunc(item as PrnPercentage, data as Dose, prnCbx);
			}
			
			protected function prnCbxChangeHandler(event:IndexChangeEvent):void
			{						
				try {
					OrderEditViewUtil.prnCbxChangeHandler(this, event);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function prnCbxEqualFunction(prnPercent1:Object, prnPercent2:Object):Boolean
			{				
				try {
					return PrnPercentage(prnPercent1).dataValue == PrnPercentage(prnPercent2).dataValue;
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
				return false;
			}
			
			/* Site function handler */
			protected function siteLabelFunction(item:Object):String 
			{
				try {
					if (orderEditViewInfo.medOrder.isMpDischarge()) {
						return OrderEditViewUtil.mpSiteLabelFunction(data as Dose,item,siteCbx,this);
					}
					else {
						return OrderEditViewUtil.siteLabelFunc(data as Dose,item,siteCbx,this);
					}
				} catch (e:Error) {
					trace(e.getStackTrace());								
				}
				return "";					
			}
			
			protected function siteEqualFunction(item1:Object, item2:Object):Boolean
			{		
				try {
					return Site(item1).siteCode == Site(item2).siteCode && Site(item1).supplSiteDesc == Site(item2).supplSiteDesc;
				} catch (e:Error) {
					trace(e.getStackTrace());					
				} 
				return false;				
			}
			
			protected function siteCbxKeyDownHandler(event:KeyboardEvent):void
			{
				OrderEditViewUtil.siteCbxKeyDownHandler(event,this,!orderEditViewInfo.medOrder.isMpDischarge(),commonUpdate);
			}
			
			protected function siteCbxChangeHandler(event:IndexChangeEvent):void
			{
				try {					
					OrderEditViewUtil.siteChangeFunc(event,this,commonUpdate,orderEditViewInfo);
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
			}
			
			protected function siteCbxOpenHandler(event:DropDownEvent):void
			{
				OrderEditViewUtil.siteCbxOpenHandler(this);
			}			
			
			protected function siteCbxCloseHandler(event:DropDownEvent):void
			{			
				OrderEditViewUtil.siteCbxCloseHandler(this);
			}
			
			protected function siteCbxFocusOutHandler():void
			{
				siteCbx.textInput.textDisplay.horizontalScrollPosition = 0;
				try {
					OrderEditViewUtil.siteCbxFocusOutHandler(this);
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			private function removeCurrentDose():void
			{		
				try {
					//PMS-2520
					orderEditViewInfo.orderEditView.manualErrorString = orderEditViewInfo.medOrderItem.getMoFirstErrorString();
					orderEditViewInfo.removeDose( doseList, doseGroupIndex, ListCollectionView(doseList).getItemIndex(data) );				
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}						
			
			public function addNewDose(tryCount:int=10):void
			{
				try {
					if ( !orderEditViewInfo.checkAddDoseOrStep(
						doseIndex, 
						doseGroupIndex, 
						orderEditViewInfo.medOrderItem,
						true ) ) {
						
						tryCount = 0;
						orderEditViewInfo.addDose(orderEditViewInfo.medOrderItem.regimen, doseGroupIndex, doseIndex);
						orderEditViewInfo.updateMoView();
					} else {
						orderEditViewInfo.checkMoErrorFocus();
					}
				} catch (e:Error) {					
					trace(e.getStackTrace());
					if ( tryCount > 0 ) {
						callLater(addNewDose,[tryCount-1]);
					}
				}
			}						
			
			public function checkFocusToError():void
			{
				try {
					if ( orderEditViewInfo.hasFocusFlag ) {
						return;
					} else {
						var uiCompList:ArrayCollection = new ArrayCollection([dosageTextInput, dosageUnitTextInput, dailyFreqCbx, supplFreqCbx, prnCbx, siteCbx]);
						var uiObjMap:Dictionary =  new Dictionary;
						var dose:Dose = data as Dose;
						uiObjMap[dosageTextInput] = dose.dosageErrorString;
						uiObjMap[dosageUnitTextInput] = dose.dosageUnitErrorString;
						uiObjMap[dailyFreqCbx] = dose.dailyFreqErrorString;
						uiObjMap[supplFreqCbx] = dose.supplFreqErrorString;
						uiObjMap[prnCbx] = dose.prnPercentErrorString;
						uiObjMap[siteCbx] = dose.siteErrorString;
						
						for each (var uiObj:UIComponent in uiCompList) {
							if (!orderEditViewInfo.hasFocusFlag) {
								OrderEditViewUtil.checkErrorFocusToUiComponent(orderEditViewInfo, uiObj, uiObjMap[uiObj] as String);
							}
							delete uiObjMap[uiObj];
						}
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			public function checkPrnPercentVisible():void
			{
				orderEditViewInfo.updateMoPrnPercentVisible(orderEditViewInfo.medOrderItem.regimen);
			}
			
			private function shortcutKey(evt:KeyboardEvent):void
			{	
				if (evt.charCode == 52 && evt.ctrlKey && data.removeBtnEnabled) {
					removeCurrentDose();
				}
			}	

		]]>
	</fx:Script>		
	
	<fx:Binding source="{doseList.getItemIndex(data)}" destination="doseIndex"/>
	<fx:Binding source="{doseGroupList.getItemIndex(doseGroup)}" destination="doseGroupIndex"/>	
	<fx:Binding source="{doseIndex == 0 &amp;&amp; doseGroupIndex == 0}" destination="isFirstDoseFlag"/>	
	<fx:Binding source="{data.outstandingDosageFlag || data.checkOutstandingDosage(orderEditViewInfo.medOrderItem.dosageCompulsory)?Dose.OUTSTANDING_COMPONENT_STYLE:Dose.NON_OUTSTANDING_COMPONENT_STYLE}" destination="dosageTextStyle"/>
	
	<mp:layout>
		<s:HorizontalLayout verticalAlign="top" paddingLeft="0" paddingRight="0" paddingTop="{MedOrderEditUiInfo.minPaddingSpace}" paddingBottom="0" gap="0"/>
	</mp:layout>
	
	<!-- Dosage -->
	<s:HGroup id="dosageGroup" 
			  width="{MedOrderEditUiInfo.dosageColWidth}"
			  paddingLeft="{MedOrderEditUiInfo.paddingSpace}" paddingRight="{MedOrderEditUiInfo.paddingSpace}" 
			  paddingTop="0" paddingBottom="0" 
			  verticalAlign="middle"			   
			  gap="{MedOrderEditUiInfo.paddingSpace}">						
		<fc:NumericInput id="dosageTextInput"
						 tabIndex="{dosageTabIndex}"
						 allowNegative="false" 			
						 numberFormat="7.4" 
						 maxChars="8"
						 text="{data.dosageText}" 			
						 width="{MedOrderEditUiInfo.dosageTxtInputWidth}"
						 height="{MedOrderEditUiInfo.uiComponentHeight}"
						 focusIn="orderEditViewInfo.moDosageFocusFlag=true"
						 focusOut="orderEditViewInfo.moDosageFocusFlag=false;dosageTextInputFocusChangeHandler(event)"	
						 keyDown="dosageTextInputKeyDownHandler(event)"
						 errorString="{data.dosageErrorString}"
						 styleName="{dosageTextStyle}"
						 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag &amp;&amp; (orderEditViewInfo.medOrderItem.isFreeTextEntryFlag || data.dosageUnit)}"/>
		<fc:UppercaseTextInput id="dosageUnitTextInput"
							   tabIndex="{dosageUnitTabIndex}"
							   text="{data.dosageUnit}"
							   maxChars="15"
							   textAlign="left"							   
							   visible="{orderEditViewInfo.medOrderItem.isFreeTextEntryFlag &amp;&amp; isFirstDoseFlag}"
							   includeInLayout="{orderEditViewInfo.medOrderItem.isFreeTextEntryFlag &amp;&amp; isFirstDoseFlag}"								 
							   width="100%"
							   height="{MedOrderEditUiInfo.uiComponentHeight}"
							   focusIn="orderEditViewInfo.moDosageUnitFocusFlag=true"
							   focusOut="orderEditViewInfo.moDosageUnitFocusFlag=false;dosageUnitFocusChangeHandler(event)"
							   keyFocusChange="dosageUnitFocusChangeHandler(event)"
							   mouseFocusChange="dosageUnitFocusChangeHandler(event)"
							   keyDown="dosageUnitKeyDownHandler(event)"
							   errorString="{data.dosageUnitErrorString}"					 
							   styleName="orderEditableComponentStyle"
							   enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>
		<s:Label text="{data.dosageUnit}"
				 width="{MedOrderEditUiInfo.dosageUnitLblWidth}"				 				 
				 visible="{!orderEditViewInfo.medOrderItem.isFreeTextEntryFlag || !isFirstDoseFlag}"
				 includeInLayout="{!orderEditViewInfo.medOrderItem.isFreeTextEntryFlag || !isFirstDoseFlag}"/>
	</s:HGroup>
	
	<!-- Daily frequency -->
	<s:HGroup width="{MedOrderEditUiInfo.dailyFreqColWidth}" 						  
			  paddingLeft="{MedOrderEditUiInfo.paddingSpace}" paddingRight="{MedOrderEditUiInfo.paddingSpace}" 
			  paddingTop="0" paddingBottom="0" 
			  verticalAlign="middle" gap="0">					
		<fc:ExtendedComboBox id="dailyFreqCbx" 
							 tabIndex="{dailyFreqTabIndex}"
							 requireSelection="false"
							 useVirtualLayout="false"							 
							 width="100%"
							 maxDropDownHeight="{MedOrderEditUiInfo.maxFreqDropDownHeight}"
							 labelFunction="dailyFreqLabelFunction"
							 itemMatchingFunction="OrderEditViewUtil.dailyFreqItemMatchingFunc"
							 dataProvider="{parentApplication.dmDailyFrequencyList}"										 
							 selectedItem="{data.dmDailyFrequency}"							 
							 equalFunction="dailyFreqEqualFunction"
							 change="dailyFreqCbxChangeHandler(event)"
							 open="dailyFreqCbxOpenHandler(event)"
							 close="dailyFreqCbxCloseHandler(event)"
							 keyDown="dailyFreqCbxKeyDownHandler(event)"
							 doubleClick="dailyFreqCbxDoubleClickHandler(event)"	
							 doubleClickEnabled="true"
							 focusIn="orderEditViewInfo.moDailyFreqFocusFlag=true;dailyFreqCbx.setFocus()"
							 focusOut="orderEditViewInfo.moDailyFreqFocusFlag=false;dailyFreqCbxFocusOutHandler();"
							 errorString="{data.dailyFreqErrorString}"							 
							 toolTip="{data.dailyFreq.desc}"						 
							 typographicCase="uppercase"
							 styleName="orderEditableComponentStyle"
							 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>
	</s:HGroup>
	
	<!-- Supplementary frequency -->
	<s:HGroup width="{MedOrderEditUiInfo.supplFreqColWidth}" 						  
			  paddingLeft="{MedOrderEditUiInfo.paddingSpace}" paddingRight="{MedOrderEditUiInfo.paddingSpace}" 
			  paddingTop="0" paddingBottom="0" 
			  verticalAlign="middle" gap="0">
		<fc:ExtendedComboBox id="supplFreqCbx"
							 tabIndex="{supplFreqTabIndex}"
							 requireSelection="false"
							 useVirtualLayout="false"							 
							 visible="{!orderEditViewInfo.showAdvancedOptionFlag}"
							 width="100%"
							 maxDropDownHeight="{MedOrderEditUiInfo.maxFreqDropDownHeight}"
							 labelFunction="supplFreqLabelFunction"
							 itemMatchingFunction="OrderEditViewUtil.supplFreqItemMatchingFunc"
							 selectedItem="{data.dmSupplFrequency}"
							 dataProvider="{orderEditViewInfo.dmSupplFrequencyList}"							 
							 equalFunction="supplFreqEqualFunction"
							 open="supplFreqCbxOpenHandler(event)"
							 close="supplFreqCbxCloseHandler(event)"							 
							 change="supplFreqCbxChangeHandler(event)"
							 keyDown="supplFreqCbxKeyDownHandler(event)"
							 doubleClick="supplFreqCbxDoubleClickHandler(event)"
							 doubleClickEnabled="true"
							 focusIn="orderEditViewInfo.moSupplFreqFocusFlag=true;"
							 focusOut="orderEditViewInfo.moSupplFreqFocusFlag=false;supplFreqCbxFocusOutHandler();"
							 errorString="{data.supplFreqErrorString}"
							 toolTip="{data.supplFreq.desc}"
							 typographicCase="uppercase"
							 styleName="orderEditableComponentStyle"
							 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>	
	</s:HGroup>
	
	<!-- PRN -->
	<s:HGroup width="{MedOrderEditUiInfo.prnColWidth+MedOrderEditUiInfo.dispColWidth}"
			  paddingLeft="0" paddingRight="0" 
			  paddingTop="0" paddingBottom="0" 
			  gap="{MedOrderEditUiInfo.paddingSpace}" 
			  horizontalAlign="center" 
			  verticalAlign="middle">			
		<s:CheckBox id="prnChkbox"
					tabIndex="{prnTabIndex}"
					selected="@{data.prn}"								
					change="prnChkboxChangeHandler(event);checkPrnPercentVisible()"
					width="{MedOrderEditUiInfo.prnChkboxWidth}"
					styleName="orderEditableComponentStyle"
					focusIn="orderEditViewInfo.moPrnFocusFlag=true;"
					focusOut="orderEditViewInfo.moPrnFocusFlag=false;"
					enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>		
		<fc:ExtendedDropDownList id="prnCbx"
								 tabIndex="{prnPercentTabIndex}"
								 width="{MedOrderEditUiInfo.prnCbxWidth}"
								 height="{MedOrderEditUiInfo.uiComponentHeight}"
								 selectedItem="{data.prnPercent}"
								 equalFunction="prnCbxEqualFunction"
								 dataProvider="{new ArrayCollection([PrnPercentage.TwentyFive,PrnPercentage.Fifty,PrnPercentage.SeventyFive,PrnPercentage.Hundred])}"
								 labelFunction="prnCbxLabelFunction"									
								 visible="{data.prn}"
								 errorString="{data.prnPercentErrorString}"
								 change="prnCbxChangeHandler(event)"
								 focusIn="orderEditViewInfo.moPrnPercentFocusFlag=true; prnCbxFocusFlag=true"
								 focusOut="orderEditViewInfo.moPrnPercentFocusFlag=false;  prnCbxFocusFlag=false"
								 styleName="{ prnCbxFocusFlag?'orderEditDropDownFocusStyle':'orderEditDropDownStyle'}"
								 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>
	</s:HGroup>
	
	<!-- Route/Site -->
	<s:HGroup width="{MedOrderEditUiInfo.routeSiteColWidth}" 						  
			  paddingLeft="{MedOrderEditUiInfo.paddingSpace}" paddingRight="{MedOrderEditUiInfo.paddingSpace}" 
			  paddingTop="0" paddingBottom="0" 
			  verticalAlign="middle"						  
			  gap="{MedOrderEditUiInfo.paddingSpace}">
		<fc:ExtendedComboBox id="siteCbx"
							 tabIndex="{siteTabIndex}"
							 dataProvider="{data.siteList}"										 
							 labelFunction="siteLabelFunction"
							 selectedItem="{orderEditViewInfo.medOrder.isMpDischarge()?data.ipSite:data.site}"
							 maxDropDownHeight="{MedOrderEditUiInfo.maxSiteDropDownHeight}"
							 width="{MedOrderEditUiInfo.siteCbxWidth}"
							 open="siteCbxOpenHandler(event)"
							 close="siteCbxCloseHandler(event)"
							 equalFunction="siteEqualFunction"
							 change="siteCbxChangeHandler(event)"
							 errorString="{data.siteErrorString}"
							 keyDown="siteCbxKeyDownHandler(event)"
							 focusIn="orderEditViewInfo.moRouteSiteFocusFlag=true;"
							 focusOut="orderEditViewInfo.moRouteSiteFocusFlag=false;siteCbxFocusOutHandler();"
							 toolTip="{siteToolTip}"
							 typographicCase="{orderEditViewInfo.siteLabelStyle}"
							 styleName="orderEditableComponentStyle"
							 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>					
	</s:HGroup>
	
	<!-- Add/Remove button -->
	<s:HGroup width="{MedOrderEditUiInfo.doseAddRemoveColWidth}"
			  paddingLeft="0" paddingRight="0" 
			  paddingTop="0" 
			  paddingBottom="0"
			  horizontalAlign="center"
			  verticalAlign="middle"
			  gap="{MedOrderEditUiInfo.paddingSpace}">
		<mx:Button id="removeBtn" 
				   icon="{MedOrderEditUiInfo.removeIcon}"				   
				   visible="{data.removeBtnEnabled}"
				   textAlign="center"
				   width="{MedOrderEditUiInfo.addRemoveBtnWidth}"
				   height="{MedOrderEditUiInfo.uiComponentHeight}"				   
				   click="removeCurrentDose()"
				   enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"
				   tabFocusEnabled="false"
				   toolTip="{removeBtn.enabled?'Remove multiple dosage line':''}"/>
		<mx:Button id="addBtn" 
				   icon="{MedOrderEditUiInfo.addIcon}"
				   visible="{data.addBtnEnabled}"
				   textAlign="center"
				   width="{MedOrderEditUiInfo.addRemoveBtnWidth}"
				   height="{MedOrderEditUiInfo.uiComponentHeight}"				   
				   click="addNewDose()"
				   enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"
				   tabFocusEnabled="false"
				   toolTip="{addBtn.enabled?'Add multiple dosage line':''}"/>
	</s:HGroup>			
</mp:ItemRenderer>
