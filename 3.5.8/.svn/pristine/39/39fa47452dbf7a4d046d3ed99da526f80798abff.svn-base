package hk.org.ha.model.pms.vo.pivas;

import hk.org.ha.model.pms.dms.persistence.DmWarning;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
import hk.org.ha.model.pms.persistence.pivas.PivasBatchPrep;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AddPivasBatchPrepDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private PivasBatchPrep pivasBatchPrep;
	
	private List<PivasContainer> pivasContainerList;
	
	private List<PivasDiluentInfo> pivasDiluentList;
	
	private List<PivasDiluentInfo> pivasSolventList;
	
	private List<DmWarning> dmWarningList;
	
	private Map<Long, List<PivasDiluentInfo>> pivasSolventListMapByPivasFormulaMethodDrugId;

	public PivasBatchPrep getPivasBatchPrep() {
		return pivasBatchPrep;
	}

	public void setPivasBatchPrep(PivasBatchPrep pivasBatchPrep) {
		this.pivasBatchPrep = pivasBatchPrep;
	}

	public List<PivasContainer> getPivasContainerList() {
		return pivasContainerList;
	}

	public void setPivasContainerList(List<PivasContainer> pivasContainerList) {
		this.pivasContainerList = pivasContainerList;
	}

	public List<PivasDiluentInfo> getPivasDiluentList() {
		return pivasDiluentList;
	}

	public void setPivasDiluentList(List<PivasDiluentInfo> pivasDiluentList) {
		this.pivasDiluentList = pivasDiluentList;
	}

	public List<PivasDiluentInfo> getPivasSolventList() {
		return pivasSolventList;
	}

	public void setPivasSolventList(List<PivasDiluentInfo> pivasSolventList) {
		this.pivasSolventList = pivasSolventList;
	}

	public List<DmWarning> getDmWarningList() {
		return dmWarningList;
	}

	public void setDmWarningList(List<DmWarning> dmWarningList) {
		this.dmWarningList = dmWarningList;
	}

	public Map<Long, List<PivasDiluentInfo>> getPivasSolventListMapByPivasFormulaMethodDrugId() {
		if (pivasSolventListMapByPivasFormulaMethodDrugId == null) {
			pivasSolventListMapByPivasFormulaMethodDrugId = new HashMap<Long, List<PivasDiluentInfo>>();
		}
		return pivasSolventListMapByPivasFormulaMethodDrugId;
	}

	public void setPivasSolventListMapByPivasFormulaMethodDrugId(
			Map<Long, List<PivasDiluentInfo>> pivasSolventListMapByPivasFormulaMethodDrugId) {
		this.pivasSolventListMapByPivasFormulaMethodDrugId = pivasSolventListMapByPivasFormulaMethodDrugId;
	}
}