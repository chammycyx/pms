<?xml version="1.0" encoding="utf-8"?>
<s:DataGroup xmlns:fx="http://ns.adobe.com/mxml/2009"
			 xmlns:s="library://ns.adobe.com/flex/spark" 
			 xmlns:mx="library://ns.adobe.com/flex/mx"
			 xmlns:vetting="hk.org.ha.view.pms.main.vetting.*"
			 xmlns:po="hk.org.ha.view.pms.main.vetting.po.*"
			 focusEnabled="false">

	<fx:Metadata>
		[Event(name="warnCodeError")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.IList;
			import mx.collections.ListCollectionView;
			import mx.events.FlexEvent;
			
			import spark.events.RendererExistenceEvent;
			
			import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
			import hk.org.ha.model.pms.vo.charging.PatientSfi;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import org.granite.tide.seam.Context;
			
			[Bindable]
			public var drugNameColWidth:int;
			
			[Bindable]
			public var doseColWidth:int;
			
			[Bindable]
			public var freqColWidth:int;
			
			[Bindable]
			public var durationColWidth:int;
			
			[Bindable]
			public var qtyColWidth:int;
			
			[Bindable]
			public var indicatorWidth:int;
			
			[Bindable]
			public var statusColWidth:int;
			
			[Bindable]
			public var dosageLineHeight:int;
			
			[Bindable]
			public var allowEditFlag:Boolean;
			
			[Bindable]
			public var sfiRefillFlag:Boolean;
			
			[Bindable]
			public var showWarnCodeFlag:Boolean;
			
			[Bindable]
			public var showExemptCodeFlag:Boolean;
			
			[Bindable]
			public var showDoseSfiQtyFlag:Boolean;
			
			[Bindable]
			public var ctx:Context;
			
			[Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[Bindable]
			public var exemptChargeReasonList:ArrayCollection;
			
			[Bindable]
			public var poiList:ListCollectionView;
			
			[Bindable]
			public var currentShown:Boolean = false;
			
			[Bindable]
			public var selectedFlag:Boolean = false;
			
			[Bindable]
			public var manualErrorString:String;
			
			[Bindable]
			public var screenEnableFlag:Boolean;
			
			[Bindable]
			public var allIssueQtyZeroFlag:Boolean = false;
			
			[Bindable]
			public var isMpDischarge:Boolean;
			
			[Bindable]
			public var propMap:PropMap;
			
			[Bindable]
			public var patientSfi:PatientSfi;
			
			override public function set dataProvider(value:IList):void {
				super.dataProvider = value;
				if (value == null) {
					poiList = null;
				}
				else {
					poiList = value as ListCollectionView;
				}
			}
			
			public function getPharmLine(index:int):OrderLineRenderer {
				return this.getElementAt(index) as OrderLineRenderer;
			}
			
			public function validate(successHandler:Function, startIndex:int = 0, checkModifyOnly:Boolean=false):void {
				for (var index:int = startIndex; index < this.dataProvider.length; index++) {
					getPharmLine(index).validate(
						function():void { validate(successHandler, index + 1, checkModifyOnly); },
						checkModifyOnly
					);
					return;
				}
				successHandler();
			}
			
			public function syncValidate():Boolean {
				for (var index:int = 0; index < this.dataProvider.length; index++) {
					if (!getPharmLine(index).syncValidate()) {
						return false;
					}
				}
				return true;
			}
			
			public function updateIssueQty():void {
				for (var index:int = 0; index < this.dataProvider.length; index++) {
					getPharmLine(index).updateIssueQty();
				}
			}
			
			public function setDefaultFocus():Boolean {
				if (dataProvider.length == 0 || getPharmLine(0) == null) {
					return false;
				}
				else {
					return getPharmLine(0).setDefaultFocus();
				}
			}
			
			public function setNextFocus():void {
				var currIndex:int = getFocusIndex();
				if (currIndex < 0) {
					currIndex = 0;
				}

				var newIndex:int = currIndex + 1;
				if (newIndex >= dataProvider.length) {
					return;
				}
				
				getPharmLine(newIndex).setDefaultFocus();
			}
			
			public function setPrevFocus():void {
				var currIndex:int = getFocusIndex();
				if (currIndex < 0) {
					currIndex = 0;
				}
				
				var newIndex:int = currIndex - 1;;
				if (newIndex < 0) {
					return;
				}

				getPharmLine(newIndex).setDefaultFocus();
			}
			
			private function getFocusIndex():int {
				var o:Object = focusManager.getFocus();
				if (o == null || !(o is DisplayObject)) {
					return -1;
				}
				
				var pharmLine:OrderLineRenderer = getFocusPharmLine(o as DisplayObject);
				if (pharmLine == null) {
					return -1;
				}

				return this.getElementIndex(pharmLine);
			}
			
			private function getFocusPharmLine(dp:DisplayObject):OrderLineRenderer {
				if (dp is OrderLineRenderer) {
					return (dp as OrderLineRenderer);
				}
				else if (dp.parent == null || !(dp.parent is DisplayObject)) {
					return null;
				}
				else {
					return getFocusPharmLine(dp.parent);
				}
			}
			
			public function setWarnCodeFocus():void {
				if (poiList.length == 0) {
					return;
				}
				getPharmLine(0).setWarnCodeFocus();
			}
			
			public function errorStringChange(evt:Event):void {
				manualErrorString = getErrorString();
			}
			
			private function getErrorString():String {
				for (var index:int = 0; index < this.dataProvider.length; index++) {
					var errorString:String = getPharmLine(index).manualErrorString;
					if (errorString != null && errorString != "") {
						return errorString;
					}
				}
				return null;
			}
			
			public function rendererRemoveHandler(evt:RendererExistenceEvent):void {
				for (var i:int=0; i<dataProvider.length; i++) {
					if (getPharmLine(i) != null) {
						getPharmLine(i).rendererRemoveHandler(evt);
					}
				}
			}
			
			public function checkAllIssueQtyZero():void {
				allIssueQtyZeroFlag = true;
				if (poiList == null) {
					return;
				}
				for each (var poi:PharmOrderItem in poiList) {
					if (poi.issueQty > 0) {
						allIssueQtyZeroFlag = false;
						return;
					}
				}
			}
		]]>
	</fx:Script>

	<s:layout>
		<s:VerticalLayout gap="0"/>
	</s:layout>
	
	<s:itemRenderer>
		<fx:Component>
			<po:OrderLineRenderer showWarnCodeFlag="{outerDocument.showWarnCodeFlag}"
								  showExemptCodeFlag="{outerDocument.showExemptCodeFlag}"
								  showDoseSfiQtyFlag="{outerDocument.showDoseSfiQtyFlag}"
								  allowEditFlag="{outerDocument.allowEditFlag}"
								  sfiRefillFlag="{outerDocument.sfiRefillFlag}"
								  drugNameColWidth="{outerDocument.drugNameColWidth}"
								  doseColWidth="{outerDocument.doseColWidth}"
								  freqColWidth="{outerDocument.freqColWidth}"
								  durationColWidth="{outerDocument.durationColWidth}"
								  qtyColWidth="{outerDocument.qtyColWidth}"
								  indicatorWidth="{outerDocument.indicatorWidth}"
								  statusColWidth="{outerDocument.statusColWidth}"
								  dosageLineHeight="{outerDocument.dosageLineHeight}"
								  ctx="{outerDocument.ctx}"
								  sysMsgMap="{outerDocument.sysMsgMap}"
								  exemptChargeReasonList="{outerDocument.exemptChargeReasonList}"
								  selectedFlag="{outerDocument.selectedFlag}"
								  rowCount="{outerDocument.poiList.length}"
								  currentShown="{outerDocument.currentShown}"
								  vettingErrorChange="outerDocument.errorStringChange(event)"
								  warnCodeError="outerDocument.dispatchEvent(new Event('warnCodeError'))"
								  screenEnableFlag="{outerDocument.screenEnableFlag}"
								  dataChange="dataChangeHandler()"
								  allIssueQtyZeroFlag="{outerDocument.allIssueQtyZeroFlag}"
								  isMpDischarge="{outerDocument.isMpDischarge}"
								  propMap="{outerDocument.propMap}"
								  patientSfi="{outerDocument.patientSfi}">
				<fx:Script>
					<![CDATA[
						private function dataChangeHandler():void {
							outerDocument.checkAllIssueQtyZero();
						}
					]]>
				</fx:Script>
			</po:OrderLineRenderer>
								  
		</fx:Component>
	</s:itemRenderer>
</s:DataGroup>
