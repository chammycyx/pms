<?xml version="1.0" encoding="utf-8"?>
<mp:ItemRenderer xmlns:fx="http://ns.adobe.com/mxml/2009" 
				 xmlns:s="library://ns.adobe.com/flex/spark"
				 xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
				 xmlns:mx="library://ns.adobe.com/flex/mx"
				 xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
				 xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
				 xmlns="hk.org.ha.view.pms.main.vetting.mo.*"
				 autoDrawBackground="true">
	
	<fx:Metadata>
		[Style(name="borderVisible", type="Boolean")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[			
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.core.UIComponent;
			import mx.events.CalendarLayoutChangeEvent;
			import mx.utils.StringUtil;
			
			import spark.events.IndexChangeEvent;
			
			import hk.org.ha.event.pms.main.vetting.CalSubTotalDosageForDangerousDrugEvent;
			import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
			import hk.org.ha.model.pms.udt.vetting.RegimenDurationUnit;
			import hk.org.ha.model.pms.udt.vetting.RegimenType;
			import hk.org.ha.model.pms.vo.rx.Dose;
			import hk.org.ha.model.pms.vo.rx.DoseGroup;
			import hk.org.ha.model.pms.vo.rx.RxItem;
			import hk.org.ha.model.pms.vo.vetting.MedOrderEditUiInfo;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewInfo;
			import hk.org.ha.model.pms.vo.vetting.OrderEditViewUtil;
			
			[Bindable]
			private var doseGroupIndex:Number;									
			[Bindable]
			private var durationUnitCbxEnabled:Boolean;
			[Bindable]
			private var durationUnitCbxFocusFlag:Boolean =false;
			
			[Bindable]
			public var orderEditViewInfo:OrderEditViewInfo;				
			[Bindable]
			public var doseGroupList:ListCollectionView;
			[Bindable]
			public var issueSfiQtyEnabled:Boolean;						
			[Bindable]
			public var startDateTabIndex:int;		
			[Bindable]
			public var durationTabIndex:int;
			[Bindable]
			public var durationUnitTabIndex:int;
			[Bindable]
			public var endDateTabIndex:int;
			
			private function calDispQtyForDangerDrug():void
			{							
				try {
					if ( !orderEditViewInfo.medOrderItem || !orderEditViewInfo.medOrderItem.rxDrug || isNaN(doseGroupIndex) ) {
						return;
					}
					if ( !orderEditViewInfo.medOrderItem.dangerDrugFlag ) {
						return;
					}
					dispatchEvent( new CalSubTotalDosageForDangerousDrugEvent(orderEditViewInfo.medOrderItem.rxDrug as RxItem, doseGroupIndex, -1, updateMoIssueQty) );
				} catch (e:Error) {
					trace(e);
				}
				
			}
			
			private function updateMoIssueQty(value:ListCollectionView):void 
			{
				try {
					var dispQtyText:String;
					var i:int = 0;
					for each ( var dose:Dose in DoseGroup(data).doseList ) {
						dispQtyText = String(value.getItemAt(i) as Number);
						if ( dispQtyText.length > 4 ) {
							dose.dispQtyText = "";
						} else {
							dose.dispQtyText = dispQtyText;
						}
						dose.baseUnit	 = "DOSE";
						++i;
					}
					orderEditViewInfo.orderEditView.manualErrorString = null;
					
					if ( focusManager.getFocus() == this.durationUnitCbx) 
					{
						this.getDispQtyItemRendererAt(0).qtyTxtInput.selectAll();
					}
					else if(focusManager.getFocus() == this.getDispQtyItemRendererAt(0).qtyTxtInput)
					{
						stage.focus = null;
						this.getDispQtyItemRendererAt(0).qtyTxtInput.setFocus();
					}
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e);
				}
			}
			
			private function commonUpdate():void
			{
				calDispQtyForDangerDrug();
				orderEditViewInfo.updateMoView();
			}
			
			private function checkRefreshToDefaultPrnPercent(dose:Dose, dangerousDrugFlag:Boolean):Boolean
			{
				if ( dose.prn && (dose.prnPercent == PrnPercentage.Zero || dose.prnPercent == null) ) {
					if (dangerousDrugFlag) {
						return true;
					} else {
						return orderEditViewInfo.medOrderItem.regimen.firstDose.dispQtyText == "";
					}			
				}
				return false;
			}
			
			private function commonDurationChangeHandler():void
			{
				var doseGroup:DoseGroup = data as DoseGroup;
				var dangerousDrugFlag:Boolean = orderEditViewInfo.medOrderItem.dangerDrugFlag;
				for each ( var dose:Dose in doseGroup.doseList ) {
					if ( checkRefreshToDefaultPrnPercent(dose, dangerousDrugFlag) ) {
						OrderEditViewUtil.updatePrnPercentage(orderEditViewInfo,dose,doseGroup.getDurationInDay(orderEditViewInfo.dmRegimenDictionary));						
					}
				}
				calDispQtyForDangerDrug();
				orderEditViewInfo.updateMoView();
			}
			
			protected function durationTxtKeyDownHandler(event:KeyboardEvent):void
			{
				if ( event.keyCode == Keyboard.ENTER ) {
					durationTxtChangeHandler();
				}
			}
			
			private function durationTooLongHandler():void
			{
				var doseGroup:DoseGroup = data as DoseGroup;
				if ( orderEditViewInfo.checkExceedMaxDuration( doseGroup.duration, doseGroup.durationUnit ) ) {
					doseGroup.durationErrorString 					  = orderEditViewInfo.sysMsgMap.getMessage("0188");
					orderEditViewInfo.orderEditView.manualErrorString = data.durationErrorString;
					durationTxt.setFocus();
				}
			}
			
			private function durationTxtChangeHandler():void
			{		
				try {
					var doseGroup:DoseGroup = data as DoseGroup;
					if ( StringUtil.trim(durationTxt.text) != data.durationText ) {
						doseGroup.durationText = durationTxt.text;
						commonDurationChangeHandler();	
						if ( orderEditViewInfo.checkExceedMaxDuration( doseGroup.duration, doseGroup.durationUnit ) ) {
							orderEditViewInfo.customizedCallLater(durationTooLongHandler,100);
						} else {
							doseGroup.durationErrorString				 	  = null;
							doseGroup.durationUnitErrorString				  = null;
							orderEditViewInfo.orderEditView.manualErrorString = null;
						}
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			private function durationFocusChangeHandler(event:FocusEvent):void
			{
				durationTxtChangeHandler();	
				var doseGroup:DoseGroup = data as DoseGroup;
				if ( !orderEditViewInfo.isOnHide ) {
					if ( data != null && doseGroup.durationErrorString ) {
						doseGroup.durationErrorString = null;
						orderEditViewInfo.orderEditView.manualErrorString = null;
					}
				}
			}
			
			private function durationUnitTooLongHandler():void
			{
				var doseGroup:DoseGroup = data as DoseGroup;
				if ( orderEditViewInfo.checkExceedMaxDuration( doseGroup.duration, doseGroup.durationUnit ) ) {
					doseGroup.durationUnitErrorString 				  = orderEditViewInfo.sysMsgMap.getMessage("0188");
					orderEditViewInfo.orderEditView.manualErrorString = data.durationUnitErrorString;
					durationUnitCbx.setFocus();
				}
			}
			
			private function durationUnitCbxChangeHandler(event:IndexChangeEvent):void {
				try {
					if( durationUnitCbx.visible == true ) {
						if ( event.newIndex >= 0 ) {
							var doseGroup:DoseGroup = data as DoseGroup;
							doseGroup.durationUnit 	= durationUnitCbx.dataProvider.getItemAt(event.newIndex) as RegimenDurationUnit;
							doseGroup.dmRegimen 	= orderEditViewInfo.dmRegimenDictionary[doseGroup.durationUnit.dataValue];
							
							if ( orderEditViewInfo.checkExceedMaxDuration( doseGroup.duration, doseGroup.durationUnit ) ) {
								orderEditViewInfo.customizedCallLater(durationUnitTooLongHandler,100);
							} else {
								doseGroup.durationErrorString					  = null;
								doseGroup.durationUnitErrorString 				  = null;	
								durationUnitCbx.errorString						  = null;
								orderEditViewInfo.orderEditView.manualErrorString = null;								
							}
							commonDurationChangeHandler();
						}
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			private function durationUnitCbxFocusChangeHandler(event:FocusEvent):void
			{
				var doseGroup:DoseGroup = data as DoseGroup;
				if ( !orderEditViewInfo.isOnHide ) {
					if ( data != null && doseGroup.durationUnitErrorString ) {
						doseGroup.durationUnitErrorString = null;
						orderEditViewInfo.orderEditView.manualErrorString = null;
					}
				}
			}
			
			protected function startDateCalendarChangeHandler(event:CalendarLayoutChangeEvent):void
			{		
				try {//PMSU-3007
					OrderEditViewUtil.startDateCalendarChangeHandler(this);
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			protected function endDateCalendarChangeHandler(event:CalendarLayoutChangeEvent):void
			{					
				try {//PMSU-3007
					OrderEditViewUtil.endDateCalendarChangeHandler(this);
					commonDurationChangeHandler();
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e.getStackTrace());
				}
			}
			
			private function removeCurrentDoseGroup():void
			{
				try {
					
					//PMS-2520
					orderEditViewInfo.orderEditView.manualErrorString = orderEditViewInfo.medOrderItem.getMoFirstErrorString();
					orderEditViewInfo.removeDoseGroup( doseGroupList, doseGroupList.getItemIndex(data) );
					orderEditViewInfo.updateMoView();
				} catch (e:Error) {
					trace(e);
				}
			}						
			
			private function addNewDoseGroup(tryCount:int=10):void
			{
				try {
					var doseGroup:DoseGroup = data as DoseGroup;
					if ( !orderEditViewInfo.checkAddDoseOrStep(
						doseGroup.doseList.length-1, 
						doseGroupIndex, 
						orderEditViewInfo.medOrderItem, 
						true) ) {
						tryCount=0;
						orderEditViewInfo.addDoseGroup(orderEditViewInfo.medOrderItem.regimen, doseGroupIndex);
						orderEditViewInfo.updateMoView();
					} else {
						orderEditViewInfo.checkMoErrorFocus();
					}
				} catch (e:Error) {
					trace(e.getStackTrace());
					if ( tryCount >= 0 ) {
						callLater(addNewDoseGroup,[tryCount-1]);
					}
				}
			}
			
			protected function changingHandler(event:IndexChangeEvent):void
			{
				if ( event.newIndex == -1 ) {
					event.preventDefault();
				}
			}						
			
			public function getDoseItemRendererAt(index:int):DoseEditItemRenderer
			{				
				if ( !doseDataGroup || !doseDataGroup.dataProvider ) {
					return null;
				}
				if ( index < doseDataGroup.dataProvider.length && index >= 0 ) {
					return doseDataGroup.getElementAt(index) as DoseEditItemRenderer;
				} else {
					return null;
				}
			}
			
			public function getDispQtyItemRendererAt(index:int):DispQtyEditItemRenderer
			{
				return dispQtyDataGroup.getElementAt(index) as DispQtyEditItemRenderer;				
			}
			
			public function checkFocusToError():void
			{
				try {
					if ( orderEditViewInfo.hasFocusFlag ) {
						return;
					} else {
						var uiCompList:ArrayCollection = new ArrayCollection([startDateCalendar, endDateCalendar, durationTxt, durationUnitCbx]);
						var uiObjMap:Dictionary =  new Dictionary;
						var doseGroup:DoseGroup = data as DoseGroup;
						uiObjMap[startDateCalendar] = doseGroup.startDateErrorString;
						uiObjMap[endDateCalendar] = doseGroup.endDateErrorString;
						uiObjMap[durationTxt] = doseGroup.durationErrorString;
						uiObjMap[durationUnitCbx] = doseGroup.durationUnitErrorString;
						
						for each (var uiObj:UIComponent in uiCompList) {
							if (!orderEditViewInfo.hasFocusFlag) {
								OrderEditViewUtil.checkErrorFocusToUiComponent(orderEditViewInfo, uiObj, uiObjMap[uiObj] as String);
							}
							delete uiObjMap[uiObj];
						}
					}
				} catch (e:Error) {
					trace(e);
				} 					
			}
			
			public function clearDurationErrorString():void
			{
				if(orderEditViewInfo.orderEditView.manualErrorString == data.endDateErrorString 
					|| orderEditViewInfo.orderEditView.manualErrorString == data.durationErrorString
					|| orderEditViewInfo.orderEditView.manualErrorString == data.durationUnitErrorString)
				{
					orderEditViewInfo.orderEditView.manualErrorString = "";
				}
				data.endDateErrorString = "";
				data.durationErrorString = "";
				data.durationUnitErrorString = "";
			}
			public function setDurationFocus():void
			{
				if ( startDateCalendar.visible) 
				{
					startDateCalendar.setFocus();
				}
				else if ( endDateCalendar.visible) 
				{
					endDateCalendar.setFocus();		
				}
				else if ( durationTxt.visible) 
				{	
					durationTxt.setFocus();
				}
			}
			
			private function labelFunction(item:Object):String
			{
				try {
					if ( !item ) {
						return "";
					} else {
						return String(item.displayValue);
					}
				} catch (e:Error) {
					trace(e);
				}
				return "";
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="{doseGroupList.getItemIndex(data)}" destination="doseGroupIndex"/>
	<fx:Binding source="{orderEditViewInfo.medOrderItem.regimen.type == RegimenType.StepUpDown || orderEditViewInfo.medOrderItem.regimen.type == RegimenType.Daily}" destination="durationUnitCbxEnabled"/>
	
	<mp:layout>
		<s:VerticalLayout paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0"/>
	</mp:layout>
	
	<s:HGroup id="hGroup" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">				
		
		<s:VGroup gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0">		
			<s:DataGroup id="doseDataGroup"
						 width="100%"					 
						 dataProvider="{data.doseList}">
				<s:layout>
					<s:VerticalLayout gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0"/>
				</s:layout>
				
				<s:itemRenderer>
					<fx:Component>
						<DoseEditItemRenderer doseGroupList="{outerDocument.doseGroupList}"
											  doseGroup="{outerDocument.data as DoseGroup}"
											  doseList="{outerDocument.data.doseList}"
											  orderEditViewInfo="{outerDocument.orderEditViewInfo}"	
											  tabFocusEnabled="true"
											  width="100%">
							<fx:Script>
								<![CDATA[
									import hk.org.ha.model.pms.vo.rx.DoseGroup;
								]]>
							</fx:Script>
						</DoseEditItemRenderer>
						
					</fx:Component>
				</s:itemRenderer>
			</s:DataGroup>	
		</s:VGroup>
		
		<!-- Duration -->
		<s:HGroup width="{MedOrderEditUiInfo.durationColWidth}"								   
				  paddingLeft="0" paddingRight="0" 
				  paddingTop="{MedOrderEditUiInfo.minPaddingSpace}" paddingBottom="0"							  
				  gap="{MedOrderEditUiInfo.paddingSpace}"
				  verticalAlign="top"
				  horizontalAlign="right">
			<fc:ExtendedAbbDateField id="startDateCalendar"
									 tabIndex="{startDateTabIndex}"
									 width="{MedOrderEditUiInfo.durationDateFieldWidth}"								   
									 showABBDateChooser="false"
									 selectedDate="@{data.startDate}"
									 visible="{data.startDateEnabled}" 
									 includeInLayout="{data.startDateEnabled}"
									 errorString="{data.startDateErrorString}"
									 change="startDateCalendarChangeHandler(event)"
									 focusIn="orderEditViewInfo.moDurationFocusFlag=true"
									 focusOut="orderEditViewInfo.moDurationFocusFlag=false"
									 styleName="orderEditableComponentStyle"
									 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>
			<s:Label width="{MedOrderEditUiInfo.durationToLblWidth}"
					 height="100%"
					 text="to"
					 verticalAlign="middle"
					 textAlign="center"
					 visible="{data.endDateEnabled}" 
					 includeInLayout="{data.endDateEnabled}"/>
			<fc:ExtendedAbbDateField id="endDateCalendar"
									 tabIndex="{endDateTabIndex}"
									 width="{MedOrderEditUiInfo.durationDateFieldWidth}"								   
									 showABBDateChooser="false"
									 selectedDate="@{data.endDate}"
									 visible="{data.endDateEnabled}" 
									 includeInLayout="{data.endDateEnabled}"
									 errorString="{data.endDateErrorString}"
									 change="endDateCalendarChangeHandler(event)"
									 focusIn="orderEditViewInfo.moDurationFocusFlag=true"
									 focusOut="orderEditViewInfo.moDurationFocusFlag=false"								   								   
									 styleName="orderEditableComponentStyle"
									 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>				
			<fc:NumericInput id="durationTxt" 
							 tabIndex="{durationTabIndex}"
							 text="{data.durationText}"	
							 numberFormat="3.0"
							 allowNegative="false"
							 textAlign="right" 						 
							 visible="{!data.endDateEnabled}" 
							 includeInLayout="{!data.endDateEnabled}" 
							 width="{MedOrderEditUiInfo.durationTxtInputWidth}"	
							 height="{MedOrderEditUiInfo.uiComponentHeight}"				  						 
							 errorString="{data.durationErrorString}"
							 focusIn="orderEditViewInfo.moDurationFocusFlag=true"
							 focusOut="orderEditViewInfo.moDurationFocusFlag=false;durationFocusChangeHandler(event)"
							 keyFocusChange="durationFocusChangeHandler(event)"
							 mouseFocusChange="durationFocusChangeHandler(event)"							 
							 keyDown="durationTxtKeyDownHandler(event)"
							 styleName="orderEditableComponentStyle"
							 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>
			<fc:ExtendedDropDownList id="durationUnitCbx"
									 tabIndex="{durationUnitTabIndex}"						 
									 visible="{!(data.endDateEnabled || !durationUnitCbxEnabled)}"
									 includeInLayout="{!(data.endDateEnabled || !durationUnitCbxEnabled)}"
									 width="{MedOrderEditUiInfo.durationDropDownListWidth}"
									 height="{MedOrderEditUiInfo.uiComponentHeight}"
									 change="durationUnitCbxChangeHandler(event)" 
									 labelFunction="labelFunction"
									 selectedItem="{data.durationUnit}"								 
									 focusIn="orderEditViewInfo.moDurationFocusFlag=true;durationUnitCbxFocusFlag=true"
									 focusOut="orderEditViewInfo.moDurationFocusFlag=false;durationUnitCbxFocusFlag=false"
									 keyFocusChange="durationUnitCbxFocusChangeHandler(event)"
									 mouseFocusChange="durationUnitCbxFocusChangeHandler(event)"
									 dataProvider="{new ArrayCollection([RegimenDurationUnit.Day, RegimenDurationUnit.Week])}"								 
									 styleName="{durationUnitCbxFocusFlag?'orderEditDropDownFocusStyle':'orderEditDropDownStyle'}"
									 errorString="{data.durationUnitErrorString}"
									 enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"/>
			
			<s:Label verticalAlign="middle" height="100%"
					 visible="{!(data.endDateEnabled || durationUnitCbxEnabled)}"
					 includeInLayout="{!(data.endDateEnabled || durationUnitCbxEnabled)}"
					 text="{data.durationUnit.displayValue}"/>						
		</s:HGroup>	
		
		<s:VGroup width="{MedOrderEditUiInfo.qtyColWidth}" height="100%" 
				  paddingTop="0" paddingBottom="0" 
				  paddingLeft="0" paddingRight="0"
				  gap="0">		
			<s:DataGroup id="dispQtyDataGroup"
						 width="100%"					 
						 dataProvider="{data.doseList}">
				<s:layout>
					<s:VerticalLayout gap="0" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0"/>
				</s:layout>
				
				<s:itemRenderer>
					<fx:Component>
						<DispQtyEditItemRenderer doseList="{outerDocument.data.doseList}"
												 doseGroupList="{outerDocument.doseGroupList}"
												 doseGroup="{DoseGroup(outerDocument.data)}"
												 orderEditViewInfo="{outerDocument.orderEditViewInfo}"
												 width="100%">
							<fx:Script>
								<![CDATA[
									import hk.org.ha.model.pms.vo.rx.DoseGroup;
								]]>
							</fx:Script>
						</DispQtyEditItemRenderer>
						
					</fx:Component>
				</s:itemRenderer>
			</s:DataGroup>	
		</s:VGroup>
		
		<!-- Add/Remove button -->
		<s:HGroup width="{MedOrderEditUiInfo.doseGroupAddRemoveColWidth - MedOrderEditUiInfo.scrollBarWidth}"		
				  paddingLeft="0" paddingRight="0" 
				  paddingTop="0" paddingBottom="0" 
				  gap="{MedOrderEditUiInfo.paddingSpace}">																
			<mx:Button icon="{MedOrderEditUiInfo.removeIcon}"					   
					   visible="{data.removeBtnEnabled}"
					   width="{MedOrderEditUiInfo.addRemoveBtnWidth}"
					   height="{MedOrderEditUiInfo.uiComponentHeight}"					   
					   click="removeCurrentDoseGroup()"
					   enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"
					   tabFocusEnabled="false"
					   toolTip="Remove step up / down dosage line"/>
			<mx:Button icon="{MedOrderEditUiInfo.addIcon}"
					   visible="{data.addBtnEnabled}"
					   width="{MedOrderEditUiInfo.addRemoveBtnWidth}"
					   height="{MedOrderEditUiInfo.uiComponentHeight}"					   
					   click="addNewDoseGroup()"
					   enabled="{!orderEditViewInfo.readOnlyFlag &amp;&amp; orderEditViewInfo.isMoiEditFlag}"
					   tabFocusEnabled="false"
					   toolTip="Add step up / down dosage line"/>
		</s:HGroup>
	</s:HGroup>	
	<s:Line xFrom="0" xTo="{hGroup.width}"
			yFrom="0" yTo="0" visible="{ListCollectionView(doseGroupList).getItemIndex(data) != (doseGroupList.length - 1)}"
			includeInLayout="{ListCollectionView(doseGroupList).getItemIndex(data) != (doseGroupList.length - 1)}"
			width="100%">
		<s:stroke>
			<s:SolidColorStroke color="0x0000FF" weight="2" joints="round"/>
		</s:stroke>
	</s:Line>	
</mp:ItemRenderer>
