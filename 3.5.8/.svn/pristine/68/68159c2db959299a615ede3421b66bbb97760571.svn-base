package hk.org.ha.model.pms.persistence.medprofile;

import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "REPLENISHMENT")
public class Replenishment extends VersionEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "replenishmentSeq")
	@SequenceGenerator(name = "replenishmentSeq", sequenceName = "SQ_REPLENISHMENT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "REPLENISH_NUM", nullable = false)
	private Integer replenishNum;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REQUEST_DATE", nullable = false)
	private Date requestDate;
	
	@Column(name = "REASON", length = 255)
	private String reason;

    @Converter(name = "Replenishment.status", converterClass = ReplenishmentStatus.Converter.class)
    @Convert("Replenishment.status")
	@Column(name = "STATUS", nullable = false, length = 1)
	private ReplenishmentStatus status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_ID")
	private MedProfile medProfile;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MED_PROFILE_MO_ITEM_ID", nullable = false)
	private MedProfileMoItem medProfileMoItem;

	@PrivateOwned
	@OneToMany(mappedBy = "replenishment", cascade = CascadeType.ALL)
	private List<ReplenishmentItem> replenishmentItemList;

	@Column(name = "DOSE_COUNT")
	private Integer doseCount;
	
	@Transient
	private Boolean dispFlag;
	
	@Transient
	private Long orgId;

	@Transient
	private Integer seqNum;
	
	@PostLoad
    public void postLoad() {
		orgId = this.getId();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Integer getReplenishNum() {
		return replenishNum;
	}

	public void setReplenishNum(Integer replenishNum) {
		this.replenishNum = replenishNum;
	}

	public Date getRequestDate() {
		return requestDate == null ? null : new Date(requestDate.getTime());
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate == null ? null : new Date(requestDate.getTime());
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public ReplenishmentStatus getStatus() {
		return status;
	}

	public void setStatus(ReplenishmentStatus status) {
		this.status = status;
	}
	
	public MedProfile getMedProfile() {
		return medProfile;
	}

	public void setMedProfile(MedProfile medProfile) {
		this.medProfile = medProfile;
	}

	public MedProfileMoItem getMedProfileMoItem() {
		return medProfileMoItem;
	}

	public void setMedProfileMoItem(MedProfileMoItem medProfileMoItem) {
		this.medProfileMoItem = medProfileMoItem;
	}

	public List<ReplenishmentItem> getReplenishmentItemList() {
		if ( replenishmentItemList == null ) {
			replenishmentItemList = new ArrayList<ReplenishmentItem>();
		}
		return replenishmentItemList;
	}

	public void addReplenishmentItem(ReplenishmentItem replenishmentItem) {
		replenishmentItem.setReplenishment(this);
		getReplenishmentItemList().add(replenishmentItem);
	}
	
	public void setReplenishmentItemList(
			List<ReplenishmentItem> replenishmentItemList) {
		this.replenishmentItemList = replenishmentItemList;
	}
	
	public Integer getDoseCount() {
		return doseCount;
	}

	public void setDoseCount(Integer doseCount) {
		this.doseCount = doseCount;
	}

	public void setDispFlag(Boolean dispFlag) {
		this.dispFlag = dispFlag;
	}

	public Boolean getDispFlag() {
		return dispFlag;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public Integer getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}	
}
