package hk.org.ha.model.pms.persistence.reftable;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.label.CustomLabelType;
import hk.org.ha.model.pms.vo.label.HkidBarcodeLabel;
import hk.org.ha.model.pms.vo.label.SpecialLabel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "CUSTOM_LABEL")
public class CustomLabel extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";	

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customLabelSeq")
	@SequenceGenerator(name = "customLabelSeq", sequenceName = "SQ_CUSTOM_LABEL", initialValue = 100000000)
	private Long id;
	
	@Column(name = "NAME", nullable = false, length = 12)
	private String name;

	@Lob
	@Column(name = "LABEL_XML")
	private String labelXml;

	@Converter(name = "CustomLabel.type", converterClass = CustomLabelType.Converter.class)
    @Convert("CustomLabel.type")
	@Column(name="TYPE", nullable = false, length = 1)
	private CustomLabelType type;
	
	@Column(name = "DESCRIPTION", length = 1000)
	private String description;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
	
	@Transient
	private SpecialLabel specialLabel;
	
	@Transient 
	private HkidBarcodeLabel hkidBarcodeLabel;
	
	public HkidBarcodeLabel getHkidBarcodeLabel() {
		if(hkidBarcodeLabel == null){			
			JaxbWrapper<HkidBarcodeLabel> spLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			hkidBarcodeLabel = spLabelJaxbWrapper.unmarshall(labelXml);
		}
		return hkidBarcodeLabel;
	}

	public void setHkidBarcodeLabel(HkidBarcodeLabel hkidBarcodeLabel) {
		JaxbWrapper<HkidBarcodeLabel> spLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		this.setLabelXml(spLabelJaxbWrapper.marshall(hkidBarcodeLabel));
		this.hkidBarcodeLabel = hkidBarcodeLabel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabelXml() {
		return labelXml;
	}

	public void setLabelXml(String labelXml) {
		this.labelXml = labelXml;
	}

	public CustomLabelType getType() {
		return type;
	}

	public void setType(CustomLabelType type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}
	
	public SpecialLabel getSpecialLabel() {
		if(specialLabel == null){
			JaxbWrapper<SpecialLabel> spLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			specialLabel = spLabelJaxbWrapper.unmarshall(labelXml);
		}
		return specialLabel;
	}

	public void setSpecialLabel(SpecialLabel specialLabel) {
		JaxbWrapper<SpecialLabel> spLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		this.setLabelXml(spLabelJaxbWrapper.marshall(specialLabel));
		this.specialLabel = specialLabel;
	}
}
