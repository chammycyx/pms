package hk.org.ha.model.pms.udt.alert;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Severity implements StringValuedEnum {
	
	Severe("S", "Severe"),
	Mild("M", "Mild"),
	SystemConversion("U", "System Conversion");
	
    private final String dataValue;
    private final String displayValue;
    
	Severity(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public static Severity dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Severity.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<Severity> {
		
		private static final long serialVersionUID = 1L;

		public Class<Severity> getEnumClass() {
    		return Severity.class;
    	}
    }
	
}
