package hk.org.ha.model.pms.udt.pivas;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PivasWorklistType implements StringValuedEnum {

	Inbox("I", "PIVAS Inbox"),
	DueOrderList("D", "PIVAS Due Order List"),
	Worklist("W", "Worklist"),
	FindPatient("P", "Find a Patient");
	
    private final String dataValue;
    private final String displayValue;

    PivasWorklistType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    } 

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PivasWorklistType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PivasWorklistType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PivasWorklistType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PivasWorklistType> getEnumClass() {
    		return PivasWorklistType.class;
    	}
    }
}



