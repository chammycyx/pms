package hk.org.ha.model.pms.biz.picking;

import static hk.org.ha.model.pms.prop.Prop.BATCH_DAYEND_ORDER_DURATION;
import static hk.org.ha.model.pms.prop.Prop.LABEL_DISPLABEL_LARGEFONT_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.PICKING_ONCE_ENABLED;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.label.DispLabelBuilderLocal;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.reftable.ItemLocation;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.uam.udt.AuthorizeStatus;
import hk.org.ha.model.pms.uam.vo.AuthenticateCriteria;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.MedOrderStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.printing.PrintStatus;
import hk.org.ha.model.pms.udt.printing.PrintType;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.reftable.OperationProfileType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.label.DispLabel;
import hk.org.ha.model.pms.vo.picking.PickItem;
import hk.org.ha.model.pms.vo.picking.PickItemListSummary;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.PrinterSelect;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.workflow.DispensingPmsModeLocal;
import hk.org.ha.service.biz.pms.uam.interfaces.UamServiceJmsRemote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("drugPickService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DrugPickServiceBean implements DrugPickServiceLocal {
	
	private static final String PICK_ITEM_CLASS_NAME = PickItem.class.getName();
	
	private static final PeriodFormatter pf = new PeriodFormatterBuilder()
	 .appendYears()
	 .appendSuffix("y")
	 .appendSeparator(" ")
	 .appendMonths()
	 .appendSuffix("m")
	 .appendSeparator(" ")
	 .appendDays()
	 .appendSuffix("d")
	 .toFormatter();
	
	@PersistenceContext
	private EntityManager em;
	
	private boolean updateSuccess;
	
	private String resultMsg; 
		
	@In
	private Workstore workstore;
	
	@In
	private SysProfile sysProfile;
	
	@In
	private PrinterSelect printerSelect;
	
	@In
	private DispLabelBuilderLocal dispLabelBuilder;
	
	@In
	private UamServiceJmsRemote uamServiceProxy;
	
	@In
	private PrintAgentInf printAgent;
	
	@In 
	private UamInfo uamInfo;
	
	@In
	private PropMap propMap;
	
	@In
	private DispensingPmsModeLocal dispensingPmsMode;
	
	@In
	private OperationProfile activeProfile;
	
	@Out(required = false)
	private Long dispOrderId;
	
	private final static String ACTIVE_OPERATION_MODE = "active.operation.mode";
	private final static String WORKFLOW_EDS_BPM_ENABLED = "workflow.eds.bpm.enabled";
	
	public void checkUserCode(String userCode) {
		resultMsg = null;
		if (!sysProfile.isTesting()) {
			if ( !sysProfile.isDebugEnabled() && !checkValidUserCode(userCode).equals(AuthorizeStatus.Succeed) ) {
				resultMsg = "0415";
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public PickItemListSummary updateDispOrderItemStatusToPicked(Date ticketDate, String ticketNum, Integer itemNum, String userCode, Long workstationId){

		PickItemListSummary pickItemListSummary = new PickItemListSummary();
		
		updateSuccess = false;
		resultMsg = null;
		
		if (!sysProfile.isTesting()) {
			checkUserCode(userCode);
			if ( StringUtils.isNotBlank(resultMsg) ) {
				return pickItemListSummary;
			}
			if (checkTicketDate(ticketDate) > 0 ){
				resultMsg = "0058";
				return pickItemListSummary;
			}
		}

		em.createQuery(
				"select o.id from DispOrder o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
				" where o.workstore = :workstore" +
				" and o.orderType = :orderType" +
				" and o.ticketDate = :ticketDate" +
				" and o.ticketNum = :ticketNum" +
				" and o.status not in :dispOrderStatus")
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", ticketDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		List<DispOrderItem> dispOrderItemList = em.createQuery(
				"select o from DispOrderItem o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
				" where o.dispOrder.workstore = :workstore" +
				" and o.dispOrder.orderType = :orderType" +
				" and o.dispOrder.ticketDate = :ticketDate" +
				" and o.dispOrder.ticketNum = :ticketNum" +
				" and o.dispOrder.status <> :dispOrderDeletedStatus"+
				" and o.itemNum = :itemNum" +
				" and o.status <> :deletedStatus")
				.setParameter("deletedStatus", DispOrderItemStatus.Deleted)
				.setParameter("workstore", workstore)
				.setParameter("orderType", OrderType.OutPatient)
				.setParameter("ticketDate", ticketDate, TemporalType.DATE)
				.setParameter("ticketNum", ticketNum)
				.setParameter("dispOrderDeletedStatus", DispOrderStatus.SysDeleted)
				.setParameter("itemNum", itemNum)
				.setHint(QueryHints.BATCH, "o.dispOrder")
				.getResultList();

		if ( !dispOrderItemList.isEmpty() ) { 
			
			DispOrderItem dispOrderItem = dispOrderItemList.get(0);

			if ( dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted ) { 
				updateSuccess = false;
				resultMsg = "0074";
				return pickItemListSummary;
			}
			
			if ( dispOrderItem.getDispOrder().getAdminStatus() == DispOrderAdminStatus.Suspended ) {
				updateSuccess = false;
				resultMsg = "0059";
				return pickItemListSummary;
			}
			
			if ( dispOrderItem.getStatus() == DispOrderItemStatus.KeepRecord ) {
				updateSuccess = false;
				resultMsg = "0061";
				return pickItemListSummary;
			}
			
			if ( dispOrderItem.getStatus() == DispOrderItemStatus.Vetted) {
				
				dispOrderItem.setStatus(DispOrderItemStatus.Picked);
				dispOrderItem.setPickUser(userCode);
				dispOrderItem.setPickDate(new Date());

				updateSuccess = true;
				resultMsg = "0073";
				
				if ( activeProfile.getType() == OperationProfileType.NonPeak ) {
					if ( PICKING_ONCE_ENABLED.get() ) {
						updateDispOrderStatusToAssembledOnNonPeakMode(dispOrderItem.getDispOrder(), userCode);
						return pickItemListSummary; 
					}
				}
				
				int count = ((Long) em.createQuery(
						"select count(o) from DispOrderItem o" + // 20120303 index check : DispOrderItem.dispOrder : FK_DISP_ORDER_ITEM_01
						" where o.dispOrder = :dispOrder" +
						" and o.status = :dispOrderItemStatus" +
						" and o.pharmOrderItem.medOrderItem.status not in :medOrderItemStatus" +
						" and o.pharmOrderItem.medOrderItem.medOrder.status <>  :medOrderStatus")
						.setParameter("dispOrder", dispOrderItem.getDispOrder())
						.setParameter("dispOrderItemStatus", DispOrderItemStatus.Vetted)
						.setParameter("medOrderItemStatus", Arrays.asList(MedOrderItemStatus.SysDeleted, MedOrderItemStatus.Deleted))
						.setParameter("medOrderStatus", MedOrderStatus.SysDeleted)
						.getSingleResult()).intValue();
				
				if ( count == 0 ){

					DispOrder dispOrder = dispOrderItem.getDispOrder();
					
					if ("EDS".equals(propMap.getValue(ACTIVE_OPERATION_MODE)) && propMap.getValueAsBoolean(WORKFLOW_EDS_BPM_ENABLED)) 
					{
						dispOrderId = dispOrder.getId();
						Contexts.getSessionContext().set("dispOrderId", dispOrderId);
						dispensingPmsMode.updateToPickedStatus(dispOrder);
					} 
					else 
					{
						dispOrder.setStatus(DispOrderStatus.Picked);
					}
					dispOrder.setPickDate(new Date());
					
				} else {

					if ( dispOrderItem.getDispOrder().getStatus() != DispOrderStatus.Picking) {

						DispOrder dispOrder = dispOrderItem.getDispOrder();
						dispOrder.setStatus(DispOrderStatus.Picking);
					}
				}
				em.flush();
				em.clear();
			}
			else if ( dispOrderItem.getStatus() == DispOrderItemStatus.Picked &&
					activeProfile.getType() == OperationProfileType.NonPeak &&
					PICKING_ONCE_ENABLED.get()) {
				
				dispOrderItem.setStatus(DispOrderItemStatus.Picked);
				dispOrderItem.setPickUser(userCode);
				dispOrderItem.setPickDate(new Date());

				updateSuccess = true;
				resultMsg = "0073";
				
				updateDispOrderStatusToAssembledOnNonPeakMode(dispOrderItem.getDispOrder(), userCode);
			}
			else {

				updateSuccess = false;
				resultMsg = "0060";
			}
			
		} else {
			List<DispOrder> dispOrderList = em.createQuery(
					"select o from DispOrder o" + // 20160615 index check : DispOrder.workstore,orderType,ticketDate,ticketNum : I_DISP_ORDER_15
					" where o.workstore = :workstore" +
					" and o.orderType = :orderType" +
					" and o.ticketDate = :ticketDate" +
					" and o.ticketNum = :ticketNum" +
					" and o.status <> :dispOrderDeletedStatus")
					.setParameter("workstore", workstore)
					.setParameter("orderType", OrderType.OutPatient)
					.setParameter("ticketDate", ticketDate, TemporalType.DATE)
					.setParameter("ticketNum", ticketNum)
					.setParameter("dispOrderDeletedStatus", DispOrderStatus.SysDeleted)
					.getResultList();
			
			if ( !dispOrderList.isEmpty() ) {
				if ( itemNum <= dispOrderList.get(0).getMaxItemNum() ) {
					updateSuccess = false;
					resultMsg = "0074";
				} else {
					updateSuccess = false;
					resultMsg = "0061";
				}
			} else {
				updateSuccess = false;
				resultMsg = "0061";
			}
			
		}
		
		if ( updateSuccess ) {
			pickItemListSummary = retrievePickingList(workstationId);
		}
		
		return pickItemListSummary;
	}
	
	private AuthorizeStatus checkValidUserCode(String userCode) {
		AuthenticateCriteria authenticateCriteria = new AuthenticateCriteria();
		authenticateCriteria.setProfileCode(uamInfo.getProfileCode());
		authenticateCriteria.setUserName(userCode);
		return uamServiceProxy.authenticate(authenticateCriteria);
	}

	public boolean isUpdateSuccess() {
		return updateSuccess;
	}

	public String getResultMsg() {
		return resultMsg;
	}
	
	private int checkTicketDate(Date ticketDate){		
		Period period = pf.parsePeriod(BATCH_DAYEND_ORDER_DURATION.get().toString()+"d");		 
		DateTime endDate = new DateTime();
		DateTime dummy = new DateTime(ticketDate);
		
		DateTime startDate = endDate.minus(period);
		 
		return startDate.toDate().compareTo(dummy.toDate());

	}
	
	@SuppressWarnings("unchecked")
	private void updateDispOrderStatusToAssembledOnNonPeakMode(DispOrder dispOrder, String userCode){
		
		List<DispOrderItem> dispOrderItemList = em.createQuery(
				"select o from DispOrderItem o" + // 20120303 index check : DispOrderItem.dispOrder : FK_DISP_ORDER_ITEM_01
				" where o.dispOrder = :dispOrder" +
				" and o.status in :statusList" ) 
				.setParameter("dispOrder", dispOrder)
				.setParameter("statusList", DispOrderItemStatus.Vetted_Picked)
				.getResultList();
		
		if ( !dispOrderItemList.isEmpty() ) {
			for ( DispOrderItem dispOrderItem : dispOrderItemList ) {					
				dispOrderItem.setStatus(DispOrderItemStatus.Assembled);
				dispOrderItem.setAssembleDate(new Date());
				dispOrderItem.setAssembleUser(userCode);
				
				dispOrderItem.setPickDate(new Date());
				dispOrderItem.setPickUser(userCode);
			}
			dispOrderItemList.get(0).getDispOrder().setStatus(DispOrderStatus.Assembled);
			em.flush();
			em.clear();
		}
	}

	public void reprintDispLabel(List<Long> dispOrderItemIdList, String printLang) {
		List<RenderAndPrintJob> renderAndPrintJobList = new ArrayList<RenderAndPrintJob>();
		for (Long dispOrderItemId:dispOrderItemIdList) {
			DispOrderItem dispOrderItem = em.find(DispOrderItem.class, dispOrderItemId);
			
			PrintOption printOption = new PrintOption();
			
			if (StringUtils.equals(printLang, "Chinese")) {
				printOption.setPrintLang(PrintLang.Chi);	
			} else {
				printOption.setPrintLang(PrintLang.Eng);
			}
			if (dispOrderItem.getBaseLabel().getLargeLayoutFlag() != null && dispOrderItem.getBaseLabel().getLargeLayoutFlag()) {
				printOption.setPrintType(PrintType.LargeLayout);
			} else if (LABEL_DISPLABEL_LARGEFONT_ENABLED.get(Boolean.FALSE)) {
				printOption.setPrintType(PrintType.LargeFont);
			} else {
				printOption.setPrintType(PrintType.Normal);
			}
			printOption.setPrintStatus(PrintStatus.Print);
			
			renderAndPrintJobList.add(new RenderAndPrintJob("DispLabel",
										printOption,
										printerSelect,
										dispLabelBuilder.getDispLabelParam((DispLabel)dispOrderItem.getBaseLabel()),
										Arrays.asList((DispLabel)dispOrderItem.getBaseLabel())));
		}
		
		printAgent.renderAndPrint(renderAndPrintJobList);
	}
	
	public PickItemListSummary retrievePickingList(Long workstationId) {
		PickItemListSummary pickItemListSummary = new PickItemListSummary();
		pickItemListSummary.setPendingItemList(retrievePendingItem(workstationId));
		pickItemListSummary.setPickedItemList(retrievePickedItem(workstationId));
		return pickItemListSummary;
	}
	
	private List<PickItem> retrievePendingItem(Long workstationId){
		List<DispOrderItemStatus> statusList = new ArrayList<DispOrderItemStatus>();
		statusList.add(DispOrderItemStatus.Vetted);
		
		List<PickItem> pendingItemList = retrieveDrugPickItemList(workstationId, statusList);
		
		Set<String> binNumSet = new HashSet<String>();
		
		for ( PickItem pickItem : pendingItemList ) {
			if ( StringUtils.isNotBlank(pickItem.getBinNum()) ) { 
				binNumSet.add(pickItem.getBinNum());
			}
		}
		
		if ( !binNumSet.isEmpty() ){
			Map<String, MachineType> binNumMap = retrieveBinNumMap(binNumSet);
			for ( PickItem pendingItem : pendingItemList ) {
				if ( StringUtils.isNotBlank(pendingItem.getBinNum()) ) { 
					pendingItem.setMachineType(binNumMap.get(pendingItem.getBinNum()));
				}
			}
		}
		return pendingItemList;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, MachineType> retrieveBinNumMap(Set<String> binNumSet){
		List<ItemLocation> itemLocationList = em.createQuery(
				"select o from ItemLocation o" + // 20120303 index check : ItemLocation.workstore,binNum : FK_ITEM_LOCATION_03
				" where o.binNum in :binNumSet" +
				" and o.workstore = :workstore")
				.setParameter("binNumSet", binNumSet)
				.setParameter("workstore", workstore)
				.getResultList();
		
		HashMap<String, MachineType> binNumMap = new HashMap();
		for (ItemLocation itemLocation : itemLocationList){
			binNumMap.put(itemLocation.getBinNum(), itemLocation.getMachine().getType());
		}

		return binNumMap;
	}
	
	private List<PickItem> retrievePickedItem(Long workstationId){
		List<DispOrderItemStatus> statusList = new ArrayList<DispOrderItemStatus>();
		statusList.add(DispOrderItemStatus.Picked);
		statusList.add(DispOrderItemStatus.Assembled);
		statusList.add(DispOrderItemStatus.Checked);
		statusList.add(DispOrderItemStatus.Issued);
		
		List<PickItem> pickedItemList = retrieveDrugPickItemList(workstationId, statusList);
		
		Set<String> binNumSet = new HashSet<String>();
		
		for ( PickItem pickItem : pickedItemList ) {
			if ( StringUtils.isNotBlank(pickItem.getBinNum()) ) { 
				binNumSet.add(pickItem.getBinNum());
			}
		}
		
		if ( !binNumSet.isEmpty() ){
			Map<String, MachineType> binNumMap = retrieveBinNumMap(binNumSet);
			for ( PickItem pickedItem : pickedItemList ) {
				if ( StringUtils.isNotBlank(pickedItem.getBinNum()) ) { 
					pickedItem.setMachineType(binNumMap.get(pickedItem.getBinNum()));
				}
			}
		}
		return pickedItemList;
	}
	
	@SuppressWarnings("unchecked")
	private List<PickItem> retrieveDrugPickItemList(Long workstationId, List<DispOrderItemStatus> statusList){
		return 
			em.createQuery(
					"select new " + PICK_ITEM_CLASS_NAME + // 20150408 index check : DispOrder.workstore,orderType,ticketDate : I_DISP_ORDER_15
					"  (o.dispOrder.id, o.id, o.dispOrder.ticketDate, o.dispOrder.ticketNum," + 
					"  o.itemNum, o.pharmOrderItem.itemCode," +
					"  o.dispQty, o.pharmOrderItem.baseUnit, o.dispOrder.suspendCode," +
					"  o.dispOrder.pharmOrder.medOrder.orderNum, o.binNum, o.dispOrder.version)" +
					" from DispOrderItem o" +
					" where o.workstationId = :workstationId" +
					" and o.status in :statusList" +
					" and o.dispOrder.adminStatus = :dispOrderAdminStatus" +
					" and o.dispOrder.status not in :dispOrderStatus" +
					" and o.dispOrder.orderType = :orderType" +
					" and o.dispOrder.workstore = :workstore" +
					" and o.dispOrder.ticketDate between :startDate and :endDate" +
					" and o.dispOrder.dayEndStatus = :dayEndStatus" +
					" and o.dispOrder.batchProcessingFlag = :batchProcessingFlag" +
					" order by" +
					" o.dispOrder.ticketDate desc," +
					" o.dispOrder.ticketNum desc," +
					" o.itemNum")
					.setParameter("workstationId", workstationId)
					.setParameter("statusList", statusList)
					.setParameter("dispOrderAdminStatus", DispOrderAdminStatus.Normal)
					.setParameter("dispOrderStatus", DispOrderStatus.Deleted_SysDeleted)
					.setParameter("orderType", OrderType.OutPatient)
					.setParameter("workstore", workstore)
					.setParameter("startDate", new DateTime().minusDays(BATCH_DAYEND_ORDER_DURATION.get()).toDate())
					.setParameter("endDate", new Date())
					.setParameter("dayEndStatus", DispOrderDayEndStatus.None)
					.setParameter("batchProcessingFlag", Boolean.FALSE)
					.getResultList();		
	}
	
	@Remove
	public void destroy() {
		if (dispOrderId != null) {
			dispOrderId = null;
		}
	}

}
