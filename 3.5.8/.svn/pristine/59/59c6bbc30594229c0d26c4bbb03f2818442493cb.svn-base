package hk.org.ha.model.pms.biz.checkissue;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.BatchIssueLog;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;

@Stateful
@Scope(ScopeType.SESSION)
@Name("batchIssueListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class BatchIssueListServiceBean implements BatchIssueListServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In
	private Workstore workstore; 
	
	@Out(required = false)
	private List<BatchIssueLog> batchIssueLogList;
	
	@SuppressWarnings("unchecked")
	public void retrieveBatchIssueLogList(){
		DateTime today = new DateTime();
		batchIssueLogList = em.createQuery(
				"select o from BatchIssueLog o" + // 20120214 index check : BatchIssueLog.createDate : I_BATCH_ISSUE_LOG_01
				" where o.workstore.hospCode = :hospCode " +
				" and o.createDate < :nextday and o.createDate > :beforeDate " +
				" order by o.issueDate, " +
				" o.updateWorkstation.workstationCode")
				.setParameter("hospCode", workstore.getHospCode())
				.setParameter("nextday", today.plusDays(1).toDate() ,TemporalType.DATE)
				.setParameter("beforeDate", today.minusDays(6).toDate() ,TemporalType.DATE)
				.getResultList();
	}	

	@Remove
	public void destroy() 
	{
		if ( batchIssueLogList!=null ) { 
			batchIssueLogList = null; 
		}
	}
}
