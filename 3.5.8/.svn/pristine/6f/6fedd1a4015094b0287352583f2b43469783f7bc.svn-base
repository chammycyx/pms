package hk.org.ha.view.pms.main.medprofile
{
	import mx.collections.ArrayCollection;
	import mx.collections.SortField;
	import mx.utils.ObjectUtil;
	
	[DefaultProperty("sortFields")]
	
	public class AdvancedComparator
	{
		private var _sortFields:ArrayCollection;
		private var _parsedMap:Object;

		public function AdvancedComparator(sortFields:ArrayCollection = null)
		{
			this.sortFields = sortFields;
		}

		[Bindable]
		public function get sortFields():ArrayCollection
		{
			return _sortFields;
		}
		
		public function set sortFields(sortFields:ArrayCollection):void
		{
			var parsedMap:Object = new Object();
			
			for each (var field:SortField in sortFields)
			{
				if (field.name != null)
					parsedMap[field.name] = field.name.split("\.");
			}
			
			_sortFields = sortFields;
			_parsedMap = parsedMap;
		}
		
		
		public function compareFunction(a:Object, b:Object, fields:Array = null):int
		{
			if (sortFields == null || sortFields.length <= 0)
				return compareValues(a, a, true);
			
			var fieldResult:int;
			
			for each (var field:SortField in _sortFields)
			{
				if (field.name == null)
				{
					fieldResult = field.compareFunction(a, b) * (field.descending ? -1 : 1);
					if (fieldResult == 0)
						continue;
					else
						return fieldResult;
				}
				
				var tokens:Array = _parsedMap[field.name];
				var refA:Object = a;
				var refB:Object = b;
				
				for each (var token:String in tokens)
				{
					refA = refA[token];
					refB = refB[token];
				}
				
				fieldResult = compareValues(refA, refB, field.caseInsensitive) *
						(field.descending ? -1 : 1);
				
				if (fieldResult != 0)
					return fieldResult;
			}
			return 0;
		}
		
		private function compareValues(a:Object, b:Object, caseInsensitive:Boolean):int
		{
			if (a is Date && b is Date)
			{
				return ObjectUtil.dateCompare(a as Date, b as Date);
			}
			else if (a is Number && b is Number)
			{
				return ObjectUtil.numericCompare(a as Number, b as Number);
			} 
			else
			{
				return ObjectUtil.stringCompare(a == null ? null : a.toString(),
						b == null ? null : b.toString(), caseInsensitive);
			}
		}
	}
}