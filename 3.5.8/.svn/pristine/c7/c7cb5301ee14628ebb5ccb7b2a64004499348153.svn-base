<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:mds="hk.org.ha.view.pms.main.alert.mds.*"
	width="920"
	keyUp="keyUpEventHandler(event)">
	
	<fx:Metadata>
		[Name("ddiMsgPopup")]
	</fx:Metadata>
	
	<fc:states>
		<s:State name="onHandDrug"/>
		<s:State name="currentRx"/>
	</fc:states>
	
	<fx:Script>
		<![CDATA[
			import mx.managers.PopUpManager;
			
			import spark.components.Button;
			
			import hk.org.ha.event.pms.main.alert.AuditAlertMsgEvent;
			import hk.org.ha.event.pms.main.alert.druginfo.popup.ShowMonographPopupEvent;
			import hk.org.ha.event.pms.main.alert.mds.RetrieveOverridePermissionEvent;
			import hk.org.ha.event.pms.main.alert.mds.popup.ShowDdiMsgPopupEvent;
			import hk.org.ha.event.pms.main.alert.mds.popup.ShowOverrideReasonLogonPopupEvent;
			import hk.org.ha.event.pms.main.alert.mds.popup.ShowOverrideReasonOpPopupEvent;
			import hk.org.ha.event.pms.main.onestop.ClearKeyboardBufferEvent;
			import hk.org.ha.event.pms.main.report.PrintMdsOpRptEvent;
			import hk.org.ha.event.pms.main.vetting.EnableVettingByBarcodeEvent;
			import hk.org.ha.model.pms.persistence.AlertEntity;
			import hk.org.ha.model.pms.persistence.disp.PharmOrder;
			import hk.org.ha.model.pms.udt.disp.MedOrderDocType;
			import hk.org.ha.model.pms.udt.disp.MedOrderPrescType;
			import hk.org.ha.model.pms.udt.disp.OnHandSourceType;
			import hk.org.ha.model.pms.vo.alert.mds.AlertDdim;
			import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;
			
			private var doNotPrescribeFunc:Function;
			private var prescribeAFunc:Function;
			private var prescribeBFunc:Function;
			private var overrideAlertFunc:Function;
			private var closeFunc:Function;
			
			[Bindable]
			private var alertMsg:AlertMsg;
			
			[In] [Bindable]
			public var pharmOrder:PharmOrder;
			
			[In] [Bindable]
			public var propMap:PropMap;
			
			[In] [Bindable]
			public var orderViewInfo:OrderViewInfo;
			
			[In] [Bindable]
			public var ddimOnHandSourceType:OnHandSourceType;
			
			[Bindable]
			private var dhOrderFlag:Boolean;
			
			[Bindable]
			private var moiDdiAlert:AlertEntity;
			
			[Bindable]
			private var alertDdim:AlertDdim;
			
			[Bindable]
			private var onHandDrugName:String;
			
			[Bindable]
			private var screenEnableFlag:Boolean = true;
			
			private var ddiMsgPopup:DdiMsgPopup;
			
			private function doNotPrescribeBtnHandler():void {
				screenEnableFlag = false;
				
				var desc:String;
				if (currentState == "currentRx") {
					desc = "A="+alertDdim.drugName1+", B="+alertDdim.drugName2;
				}
				else {
					desc = onHandDrugName;
				}
				
				dispatchEvent(new AuditAlertMsgEvent("#0646", desc, function():void {
					if (doNotPrescribeFunc != null) {
						doNotPrescribeFunc();
					}
					dispatchEvent(new ClearKeyboardBufferEvent());
					dispatchEvent(new EnableVettingByBarcodeEvent(true));
					PopUpManager.removePopUp(ddiMsgPopup);
				}));
			}
			
			private function prescribeABtnHandler():void {
				screenEnableFlag = false;
				dispatchEvent(new AuditAlertMsgEvent("#0647", "A="+alertDdim.drugName1+", B="+alertDdim.drugName2, function():void {
					if (prescribeAFunc != null) {
						prescribeAFunc();
					}
					dispatchEvent(new ClearKeyboardBufferEvent());
					dispatchEvent(new EnableVettingByBarcodeEvent(true));
					PopUpManager.removePopUp(ddiMsgPopup);
				}));
			}
			
			private function prescribeBBtnHandler():void {
				screenEnableFlag = false;
				dispatchEvent(new AuditAlertMsgEvent("#0648", "A="+alertDdim.drugName1+", B="+alertDdim.drugName2, function():void {
					if (prescribeBFunc != null) {
						prescribeBFunc();
					}
					dispatchEvent(new ClearKeyboardBufferEvent());
					dispatchEvent(new EnableVettingByBarcodeEvent(true));
					PopUpManager.removePopUp(ddiMsgPopup);
				}));
			}
			
			private function overrideAlertBtnHandler():void {
				screenEnableFlag = false;
				dispatchEvent(new RetrieveOverridePermissionEvent(
					retrievePermissionForOverrideSuccessHandler,
					retrievePermissionForOverrideFaultHandler)
				);
			}
			
			private function retrievePermissionForOverrideFaultHandler():void {
				screenEnableFlag = true;
				dispatchEvent(new ShowOverrideReasonLogonPopupEvent(
					retrievePermissionForOverrideSuccessHandler)
				);
			}
			
			private function retrievePermissionForOverrideSuccessHandler():void {
				screenEnableFlag = true;
				var evt:ShowOverrideReasonOpPopupEvent = new ShowOverrideReasonOpPopupEvent(alertMsg);
				evt.ddiAlertIndex = 0;
				evt.proceedFunc = overrideAlertSuccessHandler;
				evt.prescModeFlag = true;
				evt.cancelFunc = closeFunc;
				dispatchEvent(evt);
			}
			
			private function overrideAlertSuccessHandler():void {
				if (overrideAlertFunc != null) {
					overrideAlertFunc();
				}
				dispatchEvent(new ClearKeyboardBufferEvent());
				dispatchEvent(new EnableVettingByBarcodeEvent(true));
				PopUpManager.removePopUp(ddiMsgPopup);
			}
			
			private function closeBtnHandler():void {
				screenEnableFlag = false;
				if (closeFunc != null) {
					closeFunc();
				}
				dispatchEvent(new ClearKeyboardBufferEvent());
				dispatchEvent(new EnableVettingByBarcodeEvent(true));
				PopUpManager.removePopUp(ddiMsgPopup);
			}
			
			public function init(evt:ShowDdiMsgPopupEvent):void {
				this.doNotPrescribeFunc = evt.doNotPrescribeFunc;
				this.prescribeAFunc     = evt.prescribeAFunc;
				this.prescribeBFunc     = evt.prescribeBFunc;
				this.overrideAlertFunc  = evt.overrideAlertFunc;
				this.closeFunc          = evt.closeFunc;
				this.alertMsg           = evt.alertMsg;
				
				if (evt.onHandFlag) {
					currentState = "onHandDrug";
				}
				else {
					currentState = "currentRx";
				}

				ddiMsgPopup = this;
				callLater(closeBtn.setFocus);
				focusManager.showFocus();
			}
			
			private function keyUpEventHandler(evt:KeyboardEvent):void {
				
				if (evt.ctrlKey) {
					switch (evt.charCode) {
						case 48: // Ctrl + 0
							triggerButton(closeBtn);
							evt.stopPropagation();
							break;
						
						case 49: // Ctrl + 1
							triggerButton(doNotPrescribeBtn);
							evt.stopPropagation();
							break;
						
						case 50: // Ctrl + 2
							triggerButton(prescribeABtn);
							evt.stopPropagation();
							break;
						
						case 51: // Ctrl + 3
							triggerButton(prescribeBBtn);
							evt.stopPropagation();
							break;
						
						case 52: // Ctrl + 4
							triggerButton(overrideAlertSuccess);
							evt.stopPropagation();
							break;
						
						case 53: // Ctrl + 5
							triggerButton(viewMonoBtn);
							evt.stopPropagation();
							break;
						
						case 56: // Ctrl + 8
							triggerButton(printBtn);
							evt.stopPropagation();
							break;
					}
				}
			}
			
			private function triggerButton(button:Button):void {
				if (button != null && button.enabled && button.visible) {
					button.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
			}
			
			private function printRptHandler():void {
				dispatchEvent(new PrintMdsOpRptEvent(alertMsg, orderViewInfo.alertProfile));
			}
			
			private function viewMonographBtnHandler():void {
				dispatchEvent(new ShowMonographPopupEvent(alertDdim.hospCode1, alertDdim.monographId));
			}
			
			private function constructOnHandDrugName(drugName1:String, drugName2:String, ordNo1:Number, orderNum:String):String {
				var currOrdNo:Number;
				if (orderNum == null) {
					currOrdNo = 0;
				}
				else {
					currOrdNo = Number(orderNum.substr(3));
				}
				if (currOrdNo == ordNo1) {
					return drugName1;
				}
				else {
					return drugName2;
				}
			}
		]]>
	</fx:Script>
	
	<fx:Binding source="{pharmOrder.medOrder.prescType == MedOrderPrescType.Dh}"
				destination="dhOrderFlag"/>
	
	<fx:Binding source="{alertMsg.ddiAlertList.getItemAt(0) as AlertEntity}"
				destination="moiDdiAlert"/>
	
	<fx:Binding source="{moiDdiAlert.alert as AlertDdim}"
				destination="alertDdim"/>
	
	<fx:Binding source="{constructOnHandDrugName(alertDdim.drugName1, alertDdim.drugName2, alertDdim.ordNo1, pharmOrder.medOrder.orderNum)}"
				destination="onHandDrugName"/>
	
	<s:Panel width="100%" title="Drug-Drug Interaction Checking - Clinical Intervention">
		
		<s:layout>
			<s:VerticalLayout paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0"/>
		</s:layout>
		
		<!-- Title -->
		<s:Label styleName="mdsAlertTitleStyle" width="100%"
				 text="CAUTION for {alertMsg.mdsOrderDesc}"/>
		
		<s:VGroup paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" gap="10"
				  width="100%">
			
			<s:List width="100%" height="150"
					requireSelection="true"
					useVirtualLayout="false"
					tabFocusEnabled="false"
					dataProvider="{alertMsg.ddiAlertList}">
				<s:itemRenderer>
					<fx:Component>
						<mds:AlertMsgItemRenderer autoDrawBackground="false" showMonographText="true" width="100%"/>
					</fx:Component>
				</s:itemRenderer>
			</s:List>

			<s:Panel title="Action" width="100%" minHeight="0">
				<s:layout>
					<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" gap="10"/>
				</s:layout>

				<s:HGroup verticalAlign="middle">
					<fc:ExtendedButton label="1. Do not Prescribe" width="150"
									   id="doNotPrescribeBtn"
									   click="doNotPrescribeBtnHandler()"
									   enabled="{screenEnableFlag &amp;&amp; !dhOrderFlag}"/>
					<s:Label text.currentRx="Both drugs will be deleted from current prescription"
							 text.onHandDrug="Delete {onHandDrugName} from current prescription"
							 styleName="boldTextStyle"/>
				</s:HGroup>
				
				<s:HGroup verticalAlign="middle" includeIn="currentRx">
					<fc:ExtendedButton label="2. Prescribe A only" width="150"
									   id="prescribeABtn"
									   click="prescribeABtnHandler()"
									   enabled="{screenEnableFlag &amp;&amp; !dhOrderFlag}"/>
					<s:Label text="Prescribe only (A) {alertDdim.drugName1}"
							 styleName="boldTextStyle"/>
				</s:HGroup>
				
				<s:HGroup verticalAlign="middle" includeIn="currentRx">
					<fc:ExtendedButton label="3. Prescribe B only" width="150"
									   id="prescribeBBtn"
									   click="prescribeBBtnHandler()"
									   enabled="{screenEnableFlag &amp;&amp; !dhOrderFlag}"/>
					<s:Label text="Prescribe only (B) {alertDdim.drugName2}"
							 styleName="boldTextStyle"/>
				</s:HGroup>
				
				<s:HGroup verticalAlign="middle">
					<fc:ExtendedButton label.currentRx="4. Override Alert"
									   label.onHandDrug="2. Override Alert"
									   width="150"
									   id="overrideAlertSuccess"
									   click="overrideAlertBtnHandler()"
									   styleName="alertOverrideBtnStyle"
									   enabled="{screenEnableFlag}"/>
					<s:Label text.currentRx="Prescribe (A) &amp; (B) and give overriding reason(s)"
							 text.onHandDrug="Prescribe {onHandDrugName} and give overriding reason(s)"
							 styleName="alertOverrideLabelStyle"/>
				</s:HGroup>
			</s:Panel>
			
			<s:Label width="100%" styleName="disclaimerStyle" visible="{ddimOnHandSourceType != OnHandSourceType.Corporate}" includeInLayout="{ddimOnHandSourceType != OnHandSourceType.Corporate}"
					 text="Disclaimer:{'\n'}Drug is subjected to Drug-Drug Interaction Checking against other medication(s) on the current prescription and the on hand drug prescribed at your local hospital. The system would alert clinical staff of the contraindicated / important interaction only."/>
				
			<s:Label width="100%" styleName="disclaimerStyle" visible="{ddimOnHandSourceType == OnHandSourceType.Corporate}" includeInLayout="{ddimOnHandSourceType == OnHandSourceType.Corporate}"
					 text="Disclaimer:{'\n'}Drug is subjected to Drug-Drug Interaction Checking against other medication(s) on the current prescription and the on hand drug prescribed at all hospitals. The system would alert clinical staff of the contraindicated / important interaction only."/>
			
			<s:HGroup width="100%">
				
				<mx:Spacer width="100%"/>
				
				<fc:ExtendedButton label="Print MDS Report" id="printBtn"
								   click="printRptHandler()"
								   enabled="{screenEnableFlag}"/>
				<fc:ExtendedButton label="View Monograph" id="viewMonoBtn"
								   visible="{propMap.getValueAsBoolean('alert.mds.enabled')}"
								   includeInLayout="{propMap.getValueAsBoolean('alert.mds.enabled')}"
								   click="viewMonographBtnHandler()"
								   enabled="{screenEnableFlag}"/>
				<fc:ExtendedButton label="Close" id="closeBtn"
								   click="closeBtnHandler()"
								   enabled="{screenEnableFlag}"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
	
</fc:ExtendedNavigatorContent>
