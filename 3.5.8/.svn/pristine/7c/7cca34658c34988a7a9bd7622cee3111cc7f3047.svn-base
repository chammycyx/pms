<?xml version="1.0" encoding="utf-8"?>
<s:NavigatorContent xmlns:fx="http://ns.adobe.com/mxml/2009" 
					xmlns:s="library://ns.adobe.com/flex/spark" 
					xmlns:mx="library://ns.adobe.com/flex/mx"
					xmlns="hk.org.ha.view.pms.main.vetting.mp.*"
					xmlns:m="hk.org.ha.fmk.pms.flex.components.message.*"	
					xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*">
	
	<fx:Declarations>
		<m:SystemMessagePopupProp id="profileNotFoundMsgPopupProp" messageCode="0316" setOkButtonOnly="true"/>
		
		<mx:DateFormatter id="dateFormatterDueDate" formatString="J:NN DD-MMM"/>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ListCollectionView;
			import mx.events.ListEvent;
			
			import flashx.textLayout.elements.TextFlow;
			
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
			import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			
			[Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[Bindable]
			public var propMap:PropMap;
			
			[Bindable]
			public var privateFlag:Boolean;
			
			[Bindable]
			public var readOnly:Boolean;
			
			[Bindable]
			public var srcList:ListCollectionView;
			
			[Bindable]
			public var doubleClickHandler:Function;
			
			[Bindable]
			public var multiSelectionFlag:Boolean;
			
			[Bindable]
			public var dataFieldStr:String;
			
			[Bindable]
			public var selectedItem:*;
			
			[Bindable]
			public var selectedItems:Array;
			
			[Bindable]
			public var selectedIndex:int;
			
			[Bindable]
			public var state:String
			
			[Bindable]
			public var applyActiveListSorting:Function;
			
			[Bindable]
			public var applyInactiveListSorting:Function;
			
			[Bindable]
			public var drugDescSortCbxSelected:Boolean;
			
			[Bindable]
			public var selectable:Boolean = true;
			
			[Bindable]
			public var itemNumToIconTextFlowDictionary:Dictionary = new Dictionary;
			[Bindable]
			public var itemNumToDrugDescTextFlowDictionary:Dictionary = new Dictionary;
			[Bindable]
			public var itemNumToInstructionTextFlowDictionary:Dictionary = new Dictionary;
			[Bindable]
			public var itemEditFromInactiveListFlag:Boolean = false;
			
			public var itemListChangeHandler:Function;
			
			public function getMedProfileMoItem(item:Object):MedProfileMoItem {
				if (item is MedProfileItem) {
					return MedProfileItem(item).medProfileMoItem;
				} else if (item is MedProfileMoItem) {
					return item as MedProfileMoItem;
				} else if (item is Replenishment) {
					return Replenishment(item).medProfileMoItem;
				} else {
					return null;					
				}
			}
			
			private function dueDateLabelFunc(item:Object, column:DataGridColumn):String
			{
				try {
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return "";
					}
					return dateFormatterDueDate.format(MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0)).dueDate);
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			private function itemCodeLabelFunc(item:Object, column:DataGridColumn):String
			{
				try {
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return "";
					}
					return MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0)).itemCode;
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			public function drugDescLabelFunc(item:Object):TextFlow
			{
				try {
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return new TextFlow();
					}
					return MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0)).fullDrugDescTlf(true, (item is MedProfileMoItem));
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return new TextFlow();
			}
			
			public function instructionLabelFunc(item:Object):TextFlow
			{
				try {
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return new TextFlow();
					}
					return MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0)).regimen.getManualEntryDrugInstructionTlf();
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return new TextFlow();
			}
			
			private function durationLabelFunc(item:Object, column:DataGridColumn):String
			{
				try {
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return "";
					}
					var poi:MedProfilePoItem = MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0));
					if (poi.refillDurationText == "") {
						return "";
					} else {
						return poi.refillDurationText + " " + poi.refillDurationUnit.dataValue.toUpperCase();
					}
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			private function adjQtyLabelFunc(item:Object, column:DataGridColumn):String
			{
				try {	
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return "";
					}
					var poi:MedProfilePoItem = MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0));
					if (isNaN(poi.adjQty) || poi.adjQty == 0 || !poi.baseUnit) {
						return "";
					} else  {
						return MedProfileUtils.formatSignedNumber(poi.adjQty);
					}
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			private function issueQtyLabelFunc(item:Object, column:DataGridColumn):String
			{
				try {
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return "";
					}
					var poi:MedProfilePoItem = MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0));
					if (isNaN(poi.issueQty) || poi.issueQty == 0 || !poi.baseUnit) {
						return "";
					} else  {
						return poi.issueQtyText + " " + poi.baseUnit;
					}
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			private function lastAdjQtyLabelFunc(item:Object, column:DataGridColumn):String
			{
				try {		
					if (!item || !getMedProfileMoItem(item).medProfilePoItemList) {
						return "";
					}
					var poi:MedProfilePoItem = MedProfilePoItem(getMedProfileMoItem(item).medProfilePoItemList.getItemAt(0));
					if (isNaN(poi.lastAdjQty) || poi.lastAdjQty == 0 || !poi.baseUnit) {
						return "";
					} else  {
						return MedProfileUtils.formatSignedNumber(poi.lastAdjQty);
					}
				} catch (e:Error) {
					trace(e.getStackTrace());					
				}
				return "";
			}
			
			public function selectMedProfileItem(id:Number):Boolean {
				
				for each (var medProfileItem:* in list.dataProvider) {
					if (medProfileItem.id == id) {
						list.selectedItem = medProfileItem;
						return true;
					}
				}
				return false;
			}
			
			protected function listDoubleClickHandler(event:ListEvent):void
			{
				if (doubleClickHandler != null) {
					doubleClickHandler(event);
				}
			}
			
			
			protected function listChangeHandler(event:ListEvent):void
			{
				if (itemListChangeHandler != null) {
					itemListChangeHandler(event);
				}
			}
			
			private function dataGridRowColor(item:Object, rowIndex:int, dataIndex:int, color:uint):uint 
			{
				if (item is MedProfileItem) {
					if (MedProfileItem(item).itemColorFlag) 
					{
						return 0xE7E5E5;
					}
					else 
					{
						return color;
					}
				}
				return color;
			}
			
		]]>
	</fx:Script>
	
	<fx:Binding source="{list.selectedItem}" destination="selectedItem"/>
	<fx:Binding source="{list.selectedItems}" destination="selectedItems"/>
	
	<fc:ExtendedDataGrid id="list" 
						 dataProvider="{srcList}"
						 selectable="{selectable}"
						 selectedIndex="@{selectedIndex}"
						 width="993" height="100%"
						 doubleClickEnabled="true"
						 focusEnabled="true"
						 variableRowHeight="true" wordWrap="true"
						 borderStyle="none" sortableColumns="false"
						 resizableColumns="false" draggableColumns="false"
						 allowMultipleSelection="{multiSelectionFlag}"
						 itemDoubleClick="listDoubleClickHandler(event)"
						 change="listChangeHandler(event)"
						 rowColorFunction="dataGridRowColor"
						 paddingTop="0"
						 paddingBottom="0"
						 paddingLeft="0"
						 paddingRight="0"
						 verticalScrollPolicy="on"
						 horizontalScrollPolicy="on">
		<fc:columns>
			<mx:DataGridColumn headerText="" width="20" dataField="{dataFieldStr}">
				<mx:itemRenderer>
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false">
							
							<fx:Script>
								<![CDATA[
									import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
									import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
									
									public function isVerified(data:*):Boolean
									{
										var moi:MedProfileMoItem = outerDocument.getMedProfileMoItem(data);
										if (data is MedProfileMoItem || outerDocument.itemEditFromInactiveListFlag) {
											return moi.medProfileItem.status == MedProfileItemStatus.Verified || moi.medProfileItem.verifyDate != null;	
										} else {
											return moi.medProfileItem.status == MedProfileItemStatus.Verified;
										}
									}
								]]>
							</fx:Script>
							
							<s:HGroup visible="{isVerified(data)}"
									  includeInLayout="{isVerified(data)}"
									  toolTip="{MedProfileItemStatus.Verified.displayValue}">
								<s:BitmapImage source="@Embed(source='/assets/verified.png')"/>
							</s:HGroup>							
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:itemRenderer>
			</mx:DataGridColumn>
			<mx:DataGridColumn headerText="" width="52" dataField="{dataFieldStr}">
				<mx:itemRenderer>							
					<fx:Component>
						<s:MXDataGridItemRenderer render="mxdatagriditemrenderer1_renderHandler(event)" 
												  autoDrawBackground="false">
							<fx:Script>
								<![CDATA[
									import flash.text.engine.FontWeight;
									
									import flashx.textLayout.elements.InlineGraphicElement;
									import flashx.textLayout.elements.ParagraphElement;
									import flashx.textLayout.elements.SpanElement;
									import flashx.textLayout.elements.TextFlow;
									
									import hk.org.ha.event.pms.main.alert.mds.RetrieveMpAlertMsgEvent;
									import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveFmIndicationListByMedProfileMoItemEvent;
									import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
									import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
									import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
									import hk.org.ha.model.pms.udt.vetting.ActionStatus;
									import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;																		
									
									[Bindable]
									private var p:ParagraphElement;
									private var img:InlineGraphicElement;																		
									[Bindable]
									private var indicatorTextFlow:TextFlow = null;
									
									private function appendIcon(document:ParagraphElement, iconClass:Class, visible:Boolean):void
									{
										var image:InlineGraphicElement;
										if (visible) {
											image = new InlineGraphicElement();
											image.source = iconClass;
											image.paddingRight = 2;
											document.addChild(image);
										}
									}
									
									private function appendText(document:ParagraphElement, text:String, styleName:String, visible:Boolean):void
									{
										var span:SpanElement;
										if (visible) {
											span = new SpanElement();
											span.paddingRight = 2;
											span.fontStyle = FontStyle.ITALIC;
											span.fontWeight = FontWeight.BOLD;
											span.text = text;
											span.styleName = styleName;
											document.addChild(span);
										}
									}
									
									protected function mxdatagriditemrenderer1_renderHandler(event:Event):void
									{
										try {
											
											if (data == null) {
												return;
											} else {
												
												if (outerDocument.list.horizontalScrollPosition >= 2) {
													return;
												}
												
												var moi:MedProfileMoItem = outerDocument.getMedProfileMoItem(data);
												var poi:MedProfilePoItem = moi.medProfilePoItemList[0] as MedProfilePoItem;
												
												indicatorTextFlow = new TextFlow;
												p = new ParagraphElement;																								
												
												appendIcon(p, MedProfileUtils.urgentImg, moi.urgentFlag && MedProfileUtils.isToday(moi.firstPrintDate, true))
												appendIcon(p, MedProfileUtils.suspendedImg, moi.suspendCode != null);
												appendIcon(p, MedProfileUtils.mdsAlertImg, moi.alertFlag);
												appendIcon(p, MedProfileUtils.fmIndicationImg, moi.fmFlag);
												appendIcon(p, MedProfileUtils.dangerDrugImg, poi.dangerDrugFlag);
												appendIcon(p, MedProfileUtils.wardStockImg, poi.wardStockFlag);
												appendIcon(p, MedProfileUtils.redispImg, poi.reDispFlag);
												appendIcon(p, MedProfileUtils.singleDispImg, poi.singleDispFlag); 
												appendIcon(p, MedProfileUtils.purchaseByPatientImg, poi.actionStatus == ActionStatus.PurchaseByPatient);
												appendIcon(p, MedProfileUtils.continueWithOwnStockImg, poi.actionStatus == ActionStatus.ContinueWithOwnStock);
												appendIcon(p, MedProfileUtils.dispenseInClinicImg, poi.actionStatus == ActionStatus.DispInClinic);
												appendIcon(p, MedProfileUtils.safetyNetImg, poi.actionStatus == ActionStatus.SafetyNet);
												appendIcon(p, MedProfileUtils.unitDoseImg, poi.unitDoseFlag);
												appendIcon(p, MedProfileUtils.directLabelPrintImg, poi.directPrintFlag);
												appendIcon(p, MedProfileUtils.mdsExceptionImg, moi.mdsSuspendReason != null);
												appendIcon(p, MedProfileUtils.pivasImg, moi.pivasFlag);
												appendIcon(p, MedProfileUtils.unitDoseExceptionImg, moi.unitDoseExceptFlag);
												appendIcon(p, MedProfileUtils.itemSuspendedImg, moi.medProfileItem.isItemSuspend);
												
												indicatorTextFlow.addChild(p);
												
												outerDocument.itemNumToIconTextFlowDictionary[data.orgItemNum] = indicatorTextFlow;
											}
										} catch (e:Error) {
											trace(e.message);
										}
									}
									
									private function processMdsAlert():void {
										if ( outerDocument.state == 'active' || outerDocument.state == 'inactive') { //active list and inactive list
											var allowEditFlag:Boolean = (!(data is MedProfileMoItem) && !outerDocument.readOnly);
											var retrieveMpAlertEvent:RetrieveMpAlertMsgEvent = new RetrieveMpAlertMsgEvent(outerDocument.getMedProfileMoItem(data), allowEditFlag);
											dispatchEvent(retrieveMpAlertEvent);
										}else{
											//readOnly - for itemEdit
											dispatchEvent(new RetrieveMpAlertMsgEvent(outerDocument.getMedProfileMoItem(data), false));
										}
									}
									
									private function processFmIndication():void {
										dispatchEvent(new RetrieveFmIndicationListByMedProfileMoItemEvent(outerDocument.getMedProfileMoItem(data)));
									}
								]]>
							</fx:Script>
							
							<s:layout>
								<s:BasicLayout/>
							</s:layout>
							<s:RichText id="drugDescTxt" textFlow="{outerDocument.itemNumToDrugDescTextFlowDictionary[data.orgItemNum]}" fontWeight="bold" paddingLeft="5" width="385" height="100%"
										paddingRight="10" paddingTop="5" paddingBottom="0" visible="false"/>
							<s:RichText id="instructionTxt" textFlow="{outerDocument.itemNumToInstructionTextFlowDictionary[data.orgItemNum]}" fontWeight="bold" paddingLeft="5" width="210" height="100%"
										paddingRight="10" paddingTop="5" paddingBottom="0" visible="false"/>
							<s:RichText id="indicators" width="52" height="100%" textFlow="{indicatorTextFlow}" mouseMove="MedProfileUtils.toggleTooltip(event, indicators.textFlow)"
										mouseOut="MedProfileUtils.destroyTooltip()" click="MedProfileUtils.processDrugOrderMouseClick(event, indicators.textFlow, processMdsAlert, processFmIndication)"/>
							
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:itemRenderer>
			</mx:DataGridColumn>
			<mx:DataGridColumn headerText="Due Date" width="82" dataField="{dataFieldStr}" labelFunction="dueDateLabelFunc" headerWordWrap="true" itemRenderer="mx.controls.Label"/>
			<mx:DataGridColumn headerText="Item" width="57" dataField="{dataFieldStr}" labelFunction="itemCodeLabelFunc" headerWordWrap="true" itemRenderer="mx.controls.Label"/>
			<mx:DataGridColumn headerText="Drug Description" width="385" dataField="{dataFieldStr}" wordWrap="true" headerWordWrap="true" >
				<mx:headerRenderer>
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false">
							
							<fx:Script>
								<![CDATA[
									protected function drugDescSortCbx_clickHandler(event:MouseEvent):void
									{
										if (outerDocument.state == 'active') {
											outerDocument.applyActiveListSorting(true, drugDescSortCbx.selected, false);
										}else if (outerDocument.state == 'inactive') {
											outerDocument.applyInactiveListSorting(drugDescSortCbx.selected);
										}
									}
								]]>
							</fx:Script>
							
							<s:HGroup height="100%" width="100%" verticalAlign="middle">
								<s:Label width="100%" text="Drug Description" paddingLeft="5" paddingTop="3"/>
							</s:HGroup>
							<s:HGroup height="100%" width="100%" verticalAlign="middle" horizontalAlign="right" 
									  visible="{outerDocument.state=='active'||outerDocument.state=='inactive'}" 
									  includeInLayout="{outerDocument.state=='active'||outerDocument.state=='inactive'}" 
									  paddingRight="5" paddingTop="0">
								<fc:ExtendedCheckBox label="A-Z" id="drugDescSortCbx" selected="@{outerDocument.drugDescSortCbxSelected}" 
													 click="drugDescSortCbx_clickHandler(event)"/>
							</s:HGroup>
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:headerRenderer>
				<mx:itemRenderer>			
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false" render="mxdatagriditemrenderer2_renderHandler(event)">
							<fx:Script>
								<![CDATA[
									import flashx.textLayout.elements.TextFlow;
									
									[Bindable]
									private var textFlow:TextFlow;
									
									protected function mxdatagriditemrenderer2_renderHandler(event:Event):void
									{
										textFlow = outerDocument.drugDescLabelFunc(data);
										outerDocument.itemNumToDrugDescTextFlowDictionary[data.orgItemNum] = textFlow;
									}
								]]>
							</fx:Script>
							<s:layout>
								<s:BasicLayout/>
							</s:layout>
							<s:RichText id="drugDescTxt" textFlow="{textFlow}" fontWeight="bold" paddingLeft="5" width="385" height="100%"
										paddingRight="10" paddingTop="5" paddingBottom="0"/>
							<s:RichText id="instructionTxt" textFlow="{outerDocument.itemNumToInstructionTextFlowDictionary[data.orgItemNum]}" fontWeight="bold" paddingLeft="5" width="210" height="100%"
										paddingRight="10" paddingTop="5" paddingBottom="0" visible="false"/>
							<s:RichText id="indicators" textFlow="{outerDocument.itemNumToIconTextFlowDictionary[data.orgItemNum]}" width="52" height="100%" visible="false"/>
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:itemRenderer>
			</mx:DataGridColumn>						
			<mx:DataGridColumn headerText="Instruction" width="210" dataField="{dataFieldStr}" wordWrap="true" headerWordWrap="true" >
				<mx:itemRenderer>			
					<fx:Component>
						<s:MXDataGridItemRenderer autoDrawBackground="false" render="mxdatagriditemrenderer3_renderHandler(event)">
							<fx:Script>
								<![CDATA[
									import flashx.textLayout.elements.TextFlow;
									
									[Bindable]
									private var textFlow:TextFlow;
									
									protected function mxdatagriditemrenderer3_renderHandler(event:Event):void
									{
										textFlow = outerDocument.instructionLabelFunc(data);
										outerDocument.itemNumToInstructionTextFlowDictionary[data.orgItemNum] = textFlow;
									}
								]]>
							</fx:Script>
							<s:layout>
								<s:BasicLayout/>
							</s:layout>
							<s:RichText id="drugDescTxt" textFlow="{outerDocument.itemNumToDrugDescTextFlowDictionary[data.orgItemNum]}" fontWeight="bold" paddingLeft="5" width="385" height="100%"
										paddingRight="10" paddingTop="5" paddingBottom="0" visible="false"/>
							<s:RichText id="instructionTxt" textFlow="{textFlow}" fontWeight="bold" paddingLeft="5" width="210" height="100%"
										paddingRight="10" paddingTop="5" paddingBottom="0" />
							<s:RichText id="indicators" textFlow="{outerDocument.itemNumToIconTextFlowDictionary[data.orgItemNum]}" width="52" height="100%" visible="false"/>
						</s:MXDataGridItemRenderer>
					</fx:Component>
				</mx:itemRenderer>				
			</mx:DataGridColumn>
			<mx:DataGridColumn headerText="Dur." width="41" dataField="{dataFieldStr}" labelFunction="durationLabelFunc" headerWordWrap="true" itemRenderer="mx.controls.Label"/>
			<mx:DataGridColumn headerText=" Adj." width="55" dataField="{dataFieldStr}" labelFunction="adjQtyLabelFunc" headerWordWrap="false" itemEditor="mx.controls.Label" paddingLeft="0" paddingRight="0"/>
			<mx:DataGridColumn headerText=" Issue Qty" width="75" dataField="{dataFieldStr}" labelFunction="issueQtyLabelFunc" headerWordWrap="true" itemEditor="mx.controls.Label" paddingLeft="0" paddingRight="0"/>
			<mx:DataGridColumn headerText=" Last Adj." width="55" dataField="{dataFieldStr}" labelFunction="lastAdjQtyLabelFunc" headerWordWrap="false" itemEditor="mx.controls.Label" paddingLeft="0" paddingRight="0"/>
		</fc:columns>
	</fc:ExtendedDataGrid>
</s:NavigatorContent>
