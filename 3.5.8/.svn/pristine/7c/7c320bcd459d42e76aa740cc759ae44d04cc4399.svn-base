<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("orderPendingSuspendLogEnqView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.enquiry.RetrieveOrderPendingSuspendLogEnqListEvent;
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.fmk.pms.flex.utils.BarcodeReader;
			import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.service.app.util.calendar.AbbDateField;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.FlexEvent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			import mx.utils.StringUtil;
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In][Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[In][Bindable]
			public var orderPendingSuspendLogEnqList:ArrayCollection;
			
			[In] [Bindable]
			public var propMap:PropMap;
			
			public var isClose:Boolean;
			
			private var lastPopupDismissTime:Number;
			
			private static var LABEL_BARCODESUBMIT_DELAY:String = "label.barcodeSubmit.delay";

			public override function onShowLater():void {
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					clearHandler();
				}
				
				callLater(disbaleMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showOrderPendingSuspendLogEnqShortcutKeyHandler);
				focusManager.showFocus();
			}
			
			private function showOrderPendingSuspendLogEnqShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("OrderPendingSuspendLogEnqView"));
			}
			
			private function disbaleMenuBarCloseButton():void
			{
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
				
			}
			
			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			private function closeHandler():void {
				clearHandler();
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			private function isValidInput():Boolean {
				fromDate.errorString = "";
				toDate.errorString = "";
				caseNumTxt.errorString = "";
				stage.focus = null;
				
				if (caseNumLabel.selected)
				{
					if ( StringUtil.trim(caseNumTxt.text) == "" || caseNumTxt.text == null )
					{
						caseNumTxt.setFocus();
						caseNumTxt.errorString = sysMsgMap.getMessage("0399"); 
						return false;
					}
				}
				else if (fromToDateLabel.selected)
				{
					if (!ExtendedDateFieldValidator.isValidDate(fromDate)) {
						fromDate.setFocus();
						if (fromDate.text == "") {
							fromDate.errorString = sysMsgMap.getMessage("0441");
						} else {
							fromDate.errorString = sysMsgMap.getMessage("0001");
						}
						return false;	
					} else if (!ExtendedDateFieldValidator.isValidDate(toDate)) {
						toDate.setFocus();	
						if (toDate.text == "") {
							toDate.errorString = sysMsgMap.getMessage("0442");
						} else {
							toDate.errorString = sysMsgMap.getMessage("0001");
						}
						return false;
					} else if (ObjectUtil.dateCompare(toDate.selectedDate,fromDate.selectedDate)== -1)
					{	toDate.errorString = sysMsgMap.getMessage("0653");
						toDate.setFocus();
						return false;
					}
				}
				return true
			}
			
			private function retrieveHandler():void {				
					
				var fDate:Date = fromDate.selectedDate;
				var tDate:Date = toDate.selectedDate;
				var caseNum:String = "";
				var filter:String = "";
				
				if(isValidInput()){
					if ( filterCriteria.selectedIndex == 0 ) {
						filter = null;
					} else if ( filterCriteria.selectedIndex == 1 ) {
						filter = "S";
					} else if ( filterCriteria.selectedIndex == 2 ) {
						filter = "P";
						}	
							
					if(fromToDateLabel.selected) {
						fDate = fromDate.selectedDate;
						tDate = toDate.selectedDate;
						dispatchEvent(new RetrieveOrderPendingSuspendLogEnqListEvent(fDate, tDate, null, filter));
					}
					if (caseNumLabel.selected) {
						caseNum = caseNumTxt.text;
						dispatchEvent(new RetrieveOrderPendingSuspendLogEnqListEvent(null, null, caseNum, filter));
					}
				}
			}
			
			public function retrieveHandlerResult():void {
				
				clearErrorString();
				
				if ( orderPendingSuspendLogEnqList == null || orderPendingSuspendLogEnqList.length == 0 ) {
					this.infoString = sysMsgMap.getMessage("0005");
					return;
				}
				stateChange('Retrieve');
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( !PopupUtil.isAnyVisiblePopupExists() ) 
				{
					if ( evt.charCode == 52 && evt.ctrlKey && pmsToolbar.clearButton.enabled)	
					{
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
					}
					else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled)  //ctrl + 0 - close
					{
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
				}			
			}
			
			public function showSystemMessage(errorCode:String):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.okHandler = okPopupHandler;
				msgProp.setOkButtonOnly = true;
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			} 
			
			//close the popup and record the time that 
			private function okPopupHandler(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				lastPopupDismissTime = new Date().time;
			}
			
			private function validateDateField(evt:FocusEvent):void {
				fromDate.errorString = "";
				toDate.errorString = "";
				
				if (!ExtendedDateFieldValidator.isValidDate(fromDate) && fromDate.text != "" )
				{
					fromDate.errorString = sysMsgMap.getMessage("0001");
				}
				
				if (!ExtendedDateFieldValidator.isValidDate(toDate)&& toDate.text != "")
				{
					toDate.errorString = sysMsgMap.getMessage("0001");
				}
			}
			
			public function clearHandler():void{
				stateChange('Clear');
			}
			
			public function stateChange(state:String):void
			{
				switch (state)
				{
					case 'Retrieve':
						pmsToolbar.retrieveButton.enabled = false;
						itemSearchRadioGroup.enabled = false;
						
						fromDate.enabled = false;
						toDate.enabled = false;
						caseNumTxt.enabled = false;
						filterCriteria.enabled = false;
						
						enquiryGrid.setFocus();
						
						break;
					case 'Clear':	
						pmsToolbar.retrieveButton.enabled = true;
						itemSearchRadioGroup.enabled = true;
						
						fromToDateLabel.selected = true;
						fromDate.selectedDate = new Date();
						toDate.selectedDate = new Date();
						fromDate.enabled = true;
						toDate.enabled = true;
						caseNumTxt.text = "";
						filterCriteria.selectedIndex = 0;
						filterCriteria.enabled = true;
						fromToDateLabel.setFocus();
						ctx.orderPendingSuspendLogEnqList = null;
						clearErrorString();
						
						break;
				}
			}
			
			private function clearErrorString():void {
				fromDate.errorString = "";
				toDate.errorString = "";
				caseNumTxt.errorString = "";
				this.infoString = "";
			}
			
			private function dateKeyBoardFunc(event:KeyboardEvent):void {
				if(event.keyCode==13){
					retrieveHandler();
				}
			}
			

			protected function itemRadioGroupChangeHandler(event:Event):void
			{
				if ( fromToDateLabel.selected ) {
					caseNumTxt.errorString = "";
					caseNumTxt.text = "";
					return;
				}
				
				if ( caseNumLabel.selected ) {
					fromDate.errorString = "";
					toDate.errorString = "";
					fromDate.selectedDate = new Date();
					toDate.selectedDate = new Date();
				}
			}
			
			private function actionDateLabelFunc(item:Object, column:DataGridColumn):String {
				return dateTimeFormatter.format(item[column.dataField]);
			}
			
			private function actionTakenByLabelFunc(item:Object, column:DataGridColumn):String {
				var actionDisplay:String = item["action"];
				if (actionDisplay != "O" ) {
					return item["actionByName"] + " (" + item["actionByUser"] + ")";	
				} else {
					return "";
				}
			}
			
			private function actionLabelFunc(item:Object, column:DataGridColumn):String {
				var actionDisplay:String = item["action"];
				if (actionDisplay == "S" ) {
					return "Suspend";
				}
				else if (actionDisplay == "RS" ) {
					return "Resume suspend";
				}
				else if (actionDisplay == "P" ) {
					return "Mark pending order";
				}
				else if (actionDisplay == "O" ) {
					return "Override pending order";
				}
				else if (actionDisplay == "RP" ) {
					return "Resume from pending";
				}
				else return "";
			}
			
			private function doctorLabelFunc(item:Object, column:DataGridColumn):String {
				var actionDisplay:String = item["action"];
				if (actionDisplay == "O" ) {
					return item["actionByName"] + " (" + item["actionByUser"] + ")";	
				} else {
					return "";
				}
			}

			private function reasonLabelFunc(item:Object, column:DataGridColumn):String {
				var actionDisplay:String = item["action"];
				var suspendPendCode:String = item["suspendPendCode"];
				if (actionDisplay == "S" || actionDisplay == "P" ) {
					if ( suspendPendCode == null || suspendPendCode == "--" ) {
						return "";
					} else {
						return suspendPendCode + " - " + item["reason"];	
					}
				} else {
					return "";
				}
			}
			
			private function remarkLabelFunc(item:Object, column:DataGridColumn):String {
				var actionDisplay:String = item["action"];
				if (actionDisplay == "RP" ) {
					return item["reason"];	
				} else {
					return "";
				}
			}
			
			private function msgByDoctorLabelFunc(item:Object, column:DataGridColumn):String {
				var actionDisplay:String = item["action"];
				if (actionDisplay == "O" ) {
					return item["reason"];	
				} else {
					return "";
				}
			}

		]]>
	</fx:Script>
	
	<fx:Declarations>
		<s:RadioButtonGroup id="itemSearchRadioGroup" change="itemRadioGroupChangeHandler(event)"/>
	</fx:Declarations>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<fc:Toolbar id="pmsToolbar" width="100%" />
	
	<s:VGroup width="100%" height="100%" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10">		
		
		<s:Panel title="Search Criteria" width="100%" height="70">
			<s:layout>
				<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
			</s:layout>		
			<s:HGroup width="100%" verticalAlign="middle">
				<s:RadioButton width="80" groupName="itemSearchRadioGroup" label="From Date" id="fromToDateLabel" selected="true"/>
				<fc:ExtendedAbbDateField width="120" id="fromDate" selectedDate="{new Date()}" enabled="{fromToDateLabel.selected}" 
										 keyFocusChange="validateDateField(event)" mouseFocusChange="validateDateField(event)" creationComplete="fromDate.addEventListener(KeyboardEvent.KEY_UP, dateKeyBoardFunc)"/>
				<s:Label width="10"/>
				<s:Label text="To Date" width="50"/>
				<fc:ExtendedAbbDateField width="120" id="toDate" enabled="{fromToDateLabel.selected}" selectedDate="{new Date()}" 
										 keyFocusChange="validateDateField(event)" mouseFocusChange="validateDateField(event)" creationComplete="toDate.addEventListener(KeyboardEvent.KEY_UP, dateKeyBoardFunc)"/>
				<s:Label width="20"/>
				<s:RadioButton label="Case No." groupName="itemSearchRadioGroup" id="caseNumLabel" width="80" click="caseNumTxt.setFocus()"/>
				<fc:UppercaseTextInput id="caseNumTxt" width="120" maxChars="12" enabled="{caseNumLabel.selected}" enter="BarcodeReader.scanEnterBarcode(event,propMap.getValueAsInteger(LABEL_BARCODESUBMIT_DELAY),retrieveHandler)"/>
				<s:Label width="40"/>
				<s:Label width="30" text="Filter"/>
				<fc:AdvancedDropDownList id="filterCriteria" enabled="true" width="230" selectedIndex="-1">
					<s:ArrayCollection>
						<fx:String>Suspend and Pending Related</fx:String>
						<fx:String>Suspend Related</fx:String>
						<fx:String>Pending Related</fx:String>
					</s:ArrayCollection>
				</fc:AdvancedDropDownList>
			</s:HGroup>
		</s:Panel>
		
		<fc:ExtendedDataGrid id="enquiryGrid" dataProvider="{orderPendingSuspendLogEnqList}" width="100%" height="100%" variableRowHeight="true"
					 draggableColumns="false" resizableColumns="false" wordWrap="true" sortableColumns="false" horizontalScrollPolicy="auto">
			<fc:columns>
				<mx:DataGridColumn headerText="Action Date" dataField="actionDate" width="120" labelFunction="actionDateLabelFunc"/>
				<mx:DataGridColumn headerText="Case No." dataField="caseNum" width="100"/>
				<mx:DataGridColumn headerText="Ward" dataField="ward" width="50"/>
				<mx:DataGridColumn headerText="Order Description" dataField="orderDesc" width="400" id="drugText">
					<mx:itemRenderer>
						<fx:Component>
							<s:MXDataGridItemRenderer width="400" height="{data.orderDesc.Height}">
								<fx:Script>
									<![CDATA[
										import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
									]]>
								</fx:Script>
								
								<s:layout>
									<s:VerticalLayout gap="0"/>
								</s:layout>
								<s:RichText id="drugOrderText" fontWeight="normal" width="400" height="100%" 
											paddingLeft="5" paddingRight="5" paddingTop="5" paddingBottom="2" 
											textFlow="{MedProfileUtils.importFromTlfString(data.orderDesc)}"/>
							</s:MXDataGridItemRenderer>
						</fx:Component>
					</mx:itemRenderer>
				</mx:DataGridColumn>
				<mx:DataGridColumn headerText="Action Taken By" dataField="actionTakenBy" width="150" labelFunction="actionTakenByLabelFunc"/>
				<mx:DataGridColumn headerText="Action" dataField="action" width="150" labelFunction="actionLabelFunc"/>
				<mx:DataGridColumn headerText="Suspend / Pending Reason" dataField="reason" width="300" labelFunction="reasonLabelFunc"/>
				<mx:DataGridColumn headerText="Supplementary Reason" dataField="supplReason" width="200"/>
				<mx:DataGridColumn headerText="Resume Pending Remark" dataField="remark" width="200" labelFunction="remarkLabelFunc"/>
				<mx:DataGridColumn headerText="Doctor" dataField="doctor" width="150" labelFunction="doctorLabelFunc"/>
				<mx:DataGridColumn headerText="Message by Doctor" dataField="msgByDoctor" width="150" labelFunction="msgByDoctorLabelFunc"/>
				<mx:DataGridColumn headerText="Prescribing Hosipital" dataField="prescHosp" width="100"/>
				<mx:DataGridColumn headerText="PMS Hospital" dataField="hosp" width="100"/>
				<mx:DataGridColumn headerText="PMS Working Store" dataField="workstore" width="100"/>
				<mx:DataGridColumn headerText="CMS Item Number" dataField="itemNum" width="100"/>
			</fc:columns>
		</fc:ExtendedDataGrid>
	</s:VGroup>	
</fc:ExtendedNavigatorContent>