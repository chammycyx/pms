<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009" 
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"	
	xmlns:mo="hk.org.ha.view.pms.main.vetting.mo.*"
	width="100%">
	
	<fx:Script>
		<![CDATA[			
			import flexlib.scheduling.util.DateUtil;
			
			import hk.org.ha.event.pms.main.vetting.RetrieveCapdCalciumStrengthListEvent;
			import hk.org.ha.event.pms.main.vetting.RetrieveCapdConcentrationListEvent;
			import hk.org.ha.model.pms.dms.vo.Capd;
			import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
			import hk.org.ha.model.pms.vo.rx.CapdItem;
			import hk.org.ha.model.pms.vo.rx.CapdRxDrug;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.collections.ArrayCollection;
			import mx.utils.ObjectUtil;
			
			import org.granite.tide.seam.Context;
			
			import spark.events.DropDownEvent;
			import spark.events.IndexChangeEvent;
			
			[Bindable]
			public var medOrderItem:MedOrderItem;
			
			[Bindable]
			public var capdRxDrug:CapdRxDrug;
			
			[Bindable]
			public var capdSupplierSystemList:ArrayCollection;
			
			[Bindable]
			public var capdCalciumStrengthList:ArrayCollection;
			
			[Bindable]
			public var capdConcentrationList:ArrayCollection;
			
			[Bindable]
			public var actionStatusList:ArrayCollection;
			
			[Bindable]
			public var prnDuration:Number;
			
			[Bindable]
			public var fixPeriodFlag:Boolean;
			
			[Bindable]
			public var singleUseFlag:Boolean;
			
			[Bindable]
			private var convertBtnFocusFlag:Boolean;
			
			[Bindable]
			private var cancelBtnFocusFlag:Boolean;
			
			[Bindable]
			private var manuTypeFocusFlag:Boolean = false;
			
			[Bindable]
			private var calciumContentFocusFlag:Boolean = false;
			
			[Bindable]
			public var sysMsgMap:SysMsgMap;
			
			//Icon variables
			[Bindable]
			[Embed(source="/assets/add.png")]
			public var addIcon:Class;
			
			[Bindable]
			[Embed(source="/assets/remove.png")]
			public var removeIcon:Class;				
			
			//Function variable		
			[Bindable]
			public var addCapdItem:Function;
			
			[Bindable]
			public var removeCapdItem:Function;
			
			[Bindable]
			public var durationClickHandler:Function;
			
			[Bindable]
			public var convertClickHandler:Function;
			
			[Bindable]
			public var cancelClickHandler:Function;
			
			[Bindable]
			public var orderLineEditCapdDoseListKeyUpHandler:Function;
			
			//Capd Regimen Grid column width
			[Bindable]
			public var concentrationColWidth:int = 380;
			
			[Bindable]
			public var dosageColWidth:int = 75;
			
			[Bindable]
			public var dailyFreqColWidth:int = 100;
			
			[Bindable]
			public var prnColWidth:int = 27;
			
			[Bindable]
			public var durationColWidth:int = 188;
			
			[Bindable]
			public var qtyColWidth:int = 100;
			
			[Bindable]
			public var doseGroupAddRemoveColWidth:int = 23;
			
			[Bindable]
			public var doseAddRemoveColWidth:int = 23;
			
			//Component layout variables												
			[Bindable]
			public var dosageTxtInputWidth:int = 55;
			
			[Bindable]
			public var dosageUnitLblWidth:int = 40;
			
			[Bindable]
			public var prnChkboxWidth:int = 15;
			
			[Bindable]
			public var prnCbxWidth:int = 70;			
			
			[Bindable]
			public var qtyTxtInputWidth:int = 40;
			
			[Bindable]
			public var qtyUnitWidth:int = 40;
			
			[Bindable]			
			public var addRemoveBtnWidth:int = 18;
			
			[Bindable]
			public var durationStartDate:Date;
			
			//Layout variable
			
			[Bindable]
			public var paddingSpace:int=5;	
			
			[Bindable]
			public var bottomLayoutHeight:int=80;
			
			[Bindable]
			public var componentEnabled:Boolean=true;
			
			[Bindable]
			public var convertDisabled:Boolean = false;
			
			[Bindable]
			public var componentStyle:String = "";
			
			public function setFocusToManufacturer():void{
				supplierSystemComboBox.setFocus();
			}
			protected function supplierSystemComboBoxKeyDownHandler(event:KeyboardEvent):void
			{
				if ( event.keyCode == Keyboard.UP || event.keyCode == Keyboard.DOWN ) {
					supplierSystemComboBox.openDropDown();
				}
			}
			
			protected function supplierSystemComboBoxCloseHandler():void
			{
				if ( capdRxDrug.suspendedSupplierSystem && supplierSystemComboBox.textInput.text == capdRxDrug.orgSupplierSystem  ) 
				{
					return;
				} 
				
				capdRxDrug.suspendedSupplierSystem = false;
				if ( supplierSystemComboBox.selectedIndex < 0 ) 
				{
					supplierSystemComboBox.selectedItem = null;
					calciumContentComboBox.selectedItem = null;
					capdRxDrug.calciumStrength = calciumContentComboBox.selectedItem;
					capdRxDrug.supplierSystemErrorString = sysMsgMap.getMessage("0501");
					callLater(supplierSystemComboBox.setFocus);
				} 
				else 
				{
					capdRxDrug.supplierSystemErrorString = "";
				}
				
				capdRxDrug.supplierSystem = supplierSystemComboBox.selectedItem;
				capdRxDrug.calciumStrength = null;
				calciumContentComboBox.selectedIndex = -1;
				capdRxDrug.calciumStrengthErrorString = "";
				supplierSystemComboBox.errorString = capdRxDrug.supplierSystemErrorString;
				calciumContentComboBox.errorString = capdRxDrug.calciumStrengthErrorString;
				
				resetCapdItemList();
				dispatchEvent(new RetrieveCapdCalciumStrengthListEvent(supplierSystemComboBox.selectedItem));
				 
			}
			
			protected function supplierSystemComboBoxFocusOutHandler(event:FocusEvent):void
			{	
				try {
					if ( ((supplierSystemComboBox.errorString == null || supplierSystemComboBox.errorString == "") && supplierSystemComboBox.selectedItem != null) 
						&& ( !capdRxDrug.suspendedSupplierSystem && capdRxDrug.orgSupplierSystem != supplierSystemComboBox.textInput.text) ) 
					{
						if ( supplierSystemComboBox.textInput.text == "" || supplierSystemComboBox.selectedIndex < 0 ) 
						{
							supplierSystemComboBox.selectedIndex = -1;
							supplierSystemComboBoxCloseHandler();
						}
					}
					supplierSystemComboBox.errorString = "";
				} catch (e:Error){
					trace(e.getStackTrace());
				}
			}
			
			protected function calciumContentComboBoxKeyDownHandler(event:KeyboardEvent):void
			{
				if ( event.keyCode == Keyboard.UP || event.keyCode == Keyboard.DOWN ) {
					calciumContentComboBox.openDropDown();
				}
			}
			
			protected function calciumContentComboBoxCloseHandler():void
			{
				if ( capdRxDrug.suspendedCalciumStrength && calciumContentComboBox.textInput.text == capdRxDrug.orgCalciumStrength ) 
				{
					return;
				} 

				capdRxDrug.suspendedCalciumStrength = false;
				if ( calciumContentComboBox.selectedIndex < 0 ) 
				{
					calciumContentComboBox.selectedItem = null;
					calciumContentComboBox.textInput.text = "";
					capdRxDrug.calciumStrengthErrorString = sysMsgMap.getMessage("0502");
					callLater(calciumContentComboBox.setFocus);
				} 
				else 
				{
					capdRxDrug.calciumStrengthErrorString = "";
				}
				capdRxDrug.calciumStrength = calciumContentComboBox.selectedItem;
				calciumContentComboBox.errorString = capdRxDrug.calciumStrengthErrorString;
				resetCapdItemList();
				dispatchEvent(new RetrieveCapdConcentrationListEvent(supplierSystemComboBox.selectedItem, calciumContentComboBox.selectedItem));
				
			}
			
			protected function calciumContentComboBoxFocusOutHandler(event:FocusEvent):void
			{
				if ( ((calciumContentComboBox.errorString == null || calciumContentComboBox.errorString == "") && calciumContentComboBox.selectedItem != null)
						&&  (!capdRxDrug.suspendedCalciumStrength && calciumContentComboBox.textInput.text != capdRxDrug.orgCalciumStrength) ) 
				{
					if ( calciumContentComboBox.textInput.text == "" || calciumContentComboBox.selectedIndex < 0 ) 
					{
						calciumContentComboBox.selectedIndex = -1;
						calciumContentComboBoxCloseHandler();
					}
				}
				calciumContentComboBox.errorString = "";
			}
			
			private function resetCapdItemList():void{
				var numOfCapdItem:int = capdRxDrug.capdItemList.length;
				var orgList:ArrayCollection = ObjectUtil.copy(capdRxDrug.capdItemList) as ArrayCollection;
				capdRxDrug.capdItemList = null;
				var list:ArrayCollection = new ArrayCollection();
				
				for ( var i:int=0;i<numOfCapdItem;i++ ){
					var capdItem:CapdItem = new CapdItem();
					capdItem.addBtnEnabled = (i==numOfCapdItem-1);
					capdItem.removeBtnEnabled = (i>0);
					capdItem.startDate = CapdItem(orgList.getItemAt(i)).startDate;		
					capdItem.endDate = CapdItem(orgList.getItemAt(i)).endDate;
					capdItem.duration = CapdItem(orgList.getItemAt(i)).duration;
					capdItem.durationUnit = CapdItem(orgList.getItemAt(i)).durationUnit;
					
					capdItem.unitOfCharge = 0;
					capdItem.capd = new Capd();
					list.addItem(capdItem);
				}
				capdRxDrug.prnPercentVisibleFlag = false;
				capdRxDrug.capdItemList = list;
			}
			
			public function resetCapdAddRemoveBtn():void {
				var numOfCapdItem:int = capdRxDrug.capdItemList.length;
				for each(var capdItem:CapdItem in capdRxDrug.capdItemList)
				{
					var i:int = capdRxDrug.capdItemList.getItemIndex(capdItem);
					capdItem.addBtnEnabled = (i==numOfCapdItem-1);
					capdItem.removeBtnEnabled = (i>0);
				}
			}
			
			private function focusNextItem(evt:FocusEvent):void {
				switch ( evt.currentTarget ) {
					case supplierSystemComboBox:
						evt.preventDefault();
						if ( evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								callLater(cancelBtn.setFocus);
							}
						} else if ( evt.keyCode == Keyboard.TAB ) {
							callLater(calciumContentComboBox.setFocus);
						}
						break;
					case calciumContentComboBox:
						if ( evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB ) {
								evt.preventDefault();
								callLater(supplierSystemComboBox.setFocus);
							}
						}
						break;
					case specialInstructionTextArea:
						if ( evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB )
							{
								callLater(actionStatusDropDownList.setFocus);
							}
						} else if ( evt.keyCode == Keyboard.TAB ) 
						{
							callLater(convertBtn.setFocus);
						}
						break;
					case convertBtn:
						evt.preventDefault();
						if ( evt.shiftKey ) {
							if ( evt.keyCode == Keyboard.TAB )
							{
								callLater(specialInstructionTextArea.setFocus);
							}
						} else if ( evt.keyCode == Keyboard.TAB ) 
						{
							callLater(cancelBtn.setFocus);
						}
						break;
					case cancelBtn:
						evt.preventDefault();
						if ( componentEnabled ) {
							if ( evt.shiftKey ) {
								if ( evt.keyCode == Keyboard.TAB )
								{
									callLater(convertBtn.setFocus);
								}
							} else if ( evt.keyCode == Keyboard.TAB ) 
							{
								callLater(supplierSystemComboBox.setFocus);
							}
						}
						break;
				}
			}
			
			private function setActionStatusFocusFlag(focus:Boolean):void {
				try {
					capdRxDrug.actionStatusFocusFlag = focus;
				} catch(e:Error) {
					trace(e.getStackTrace());
				}
			}
			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<s:VGroup paddingLeft="0" paddingTop="0" 
			  paddingRight="0" paddingBottom="0"
			  width="100%"
			  gap="0">
		
		<s:HGroup width="100%" height="30" paddingLeft="5" paddingBottom="10" paddingTop="10" gap="10" verticalAlign="middle" keyUp="orderLineEditCapdDoseListKeyUpHandler(event)">
			<s:Label text="Manufacturer and Type" styleName="{manuTypeFocusFlag?'orderEditHeaderTextSelectStyle':'boldTextStyle'}"/>		
			<fc:ExtendedComboBox id="supplierSystemComboBox" 
								 width="250"
								 dataProvider="{capdSupplierSystemList}" 
								 selectedItem="{capdRxDrug.supplierSystem}"
								 errorString="{capdRxDrug.supplierSystemErrorString}"
								 enabled="{componentEnabled &amp;&amp; !convertDisabled}"
								 styleName="{capdRxDrug.supplierSystemStyle}"
								 keyFocusChange="focusNextItem(event)"
								 mouseEnabled="{!orderLineEditCapdDoseList.dateFieldErrorFlag}"
								 mouseChildren="{!orderLineEditCapdDoseList.dateFieldErrorFlag}"
								 focusIn="manuTypeFocusFlag = true"
								 focusOut="supplierSystemComboBoxFocusOutHandler(event);manuTypeFocusFlag = false"
								 typographicCase="uppercase"
								 close="supplierSystemComboBoxCloseHandler()"
								 keyDown="supplierSystemComboBoxKeyDownHandler(event)"/>	
			<s:Label text="Calcium Content" styleName="{calciumContentFocusFlag?'orderEditHeaderTextSelectStyle':'boldTextStyle'}"/>
			<fc:ExtendedComboBox id="calciumContentComboBox" 
								 width="150"
								 dataProvider="{capdCalciumStrengthList}" 
								 selectedItem="{capdRxDrug.calciumStrength}"
								 errorString="{capdRxDrug.calciumStrengthErrorString}"
								 enabled="{componentEnabled &amp;&amp; !convertDisabled}"
								 styleName="{capdRxDrug.calciumStrengthStyle}" 
								 mouseEnabled="{!orderLineEditCapdDoseList.dateFieldErrorFlag}"
								 mouseChildren="{!orderLineEditCapdDoseList.dateFieldErrorFlag}"
								 focusIn="calciumContentFocusFlag = true"
								 focusOut="calciumContentComboBoxFocusOutHandler(event);calciumContentFocusFlag = false"
								 typographicCase="uppercase"
								 keyFocusChange="focusNextItem(event)"
								 close="calciumContentComboBoxCloseHandler()"
								 keyDown="calciumContentComboBoxKeyDownHandler(event)"/>		
		</s:HGroup>
		
		<s:Line width="100%">
			<s:stroke>
				<s:SolidColorStroke weight="1" />
			</s:stroke>
		</s:Line>
		
		<s:HGroup paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" horizontalAlign="left" verticalAlign="middle" height="25" gap="0" fontWeight="bold">
			<s:HGroup verticalAlign="middle" horizontalAlign="left" paddingLeft="5" width="{concentrationColWidth}">
				<s:Label text="Concentration / Ingredient / Volume" styleName="{capdRxDrug.conventrationFocusFlag?'orderEditHeaderTextSelectStyle':''}"/>	
			</s:HGroup>
			<s:HGroup verticalAlign="middle" horizontalAlign="left" paddingLeft="5" width="{dosageColWidth}">
				<s:Label text="Dosage" styleName="{capdRxDrug.dosageFocusFlag?'orderEditHeaderTextSelectStyle':''}"/>
			</s:HGroup>	
			<s:HGroup verticalAlign="middle" horizontalAlign="left" paddingLeft="5" width="{dailyFreqColWidth}">
				<s:Label text="Daily Frequency" />
			</s:HGroup>
			<s:HGroup verticalAlign="middle" horizontalAlign="left" paddingLeft="5" width="{prnColWidth}">
				<s:Label text="PRN" styleName="{capdRxDrug.prnFocusFlag?'orderEditHeaderTextSelectStyle':''}"/>
			</s:HGroup>	
			<s:HGroup verticalAlign="middle" horizontalAlign="left" paddingLeft="5" width="{prnCbxWidth}">
				<s:Label text="Dispense" styleName="{capdRxDrug.prnPercentFocusFlag?'orderEditHeaderTextSelectStyle':''}" visible="{capdRxDrug.prnPercentVisibleFlag}"/>
			</s:HGroup>	
			<fc:ExtendedButton label="Duration" 
							   tabFocusEnabled="false"
							   width="{durationColWidth}" 
							   click="durationClickHandler(event)" enabled="{!convertDisabled}"
							   styleName="{capdRxDrug.durationFocusFlag?'orderEditHeaderTextSelectStyle':''}"/>
			<s:HGroup verticalAlign="middle" horizontalAlign="left" paddingLeft="10" width="{qtyColWidth + doseGroupAddRemoveColWidth}">
				<s:Label text="Qty" styleName="{capdRxDrug.qtyFocusFlag?'orderEditHeaderTextSelectStyle':''}"/>
			</s:HGroup>	
		</s:HGroup>
		
		
		<s:VGroup id="content" width="100%" height="250" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">		
			
			<!-- dose line -->
			<mo:OrderLineEditCapdDose id="orderLineEditCapdDoseList" 
									  dataProvider="{capdRxDrug.capdItemList}"
									  capdRxDrug="{capdRxDrug}"
									  tabFocusEnabled="false"
									  focusEnabled="false"
									  hasFocusableChildren="true"
									  capdConcentrationList="{capdConcentrationList}"
									  width="100%" height="100%"
									  borderVisible="false"		
									  addCapdItem="{addCapdItem}"
									  removeCapdItem="{removeCapdItem}"
									  dateFieldEnabled="{capdRxDrug.dateFieldEnabled}"
									  keyUp="orderLineEditCapdDoseListKeyUpHandler(event)"
									  styleName="boldTextStyle"
									  
									  durationColWidth="{durationColWidth}"
									  concentrationCalWidth="{concentrationColWidth}"
									  dosageColWidth="{dosageColWidth}"
									  dailyFreqColWidth="{dailyFreqColWidth}"								
									  prnColWidth="{prnColWidth+prnCbxWidth}"
									  doseAddRemoveColWidth="{doseAddRemoveColWidth}"
									  dosageTxtInputWidth="{dosageTxtInputWidth}"
									  dosageUnitLblWidth="{dosageUnitLblWidth}"
									  prnChkboxWidth="{prnChkboxWidth}"
									  prnCbxWidth="{prnCbxWidth}"
									  addRemoveBtnWidth="{addRemoveBtnWidth}"	
									  qtyColWidth="{qtyColWidth}"
									  qtyUnitWidth="{qtyUnitWidth}"
									  qtyTxtInputWidth="{qtyTxtInputWidth}"
									  
									  paddingSpace="{paddingSpace}"
									  
									  addIcon="{addIcon}"
									  removeIcon="{removeIcon}"
									  
									  durationStartDate="{durationStartDate}"
									  prnDuration="{prnDuration}"
									  componentEnabled="{componentEnabled}" 
									  convertDisabled="{convertDisabled}"
									  sysMsgMap="{sysMsgMap}"/>		
			
		</s:VGroup>
		
		<s:Line width="100%">
			<s:stroke>
				<s:SolidColorStroke weight="1" />
			</s:stroke>
		</s:Line>
		
		<s:VGroup width="100%" height="{bottomLayoutHeight}" paddingTop="5" paddingBottom="5" paddingLeft="0" paddingRight="0" gap="5">
			<s:HGroup width="100%" height="100%" paddingTop="5" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="10">
				<s:VGroup width="30%" height="100%" paddingLeft="10">
					<s:CheckBox label="Fixed Period" enabled="false" selected="{fixPeriodFlag}"/>
					<s:CheckBox label="Single Use" enabled="false" selected="{singleUseFlag}"/>
					<s:HGroup width="100%" verticalAlign="middle">
						<s:Label text="Action Status" styleName="{capdRxDrug.actionStatusFocusFlag?'orderEditHeaderTextSelectStyle':''}"/>
						<fc:AdvancedDropDownList id="actionStatusDropDownList" width="180" 
												 styleName="{componentStyle}"	
												 labelField="displayValue"
												 mouseChildren="{!orderLineEditCapdDoseList.dateFieldErrorFlag}"
												 enabled="{componentEnabled &amp;&amp; !convertDisabled}"
												 focusIn="{setActionStatusFocusFlag(true)}"
												 focusOut="{setActionStatusFocusFlag(false)}"/>	
					</s:HGroup>
				</s:VGroup>
				<s:Panel title="Special Instruction" width="100%" height="115">
					<s:layout>
						<s:VerticalLayout paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="0"/>
					</s:layout>
					<fc:ExtendedTextArea id="specialInstructionTextArea" 
								maxChars="100" 
								editable="{componentEnabled &amp;&amp; !convertDisabled}" enabled="{componentEnabled &amp;&amp; !convertDisabled}" 
								width="100%" height="100%" styleName="{componentStyle}" keyFocusChange="focusNextItem(event)"/>
				</s:Panel>
				<s:Panel id="postDispCommentPanel" title="Post-Dispensing Comment" width="100%" height="115">
					<s:layout>
						<s:VerticalLayout paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="0"/>
					</s:layout>
					<s:TextArea id="postDispCommentTextArea" 
								maxChars="100" enabled="false" width="100%" height="100%" styleName="orderEditableComponentStyle"/>
				</s:Panel>
			</s:HGroup>
			
			<s:HGroup width="100%" paddingRight="10" paddingBottom="5" paddingTop="0" paddingLeft="10" verticalAlign="middle" horizontalAlign="right">
				<fc:ExtendedButton id="convertBtn" label="Convert" enabled="{componentEnabled &amp;&amp; !convertDisabled}" keyFocusChange="focusNextItem(event)" 
								   focusIn="convertBtnFocusFlag = true" focusOut="convertBtnFocusFlag = false" click="convertClickHandler()" 
								   styleName="{convertBtnFocusFlag?'moButtonFocusStyle':'normalFontWeightStyle'}"/>
				<fc:ExtendedButton id="cancelBtn" label="Cancel" 
								   focusIn="cancelBtnFocusFlag = true" focusOut="cancelBtnFocusFlag = false"  enabled="{!convertDisabled}"
								   click="capdRxDrug.cancelBtnClickedFlag=true;cancelClickHandler()" keyFocusChange="focusNextItem(event)" 
								   styleName="{cancelBtnFocusFlag?'moButtonFocusStyle':'normalFontWeightStyle'}"/>
			</s:HGroup>
		</s:VGroup>
	</s:VGroup>
	
</fc:ExtendedNavigatorContent>
