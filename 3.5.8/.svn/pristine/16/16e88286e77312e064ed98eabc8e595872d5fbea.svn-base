<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="PivasWorkloadSummaryPatientXls" language="groovy" pageWidth="1000" pageHeight="595" orientation="Landscape" columnWidth="1000" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="1.4641000000000035"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String">
		<defaultValueExpression><![CDATA["PivasConsumptionRpt/"]]></defaultValueExpression>
	</parameter>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<parameter name="supplyDateFrom" class="java.util.Date"/>
	<parameter name="supplyDateTo" class="java.util.Date"/>
	<parameter name="satelliteFlag.dataValue" class="java.lang.String"/>
	<parameter name="itemCode" class="java.lang.String"/>
	<parameter name="drugKeyFlag" class="java.lang.Boolean"/>
	<field name="supplyDate" class="java.util.Date"/>
	<field name="itemCode" class="java.lang.String"/>
	<field name="pivasFormulaDesc" class="java.lang.String"/>
	<field name="ward" class="java.lang.String"/>
	<field name="totalNumOfDose" class="java.lang.Integer"/>
	<title>
		<band height="60">
			<textField>
				<reportElement x="0" y="0" width="1000" height="20"/>
				<textElement verticalAlignment="Middle" markup="none">
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["PIVAS Workload Summary (Patient)" + " (" + $P{hospCode} + " - " + $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="20" width="1000" height="20"/>
				<textElement verticalAlignment="Top" markup="none">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Supply Date: " +
(
    $P{supplyDateTo} != null ?
    new SimpleDateFormat("dd-MMM-yyyy").format($P{supplyDateFrom}) + " to " + new SimpleDateFormat("dd-MMM-yyyy").format($P{supplyDateTo}):
    new SimpleDateFormat("dd-MMM-yyyy HH:mm").format($P{supplyDateFrom})
) +
(
    ($P{satelliteFlag.dataValue} != null)?
    "   ,   " + "Satellite: " + $P{satelliteFlag.dataValue}:
    ""
) +
($P{itemCode} != null ? "   ,   "  + "Item Code: " + $P{itemCode}: "") +
( ($P{drugKeyFlag} != null && $P{drugKeyFlag}) ? "   ,   "  + "Drug Key: Y" : "")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Transparent" x="0" y="40" width="100" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Supply Date]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="100" y="40" width="100" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Item Code]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="200" y="40" width="500" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[PIVAS Formula]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="700" y="40" width="100" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Ward]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="800" y="40" width="200" height="20" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true" isStrikeThrough="false"/>
				</textElement>
				<text><![CDATA[Total Number of Doses]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="20">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="0" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[$F{supplyDate} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd-MMM-yyyy").format($F{supplyDate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="100" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[$F{itemCode} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="200" y="0" width="500" height="20">
					<printWhenExpression><![CDATA[$F{pivasFormulaDesc} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{pivasFormulaDesc}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="700" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[$F{ward} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ward}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="800" y="0" width="200" height="20">
					<printWhenExpression><![CDATA[$F{totalNumOfDose} != null]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{totalNumOfDose}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
