package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Customizer;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "ITEM_LOCATION")
@Customizer(AuditCustomizer.class)
public class ItemLocation extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "itemLocationSeq")
	@SequenceGenerator(name = "itemLocationSeq", sequenceName = "SQ_ITEM_LOCATION", initialValue = 100000000)
	private Long id;

	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;

	@Column(name = "BIN_NUM", nullable = false, length = 4)
	private String binNum;

	@Column(name = "BAKER_CELL_NUM", length = 5)
	private String bakerCellNum;
	
	@Column(name = "MAX_QTY", nullable = false, length = 5)
	private Integer maxQty;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "MACHINE_ID")
	private Machine machine;

	@Transient
	private boolean markDelete = false;	
	
	@Transient
	private DmDrug dmDrug;
	
	@Transient
	private String prepackQty;

	public String getPrepackQty() {
		return prepackQty;
	}

	public void setPrepackQty(String prepackQty) {
		this.prepackQty = prepackQty;
	}

	@PostLoad
	public void postLoad(){
		if(itemCode !=null && Identity.instance().isLoggedIn()){
			dmDrug = DmDrugCacher.instance().getDmDrug(itemCode);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBinNum() {
		return binNum;
	}

	public void setBinNum(String binNum) {
		this.binNum = binNum;
	}

	public String getBakerCellNum() {
		return bakerCellNum;
	}

	public void setBakerCellNum(String bakerCellNum) {
		this.bakerCellNum = bakerCellNum;
	}

	public Integer getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public boolean isMarkDelete() {
		return markDelete;
	}
	
	@JSON(include=false)
	public DmDrug getDmDrug(){
		return dmDrug;
	}
	
	public void setDmDrug(DmDrug dmDrug){
		this.dmDrug = dmDrug;
	}
}
