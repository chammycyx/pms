package hk.org.ha.model.pms.vo.alert;

import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
import hk.org.ha.model.pms.udt.alert.AllergenType;
import hk.org.ha.model.pms.vo.alert.ehr.EhrAllergySummary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AlertProfile")
@ExternalizedBean(type=DefaultExternalizer.class)
public class AlertProfile implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="PatAllergy")
	private List<PatAllergy> patAllergyList;
	
	@XmlElement(name="PatAdr")
	private List<PatAdr> patAdrList;
	
	@XmlElement(name="PatAlert")
	private List<PatAlert> patAlertList;
	
	@XmlElement(name="G6pdAlert")
	private PatAlert g6pdAlert;
	
	@XmlElement(required = true)
	private AlertProfileStatus status;
	
	@XmlElement
	private String message;
	
	@XmlElement
	private Date lastVerifyDate;

	@XmlElement
	private SteroidTag steroidTag;
	
	@XmlElement
	private EhrAllergySummary ehrAllergySummary;
	
	@XmlElement
	private boolean ehrAllergyOnly = false;
	
	public AlertProfile() {
	}
	
	public List<PatAllergy> getPatAllergyList() {
		if (patAllergyList == null) {
			patAllergyList = new ArrayList<PatAllergy>();
		}
		return patAllergyList;
	}

	public void setPatAllergyList(List<PatAllergy> patAllergyList) {
		this.patAllergyList = patAllergyList;
	}

	public List<PatAdr> getPatAdrList() {
		if (patAdrList == null) {
			patAdrList = new ArrayList<PatAdr>();
		}
		return patAdrList;
	}

	public void setPatAdrList(List<PatAdr> patAdrList) {
		this.patAdrList = patAdrList;
	}

	public List<PatAlert> getPatAlertList() {
		if (patAlertList == null) {
			patAlertList = new ArrayList<PatAlert>();
		}
		return patAlertList;
	}

	public void setPatAlertList(List<PatAlert> patAlertList) {
		this.patAlertList = patAlertList;
	}

	public PatAlert getG6pdAlert() {
		return g6pdAlert;
	}

	public void setG6pdAlert(PatAlert g6pdAlert) {
		this.g6pdAlert = g6pdAlert;
	}

	public AlertProfileStatus getStatus() {
		return status;
	}

	public void setStatus(AlertProfileStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getLastVerifyDate() {
		if (lastVerifyDate == null) {
			return null;
		}
		else {
			return new Date(lastVerifyDate.getTime());
		}
	}

	public void setLastVerifyDate(Date lastVerifyDate) {
		if (lastVerifyDate == null) {
			this.lastVerifyDate = null;
		}
		else {
			this.lastVerifyDate = new Date(lastVerifyDate.getTime());
		}
	}
	
	public SteroidTag getSteroidTag() {
		return steroidTag;
	}

	public void setSteroidTag(SteroidTag steroidTag) {
		this.steroidTag = steroidTag;
	}
	
	public EhrAllergySummary getEhrAllergySummary() {
		return ehrAllergySummary;
	}

	public void setAllergySummary(EhrAllergySummary ehrAllergySummary) {
		this.ehrAllergySummary = ehrAllergySummary;
	}

	public boolean isEhrAllergyOnly() {
		return ehrAllergyOnly;
	}

	public void setEhrAllergyOnly(boolean ehrAllergyOnly) {
		this.ehrAllergyOnly = ehrAllergyOnly;
	}

	public String getAllergyDesc() {
		StringBuilder sb = new StringBuilder();
		for (PatAllergy patAllergy : getPatAllergyList()) {
			if (patAllergy.getAllergenType() == AllergenType.NonDrug
			 || patAllergy.getAllergenType() == AllergenType.NoKnownDrugAllergy
			 || patAllergy.getAllergenType() == AllergenType.XP) {
				continue;
			}
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(patAllergy.getAllergenName());
		}
		return sb.toString();
	}
	
	@ExternalizedProperty
	public String getAdrDesc() {
		if (getPatAdrList().isEmpty()) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (PatAdr patAdr : getPatAdrList()) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(patAdr.getDrugName());
		}
		return sb.toString();
	} 
}
