<?xml version="1.0" encoding="utf-8"?>
<s:ButtonBar xmlns:fx="http://ns.adobe.com/mxml/2009" 
		 xmlns:s="library://ns.adobe.com/flex/spark" 
		 xmlns:mx="library://ns.adobe.com/flex/mx"
		 requireSelection="true"
		 itemRendererFunction="{buttonRendererFunction}">
	
	<fx:Metadata>
		[Name("pharmInboxButtonBar")]
	</fx:Metadata>
	
	<fx:Declarations>
		<s:ArrayCollection id="buttonFactoryList">
			<fx:Component>
				<s:ButtonBarButton styleName="normalButtonStyle"
								   click="outerDocument.dispatchEvent(new CategoryButtonClickEvent(MedProfileStatOrderType.Normal))">
					<fx:Script>
						<![CDATA[
							import hk.org.ha.event.pms.main.inbox.CategoryButtonClickEvent;
							import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
						]]>
					</fx:Script>
				</s:ButtonBarButton>
			</fx:Component>
			<fx:Component>
				<s:ButtonBarButton styleName="{outerDocument.shouldShowAlert(outerDocument.urgentAlertDate,
								              outerDocument.firstUrgentDueDate ,outerDocument.urgentCount) ?
								              'urgentButtonAlertStyle' : 'urgentButtonStyle'}"
								   click="outerDocument.dispatchEvent(new CategoryButtonClickEvent(MedProfileStatOrderType.Urgent))">
					<fx:Script>
						<![CDATA[
							import hk.org.ha.event.pms.main.inbox.CategoryButtonClickEvent;
							import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
						]]>
					</fx:Script>
				</s:ButtonBarButton>
			</fx:Component>
			<fx:Component>
				<s:ButtonBarButton styleName="{outerDocument.shouldShowAlert(outerDocument.pendingSuspendAlertDate,
								              outerDocument.firstPendingSuspendDueDate, outerDocument.pendingSuspendCount) ?
								              'pendingSuspendButtonAlertStyle' : 'pendingSuspendButtonStyle'}"
								   click="outerDocument.dispatchEvent(new CategoryButtonClickEvent(MedProfileStatOrderType.PendingSuspend))">
					<fx:Script>
						<![CDATA[
							import hk.org.ha.event.pms.main.inbox.CategoryButtonClickEvent;
							import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
						]]>
					</fx:Script>
				</s:ButtonBarButton>
			</fx:Component>
			<fx:Component>
				<s:ButtonBarButton styleName="filterButtonStyle"
								   click="outerDocument.dispatchEvent(new CategoryButtonClickEvent(MedProfileStatOrderType.All))">
					<fx:Script>
						<![CDATA[
							import hk.org.ha.event.pms.main.inbox.CategoryButtonClickEvent;
							import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
						]]>
					</fx:Script>
				</s:ButtonBarButton>
			</fx:Component>
		</s:ArrayCollection>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.utils.ObjectUtil;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.event.pms.main.inbox.ReceiveCategorySummaryEvent;
			import hk.org.ha.event.pms.main.inbox.ReceiveMedProfileStatListEvent;
			import hk.org.ha.event.pms.main.inbox.UpdateMedProfileStatTypesEvent;
			import hk.org.ha.event.pms.main.inbox.UpdateStatTypesAndFiltersEvent;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileStatAdminType;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileStatOrderType;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			
			import spark.events.IndexChangeEvent;

			[In] [Bindable]
			public var propMap:PropMap;
			
			[In] [Bindable]
			public var normalCount:Number;
			
			[In] [Bindable]
			public var urgentCount:Number;

			[In] [Bindable]
			public var pendingSuspendCount:Number;
			
			[In] [Bindable]
			public var pendingCount:Number;
			
			[In] [Bindable]
			public var suspendCount:Number;

			[In] [Bindable]
			public var overridePendingCount:Number;			
			
			[In] [Bindable]
			public var firstUrgentDueDate:Date;
			
			[In] [Bindable]
			public var firstPendingSuspendDueDate:Date;
			
			[Bindable]
			public var medProfileStatCount:Number;
			
			[Bindable]
			public var urgentAlertDate:Date;
			
			[Bindable]
			public var pendingSuspendAlertDate:Date;
						
			[Observer]
			public function refresh(evt:ReceiveCategorySummaryEvent):void {
				
				var newChildren:ArrayCollection = new ArrayCollection();
				newChildren.addAll(this.dataProvider);
				var currentIndex:Number = this.selectedIndex;
				this.dataProvider.removeAll();
				for each (var obj:Object in newChildren) 
					this.dataProvider.addItem(obj);
				this.selectedIndex = currentIndex;
			}
			
			public function buttonRendererFunction(data:Object):IFactory {
				var i:int = dataProvider.getItemIndex(data);
				return buttonFactoryList.getItemAt(i) as IFactory;
			}
			
			public function shouldShowAlert(alertDate:Date, dueDate:Date, count:Number):Boolean {
				return count > 0 && (dueDate == null || ObjectUtil.dateCompare(dueDate, alertDate) < 0);
			}
		]]>
	</fx:Script>
	
	<mx:ArrayCollection>
		<fx:String>Normal ({normalCount})</fx:String>
		<fx:String>Urgent ({urgentCount})</fx:String>
		<fx:String>Pending ({pendingCount}) [{overridePendingCount}] / Suspend ({suspendCount})</fx:String>
		<fx:String>Filter {selectedIndex != 3 ? '' : MedProfileUtils.formatBrackets(medProfileStatCount)}</fx:String>
	</mx:ArrayCollection>
	
</s:ButtonBar>
