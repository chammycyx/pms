<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="100%" height="100%" 
	xmlns:support="hk.org.ha.view.pms.main.support.*">
	
	<fx:Metadata>
		[Name("propMaintView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.support.RetrieveHospitalListEvent;
			import hk.org.ha.event.pms.main.support.RetrieveOperationProfileListEvent;
			import hk.org.ha.event.pms.main.support.RetrievePropListEvent;
			import hk.org.ha.event.pms.main.support.RetrieveWorkstoreListEvent;
			import hk.org.ha.event.pms.main.support.UpdatePropListEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.model.pms.persistence.corp.Hospital;
			import hk.org.ha.model.pms.persistence.corp.Workstore;
			import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
			import hk.org.ha.model.pms.udt.DataType;
			import hk.org.ha.model.pms.udt.YesNoFlag;
			import hk.org.ha.model.pms.udt.support.PropType;
			import hk.org.ha.model.pms.vo.support.PropInfo;
			import hk.org.ha.model.pms.vo.support.PropInfoItem;
			import hk.org.ha.model.pms.vo.support.PropMaintUtil;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.support.popup.PropMaintPopup;
			
			import mx.collections.ArrayCollection;
			import mx.events.DataGridEvent;
			import mx.events.IndexChangedEvent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			
			[Bindable]
			private var backgroundColor:uint = 0xE7E5E5; //DataGridColumn non-editable backgroundColor
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In]
			public var workstore:Workstore;
			
			[In][Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[In][Bindable]
			public var propInfoItemList:ArrayCollection;
			
			[In][Bindable]
			public var propInfoList:ArrayCollection;
			
			[Bindable]
			private var propMaintHospitalList:ArrayCollection;
			
			[Bindable]
			public var propMaintWorkstoreList:ArrayCollection;
			
			[Bindable]
			public var propMaintOperationProfileList:ArrayCollection;
			
			[Bindable]
			public var currentPropType:PropType;
			
			[Bindable]
			public var ruleValueUpdated:Boolean = false;
			
			[Bindable]
			public var popupFlag:Boolean = true;
			
			public var ruleValidFlag:Boolean = true;
			private var updatePropList:ArrayCollection;
			private var tapIndex:int = 0;
			private var propValueGridSelectedIndex:int = -1;
			private var propDescGridSelectedIndex:int = -1;
			private var initScreen:Boolean = true;
			private var shortcutFlag:Boolean = false;
			
			public override function onShowLater():void	{
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.saveYesFunc = saveYesHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					initScreen = true;
				}
				
				if ( initScreen ) {
					initScreen = false;
					retrieveHandler();
				}
				
				initSearchList();
				if ( propValueGrid != null ) {
					propValueGrid.enabled = true;
				}
				callLater(disableMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showPropMaintShortcutKeyHandler);
				this.setFocus();
			}
			
			private function initSearchList():void {
				if ( propMaintHospitalList == null ) {
					dispatchEvent(new RetrieveHospitalListEvent());
				}
				if ( propMaintWorkstoreList == null ) {
					dispatchEvent(new RetrieveWorkstoreListEvent());
				}
			}
			
			private function showPropMaintShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("PropMaintView"));
			}
			
			public override function onHide():void {
				if ( propValueGrid != null ) {
					propValueGrid.enabled = false;
				}
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			private function disableMenuBarCloseButton():void {
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
				
			}
			
			private function closeHandler():void {
				initScreen = true;
				tabBar.selectedIndex = 0;
				propDescPanel.visible = false;
				propDescPanel.includeInLayout = false;
				propValuePanel.visible = false;
				propValuePanel.includeInLayout = false;
				workstoreSearchGroup.visible = false;
				workstoreSearchGroup.includeInLayout = false;
				operationModeSearchGroup.visible = false;
				operationModeSearchGroup.includeInLayout = false;
				propValueGrid.forceEnableSelectionIndicator = false;				
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			private function retrieveHandler():void{
				clearFilterFunc();
				
				ruleValidFlag = true;
				ruleValueUpdated = false;
				propValueGrid.dataProvider = null;
				propDescGrid.dataProvider = null;
				
				switch (tabBar.selectedIndex) {
					case 0:
						currentPropType = PropType.CorporateProp;
						dispatchEvent(new RetrievePropListEvent(currentPropType, workstore.hospital) );
						break;
					case 1:
						currentPropType = PropType.HospitalProp;
						dispatchEvent(new RetrievePropListEvent(currentPropType, workstore.hospital) );
						break;
					case 2: 
						currentPropType = PropType.WorkstoreProp;
						var selectedHospital:Hospital = hospitalDropDown.selectedItem as Hospital;
						if ( selectedHospital == null ) {
							selectedHospital = workstore.hospital;
						}
						dispatchEvent(new RetrievePropListEvent(currentPropType, selectedHospital) );
						break;
					case 3:
						currentPropType = PropType.OperationProp;
						operationModeDropDown.errorString = "";
						var selectedWorkstore:Workstore = this.workstoreDropDown.selectedItem;
						var selecedProfile:OperationProfile = this.operationModeDropDown.selectedItem;
						if ( selecedProfile == null ) {
							operationModeDropDown.errorString = sysMsgMap.getMessage("0566");
							operationModeDropDown.setFocus();
						} else {
							dispatchEvent(new RetrievePropListEvent(currentPropType, selectedWorkstore.hospital, selecedProfile.id));
						}
						break;
				}
			}
			
			public function refreshPropDescGrid():void {
				switch ( tabBar.selectedIndex ) {
					case 0:
						propValueGrid.dataProvider = propInfoItemList;
						if ( propInfoItemList.length > 0 ) {
							propValueGrid.selectedIndex = 0;
						}
						initTabView(tabBar.selectedIndex);
						break;
					case 1:
					case 2:
						propDescGrid.dataProvider = propInfoList;
						if ( propInfoList != null && propInfoList.length > 0 ) {
							propDescGrid.selectedIndex = 0;
							propDescGridSelectedIndex = 0;
						}
						initTabView(tabBar.selectedIndex);
						break;
					case 3:
						propValueGrid.dataProvider = propInfoItemList;
						if ( propInfoItemList.length > 0 ) {
							propValueGrid.selectedIndex = 0;
						}
						break;
				}
				propValueGrid.enabled = true;
			}
			
			private function saveYesHandler():void 
			{
				if ( ruleValueUpdated ) {
					ruleValidFlag = isValidRuleValue(propValueGrid.selectedItem, propValueGrid.selectedItem.prop.type, propValueGrid.selectedItem.prop.validation);
				} else if ( !ruleValidFlag ) {
					setErrorPosition();
					return;
				}
				
				if ( ruleValidFlag ) {
					if ( currentPropType == PropType.CorporateProp || currentPropType == PropType.OperationProp) {
						convertPropInfoItemList();
					} else {
						convertPropInfoList();
					}

					propValueGrid.enabled = false;
					dispatchEvent(new UpdatePropListEvent(updatePropList));
				}
			}
			
			private function convertPropInfoItemList():void {
				updatePropList = new ArrayCollection();
				for each ( var propInfoItem:PropInfoItem in propInfoItemList ) {
					if ( propInfoItem.edited ) {
						updatePropList.addItem(propInfoItem);
					}
				}
			}
			
			private function convertPropInfoList():void {
				updatePropList = new ArrayCollection();
				for each ( var propInfo:PropInfo in propInfoList ) {
					for each (var propInfoItem:PropInfoItem in propInfo.propInfoItemList) {
						if ( propInfoItem.edited ) {
							updatePropList.addItem(propInfoItem);
						}
					}
				}
			}
			
			private function initTabView(selectedTab:Number):void
			{
				propDescGrid.dataProvider = null;

				switch (selectedTab)
				{
					case 0:
						propValuePanel.title = "Property List";
						propValueGridCodeCol.headerText = "Description";
						propValueGridCodeCol.dataField = "description";
						propValueGrid.dataProvider = propInfoItemList;
						propValueGrid.selectedIndex = 0;
						propValueGrid.rowColorFunction = gridRowColor;
						propDescPanel.visible = false;
						propDescPanel.includeInLayout = false;
						propValuePanel.visible = true;
						propValuePanel.includeInLayout = true;
						workstoreSearchGroup.visible = false;
						workstoreSearchGroup.includeInLayout = false;
						operationModeSearchGroup.visible = false;
						operationModeSearchGroup.includeInLayout = false;
						propValueGrid.forceEnableSelectionIndicator = false;
						propValueGridCodeCol.setStyle("backgroundColor", backgroundColor);
						
						break;
					case 1:
						propValueGrid.rowColorFunction = null;
						propDescGrid.dataProvider = propInfoList;
						if ( propInfoList != null && propInfoList.length > 0 ) {
							propDescGrid.selectedIndex = 0;
							propDescGridSelectedIndex = 0;
							propValueGrid.dataProvider = propDescGrid.selectedItem.propInfoItemList;
							if ( propDescGrid.selectedItem.supportMaintFlag ) {
								propValueGridCodeCol.setStyle("backgroundColor", backgroundColor);
								propValueGrid.forceEnableSelectionIndicator = false;
							} else {
								propValueGridCodeCol.clearStyle("backgroundColor");
								propValueGrid.forceEnableSelectionIndicator = true;
							}
						}
						propValuePanel.title = "Hospital List";
						propValueGridCodeCol.headerText = "Hospital Code";
						propValueGridCodeCol.dataField = "hospCode";
						propDescPanel.visible = true;
						propDescPanel.includeInLayout = true;
						propValuePanel.visible = true;
						propValuePanel.includeInLayout = true;
						workstoreSearchGroup.visible = false;
						workstoreSearchGroup.includeInLayout = false;
						operationModeSearchGroup.visible = false;
						operationModeSearchGroup.includeInLayout = false;
						
						
						break;
					case 2:
						propValueGrid.rowColorFunction = null;
						propValuePanel.title = "Working Store List";
						propValueGridCodeCol.headerText = "Working Store";
						propValueGridCodeCol.dataField = "workstoreCode";
						propDescGrid.dataProvider = propInfoList;
						if ( propInfoList != null && propInfoList.length > 0 ) {
							propDescGrid.selectedIndex = 0;
							propDescGridSelectedIndex = 0;
							propValueGrid.dataProvider = propDescGrid.selectedItem.propInfoItemList;
							if ( propDescGrid.selectedItem.supportMaintFlag ) {
								propValueGridCodeCol.setStyle("backgroundColor", backgroundColor);
								propValueGrid.forceEnableSelectionIndicator = false;
							} else {
								propValueGridCodeCol.clearStyle("backgroundColor");
								propValueGrid.forceEnableSelectionIndicator = true;
							}
						}
						propDescPanel.visible = true;
						propDescPanel.includeInLayout = true;
						propValuePanel.visible = true;
						propValuePanel.includeInLayout = true;
						workstoreSearchGroup.visible = true;
						workstoreSearchGroup.includeInLayout = true;
						operationModeSearchGroup.visible = false;
						operationModeSearchGroup.includeInLayout = false;
						
						
						break;
					case 3:
						propValuePanel.title = "Property List";
						propValueGridCodeCol.headerText = "Description";
						propValueGridCodeCol.dataField = "description";
						propValueGrid.dataProvider = null;
						propValueGrid.rowColorFunction = gridRowColor;
						propDescPanel.visible = false;
						propDescPanel.includeInLayout = false;
						propValuePanel.visible = true;
						propValuePanel.includeInLayout = true;
						workstoreSearchGroup.visible = false;
						workstoreSearchGroup.includeInLayout = false;
						operationModeSearchGroup.visible = true;
						operationModeSearchGroup.includeInLayout = true;
						propValueGrid.forceEnableSelectionIndicator = false;
						propValueGridCodeCol.setStyle("backgroundColor", backgroundColor);
						setWorkstoreListSelectedItem();
						
						break;
				}
			}
			
			public function setHospitalListSelectedItem():void{
				hospitalDropDown.selectedIndex = -1;
				for each ( var hospital:Hospital in propMaintHospitalList) {
					if ( hospital.hospCode == workstore.hospCode ) {
						hospitalDropDown.selectedItem = hospital;
						break;
					}
				}
			}
			
			private function setWorkstoreListSelectedItem():void {
				this.workstoreDropDown.selectedIndex = -1;
				for each ( var wks:Workstore in propMaintWorkstoreList ) {
					if ( wks.hospCode == workstore.hospCode && wks.workstoreCode == workstore.workstoreCode ) {
						workstoreDropDown.selectedItem = wks;
						callLater(workstoreDropDownChangeHandler);
						break;
					}
				}
			}
			
			public function setHospitalList(list:ArrayCollection):void {
				propMaintHospitalList = list;
			}
			
			public function setWorkstoreList(list:ArrayCollection):void {
				propMaintWorkstoreList = list;
			}
			
			public function setOperationProfileList(list:ArrayCollection):void {
				propMaintOperationProfileList = list;
			}
			
			private function hospitalLabelFunction(hospital:Hospital):String {
				return hospital.hospCode;
			}
			
			private function workstoreLabelFunction(workstore:Workstore):String {
				return workstore.hospCode +" ("+workstore.workstoreCode+")";
			}
			
			private function operationNameLabelFunction(operationProfile:OperationProfile):String {
				return operationProfile.name;
			}
			
			private function propDescGridItemChangeHandler(evt:ListEvent):void
			{	
				if ( !ruleValidFlag ) {
					setErrorPosition();
				} else {
					propDescGridSelectedIndex = propDescGrid.selectedIndex;
					if ( this.currentPropType == PropType.WorkstoreProp || this.currentPropType == PropType.HospitalProp ) {

						if (propDescGrid.selectedItem != null) {
							propValueGrid.dataProvider = propDescGrid.selectedItem.propInfoItemList;
							
							if ( propDescGrid.selectedItem.supportMaintFlag ) {
								propValueGridCodeCol.setStyle("backgroundColor", backgroundColor);
								propValueGrid.forceEnableSelectionIndicator = false;
							} else {
								propValueGridCodeCol.clearStyle("backgroundColor");
								propValueGrid.forceEnableSelectionIndicator = true;
							}
						}else {
							propValueGrid.dataProvider = null;
						}
					} 
				}
			}
			
			private function propValueGridItemEditBeginningHandler(evt:DataGridEvent):void
			{	
				if ( shortcutFlag ) {
					evt.preventDefault();
					shortcutFlag = false;
					return;
				}
				if(evt.rowIndex >= 0){
					var data:Object = evt.currentTarget.dataProvider.getItemAt(evt.rowIndex);
					if ( !this.ruleValidFlag && ( data.errorString == null || data.errorString == '') ) {
						evt.preventDefault();
						setErrorPosition();
					} else if( evt.dataField == "prop.description" || evt.dataField == "description" || evt.dataField == "hospCode" || evt.dataField == "workstoreCode" ){
						evt.preventDefault();
					}  else  if ( ((tabBar.selectedIndex == 0 || tabBar.selectedIndex == 3 ) && data != null && data.supportMaintFlag == false) || 
							(tabBar.selectedIndex != 0 && propDescGrid.selectedItem != null && propDescGrid.selectedItem.supportMaintFlag == false )) {
						evt.preventDefault();
					}
					else{
						var type:DataType = getType();
						if ( evt.dataField == "displayValue" && 
							(type == DataType.ListIntegerType || type == DataType.ListStringType) && 
							popupFlag) {
							evt.preventDefault();
							showCheckBoxPopup(getValidation(), data.displayValue);
							popupFlag = false;
						}
					} 
				}
			}
			
			public function getType():DataType {
					return propValueGrid.selectedItem.prop.type;
			}
			
			public function getValidation():String {
					return propValueGrid.selectedItem.prop.validation;
			}
			
			private function setErrorPosition():void {
				tabBar.selectedIndex = tapIndex;
				propDescGrid.selectedIndex = propDescGridSelectedIndex;
				propValueGrid.selectedIndex = propValueGridSelectedIndex;
				propValueGrid.editedItemPosition = {rowIndex:propValueGridSelectedIndex,columnIndex:1}; 
			}
				
			private function destroyPropValueDataGridEditor(evt:DataGridEvent):void 
			{
				propValueGridSelectedIndex = propValueGrid.selectedIndex;				
				switch (evt.dataField) {
					case "displayValue":
						if ( ruleValueUpdated ) {
							ruleValidFlag = isValidRuleValue(propValueGrid.selectedItem, propValueGrid.selectedItem.prop.type, propValueGrid.selectedItem.prop.validation);
							if ( ruleValidFlag ) {
								propValueGrid.destroyItemEditor();	
							} else {
								evt.preventDefault();
							}
						} else {
							propValueGrid.destroyItemEditor();	
						}
						break;
				}
				this.refreshDashboard();				
			}
			
			private function isValidRuleValue(selectedItem:Object, dataType:DataType, validation:String):Boolean {
				ruleValueUpdated = false;
				
				if ( dataType == DataType.BooleanType ) {
					selectedItem.value = (selectedItem.displayValue==YesNoFlag.Yes.displayValue)?YesNoFlag.Yes.dataValue:YesNoFlag.No.dataValue;
				} else {
					selectedItem.value = selectedItem.displayValue;
				}
				
				var result:Object = PropMaintUtil.validateRuleValue(selectedItem.value, dataType, validation, selectedItem.errorString, true);
				if ( result.errorCode != "" ) {
					if ( result.errorMsgParm != null ) {
						selectedItem.errorString = sysMsgMap.getMessage(result.errorCode, result.errorMsgParm);
					} else {
						selectedItem.errorString = sysMsgMap.getMessage(result.errorCode);
					}
				} else {
					selectedItem.errorString = "";
				}
				propValueGrid.selectedItem = selectedItem;
				return result.valid;
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if (evt.ctrlKey) {
					if ( evt.charCode == 55 && pmsToolbar.saveButton.enabled){
						shortcutFlag = true;
						propValueGrid.destroyItemEditor();
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.saveButton));
					}
					else if ( evt.charCode == 48 && pmsToolbar.closeButton.enabled){	//ctrl + 0 - close
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
				}
			}
			
			public function showCheckBoxPopup(validation:String, propValue:String):void {
				var propMaintPopup:PropMaintPopup = PropMaintPopup(PopUpManager.createPopUp(this, PropMaintPopup, true));
				propMaintPopup.propValue = propValue;
				propMaintPopup.validation = validation;
				propMaintPopup.initPopup();
				propMaintPopup.popupOkHandler = function():void {
					(propValueGrid.selectedItem as PropInfoItem).value = propMaintPopup.propValue;
					(propValueGrid.selectedItem as PropInfoItem).displayValue = propMaintPopup.propValue;
					(propValueGrid.selectedItem as PropInfoItem).edited = true;
					propMaintPopup.removePopup();
					popupFlag = true;
				};
				propMaintPopup.popupCancelHandler = function():void {
					propMaintPopup.removePopup();
					popupFlag = true;
				}
				PopUpManager.centerPopUp(propMaintPopup);
			}
			
			private function gridRowColor(item:Object, rowIndex:int, dataIndex:int, color:uint):uint 
			{
				if (item.supportMaintFlag ) 
				{
					return color;
				}
				else 
				{
					return backgroundColor;
				}
			}

			protected function propValueGridKeyFocusChangeHandler(evt:FocusEvent):void
			{
				if ( propValueGrid.editedItemPosition == null || propValueGrid.editedItemPosition.columnIndex != 1 ) {
					return;
				}
				
				if ( evt.shiftKey ) {
					evt.preventDefault();
					if ( ruleValueUpdated ) {
						this.ruleValidFlag = this.isValidRuleValue(propValueGrid.selectedItem, propValueGrid.selectedItem.prop.type, propValueGrid.selectedItem.prop.validation);
					}
					if ( ruleValidFlag ) {
						if ( propValueGrid.selectedIndex > 0 ) {
							var nextShiftRow:Number = getNextRowDesc(propValueGrid.selectedIndex);
							propValueGrid.selectedIndex = nextShiftRow;
							propValueGrid.editedItemPosition = {columnIndex:1, rowIndex:nextShiftRow};
						}
					}
				} else {
					evt.preventDefault();
					if ( ruleValueUpdated ) {
						this.ruleValidFlag = this.isValidRuleValue(propValueGrid.selectedItem, propValueGrid.selectedItem.prop.type, propValueGrid.selectedItem.prop.validation);
					}
					if ( ruleValidFlag ) {
						if ( propValueGrid.selectedIndex < propValueGrid.dataProvider.length ) {
							var nextRow:Number = getNextRowAsc(propValueGrid.selectedIndex);
							propValueGrid.selectedIndex = nextRow;
							propValueGrid.editedItemPosition = {columnIndex:1, rowIndex:nextRow};
						}
					}
				}
			}
			
			private function getNextRowDesc(currentRow:Number):Number {
				for (var row:Number = currentRow-1; row >=0; row-- ) {
					var item:Object = propValueGrid.dataProvider.getItemAt(row);
					if ( item.supportMaintFlag ) {
						return row;
					}
				}
				return currentRow;
			}
			
			private function getNextRowAsc(currentRow:Number):Number {
				for (var row:Number = currentRow+1; row < propValueGrid.dataProvider.length; row++ ) {
					var item:Object = propValueGrid.dataProvider.getItemAt(row);
					if ( item.supportMaintFlag ) {
						return row;
					}
				}
				return currentRow;
			}

			protected function workstoreDropDownChangeHandler():void
			{
				filterDesc.text = "";
				propValueGrid.dataProvider = null;
				dispatchEvent(new RetrieveOperationProfileListEvent(workstoreDropDown.selectedItem.hospCode, workstoreDropDown.selectedItem.workstoreCode));
			}

			protected function tabBarChangeHandler(evt:IndexChangedEvent):void
			{
				tapIndex = evt.newIndex;
				switch ( evt.newIndex ) {
					case 0:
						retrieveHandler();
						break;
					case 1:
						retrieveHandler();
						break;
					case 2:
						setHospitalListSelectedItem();
						retrieveHandler();
						break;
					case 3:
						initTabView(3);
						break;
				}
			}
			
			protected function filterListHandler():void
			{
				setFilterFunc();
			}
			
			private function setFilterFunc():void 
			{
				switch ( tabBar.selectedIndex ) {
					case 0:
					case 3:
						if(propInfoItemList == null) {
							return;
						}
						propInfoItemList.filterFunction = propInfoItemListFilterAllFunc;
						propInfoItemList.refresh();
						
						var newArrayList:ArrayCollection = new ArrayCollection();
						newArrayList.addAll(propInfoItemList);
						propDescGrid.dataProvider = newArrayList;
						break;
					case 1:
					case 2:
						if(propInfoList == null){
							return;
						}
						propInfoList.filterFunction = propInfoListFilterAllFunc;
						propInfoList.refresh();
						
						var newArrayList:ArrayCollection = new ArrayCollection();
						newArrayList.addAll(propInfoList);
						propDescGrid.dataProvider = newArrayList;

						callLater(function():void{
							if (propInfoList!=null && propInfoList.length>0) {
								propDescGrid.selectedIndex = 0;
							}
							propDescGrid.dispatchEvent(new ListEvent(ListEvent.CHANGE));
						});
						break;
				}
			}
			
			public function propInfoItemListFilterAllFunc(propInfoItem:PropInfoItem):Boolean
			{
				var fullPropDescFilterFlag:Boolean = true;
				
				if ( filterDesc.text != "" ) {
					if (propInfoItem.description.toLocaleLowerCase().match(filterDesc.text.toLowerCase())) {
						fullPropDescFilterFlag = true;
					}else {
						fullPropDescFilterFlag = false;
					}
				}
				return fullPropDescFilterFlag;
			}
			
			private function clearFilterFunc():void 
			{
				filterDesc.text = "";
				
				switch ( tabBar.selectedIndex ) {
					case 0:
					case 3:
						if(propInfoItemList == null)
						{
							return;
						}
						propInfoItemList.filterFunction = null;
						propInfoItemList.refresh();
						propDescGrid.dataProvider = propInfoItemList;
						break;
					case 1:
					case 2:
						if(propInfoList == null)
						{
							return;
						}
						propInfoList.filterFunction = null;
						propInfoList.refresh();
						propDescGrid.dataProvider = propInfoList;
						
						callLater(function():void{
							if (propInfoList!=null && propInfoList.length>0) {
								propDescGrid.selectedIndex = 0;
								propDescGrid.dispatchEvent(new ListEvent(ListEvent.CHANGE));
							}
						});
						break;
				}
			}
			
			public function propInfoListFilterAllFunc(propInfo:PropInfo):Boolean
			{
				var fullPropDescFilterFlag:Boolean = true;

				if ( filterDesc.text != "" ) {
					if (propInfo.description.toLocaleLowerCase().match(filterDesc.text.toLowerCase())) {
						fullPropDescFilterFlag = true;
					}else {
						fullPropDescFilterFlag = false;
					}
				}
				return fullPropDescFilterFlag;
			}

		]]>
		
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	<fc:Toolbar id="pmsToolbar"	width="100%"/>
	<s:VGroup width="100%" height="100%" gap="0" paddingBottom="10" paddingLeft="10" paddingRight="10" paddingTop="10" >
		<mx:TabNavigator id="tabBar" paddingTop="0" paddingBottom="0" paddingLeft="0" 
						 	paddingRight="0" width="100%" height="22" change="tabBarChangeHandler(event)">
			<s:NavigatorContent label="Cluster"/>
			<s:NavigatorContent label="Hospital"/>
			<s:NavigatorContent label="Working Store"/>
			<s:NavigatorContent label="Operation Mode"/>
		</mx:TabNavigator>
		
		<s:BorderContainer width="100%" height="100%">
			<s:layout>
				<s:VerticalLayout paddingTop="10" paddingBottom="10" 
								  paddingLeft="10" paddingRight="10" 				  
								  gap="10"/>
			</s:layout>
			<s:HGroup>
				<s:VGroup>
					<s:HGroup id="workstoreSearchGroup" verticalAlign="middle" horizontalAlign="left" width="100%" height="30" visible="false" includeInLayout="false">
						<s:Label text="Hospital"/>
						<fc:ExtendedDropDownList id="hospitalDropDown" dataProvider="{propMaintHospitalList}"
												 width="70"
												 labelField="hospCode" labelFunction="hospitalLabelFunction" 
												 change="retrieveHandler()"/>
					</s:HGroup>
					<s:HGroup id="operationModeSearchGroup" verticalAlign="middle" horizontalAlign="left" width="100%" height="30" visible="false" includeInLayout="false">
						<s:Label text="Hospital (Working Store)"/>
						<fc:ExtendedDropDownList id="workstoreDropDown" dataProvider="{propMaintWorkstoreList}" 
												 width="112"
												 labelField="hospCode" labelFunction="workstoreLabelFunction" 
												 change="workstoreDropDownChangeHandler()"/>
						<s:Label text="Operation Mode"/>
						<fc:ExtendedDropDownList id="operationModeDropDown"
												 width="112"
												 dataProvider="{propMaintOperationProfileList}" 
												 labelField="name"
												 labelFunction="operationNameLabelFunction"
												 change="retrieveHandler()"/>
					</s:HGroup>
				</s:VGroup>	
				<s:HGroup id="filterSearchGroup" verticalAlign="middle" horizontalAlign="left" width="100%" height="30" visible="true" includeInLayout="true">
					<s:Label text="Property Description"/>
					<s:TextInput id="filterDesc"/>
					<fc:ExtendedButton id="filterBtn" label="Filter" click="filterListHandler()"/>
					<fc:ExtendedButton id="resetBtn" label="Reset" click="clearFilterFunc()"/>
				</s:HGroup>
			</s:HGroup>
			<s:HGroup width="100%" height="100%" >
				<s:Panel id="propDescPanel" title="Property List" width="100%" height="100%" visible="false" includeInLayout="false">
					<fc:ExtendedDataGrid id="propDescGrid" width="100%" height="100%" change="propDescGridItemChangeHandler(event)" 
										 resizableColumns="false" sortableColumns="false">
						<fc:columns>
							<mx:DataGridColumn id="descriptionCol" headerText="Description" dataField="description" >
								<mx:itemRenderer>
									<fx:Component>
										<s:MXDataGridItemRenderer autoDrawBackground="false">
											<s:layout>
												<s:HorizontalLayout horizontalAlign="center" verticalAlign="middle"/>
											</s:layout>
											<s:Label paddingLeft="5" text="{dataGridListData.label}" showTruncationTip="true" maxDisplayedLines="1" width="{outerDocument.descriptionCol.width-6}"/>
										</s:MXDataGridItemRenderer>
									</fx:Component>
								</mx:itemRenderer>
							</mx:DataGridColumn>
						</fc:columns>
					</fc:ExtendedDataGrid>
				</s:Panel>
				
				<s:Panel id="propValuePanel" width="100%" height="100%" visible="false" includeInLayout="false">
					<s:layout>
						<s:VerticalLayout paddingBottom="0" paddingLeft="0" paddingRight="0" paddingTop="0" gap="0"/>
					</s:layout>
					<fc:ExtendedDataGrid id="propValueGrid" 
										 resizableColumns="false" 
										 sortableColumns="false"
										 itemEditBeginning="propValueGridItemEditBeginningHandler(event)" 
										 itemEditEnd="destroyPropValueDataGridEditor(event)"
										 editable="true" width="100%" height="100%"
										 keyFocusChange="propValueGridKeyFocusChangeHandler(event)"
										 variableRowHeight="true" >
						<fc:columns>
							<mx:DataGridColumn id="propValueGridCodeCol" backgroundColor="{backgroundColor}">
								<mx:itemRenderer>
									<fx:Component>
										<s:MXDataGridItemRenderer autoDrawBackground="false">
											<s:layout>
												<s:HorizontalLayout horizontalAlign="center" verticalAlign="top" paddingTop="5"/>
											</s:layout>
											<s:Label paddingLeft="5" 
													 text="{dataGridListData.label}" 
													 showTruncationTip="true" 
													 maxDisplayedLines="1" 
													 width="{outerDocument.propValueGridCodeCol.width-6}"/>
										</s:MXDataGridItemRenderer>
									</fx:Component>
								</mx:itemRenderer>
							</mx:DataGridColumn>
							<mx:DataGridColumn headerText="Property Value" dataField="displayValue" editorUsesEnterKey="true">
								<mx:itemRenderer>
									<fx:Component>
										<support:PropValueItemRenderer currentMaint="supportMaint" 
																	   popupFlag="@{outerDocument.popupFlag}"
																	   showCheckBoxPopup="{outerDocument.showCheckBoxPopup}"/>
									</fx:Component>
								</mx:itemRenderer>
								<mx:itemEditor>
									<fx:Component>
										<support:PropValueItemEditRenderer currentScreen="propMaint" 
																		   sysMsgMap="{outerDocument.sysMsgMap}" 
																		   rulValueChange="@{outerDocument.ruleValueUpdated}"
																		   popupFlag="@{outerDocument.popupFlag}"
																		   showCheckBoxPopup="{outerDocument.showCheckBoxPopup}"/>
									</fx:Component>
								</mx:itemEditor>
							</mx:DataGridColumn>
						</fc:columns>
					</fc:ExtendedDataGrid>
				</s:Panel>
			</s:HGroup>
			
		</s:BorderContainer>
	</s:VGroup>
	
	
</fc:ExtendedNavigatorContent>