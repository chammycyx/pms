package hk.org.ha.model.pms.udt.reftable.mp;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum DosageConversionType implements StringValuedEnum{

	Fixed("Y", "Fixed"),
	Variable("N", "Variable");

	private final String dataValue;
	private final String displayValue;
	
	DosageConversionType(final String dataValue, final String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	public String getDataValue() {
		return this.dataValue;
	}

	public String getDisplayValue() {
		return this.displayValue;
	}
	
	public static DosageConversionType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DosageConversionType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DosageConversionType> {

		private static final long serialVersionUID = 1L;

		public Class<DosageConversionType> getEnumClass() {
			return DosageConversionType.class;
		}

	}
}
