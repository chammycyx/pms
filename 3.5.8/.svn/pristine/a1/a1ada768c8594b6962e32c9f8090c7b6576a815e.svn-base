<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="1010" height="600">

	<fx:Metadata>
		[Name("batchPrepFormulaSelectionPopup")]
	</fx:Metadata>
	
	<fx:Declarations>
		<s:ArrayCollection id="pivasNormalFormulaListView" list="{pivasFormulaList}" filterFunction="{normalFormulaListFilterFunction}"/>
		<s:ArrayCollection id="pivasMergeFormulaListView" list="{pivasFormulaList}" filterFunction="{mergeFormulaListFilterFunction}">
			<s:sort>
				<s:Sort compareFunction="{mergeFormulaListComparator.compareFunction}"/>
			</s:sort>			
		</s:ArrayCollection>
		<mp:AdvancedComparator id="mergeFormulaListComparator">
			<s:ArrayCollection>
				<mx:SortField name="itemCode"/>
				<mx:SortField name="printName"/>
				<mx:SortField name="dmSite.ipmoeDesc"/>
			</s:ArrayCollection>
		</mp:AdvancedComparator>		
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.controls.DataGrid;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.IndexChangedEvent;
			import mx.managers.PopUpManager;
			
			import hk.org.ha.event.pms.main.pivas.CreatePivasBatchPrepEvent;
			import hk.org.ha.event.pms.main.pivas.RetrieveBatchPrepMethodInfoListEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaByIdEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaListByDrugKeyEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaListByPrefixDisplayNameEvent;
			import hk.org.ha.event.pms.main.pivas.RetrievePivasFormulaMethodListByFormulaIdEvent;
			import hk.org.ha.event.pms.main.reftable.pivas.RetrieveDmDrugLiteMapEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
			import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaType;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileMoItem;
			import hk.org.ha.model.pms.udt.rx.RxItemType;
			import hk.org.ha.model.pms.vo.drug.DmDrugLite;
			import hk.org.ha.model.pms.vo.pivas.BatchPrepMethodInfo;
			import hk.org.ha.model.pms.vo.rx.Dose;
			import hk.org.ha.model.pms.vo.rx.IvFluid;
			import hk.org.ha.model.pms.vo.rx.IvRxDrug;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			import hk.org.ha.view.pms.main.pivas.PivasWorklistUiInfo;
			
			public const CORE_COLUMN_WIDTH:int = 40;
			
			public const LOCAL_SUSPEND_COLUMN_WIDTH:int = 20;
			
			public const ITEM_CODE_COLUMN_WIDTH:int = 80;
			
			public const MANUF_COLUMN_WIDTH:int = 60;
			
			public const ITEM_DESC_COLUMN_WIDTH:int = 420;
			
			public const REQUIRED_VOLUME_COLUMN_WIDTH:int = 100;
			
			public const DILUENT_COLUMN_WIDTH:int = 100;
			
			public const SHELF_LIFE_COLUMN_WIDTH:int = 80;
			
			public const STATUS_COLUMN_WIDTH:int = 80;

			[In][Bindable]
			public var sysMsgMap:SysMsgMap;
			
			private var _okHandler:Function;

			private var _cancelHandler:Function;
			
			[Bindable]
			private var pivasFormulaList:ArrayCollection;
			
			[Bindable]
			private var pivasFormulaMethodList:ArrayCollection;
			
			[Bindable]
			private var pivasMergeFormulaMethodList:ArrayCollection;
			
			[Bindable]
			public var pivasMergeFormulaDmDrugLiteMap:Object;
			
			[Bindable]
			private var okBtnEnable:Boolean = false;
			
			private var pivasFormulaId:Number = NaN;
			
			private var pivasFormulaMethodId:Number = NaN;
			
			public function initPopupFromBatchPrep(displayName:String):void {
				popupPanel.title = "Add Batch Preparation";
				
				if ( displayName.length > 0 ) {
					displayNameTxt.text = displayName;
					searchbuttonClickHandler();
				} else {
					displayNameTxt.setFocus();
					focusManager.showFocus();
				}
			}
			
			public function initPopupFromPrepDetail(_pivasFormulaId:Number, _pivasFormulaMethodId:Number, _medProfile:MedProfile, _medProfileMoItem:MedProfileMoItem, _refillFlag:Boolean, okHandler:Function, cancelHandler:Function):void {
				popupPanel.title = "Formula Lookup";
				
				formulaSearchGroup.visible = false;
				formulaSearchGroup.includeInLayout = false;
				
				mergeFormulaTab.enabled = false;
				
				pivasFormulaId = _pivasFormulaId;
				pivasFormulaMethodId = _pivasFormulaMethodId;
				_okHandler = okHandler;
				_cancelHandler = cancelHandler;
				
				if (isNaN(_medProfileMoItem.pivasFormulaId) && ! _refillFlag) {
					// IPMOE order without formula
					var retrievePivasFormulaListByDrugKeyEvent:RetrievePivasFormulaListByDrugKeyEvent = new RetrievePivasFormulaListByDrugKeyEvent();
					retrievePivasFormulaListByDrugKeyEvent.patHospCode = _medProfile.patHospCode;
					retrievePivasFormulaListByDrugKeyEvent.pasWardCode = _medProfile.medCase.pasWardCode;
					retrievePivasFormulaListByDrugKeyEvent.drugKey = PivasWorklistUiInfo.extractOrderDrugKey(_medProfileMoItem);
					retrievePivasFormulaListByDrugKeyEvent.dosageUnit = PivasWorklistUiInfo.extractOrderDosageUnit(_medProfileMoItem); 
					retrievePivasFormulaListByDrugKeyEvent.siteCode = PivasWorklistUiInfo.extractOrderSiteCode(_medProfileMoItem);
					retrievePivasFormulaListByDrugKeyEvent.callbackFunc = setFormulaList;
					
					if (_medProfileMoItem.rxItemType == RxItemType.Iv) {
						var ivRxDrug:IvRxDrug = _medProfileMoItem.rxItem as IvRxDrug;
						if (ivRxDrug.ivFluidList != null && ivRxDrug.ivFluidList.length > 0) {
							var firstIvFluid:IvFluid = ivRxDrug.ivFluidList.getItemAt(0) as IvFluid;
							retrievePivasFormulaListByDrugKeyEvent.diluentCode = firstIvFluid.diluentCode;
						}
					} else {
						var firstDose:Dose = PivasWorklistUiInfo.extractOrderFirstDose(_medProfileMoItem);
						if (firstDose.doseFluid != null) {
							retrievePivasFormulaListByDrugKeyEvent.diluentCode = firstDose.doseFluid.diluentCode;
						}
					}
					
					retrievePivasFormulaListByDrugKeyEvent.strength = PivasWorklistUiInfo.extractOrderStrength(_medProfileMoItem);
					
					dispatchEvent(retrievePivasFormulaListByDrugKeyEvent);
					
				} else {
					dispatchEvent(new RetrievePivasFormulaByIdEvent(_pivasFormulaId, updatePivasFormulaForSavedFormula));
				}
			}
			
			private function cancelHandler(evt:MouseEvent):void {
				if (_cancelHandler != null) {
					_cancelHandler();
				}
				
				PopUpManager.removePopUp(this);
			}
			
			private function okHandler(evt:MouseEvent):void {
				
				var pivasFormula:PivasFormula = null;
				var pivasFormulaMethodId:Number = NaN;
				
				switch (tabNavigator.selectedChild) {
					case formulaTab:
						pivasFormula = formulaGrid.selectedItem as PivasFormula;
						pivasFormulaMethodId = preparationMethodGrid.selectedItem == null ? NaN : preparationMethodGrid.selectedItem.pivasFormulaMethodId;
						break;
					case mergeFormulaTab:
						pivasFormula = mergeFormulaGrid.selectedItem as PivasFormula;
						pivasFormulaMethodId = mergePrepMethodList.selectedItem == null ? NaN : mergePrepMethodList.selectedItem.id;
						break;
				}
				
				if (pivasFormula == null || isNaN(pivasFormulaMethodId)) {
					return;
				}
				
				if (_okHandler != null) {
					_okHandler(pivasFormula, pivasFormulaMethodId);
				} else {
					dispatchEvent(new CreatePivasBatchPrepEvent(pivasFormulaMethodId));
				}
				PopUpManager.removePopUp(this);
			}
			
			public function showSystemMessage(errorCode:String, oKfunc:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if (oKfunc != null)
				{
					msgProp.okHandler = oKfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}

			protected function searchbuttonClickHandler():void
			{
				if ( displayNameTxt.text.length == 0 ) {
					//Please input display name.
					showSystemMessage("0878", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							displayNameTxt.setFocus();
						}
					);
					return;
				}
				dispatchEvent(new RetrievePivasFormulaListByPrefixDisplayNameEvent(displayNameTxt.text, true));
			}
			
			public function setFormulaList(inPivasFormulaList:ArrayCollection):void {

				this.pivasFormulaList = inPivasFormulaList;
				
				if (pivasFormulaMethodList != null) {
					pivasFormulaMethodList.removeAll();
				}
				if (pivasMergeFormulaMethodList != null) {
					pivasMergeFormulaMethodList.removeAll();
				}
				
				if ( pivasFormulaList.length == 0 ) {
					showSystemMessage("0005");
				} else {
					var targetPivasFormula:PivasFormula = null;
					if ( ! isNaN(pivasFormulaId)) {
						for each (var pivasFormula:PivasFormula in pivasFormulaList) {
							if (pivasFormula.id == pivasFormulaId) {
								targetPivasFormula = pivasFormula;
								break;
							}
						}
					}
					
					if (targetPivasFormula != null) {
						formulaGrid.selectedItem = targetPivasFormula;
					} else {
						formulaGrid.selectedIndex = 0;
					}
					
					formulaGrid_changeHandler();
					
					if (mergeFormulaGrid != null) {
						retrievePivasMergeFormulaDmDrugLiteMap();
						mergeFormulaGrid.selectedIndex = 0;
						mergeFormulaGrid_changeHandler();
						
					}
					if ( pivasNormalFormulaListView.length == 0 && pivasMergeFormulaListView.length > 0 ) {
						tabNavigator.selectedChild = mergeFormulaTab;
					} else {
						tabNavigator.selectedChild = formulaTab;
					}
				}
			}
			
			public function setFormulaMethodList(inPivasFormulaMethodList:ArrayCollection):void {
				pivasFormulaMethodList = inPivasFormulaMethodList;
				
				var targetBatchPrepMethodInfo:BatchPrepMethodInfo = null;
				if ( ! isNaN(pivasFormulaMethodId)) {
					for each (var batchPrepMethodInfo:BatchPrepMethodInfo in pivasFormulaMethodList) {
						if (batchPrepMethodInfo.pivasFormulaMethodId == pivasFormulaMethodId) {
							targetBatchPrepMethodInfo = batchPrepMethodInfo;
							break;
						}
					}
				}

				if (targetBatchPrepMethodInfo != null) {
					preparationMethodGrid.selectedItem = targetBatchPrepMethodInfo;
				} else {
					preparationMethodGrid.selectedIndex = 0;
				}
			}
			
			public function setMergeFormulaMethodList(pivasMergeFormulaMethodList:ArrayCollection):void {
				this.pivasMergeFormulaMethodList = pivasMergeFormulaMethodList;
				mergePrepMethodList.selectedIndex = 0;
			}
			
			private function setPivasMergeFormulaDmDrugLiteMap(pivasMergeFormulaDmDrugLiteMap:Object):void
			{
				if (pivasMergeFormulaDmDrugLiteMap != null) {
					this.pivasMergeFormulaDmDrugLiteMap = pivasMergeFormulaDmDrugLiteMap;
					
					for each (var pivasFormulaMethod:PivasFormulaMethod in pivasMergeFormulaMethodList) {
						pivasFormulaMethod.pivasFormulaMethodDrugList.refresh();
					}
				}
			}
			
			public function updatePivasFormulaForSavedFormula(pivasFormula:PivasFormula):void {
				var tempPivasFormulaList:ArrayCollection = new ArrayCollection();
				tempPivasFormulaList.addItem(pivasFormula);
				
				setFormulaList(tempPivasFormulaList);
			}
			
			private function concnLabelFunc(pivasFormula:PivasFormula, column:DataGridColumn):String {
				return concnDataTipFunc(pivasFormula);
			}
			
			private function concnDataTipFunc(pivasFormula:PivasFormula):String {
				if (pivasFormula.firstPivasFormulaDrug.pivasDosageUnit != "ML") {
					return pivasFormula.concn + (pivasFormula.firstPivasFormulaDrug.pivasDosageUnit == "ML" ? "" : " " + pivasFormula.firstPivasFormulaDrug.pivasDosageUnit + "/ML");
				}
				
				if (pivasFormula.firstPivasFormulaDrug.pivasDosageUnit == "ML" && !isNaN(pivasFormula.coreVolume) && !isNaN(pivasFormula.coreDiluentVolume)) {
					if ( pivasFormula.coreVolume == (pivasFormula.coreVolume + pivasFormula.coreDiluentVolume) ) {
						if ( pivasFormula.firstPivasFormulaDrug.strength == null ) {
							return "";
						} else {
							return pivasFormula.firstPivasFormulaDrug.strength;
						}
					} else {
						var str:String = "";
						str += pivasFormula.firstPivasFormulaDrug.strength == null?"":pivasFormula.firstPivasFormulaDrug.strength;
						if ( str.length > 0 ) {
							str += " ";
						}
						str +="(" + pivasFormula.coreVolume + "ML in " + setPrecision((pivasFormula.coreVolume + pivasFormula.coreDiluentVolume),2) + "ML";
						if ( pivasFormula.dmDiluent!=null ) {
							str +=" "+pivasFormula.dmDiluent.pivasDiluentDesc;
						}
						str +=")";
						return str;
					}
				}
				return "";
			}
			
			private function setPrecision(number:Number, precision:int):Number {
				precision = Math.pow(10, precision);
				return Math.round(number * precision)/precision;
			}
			
			private function normalFormulaListFilterFunction(pivasFormula:PivasFormula):Boolean {
				return pivasFormula.type == PivasFormulaType.Normal;
			}
			
			private function mergeFormulaListFilterFunction(pivasFormula:PivasFormula):Boolean {
				return pivasFormula.type == PivasFormulaType.Merge;
			}
			
			private function diluentLabelFunc(pivasFormula:PivasFormula, column:DataGridColumn):String {
				return diluentDataTipFunc(pivasFormula);
			}
			
			private function diluentDataTipFunc(pivasFormula:PivasFormula):String {
				if (pivasFormula.dmDiluent == null) {
					return "Not applicable";
				} else {
					return pivasFormula.dmDiluent.pivasDiluentDesc;
				}
			}
			
			private function siteLabelFunc(pivasFormula:PivasFormula, column:DataGridColumn):String {
				return pivasFormula.dmSite.ipmoeDesc;
			}
			
			private function coreLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				return batchPrepMethodInfo.coreFlag==true?"Y":"";
			}
			
			private function reconMethodLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				return reconMethodDataTipFunction(batchPrepMethodInfo);
			}
			
			private function reconMethodDataTipFunction(batchPrepMethodInfo:BatchPrepMethodInfo):String {
				if (batchPrepMethodInfo.requireSolventFlag) {
					return batchPrepMethodInfo.pivasDrugManufMethodDesc;
				}
				else {
					return "No Reconstitution Required";
				}
			}
			
			private function companyNameDataTipFunction(batchPrepMethodInfo:BatchPrepMethodInfo):String 
			{
				return batchPrepMethodInfo.companyName;
			}
			
			private function volumeLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				if ( !isNaN(batchPrepMethodInfo.volume) ){
					return batchPrepMethodInfo.volume.toString();
				}
				return "";
			}
			
			private function diluentVolumeLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				if ( !isNaN(batchPrepMethodInfo.diluentVolume) ){
					return batchPrepMethodInfo.diluentVolume.toString();
				}
				return "";
			}
			
			private function finalVolumnLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				if ( !isNaN(batchPrepMethodInfo.afterVolume) ){
					return batchPrepMethodInfo.afterVolume.toString();
				}
				return "";
			}
			
			private function hqSuspendLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				return batchPrepMethodInfo.hqSuspend==true?"Y":"N";
			}
			
			private function localSuspendLabelFunc(batchPrepMethodInfo:BatchPrepMethodInfo, column:DataGridColumn):String {
				return batchPrepMethodInfo.suspend==true?"Y":"N";
			}
			
			public function mergePrepMethodLocalSuspendTextFunc(itemCode:String):String {
				if (pivasMergeFormulaDmDrugLiteMap != null) {
					var dmDrugLite:DmDrugLite = pivasMergeFormulaDmDrugLiteMap[itemCode];
					return dmDrugLite != null && dmDrugLite.itemStatus != "A" ? "S" : "";
				}
				return "";
			}
			
			private function isAnyPivasMergeFormulaMethodDrugItemLocalSuspended(pivasMergeFormulaMethod:PivasFormulaMethod):Boolean {
				if (pivasMergeFormulaDmDrugLiteMap != null) {
					for each (var pivasFormulaMethodDrug:PivasFormulaMethodDrug in pivasMergeFormulaMethod.pivasFormulaMethodDrugList) {
						var dmDrugLite:DmDrugLite = pivasMergeFormulaDmDrugLiteMap[pivasFormulaMethodDrug.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode];
						if (dmDrugLite != null && dmDrugLite.itemStatus != "A") {
							return true;
						}
					}
				}
				return false;
			}
			
			private function retrievePivasMergeFormulaDmDrugLiteMap():void
			{
				var itemCodeList:ArrayCollection = new ArrayCollection();
				
				for each (var pivasMergeFormula:PivasFormula in pivasMergeFormulaListView) {
					for each (var pivasFormulaMethod:PivasFormulaMethod in pivasMergeFormula.pivasFormulaMethodList) {
						for each (var pivasFormulaMethodDrug:PivasFormulaMethodDrug in pivasFormulaMethod.pivasFormulaMethodDrugList) {
							var itemCode:String = pivasFormulaMethodDrug.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode;
							if (!itemCodeList.contains(itemCode)) {
								itemCodeList.addItem(itemCode);
							}
						}
					}
				}
				
				if (itemCodeList.length > 0) {
					dispatchEvent(new RetrieveDmDrugLiteMapEvent(itemCodeList, setPivasMergeFormulaDmDrugLiteMap));
				}
			}
			
			protected function formulaGrid_changeHandler():void
			{
				var pivasFormula:PivasFormula = formulaGrid.selectedItem as PivasFormula;
				if ( pivasFormula != null ) {
					dispatchEvent( new RetrieveBatchPrepMethodInfoListEvent(pivasFormula.id, setFormulaMethodList) );
				}
			}
			
			protected function mergeFormulaGrid_changeHandler():void
			{
				var pivasFormula:PivasFormula = mergeFormulaGrid.selectedItem as PivasFormula;
				if (pivasFormula != null) {
					dispatchEvent(new RetrievePivasFormulaMethodListByFormulaIdEvent(pivasFormula.id, setMergeFormulaMethodList));
				}
			}
			
			protected function tabNavigator_changeHandler(evt:IndexChangedEvent):void
			{
				if (evt.newIndex == 1 && pivasMergeFormulaListView.length > 0 && mergeFormulaGrid.selectedIndex == -1) {
					retrievePivasMergeFormulaDmDrugLiteMap();
					mergeFormulaGrid.selectedIndex = 0;
					mergeFormulaGrid_changeHandler();
				}
			}

		]]>
	</fx:Script>
	
	<fx:Binding source="{(tabNavigator.selectedChild == mergeFormulaTab &amp;&amp; mergePrepMethodList.selectedItem != null &amp;&amp; !isAnyPivasMergeFormulaMethodDrugItemLocalSuspended(mergePrepMethodList.selectedItem as PivasFormulaMethod)) ||
				(tabNavigator.selectedChild == formulaTab &amp;&amp; preparationMethodGrid.selectedItem != null &amp;&amp; !preparationMethodGrid.selectedItem.suspend &amp;&amp; !preparationMethodGrid.selectedItem.hqSuspend)}"
				destination="okBtnEnable"/>
	
	<s:Panel id="popupPanel" width="100%" height="100%">
		<s:layout>
			<s:VerticalLayout gap="10" paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10"/>
		</s:layout>
		<s:HGroup id="formulaSearchGroup" width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Display Name"/>
			<fc:UppercaseTextInput id="displayNameTxt" width="200"/>
			<fc:ExtendedButton label="Search" click="searchbuttonClickHandler()"/>
		</s:HGroup>
		<mx:TabNavigator id="tabNavigator" width="100%" height="100%" change="tabNavigator_changeHandler(event)">
			<fc:ExtendedNavigatorContent id="formulaTab" width="100%" height="100%" label="Formula ({pivasNormalFormulaListView.length})">
				<fc:layout>
					<s:VerticalLayout gap="10" paddingLeft="5" paddingRight="5" paddingTop="0" paddingBottom="5"/>
				</fc:layout>
				<s:VGroup width="100%" height="50%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">
					<s:Panel title="Formula" width="100%" height="100%">
						<fc:ExtendedDataGrid id="formulaGrid" dataProvider="{pivasNormalFormulaListView}" width="100%" height="100%"
											 draggableColumns="false" horizontalScrollPolicy="auto" 
											 sortableColumns="false" resizableColumns="false" variableRowHeight="true" 
											 change="formulaGrid_changeHandler()">
							<fc:columns>
								<mx:DataGridColumn headerText="Display Name" dataField="firstPivasFormulaDrug.displayname" width="{formulaGrid.width*0.36}"/>
								<mx:DataGridColumn headerText="Salt Property" dataField="firstPivasFormulaDrug.saltProperty" width="{formulaGrid.width*0.09}"/>
								<mx:DataGridColumn headerText="Form Code" dataField="firstPivasFormulaDrug.formCode" width="{formulaGrid.width*0.08}"/>
								<mx:DataGridColumn headerText="Formula Concentration / Strength (Ratio)" headerWordWrap="true" 
												   labelFunction="concnLabelFunc" width="{formulaGrid.width*0.17}" 
												   dataTipFunction="concnDataTipFunc" 
												   showDataTips="true"/>
								<mx:DataGridColumn headerText="Diluent" 
												   labelFunction="diluentLabelFunc" width="{formulaGrid.width*0.14}" 
												   dataTipFunction="diluentDataTipFunc" 
												   showDataTips="true"/>
								<mx:DataGridColumn headerText="Route / Site" labelFunction="siteLabelFunc"/>
							</fc:columns>
						</fc:ExtendedDataGrid>
					</s:Panel>
					<s:BorderContainer width="100%" height="20">
						<s:HGroup width="100%"  paddingRight="5" paddingTop="4" horizontalAlign="right" verticalAlign="middle">
							<s:Label text="{pivasNormalFormulaListView.length} record(s)" />
						</s:HGroup>
					</s:BorderContainer>
				</s:VGroup>
				<s:VGroup width="100%" height="50%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">
					<s:Panel title="Preparation Method" width="100%" height="100%">
						<fc:ExtendedDataGrid id="preparationMethodGrid" dataProvider="{pivasFormulaMethodList}" 
											 width="100%" height="100%"
											 draggableColumns="false" horizontalScrollPolicy="auto" 
											 sortableColumns="false" resizableColumns="false" variableRowHeight="true" >
							<fc:columns>
								<mx:DataGridColumn headerText="Core" labelFunction="coreLabelFunc" width="{preparationMethodGrid.width*0.038}"/>
								<mx:DataGridColumn headerText="Item Code" dataField="itemCode" width="{preparationMethodGrid.width*0.073}"/>
								<mx:DataGridColumn headerText="Manufacturer" dataField="companyCode" width="{preparationMethodGrid.width*0.09}" showDataTips="true" dataTipFunction="companyNameDataTipFunction"/>
								<mx:DataGridColumn headerText="Reconstitution Method" labelFunction="reconMethodLabelFunc" width="{preparationMethodGrid.width*0.3}" showDataTips="true" dataTipFunction="reconMethodDataTipFunction"/>
								<mx:DataGridColumn headerText="Required Volume" dataField="volume" width="{preparationMethodGrid.width*0.11}" labelFunction="volumeLabelFunc"/>
								<mx:DataGridColumn headerText="Diluent Volume" dataField="diluentVolume" width="{preparationMethodGrid.width*0.095}" labelFunction="diluentVolumeLabelFunc"/>
								<mx:DataGridColumn headerText="Final Volume" labelFunction="finalVolumnLabelFunc" width="{preparationMethodGrid.width*0.085}"/>
								<mx:DataGridColumn headerText="HQ Suspend" labelFunction="hqSuspendLabelFunc" width="{preparationMethodGrid.width*0.082}"/>
								<mx:DataGridColumn headerText="Local Suspend" labelFunction="localSuspendLabelFunc"/>
							</fc:columns>
						</fc:ExtendedDataGrid>
					</s:Panel>
					<s:BorderContainer width="100%" height="20">
						<s:HGroup width="100%" paddingRight="5" paddingTop="4" horizontalAlign="right" verticalAlign="middle">
							<s:Label text="{pivasFormulaMethodList.length} record(s)" />
						</s:HGroup>
					</s:BorderContainer>
				</s:VGroup>
			</fc:ExtendedNavigatorContent>
			<fc:ExtendedNavigatorContent id="mergeFormulaTab" width="100%" height="100%" label="Merge Formula ({pivasMergeFormulaListView.length})">
				<fc:layout>
					<s:VerticalLayout gap="10" paddingLeft="5" paddingRight="5" paddingTop="0" paddingBottom="5"/>
				</fc:layout>
				<s:VGroup width="100%" height="50%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">
					<s:Panel title="Merge Formula" width="100%" height="100%">
						<fc:ExtendedDataGrid id="mergeFormulaGrid" dataProvider="{pivasMergeFormulaListView}" width="100%" height="177"
											 draggableColumns="false" verticalScrollPolicy="on" horizontalScrollPolicy="off" 
											 sortableColumns="false" resizableColumns="false" variableRowHeight="true" wordWrap="true"
											 change="mergeFormulaGrid_changeHandler()">
							<fc:columns>
								<mx:DataGridColumn headerText="Item Code" dataField="itemCode" width="80"/>
								<mx:DataGridColumn headerText="Preparation Description" dataField="printName"/>
								<mx:DataGridColumn headerText="Route / Site" labelFunction="{siteLabelFunc}" dataTipFunction="{siteLabelFunc}" width="160"/>
							</fc:columns>
						</fc:ExtendedDataGrid>
					</s:Panel>
					<s:BorderContainer width="100%" height="20">
						<s:HGroup width="100%" paddingRight="5" paddingTop="4" horizontalAlign="right" verticalAlign="middle">
							<s:Label text="{pivasMergeFormulaListView.length} record(s)" />
						</s:HGroup>
					</s:BorderContainer>
				</s:VGroup>
				<s:VGroup width="100%" height="50%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">
					<s:Panel title="Preparation Method" width="100%" height="100%">
						<s:layout>
							<s:VerticalLayout gap="0"/>
						</s:layout>
						
						<fc:ExtendedDataGrid id="mergePrepMethodDataGrid" width="100%" height="22"
											 borderStyle="none" focusEnabled="false" resizableColumns="false"
											 draggableColumns="false" sortableColumns="false">
							<fc:columns>
								<fc:ExtendedDataGridColumn width="{CORE_COLUMN_WIDTH}" headerText="Core" dataField="coreFlag"/>
								<fc:ExtendedDataGridColumn width="{LOCAL_SUSPEND_COLUMN_WIDTH}" headerText="S"/>
								<fc:ExtendedDataGridColumn width="{ITEM_CODE_COLUMN_WIDTH}" headerText="Item Code" />
								<fc:ExtendedDataGridColumn width="{MANUF_COLUMN_WIDTH}" headerText="Manuf."/>
								<fc:ExtendedDataGridColumn width="{ITEM_DESC_COLUMN_WIDTH}" headerText="Item Description"/>
								<fc:ExtendedDataGridColumn width="{REQUIRED_VOLUME_COLUMN_WIDTH}" headerText="Required Vol."/>
								<fc:ExtendedDataGridColumn width="{DILUENT_COLUMN_WIDTH}" headerText="Diluent"/>
								<fc:ExtendedDataGridColumn width="{SHELF_LIFE_COLUMN_WIDTH}" headerText="Shelf Life"/>
								<fc:ExtendedDataGridColumn headerText="Status"/>
							</fc:columns>
						</fc:ExtendedDataGrid>
						<s:List id="mergePrepMethodList" width="100%" height="100%" fontWeight="bold"
								useVirtualLayout="false" hasFocusableChildren="true"
								verticalScrollPolicy="on" horizontalScrollPolicy="off"
								dataProvider="{pivasMergeFormulaMethodList}"
								keyUp="">
							<s:itemRenderer>
								<fx:Component>
									<s:ItemRenderer>
										<fx:Script>
											<![CDATA[
												import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
											]]>
										</fx:Script>
										
										<s:layout>
											<s:VerticalLayout gap="0"/>
										</s:layout>
										
										<s:HGroup width="100%" gap="0">
											<s:VGroup width="{outerDocument.CORE_COLUMN_WIDTH - 1}" height="100%" verticalAlign="middle" horizontalAlign="center">
												<s:Label text="{data.coreFlag ? 'Y' : ''}"/>
											</s:VGroup>
											<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
											<s:DataGroup width="{outerDocument.LOCAL_SUSPEND_COLUMN_WIDTH + outerDocument.ITEM_CODE_COLUMN_WIDTH + outerDocument.MANUF_COLUMN_WIDTH + outerDocument.ITEM_DESC_COLUMN_WIDTH + outerDocument.REQUIRED_VOLUME_COLUMN_WIDTH - 1}"
														 dataProvider="{data.pivasFormulaMethodDrugList}">
												<s:layout>
													<s:VerticalLayout gap="0"/>
												</s:layout>
												<s:itemRenderer>
													<fx:Component>
														<s:ItemRenderer>
															<fx:Script>
																<![CDATA[
																	import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
																]]>
															</fx:Script>
															
															<s:layout>
																<s:VerticalLayout gap="0"/>
															</s:layout>
															<mx:HRule styleName="{itemIndex != 0 ? 'vettingMpRuleStyle' : 'vettingMpInvisibleRuleStyle'}" width="100%"/>
															<s:HGroup gap="0">
																<s:Label width="{outerDocument.outerDocument.LOCAL_SUSPEND_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
																		 text="{outerDocument.outerDocument.mergePrepMethodLocalSuspendTextFunc(data.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode)}"/>
																<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
																<s:Label width="{outerDocument.outerDocument.ITEM_CODE_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
																		 text="{data.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode}"/>
																<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
																<s:Label width="{outerDocument.outerDocument.MANUF_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
																		 text="{data.pivasDrugManufMethod.pivasDrugManuf.manufacturerCode}"/>
																<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
																<s:Label width="{outerDocument.outerDocument.ITEM_DESC_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
																		 text="{data.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.dmDrug.fullDrugDesc}"/>
																<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
																<s:Label width="{outerDocument.outerDocument.REQUIRED_VOLUME_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
																		 text="{MedProfileUtils.formatBrackets(data.afterVolume, '', 'ML')}"/>
															</s:HGroup>
														</s:ItemRenderer>
													</fx:Component>
												</s:itemRenderer>
											</s:DataGroup>
											<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
											<s:Label width="{outerDocument.DILUENT_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
													 text="{MedProfileUtils.formatBrackets(data.diluentVolume, '', 'ML')}"/>
											<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
											<s:Label width="{outerDocument.SHELF_LIFE_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
													 text="{MedProfileUtils.formatBrackets(data.shelfLife, '', ' hr')}"/>
											<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
											<s:Label paddingLeft="5" paddingTop="5" paddingBottom="5"
													 text="{data.status}"/>
										</s:HGroup>
										<mx:HRule styleName="vettingMpRuleStyle" width="100%"/>
									</s:ItemRenderer>
								</fx:Component>
							</s:itemRenderer>
						</s:List>
					</s:Panel>
					<s:BorderContainer width="100%" height="20">
						<s:HGroup width="100%"  paddingRight="5" paddingTop="4" horizontalAlign="right" verticalAlign="middle">
							<s:Label text="{pivasMergeFormulaMethodList.length} record(s)" />
						</s:HGroup>
					</s:BorderContainer>
				</s:VGroup>
			</fc:ExtendedNavigatorContent>
		</mx:TabNavigator>
		<s:HGroup width="100%" horizontalAlign="right">
			<fc:ExtendedButton id="okBtn" label="OK" click="okHandler(event)" enabled="{okBtnEnable}"/>
			<fc:ExtendedButton id="cancelBtn" label="Cancel" click="cancelHandler(event)"/>
		</s:HGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>
