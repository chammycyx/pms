<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:mdcs="com.iwobanas.controls.*"
	xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
	width="400" height="200" 
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*">
	
	<fx:Metadata>
		[Name("suspendPrescPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.lookup.popup.SuspendPrescPopupProp;
			import hk.org.ha.event.pms.main.reftable.rdr.RetrieveSuspendReasonRdrListEvent;
			import hk.org.ha.model.pms.persistence.reftable.SuspendReason;
			import hk.org.ha.model.pms.vo.security.PropMap;
			
			import mx.collections.ArrayCollection;
			import mx.managers.PopUpManager;
			
			import spark.components.supportClasses.DropDownListBase;
			import spark.events.DropDownEvent;
						
			[In][Bindable]
			public var suspendReasonRdrList:ArrayCollection;
			
			[In]
			public var propMap:PropMap;
						
			private static var SUSPEND_ORDER_SUSPENDCODE:String = "suspend.order.suspendCode";
			private static var ONESTOP_ALLOWMODIFY_ORDER_SUSPENDCODE:String = "oneStop.allowModify.order.suspendCode";
			private static var ONESTOP_UNCOLLECT_ORDER_SUSPENDCODE:String = "oneStop.uncollect.order.suspendCode";
			private static var VETTING_CANCEL_ORDER_SUSPEND_CODE:String = "vetting.cancel.order.suspendCode";
			private static var ALLOW_MODIFY:String = "Allow Modify";
			private static var UNCOLLECT:String = "Uncollect";
			private static var SUSPEND:String = "Suspend";
			private static var VETTING:String = "vetting";
						
			private var suspendReason:SuspendReason;
			private var caller:String;
			
			public var cancelFunc:Function;
			
			public function setDefaultSuspendCode():void
			{
				var defaultCode:String;
				switch (caller)
				{
					case SUSPEND:
						defaultCode = propMap.getValue(SUSPEND_ORDER_SUSPENDCODE);
						break;
					
					case ALLOW_MODIFY:
						defaultCode = propMap.getValue(ONESTOP_ALLOWMODIFY_ORDER_SUSPENDCODE);
						break;
					
					case UNCOLLECT:
						defaultCode = propMap.getValue(ONESTOP_UNCOLLECT_ORDER_SUSPENDCODE);
						break;
					
					case VETTING:
						defaultCode = propMap.getValue(VETTING_CANCEL_ORDER_SUSPEND_CODE);
						break;
				}
				
				for each (var suspendReason:SuspendReason in suspendReasonRdrList)
				{
					if (suspendReason.suspendCode == defaultCode)
					{
						suspendCodeDdl.selectedItem = suspendReason;
						break;
					}
				}
				
			}
			
			private function close(evt:MouseEvent):void {
				PopUpManager.removePopUp(this);
				if ( cancelFunc != null ){
					cancelFunc();
				}
			}
			
			public function updateSuspendPrescPopupDisplay(suspendPrescPopupProp:SuspendPrescPopupProp):void{			
				caller = suspendPrescPopupProp.caller;
				if (suspendPrescPopupProp.caller == SUSPEND || suspendPrescPopupProp.caller == ALLOW_MODIFY ||
					suspendPrescPopupProp.caller == UNCOLLECT || suspendPrescPopupProp.caller == VETTING)
				{
					dispatchEvent(new RetrieveSuspendReasonRdrListEvent(setDefaultSuspendCode));
				}
				else
				{
					dispatchEvent(new RetrieveSuspendReasonRdrListEvent());
				}				
				
				ctx.suspendReasonRdrList = null;
				
				if(suspendPrescPopupProp==null){
					return;
				}
				
				if(suspendPrescPopupProp.okHandler != null){
					lookupOk.addEventListener(MouseEvent.CLICK, suspendPrescPopupProp.okHandler);
				}else{						
					lookupOk.addEventListener(MouseEvent.CLICK, close);
				}
				
				if(suspendPrescPopupProp.cancelHandler != null){
					lookupCancel.addEventListener(MouseEvent.CLICK, suspendPrescPopupProp.cancelHandler);
				}else{
					lookupCancel.addEventListener(MouseEvent.CLICK, close);
				}
				
				ticketDateTxt.text = dateFormatter.format(suspendPrescPopupProp.ticketDate).toString();
				ticketNumTxt.text = suspendPrescPopupProp.ticketNum;
				suspendCodeDdl.setFocus();
			}
			
			public function showPopupFocus():void {
				focusManager.showFocus();
			}
			
			public function suspenCodeDropDownLabelFunc(suspendReason:SuspendReason):String {
				if (suspendReason == null) {
					return "";
				} else {
					return suspendReason.suspendCode + " - " + suspendReason.description;
				}
			}
			
			public function suspenCodeDropDownExtLabelFunc(suspendReason:SuspendReason):String {
				if (suspendReason == null) {
					return "";
				} else {
					return suspendReason.suspendCode;
				}
			}

			private function dropDownListopenHandler(evt:DropDownEvent, obj:DropDownListBase, width:int, height:int):void {
				obj.dropDown.width = width;
				obj.dropDown.height = height;
			}
			
			protected function lookupOkKeyUpHandler(event:KeyboardEvent):void{				
				if( event.keyCode == Keyboard.ENTER ){
					if ( lookupOk.enabled ) {
						lookupOk.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					}
				}
			}
		]]>
	</fx:Script>
	<s:Panel width="100%" height="100%" title="Suspend Prescription">
		<s:VGroup width="100%" height="100%" left="10" right="10" bottom="10" top="10" gap="10">
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="left">
				<s:Label text="Dispensing Date" width="100"/>
				<s:TextInput id="ticketDateTxt" enabled="false" width="120"/>
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="left">
				<s:Label text="Ticket No." width="100"/>
				<s:TextInput id="ticketNumTxt" enabled="false" width="120"/>
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="left">
				<s:Label text="Suspend Code" width="100"/>
				<fc:ExtendedDropDownList id="suspendCodeDdl" dataProvider="{suspendReasonRdrList}" width="120" extLabelFunction="suspenCodeDropDownExtLabelFunc" 
										 labelFunction="suspenCodeDropDownLabelFunc" open="dropDownListopenHandler(event, suspendCodeDdl, 300, 250)" 
										 keyUp="lookupOkKeyUpHandler(event)"/>				
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="left">
				<s:Label text="Suspend Description" width="100"/>
				<s:TextInput id="suspendDescTxt" enabled="false" text="{suspendCodeDdl.selectedItem.description}" width="270"/>
			</s:HGroup>
			<s:HGroup width="100%" verticalAlign="middle" horizontalAlign="right">
				<fc:ExtendedButton label="OK" id="lookupOk" enabled="{suspendCodeDdl.selectedIndex>=0}"/>
				<fc:ExtendedButton label="Cancel" id="lookupCancel"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>