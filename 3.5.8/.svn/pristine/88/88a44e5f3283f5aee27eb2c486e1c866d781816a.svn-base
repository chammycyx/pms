package hk.org.ha.model.pms.vo.vetting.mp {
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import hk.org.ha.event.pms.main.vetting.popup.ShowDailyFreqPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowRouteSitePopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowSupplFreqPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedAbbDateField;
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedComboBox;
	import hk.org.ha.fmk.pms.flex.components.core.ExtendedDropDownList;
	import hk.org.ha.fmk.pms.flex.utils.ExtendedDateFieldValidator;
	import hk.org.ha.model.pms.dms.persistence.DmDailyFrequency;
	import hk.org.ha.model.pms.dms.persistence.DmSite;
	import hk.org.ha.model.pms.dms.persistence.DmSupplFrequency;
	import hk.org.ha.model.pms.dms.persistence.DmSupplSite;
	import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
	import hk.org.ha.model.pms.udt.vetting.ActionStatus;
	import hk.org.ha.model.pms.udt.vetting.FmStatus;
	import hk.org.ha.model.pms.udt.vetting.PrnPercentage;
	import hk.org.ha.model.pms.udt.vetting.RegimenType;
	import hk.org.ha.model.pms.vo.medprofile.VettingEditItemViewInfo;
	import hk.org.ha.model.pms.vo.rx.Dose;
	import hk.org.ha.model.pms.vo.rx.DoseGroup;
	import hk.org.ha.model.pms.vo.rx.Freq;
	import hk.org.ha.model.pms.vo.rx.Regimen;
	import hk.org.ha.model.pms.vo.rx.Site;
	import hk.org.ha.model.pms.vo.security.PropMap;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	import hk.org.ha.view.pms.main.vetting.popup.DailyFreqPopup;
	import hk.org.ha.view.pms.main.vetting.popup.DayOfWeekPopup;
	import hk.org.ha.view.pms.main.vetting.popup.RouteSitePopup;
	import hk.org.ha.view.pms.main.vetting.popup.SupplFreqPopup;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	import mx.utils.StringUtil;
	
	import org.granite.tide.seam.Context;
	
	import spark.components.Application;
	import spark.components.DropDownList;
	import spark.events.DropDownEvent;
	import spark.events.IndexChangeEvent;
	
	[Bindable]
	public class VettingEditItemViewUtil {
		
		public static function dailyFreqItemMatchingFunc(comboBox:ExtendedComboBox, inputText:String):Vector.<int> {
			var result:Vector.<int> = new Vector.<int>;
			var dmDailyFreq:DmDailyFrequency;
			var enteredText:String = inputText.toUpperCase();
			var targetIndex:int = -1;
			for ( var index:int = 0; index < comboBox.dataProvider.length; index++ ) {
				dmDailyFreq = comboBox.dataProvider.getItemAt(index) as DmDailyFrequency;
				if ( dmDailyFreq.dailyFreqDesc.indexOf(enteredText) == 0 ) {
					if ( dmDailyFreq.dailyFreqDesc == enteredText ) {
						targetIndex = index;
					} else {
						result.push(index);
					}
				}
			}
			
			if ( targetIndex != -1 ) {
				result.unshift(targetIndex);
			}
			return result;
		}
		
		public static function clearDailyFreq(data:Dose, dailyFreqCbx:ExtendedComboBox):void
		{
			data.dailyFreq				= new Freq;
			data.dmDailyFrequency		= new DmDailyFrequency;
			dailyFreqCbx.selectedItem	= null;
		}
		
		public static function dailyFreqCbxChangeHandler(event:IndexChangeEvent,itemRenderer:*,vettingEditItemViewInfo:VettingEditItemViewInfo):void
		{		
			var data:Dose = itemRenderer.data as Dose;
			var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
			try {
				if ( !data || event.newIndex < 0 ) {
					if ( data && data.dailyFreq && data.dailyFreq.param && data.dailyFreq.param.length > 0 ) {
						return;
					}
					clearDailyFreq(data,dailyFreqCbx);
					data.dailyFreqErrorString = vettingEditItemViewInfo.sysMsgMap.getMessage("0055");
					return;
				}
				
				var oldFreqCode:String					= (data.dailyFreq) ? data.dailyFreq.code : null;
				var dmDailyFrequency:DmDailyFrequency 	= itemRenderer.parentApplication.dmDailyFrequencyList.getItemAt(event.newIndex) as DmDailyFrequency; 	
				data.dmDailyFrequency 					= dmDailyFrequency;
				data.dailyFreq 		  					= new Freq;
				//PMS-3455
			
				data.startAdminRate = null;
				data.endAdminRate = null;
				
				Freq(data.dailyFreq).param				= [];
				Freq(data.dailyFreq).code   			= dmDailyFrequency.dailyFreqCode;
				Freq(data.dailyFreq).desc				= dmDailyFrequency.dailyFreqDesc;
				Freq(data.dailyFreq).multiplierType		= "-";
				if ( dmDailyFrequency.multiplierType == "F" ) {
					data.dmAdminTime 	= null;
					data.adminTimeCode 	= null;
				}				
				
				if ( data.isStop ) {					
					data.clearDoseForStop();
				}
				
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}			
		
		
		public static function dailyFreqCbxKeyDownHandler(event:KeyboardEvent, itemRenderer:*, updateFunc:Function,vettingEditItemViewInfo:VettingEditItemViewInfo):void
		{
			var data:Dose = itemRenderer.data as Dose;
			var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
			
			if ( event.keyCode == Keyboard.UP || 
				event.keyCode == Keyboard.DOWN ||
				event.keyCode == Keyboard.HOME ||
				event.keyCode == Keyboard.END ) {						
				dailyFreqCbx.openDropDown();
				dailyFreqCbx.textInput.textDisplay.horizontalScrollPosition = 0;//PMSIPU-987
				//PMS-2986
			} else if ( (event.keyCode == Keyboard.DELETE || event.keyCode == Keyboard.BACKSPACE) && !dailyFreqCbx.isDropDownOpen ) {
				if(StringUtil.trim(dailyFreqCbx.textInput.text) != ""  ){
					//PMS-2986
					if ( !data.dailyFreq  || dailyFreqCbx.textInput.text != data.dailyFreq.desc ) {
						clearDailyFreq(data,dailyFreqCbx);
						if ( StringUtil.trim(dailyFreqCbx.textInput.text) != "" ) {
							dailyFreqCbx.setFocus();
						} else {
							data.dailyFreqErrorString = null;
						}
						updateFunc();
					}
					
				}
			} else if( (event.keyCode == Keyboard.ENTER ||  event.keyCode == Keyboard.TAB  )  ){
				if ( StringUtil.trim(dailyFreqCbx.textInput.text) == "" && 
					data.dailyFreq && 
					data.dailyFreq.code && 
					data.dailyFreq.code != "" ) {
					clearDailyFreq(data,dailyFreqCbx);
					if ( updateFunc != null ) {
						updateFunc();
					}
					
				}
			} 
		}
		
		
		public static function dailyFreqPopupHandler(itemRenderer:*, recalHandler:Function, vettingEditItemViewInfo:VettingEditItemViewInfo):void
		{
			var data:Dose = itemRenderer.data as Dose;
			var dailyFreqCbx:ExtendedComboBox = itemRenderer.dailyFreqCbx as ExtendedComboBox;
			
			try {
				if ( !data || !data.dailyFreq || !data.dailyFreq.param || data.dailyFreq.param.length > 0 ) {
					recalHandler();
					return;
				}
				var dmDailyFrequency:DmDailyFrequency = dailyFreqCbx.selectedItem as DmDailyFrequency;						
				if( dmDailyFrequency && !isNaN(dmDailyFrequency.numOfInputValue) 
					&& dmDailyFrequency.numOfInputValue > 0 ) {
					
					var okHandler:Function = function(popup:DailyFreqPopup):void {		
						try {
							data.dailyFreq.param.push( Number(popup.param1.selectedItem).toString() );
							data.dailyFreq.desc         = data.getDailyFreqFullDesc();
							dailyFreqCbx.textInput.text = data.dailyFreq.desc;
							dailyFreqCbx.toolTip 		= data.dailyFreq.desc;						
							recalHandler();
							PopUpManager.removePopUp(popup);
						} catch (e:Error) {
							trace(e.getStackTrace());
						}
					}									
					
					var cancelHandler:Function = function(evt:Event):void {
						clearDailyFreq(data,dailyFreqCbx);										
						PopUpManager.removePopUp( DailyFreqPopup(UIComponent(evt.target).parentDocument) );
					}
					
					vettingEditItemViewInfo.context.dispatchEvent( new ShowDailyFreqPopupEvent(dmDailyFrequency,okHandler, cancelHandler));					
				} else {
					recalHandler();
				}
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		/* Supplementary Frequency Function */
		public static function supplFreqItemMatchingFunc(comboBox:ExtendedComboBox, inputText:String):Vector.<int> {
			var result:Vector.<int> = new Vector.<int>;
			var dmSupplFreq:DmSupplFrequency;
			var enteredText:String = inputText.toUpperCase();
			var targetIndex:int = -1;
			for ( var index:int = 0; index < comboBox.dataProvider.length; index++ ) {
				dmSupplFreq = comboBox.dataProvider.getItemAt(index) as DmSupplFrequency;
				if ( dmSupplFreq.supplFreqDesc.indexOf(enteredText) == 0 ) {
					if ( dmSupplFreq.supplFreqDesc == enteredText ) {
						targetIndex = index;
					} else {
						result.push(index);
					}
				}
			}
			
			if ( targetIndex != -1 ) {
				result.unshift(targetIndex);
			}
			return result;
		}
		
		public static function supplFreqCbxKeyDownHandler(event:KeyboardEvent, itemRenderer:*, updateFunc:Function):void
		{
			try {
				var data:Dose = itemRenderer.data as Dose;
				var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
				
				if ( event.keyCode == Keyboard.UP || 
					event.keyCode == Keyboard.DOWN ||
					event.keyCode == Keyboard.HOME ||
					event.keyCode == Keyboard.END ) {					
					supplFreqCbx.openDropDown();	
					supplFreqCbx.textInput.textDisplay.horizontalScrollPosition = 0;//PMSIPU-987
					//PMS-2986
				} else if ( (event.keyCode == Keyboard.DELETE || event.keyCode == Keyboard.BACKSPACE) && !supplFreqCbx.isDropDownOpen ) {
					if(StringUtil.trim(supplFreqCbx.textInput.text) != ""  ){
						//PMS-2986
						if ( !data.supplFreq || supplFreqCbx.textInput.text != data.supplFreq.desc ) {
							clearSupplFreq(data,supplFreqCbx);
							
							if ( StringUtil.trim(supplFreqCbx.textInput.text) != "" ) {
								supplFreqCbx.setFocus();
							} else {
								data.supplFreqErrorString = null;
							}
							
							updateFunc();
						}
					
					} 
				}else if ( (event.keyCode == Keyboard.ENTER ||  event.keyCode == Keyboard.TAB ) ){
					if ( StringUtil.trim(supplFreqCbx.textInput.text) == "" && 
						data.supplFreq && 
						data.supplFreq.code && 
						data.supplFreq.code != "" ) {
						clearSupplFreq(data as Dose,supplFreqCbx);
						if ( updateFunc != null ) {
							updateFunc();
						}
					}
				} 
				
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
		}
		
		public static function supplFreqCbxChangeHandler(event:IndexChangeEvent,itemRenderer:*,updateFunc:Function,retainDescFlag:Boolean,vettingEditItemViewInfo:VettingEditItemViewInfo):void
		{
			var data:Dose = itemRenderer.data as Dose;
			var dmSupplFrequency:DmSupplFrequency;
			var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
			var pattern:RegExp = /[ ]*$/;
			var inputText:String = supplFreqCbx.textInput.text.replace(pattern,"");
			
			if ( event.newIndex < 0 ) {
				if ( data.dmSupplFrequency && 
					data.supplFreq && 
					data.supplFreq.param && 
					data.supplFreq.param.length > 0 &&
					retainDescFlag &&
					event.newIndex == -1 ) {
					//					//PMS-3455
					
					data.startAdminRate = null;
					data.endAdminRate = null;
					
					supplFreqCbx.selectedItem = data.dmSupplFrequency;
					supplFreqCbx.textInput.text = data.supplFreq.desc;
					retainDescFlag = false;
				} else {
					if ( !data.supplFreq || inputText != data.supplFreq.desc ) {
						clearSupplFreq(data,supplFreqCbx);
						
						if ( StringUtil.trim(supplFreqCbx.textInput.text) != "" ) {
							supplFreqCbx.setFocus();
						} else {
							data.supplFreqErrorString = null;
						}
						
						updateFunc();
					}
				}
			} else {						
				dmSupplFrequency              = vettingEditItemViewInfo.dmSupplFrequencyList.getItemAt(event.newIndex) as DmSupplFrequency;
				data.dmSupplFrequency 		  = dmSupplFrequency;
				
				if ( data.dmSupplFrequency && data.dmSupplFrequency.multiplierType == "E" && dmSupplFrequency.multiplierType == "E" ) {
					data.prevParam = data.supplFreq.param;
				} else {
					data.prevParam = [];
				}
				data.supplFreq 				  = new Freq;
				data.supplFreq.desc			  = dmSupplFrequency.supplFreqDesc;
				data.supplFreq.param 		  = [];
				data.supplFreq.code 		  = dmSupplFrequency.supplFreqCode;
				data.supplFreq.multiplierType = "-";
				if ( dmSupplFrequency.numOfInputValue <= 0 || isNaN(dmSupplFrequency.numOfInputValue) ) {
					if ( data.supplFreqErrorString ) {
						data.supplFreqErrorString = null;
						vettingEditItemViewInfo.manualErrorString = null;
					}
				}					
			}
			
		}
		
		public static function supplFreqCbxPopUpHandler(itemRenderer:*,recalHandler:Function,vettingEditItemViewInfo:VettingEditItemViewInfo ):void
		{
			var data:Dose = itemRenderer.data as Dose;
			var dmSupplFrequency:DmSupplFrequency = data.dmSupplFrequency;
			var supplFreqCbx:ExtendedComboBox = itemRenderer.supplFreqCbx as ExtendedComboBox;
			var okHandler:Function;
			
			try {
				
				if ( !data || !data.supplFreq || !data.supplFreq.param || data.supplFreq.param.length > 0 ) {
					recalHandler();
					return;
				}
				
				if ( !dmSupplFrequency ) { return; }
			} catch (e:Error) {
				trace(e.getStackTrace());
			}
			if ( dmSupplFrequency.multiplierType == "E" ) {
				okHandler = function(popup:DayOfWeekPopup):void {
					try {
						var resultParam:String = ((popup.sunday.selected)?"1":"0") +
							((popup.monday.selected)?"1":"0") +
							((popup.tuesday.selected)?"1":"0") +
							((popup.wednesday.selected)?"1":"0") +
							((popup.thursday.selected)?"1":"0") +
							((popup.friday.selected)?"1":"0") +
							((popup.saturday.selected)?"1":"0");
						if(resultParam != "0000000") {										
							Freq(data.supplFreq).param 			= [resultParam];											
							Freq(data.supplFreq).desc 			= data.supplFreqFullDesc;
							Freq(data.supplFreq).multiplierType = "E";
							supplFreqCbx.textInput.text 		= String(Freq(data.supplFreq).desc).toUpperCase();
							supplFreqCbx.toolTip 				= Freq(data.supplFreq).desc;							
							
							recalHandler();
							PopUpManager.removePopUp(popup);
						}
					} catch (e:Error) {
						trace(e.getStackTrace());
					}
				}								
			} else {
				if ( isNaN(dmSupplFrequency.numOfInputValue) || dmSupplFrequency.numOfInputValue <= 0 ) {
					
					recalHandler();
					return;
				}	
				okHandler = function(popup:SupplFreqPopup):void {	
					try {
						if( dmSupplFrequency.numOfInputValue > 0 ) {
							Freq(data.supplFreq).param.push( Number(popup.param1.selectedItem).toString() );
						}
						if( dmSupplFrequency.numOfInputValue > 1 ) {
							Freq(data.supplFreq).param.push( Number(popup.param2.selectedItem).toString() );
						}										
						Freq(data.supplFreq).desc	= data.supplFreqFullDesc;
						supplFreqCbx.textInput.text = Freq(data.supplFreq).desc;
						supplFreqCbx.toolTip 		= Freq(data.supplFreq).desc;
						
						recalHandler();
						PopUpManager.removePopUp(popup);
					} catch (e:Error) {
						trace(e.getStackTrace());
					}
				}
			}																					
			
			var cancelHandler:Function = function(popup:SupplFreqPopup):void {
				clearSupplFreq(data,supplFreqCbx);					
				PopUpManager.removePopUp( popup );
			}
			
			vettingEditItemViewInfo.context.dispatchEvent( new ShowSupplFreqPopupEvent(
				data.prevParam,
				dmSupplFrequency, 										
				okHandler,
				cancelHandler)
			);				
		}
		
		public static function clearSupplFreq(data:Dose, supplFreqCbx:ExtendedComboBox):void
		{
			data.supplFreq 				= new Freq;
			data.dmSupplFrequency 		= new DmSupplFrequency;
			supplFreqCbx.selectedItem 	= null;
		}
	}
}