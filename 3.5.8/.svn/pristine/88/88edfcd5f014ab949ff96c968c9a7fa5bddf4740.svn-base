<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fx="http://ns.adobe.com/mxml/2009" 
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:msg="hk.org.ha.fmk.pms.flex.components.message.*"
	width="800" height="550">
	
	<fx:Metadata>
		[Name("tpnPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ListCollectionView;
			import mx.controls.dataGridClasses.DataGridColumn;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			
			import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.RetrieveTpnRequestListEvent;
			import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ShowDeleteRequestPopupEvent;
			import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ShowDurationPopupEvent;
			import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ShowModifyDurationPopupEvent;
			import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ShowTpnPopupEvent;
			import hk.org.ha.event.pms.main.vetting.mp.tpn.popup.ViewTpnRecordEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.persistence.medprofile.TpnRequest;
			import hk.org.ha.model.pms.udt.medprofile.TpnRequestStatus;
			import hk.org.ha.model.pms.vo.security.PropMap;
			
			private static const PERMISSION_EDIT_TPN_REQUEST:String = "editTpnRequest";
			
			[In] [Bindable]
			public var permissionMap:PropMap;
			
			[Bindable]
			private var readOnly:Boolean;
			
			[In][Bindable]
			public var tpnRequestList:ListCollectionView;
			
			[In][Bindable]
			public var tpnRequest:TpnRequest;
			
			[Bindable]
			private var currentDate:Date;
			
			private var tpnPopup:TpnPopup;
			
			public function init(evt:ShowTpnPopupEvent):void {
				currentDate = new Date();
				tpnPopup = this;
				
				readOnly = evt.readOnlyFlag;
				if (readOnly) {
					callLater(closeBtn.setFocus);
				}
				
				if(tpnRequestList == null || tpnRequestList.length == 0){
					dispatchEvent(new RetrieveTpnRequestListEvent());
				}
				
				focusManager.showFocus();
			}
			
			private function newRequestHandler():void {
				var isDuplicateFlag:Boolean = false;
				dispatchEvent(new ShowDurationPopupEvent(isDuplicateFlag, null, NaN, tpnPopup));
			}
			
			private function modifyRequestHandler():void {
				dispatchEvent(new ShowModifyDurationPopupEvent(TpnRecordList.selectedItem as TpnRequest, TpnRecordList.selectedIndex, tpnPopup));
			}
			
			private function duplicateRequestHandler():void {
				var isDuplicateFlag:Boolean = true;
				dispatchEvent(new ShowDurationPopupEvent(isDuplicateFlag, TpnRecordList.selectedItem as TpnRequest, TpnRecordList.selectedIndex, tpnPopup));
			}
			
			private function deleteRequestHandler():void {
				dispatchEvent(new ShowDeleteRequestPopupEvent(TpnRecordList.selectedIndex, tpnPopup));
			}
			
			private function viewRequestHandler():void {
				dispatchEvent(new ViewTpnRecordEvent(TpnRecordList.selectedIndex, postViewRecord));
			}
			
			private function postViewRecord(tpnUrl:String):void {
				if(tpnUrl != null){
					if(ExternalInterface.available){
						ExternalInterface.call("openTpnWindow", tpnUrl);
					}
					showSystemMessagePopup("0977");
				}else{
					showSystemMessagePopup("0978");
				}
				
				PopUpManager.removePopUp(tpnPopup);
			}
			
			private function closeHandler():void {
				PopUpManager.removePopUp(tpnPopup);
			}
			
			private function showSystemMessagePopup(code:String, callback:Function=null):void {
				var prop:SystemMessagePopupProp = new SystemMessagePopupProp(code);
				prop.okHandler = function(evt:MouseEvent):void {
					if(callback != null){
						callLater(callback);
					}
					removePopup(evt);
				};
				dispatchEvent(new RetrieveSystemMessageEvent(prop));
			}
			
			private function removePopup(evt:Event):void {
				if (evt != null) {
					PopUpManager.removePopUp((evt.currentTarget as UIComponent).parentDocument as IFlexDisplayObject);
				}
			}
			
			private function createDateLabelFunction(item:TpnRequest, column:DataGridColumn):String{
				return tpnDateFormatter.format(item.createDate);
			}
			
			private function enumLabelFunc(item:TpnRequest, column:DataGridColumn):String {
				if (item.status == null) {
					return "";
				}
				else {
					if(item.status == TpnRequestStatus.Initial || item.status == TpnRequestStatus.Processed){
						return "";
					}
					return item.status.displayValue;
				}
			}
			
			private function dateTimeLabelFunc(item:TpnRequest, column:DataGridColumn):String {
				return dateTimeFormatter.format(item.updateDate);
			}
		]]>
	</fx:Script>
	
	<fx:Declarations>
		<mx:DateFormatter id="tpnDateFormatter" formatString="DD-MMM-YYYY JJ:NN (EEEE)"/>
	</fx:Declarations>
	
	<fx:Declarations>
		<mx:DateFormatter id="dateFormatter" formatString="DD-MMM-YYYY"/>
	</fx:Declarations>
	
	<s:Panel title="Total Parenteral Nutrition" width="100%" height="100%">
		<s:layout>
			<s:VerticalLayout paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" gap="10"/>
		</s:layout>
		<s:VGroup width="100%" height="100%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0">
			<s:Panel id="TpnPanel" title="TPN Record"
					 width="100%" height="100%"
					 mouseEnabled="true">
				<fc:ExtendedDataGrid id="TpnRecordList" dataProvider="{tpnRequestList}"
									 width="100%" height="100%" tabFocusEnabled="true" resizableColumns="false"
									 horizontalScrollPolicy="auto" draggableColumns="false"
									 variableRowHeight="true" wordWrap="true" sortableColumns="false">
					<fc:columns>
						<fc:ExtendedDataGridColumn headerText="Record Creation Date / Time" width="250" dataField="createDate" labelFunction="createDateLabelFunction"/>
						<fc:ExtendedDataGridColumn headerText="Duration (in days)" width="150" textAlign="right" dataField="duration"/>
						<fc:ExtendedDataGridColumn headerText="Status" width="70" dataField="status" labelFunction="enumLabelFunc"/>
						<fc:ExtendedDataGridColumn headerText="Last Update Date / Time" width="250" dataField="updateDate" labelFunction="dateTimeLabelFunc"/>
					</fc:columns>
				</fc:ExtendedDataGrid>
			</s:Panel>
			<s:BorderContainer width="100%" height="20">
				<s:HGroup width="100%"  paddingRight="5" paddingTop="4" horizontalAlign="right" verticalAlign="middle">
					<s:Label text="{tpnRequestList.length} record(s)" />
				</s:HGroup>
			</s:BorderContainer>
		</s:VGroup>
		<s:HGroup width="100%">
			<fc:ExtendedButton id="newBtn" label="New" enabled="{!readOnly &amp;&amp; permissionMap.getValueAsBoolean(PERMISSION_EDIT_TPN_REQUEST)}" click="newRequestHandler()"/>
			<fc:ExtendedButton id="modifyBtn" enabled="{!readOnly &amp;&amp; TpnRecordList.selectedIndex >= 0 &amp;&amp; (TpnRecordList.selectedItem as TpnRequest).status != TpnRequestStatus.Deleted &amp;&amp; dateFormatter.format((TpnRecordList.selectedItem as TpnRequest).createDate) == dateFormatter.format(currentDate) &amp;&amp; permissionMap.getValueAsBoolean(PERMISSION_EDIT_TPN_REQUEST)}" label="Modify" click="modifyRequestHandler()"/>
			<fc:ExtendedButton id="duplicateBtn" enabled="{!readOnly &amp;&amp; TpnRecordList.selectedIndex >= 0 &amp;&amp; (TpnRecordList.selectedItem as TpnRequest).status != TpnRequestStatus.Deleted &amp;&amp; permissionMap.getValueAsBoolean(PERMISSION_EDIT_TPN_REQUEST)}" label="Duplicate" click="duplicateRequestHandler()"/>
			<fc:ExtendedButton id="deleteBtn" enabled="{!readOnly &amp;&amp; TpnRecordList.selectedIndex >= 0 &amp;&amp; (TpnRecordList.selectedItem as TpnRequest).status != TpnRequestStatus.Deleted &amp;&amp; dateFormatter.format((TpnRecordList.selectedItem as TpnRequest).createDate) == dateFormatter.format(currentDate) &amp;&amp; permissionMap.getValueAsBoolean(PERMISSION_EDIT_TPN_REQUEST)}" label="Delete" click="deleteRequestHandler()"/>
			<fc:ExtendedButton id="viewBtn" enabled="{TpnRecordList.selectedIndex >= 0}" label="View" click="viewRequestHandler()"/>
			<s:Spacer width="320"/>
			<fc:ExtendedButton id="closeBtn" label="Close" click="closeHandler()"/>
		</s:HGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>