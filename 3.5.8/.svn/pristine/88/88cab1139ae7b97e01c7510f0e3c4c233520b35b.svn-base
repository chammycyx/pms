package hk.org.ha.model.pms.biz.reftable;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.cache.CacheResultCleanUp;
import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.WorkstoreDrugCacherInf;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.persistence.reftable.ChargeReason;
import hk.org.ha.model.pms.persistence.reftable.FdnMapping;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.persistence.reftable.OperationWorkstation;
import hk.org.ha.model.pms.persistence.reftable.Workstation;
import hk.org.ha.model.pms.udt.reftable.MachineType;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("refTableManager")
@MeasureCalls
public class RefTableManagerBean implements RefTableManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private WorkstoreDrugCacherInf workstoreDrugCacher;

	@Override
	@CacheResult(name="activeProfile", timeout = "@cache.result.active-profile.timeout@", keyable = WorkstoreKeyable.class)
	public OperationProfile retrieveActiveProfile(Workstore workstore) {
		
		OperationProfile activeProfile = (OperationProfile) em.createQuery(
				"select o from OperationProfile o" + // 20120303 index check : OperationProfile.workstore : UI_REFILL_CONFIG_01
				" where o.id = o.workstore.operationProfileId" +
				" and o.workstore = :workstore")
				.setParameter("workstore", workstore)
				.getSingleResult();
		
		return activeProfile;
	}
	
	@CacheResultCleanUp(name="activeProfile")
	public void clearActiveProfile() {		
	}
	
	@SuppressWarnings("unchecked")
	public OperationWorkstation retrieveOperationWorkstation(Workstation workstation, OperationProfile activeProfile) {

		List<OperationWorkstation> list = em.createQuery(
				"select o from OperationWorkstation o" + // 20120906 index check : OperationWork.operationProfile,operationProfile : UI_OPERATION_WORKSTATION_01
				" where o.workstation = :workstation" +
				" and o.operationProfile = :activeProfile")
				.setParameter("workstation", workstation)
				.setParameter("activeProfile", activeProfile)
				.getResultList();
		
		if (!list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public String getFdnByItemCodeOrderTypeWorkstore(String itemCode, OrderType orderType, Workstore workstore) {
		
		List<FdnMapping> fdnMappingList = getFdnMappingList(Arrays.asList(itemCode), orderType, workstore);
		if (fdnMappingList.size() == 0) {
			return workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode).getDmDrug().getDmMoeProperty().getTradename();			
		} else {
			return fdnMappingList.get(0).getFloatDrugName().toUpperCase();
		}
	}
	
	public Map<String, String> getFdnByItemCodeListOrderTypeWorkstore(Collection<String> itemCodeList, OrderType orderType, Workstore workstore) {

		Map<String, String> resultMap = new HashMap<String, String>();
		Map<String, String> fdnMappingMap = getFdnMappingMap(itemCodeList, orderType, workstore);

		for (String itemCode: itemCodeList) {
			String fdn = fdnMappingMap.get(itemCode);
			WorkstoreDrug drug;
			if (fdn == null && (drug = workstoreDrugCacher.getWorkstoreDrug(workstore, itemCode)) != null) {
				fdn = drug.getDmDrug().getDmMoeProperty().getTradename();
			}
			if (fdn != null) {
				resultMap.put(itemCode, fdn.toUpperCase());
			}
		}
		return resultMap;
	}

	public Map<String,String> getFdnMappingMapByPharmOrderItemList(List<PharmOrderItem> pharmOrderItemList, OrderType orderType, Workstore workstore) {
		List<String> itemCodeList = new ArrayList<String>();
		for (PharmOrderItem pharmOrderItem : pharmOrderItemList) {
			itemCodeList.add(pharmOrderItem.getItemCode());
		}
		return getFdnMappingMap(itemCodeList, orderType, workstore);
	}

	public Map<String,String> getFdnMappingMapByDispOrderItemList(List<DispOrderItem> dispOrderItemList, OrderType orderType, Workstore workstore) {
		List<String> itemCodeList = new ArrayList<String>();
		for (DispOrderItem dispOrderItem : dispOrderItemList) {
			itemCodeList.add(dispOrderItem.getPharmOrderItem().getItemCode());
		}
		return getFdnMappingMap(itemCodeList, orderType, workstore);
	}
	
	private Map<String,String> getFdnMappingMap(Collection<String> itemCodeList, OrderType orderType, Workstore workstore) {
		
		List<FdnMapping> fdnMappingList = getFdnMappingList(itemCodeList, orderType, workstore);

		Map<String,String> fdnMappingMap = new HashMap<String,String>();
		for (FdnMapping fdnMapping : fdnMappingList) {
			fdnMappingMap.put(fdnMapping.getItemCode(), fdnMapping.getFloatDrugName().toUpperCase());
		}

		return fdnMappingMap;
	}
	
	@SuppressWarnings("unchecked")
	private List<FdnMapping> getFdnMappingList(Collection<String> itemCodeList, OrderType orderType, Workstore workstore) {
		
		if ( itemCodeList.isEmpty() ) {
			return new ArrayList<FdnMapping>();
		} else {
			return QueryUtils.splitExecute(em.createQuery(
					"select f from FdnMapping f" + // 20120215 index check : MedProfile.workstore,orderType,itemCode : UI_FDN_MAPPING_01
					" where f.itemCode in :itemCodeList" +
					" and f.workstore = :workstore" +
					" and f.orderType= :orderType")
					.setParameter("workstore", workstore)
					.setParameter("orderType", orderType), "itemCodeList", itemCodeList);
		}
	}
		
	@SuppressWarnings("unchecked")
	public List<ChargeReason> retrieveExemptChargeReasonList() {
		return em.createQuery(
				"select o from ChargeReason o " + // 20120214 index check : none
				"where o.type = 'E' " + 
				"order by o.reasonCode"
				).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<OperationWorkstation> getActiveAtdpsWorkstation(OperationProfile activeProfile) {
		List<OperationWorkstation> operationWorkstationList = em.createQuery(
				"select o from OperationWorkstation o " + // 20140826 index check : OperationWorkstation.operationProfile : FK_OPERATION_WORKSTATION_01
				" where o.operationProfile = :activeProfile" +
				" and o.machine.type = :machineType" +
				" and o.enableFlag = :enableFlag")
				.setParameter("activeProfile", activeProfile)
				.setParameter("machineType", MachineType.ATDPS)
				.setParameter("enableFlag", true)
				.getResultList();
		
		if ( !operationWorkstationList.isEmpty() ) {
			for ( OperationWorkstation ow : operationWorkstationList ) {
				if ( ow.getMachine() != null ) {
					ow.getMachine().getPickStation();
				}
			}
		}
		return operationWorkstationList;
	}
}
