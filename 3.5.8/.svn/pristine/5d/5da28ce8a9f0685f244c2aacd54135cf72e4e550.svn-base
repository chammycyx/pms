package hk.org.ha.model.pms.biz.checkissue.mp;

import static hk.org.ha.model.pms.prop.Prop.LABEL_DIVIDERLABEL_ENABLED;
import static hk.org.ha.model.pms.prop.Prop.LABEL_MPDISPLABEL_SORT;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.delivery.BatchNumGeneratorLocal;
import hk.org.ha.model.pms.biz.delivery.BatchNumGeneratorLocal.BatchType;
import hk.org.ha.model.pms.biz.label.LabelManagerLocal;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.report.MpTrxRptPrintManagerLocal;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequestAlert;
import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryStatus;
import hk.org.ha.model.pms.udt.medprofile.PrintMode;
import hk.org.ha.model.pms.vo.checkissue.mp.AssignBatchResult;
import hk.org.ha.model.pms.vo.checkissue.mp.AssignBatchSummary;
import hk.org.ha.model.pms.vo.checkissue.mp.AssignBatchSummaryItem;
import hk.org.ha.model.pms.vo.label.DeliveryLabel;
import hk.org.ha.model.pms.vo.label.MpDispLabel;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("assignBatchService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AssignBatchServiceBean implements AssignBatchServiceLocal {

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@In
	private AuditLogger auditLogger;

	@In
	private Workstore workstore;

	@In
	private BatchNumGeneratorLocal batchNumGenerator;

	@In
	private LabelManagerLocal labelManager;

	@In
	private CheckManagerLocal checkManager;

	@In
	private MpTrxRptPrintManagerLocal mpTrxRptPrintManager;

	@In
	private SendServiceLocal sendService;
	
	@In
	private PrintAgentInf printAgent;

	@Out(required = false)
	private AssignBatchSummaryItem assignBatchSummaryItem;

	@Out(required = false)
	private AssignBatchSummary assignBatchSummary;

	private String msgCode;

	private DateFormat df = new SimpleDateFormat("yyyyMMdd"); 

	private boolean autoUpdate = true;
	
	private static DeliveryLabelRenderAndPrintJobComparator deliveryLabelRenderAndPrintJobComparator = new DeliveryLabelRenderAndPrintJobComparator();

	public void retrieveAssignBatchSummary(Date batchDate, String batchNum) {
		assignBatchSummary = null;
		Delivery delivery  = retrieveDelivery( batchDate,  batchNum);
		if (delivery != null) {
			assignBatchSummary = new AssignBatchSummary();
			convertToAssignBatchSummary(assignBatchSummary, delivery);
		}
	}


	@SuppressWarnings("unchecked")
	public Delivery retrieveDelivery(Date batchDate, String batchNum){

		List<Delivery> deliveryList = em.createQuery(
				"select o from Delivery o" + // 20120314 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
				" where o.hospital = :hospital" +
				" and o.batchDate = :batchDate" +
				" and o.batchNum = :batchNum" +
				" and o.status = :status" +
		" and o.deliveryRequest.status = :deliveryRequestStatus")
		.setParameter("hospital", workstore.getHospital())
		.setParameter("batchDate", batchDate)
		.setParameter("batchNum", batchNum)
		.setParameter("status", DeliveryStatus.Assigning)
		.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed)
		.getResultList();

		if (!deliveryList.isEmpty()) {

			return deliveryList.get(0);
		}else{

			return null;
		}

	}



	private void convertToAssignBatchSummary(
			AssignBatchSummary assignBatchSummary, Delivery delivery) {

		assignBatchSummary.setDeliveryId(delivery.getId());
		assignBatchSummary.setBatchDate(delivery.getBatchDate());
		assignBatchSummary.setBatchNum(delivery.getBatchNum());
		assignBatchSummary.setWardCode(delivery.getWardCode());
		assignBatchSummary
		.setAssignBatchItemList(new ArrayList<AssignBatchSummaryItem>());

		for (DeliveryItem deliveryItem : delivery.getDeliveryItemList()) {
			if (deliveryItem.getStatus() == DeliveryItemStatus.Deleted
					|| deliveryItem.getStatus() == DeliveryItemStatus.Unlink) {
				continue;
			}
			AssignBatchSummaryItem item = new AssignBatchSummaryItem();
			convertToAssignBatchSummaryItem(item, deliveryItem);
			assignBatchSummary.getAssignBatchItemList().add(item);
		}
	}

	public void retrieveUnlinkedItemByTrxId(String trxIdString) {
		msgCode = null;
		assignBatchSummaryItem = null;
		Long trxId = null;

		if (trxIdString != null) {
			try {
				trxId = Long.parseLong(trxIdString.trim());
			} catch (java.lang.NumberFormatException e) {
				trxId = null;
			}
		} else {
			trxId = null;
		}

		if (trxId == null) {
			msgCode = "0005";
			return;
		}

		DeliveryItem deliveryItem = em.find(DeliveryItem.class, trxId);
		if (deliveryItem != null) {
			if (deliveryItem.getStatus() != DeliveryItemStatus.Unlink) {
				msgCode = "0348"; // Item has not been unlinked
				return;
			}
			assignBatchSummaryItem = new AssignBatchSummaryItem();
			convertToAssignBatchSummaryItem(assignBatchSummaryItem,
					deliveryItem);
		}
	}

	private void convertToAssignBatchSummaryItem(
			AssignBatchSummaryItem assignBatchSummaryItem,
			DeliveryItem deliveryItem) {

		JaxbWrapper<MpDispLabel> labelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);

		MedProfilePoItem medProfilePoItem = deliveryItem.resolveMedProfilePoItem();
		//PMS-3312

		assignBatchSummaryItem.setDeliveryItemId(deliveryItem.getId());
		assignBatchSummaryItem.setCaseNum(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getMedCase().getCaseNum());
		assignBatchSummaryItem.setMedProfileId(medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getId());

		MpDispLabel mpDispLabel = labelJaxbWrapper.unmarshall(deliveryItem.getLabelXml());
		assignBatchSummaryItem.setWardCode(mpDispLabel.getWard());
		assignBatchSummaryItem.setBedNum(mpDispLabel.getBedNum());

		Patient patient = medProfilePoItem.getMedProfileMoItem().getMedProfileItem().getMedProfile().getPatient();		

		if(StringUtils.isNotBlank(patient.getNameChi())) {
			assignBatchSummaryItem.setPatientName(patient.getNameChi());
			assignBatchSummaryItem.setHavePatientChiName(true);
		}else{
			assignBatchSummaryItem.setPatientName(patient.getName());
			assignBatchSummaryItem.setHavePatientChiName(false);
		}

		assignBatchSummaryItem.setItemCode(medProfilePoItem.getItemCode());
		assignBatchSummaryItem.setDrugName(medProfilePoItem.getDrugName());
		assignBatchSummaryItem.setIssueQty(deliveryItem.getIssueQty());
		assignBatchSummaryItem.setBaseUnit(medProfilePoItem.getBaseUnit());
		assignBatchSummaryItem.setDoseUnit(medProfilePoItem.getDoseUnit());
		assignBatchSummaryItem.setFormLabelDesc(medProfilePoItem
				.getFormLabelDesc());
		assignBatchSummaryItem.setStrength(medProfilePoItem.getStrength());
		assignBatchSummaryItem.setVolumeText(medProfilePoItem.getVolumeText());
	}
	
	public List<AssignBatchResult> assignBatchByUnlinkItemList(List<AssignBatchSummaryItem> assignBatchSummaryItemList,
											boolean printTrxRptFlag, boolean checkFlag, boolean assignBatch, String oriBatchNum, Date batchDate){
		
		Map<String, List<AssignBatchSummaryItem>> assignBatchSummaryItemMap = new HashMap<String, List<AssignBatchSummaryItem>>();
		for ( AssignBatchSummaryItem assignBatchSummaryItem : assignBatchSummaryItemList ) {
			if ( !assignBatchSummaryItemMap.containsKey(assignBatchSummaryItem.getDestination()) ) {
				assignBatchSummaryItemMap.put(assignBatchSummaryItem.getDestination(), new ArrayList<AssignBatchSummaryItem>());
			}
			assignBatchSummaryItemMap.get(assignBatchSummaryItem.getDestination()).add(assignBatchSummaryItem);
		}
		
		List<AssignBatchResult> assignBatchResultList = new ArrayList<AssignBatchResult>();
		Set<Workstore> workstoreSet = new HashSet<Workstore>();
		List<Delivery> deliveryList = new ArrayList<Delivery>();
		List<DeliveryItem> deliveryItemWithCheckedStatusList = new ArrayList<DeliveryItem>();
		for ( Map.Entry<String, List<AssignBatchSummaryItem>> entry : assignBatchSummaryItemMap.entrySet() ) {
			String wardCode = entry.getKey();
			AssignBatchResult result = assignBatch(wardCode, entry.getValue(), printTrxRptFlag, checkFlag, assignBatch, oriBatchNum, batchDate, true);
			Delivery delivery = result.getDelivery();
			workstoreSet.add(delivery.getDeliveryRequest().getWorkstore());
			deliveryList.add(delivery);
			deliveryItemWithCheckedStatusList.addAll(result.getDeliveryItemWithCheckedStatusList());
			
			AssignBatchResult assignBatchResult = new AssignBatchResult();
			assignBatchResult.setBatchDate(delivery.getBatchDate());
			assignBatchResult.setBatchNum(result.getBatchNum());
			assignBatchResult.setWardCode(result.getWardCode());
			assignBatchResultList.add(assignBatchResult);
		}
		
		List<RenderAndPrintJob> deliveryLabelPrintJobList = new ArrayList<RenderAndPrintJob>();
		for ( Delivery delivery : deliveryList ) {
			if (printTrxRptFlag) {
				mpTrxRptPrintManager.printMpTrxRptForAssignBatch(delivery);
			}

			if (checkFlag) {
				deliveryLabelPrintJobList.add(labelManager.printDeliveryLabel(delivery, false));
			}
		}
		
		if ( !deliveryLabelPrintJobList.isEmpty() ) {
			Collections.sort(deliveryLabelPrintJobList, deliveryLabelRenderAndPrintJobComparator);
			printAgent.renderAndPrint(deliveryLabelPrintJobList);
		}
		
		//Send message to corp
		if( !deliveryItemWithCheckedStatusList.isEmpty() ){
			checkManager.sendMsgToCmsFromCheckedDeliveryItemList(deliveryItemWithCheckedStatusList);
		}
		
		if( autoUpdate ){
			for ( Workstore workstore : workstoreSet ) {
				sendService.dispatchUpdateEvent(workstore);
			}
		}
		
		return assignBatchResultList;
	}
	
	public String assignBatch(String wardCode, List<AssignBatchSummaryItem> assignBatchSummaryItemList,
								boolean printTrxRptFlag, boolean checkFlag, boolean assignBatch, String oriBatchNum, Date batchDate) {
		AssignBatchResult assignBatchResult = assignBatch(wardCode, assignBatchSummaryItemList, printTrxRptFlag, checkFlag, assignBatch, oriBatchNum, batchDate, false);
		return assignBatchResult.getBatchNum();
	}

	private AssignBatchResult assignBatch(String wardCode,
			List<AssignBatchSummaryItem> assignBatchSummaryItemList,
			boolean printTrxRptFlag, boolean checkFlag, boolean assignBatch, String oriBatchNum, Date batchDate, boolean callByUnlinkItemList) {
		
		Date today = new Date();
		Delivery oldDelivery = null;
		Delivery updateDelivery = null;
		Delivery newDelivery = null;
		String newBatchNum = null;
		DeliveryRequest newDeliveryRequest = null;
		List<DeliveryItem> deliveryItemWithCheckedStatusList = new ArrayList<DeliveryItem>();
		Map<String, DeliveryRequestAlert> deliveryRequestAlertCaseNumMap = new HashMap<String, DeliveryRequestAlert>();
		Map<Long, DeliveryRequestAlert> deliveryRequestAlertMedProfileIdMap = new HashMap<Long, DeliveryRequestAlert>();
		List<DeliveryRequestAlert> deliveryRequestAlertList = new ArrayList<DeliveryRequestAlert>();
		HashMap<Long,String> aduitDeliveryItemMap = new HashMap<Long,String> ();

		String previousDeliveryStatus = "";

		// for transaction report to check update
		if(batchDate != null && oriBatchNum!= null ){
			updateDelivery = retrieveDelivery(batchDate, oriBatchNum);
			previousDeliveryStatus = updateDelivery.getStatus().getDataValue();

		}else{
			newBatchNum = batchNumGenerator.getNextFormattedBatchNum(
					workstore.getHospital(), today, BatchType.Normal);
			newDelivery = new Delivery();
			newDelivery.setBatchDate(today);

			newDelivery.setWardCode(wardCode);
			newDelivery.setPrintMode(PrintMode.Direct);
			newDelivery.setDeliveryItemList(new ArrayList<DeliveryItem>());

			previousDeliveryStatus = "A";

			if (checkFlag) {
				newDelivery.setStatus(DeliveryStatus.Checked);
				newDelivery.setCheckDate(today);
			} else {
				newDelivery.setStatus(DeliveryStatus.Printed);
			}

			newDelivery.setHospital(workstore.getHospital());

			newDeliveryRequest = new DeliveryRequest();

			newDeliveryRequest.setStatus(DeliveryRequestStatus.Completed);
			newDeliveryRequest.setSortName(LABEL_MPDISPLABEL_SORT.get()); 
			newDeliveryRequest.setDividerLabelFlag(LABEL_DIVIDERLABEL_ENABLED.get(false));
			newDeliveryRequest.setWorkstore(workstore);
			newDeliveryRequest.setDeliveryRequestAlertList(new ArrayList<DeliveryRequestAlert>());
			newDelivery.setDeliveryRequest(newDeliveryRequest);
		}

		for (AssignBatchSummaryItem item : assignBatchSummaryItemList) {
			DeliveryItem deliveryItem = em.find(DeliveryItem.class, item.getDeliveryItemId());
			String previousDisplayValue =  deliveryItem.getStatus().getDataValue(); 
			oldDelivery = deliveryItem.getDelivery();

			for (DeliveryRequestAlert deliveryRequestAlert : deliveryItem.getDelivery().getDeliveryRequest().getDeliveryRequestAlertList()) {
				
				if (  deliveryRequestAlert.getMedProfileId() != null && 
						item.getMedProfileId() != null && 
						(deliveryRequestAlert.getMedProfileId().compareTo(item.getMedProfileId()) == 0) ) 
				{
					if (deliveryRequestAlertMedProfileIdMap.containsKey(deliveryRequestAlert.getMedProfileId())) {
						DeliveryRequestAlert mapItem = deliveryRequestAlertMedProfileIdMap.get(deliveryRequestAlert.getMedProfileId());
						int result = mapItem.getDeliveryRequest().getCreateDate().compareTo(deliveryRequestAlert.getDeliveryRequest().getCreateDate());
						if (result < 0) { 	// mapItem.getDeliveryRequest().getCreateDate() < deliveryRequestAlert.getDeliveryRequest().getCreateDate()
							deliveryRequestAlertMedProfileIdMap.remove(deliveryRequestAlert.getMedProfileId());
						} else {
							continue;
						}
					}
					deliveryRequestAlertMedProfileIdMap.put(deliveryRequestAlert.getMedProfileId(),deliveryRequestAlert);
				} 
				else if ( deliveryRequestAlert.getCaseNum() != null && 
							item.getCaseNum() != null && 
						StringUtils.equals(deliveryRequestAlert.getCaseNum(), item.getCaseNum())) 
				{
					if (deliveryRequestAlertCaseNumMap.containsKey(deliveryRequestAlert.getCaseNum())) {
						DeliveryRequestAlert mapItem = deliveryRequestAlertCaseNumMap.get(deliveryRequestAlert.getCaseNum());
						int result = mapItem.getDeliveryRequest().getCreateDate().compareTo(deliveryRequestAlert.getDeliveryRequest().getCreateDate());
						if (result < 0) { 	// mapItem.getDeliveryRequest().getCreateDate() < deliveryRequestAlert.getDeliveryRequest().getCreateDate()
							deliveryRequestAlertCaseNumMap.remove(deliveryRequestAlert.getCaseNum());
						} else {
							continue;
						}
					}
					deliveryRequestAlertCaseNumMap.put(deliveryRequestAlert.getCaseNum(),deliveryRequestAlert);
				} else if ( deliveryRequestAlert.getMedProfileId() == null && 
						item.getMedProfileId() == null && 
						deliveryRequestAlert.getCaseNum() == null && 
						item.getCaseNum() == null ) 
				{
					if ( !deliveryRequestAlertList.isEmpty() ) {
						DeliveryRequestAlert mapItem = deliveryRequestAlertList.get(0);
						int result = mapItem.getDeliveryRequest().getCreateDate().compareTo(deliveryRequestAlert.getDeliveryRequest().getCreateDate());
						if (result < 0) { 	// mapItem.getDeliveryRequest().getCreateDate() < deliveryRequestAlert.getDeliveryRequest().getCreateDate()
							deliveryRequestAlertList.remove(mapItem);
						} else {
							continue;
						}
					}
					deliveryRequestAlertList.add(deliveryRequestAlert);
				}				
			}
			
			if (checkFlag && !assignBatch) {


				if(oriBatchNum == null){
					//new batch num
					deliveryItem.setStatus(DeliveryItemStatus.Checked);
					deliveryItemWithCheckedStatusList.add(deliveryItem);
					newDelivery.setBatchNum(newBatchNum);
					newDelivery.getDeliveryItemList().add(deliveryItem);
					deliveryItem.setDelivery(newDelivery);

				}else{


					//Use Old Delivery 
					deliveryItem.setStatus(DeliveryItemStatus.Checked);
					updateDelivery.setCheckDate(today);
					updateDelivery.setBatchNum(oriBatchNum);
					updateDelivery.setStatus(DeliveryStatus.Checked);
					deliveryItem.setDelivery(updateDelivery);
					boolean checkSameDeliveryFlag = false;
					for (DeliveryItem updateDeliveryItem: updateDelivery.getDeliveryItemList()){
						if(updateDeliveryItem.getId().equals(deliveryItem.getId()) ){
							checkSameDeliveryFlag = true;
							break;
						}

					}
					if(!checkSameDeliveryFlag){
						updateDelivery.getDeliveryItemList().add(deliveryItem);
					}

					updateDelivery.setModifyDate(today);
					deliveryItem.setDelivery(updateDelivery);
					deliveryItemWithCheckedStatusList.add(deliveryItem);
				}


			} else if (!checkFlag && assignBatch) {


				if(oriBatchNum == null ){
					//new batch num
					newDelivery.setBatchNum(newBatchNum);
					newDelivery.setStatus(DeliveryStatus.Assigning);
					deliveryItem.setDelivery(newDelivery);
					newDelivery.getDeliveryItemList().add(deliveryItem);


				}else{
					//Use Old Delivery 
					updateDelivery.setBatchNum(oriBatchNum);
					updateDelivery.setStatus(DeliveryStatus.Assigning);

					boolean checkSameDeliveryFlag = false;
					for (DeliveryItem updateDeliveryItem: updateDelivery.getDeliveryItemList()){
						if(updateDeliveryItem.getId().equals(deliveryItem.getId()) ){
							checkSameDeliveryFlag = true;
							break;
						}

					}
					if(!checkSameDeliveryFlag){
						updateDelivery.getDeliveryItemList().add(deliveryItem);
					}
					updateDelivery.setModifyDate(new Date());
					deliveryItem.setDelivery(updateDelivery);
				}
				deliveryItem.setStatus(DeliveryItemStatus.Printed);


			} else if (checkFlag && assignBatch) {
				//new batch num
				deliveryItem.setStatus(DeliveryItemStatus.Checked);
				deliveryItemWithCheckedStatusList.add(deliveryItem);
				newDelivery.setBatchNum(newBatchNum);
				newDelivery.getDeliveryItemList().add(deliveryItem);
				deliveryItem.setDelivery(newDelivery);
			}


			// for transaction report to check update
			if(oldDelivery != null){
				oldDelivery.setModifyDate(new Date());
				em.merge(oldDelivery);
			}


			aduitDeliveryItemMap.put(deliveryItem.getId(), previousDisplayValue+"=>"+deliveryItem.getStatus().getDataValue());

		}
		
		deliveryRequestAlertList.addAll(deliveryRequestAlertMedProfileIdMap.values());
		deliveryRequestAlertList.addAll(deliveryRequestAlertCaseNumMap.values());

		if(newDelivery != null){
			for ( DeliveryRequestAlert deliveryRequestAlert : deliveryRequestAlertList ){
				try {
					DeliveryRequestAlert newDeliveryRequestAlert;
					newDeliveryRequestAlert = (DeliveryRequestAlert) BeanUtils.cloneBean(deliveryRequestAlert);
					newDeliveryRequestAlert.setId(null);
					newDeliveryRequestAlert.setDeliveryRequest(newDeliveryRequest);

					em.persist(newDeliveryRequestAlert);

					newDeliveryRequest.addDeliveryRequestAlert(newDeliveryRequestAlert);
				} catch (IllegalAccessException e) {
					logger.debug("IllegalAccessException: #0", e);
				} catch (InstantiationException e) {
					logger.debug("InstantiationException: #0", e);
				} catch (InvocationTargetException e) {
					logger.debug("InvocationTargetException: #0", e);
				} catch (NoSuchMethodException e) {
					logger.debug("NoSuchMethodException: #0", e);
				}
			}
		}else if (updateDelivery != null){

			for ( DeliveryRequestAlert deliveryRequestAlert : deliveryRequestAlertList ){
				try {
					Boolean checkSameFlag = false;

					for (DeliveryRequestAlert dra :updateDelivery.getDeliveryRequest().getDeliveryRequestAlertList()){
						if (  deliveryRequestAlert.getMedProfileId() != null && 
								dra.getMedProfileId() != null && 
								(deliveryRequestAlert.getMedProfileId().compareTo(dra.getMedProfileId()) == 0) ) 
						{
							checkSameFlag = true;
							break;
							
						} else if( deliveryRequestAlert.getCaseNum() != null && 
									dra.getCaseNum() != null && 
									dra.getCaseNum().equals(deliveryRequestAlert.getCaseNum())){
							checkSameFlag = true;
							break;
						} else if ( deliveryRequestAlert.getMedProfileId() == null && 
										dra.getMedProfileId() == null && 
										deliveryRequestAlert.getCaseNum() == null && 
										dra.getCaseNum() == null )
						{
							checkSameFlag = true;
							break;
						}
					}
					if(!checkSameFlag){
						DeliveryRequestAlert newDeliveryRequestAlert;
						newDeliveryRequestAlert = (DeliveryRequestAlert) BeanUtils.cloneBean(deliveryRequestAlert);
						newDeliveryRequestAlert.setId(null);
						newDeliveryRequestAlert.setDeliveryRequest(updateDelivery.getDeliveryRequest());
						em.persist(newDeliveryRequestAlert);
						updateDelivery.getDeliveryRequest().addDeliveryRequestAlert(newDeliveryRequestAlert);
					}

				} catch (IllegalAccessException e) {
					logger.debug("IllegalAccessException: #0", e);
				} catch (InstantiationException e) {
					logger.debug("InstantiationException: #0", e);
				} catch (InvocationTargetException e) {
					logger.debug("InvocationTargetException: #0", e);
				} catch (NoSuchMethodException e) {
					logger.debug("NoSuchMethodException: #0", e);
				}
			}

		}


		// for transaction report to check update
		if (!checkFlag && assignBatch) {
			//Only Assign not Check 
			if(oriBatchNum == null ){
				//New Num Assign
				em.persist(newDelivery);
			}else{
				em.merge(updateDelivery);

			}

		}else if (checkFlag && assignBatch) {
			//new Assign and Check
			em.persist(newDelivery);

		} if (checkFlag && !assignBatch){
			//Only checkFlag not Assign 
			if(oriBatchNum == null ){
				//New Num Assign
				em.persist(newDelivery);
			}else{
				em.merge(updateDelivery);
			}
		}
		if(newDelivery != null){
			em.persist(newDeliveryRequest);
		}else if (updateDelivery != null){
			em.merge(updateDelivery.getDeliveryRequest());
		}

		em.flush();
		
		if ( !callByUnlinkItemList ) {
			if(newDelivery != null){
				printTrxReportAndDeliverLabel( printTrxRptFlag,  checkFlag, newDelivery);

			}else if (updateDelivery != null){

				printTrxReportAndDeliverLabel( printTrxRptFlag,  checkFlag, updateDelivery);

			}
			
			//Send message to corp
			if(deliveryItemWithCheckedStatusList != null && deliveryItemWithCheckedStatusList.size() > 0){
				checkManager.sendMsgToCmsFromCheckedDeliveryItemList(deliveryItemWithCheckedStatusList);
			}
		}

		Delivery auditUsedDelivery = new Delivery();
		if(oriBatchNum == null ){
			auditUsedDelivery = newDelivery;
		}else{
			auditUsedDelivery = updateDelivery;

		}

		if(checkFlag){

			if( !callByUnlinkItemList && autoUpdate ){
				sendService.dispatchUpdateEvent(auditUsedDelivery.getDeliveryRequest().getWorkstore());
			}

			auditLogger.log("#0679", 
					df.format(auditUsedDelivery.getBatchDate())+auditUsedDelivery.getBatchNum()+"="+previousDeliveryStatus+"=>"+auditUsedDelivery.getStatus().getDataValue(), aduitDeliveryItemMap,printTrxRptFlag,auditUsedDelivery.getWardCode());

		}else{
			auditLogger.log("#0678", 
					df.format(auditUsedDelivery.getBatchDate())+auditUsedDelivery.getBatchNum()+"="+previousDeliveryStatus+"=>"+auditUsedDelivery.getStatus().getDataValue(), aduitDeliveryItemMap,printTrxRptFlag,auditUsedDelivery.getWardCode());

		}

		
		AssignBatchResult assignBatchResult = new AssignBatchResult();
		if(newDelivery != null){
			assignBatchResult.setDelivery(newDelivery);
			assignBatchResult.setBatchNum(newDelivery.getBatchNum());
		} else {
			assignBatchResult.setDelivery(updateDelivery);
			assignBatchResult.setBatchNum(updateDelivery.getBatchNum());
		}
		assignBatchResult.setDeliveryItemWithCheckedStatusList(deliveryItemWithCheckedStatusList);
		assignBatchResult.setWardCode(wardCode);

		return assignBatchResult;
	}

	public void printTrxReportAndDeliverLabel(boolean printTrxRptFlag, boolean checkFlag, Delivery delivery){

		if (printTrxRptFlag) {
			mpTrxRptPrintManager.printMpTrxRptForAssignBatch(delivery);
		}

		if (checkFlag) {
			printAgent.renderAndPrint(labelManager.printDeliveryLabel(delivery, false));
		}
	}
	
    private static class DeliveryLabelRenderAndPrintJobComparator implements Comparator<RenderAndPrintJob>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(RenderAndPrintJob o1, RenderAndPrintJob o2) {
			DeliveryLabel label1 = resolveDeliveryLabel(o1);
			DeliveryLabel label2 = resolveDeliveryLabel(o2);
			
			if (label1 == label2) {
				return 0;
			} else if (label1 == null) {
				return -1;
			} else if (label2 == null) {
				return 1;
			} else {
				return label1.getBatchNum().compareTo(label2.getBatchNum());
			}
		}
	    
	    private DeliveryLabel resolveDeliveryLabel(RenderAndPrintJob renderAndPrintJob) {
			
			Object obj = MedProfileUtils.getFirstItem(renderAndPrintJob.getObjectList());
			return obj instanceof DeliveryLabel ? (DeliveryLabel) obj : null; 
		}
   }

	public String getMsgCode() {
		return msgCode;
	}

	@Remove
	public void destroy() {
		if (assignBatchSummaryItem != null) {
			assignBatchSummaryItem = null;
		}
		if (assignBatchSummary != null) {
			assignBatchSummary = null;
		}
	}
}
