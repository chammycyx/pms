<?xml version="1.0" encoding="utf-8"?>
<s:VGroup xmlns:fx="http://ns.adobe.com/mxml/2009" 
		  xmlns:s="library://ns.adobe.com/flex/spark" 
		  xmlns:mx="library://ns.adobe.com/flex/mx"
		  xmlns="hk.org.ha.view.pms.main.vetting.mp.*"
		  gap="0"
		  xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*">
	<fx:Declarations>
		<!-- Place non-visual elements (e.g., services, value objects) here -->
		<fx:Component id="replenishmentRenderer">
			<ReplenishmentRenderer sysMsgMap="{outerDocument.sysMsgMap}" propMap="{outerDocument.propMap}" readOnly="{outerDocument.readOnly}" privateFlag="{outerDocument.privateFlag}"
								   activeListDictionary="{outerDocument.activeListDictionary}" 
								   showSfiPurchaseRequestForReplenishment="{outerDocument.showSfiPurchaseRequestForReplenishment}"
								   clearPurchaseRequestForReplenishment="{outerDocument.clearPurchaseRequestForReplenishment}"/>
		</fx:Component>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[			
			import mx.collections.ArrayCollection;
			
			import spark.events.IndexChangeEvent;
			
			import hk.org.ha.fmk.pms.flex.components.core.Toolbar;
			import hk.org.ha.fmk.pms.flex.utils.TimeUtil;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
			import hk.org.ha.model.pms.persistence.medprofile.Replenishment;
			import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
			import hk.org.ha.model.pms.udt.medprofile.ReplenishmentStatus;
			import hk.org.ha.model.pms.vo.medprofile.ReplenishmentRequestUiInfo;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			[Bindable]
			public var numOfPurchaseItem:Number;
			[Bindable]
			public var replenishmentList:ArrayCollection;			
			[Bindable]
			public var numOfOutstanding:Number;
			[Bindable]
			public var numOfItemToDo:Number;
			[Bindable]
			public var numOfChemoItemToDo:Number;
			[Bindable]
			public var toolbar:Toolbar;
			[Bindable]
			public var propMap:PropMap;
			[Bindable]
			public var sysMsgMap:SysMsgMap;
			[Bindable]
			public var readOnly:Boolean;
			[Bindable]
			public var privateFlag:Boolean;
			[Bindable]
			public var activeListDictionary:Dictionary;
			[Bindable]
			public var showSfiPurchaseRequestForReplenishment:Function;
			[Bindable]
			public var clearPurchaseRequestForReplenishment:Function;
			
			public var checkAllReplenishmentItemIssueQty:Function;				
			
			public function onShow():void
			{
				tabBar.resetToDefaultTab();
			}
			
			protected function itemRendererFunction(data:Object):IFactory {
				return replenishmentRenderer;
			}
					

			public function get isResetEnabled():Boolean
			{				
				var currentDate:Date = TimeUtil.getCurrentServerTime();
				var replenishment:Replenishment	= list.selectedItem as Replenishment;	
				var mpiId:Number = replenishment.medProfileMoItem.medProfileItem.id;
				
				if (currentDate.date != replenishment.updateDate.date
					|| currentDate.month != replenishment.updateDate.month
					|| currentDate.fullYear != replenishment.updateDate.fullYear) {
					return false;
				}
				if (!activeListDictionary[mpiId]) {
					return false;
				}				
				if (replenishment.status != ReplenishmentStatus.Processed) {
					return false;
				} 
				if (MedProfileItem(activeListDictionary[mpiId]).modified) {
					return false;
				}
				if (replenishment.medProfileMoItem.medProfileItem.status != MedProfileItemStatus.Verified) {
					return false;
				}
				if (MedProfileItem(activeListDictionary[mpiId]).medProfileMoItem.itemNum != replenishment.medProfileMoItem.itemNum) {
					return false;
				}
				
				return true;
			}

			protected function listChangeHandler(event:IndexChangeEvent):void
			{
				toolbar.resetButton.enabled = isResetEnabled;
			}

		]]>
	</fx:Script>
		
	<s:states>
		<s:State name="ToDoList"/>
		<s:State name="EditItem"/>
		<s:State name="ReplenishmentRequest"/>
	</s:states>
	
	<VettingMainTabBar id="tabBar" propMap="{propMap}" defaultTab="{tabBar.replenishmentRequestTab}"
					   toDoListCount="{numOfItemToDo}" replenishmentRequestCount="{numOfOutstanding}" purchaseRequestCount="{numOfPurchaseItem}" chemoCount="{numOfChemoItemToDo}"/>
	
	<s:BorderContainer width="100%" height="100%">
		<s:layout>
			<s:VerticalLayout gap="0"/>
		</s:layout>	
		<fc:ExtendedDataGrid id="replenishGrid" width="100%" height="0%" borderStyle="none" wordWrap="true" 
							 sortableColumns="false" resizableColumns="false" draggableColumns="false">
			<fc:columns>
				<mx:DataGridColumn width="@{ReplenishmentRequestUiInfo.dispColWidth}"			   headerText="Disp."/>
				<mx:DataGridColumn width="@{ReplenishmentRequestUiInfo.drugColWidth}" 			   headerText="Drug"/>
				<mx:DataGridColumn width="@{ReplenishmentRequestUiInfo.issueQtyColWidth}" headerText="No. of Dose / &#10;Issue Qty"/>
				<mx:DataGridColumn width="@{ReplenishmentRequestUiInfo.reasonColWidth}"			   headerText="Reason"/>
				<mx:DataGridColumn width="@{ReplenishmentRequestUiInfo.requestDateColWidth}" 	   headerText="Request&#10;Date / Time"/>
				<mx:DataGridColumn width="@{ReplenishmentRequestUiInfo.statusColWidth}" 		   headerText="Status"/>
			</fc:columns>
		</fc:ExtendedDataGrid>		
		<s:List id="list" width="100%" height="100%" styleName="toDoListStyle"				 
				useVirtualLayout="false" hasFocusableChildren="true"
				doubleClickEnabled="false"				 
				dataProvider="{replenishmentList}"
				change="listChangeHandler(event)"
				itemRendererFunction="{itemRendererFunction}"/>		
	</s:BorderContainer>
</s:VGroup>
