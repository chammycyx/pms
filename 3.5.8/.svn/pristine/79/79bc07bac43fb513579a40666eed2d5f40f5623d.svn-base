<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="630" height="400">
	
	<fx:Metadata>
		[Name("operationModeMaintActiveProfileConfirmationPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.reftable.RetrievePmsSessionInfoListEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.udt.reftable.OperationWorkstationType;
			import hk.org.ha.model.pms.vo.reftable.PmsSessionInfo;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			
			[In][Bindable]
			public var  pmsSessionInfoList:ArrayCollection;
			
			[Bindable]
			private var activeProfileName:String;
			
			private var pmsSessionInfo:PmsSessionInfo;
			
			[Bindable]
			private var yesHandler:Function;
			
			public function initPopup(inActiveProfileName:String, inYesFunc:Function):void{
				dispatchEvent(new RetrievePmsSessionInfoListEvent(this));
				yesHandler = inYesFunc;
				activeProfileName = inActiveProfileName;
				noButton.enabled = false;
				yesButton.enabled = false;
			}
			
			private function yesFunc(evt:MouseEvent):void {
				yesHandler(evt);
				ctx.pmsSessionInfoList = null;
			}
			 
			private function noFunc(evt:MouseEvent):void {
				PopUpManager.removePopUp(this);
				ctx.pmsSessionInfoList = null;
			}
			
			public function refreshPopup():void {
				yesButton.enabled = true;
				noButton.enabled = true;
				if ( pmsSessionInfoList.length == 0 ) {
					alertLabel.visible = true;
					alertLabel.includeInLayout = true;
				} 
				
				callLater(yesButton.setFocus);
				
			}
			
			private function showSystemMessage(errorCode:String, okHandler:Function):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				msgProp.okHandler = okHandler;
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function workstationTypeLabelFunction(item:PmsSessionInfo, column:DataGridColumn):String{
				return item.workstationType.displayValue;
			}
			
			
		]]>
	</fx:Script>	
	
	<s:Panel width="100%" height="100%" title="Active Operation Profile">
		<s:VGroup width="100%" height="100%" paddingLeft="10" paddingTop="10" gap="10" paddingRight="10" paddingBottom="10">
			<fc:ExtendedDataGrid id="activeWorkstationGrid"
								 dataProvider="{pmsSessionInfoList}"
								 height="75%" width="100%" 
								 horizontalScrollPolicy="auto" 
								 draggableColumns="false" 
								 sortableColumns="false"
								 resizableColumns="false" 
								 doubleClickEnabled="true" >
				<fc:columns>
					<mx:DataGridColumn headerText="Active Workstation" dataField="workstationCode" width="{activeWorkstationGrid.width*0.2}"/>
					<mx:DataGridColumn headerText="Computer Name" dataField="hostName" width="{activeWorkstationGrid.width*0.35}"/>
					<mx:DataGridColumn headerText="User ID" dataField="userId" width="{activeWorkstationGrid.width*0.2}"/>
					<mx:DataGridColumn headerText="Station Type" dataField="workstationType" labelFunction="workstationTypeLabelFunction"/>
				</fc:columns>
			</fc:ExtendedDataGrid>
			<s:HGroup id="alertLabel" width="100%" height="5%" visible="false" includeInLayout="false">
				<s:Label text="Failed to retrieve workstations status."
						 width="100%" height="100%" styleName="activeProfileConfirmAlertLabelStyle"/>
			</s:HGroup>
			<s:HGroup width="100%" height="15%">
				<s:Label text="{'ALL workstations will be forcibly logged off upon activating the new profile.  Do you want to activate the operation profile ['+activeProfileName+']?'}"
						 width="100%" height="100%" styleName="activeProfileConfirmLabelStyle"/>
			</s:HGroup>
			<s:HGroup width="100%" height="10%" horizontalAlign="right">
				<fc:ExtendedButton id="yesButton" label="Yes" click="yesFunc(event)"/>
				<fc:ExtendedButton id="noButton" label="No" click="noFunc(event)"/>
			</s:HGroup>
		</s:VGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>