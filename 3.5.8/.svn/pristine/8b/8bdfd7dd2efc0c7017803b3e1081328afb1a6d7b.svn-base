/*
  GRANITE DATA SERVICES
  Copyright (C) 2011 GRANITE DATA SERVICES S.A.S.

  This file is part of Granite Data Services.

  Granite Data Services is free software; you can redistribute it and/or modify
  it under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  Granite Data Services is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

package hk.org.ha.model.pms.exception;

import java.util.Map;

import java.sql.SQLSyntaxErrorException;

import org.granite.messaging.service.ExceptionConverter;
import org.granite.messaging.service.ServiceException;


public class DatabaseExceptionConverter implements ExceptionConverter {
    	
    public static final String NO_PRIVILEGES = "Database.NoPrivileges";

    public boolean accepts(Throwable t, Throwable finalException) {    	
        return t.getClass().equals(SQLSyntaxErrorException.class) && t.getMessage().contains("ORA-01031");
    }

    public ServiceException convert(Throwable t, String detail, Map<String, Object> extendedData) {
    	ServiceException se = new ServiceException(NO_PRIVILEGES, t.getMessage(), detail, t);
        se.getExtendedData().putAll(extendedData);
        return se;
    }
}
