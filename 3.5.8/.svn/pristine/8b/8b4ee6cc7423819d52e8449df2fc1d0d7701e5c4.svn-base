<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RefillWorkloadForecastRpt" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="812" leftMargin="15" rightMargin="15" topMargin="15" bottomMargin="15">
	<property name="ireport.zoom" value="1.4641000000000013"/>
	<property name="ireport.x" value="644"/>
	<property name="ireport.y" value="0"/>
	<parameter name="hospCode" class="java.lang.String"/>
	<parameter name="workstoreCode" class="java.lang.String"/>
	<parameter name="startDate" class="java.util.Date"/>
	<parameter name="endDate" class="java.util.Date"/>
	<field name="itemCode" class="java.lang.String"/>
	<field name="fullDrugDesc" class="java.lang.String"/>
	<field name="totalRefillQtyCount" class="java.lang.String"/>
	<field name="baseUnit" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="49" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="635" height="20"/>
				<textElement>
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Forecast for Refill Quantity (" + $P{hospCode} +" - "+ $P{workstoreCode} + ")"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="215" y="20" width="195" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Filter by: Not Yet Dispensed]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="100" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Refill Due Date from]]></text>
			</staticText>
			<staticText>
				<reportElement x="129" y="20" width="12" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[to]]></text>
			</staticText>
			<textField pattern="dd-MMM-yyyy">
				<reportElement x="140" y="20" width="57" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$P{endDate}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy">
				<reportElement x="79" y="20" width="60" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$P{startDate}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="56" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="8" width="100" height="12"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Item Code]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="8" width="435" height="12"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Item Description]]></text>
			</staticText>
			<staticText>
				<reportElement x="535" y="9" width="100" height="12"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Total Refill Qty]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="20" width="812" height="1"/>
			</line>
			<textField pattern="dd-MMM-yyyy">
				<reportElement x="0" y="21" width="100" height="13"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{itemCode}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="100" y="20" width="435" height="13"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{fullDrugDesc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="535" y="22" width="149" height="12"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{totalRefillQtyCount}+" " + $F{baseUnit}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="397" y="36" width="20" height="13"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[End]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="34" width="812" height="1"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="15" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="663" y="0" width="132" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="798" y="0" width="28" height="15"/>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="&apos;Printed on&apos; dd-MMM-yyyy &apos;at&apos; HH:mm ">
				<reportElement x="0" y="0" width="220" height="15"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
