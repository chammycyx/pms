package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.biz.printing.PrinterSelectorLocal;
import hk.org.ha.model.pms.biz.validation.SearchTextValidatorLocal;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.disp.CapdVoucherItem;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.reftable.OperationProfile;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.reftable.PrintDocType;
import hk.org.ha.model.pms.udt.report.PrescRptStatus;
import hk.org.ha.model.pms.udt.validation.SearchTextType;
import hk.org.ha.model.pms.udt.vetting.CapdVoucherStatus;
import hk.org.ha.model.pms.vo.printing.PrintOption;
import hk.org.ha.model.pms.vo.printing.RenderAndPrintJob;
import hk.org.ha.model.pms.vo.printing.RenderJob;
import hk.org.ha.model.pms.vo.report.CapdVoucherDtl;
import hk.org.ha.model.pms.vo.report.CapdVoucherRpt;
import hk.org.ha.model.pms.vo.report.CapdVoucherRptDtl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.joda.time.DateTime;
@Stateful
@Scope(ScopeType.SESSION)
@Name("capdVoucherRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CapdVoucherRptServiceBean implements CapdVoucherRptServiceLocal {
	@PersistenceContext
	private EntityManager em;
	
	@In
	private PrinterSelectorLocal printerSelector;
	
	@In
	private PrintAgentInf printAgent;
	
	@In 
	private Workstore workstore;

	private List<CapdVoucherRpt> capdVoucherRptList;
	
	private boolean retrieveSuccess;
	
	private boolean validHkid;
	
    private Map<String, Object> parameters = new HashMap<String, Object>();
    
    @In
	private SearchTextValidatorLocal searchTextValidator;
	
	@SuppressWarnings("unchecked")
	public List<CapdVoucherRpt>  retrieveCapdVoucherRptList(Date dateFrom, Date dateTo, String hkid, String patName, String voucherNum) {
		List<CapdVoucherItem> capdVoucherItemList = new ArrayList<CapdVoucherItem>();
		capdVoucherRptList = new ArrayList<CapdVoucherRpt>();

		DateTime dt = new DateTime(dateTo);
		Date exactDateTo = dt.plusDays(1).toDate();
		validHkid = true;
		
		if(StringUtils.isNotBlank(hkid)){
			if(!searchTextValidator.checkSearchInputType(hkid).equals(SearchTextType.Hkid)){
				validHkid = false;
			}

			capdVoucherItemList = em.createQuery(
					"select o from CapdVoucherItem o" + // 20120305 index check : PharmOrder.hkid : I_PHARM_ORDER_01
					" where o.capdVoucher.dispOrder.pharmOrder.hkid = :hkid" +
					" and o.capdVoucher.dispOrder.workstore = :workstore" +
					" and o.capdVoucher.createDate between :dateFrom and :dateTo"+
					" and o.capdVoucher.status  in :capdStatus" +
					" and (o.capdVoucher.dispOrder.status not in :status" +
					" or (o.capdVoucher.dispOrder.status =:statusDelete" +
					" or (o.capdVoucher.dispOrder.status =:statusSysDelete and o.dispOrderItem.status in :doiStatus) ) )" +
					" order by o.capdVoucher.voucherNum, o.dispOrderItem.pharmOrderItem.itemNum")
			.setParameter("hkid", hkid)
			.setParameter("workstore", workstore)
			.setParameter("dateFrom", dateFrom, TemporalType.DATE)
			.setParameter("dateTo", exactDateTo, TemporalType.DATE)
			.setParameter("capdStatus", CapdVoucherStatus.Issued_Suspended)
			.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
		    .setParameter("statusDelete", DispOrderStatus.Deleted)
		    .setParameter("statusSysDelete", DispOrderStatus.SysDeleted)
		    .setParameter("doiStatus", DispOrderItemStatus.KeepRecord_Deleted)
			.getResultList();

			removeDuplicateVoucher(capdVoucherItemList);
		}
		
		if(StringUtils.isNotBlank(patName)){
			capdVoucherItemList = em.createQuery(
					"select o from CapdVoucherItem o" + // 20120305 index check : PharmOrder.name : I_PHARM_ORDER_02
					" where o.capdVoucher.dispOrder.pharmOrder.name = :patName" +
					" and o.capdVoucher.dispOrder.workstore = :workstore" +
					" and o.capdVoucher.createDate between :dateFrom and :dateTo"+
					" and o.capdVoucher.status  in :capdStatus" +
					" and (o.capdVoucher.dispOrder.status not in :status" +
					" or (o.capdVoucher.dispOrder.status =:statusDelete" +
					" or (o.capdVoucher.dispOrder.status =:statusSysDelete and o.dispOrderItem.status in :doiStatus) ) )" +
					" order by o.capdVoucher.voucherNum, o.dispOrderItem.pharmOrderItem.itemNum")
			.setParameter("patName", patName)
			.setParameter("workstore", workstore)
			.setParameter("dateFrom", dateFrom, TemporalType.DATE)
			.setParameter("dateTo", exactDateTo, TemporalType.DATE)
			.setParameter("capdStatus", CapdVoucherStatus.Issued_Suspended)
			.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
		    .setParameter("statusDelete", DispOrderStatus.Deleted)
		    .setParameter("statusSysDelete", DispOrderStatus.SysDeleted)
		    .setParameter("doiStatus", DispOrderItemStatus.KeepRecord_Deleted)
			.getResultList();

			removeDuplicateVoucher(capdVoucherItemList);
		}
		
		if(StringUtils.isNotBlank(voucherNum)){
			capdVoucherItemList = em.createQuery(
					"select o from CapdVoucherItem o" + // 20120305 index check : CapdVoucher.voucherNum : I_CAPD_VOUCHER_01
					" where o.capdVoucher.voucherNum = :voucherNum" +
					" and o.capdVoucher.dispOrder.workstore = :workstore" +
					" and o.capdVoucher.createDate between :dateFrom and :dateTo"+
					" and o.capdVoucher.status  in :capdStatus" +
					" and (o.capdVoucher.dispOrder.status not in :status" +
					" or (o.capdVoucher.dispOrder.status =:statusDelete" +
					" or (o.capdVoucher.dispOrder.status =:statusSysDelete and o.dispOrderItem.status in :doiStatus) ) )" +
					" order by o.capdVoucher.voucherNum, o.dispOrderItem.pharmOrderItem.itemNum")
			.setParameter("voucherNum", voucherNum)
			.setParameter("workstore", workstore)
			.setParameter("dateFrom", dateFrom, TemporalType.DATE)
			.setParameter("dateTo", exactDateTo, TemporalType.DATE)
			.setParameter("capdStatus", CapdVoucherStatus.Issued_Suspended)
			.setParameter("status", DispOrderStatus.Deleted_SysDeleted)
		    .setParameter("statusDelete", DispOrderStatus.Deleted)
		    .setParameter("statusSysDelete", DispOrderStatus.SysDeleted)
		    .setParameter("doiStatus", DispOrderItemStatus.KeepRecord_Deleted)
			.getResultList();
			
			removeDuplicateVoucher(capdVoucherItemList);
		}

		constructCapdVoucherRptList(capdVoucherItemList);

		if ( capdVoucherItemList.size()>0) {
			retrieveSuccess = true;
		}
		else{
			retrieveSuccess = false;
		}

		return capdVoucherRptList;
		
	}
	
	//remove duplicate: (dispOrderItem status = K and dispOrder status = X) / (dispOrderItem status = K and dispOrder status = I)
	private void removeDuplicateVoucher(List<CapdVoucherItem> capdVoucherItemList) { 
		Map<String, CapdVoucherItem> capdVoucherItemMap = new HashMap<String, CapdVoucherItem>();
		List<CapdVoucherItem> delCapdVoucherItemList = new ArrayList<CapdVoucherItem>();
		
		for ( CapdVoucherItem capdVoucherItem : capdVoucherItemList ) {
			if (capdVoucherItemMap.get(capdVoucherItem.getCapdVoucher().getVoucherNum()+capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode()) != null ) {
				CapdVoucherItem item = capdVoucherItemMap.get(capdVoucherItem.getCapdVoucher().getVoucherNum()+capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode());
				
				if (  item.getUpdateDate().compareTo(capdVoucherItem.getUpdateDate()) < 0 ) { 
					capdVoucherItemMap.put(capdVoucherItem.getCapdVoucher().getVoucherNum()+capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode(), capdVoucherItem);
					delCapdVoucherItemList.add(item);
				} else {
					delCapdVoucherItemList.add(capdVoucherItem);
				}
			} else {
				capdVoucherItemMap.put(capdVoucherItem.getCapdVoucher().getVoucherNum()+capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode(), capdVoucherItem);
			}
		}
		
		for ( CapdVoucherItem delItem : delCapdVoucherItemList ) {
			capdVoucherItemList.remove(delItem);
		}
	}
	
	private void constructCapdVoucherRptList(List<CapdVoucherItem> capdVoucherItemList){
		Map<CapdVoucherRptDtl, CapdVoucherRptDtl> capdVoucherRptMap = new TreeMap<CapdVoucherRptDtl, CapdVoucherRptDtl>();
		Map<String, String> voucherNumMap = new HashMap<String, String>();
		
		Integer issueCount = 0;
		Integer issueItemCount = 0;
		Integer suspendCount = 0;
		Integer suspendItemCount = 0;
		Integer deleteCount = 0;
		Integer deleteItemCount = 0;
		
		
		for (CapdVoucherItem capdVoucherItem : capdVoucherItemList) 
		{
			CapdVoucherRptDtl key = null;
			
			if ( capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedCase() != null ) {
				key = new CapdVoucherRptDtl(
						capdVoucherItem.getCapdVoucher().getDispOrder().getDispDate(),
						capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedCase().getCaseNum(),
						capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedOrder().getPatient().getName(),
						capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedOrder().getPatient().getHkid());	
			} else {
				key = new CapdVoucherRptDtl(
						capdVoucherItem.getCapdVoucher().getDispOrder().getDispDate(),
						"",
						capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedOrder().getPatient().getName(),
						capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedOrder().getPatient().getHkid());	
			}
			CapdVoucherRptDtl value = capdVoucherRptMap.get(key);
			if (value == null) {
				value = key;
				value.setPatNameChi(capdVoucherItem.getCapdVoucher().getDispOrder().getPharmOrder().getMedOrder().getPatient().getNameChi());
				capdVoucherRptMap.put(key, value);
			}

			CapdVoucherDtl capdVoucherDtl = new CapdVoucherDtl();
			
			capdVoucherDtl.setVoucherNum(capdVoucherItem.getCapdVoucher().getVoucherNum());
			capdVoucherDtl.setItemCode(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode());
			capdVoucherItem.getDispOrderItem().getPharmOrderItem().loadDmInfo();
			DmDrug dmDrug = DmDrugCacher.instance().getDmDrug(capdVoucherItem.getDispOrderItem().getPharmOrderItem().getItemCode());
			capdVoucherDtl.setDrugName(capdVoucherItem.getDrugDesc());
			capdVoucherDtl.setBaseUnit(dmDrug.getBaseUnit());
			capdVoucherDtl.setDispQty(capdVoucherItem.getQty());			
			
			boolean isSuspended = capdVoucherItem.getDispOrderItem().getDispOrder().getAdminStatus().equals(DispOrderAdminStatus.Suspended);
			
			DispOrderItem dispOrderItem = capdVoucherItem.getDispOrderItem();
			if (dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.SysDeleted || dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted ) {
				capdVoucherDtl.setStatus(CapdVoucherStatus.Deleted.toString());
				deleteItemCount++;
				
			}else if ( isSuspended ) {//Suspended
				capdVoucherDtl.setStatus( PrescRptStatus.Suspended.getDisplayValue());
				suspendItemCount++;
				
			} else if (capdVoucherItem.getCapdVoucher().getStatus().equals(CapdVoucherStatus.Issued) || capdVoucherItem.getCapdVoucher().getStatus().equals(CapdVoucherStatus.Suspended)){
				capdVoucherDtl.setStatus(CapdVoucherStatus.Issued.toString());
				issueItemCount++;
			} 
			
			capdVoucherDtl.setManualVoucherNum(capdVoucherItem.getCapdVoucher().getManualVoucherNum());

			value.addCapdVoucherDtl(capdVoucherDtl);
			
			String voucherValue = voucherNumMap.get(capdVoucherItem.getCapdVoucher().getVoucherNum());
			if(voucherValue==null){
				
				if (dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.SysDeleted || dispOrderItem.getDispOrder().getStatus() == DispOrderStatus.Deleted) {
					deleteCount++;
				}
				else if(isSuspended){
					suspendCount++;
				}
				else if(capdVoucherItem.getCapdVoucher().getStatus().equals(CapdVoucherStatus.Issued) || capdVoucherItem.getCapdVoucher().getStatus().equals(CapdVoucherStatus.Suspended)){
					issueCount++;
				}
				voucherNumMap.put(capdVoucherItem.getCapdVoucher().getVoucherNum(), capdVoucherItem.getCapdVoucher().getVoucherNum());
			}
		}

		CapdVoucherRpt rpt = new CapdVoucherRpt();
		//...set calculate count
		rpt.setIssueItemCount(issueItemCount);
		rpt.setDeleteItemCount(deleteItemCount);
		rpt.setSuspendItemCount(suspendItemCount);
		rpt.setIssueVoucherCount(issueCount);
		rpt.setDeleteVoucherCount(deleteCount);
		rpt.setSuspendVoucherCount(suspendCount);
		
		rpt.setCapdVoucherRptDtlList(new ArrayList<CapdVoucherRptDtl>(capdVoucherRptMap.values()));
		
		capdVoucherRptList.add(rpt);

	}
	
	public void generateCapdVoucherRpt() {
		OperationProfile activeProfile = em.find(OperationProfile.class, workstore.getOperationProfileId());
		
		parameters.put("hospCode", workstore.getHospCode());
		parameters.put("workstoreCode", workstore.getWorkstoreCode());	
		parameters.put("activeProfileName", activeProfile.getName());	
		
		printAgent.renderAndRedirect(new RenderJob(
				"CapdVoucherRpt", 
				new PrintOption(), 
				parameters, 
				capdVoucherRptList));

	}

	public void printCapdVoucherRpt(){

		printAgent.renderAndPrint(new RenderAndPrintJob(
				"CapdVoucherRpt", 
				new PrintOption(), 
				printerSelector.retrievePrinterSelect(PrintDocType.Report), 
				parameters, 
				capdVoucherRptList));
		
	}
	
	public boolean isRetrieveSuccess() {
		return retrieveSuccess;
	}
	
	public boolean isValidHkid() {
		return validHkid;
	}
	
	@Remove
	public void destroy() {
		if(capdVoucherRptList!=null){
			capdVoucherRptList=null;
		}
		
	}

}
