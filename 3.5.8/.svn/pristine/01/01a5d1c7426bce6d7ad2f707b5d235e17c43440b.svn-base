package hk.org.ha.view.pms.main.medprofile
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import hk.org.ha.event.pms.main.alert.popup.ShowAlertProfilePopupEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateMedProfileInMemoryEvent;
	import hk.org.ha.event.pms.main.vetting.mp.UpdateMedProfileItemListInMemoryEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.ShowConfirmWardTransferPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.fmk.pms.flex.utils.TimeUtil;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfileItem;
	import hk.org.ha.model.pms.persistence.medprofile.MedProfilePoItem;
	import hk.org.ha.model.pms.udt.alert.AlertProfileStatus;
	import hk.org.ha.model.pms.udt.alert.ehr.EhrAlertStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileItemStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileStatus;
	import hk.org.ha.model.pms.udt.medprofile.MedProfileTransferAlertType;
	import hk.org.ha.model.pms.vo.alert.AlertProfile;
	import hk.org.ha.model.pms.vo.security.PropMap;
	
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.security.Identity;

	public class VettingOnShowAlertUtils
	{
		
		public static var chainEndFunc:Function;
		public static var chainCancelFunc:Function;
		public static var eventChain:ArrayCollection;
		public static var medProfile:MedProfile;
		public static var profileLocked:Boolean;
		public static var errorOnRetrieveItemCutOffDate:Boolean;
		public static var ctx:Context;
		public static var identity:Identity;
		public static var medProfileItemList:ArrayCollection;
		
		public function VettingOnShowAlertUtils():void {
			super();
		}
		
		public static function addChainEvent(event:Event):void 
		{
			eventChain.addItem(event);
		}
		
		public static function nextChainEvent(event:Event = null):void 
		{
			if (event != null) {
				removePopup(event);
			}
			
			if (eventChain.length > 0) {
				var event:Event = eventChain.removeItemAt(0) as Event;
				ctx.dispatchEvent(event);
			} else if (chainEndFunc != null){
				chainEndFunc();
			}
		}
		
		public static function onShowAlertCheck(prop:VettingOnShowAlertProp):ArrayCollection {
			eventChain = prop.eventChain;
			chainEndFunc = prop.chainEndFunc;
			chainCancelFunc = prop.chainCancelFunc;
			medProfile = prop.medProfile;
			ctx = prop.ctx;
			profileLocked = prop.profileLocked;
			errorOnRetrieveItemCutOffDate = prop.errorOnRetrieveItemCutOffDate;
			identity = prop.identity;
			medProfileItemList = prop.medProfileItemList;
			
			var warnProfileLocked:Boolean = checkProfileLocked(prop.medProfile, prop.profileLocked, prop.cancelVetting);
			var warnServiceErrors:Boolean = checkServiceErrors(prop.errorOnRetrieveItemCutOffDate);
			var warnSpecialtyMapping:Boolean = checkSpecialtyMapping(prop.medProfile);
			
			if ( warnProfileLocked || warnServiceErrors || warnSpecialtyMapping ) {
				checkAlertProfile(prop.alertServiceError, prop.ehrAlertServiceError, prop.alertProfile, prop.propMap, prop.medProfile);
				return eventChain;
			}
			
			var warnMedProfileStatusDischarge:Boolean = checkMedProfileStatusDischarge(prop.medProfile, prop.profileLocked)
			
			if ( warnMedProfileStatusDischarge ) {
				checkAlertProfile(prop.alertServiceError, prop.ehrAlertServiceError, prop.alertProfile, prop.propMap, prop.medProfile);
				return eventChain;
			}
				
			var wardTransferResult:String = checkProfileWardTransfer(prop.medProfile, prop.profileLocked, prop.reviewWardTransferMedProfile, prop.updateWardTransferMedProfile);
			
			if ( wardTransferResult != "alertA" ) {
				checkMedProfileStatus(prop.medProfile, prop.profileLocked);
				checkAlertProfile(prop.alertServiceError, prop.ehrAlertServiceError, prop.alertProfile, prop.propMap, prop.medProfile);
			}
			
			return eventChain;
		}
		
		public static function checkSpecialtyMapping(medProfile:MedProfile):Boolean
		{
			if (medProfile.wardCode == null) {
				addChainEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0272", nextChainEvent)));
				return true;
			}
			return false;
		}
		
		public static function checkProfileLocked(medProfile:MedProfile, profileLocked:Boolean, cancelVetting:Function):Boolean
		{
			if (profileLocked) {
				var profileLockedMsgPopupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				profileLockedMsgPopupProp.messageCode = "0317";
				profileLockedMsgPopupProp.setYesNoButton = true;
				profileLockedMsgPopupProp.setYesDefaultButton = false;
				profileLockedMsgPopupProp.setNoDefaultButton = true;
				profileLockedMsgPopupProp.messageParams = new Array([medProfile.workstationHostName]);
				profileLockedMsgPopupProp.yesHandler = nextChainEvent;
				profileLockedMsgPopupProp.noHandler = cancelVetting;
				
				addChainEvent(new RetrieveSystemMessageEvent(profileLockedMsgPopupProp));
				return true;
			}
			return false;
		}
		
		public static function checkProfileWardTransfer(medProfile:MedProfile, profileLocked:Boolean, reviewWardTransferMedProfile:Function, updateWardTransferMedProfile:Function):String
		{
			if( MedProfileTransferAlertType.Notify == medProfile.transferAlertType && !medProfile.inactivePasWardFlag ){
				addChainEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0969", updateWardTransferAfterType)));
				return "alertB";
			} else if( !profileLocked && MedProfileTransferAlertType.Review == medProfile.transferAlertType ) {
				addChainEvent(new ShowConfirmWardTransferPopupEvent(reviewWardTransferMedProfile, updateWardTransferMedProfile));
				return "alertA";
			}
			
			return "none";
		}
		
		public static function updateWardTransferAfterType(evt:Event):void 
		{
			medProfile.transferAlertType = MedProfileTransferAlertType.None;
			ctx.dispatchEvent(new UpdateMedProfileInMemoryEvent(medProfile));
			nextChainEvent(evt);
		}
		
		public static function checkMedProfileStatusDischarge(medProfile:MedProfile, profileLocked:Boolean):Boolean 
		{
			if (medProfile.isDischarge() && !profileLocked) {
				if (medProfile.cancelDischargeDate == null || medProfile.wardCode == null) {
					addChainEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0694", nextChainEvent)));
					return true;
				}
			}
			return false;
		}
		
		public static function checkMedProfileStatus(medProfile:MedProfile, profileLocked:Boolean):void
		{
			if (medProfile.status == MedProfileStatus.HomeLeave) {
				var homeLeaveMsgPopupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				homeLeaveMsgPopupProp.messageCode = "0672";
				homeLeaveMsgPopupProp.yesHandler = confirmActiveProfileStatus;
				homeLeaveMsgPopupProp.noHandler = keepProfileStatus;
				homeLeaveMsgPopupProp.setYesNoButton = true;
				homeLeaveMsgPopupProp.setNoDefaultButton = true;
				homeLeaveMsgPopupProp.setYesDefaultButton = false;
				addChainEvent(new RetrieveSystemMessageEvent(homeLeaveMsgPopupProp));
			} else if (medProfile.status == MedProfileStatus.PendingDischarge) {
				var pendingDischargeMsgPopupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				pendingDischargeMsgPopupProp.messageCode = "0673";
				pendingDischargeMsgPopupProp.yesHandler = confirmActiveProfileStatus;
				pendingDischargeMsgPopupProp.noHandler = keepProfileStatus;
				pendingDischargeMsgPopupProp.setYesNoButton = true;
				pendingDischargeMsgPopupProp.setNoDefaultButton = true;
				pendingDischargeMsgPopupProp.setYesDefaultButton = false;
				addChainEvent(new RetrieveSystemMessageEvent(pendingDischargeMsgPopupProp));
			} else if (medProfile.isDischarge() && !profileLocked) {
				if (medProfile.cancelDischargeDate != null && medProfile.wardCode != null) {
					var cancelDischargeMsgPopupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
					cancelDischargeMsgPopupProp.messageCode = "0693";
					cancelDischargeMsgPopupProp.setYesNoButton = true;
					cancelDischargeMsgPopupProp.messageWinWidth = 520;
					cancelDischargeMsgPopupProp.messageWinHeight = 270;
					cancelDischargeMsgPopupProp.messageParams = new Array (MedProfileUtils.formatDateTime(medProfile.dischargeDate, false), 
																	MedProfileUtils.formatDateTime(medProfile.cancelDischargeDate, false), 
																	MedProfileUtils.formatTimeDiff(medProfile.dischargeDate, medProfile.cancelDischargeDate));
					cancelDischargeMsgPopupProp.yesHandler = confirmCancelDischarge;
					cancelDischargeMsgPopupProp.noHandler = cancelCancelDischarge;
					addChainEvent(new RetrieveSystemMessageEvent(cancelDischargeMsgPopupProp));
				}
			}
		}
		
		public static function confirmActiveProfileStatus(evt:Event):void
		{
			if ( medProfile.status == MedProfileStatus.HomeLeave ) {
				resetHomeLeave();
			} else if ( medProfile.status == MedProfileStatus.PendingDischarge ) {
				medProfile.resumeActiveDate = null;
			}
			medProfile.status = MedProfileStatus.Active;
			medProfile.inactiveType = null;
			ctx.protectedMode = profileLocked || medProfile.wardCode == null || errorOnRetrieveItemCutOffDate;
			ctx.readOnly = ctx.protectedMode;
			ctx.dispatchEvent(new UpdateMedProfileInMemoryEvent(medProfile));
			nextChainEvent(evt);
		}
		
		public static function resetHomeLeave():void {
			var manualItemList:ArrayCollection = MedProfileUtils.resetHomeLeave(medProfile, medProfileItemList);
			
			if (manualItemList.length > 0) {
				ctx.dispatchEvent(new UpdateMedProfileItemListInMemoryEvent(manualItemList));
			}
		}
		
		public static function keepProfileStatus(evt:Event):void
		{
			removePopup(evt);
			nextChainEvent(evt);
		}
		
		public static function confirmCancelDischarge(evt:Event):void
		{
			medProfile.status = MedProfileStatus.Active;
			medProfile.inactiveType = null;
			medProfile.confirmCancelDischargeUser = identity.username;
			medProfile.confirmCancelDischargeDate = null;
			ctx.protectedMode = profileLocked || medProfile.wardCode == null || errorOnRetrieveItemCutOffDate;
			ctx.readOnly = ctx.protectedMode;
			ctx.dispatchEvent(new UpdateMedProfileInMemoryEvent(medProfile));
			nextChainEvent(evt);
		}
		
		public static function cancelCancelDischarge(evt:Event):void
		{
			removePopup(evt);
			ctx.dispatchEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0694", nextChainEvent)));
		}
		
		public static function checkAlertProfile(alertServiceError:Boolean, ehrAlertServiceError:Boolean, alertProfile:AlertProfile, propMap:PropMap, medProfile:MedProfile):void 
		{
			if (alertServiceError) {
				addChainEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0592", nextChainEvent)));
			}
			if (ehrAlertServiceError) {
				addChainEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0984", nextChainEvent)));
			}
			
			if (alertProfile != null && alertProfile.status == AlertProfileStatus.RecordExist && propMap.getValueAsBoolean("vetting.medProfile.alert.profile.popup")) {
				addChainEvent(new ShowAlertProfilePopupEvent(alertProfile, medProfile.medCase.caseNum, medProfile.patHospCode, medProfile.patient, nextChainEvent));						
			}
		}
		
		public static function checkServiceErrors(errorOnRetrieveItemCutOffDate:Boolean):Boolean
		{
			if (errorOnRetrieveItemCutOffDate) {	
				addChainEvent(new RetrieveSystemMessageEvent(createSystemMessagePopupPropWithtOkBtn("0699", nextChainEvent)));
				return true;
			}
			return false;
		}
		
		private static function createSystemMessagePopupPropWithtOkBtn(messageCode:String, okHandler:Function=null):SystemMessagePopupProp 
		{
			var systemMessagePopupProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			systemMessagePopupProp.messageCode = messageCode;
			systemMessagePopupProp.setOkButtonOnly = true;
			if ( okHandler != null ) {
				systemMessagePopupProp.okHandler = okHandler;
			}
			return systemMessagePopupProp;
		}
		
		protected static function removePopup(evt:Event):void 
		{
			if (evt != null) {
				PopUpManager.removePopUp((evt.currentTarget as UIComponent).parentDocument as IFlexDisplayObject);
			}
		}
		
	}
}