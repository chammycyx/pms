package hk.org.ha.model.pms.biz.delivery;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.medprofile.MedProfileUtils;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.persistence.medprofile.Delivery;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryItem;
import hk.org.ha.model.pms.persistence.medprofile.DeliveryRequest;
import hk.org.ha.model.pms.udt.medprofile.DeliveryItemStatus;
import hk.org.ha.model.pms.udt.medprofile.DeliveryRequestStatus;
import hk.org.ha.model.pms.vo.label.LabelContainer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("deliveryManager")
@MeasureCalls
public class DeliveryManagerBean implements DeliveryManagerLocal {
	
	private static final List<DeliveryItemStatus> NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST = Arrays.asList(DeliveryItemStatus.KeepRecord,
																										DeliveryItemStatus.Unlink,
																										DeliveryItemStatus.Deleted);
	
	private static final List<DeliveryItemStatus> NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_LABELREPRINT = Arrays.asList(DeliveryItemStatus.KeepRecord, DeliveryItemStatus.Deleted);
	
	@PersistenceContext
	private EntityManager em;
	
	@In 
	private Workstore workstore;
	
	@SuppressWarnings("unchecked")
	public TreeMap<Long, Delivery> retrieveDeliveryNumOfLabel(TreeMap<Long, Delivery> deliveryMap) {
		
		if (deliveryMap.size() >0){

			Query query = em.createQuery(
					"select o.delivery.id, count(o) from DeliveryItem o" + // 20120227 index check : DeliveryItem.delivery : FK_DELIVERY_ITEM_01
					" where o.delivery.id in :deliveryIdList" +
					" and o.delivery.hospital = :hospital" +
					" and o.status not in :deliveryItemStatusList" +
					" group by o.delivery.id")
					.setParameter("deliveryIdList", deliveryMap.keySet())
					.setParameter("hospital", workstore.getHospital())
					.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST);

			List numOfLabelList = query.getResultList();
			
			for (Object object : numOfLabelList) {
				if ( deliveryMap.get((Long)Array.get(object, 0)) != null ) {
					deliveryMap.get((Long)Array.get(object, 0)).setNumOfLabel(((Long)Array.get(object, 1)).intValue());
				}
			}
			
		}
		
		return deliveryMap;
		
	}
	
	@SuppressWarnings("unchecked")
	public TreeMap<Long, Delivery> retrieveDeliveryNumOfLabelForLabelReprint(TreeMap<Long, Delivery> deliveryMap, String caseNum, String trxId) {
		
		if (deliveryMap.size() >0){
			
			if (StringUtils.isNotBlank(caseNum) ) {

				//PMS-4005 append the unlinked item into deliveryMap
				
					List<DeliveryItem> diList = new ArrayList<DeliveryItem>();
					Query unlinkItemQuery = em.createQuery(
							"select o from DeliveryItem o" + // 20120227 index check : DeliveryItem.delivery : FK_DELIVERY_ITEM_01
							" where o.delivery.id in :deliveryIdList" +
							" and o.delivery.hospital = :hospital" +
							" and o.status not in :deliveryItemStatusList" +
							" and o.medProfile.medCase.caseNum = :caseNum" +
							" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" +
							" order by o.delivery.batchNum, o.status ")
							.setParameter("deliveryIdList", deliveryMap.keySet())
							.setParameter("hospital", workstore.getHospital())
							.setParameter("caseNum", caseNum)
							.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_LABELREPRINT)
							.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed);
					 diList = unlinkItemQuery.getResultList();
					
					Delivery delivery = new Delivery();
					TreeMap<Long, Delivery> voDeliveryMap = new TreeMap<Long, Delivery>();
					for(DeliveryItem di: diList){
						
						Delivery unlinkDelivery = di.getDelivery();
						DeliveryRequest unlinkDeliveryRequest = di.getDelivery().getDeliveryRequest();
						
						if(DeliveryItemStatus.Unlink.equals(di.getStatus())){
							//unlink as new item
							delivery = new Delivery(); 
							delivery.setBatchNum(unlinkDelivery.getBatchNum());
							delivery.setBatchDate(unlinkDelivery.getBatchDate());
							delivery.setCreateDate(unlinkDelivery.getCreateDate());
							delivery.setPrintMode(unlinkDelivery.getPrintMode());
							delivery.setStatus(unlinkDelivery.getStatus());
							delivery.getDeliveryItemList().add(di);
							delivery.setTrxIdDeliveryItem(di.getId().toString());
							delivery.setNumOfLabel(1);
							delivery.setDeliveryRequest(unlinkDeliveryRequest);
							voDeliveryMap.put(unlinkDelivery.getId()+di.getId()*100, delivery);
							
						}else {
							if(voDeliveryMap.containsKey(unlinkDelivery.getId())){
								//find the previous batchnum to update the deliveryitem
								Delivery updateDelivery = voDeliveryMap.get(unlinkDelivery.getId());
								
								updateDelivery.getDeliveryItemList().add(di);
								updateDelivery.setTrxIdDeliveryItem("");
								updateDelivery.setNumOfLabel(updateDelivery.getDeliveryItemList().size());
								voDeliveryMap.put(unlinkDelivery.getId(), updateDelivery);
							
							}else{
								//new batchnum
								delivery = new Delivery(); 
								delivery.setBatchNum(unlinkDelivery.getBatchNum());
								delivery.setBatchDate(unlinkDelivery.getBatchDate());
								delivery.setCreateDate(unlinkDelivery.getCreateDate());
								delivery.setPrintMode(unlinkDelivery.getPrintMode());
								delivery.setStatus(unlinkDelivery.getStatus());
								delivery.setDeliveryRequest(unlinkDeliveryRequest);
								delivery.getDeliveryItemList().add(di);
								delivery.setTrxIdDeliveryItem("");
								delivery.setNumOfLabel(1);
								voDeliveryMap.put(unlinkDelivery.getId(), delivery);
							}
						}
					
					}
				
					deliveryMap = voDeliveryMap;
				
				//
			} else if ( StringUtils.isNotBlank(trxId)){
				
				Query query = em.createQuery(
						"select o.delivery.id, o.status from DeliveryItem o" + // 20120227 index check : DeliveryItem.delivery : FK_DELIVERY_ITEM_01
						" where o.id = :deliveryItemId" +
						" and o.delivery.hospital = :hospital" +
						" and o.status not in :deliveryItemStatusList" +
						" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" 	)
						.setParameter("deliveryItemId", Long.parseLong(trxId))
						.setParameter("hospital", workstore.getHospital())
						.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_LABELREPRINT)
						.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed);
	
				List resultList = query.getResultList();
				
				for (Object object : resultList) {
					if ( deliveryMap.get((Long)Array.get(object, 0)) != null ) {
						deliveryMap.get((Long)Array.get(object, 0)).getDeliveryRequest();
						
						if(DeliveryItemStatus.Unlink.equals(((DeliveryItemStatus)Array.get(object, 1)))){
							deliveryMap.get((Long)Array.get(object, 0)).setTrxIdDeliveryItem(trxId);
						}else {
							deliveryMap.get((Long)Array.get(object, 0)).setTrxIdDeliveryItem("");
							
						}
						deliveryMap.get((Long)Array.get(object, 0)).setNumOfLabel((1));
					}
				}
				
			}else {
				
				Query query = em.createQuery(
						"select o.delivery.id, count(o) from DeliveryItem o" + // 20120227 index check : DeliveryItem.delivery : FK_DELIVERY_ITEM_01
						" where o.delivery.id in :deliveryIdList" +
						" and o.delivery.hospital = :hospital" +
						" and o.status not in :deliveryItemStatusList" +
						" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" +
						" group by o.delivery.id")
						.setParameter("deliveryIdList", deliveryMap.keySet())
						.setParameter("hospital", workstore.getHospital())
						.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST)
						.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed);

				List numOfLabelList = query.getResultList();
				
				for (Object object : numOfLabelList) {
					
					
					if ( deliveryMap.get((Long)Array.get(object, 0)) != null ) {
						deliveryMap.get((Long)Array.get(object, 0)).getDeliveryRequest();
						deliveryMap.get((Long)Array.get(object, 0)).setNumOfLabel(((Long)Array.get(object, 1)).intValue());
					}
				}
			}
		}
		
		return deliveryMap;
		
	}
	
	public List<DeliveryItem> retrieveDeliveryItemList(String wardCode, String batchNum, String caseNum, Date dispenseDate, String deliveryItemId) {
		return retrieveDeliveryItemList(wardCode, batchNum, caseNum, dispenseDate, deliveryItemId, NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST);
	}
	
	public List<DeliveryItem> retrieveDeliveryItemListForLabelReprint(String wardCode, String batchNum, String caseNum, Date dispenseDate, String deliveryItemId) {
		return retrieveDeliveryItemList(wardCode, batchNum, caseNum, dispenseDate, deliveryItemId,
				StringUtils.isNotBlank(caseNum) || StringUtils.isNotBlank(deliveryItemId) ? NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_LABELREPRINT : NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST);
	}
	
	@SuppressWarnings("unchecked")
	private List<DeliveryItem> retrieveDeliveryItemList(String wardCode, String batchNum, String caseNum, Date dispenseDate, String deliveryItemId, List<DeliveryItemStatus> deliveryItemStatusList) {
		
		StringBuilder sql = new StringBuilder(
				"select o from DeliveryItem o" + // 20120305 index check : Delivery.hospital,batchDate,batchNum MedCase.caseNum : I_DELIVERY_01 I_MED_CASE_01
				" where o.delivery.hospital = :hospital" +
				" and o.status not in :deliveryItemStatusList" +
				" and o.delivery.deliveryRequest.status = :deliveryRequestStatus");

		if (StringUtils.isNotBlank(deliveryItemId)) {
			sql.append(" and o.id = :deliveryItemId");
		} else {			
			if (dispenseDate != null) {
				sql.append(" and o.delivery.batchDate = :dispenseDate");
			}
			
			if (StringUtils.isNotBlank(wardCode)) {
				sql.append(" and o.medProfile.wardCode = :wardCode");
			}
			
			if (StringUtils.isNotBlank(batchNum)) {
				sql.append(" and o.delivery.batchNum = :batchNum");
			}
			
			if (StringUtils.isNotBlank(caseNum)) {
				sql.append(" and o.medProfile.medCase.caseNum = :caseNum");
			}
		}	
		
		Query query = em.createQuery(sql.toString());
		
		query.setParameter("hospital", workstore.getHospital());
		query.setParameter("deliveryItemStatusList", deliveryItemStatusList);
		query.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed);

		if (StringUtils.isNotBlank(deliveryItemId)) {
			try {
				query.setParameter("deliveryItemId", Long.parseLong(deliveryItemId));
			} catch (Exception ex) {
				return Collections.EMPTY_LIST;
			}
		} else {		
			if (dispenseDate != null) {
				query.setParameter("dispenseDate", dispenseDate, TemporalType.DATE);
			}
			
			if (StringUtils.isNotBlank(wardCode)) {
				query.setParameter("wardCode", wardCode);
			}
			
			if (StringUtils.isNotBlank(batchNum)) {
				query.setParameter("batchNum", batchNum);
			}
			
			if (StringUtils.isNotBlank(caseNum)) {
				query.setParameter("caseNum", caseNum);
			}
			
			query.setHint(QueryHints.FETCH, "o.delivery.deliveryRequest");
		}
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<DeliveryItem> retrieveDeliveryItemListByBatchNum(String batchNum, Date dispenseDate, String caseNum) {
		
		if (dispenseDate == null) {
			dispenseDate = DateUtils.truncate(new Date(), Calendar.DATE);
		}
		
		String sql = "select o from DeliveryItem o" + // 20120305 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate = :dispenseDate" +
				" and o.delivery.batchNum = :batchNum" +
				" and o.status not in :deliveryItemStatusList";		

		// Normal Path
		StringBuilder normalSql = new StringBuilder(sql);		
		if (StringUtils.isNotBlank(caseNum)) {
			normalSql.append(" and o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase.caseNum = :caseNum");
		}
		
		Query normalQuery = em.createQuery(normalSql.toString());
		
		normalQuery.setParameter("hospital", workstore.getHospital())
				.setParameter("dispenseDate", dispenseDate, TemporalType.DATE)
				.setParameter("batchNum", batchNum)
				.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST);

		if (StringUtils.isNotBlank(caseNum)) {
			normalQuery.setParameter("caseNum", caseNum);
		}

		normalQuery.setHint(QueryHints.FETCH, "o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase");
		
		return normalQuery.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	public List<DeliveryItem> retrieveDeliveryItemListByBatchNumForLabelReprint(String batchNum, Date dispenseDate, String caseNum, String trxId) {
		
		if (dispenseDate == null) {
			dispenseDate = DateUtils.truncate(new Date(), Calendar.DATE);
		}
		String sql = "select o from DeliveryItem o" + // 20120305 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
		" where o.delivery.hospital = :hospital" +
		" and o.delivery.batchDate = :dispenseDate" +
		" and o.delivery.batchNum = :batchNum" +
		" and o.delivery.deliveryRequest.status = :deliveryRequestStatus" ;
		
		if( StringUtils.isNotBlank(caseNum) && StringUtils.isNotBlank(trxId)){
			sql +=	" and o.status = :deliveryItemStatus ";		
		}else{
			sql +=  " and o.status not in :deliveryItemStatusList";		
		}

		StringBuilder normalSql = new StringBuilder(sql);		
		if (StringUtils.isNotBlank(caseNum)) {
			normalSql.append(" and o.medProfile.medCase.caseNum = :caseNum");
		}
		
		if (StringUtils.isNotBlank(trxId)) {
			normalSql.append(" and o.id = :trxId");
		}
		
		Query normalQuery = em.createQuery(normalSql.toString());
		
		normalQuery.setParameter("hospital", workstore.getHospital());
		normalQuery.setParameter("dispenseDate", dispenseDate, TemporalType.DATE);
		normalQuery.setParameter("batchNum", batchNum);
		normalQuery.setParameter("deliveryRequestStatus", DeliveryRequestStatus.Completed);
		
		if( StringUtils.isNotBlank(caseNum) && StringUtils.isNotBlank(trxId)){
			normalQuery.setParameter("deliveryItemStatus", DeliveryItemStatus.Unlink);
		}else if ( StringUtils.isNotBlank(trxId) ) {
			normalQuery.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST_FOR_LABELREPRINT);
		}else {
			normalQuery.setParameter("deliveryItemStatusList", NON_ACTIVE_DELIVERY_ITEM_STATUS_LIST);			
		}

		if (StringUtils.isNotBlank(caseNum)) {
			normalQuery.setParameter("caseNum", caseNum);
		}
		
		if (StringUtils.isNotBlank(trxId)) {
			normalQuery.setParameter("trxId", Long.parseLong(trxId));
		}

		normalQuery.setHint(QueryHints.FETCH, "o.medProfile.medCase");
		
		return normalQuery.getResultList();		
	}
	
    public Date calculateTargetDeliveryDate(List<DeliveryItem> deliveryItemList) {
    	
    	Date targetDate = null;
    	
    	for (DeliveryItem deliveryItem: deliveryItemList) {
    		if ( !deliveryItem.getStatus().equals(DeliveryItemStatus.Deleted) && !deliveryItem.getStatus().equals(DeliveryItemStatus.Unlink)) {

    			Date dueDate = deliveryItem.getDueDate();
        		
        		//PMS-3170 add skip STAT flag = True
        		if ( (targetDate == null || (dueDate != null && targetDate.after(dueDate))) && 
        				!deliveryItem.getStatus().equals( DeliveryItemStatus.KeepRecord) ) {
        			targetDate = dueDate;
        		}
    		}
    	}
    	
    	return targetDate;
    	
    }
	
    @SuppressWarnings("unchecked")
	public List<DeliveryItem> retrieveDeliveryItemListByBatchNumForReprint(String batchNum, Date dispenseDate) {
		
		String sql = "select o from DeliveryItem o" + // 20120305 index check : Delivery.hospital,batchDate,batchNum : I_DELIVERY_01
				" where o.delivery.hospital = :hospital" +
				" and o.delivery.batchDate = :dispenseDate" +
				" and o.delivery.batchNum = :batchNum" ;

		StringBuilder normalSql = new StringBuilder(sql);		
		
		
		Query normalQuery = em.createQuery(normalSql.toString());
		
		normalQuery.setParameter("hospital", workstore.getHospital())
				.setParameter("dispenseDate", dispenseDate, TemporalType.DATE)
				.setParameter("batchNum", batchNum);


		normalQuery.setHint(QueryHints.LEFT_FETCH, "o.medProfilePoItem.medProfileMoItem.medProfileItem.medProfile.medCase")
				.setHint(QueryHints.LEFT_FETCH, "o.pivasWorklist.medProfileMoItem.medProfileItem.medProfile.medCase");
		
		List<DeliveryItem> normalList = normalQuery.getResultList();
				
		for (DeliveryItem di : normalList){
			if (di.getReplenishmentItem() != null) {
				di.getReplenishmentItem().getReplenishment().getReplenishmentItemList().size();
			}
		}
		
		return normalList;		
	}

	@Override
    @SuppressWarnings("unchecked")
    public List<DeliveryItem> retrieveDeliveryItemListById(Collection<Long> deliveryItemIdList) {

    	return QueryUtils.splitExecute(em.createQuery(
    					"select o from DeliveryItem o" + // 20160615 index check : DeliveryItem.id : PK_DELIVERY_ITEM
    					" where o.id in :idList")
    					, "idList", deliveryItemIdList);
    }
    
	@Remove
	public void destroy() {
		
	}

}
