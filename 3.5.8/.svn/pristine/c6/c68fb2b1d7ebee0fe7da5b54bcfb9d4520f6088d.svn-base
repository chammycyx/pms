package hk.org.ha.control.pms.main.vetting
{
	import hk.org.ha.event.pms.main.vetting.CheckFmIndicationForOrderEditEvent;
	import hk.org.ha.event.pms.main.vetting.EnableVettingEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.RetrieveFmIndicationListByMedProfileMoItemEvent;
	import hk.org.ha.event.pms.main.vetting.mp.popup.ShowMedProfileFmIndicationPopupEvent;
	import hk.org.ha.event.pms.main.vetting.popup.RetrieveFmIndicationByIdEvent;
	import hk.org.ha.event.pms.main.vetting.popup.RetrieveFmIndicationByMedOrderItemEvent;
	import hk.org.ha.event.pms.main.vetting.popup.ShowFmIndicationPopupEvent;
	import hk.org.ha.model.pms.biz.vetting.FmIndicationServiceBean;
	import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
	import hk.org.ha.model.pms.persistence.disp.PharmOrder;
	import hk.org.ha.model.pms.vo.vetting.FmIndication;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.TideFaultEvent;
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("fmIndicationPopupServiceCtl", restrict="true")]
	public class FmIndicationPopupServiceCtl
	{
		[In]
		public var fmIndicationService:FmIndicationServiceBean;
		
		[In]
		public var isSpecialDrug:Boolean;
		
		[In]
		public var ctx:Context;
		
		[In]
		public var pharmOrder:PharmOrder;
		
		private var callbackFunc:Function;
		
		private var retrieveFmIndicationListByMedOrderItemEvent:RetrieveFmIndicationByMedOrderItemEvent;
		
		[Observer]
		public function retrieveFmIndicationListById(evt:RetrieveFmIndicationByIdEvent):void {
			fmIndicationService.retrieveFmIndicationListById(evt.medOrderItemId, retrieveFmIndicationListByIdResult, retrieveFmIndicationListByIdFault);
		}
		
		private function retrieveFmIndicationListByIdResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowFmIndicationPopupEvent(evt.result as ArrayCollection));
		}
		
		private function retrieveFmIndicationListByIdFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new EnableVettingEvent(true));
		}
		
		[Observer]
		public function retrieveFmIndicationListByMedOrderItem(evt:RetrieveFmIndicationByMedOrderItemEvent):void {
			retrieveFmIndicationListByMedOrderItemEvent = evt;
			
			evt.medOrderItem.dislink();
			fmIndicationService.retrieveFmIndicationListByMedOrderItem(evt.medOrderItem, retrieveFmIndicationListByMedOrderItemResult, retrieveFmIndicationListByMedOrderItemFault);
		}
		
		private function retrieveFmIndicationListByMedOrderItemResult(evt:TideResultEvent):void {
			var medOrderItem:MedOrderItem = retrieveFmIndicationListByMedOrderItemEvent.medOrderItem;
			
			medOrderItem.medOrder = pharmOrder.medOrder;
			pharmOrder.relinkAllItem();

			evt.context.dispatchEvent(new ShowFmIndicationPopupEvent(evt.result as ArrayCollection));
		}
		
		private function retrieveFmIndicationListByMedOrderItemFault(evt:TideFaultEvent):void {
			evt.context.dispatchEvent(new EnableVettingEvent(true));
		}
		
		[Observer]
		public function retrieveFmIndicationListByMedProfileMoItem(evt:RetrieveFmIndicationListByMedProfileMoItemEvent):void {
			fmIndicationService.retrieveFmIndicationListByMedProfileMoItem(evt.medProfileMoItem, retrieveFmIndicationListByMedProfileMoItemResult, retrieveFmIndicationListByMedProfileMoItemFault);
		}
		
		private function retrieveFmIndicationListByMedProfileMoItemResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowMedProfileFmIndicationPopupEvent(evt.result as ArrayCollection));
		}
		
		private function retrieveFmIndicationListByMedProfileMoItemFault(evt:TideFaultEvent):void {
		}
		
		[Observer]
		public function checkFmIndicationForOrderEdit(evt:CheckFmIndicationForOrderEditEvent):void
		{
			callbackFunc = evt.callbackFunc;			
			fmIndicationService.checkFmIndicationForOrderEdit(evt.pmsFmStatusSearchCriteria, checkFmIndicationForOrderEditResult);			
		}
		
		private function checkFmIndicationForOrderEditResult(evt:TideResultEvent):void 
		{
			if ( callbackFunc != null ) {
				callbackFunc(isSpecialDrug);
			}
		}
	}
}