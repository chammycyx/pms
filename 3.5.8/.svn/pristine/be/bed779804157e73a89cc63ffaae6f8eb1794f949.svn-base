<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent xmlns:fx="http://ns.adobe.com/mxml/2009" 
							   xmlns:s="library://ns.adobe.com/flex/spark" 
							   xmlns:mx="library://ns.adobe.com/flex/mx"
							   xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
							   xmlns:m="hk.org.ha.fmk.pms.flex.components.message.*">
	
	<fx:Declarations>
		<m:SystemMessagePopupProp id="profileNotFoundMsgPopupProp" messageCode="0316" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="optimisticLockedMsgPopupProp" messageCode="0704" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="profileLockedMsgPopupProp" messageCode="0705" setOkButtonOnly="true"/>
		<mx:DateFormatter id="dateFormatter" formatString="YYYYMMDDJJNN"/>
	</fx:Declarations>

	<fx:Metadata>
		[Name("manualTransferPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.inbox.ManualTransferEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
			import hk.org.ha.model.pms.udt.medprofile.ManualTransferResult;
			
			import mx.controls.DateField;
			import mx.managers.PopUpManager;
			
			private static const COLUMN_WIDTH_DESCRIPTION:Number = 350;
			private static const COLUMN_WIDTH_VALUE:Number = 150;
			
			[Bindable]
			private var medProfile:MedProfile;
			
			private var successFunction:Function;
			
			public function setupUI(medProfile:MedProfile, successFunction:Function):void {
				this.medProfile = medProfile;
				this.successFunction = successFunction;
				PopUpManager.centerPopUp(this);
			}
			
			public function update():void {

				dispatchEvent(new ManualTransferEvent(medProfile, newTransferFlagCheckBox.selected, null, false, null,
					function (result:ManualTransferResult):void {
						
						switch (result) {
							case ManualTransferResult.NoProfileFound:
								dispatchEvent(new RetrieveSystemMessageEvent(profileNotFoundMsgPopupProp));
								break;
							case ManualTransferResult.OptimisticLock:
								dispatchEvent(new RetrieveSystemMessageEvent(optimisticLockedMsgPopupProp));
								break;
							case ManualTransferResult.ProcessingByOthers:
								dispatchEvent(new RetrieveSystemMessageEvent(profileLockedMsgPopupProp));
								break;
							case ManualTransferResult.Success:
								if (successFunction != null) { 
									successFunction();
								}
								close();
								break;
						}
					}
				));
			}
			
			public function close():void {
				PopUpManager.removePopUp(this);				
			}
		]]>
	</fx:Script>
	
	<s:Panel title="Refresh Profile Setting">
		<s:layout>
			<s:VerticalLayout paddingTop="20" paddingLeft="20" paddingRight="20" paddingBottom="20"/>
		</s:layout>

		<s:HGroup>
			<s:Label width="{COLUMN_WIDTH_DESCRIPTION}" fontWeight="bold" text="Setting"/>
			<s:Label width="{COLUMN_WIDTH_VALUE}" fontWeight="bold" text="Current value"/>
			<s:Label width="{COLUMN_WIDTH_VALUE}" fontWeight="bold" text="New value"/>
		</s:HGroup>
		
		<s:HGroup verticalAlign="bottom">
			<s:Label width="{COLUMN_WIDTH_DESCRIPTION}" text="Refresh patient profile required"/>
			<s:CheckBox width="{COLUMN_WIDTH_VALUE}" enabled="false" selected="{medProfile.transferFlag}"/>
			<s:CheckBox id="newTransferFlagCheckBox" width="{COLUMN_WIDTH_VALUE}" selected="{medProfile.transferFlag}"/>
		</s:HGroup>
		
		<s:controlBarContent>
			<s:HGroup width="100%" horizontalAlign="right">
				<fc:ExtendedButton label="Update" click="update()"/>
				<fc:ExtendedButton label="Cancel" click="close()"/>
			</s:HGroup>
		</s:controlBarContent>		
	</s:Panel>
	
</fc:ExtendedNavigatorContent>
