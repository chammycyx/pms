package hk.org.ha.model.pms.biz.report;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.biz.ApplicationProp;
import hk.org.ha.model.pms.biz.printing.PrintAgentInf;
import hk.org.ha.model.pms.exception.sftp.SftpCommandException;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.report.DnldTrxFile;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.lingala.zip4j.exception.ZipException;
import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.ChannelSftp.LsEntry;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dnldTrxFileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DnldTrxFileServiceBean implements DnldTrxFileServiceLocal {

	@Logger
	private Log logger;

	@In
	private Workstore workstore;

	@In
	private ReportProvider<JRDataSource> reportProvider;
	
	@In
	private PrintAgentInf printAgent;
	
	@In
	AuditLogger auditLogger;
	
	@In
	private ApplicationProp applicationProp;
	
	@Out(required = false)
	private List<DnldTrxFile> dailyTrxFileList;

	@Out(required = false)
	private List<DnldTrxFile> monthlyArchiveList;

	private String filePath = null;
	private String fileName = null;
	private String password = null;
		
	private DateFormat yearFormat = new SimpleDateFormat("yyyy"); 
	private DateFormat yearShortFormat = new SimpleDateFormat("yy"); 
	private DateFormat monthFormat = new SimpleDateFormat("MM"); 
	
	private DateFormat df = new SimpleDateFormat("yyMM"); 
	

	private static final String DNLD_TRX_FILE_EXT = ".dat";
	private static final String SLASH = "/";
	private static final String EXCEPTION_CLASS_NAME = "DnldTrxFile";
	private static final String MONTH = "month";
	private static final String PERMISSION_DENIED = "Permission denied";
	
	private File tempDir;
	private ArrayList<File> fileArrayList;

	@SuppressWarnings("unchecked")
	public void retrieveDnldTrxFileList() throws ParseException, SftpCommandException {
		
		dailyTrxFileList = new ArrayList<DnldTrxFile>();
		monthlyArchiveList = new ArrayList<DnldTrxFile>();
		ChannelSftp pmsChannelSftp;
		
		try {
			Context context = Contexts.getEventContext();
			context.set("sftp_pms_host", applicationProp.getSftpPmsHost());
			context.set("sftp_pms_username", applicationProp.getSftpPmsUsername());
			context.set("sftp_pms_password", applicationProp.getSftpPmsPassword());
			context.set("sftp_pms_rootDir", applicationProp.getSftpPmsRootDir());
			pmsChannelSftp = (ChannelSftp) Component.getInstance("pmsChannelSftp");
			pmsChannelSftp.cd(applicationProp.getSftpPmsRootDir());
		} 
		catch (Exception e) 
		{
			logger.error("Login failed to file server: #0", applicationProp.getSftpPmsHost());
			throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
		}
		
		try 
		{
			pmsChannelSftp.cd(applicationProp.getDnldTrxDataSubDir());
		} 
		catch (SftpException e) 
		{			
			logger.error("Sftp command error, either no sub directory nor permission denied");
			throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
		}
		
		//retrieve day end report for current month
		try
		{
			pmsChannelSftp.cd(applicationProp.getSftpPmsRootDir() + SLASH + getTrxFileDir(new Date()));
			List fileVector = pmsChannelSftp.ls(".");
			if (fileVector.size() != 0) 
			{
				for (int i = 0; i < fileVector.size(); i++) 
				{
					LsEntry lsEntry = (LsEntry) fileVector.get(i);
					if (!lsEntry.getFilename().equals(".")	&& !lsEntry.getFilename().equals("..")) 
					{
						dailyTrxFileList.add( new DnldTrxFile(removeFileExt(lsEntry.getFilename()),  
								new Date((long)lsEntry.getAttrs().getMTime()*1000L)));
						
					}
				}
			}
		}
		catch (SftpException e) 
		{
			if (e.getMessage().equals(PERMISSION_DENIED))
			{
				logger.error("Sftp command error, permission denied");
				throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
			}
			
			logger.debug("Sftp command error, report target directory not exists");
		}
		
		//retrieve past two months day end files for archive
		for (int i = 2; i >= 1; i--)
		{
			try
			{
				DateTime targetDate = new DateTime().minusMonths(i);
				pmsChannelSftp.cd(applicationProp.getSftpPmsRootDir() + SLASH + getTrxFileDir(targetDate.toDate()));
				List fileVector = pmsChannelSftp.ls(".");
				
				if (fileVector.size() != 0) 
				{
					for (int j = 0; j < fileVector.size(); j++) 
					{
						LsEntry lsEntry = (LsEntry) fileVector.get(j);
						if (!lsEntry.getFilename().equals(".")	&& !lsEntry.getFilename().equals("..")) 
						{
							monthlyArchiveList.add( new DnldTrxFile(workstore.getHospCode() + 
									"-" + 
									yearShortFormat.format(targetDate.toDate()) + 
									monthFormat.format(targetDate.toDate()), null) );
							break;
						}
					}
				}
			}
			catch (SftpException e) 
			{
				if (e.getMessage().equals(PERMISSION_DENIED))
				{
					logger.error("Sftp command error, permission denied");
					throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
				}
				
				logger.debug("Sftp command error, report target directory not exists");
				continue;
			}
		}	
	}
	
	public void dnldDailyTrxFile(String fileName, String password) throws SftpCommandException, ZipException, IOException {

		long startTime = System.currentTimeMillis();
		
		this.password = password;
		this.fileName = fileName;
		
		filePath = getTrxFileDir(new Date()) + SLASH + fileName + DNLD_TRX_FILE_EXT;

		getDnldTrxFile();

		long stopTime = System.currentTimeMillis();
		
		auditLogger.log("#0973", fileName, (stopTime - startTime));
	}

	public void dnldMonthlyArchiveFile(String fileName, String password) throws ParseException, SftpCommandException, ZipException, IOException {
		
		long startTime = System.currentTimeMillis();
		
		this.password = password;
		this.fileName = fileName;
		
		Date date = df.parse( StringUtils.substringAfter(fileName, "-") );
		filePath = getTrxFileDir(date) + SLASH;
		
		getDnldTrxFile();
		
		long stopTime = System.currentTimeMillis();
		
		auditLogger.log("#0974", fileName, (stopTime - startTime));
	}

	@SuppressWarnings("unchecked")
	private void getDnldTrxFile() throws SftpCommandException, ZipException, IOException {

        tempDir = new File(System.getProperty("java.io.tmpdir") + System.currentTimeMillis());
        if (!tempDir.mkdirs()) {
        	throw new IOException("Cannot create directory : " + tempDir);
        }
		
		fileArrayList = new ArrayList<File>();
		
		if (filePath != null) {
			
			ChannelSftp pmsChannelSftp;
			
			try {
				Context context = Contexts.getEventContext();
				context.set("sftp_pms_host", applicationProp.getSftpPmsHost());
				context.set("sftp_pms_username", applicationProp.getSftpPmsUsername());
				context.set("sftp_pms_password", applicationProp.getSftpPmsPassword());
				context.set("sftp_pms_rootDir", applicationProp.getSftpPmsRootDir());
				pmsChannelSftp = (ChannelSftp) Component.getInstance("pmsChannelSftp");
				pmsChannelSftp.cd(applicationProp.getSftpPmsRootDir());
			} catch (SftpException e) {
				logger.error("Sftp command error");
				throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
			}
			
			String tempFileString;

			if (!SLASH.equals(StringUtils.substring(filePath, -1))) {

				tempFileString = tempDir + SLASH + fileName + DNLD_TRX_FILE_EXT;
				
				try {
					logger.debug("root directory #1, filePath #2 tempFileString #3 ", 
							 pmsChannelSftp.pwd(),
							 filePath,
							 tempFileString);
					pmsChannelSftp.get(filePath, tempFileString);
					fileArrayList.add(new File(tempFileString));
					
				} catch (SftpException e) {
					logger.error("Sftp command error");
					throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
				}
				
			} else {
				
				try {
					
					pmsChannelSftp.cd(filePath);
					List fileVector = pmsChannelSftp.ls(".");

					if (fileVector.size() != 0) {
						for (int i = 0; i < fileVector.size(); i++) {
							LsEntry lsEntry = (LsEntry) fileVector.get(i);

							if (!lsEntry.getFilename().equals(".")
									&& !lsEntry.getFilename().equals("..")) {

								tempFileString = tempDir + SLASH + lsEntry.getFilename();
								logger.debug("root directory #1, lsEntry.getFilename() #2 tempFileString #3", 
										 pmsChannelSftp.pwd(),
										 lsEntry.getFilename(),
										 tempFileString);
								pmsChannelSftp.get(lsEntry.getFilename(), tempFileString);
								fileArrayList.add(new File(tempFileString));
								
							}
						}
					}
					
				} catch (SftpException e) {
					logger.error("Sftp command error");
					throw new SftpCommandException(e, EXCEPTION_CLASS_NAME);
				}
				
			}
		}
	}

	public void dnldTrxFile() throws IOException, ZipException, SftpCommandException {

		if (fileArrayList.size() > 0) {
			File tempFile = printAgent.generateZipFile(fileArrayList, password);
			String contentId = reportProvider.storeReport(tempFile, ReportProvider.ZIP);
			FileUtils.deleteQuietly(tempFile);
			
			reportProvider.redirectReport(contentId, fileName);
		}
		
		if (tempDir.isDirectory()) {
			FileUtils.deleteDirectory(tempDir);
		}
		
	}
	
	private String removeFileExt(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf('.'));
	}

	private String getTrxFileDir(Date date) {
		return applicationProp.getDnldTrxDataSubDir() + SLASH + MONTH + SLASH + yearFormat.format(date) + SLASH + monthFormat.format(date) + SLASH + workstore.getHospCode();
	}

	@Remove
	public void destroy() {

		if (dailyTrxFileList != null) {
			dailyTrxFileList = null;
		}

		if (monthlyArchiveList != null) {
			monthlyArchiveList = null;
		}
	}

	public File getTempDir() {
		return tempDir;
	}

	public ArrayList<File> getFileArrayList() {
		return fileArrayList;
	}

	public void setFileArrayList(ArrayList<File> fileArrayList) {
		this.fileArrayList = fileArrayList;
	}
}
