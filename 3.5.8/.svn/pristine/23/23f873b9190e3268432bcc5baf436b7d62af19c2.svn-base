package hk.org.ha.control.pms.security {
	
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.utils.Timer;
	
	import mx.core.UIComponent;
	
	import hk.org.ha.event.pms.main.show.ShowStartupViewEvent;
	import hk.org.ha.event.pms.security.CloseAndLogoffEvent;
	import hk.org.ha.event.pms.security.DebugLogonEvent;
	import hk.org.ha.event.pms.security.LogoffEvent;
	import hk.org.ha.event.pms.security.PostLogonEvent;
	import hk.org.ha.event.pms.security.SaveLogonReasonEvent;
	import hk.org.ha.event.pms.security.show.ShowLogonReasonPopupEvent;
	import hk.org.ha.event.pms.security.show.ShowLogonSuccessEvent;
	import hk.org.ha.event.pms.security.show.ShowUnauthorizeWorkstationPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
	import hk.org.ha.fmk.pms.flex.components.window.Window;
	import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
	import hk.org.ha.fmk.pms.security.UamInfo;
	import hk.org.ha.model.pms.biz.security.AccessReasonServiceBean;
	import hk.org.ha.model.pms.biz.security.PostLogonServiceBean;
	import hk.org.ha.model.pms.persistence.reftable.Workstation;
	import hk.org.ha.model.pms.vo.security.PropMap;
	import hk.org.ha.model.pms.vo.sys.PostLogonResult;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.security.Identity;
	
	[Bindable]
	[Name("logonCtl")]
	public class LogonCtl 
	{
		
		private static const CLOSE_ON_LOGOFF:String = "closeOnLogOff";
		
		[In]
		public var postLogonService:PostLogonServiceBean;
		
		[In]
		public var accessReasonService:AccessReasonServiceBean;
		
		[In]
		public var identity:Identity; 
		
		[In]
		public var workstation:Workstation;
		
		[In]
		public var uamInfo:UamInfo;
		
		[In]
		public var permissionMap:PropMap;
		
		[In]
		public var window:Window;
		
		[In]
		public var closeMenuItem:MenuItem;
		
		[Out]
		public var sysMsgMap:SysMsgMap;
		
		private var debugLogonEvent:DebugLogonEvent;
				
		[Observer]
		public function debugLogon(evt:DebugLogonEvent):void 
		{
			identity.username = evt.username;
			identity.password = evt.password;
			debugLogonEvent = evt;
			identity.login(debugLogonResult);
		}
		
		private function debugLogonResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new PostLogonEvent(debugLogonEvent.hospCode, debugLogonEvent.workstoreCode));
		}
		
		[Observer]
		public function logoff(evt:LogoffEvent):void 
		{	
			ExternalInterface.call("logoff");
		}
		
		[Observer]
		public function postLogon(evt:PostLogonEvent):void {
			postLogonService.postLogon(evt.hospCode, evt.workstoreCode, postLogonResult);
		}
		
		private function postLogonResult(evt:TideResultEvent):void {

			sysMsgMap = (evt.result as PostLogonResult).sysMsgMap;
			
			if (workstation == null) {
				evt.context.dispatchEvent(new ShowUnauthorizeWorkstationPopupEvent());
				return;
			}
			
			if(permissionMap.getValueAsBoolean("logonReason"))
			{
				evt.context.dispatchEvent(new ShowLogonReasonPopupEvent());
				return;
			}

			evt.context.dispatchEvent(new ShowLogonSuccessEvent());
		}
		
		[Observer]
		public function saveLogonReason(evt:SaveLogonReasonEvent):void {
			accessReasonService.saveLogonReason(evt.reason, saveLogonReasonResult);
		}
		
		private function saveLogonReasonResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowLogonSuccessEvent());
		}
		
		[Observer]
		public function closeAndLogoff(evt:CloseAndLogoffEvent):void {
			for each (var menuItem:MenuItem in window.menuItemList) {
				if (menuItem.properties[CLOSE_ON_LOGOFF]) {
					closeWindow(menuItem, evt.caller);
					return;
				}
			}
			
			if (evt.waitFlag) {
				var timer:Timer = new Timer(500, 1);
				timer.addEventListener(TimerEvent.TIMER_COMPLETE, function():void {
					dispatchEvent(new LogoffEvent());
				});
				timer.start();
			}
			else {
				dispatchEvent(new LogoffEvent());
			}
		}
		
		private function closeWindow(menuItem:MenuItem, caller:UIComponent):void {
			if (window.currentMenuItem != menuItem) {
				dispatchEvent(new WindowSwitchEvent(menuItem, caller));
			}

			var timer:Timer = new Timer(1000, 1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, function():void {
				dispatchEvent(new WindowSwitchEvent(closeMenuItem, caller));
			});
			caller.callLater(caller.callLater, new Array(timer.start));
		}
	}
}