<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:tv="org.granite.tide.validators.*"
	xmlns:tsv="org.granite.tide.seam.validators.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("dueOrderScheduleTemplateMaintView")]
	</fx:Metadata>
	
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.reftable.mp.CheckDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent;
			import hk.org.ha.event.pms.main.reftable.mp.CopyDueOrderScheduleTemplateMaintDeliveryScheduleEvent;
			import hk.org.ha.event.pms.main.reftable.mp.DeleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent;
			import hk.org.ha.event.pms.main.reftable.mp.RefreshDueOrderScheduleTemplateMaintWardAndWardGroupListEvent;
			import hk.org.ha.event.pms.main.reftable.mp.RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent;
			import hk.org.ha.event.pms.main.reftable.mp.RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent;
			import hk.org.ha.event.pms.main.reftable.mp.UpdateDueOrderScheduleTemplateMaintDeliveryScheduleEvent;
			import hk.org.ha.event.pms.main.reftable.popup.DueOrderScheduleTemplateMaintCopyPopupProp;
			import hk.org.ha.event.pms.main.reftable.popup.DueOrderScheduleTemplateMaintItemPopupProp;
			import hk.org.ha.event.pms.main.reftable.popup.DueOrderScheduleTemplateMaintNewPopupProp;
			import hk.org.ha.event.pms.main.reftable.popup.ShowDueOrderScheduleTemplateMaintCopyPopupEvent;
			import hk.org.ha.event.pms.main.reftable.popup.ShowDueOrderScheduleTemplateMaintItemPopupEvent;
			import hk.org.ha.event.pms.main.reftable.popup.ShowDueOrderScheduleTemplateMaintNewPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.persistence.medprofile.DeliverySchedule;
			import hk.org.ha.model.pms.persistence.medprofile.DeliveryScheduleItem;
			import hk.org.ha.model.pms.udt.RecordStatus;
			import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemWardType;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			
			[In]
			public var permissionMap:PropMap;	
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[In]
			public var closeMenuItem:MenuItem;
			
			[In] [Bindable]
			public var dueOrderScheduleTemplateMaintWardSelectionList:ArrayCollection;
			
			[In] [Bindable]
			public var dueOrderScheduleTemplateMaintWardGroupSelectionList:ArrayCollection;
			
			[Bindable]
			private var backgroundColor:uint = 0xE7E5E5; // DataGridColumn non-editable backgroundColor
			
			[Bindable]
			public var deliveryScheduleList:ArrayCollection;
			
			[Bindable]
			public var selectedDeliverySchedule:DeliverySchedule;
			
			[Bindable]
			public var selectedDeliveryScheduleItemList:ArrayCollection = new ArrayCollection();
			
			[Bindable]
			public var delDeliveryScheduleItemList:ArrayCollection = new ArrayCollection();
			
			[Bindable]
			public var deliveryScheduleItemContextMenu:ContextMenu = new ContextMenu();
			protected var deliveryScheduleItemAddMenuItem:ContextMenuItem = new ContextMenuItem("Add Schedule", true);
			
			public static const PERMISSION_DUE_ORDER_SCHEDULE_TEMPLATE_MAINT_FULL_ACCESS:String = "dueOrderScheduleTemplateMaintFullAccess";
			
			// for setting saveMsg and deleteMsg in pmsToolbar
			private var dsDescModifiedFlag:Boolean = false;
			private var dsiAddDelFlag:Boolean = false;
			
			private static var ACTION_ADD:String = "add";
			private static var ACTION_UPDATE:String = "update";
			
			private static var STATE_CLEAR:String = "Clear";
			private static var STATE_NEW:String = "New";
			private static var STATE_RETRIEVE:String = "Retrieve";
			
			
			public override function onShow():void {
			}
			
			public override function onShowLater():void {
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.retrieveFunc = retrieveHandler;
					// pmsToolbar.saveMsg is set when call setDsiAddDelFlag()
					pmsToolbar.saveYesFunc = saveYesHandler;
					pmsToolbar.newFunc = newHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.copyFunc = copyHandler;
					// pmsToolbar.deleteMsg is set in retrieveHandler()
					pmsToolbar.deleteYesFunc = deleteYesHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					
					callLater(deliveryScheduleDdl.setFocus);
					focusManager.showFocus();
				}
				
				if (deliveryScheduleList == null) {
					stateChange(STATE_CLEAR);
					retrieveDeliveryScheduleList();
					
					// refresh ward/wardGroup list when dsList is refreshed, to ensure both list will never be refreshed during editing
					refreshDueOrderScheduleTemplateMaintWardAndWardGroupList();
				}
				
				callLater(disableMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showDueOrderLabelScheduleTemplateMaintShortcutKeyHandler);
			}
			
			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			private function showDueOrderLabelScheduleTemplateMaintShortcutKeyHandler():void {
				dispatchEvent(new ShowKeyGuidePopupEvent("DueOrderScheduleTemplateMaintView"));
			}
			
			private function disableMenuBarCloseButton():void {
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
			}
			
			public function refreshDueOrderScheduleTemplateMaintWardAndWardGroupList():void {
				dispatchEvent(new RefreshDueOrderScheduleTemplateMaintWardAndWardGroupListEvent());
			}
			
			public function retrieveDeliveryScheduleList():void {
				dispatchEvent(new RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleListEvent(postRetrieveDeliveryScheduleList));
			}
			
			public function postRetrieveDeliveryScheduleList(inDeliveryScheduleList:ArrayCollection):void {
				deliveryScheduleList = inDeliveryScheduleList;
				deliveryScheduleDdl.selectedIndex = -1;
				callLater(deliveryScheduleDdl.setFocus);
			}
			
			private function retrieveHandler():void {
				
				if (deliveryScheduleDdl.selectedIndex < 0) {
					// Please select template name.
					showSystemMessage("1044", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							callLater(deliveryScheduleDdl.setFocus);
						});
					return;
				}
				
				selectedDeliverySchedule = deliveryScheduleDdl.selectedItem as DeliverySchedule;
				
				// set deleteMsg for del handler
				// Are you sure to delete the current schedule template [#0]?
				pmsToolbar.deleteMsg = sysMsgMap.getToolbarConfirmationMessage("1039", new Array(selectedDeliverySchedule.name));
				
				retrieveDeliveryScheduleItemList(selectedDeliverySchedule.id);
			}
			
			public function retrieveDeliveryScheduleItemList(DeliveryScheduleId:Number):void {
				dispatchEvent(new RetrieveDueOrderScheduleTemplateMaintDeliveryScheduleItemListEvent(DeliveryScheduleId, postRetrieveDeliveryScheduleItemList));
			}
			
			public function postRetrieveDeliveryScheduleItemList(inDeliveryScheduleItemList:ArrayCollection):void {
				selectedDeliveryScheduleItemList = inDeliveryScheduleItemList;
				deliveryScheduleDescTxt.text = selectedDeliverySchedule.description;
				stateChange(STATE_RETRIEVE);
			}
			
			private function clearHandler():void {
				if (dsDescModifiedFlag || dsiAddDelFlag) {
					// Change(s) has been made without save. Continue to clear and abort the changes?
					showSystemMessageYesNoBtn("1036", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							clear();
							return;
						}
					);
				} else {
					clear();
				}
			}
			
			private function clear():void {
				// refresh deliveryScheduleList to clear unsaved deliverySchedule
				retrieveDeliveryScheduleList();
				stateChange(STATE_CLEAR);
			}
			
			private function closeHandler():void {
				if (dsDescModifiedFlag || dsiAddDelFlag) {
					// Change(s) has been made without save. Continue to close and abort the changes?
					showSystemMessageYesNoBtn("1074", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							close();
							return;
						}
					);
				} else {
					close();
				}
			}
			
			private function close():void {
				deliveryScheduleList = null;
				stateChange(STATE_CLEAR);
				dispatchEvent(new CloseActiveWindowEvent());
			}
			
			private function newHandler():void {
				
				var dueOrderScheduleTemplateMaintNewPopupProp:DueOrderScheduleTemplateMaintNewPopupProp = new DueOrderScheduleTemplateMaintNewPopupProp();
				dueOrderScheduleTemplateMaintNewPopupProp.okHandler = 
					function(deliveryScheduleName:String):void {
					
					var newDeliverySchedule:DeliverySchedule = new DeliverySchedule();
					newDeliverySchedule.name = deliveryScheduleName;
					newDeliverySchedule.description = '';
					newDeliverySchedule.status = RecordStatus.Active;
					
					deliveryScheduleList.addItem(newDeliverySchedule);
					deliveryScheduleDdl.selectedIndex = deliveryScheduleList.getItemIndex(newDeliverySchedule);
					
					selectedDeliverySchedule = newDeliverySchedule;
					
					newDeliverySchedule.deliveryScheduleItemList = new ArrayCollection();
					selectedDeliveryScheduleItemList = new ArrayCollection();
					
					stateChange(STATE_NEW);
				}
				dispatchEvent(new ShowDueOrderScheduleTemplateMaintNewPopupEvent(dueOrderScheduleTemplateMaintNewPopupProp));
			}
			
			// copy btn is enabled only when new deliverySchedle is created but not saved
			// and there is at least 1 deliverySchedule in dropdown
			private function copyHandler():void {
				
				var copyFromDeliveryScheduleList:ArrayCollection = ObjectUtil.copy(deliveryScheduleList) as ArrayCollection;
				// block from choosing current new unsaved deliverySchedule from copy selection
				for each (var currentDeliverySchedule:DeliverySchedule in copyFromDeliveryScheduleList) {
					if (isNaN(currentDeliverySchedule.id)) {
						copyFromDeliveryScheduleList.removeItemAt(copyFromDeliveryScheduleList.getItemIndex(currentDeliverySchedule));
						break;
					}
				}
				
				var dueOrderScheduleTemplateMaintCopyPopupProp:DueOrderScheduleTemplateMaintCopyPopupProp = new DueOrderScheduleTemplateMaintCopyPopupProp();
				dueOrderScheduleTemplateMaintCopyPopupProp.copyFromDeliveryScheduleList = copyFromDeliveryScheduleList;
				dueOrderScheduleTemplateMaintCopyPopupProp.okHandler = 
					function(copyFromDeliveryScheduleId:Number):void {
						copyDeliverySchedule(copyFromDeliveryScheduleId);
					}
				dispatchEvent(new ShowDueOrderScheduleTemplateMaintCopyPopupEvent(dueOrderScheduleTemplateMaintCopyPopupProp));
			}
			
			public function copyDeliverySchedule(copyFromDeliveryScheduleId:Number):void {
				dispatchEvent(new CopyDueOrderScheduleTemplateMaintDeliveryScheduleEvent(copyFromDeliveryScheduleId, postCopyDeliverySchedule));
			}
			
			public function postCopyDeliverySchedule(inDeliveryScheduleItemList:ArrayCollection):void {
				if (inDeliveryScheduleItemList != null && inDeliveryScheduleItemList.length > 0) {
					for each (var item:DeliveryScheduleItem in inDeliveryScheduleItemList) {
						// base entity, invalid ward/wardGroup of deliveryScheduleItem are removed at backend
						item.deliverySchedule = selectedDeliverySchedule;
					}
					setDsiAddDelFlag();
					selectedDeliveryScheduleItemList.addAll(inDeliveryScheduleItemList);
					selectAndScrollToLastDsi();
				}
			}
			
			private function saveYesHandler():void  {
				
				// block saving template if no item
				if (selectedDeliveryScheduleItemList.length == 0) {
					// Please add at least one schedule.
					showSystemMessage("1043", 
						function(evt:MouseEvent):void {
							PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
							callLater(deliveryScheduleItemGrid.setFocus);
						});
					return;
				}
				
				
				// need to validate ward/wardGroup csv once
				// when deliveryScheduleItem is copied from other template, 
				// it is possible that no valid ward/wardGroup remained in this item
				for each (var deliveryScheduleItem:DeliveryScheduleItem in selectedDeliveryScheduleItemList) {
					// copied ds item must be not saved before
					// only non-saved ds item is required to be validated
					if (isNaN(deliveryScheduleItem.id) && deliveryScheduleItem.wardType != null && deliveryScheduleItem.wardType != DeliveryScheduleItemWardType.AllWard) {
						if (deliveryScheduleItem.wardType == DeliveryScheduleItemWardType.Ward && (deliveryScheduleItem.wardCodeCsv == null || deliveryScheduleItem.wardCodeCsv == "")) {
							// Please select ward.
							showSystemMessage("0284", 
								function(evt:MouseEvent):void {
									PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
									
									deliveryScheduleItemGrid.selectedItem = deliveryScheduleItem as DeliveryScheduleItem;
									callLater(deliveryScheduleItemGrid.setFocus);
								});
							return;
						} else if (deliveryScheduleItem.wardType == DeliveryScheduleItemWardType.WardGroup && (deliveryScheduleItem.wardGroupCsv == null || deliveryScheduleItem.wardGroupCsv == "")) {
							// Please select ward group.
							showSystemMessage("0716", 
								function(evt:MouseEvent):void {
									PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
									
									deliveryScheduleItemGrid.selectedItem = deliveryScheduleItem as DeliveryScheduleItem;
									callLater(deliveryScheduleItemGrid.setFocus);
								});
							return;
						}
					}
				}
				
				// duplicate name checking is only for new template
				// check duplicate template name again before save
				// user is required to clear the template for re-inputting a new name
				if (isNaN(selectedDeliverySchedule.id)) {
					dispatchEvent(new CheckDueOrderScheduleTemplateMaintDeliveryScheduleNameExistEvent(selectedDeliverySchedule.name, 
						function (deliveryScheduleNameExist:Boolean):void {
							if (deliveryScheduleNameExist) {
								// Template name already exists.
								showSystemMessage("1037", 
									function(evt:MouseEvent):void {
										PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
										callLater(deliveryScheduleDdl.setFocus);
									});
								return;
							} else {
								postSaveHandler();
							}
						}
					));
				} else {
					postSaveHandler();
				}
			}
			
			public function postSaveHandler():void {
				selectedDeliverySchedule.description = deliveryScheduleDescTxt.text;
				// selectedDeliveryScheduleItemList is the dataProvider for grid
				// new list is used to pass to backend to avoid reappearence of deleted item upon clicking save
				var updateDeliveryScheduleItemList:ArrayCollection = new ArrayCollection();
				updateDeliveryScheduleItemList.addAll(selectedDeliveryScheduleItemList);
				updateDeliveryScheduleItemList.addAll(delDeliveryScheduleItemList);
				dispatchEvent(new UpdateDueOrderScheduleTemplateMaintDeliveryScheduleEvent(selectedDeliverySchedule, updateDeliveryScheduleItemList, showSaveSuccessMessage));
			}
			
			// delete toolabr btn only be enabled after retrivial
			// schedule to del must be saved before
			// pmsToolbar.deleteMsg is set in retrieveHandler()
			private function deleteYesHandler():void {
				dispatchEvent(new DeleteDueOrderScheduleTemplateMaintDeliveryScheduleEvent(selectedDeliverySchedule.id, postRetrieveDeliveryScheduleList));
				
				refreshDueOrderScheduleTemplateMaintWardAndWardGroupList();
				stateChange(STATE_CLEAR);
			}
			
			protected function initContextMenu():void  { 
				deliveryScheduleItemAddMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, addDeliveryScheduleItem);				
				deliveryScheduleItemContextMenu.hideBuiltInItems();
				deliveryScheduleItemContextMenu.customItems = [deliveryScheduleItemAddMenuItem];
				// for non fullAccess, context menu, add item is not enabled
				deliveryScheduleItemContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, updateDeliveryScheduleItemContextMenu);
				deliveryScheduleItemVGroup.contextMenu = deliveryScheduleItemContextMenu;
			}
			
			private function updateDeliveryScheduleItemContextMenu(evt:ContextMenuEvent):void {
				deliveryScheduleItemAddMenuItem.enabled = (isFullAccess() && pmsToolbar.saveButton.enabled);
			}
			
			// for non fullAccess, context menu add item is not enabled
			private function addDeliveryScheduleItem(evt:ContextMenuEvent):void {
				showDeliveryScheduleItemPopup(ACTION_ADD);
			}
			
			private function showDeliveryScheduleItemPopup(action:String):void {
				var dueOrderScheduleTemplateMaintItemPopupProp:DueOrderScheduleTemplateMaintItemPopupProp = new DueOrderScheduleTemplateMaintItemPopupProp();
				var targetDeliveryScheduleItem:DeliveryScheduleItem = null;
				
				if ( action == ACTION_UPDATE ) {
					// wardSelectionList and wardGroupSelectionList is only a base for lookup
					// these lists content will not be modified
					targetDeliveryScheduleItem = deliveryScheduleItemGrid.selectedItem as DeliveryScheduleItem;
				} else {
					targetDeliveryScheduleItem = new DeliveryScheduleItem();
				}
				
				dueOrderScheduleTemplateMaintItemPopupProp.action = action;
				dueOrderScheduleTemplateMaintItemPopupProp.deliveryScheduleItem = targetDeliveryScheduleItem;
				dueOrderScheduleTemplateMaintItemPopupProp.activeWardSelectionList = dueOrderScheduleTemplateMaintWardSelectionList;
				dueOrderScheduleTemplateMaintItemPopupProp.wardGroupSelectionList = dueOrderScheduleTemplateMaintWardGroupSelectionList;
				
				dueOrderScheduleTemplateMaintItemPopupProp.okHandler = function(action:String, deliveryScheduleItem:DeliveryScheduleItem):void {
					
					if ( action == ACTION_ADD ) {
						
						setDsiAddDelFlag();
						
						deliveryScheduleItem.deliverySchedule = selectedDeliverySchedule;
						selectedDeliveryScheduleItemList.addItem(deliveryScheduleItem);
						selectAndScrollToLastDsi();
						
					} else if ( action == ACTION_UPDATE ) {
						
						targetDeliveryScheduleItem.scheduleTimeStr = deliveryScheduleItem.scheduleTimeStr;
						targetDeliveryScheduleItem.highlightFlag = deliveryScheduleItem.highlightFlag;
						targetDeliveryScheduleItem.orderType = deliveryScheduleItem.orderType;
						targetDeliveryScheduleItem.dueHourStr = deliveryScheduleItem.dueHourStr;
						targetDeliveryScheduleItem.dueOnType = deliveryScheduleItem.dueOnType;
						targetDeliveryScheduleItem.generateDay = deliveryScheduleItem.generateDay;
						targetDeliveryScheduleItem.wardType = deliveryScheduleItem.wardType;
						
						// wardList and wardGroupList is saved in csv form
						targetDeliveryScheduleItem.wardCodeCsv = deliveryScheduleItem.wardCodeCsv;
						targetDeliveryScheduleItem.wardGroupCsv = deliveryScheduleItem.wardGroupCsv;
						
						targetDeliveryScheduleItem.statFlag = deliveryScheduleItem.statFlag;
						targetDeliveryScheduleItem.urgentFlag = deliveryScheduleItem.urgentFlag;
						targetDeliveryScheduleItem.overdueFlag = deliveryScheduleItem.overdueFlag;
					}
					callLater(deliveryScheduleItemGrid.setFocus);
				}
				dispatchEvent(new ShowDueOrderScheduleTemplateMaintItemPopupEvent(dueOrderScheduleTemplateMaintItemPopupProp));
			}
			
			private function selectAndScrollToLastDsi():void {
				// selectedDeliveryScheduleItemList will not be empty at here
				// template can only be saved with at least 1 dsi, so copied template contains at least 1 dsi
				var lastDsiIndex:Number = selectedDeliveryScheduleItemList.length - 1;
				deliveryScheduleItemGrid.selectedItem = selectedDeliveryScheduleItemList.getItemAt(lastDsiIndex);
				deliveryScheduleItemGrid.verticalScrollPosition = deliveryScheduleItemGrid.maxVerticalScrollPosition;
			}
			
			// new, edit, delete, add item, del item
			public function isFullAccess():Boolean {
				return permissionMap.getValueAsBoolean(DueOrderScheduleTemplateMaintView.PERMISSION_DUE_ORDER_SCHEDULE_TEMPLATE_MAINT_FULL_ACCESS);
			}
			
			private function setDsiAddDelFlag():void {
				if (!dsiAddDelFlag) {
					dsiAddDelFlag = true;
					// set pmsToolbar.saveMsg
					// Schedule(s) has been updated. Continue to save?
					pmsToolbar.saveMsg = sysMsgMap.getToolbarConfirmationMessage("1038", null);
				}
			}
			
			public function showSaveSuccessMessage(inDeliveryScheduleList:ArrayCollection):void {
				// Save successfully.
				showSystemMessage("0446", 
					function saveSuccessOkHandler(evt:MouseEvent):void {
						PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
						
						postRetrieveDeliveryScheduleList(inDeliveryScheduleList);
						refreshDueOrderScheduleTemplateMaintWardAndWardGroupList();
						stateChange(STATE_CLEAR);
					});
			}
			
			public function showSystemMessage(errorCode:String, okfunc:Function=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				if ( okfunc != null ) {
					msgProp.okHandler = okfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function showSystemMessageYesNoBtn(errorCode:String, yesfunc:Function=null, param:Array=null):void {
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setYesNoButton = true;
				msgProp.setYesDefaultButton = false;
				msgProp.setNoDefaultButton = true;
				if ( param != null ) {
					msgProp.messageParams = param;
				}
				
				if (yesfunc != null) {
					msgProp.yesHandler = yesfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function stateChange(state:String):void {
				switch (state) {
					case STATE_CLEAR: // initialized
						if (!isFullAccess()) {
							pmsToolbar.retrieveButton.enabled = true;
							pmsToolbar.saveButton.enabled = false;
							pmsToolbar.clearButton.enabled = true;
							pmsToolbar.newButton.enabled = false;
							pmsToolbar.copyButton.enabled = false;
							pmsToolbar.deleteButton.enabled = false;
						} else {
							pmsToolbar.retrieveButton.enabled = true;
							pmsToolbar.saveButton.enabled = false;
							pmsToolbar.clearButton.enabled = true;
							pmsToolbar.newButton.enabled = true;
							pmsToolbar.copyButton.enabled = false;
							pmsToolbar.deleteButton.enabled = false;
						}
						
						// reset saveMsg and deleteMsg
						pmsToolbar.saveMsg = null;
						pmsToolbar.deleteMsg = null;
						
						dsDescModifiedFlag = false;
						dsiAddDelFlag = false;
						
						deliveryScheduleDdl.enabled = true;
						deliveryScheduleDdl.selectedIndex = -1;
						
						deliveryScheduleDescTxt.enabled = false;
						deliveryScheduleDescTxt.text = '';
						
						deliveryScheduleItemGrid.enabled = false;
						
						callLater(deliveryScheduleDdl.setFocus);
						
						selectedDeliverySchedule = null;
						selectedDeliveryScheduleItemList.removeAll();
						delDeliveryScheduleItemList.removeAll();
						
						break;
					
					case STATE_NEW: // creating new template
						// new btn will not be enabled without fullAccess
						pmsToolbar.retrieveButton.enabled = false;
						pmsToolbar.saveButton.enabled = true;
						pmsToolbar.clearButton.enabled = true;
						pmsToolbar.newButton.enabled = false;
						
						// new unsaved deliverySchedule is included in deliveryScheduleList
						if (deliveryScheduleList != null && deliveryScheduleList.length > 1) {
							pmsToolbar.copyButton.enabled = true;
						} else {
							// no active saved deliverySchedule is provided for copying
							// unsaved deliverySchedule cannot be selected for copying
							pmsToolbar.copyButton.enabled = false;
						}
						
						pmsToolbar.deleteButton.enabled = false;
						
						// deliveryScheduleDdl index is set already upon template name is input
						deliveryScheduleDdl.enabled = false;
						
						deliveryScheduleDescTxt.enabled = true;
						deliveryScheduleDescTxt.text = '';
						
						deliveryScheduleItemGrid.enabled = true;
						
						callLater(deliveryScheduleItemGrid.setFocus);
						
						break;
					
					case STATE_RETRIEVE: // editing saved record
						if (!isFullAccess()) {
							pmsToolbar.retrieveButton.enabled = false;
							pmsToolbar.saveButton.enabled = false;
							pmsToolbar.clearButton.enabled = true;
							pmsToolbar.newButton.enabled = false;
							pmsToolbar.copyButton.enabled = false;
							pmsToolbar.deleteButton.enabled = false;
							
							deliveryScheduleDescTxt.enabled = false;
						} else {
							pmsToolbar.retrieveButton.enabled = false;
							pmsToolbar.saveButton.enabled = true;
							pmsToolbar.clearButton.enabled = true;
							pmsToolbar.newButton.enabled = false;
							pmsToolbar.copyButton.enabled = false;
							pmsToolbar.deleteButton.enabled = true;
							
							deliveryScheduleDescTxt.enabled = true;
						}
						
						deliveryScheduleDdl.enabled = false;
						deliveryScheduleItemGrid.enabled = true;
						
						if (selectedDeliveryScheduleItemList != null && selectedDeliveryScheduleItemList.length > 0) {
							deliveryScheduleItemGrid.selectedIndex = 0;
						}
						
						callLater(deliveryScheduleItemGrid.setFocus);
						
						break;
				}
			}
			
			public function scheduleTimeLabelFunction(item:Object, column:DataGridColumn):String {
				if ( item != null && item.scheduleTimeStr != null && item.scheduleTimeStr.length == 4 ) {
					return item.scheduleTimeStr.substr(0, 2) + ":" + item.scheduleTimeStr.substr(2);
				}
				return "";
			}
			
			public function orderTypeLabelFunction(item:DeliveryScheduleItem, column:DataGridColumn):String {
				if (item != null && item.orderType != null) {
					return item.orderType.displayValue;
				}
				return "";
			}
			
			public function dueTimeLabelFunction(item:DeliveryScheduleItem, column:DataGridColumn):String {
				if (item != null && item.dueHourStr != null) {
					return item.dueHourStr + ":59";
				}
				return "";
			}
			
			public function dueOnTypeLabelFunction(item:DeliveryScheduleItem, column:DataGridColumn):String {
				if (item != null && item.dueOnType != null) {
					return item.dueOnType.displayValue;
				}
				return "";
			}
			
			public function wardTypeLabelFunction(item:DeliveryScheduleItem, column:DataGridColumn):String {
				if (item != null && item.wardType != null) {
					return item.wardType.displayValue;
				}
				return "";
			}
			
			public function wardInfoLabelFunc(item:Object, column:DataGridColumn):String {
				if (item != null && item.wardType != null) {
					if (item.wardType == DeliveryScheduleItemWardType.WardGroup) {
						return item.wardGroupCsv;
					} else if (item.wardType == DeliveryScheduleItemWardType.Ward) {
						return item.wardCodeCsv;
					} // else AllWard, show empty
				}
				return "";
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( !PopupUtil.isAnyVisiblePopupExists() ) {
					if (evt.ctrlKey) {
						if ( evt.charCode == 55 && pmsToolbar.saveButton.enabled){ // Ctrl + 7 - save
							dispatchEvent(new ShortcutKeyEvent(pmsToolbar.saveButton));
						} else if ( evt.charCode == 48 && pmsToolbar.closeButton.enabled){ // Ctrl + 0 - close
							dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
						} else if ( evt.charCode == 52 && pmsToolbar.clearButton.enabled){ // Ctrl + 4 - clear
							dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
						} 
					}
				}
			}
			
			public function deliveryScheduleDescTxtChangeHandler():void {
				if (!dsDescModifiedFlag) {
					dsDescModifiedFlag = true;
				}
			}
			
			protected function deliveryScheduleItemGrid_keyUpHandler(evt:KeyboardEvent):void {
				if (!isFullAccess() || PopupUtil.isAnyVisiblePopupExists()) {
					return;
				}
				
				if (evt.charCode == 13) { // Enter
					if (deliveryScheduleItemGrid.selectedItem != null && isNaN(deliveryScheduleItemGrid.selectedItem.id)) {
						showDeliveryScheduleItemPopup(ACTION_UPDATE);
					}
				}
				
				if (evt.ctrlKey) {
					if ( evt.keyCode == Keyboard.INSERT ) { // Ctrl + Insert
						showDeliveryScheduleItemPopup(ACTION_ADD);
					} else if (deliveryScheduleItemGrid.selectedItem != null && evt.keyCode == Keyboard.DELETE) { // Ctrl + Delete
						deleteDeliveryScheduleItemHandler();
					}
				}
			}
			
			protected function deliveryScheduleDdlKeyDownHandler(event:KeyboardEvent):void {
				if( event.keyCode == 13 && pmsToolbar.retrieveButton.enabled == true){
					retrieveHandler();
				}
			}
			
			protected function deliveryScheduleItemGrid_itemDoubleClickHandler(event:ListEvent):void {
				if (isFullAccess() && isNaN(deliveryScheduleItemGrid.selectedItem.id)) {
					showDeliveryScheduleItemPopup(ACTION_UPDATE);
				}
			}
			
			private function deleteDeliveryScheduleItemHandler():void {
				if ( deliveryScheduleItemGrid.selectedIndex < 0) {
					return;
				}
				
				var delDeliveryScheduleItem:DeliveryScheduleItem = deliveryScheduleItemGrid.selectedItem as DeliveryScheduleItem;
				
				setDsiAddDelFlag();
				
				if (!isNaN(delDeliveryScheduleItem.id)) {
					delDeliveryScheduleItem.status = RecordStatus.Delete;
					delDeliveryScheduleItemList.addItem(delDeliveryScheduleItem);
				}
				selectedDeliveryScheduleItemList.removeItemAt(selectedDeliveryScheduleItemList.getItemIndex(delDeliveryScheduleItem));
			}
			
			private function deliveryScheduleItemGridRowColorFunc(item:*, rowIndex:int, dataIndex:int, color:uint):uint {
				if (!isNaN(item.id)) {
					return backgroundColor;
				} else {
					return color;
				}
			}
			
		]]>
		
	</fx:Script>
	
	
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	<fc:Toolbar id="pmsToolbar"	width="100%"/>
	
	<s:VGroup paddingLeft="10" paddingTop="10" paddingRight="10" paddingBottom="10" gap="10" width="100%" height="100%" enabled="true">
				<s:HGroup width="100%" verticalAlign="middle">
					<s:Label text="Template Name" verticalAlign="middle" width="90"/>
					<s:Group toolTip="{(deliveryScheduleDdl.selectedItem as DeliverySchedule).name}">
						<fc:AdvancedDropDownList id="deliveryScheduleDdl" 
												 dataProvider="{deliveryScheduleList}" 
												 labelField="name" width="350" keyDown="deliveryScheduleDdlKeyDownHandler(event)"/>		
					</s:Group>
					<s:Group toolTip="{deliveryScheduleDescTxt.text}">
						<fc:ExtendedTextInput id="deliveryScheduleDescTxt" width="543" maxChars="100" enabled="false" change="deliveryScheduleDescTxtChangeHandler()"/>
					</s:Group>
				</s:HGroup>
				<s:VGroup id="deliveryScheduleItemVGroup" width="100%" height="100%" paddingLeft="0" paddingRight="0" paddingTop="0" paddingBottom="0" gap="0"
						  mouseEnabled="true" creationComplete="initContextMenu()">
					<s:Panel title="Due Order Label Generation Schedule List" width="100%" height="100%">
						<fc:ExtendedDataGrid id="deliveryScheduleItemGrid" width="100%" height="100%"  dataProvider="{selectedDeliveryScheduleItemList}"
											 draggableColumns="false" horizontalScrollPolicy="auto" sortableColumns="false" resizableColumns="false"
											 doubleClickEnabled="true" variableRowHeight="true" 
											 itemDoubleClick="deliveryScheduleItemGrid_itemDoubleClickHandler(event)"
											 keyUp="deliveryScheduleItemGrid_keyUpHandler(event)"
											 rowColorFunction="deliveryScheduleItemGridRowColorFunc">
							<fc:columns>
								<mx:DataGridColumn id="scheduleTimeCol" headerText="Schedule&#10;Time" dataField="scheduleTimeStr" width="{deliveryScheduleItemGrid.width*0.07}">
								<mx:itemRenderer>
									<fx:Component>
										<s:MXDataGridItemRenderer autoDrawBackground="false">
											<s:layout>
												<s:HorizontalLayout verticalAlign="middle" horizontalAlign="center"/>
											</s:layout>
											<s:Label paddingLeft="5" text="{outerDocument.scheduleTimeLabelFunction(data, outerDocument.scheduleTimeCol)}" styleName="{data.highlightFlag ? 'dueOrderHighlightScheduleStyle' : ''}" width="100%" paddingRight="3" verticalAlign="middle" height="100%"/>
										</s:MXDataGridItemRenderer>
									</fx:Component>
								</mx:itemRenderer>
								</mx:DataGridColumn>
								<mx:DataGridColumn headerText="Order Type" dataField="orderType" labelFunction="orderTypeLabelFunction" width="{deliveryScheduleItemGrid.width*0.09}"/>
								<mx:DataGridColumn headerText="Due&#10;Time" dataField="dueHourStr" labelFunction="dueTimeLabelFunction" width="{deliveryScheduleItemGrid.width*0.05}"/>
								<mx:DataGridColumn headerText="Due Time on" dataField="dueOnType" labelFunction="dueOnTypeLabelFunction" width="{deliveryScheduleItemGrid.width*0.15}"/>
								<mx:DataGridColumn headerText="No. of Day(s) of&#10;Refill Generated" textAlign="right" dataField="generateDay" width="{deliveryScheduleItemGrid.width*0.1}"/>
								<mx:DataGridColumn headerText="Ward" dataField="wardType" labelFunction="wardTypeLabelFunction" width="{deliveryScheduleItemGrid.width*0.08}"/>
								<mx:DataGridColumn id="wardInfoCol" headerText="Ward Information" headerWordWrap="true" sortable="false" width="{deliveryScheduleItemGrid.width*0.222}">
									<mx:itemRenderer>
										<fx:Component>
											<s:MXDataGridItemRenderer autoDrawBackground="false">
												<s:layout>
													<s:HorizontalLayout horizontalAlign="left" verticalAlign="middle"/>
												</s:layout>
												<s:Label paddingLeft="5" text="{outerDocument.wardInfoLabelFunc(data, outerDocument.wardInfoCol)}" styleName="{data.edited?'boldTextStyle':''}" showTruncationTip="true" maxDisplayedLines="1" width="{outerDocument.wardInfoCol.width}"/>
											</s:MXDataGridItemRenderer>
										</fx:Component>
									</mx:itemRenderer>
								</mx:DataGridColumn>
								<mx:DataGridColumn headerText="Stat" dataField="statFlag" editable="false" sortable="false" resizable="false" width="{deliveryScheduleItemGrid.width*0.04}">
									<mx:itemRenderer>
										<fx:Component>
											<s:MXDataGridItemRenderer autoDrawBackground="false">
												<s:layout>
													<s:HorizontalLayout verticalAlign="middle" horizontalAlign="center"/>
												</s:layout>
												<s:CheckBox id="statCbx" selected="@{data.statFlag}" enabled="false"/>
											</s:MXDataGridItemRenderer>
										</fx:Component>
									</mx:itemRenderer>
								</mx:DataGridColumn>
								<mx:DataGridColumn headerText="Urgent" dataField="urgentFlag" editable="false" sortable="false" resizable="false" width="{deliveryScheduleItemGrid.width*0.05}" >
									<mx:itemRenderer>
										<fx:Component>
											<s:MXDataGridItemRenderer autoDrawBackground="false">
												<s:layout>
													<s:HorizontalLayout verticalAlign="middle" horizontalAlign="center"/>
												</s:layout>
												<s:CheckBox id="urgentCbx" selected="@{data.urgentFlag}" enabled="false"/>
											</s:MXDataGridItemRenderer>
										</fx:Component>
									</mx:itemRenderer>
								</mx:DataGridColumn>
								<mx:DataGridColumn headerText="Overdue" dataField="overdueFlag" editable="false" sortable="false" resizable="false" width="{deliveryScheduleItemGrid.width*0.06}" >
									<mx:itemRenderer>
										<fx:Component>
											<s:MXDataGridItemRenderer autoDrawBackground="false">
												<s:layout>
													<s:HorizontalLayout verticalAlign="middle" horizontalAlign="center"/>
												</s:layout>
												<s:CheckBox id="overDueCbx" selected="@{data.overdueFlag}" enabled="false"/>
											</s:MXDataGridItemRenderer>
										</fx:Component>
									</mx:itemRenderer>
								</mx:DataGridColumn>
								<mx:DataGridColumn headerText="Highlight&#10;Schedule" dataField="highlightFlag" editable="false" sortable="false" resizable="false" width="{deliveryScheduleItemGrid.width*0.07}" >
									<mx:itemRenderer>
										<fx:Component>
											<s:MXDataGridItemRenderer autoDrawBackground="false">
												<s:layout>
													<s:HorizontalLayout verticalAlign="middle" horizontalAlign="center"/>
												</s:layout>
												<s:CheckBox id="highlightFlag" selected="@{data.highlightFlag}" enabled="false"/>
											</s:MXDataGridItemRenderer>
										</fx:Component>
									</mx:itemRenderer>
								</mx:DataGridColumn>
							</fc:columns>
						</fc:ExtendedDataGrid>
					</s:Panel>
					<s:BorderContainer width="100%" height="20">
						<s:HGroup width="100%" horizontalAlign="left" paddingLeft="5" paddingTop="5">
							<s:Label text="&lt;Ctrl+Insert&gt; to add record; &lt;Ctrl+Delete&gt; to delete record" styleName="footNoteLabelStyle"/>
						</s:HGroup>
					</s:BorderContainer>
				</s:VGroup>
	</s:VGroup>
</fc:ExtendedNavigatorContent>