<?xml version="1.0" encoding="utf-8"?>
<s:TitleWindow xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
			   xmlns:mp="hk.org.ha.view.pms.main.medprofile.*"
			   xmlns:v="hk.org.ha.view.pms.main.vetting.mp.*"
			   xmlns:fx="http://ns.adobe.com/mxml/2009" 
			   xmlns:s="library://ns.adobe.com/flex/spark" 
			   xmlns:mx="library://ns.adobe.com/flex/mx"
			   xmlns:m="hk.org.ha.fmk.pms.flex.components.message.*"
			   title="Merge Formula"
			   mouseEnabled="true">
	<fx:Metadata>
		[Name("pivasMergeFormulaEditMergeFormulaDetailsPopup")]
	</fx:Metadata>
	
	<fx:Declarations>
		<s:ArrayCollection id="pivasFormulaMethodList" list="{pivasFormula.pivasFormulaMethodList}" filterFunction="pivasFormulaMethodListFilterFunction"/>
		<m:SystemMessagePopupProp id="missingContainerMsgPopupProp" messageCode="0850" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="missingPrintNameMsgPopupProp" messageCode="0853" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="printNameTooLongMsgPopupProp" messageCode="1017" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="missingPrepMethodMsgPopupProp" messageCode="0842" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="invalidCorePrepMethodMsgPopupProp" messageCode="0840" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="noCorePrepMethodMsgPopupProp" messageCode="0839" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="duplicateMethodMsgPopupProp" messageCode="0912" setOkButtonOnly="true"/>
		<m:SystemMessagePopupProp id="confirmSaveFormulaMsgPopupProp" messageCode="0304" setOkCancelButton="true" okHandler="confirmSaveFormulaStep2"/>
		<m:SystemMessagePopupProp id="confirmNoApplyToMsgPopupProp" messageCode="0838" setOkCancelButton="true" okHandler="proceedSaveFormula"/>
		<m:SystemMessagePopupProp id="duplicateFormulaMsgPoupProp" messageCode="1019" setYesNoButton="true" setYesDefaultButton="false" setNoDefaultButton="true"
								  yesHandler="deleteDuplicateFormula"/>
		<fx:Component id="itemHqSuspendMsgPopupPropFactory">
			<m:SystemMessagePopupProp messageCode="0847" setOkButtonOnly="true" okHandler="{outerDocument.resetCoreFlag}"/>
		</fx:Component>
		<fx:Component id="itemLocalSuspendMsgPopupPropFactory">
			<m:SystemMessagePopupProp messageCode="0846" setOkButtonOnly="true" okHandler="{outerDocument.resetCoreFlag}"/>
		</fx:Component>
		<fx:Component id="confirmDeleteMethodMsgPopupPropFactory">
			<m:SystemMessagePopupProp messageCode="1014" setYesNoButton="true" setYesDefaultButton="false" setNoDefaultButton="true"/>
		</fx:Component>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.controls.dataGridClasses.DataGridColumn;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.components.supportClasses.ItemRenderer;
			
			import hk.org.ha.event.pms.main.drug.RetrieveDmWarningListEvent;
			import hk.org.ha.event.pms.main.reftable.pivas.DeletePivasFormulaEvent;
			import hk.org.ha.event.pms.main.reftable.pivas.UpdatePivasFormulaEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasContainer;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrug;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManuf;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasDrugManufMethod;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormula;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaConfig;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaDrug;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethod;
			import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaMethodDrug;
			import hk.org.ha.model.pms.dms.udt.pivas.AckStatus;
			import hk.org.ha.model.pms.dms.udt.pivas.ExpiryDateTitle;
			import hk.org.ha.model.pms.dms.udt.pivas.LabelOptionType;
			import hk.org.ha.model.pms.dms.udt.pivas.PivasFormulaMethodStatus;
			import hk.org.ha.model.pms.dms.udt.pivas.TargetType;
			import hk.org.ha.model.pms.vo.drug.DmDrugLite;
			import hk.org.ha.model.pms.vo.security.PropMap;
			import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
			import hk.org.ha.view.pms.main.reftable.pivas.PivasFormulaMaintView;
			
			import org.granite.util.Enum;
			
			
			private static const labelColumnWidth:int = 80;

			public const CORE_COLUMN_WIDTH:int = 40;
			
			public const LOCAL_SUSPEND_COLUMN_WIDTH:int = 20;
			
			public const ITEM_CODE_COLUMN_WIDTH:int = 80;
			
			public const MANUF_COLUMN_WIDTH:int = 60;
			
			public const ITEM_DESC_COLUMN_WIDTH:int = 430;
			
			public const REQUIRED_VOLUME_COLUMN_WIDTH:int = 100;
			
			public const DILUENT_COLUMN_WIDTH:int = 100;
			
			public const SHELF_LIFE_COLUMN_WIDTH:int = 80;
			
			public const STATUS_COLUMN_WIDTH:int = 80;
			
			[Bindable]
			public var isLoading:Boolean;
			
			[In] [Bindable]
			public var permissionMap:PropMap;
			
			[Bindable]
			public var pivasFormula:PivasFormula;
			
			[Bindable]
			public var pivasFormulaDrugList:ArrayCollection;
			
			[Bindable]
			public var pivasFormulaConfig:PivasFormulaConfig;
			
			[Bindable]
			public var duplicateFormulaExists:Boolean;
			
			[Bindable]
			public var dmDrugLiteMap:Object;
			
			[In]
			[Bindable]
			public var pivasContainerList:ListCollectionView;

			public var successUpdateFormulaFunction:Function;
			
			public var successDeleteDuplicateFormulaFunction:Function;
			
			public var cancelFunction:Function;
			
			private var addPreparationMethodMenuItem:ContextMenuItem = new ContextMenuItem("Add Preparation Method");		
			
			public function setupUI(pivasFormula:PivasFormula, pivasFormulaDrugList:ArrayCollection, pivasFormulaConfig:PivasFormulaConfig, duplicateFormulaExists:Boolean, dmDrugLiteMap:Object,
									successUpdateFormulaFunction:Function, successDeleteDuplicateFormulaFunction:Function, cancelFunction:Function):void
			{
				this.pivasFormula = pivasFormula;
				this.pivasFormulaDrugList = pivasFormulaDrugList;
				this.pivasFormulaConfig = pivasFormulaConfig;
				this.duplicateFormulaExists = duplicateFormulaExists;
				this.dmDrugLiteMap = dmDrugLiteMap;
				this.successUpdateFormulaFunction = successUpdateFormulaFunction;
				this.successDeleteDuplicateFormulaFunction = successDeleteDuplicateFormulaFunction;
				this.cancelFunction = cancelFunction;
				PopUpManager.centerPopUp(this);
				if (printNameInput.editable) {
					printNameInput.setFocus();
				} else {
					containerDropDownList.setFocus();
					containerDropDownList.drawFocus(true);
				}
				dispatchEvent(new RetrieveDmWarningListEvent());
				if (duplicateFormulaExists) {
					dispatchEvent(new RetrieveSystemMessageEvent(duplicateFormulaMsgPoupProp));
				} else {
					checkCoreMethodItemSuspend(pivasFormula, dmDrugLiteMap);
				}
				initContextMenu();
			}
			
			private function initContextMenu():void
			{			
				addPreparationMethodMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, addPrepMethod);
				var menu:ContextMenu = new ContextMenu();
				menu.hideBuiltInItems();
				menu.customItems = [addPreparationMethodMenuItem];
				preparationMethodPanel.contextMenu = menu;
			}
			
			private function deleteDuplicateFormula(evt:Event):void
			{
				MedProfileUtils.removePopup(evt);
				dispatchEvent(new DeletePivasFormulaEvent(pivasFormula, finishDeleteDuplicateFormula, function():void {}));
			}
			
			private function finishDeleteDuplicateFormula():void
			{
				PopUpManager.removePopUp(this);
				successDeleteDuplicateFormulaFunction();
			}
			
			private function checkCoreMethodItemSuspend(pivasFormula:PivasFormula, dmDrugLiteMap:Object):void
			{
				if (dmDrugLiteMap == null) {
					return;
				}
				
				for each (var pivasFormulaMethod:PivasFormulaMethod in pivasFormulaMethodList) {
					if (!pivasFormulaMethod.coreFlag || !pivasFormulaMethodListFilterFunction(pivasFormulaMethod)) {
						continue;
					}
					
					for each (var pivasFormulaMethodDrug:PivasFormulaMethodDrug in pivasFormulaMethod.pivasFormulaMethodDrugList) {
						
						var pivasDrugManufMethod:PivasDrugManufMethod = pivasFormulaMethodDrug.pivasDrugManufMethod;
						var pivasDrugManuf:PivasDrugManuf = pivasDrugManufMethod.pivasDrugManuf;
						var pivasDrug:PivasDrug = pivasDrugManuf.pivasDrug;
						var reconstitutionLabel:String = pivasDrugManuf.requireSolventFlag ? pivasDrugManufMethod.methodDesc : "No Reconstitution Required";
						
						var dmDrugLite:DmDrugLite = dmDrugLiteMap[pivasDrug.itemCode];
						if (dmDrugLite == null || dmDrugLite.hqSuspend == "Y") {
							var itemHqSuspendMsgPopupProp:SystemMessagePopupProp = itemHqSuspendMsgPopupPropFactory.newInstance();
							itemHqSuspendMsgPopupProp.messageParams = [pivasDrug.itemCode, pivasDrugManuf.manufacturerCode, reconstitutionLabel];
							dispatchEvent(new RetrieveSystemMessageEvent(itemHqSuspendMsgPopupProp));
							return;
						} else if (dmDrugLite.suspend == "Y") {
							var itemLocalSuspendMsgPopupProp:SystemMessagePopupProp = itemLocalSuspendMsgPopupPropFactory.newInstance();
							itemLocalSuspendMsgPopupProp.messageParams = [pivasDrug.itemCode, pivasDrugManuf.manufacturerCode, reconstitutionLabel];
							dispatchEvent(new RetrieveSystemMessageEvent(itemLocalSuspendMsgPopupProp));
							return;
						}
					}
				}
			}
			
			public function resetCoreFlag(evt:Event = null):void
			{
				MedProfileUtils.removePopup(evt);
				for each (var pivasFormulaMethod:PivasFormulaMethod in pivasFormula.pivasFormulaMethodList) {
					pivasFormulaMethod.coreFlag = false;
				}
				coreFlagChanged();
			}
			
			private function pivasFormulaMethodListFilterFunction(pivasFormulaMethod:PivasFormulaMethod):Boolean
			{
				return pivasFormulaMethod.status == PivasFormulaMethodStatus.Pending || pivasFormulaMethod.status == PivasFormulaMethodStatus.Active || pivasFormulaMethod.status == PivasFormulaMethodStatus.Suspend;
			}
			
			private function prepMethodListKeyUpHandler(evt:KeyboardEvent):void
			{
				if (evt.keyCode == Keyboard.ENTER) {
					editPrepMethod();
					return;
				}
				
				if (!evt.ctrlKey) {
					return;
				}
				
				switch (evt.keyCode) {
					case Keyboard.INSERT:
						addPrepMethod();
						break;
					case Keyboard.DELETE:
						deletePrepMethod();
						break;
				}
			}
			
			private function editPrepMethod(evt:MouseEvent = null):void
			{
				var pivasFormulaMethod:PivasFormulaMethod = prepMethodList.selectedItem as PivasFormulaMethod;
				if ((evt != null && !MedProfileUtils.containsParent(evt.target as DisplayObject, ItemRenderer)) || pivasFormulaMethod == null) {
					return;
				}
				
				var addPrepMethodPopup:PivasMergeFormulaAddPreparationMethodPopup = PopUpManager.createPopUp(
					parentApplication as DisplayObject, PivasMergeFormulaAddPreparationMethodPopup, true) as PivasMergeFormulaAddPreparationMethodPopup;
				addPrepMethodPopup.setupUI(pivasFormulaMethod, dmDrugLiteMap, setUpdatedDmDrugLiteMap);
			}
			
			private function addPrepMethod(evt:ContextMenuEvent = null):void
			{
				var addPrepMethodPopup:PivasMergeFormulaAddPreparationMethodPopup = PopUpManager.createPopUp(
					parentApplication as DisplayObject, PivasMergeFormulaAddPreparationMethodPopup, true) as PivasMergeFormulaAddPreparationMethodPopup;
				
				var pivasFormulaMethod:PivasFormulaMethod = new PivasFormulaMethod();
				pivasFormulaMethod.pivasFormula = pivasFormula;
				pivasFormulaMethod.status = PivasFormulaMethodStatus.Pending;
				pivasFormulaMethod.initialStatus = PivasFormulaMethodStatus.Pending;
				pivasFormulaMethod.ackStatus = AckStatus.None;
				
				var pivasFormulaMethodDrugList:ArrayCollection = new ArrayCollection();
				
				for each (var pivasFormulaDrug:PivasFormulaDrug in pivasFormulaDrugList) {
					var pivasFormulaMethodDrug:PivasFormulaMethodDrug = new PivasFormulaMethodDrug();
					pivasFormulaMethodDrug.pivasFormulaMethod = pivasFormulaMethod;
					pivasFormulaMethodDrug.pivasFormulaDrug = pivasFormulaDrug;
					pivasFormulaMethodDrugList.addItem(pivasFormulaMethodDrug);
				}
				
				pivasFormulaMethod.pivasFormulaMethodDrugList = pivasFormulaMethodDrugList;
				
				addPrepMethodPopup.setupUI(pivasFormulaMethod, dmDrugLiteMap, proceedAddPrepMethod);
			}
			
			private function proceedAddPrepMethod(pivasFormulaMethod:PivasFormulaMethod, updatedDmDrugLiteMap:Object):void
			{
				for each (var currentItem:PivasFormulaMethod in pivasFormulaMethodList) {
					if (checkDuplicateMethod(currentItem, pivasFormulaMethod)) {
						dispatchEvent(new RetrieveSystemMessageEvent(duplicateMethodMsgPopupProp));
						return;
					}
				}
				pivasFormula.pivasFormulaMethodList.addItem(pivasFormulaMethod);
				setUpdatedDmDrugLiteMap(pivasFormulaMethod, updatedDmDrugLiteMap);
			}
			
			private function setUpdatedDmDrugLiteMap(editedPivasFormulaMethod:PivasFormulaMethod, updatedDmDrugLiteMap:Object):void {
				if (updatedDmDrugLiteMap != null) {
					this.dmDrugLiteMap = updatedDmDrugLiteMap;
					for each (var pivasFormulaMethod:PivasFormulaMethod in pivasFormulaMethodList) {
						pivasFormulaMethod.pivasFormulaMethodDrugList.refresh();
					}
				}
			}
			
			private function checkDuplicateMethod(pivasFormulaMethod1:PivasFormulaMethod, pivasFormulaMethod2:PivasFormulaMethod):Boolean
			{
				if (pivasFormulaMethod1.pivasFormulaMethodDrugList.length != pivasFormulaMethod2.pivasFormulaMethodDrugList.length) {
					return false;
				}
				
				var pivasFormulaMethodDrugList:ArrayCollection = new ArrayCollection();
				pivasFormulaMethodDrugList.addAll(pivasFormulaMethod1.pivasFormulaMethodDrugList);
				for each (var currentItem:PivasFormulaMethodDrug in pivasFormulaMethod2.pivasFormulaMethodDrugList) {
					var index:int = findPivasFormulaMethodDrug(pivasFormulaMethodDrugList, currentItem);
					if (index == -1) {
						return false;
					}
					pivasFormulaMethodDrugList.removeItemAt(index);
				}
				
				return true;
			}
			
			private function findPivasFormulaMethodDrug(pivasFormulaMethodDrugList:ArrayCollection, piavasFormulaMethodDrug:PivasFormulaMethodDrug):int
			{
				var index:int = 0;
				for each (var currentItem:PivasFormulaMethodDrug in pivasFormulaMethodDrugList) {
					if (currentItem.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode == piavasFormulaMethodDrug.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode &&
						currentItem.pivasDrugManufMethod.pivasDrugManuf.manufacturerCode == piavasFormulaMethodDrug.pivasDrugManufMethod.pivasDrugManuf.manufacturerCode) {
						return index; 
					}
					index++;
				}
				return -1;
			}
			
			private function deletePrepMethod():void
			{
				var pivasFormulaMethod:PivasFormulaMethod = prepMethodList.selectedItem;
				
				if (pivasFormulaMethod != null && pivasFormulaMethod.status != PivasFormulaMethodStatus.Active &&
						(permissionMap.getValueAsBoolean(PivasFormulaMaintView.PERMISSION_PIVAS_FORMULA_MAINT_FULL_ACCESS) ||
						pivasFormulaMethod.status == PivasFormulaMethodStatus.Pending)) {
					
					var confirmDeleteMethodMsgPopupProp:SystemMessagePopupProp = confirmDeleteMethodMsgPopupPropFactory.newInstance();
					confirmDeleteMethodMsgPopupProp.yesHandler = function(evt:Event):void {
						pivasFormulaMethod.coreFlag = false;
						pivasFormulaMethod.status = PivasFormulaMethodStatus.Deleted;
						if (isNaN(pivasFormulaMethod.id)) {
							MedProfileUtils.removeItem(pivasFormula.pivasFormulaMethodList, pivasFormulaMethod);
						}
						MedProfileUtils.removePopup(evt);
					};
					dispatchEvent(new RetrieveSystemMessageEvent(confirmDeleteMethodMsgPopupProp));
				}
			}
			
			private function coreFlagChanged():void
			{
				dispatchEvent(new Event("coreFlagChanged"));
			}
			
			[Bindable("coreFlagChanged")]
			public function checkCoreFlag(coreFlag:Boolean):Boolean
			{
				return coreFlag;
			}
			
			public function coreFlagCheckBoxChangeHandler(item:*, selected:Boolean):void
			{
				for each (var pivasFormulaMethod:PivasFormulaMethod in pivasFormula.pivasFormulaMethodList) {
					pivasFormulaMethod.coreFlag = pivasFormulaMethod === item ? selected : false;
				}
				coreFlagChanged();
			}
			
			private function constructContainerList(pivasContainerList:ListCollectionView):ArrayCollection
			{
				var resultList:ArrayCollection = new ArrayCollection([null]);
				resultList.addAll(pivasContainerList);
				return resultList;
			}
			
			private function labelOptionTypeDropDownListChangeHandler():void
			{
				if (pivasFormula.labelOptionType == LabelOptionType.None) {
					numOfLabelDropDownList.selectedIndex = -1;
				}
			}
			
			private function numOfLabelDropDownListOpenHandler():void
			{
				numOfLabelDropDownList.dropDown.height = 200;
				numOfLabelDropDownList.setFocus();
			}
			
			private function containerEqualFunction(c1:PivasContainer, c2:PivasContainer):Boolean
			{
				return c1 == null || c2 == null ? c1 == c2 : c1.id == c2.id;
			}
			
			private function drugLabelFunction(pivasFormulaDrug:PivasFormulaDrug, column:DataGridColumn):String
			{
				return MedProfileUtils.concat(pivasFormulaDrug.displayname, pivasFormulaDrug.saltProperty, pivasFormulaDrug.pivasDosageUnit == "ML" ? pivasFormulaDrug.strength : null);
			}
			
			private function dosageLabelFunction(pivasFormulaDrug:PivasFormulaDrug, column:DataGridColumn):String
			{
				return isNaN(pivasFormulaDrug.dosage) ? "" : String(pivasFormulaDrug.dosage);
			}
			
			private function containerLabelFunc(container:PivasContainer):String 
			{
				return container == null ? "(Blank)" : container.containerName;
			}
			
			private function containerExtLabelFunc(container:PivasContainer):String
			{
				return container == null ? "" : container.containerName;
			}
			
			private function numberLabelFunction(number:Number):String
			{
				return isNaN(number) ? "(Blank)" : number.toString();
			}
			
			private function enumLabelFunction(enum:Enum):String
			{
				return enum.dataValue == "-" ? "(Blank)" : enum.displayValue;
			}
			
			private function enumExtLabelFunction(enum:Enum):String
			{
				return enum.dataValue == "-" ? "" : enum.displayValue;
			}
			
			public function localSuspendTextFunc(itemCode:String):String {
				if (dmDrugLiteMap != null) {
					var dmDrugLite:DmDrugLite = dmDrugLiteMap[itemCode];
					return dmDrugLite != null && dmDrugLite.itemStatus != "A" ? "S" : "";
				}
				return "";
			}
			
			private function hasNoCoreOnAnyActivePrepMethod():Boolean
			{
				var anyActive:Boolean = false;
				var anyCore:Boolean = false;
				for each (var pivasFormulaMethod:PivasFormulaMethod in pivasFormulaMethodList) {
					anyActive = anyActive || pivasFormulaMethod.status == PivasFormulaMethodStatus.Active;
					anyCore = anyCore || pivasFormulaMethod.coreFlag;
				}
				return anyActive && !anyCore;
			}
			
			private function hasInactiveCorePrepMethod():Boolean
			{
				for each (var pivasFormulaMethod:PivasFormulaMethod in pivasFormulaMethodList) {
					if (pivasFormulaMethod.coreFlag && (pivasFormulaMethod.status == PivasFormulaMethodStatus.Pending || pivasFormulaMethod.status == PivasFormulaMethodStatus.Suspend)) {
						return true;
					}
				}
				return false;
			}
			
			private function save():void
			{
				if (containerDropDownList.selectedItem == null) {
					containerDropDownList.setFocus();
					containerDropDownList.drawFocus(true);
					dispatchEvent(new RetrieveSystemMessageEvent(missingContainerMsgPopupProp));
					return;
				}
				
				if (StringUtil.trim(printNameInput.text).length == 0) {
					printNameInput.setFocus();
					dispatchEvent(new RetrieveSystemMessageEvent(missingPrintNameMsgPopupProp));
					return;
				}
				
				if (printNameInput.text.length > 200) {
					printNameInput.setFocus();
					dispatchEvent(new RetrieveSystemMessageEvent(printNameTooLongMsgPopupProp));
					return;
				}
				
				if (pivasFormulaMethodList.length == 0) {
					prepMethodList.setFocus();
					dispatchEvent(new RetrieveSystemMessageEvent(missingPrepMethodMsgPopupProp));
					return;
				}
				
				if (hasNoCoreOnAnyActivePrepMethod()) {
					prepMethodList.setFocus();
					dispatchEvent(new RetrieveSystemMessageEvent(noCorePrepMethodMsgPopupProp));
					return;
				}
				
				if (hasInactiveCorePrepMethod()) {
					prepMethodList.setFocus();
					dispatchEvent(new RetrieveSystemMessageEvent(invalidCorePrepMethodMsgPopupProp));
					return;
				}

				dispatchEvent(new RetrieveSystemMessageEvent(confirmSaveFormulaMsgPopupProp));
			}

			private function confirmSaveFormulaStep2(evt:Event = null):void
			{
				MedProfileUtils.removePopup(evt);
				if (applyToDropDownList.selectedItem == TargetType.None && !batchPrepCheckBox.selected) {
					dispatchEvent(new RetrieveSystemMessageEvent(confirmNoApplyToMsgPopupProp));
				} else {
					proceedSaveFormula();
				}
			}
			
			private function proceedSaveFormula(evt:Event = null):void
			{
				MedProfileUtils.removePopup(evt);
				
				pivasFormula.shelfLife = 0;
				pivasFormula.labelOption = pivasFormulaConfig.labelOption;
				pivasFormula.labelOptionType = pivasFormulaConfig.labelOptionType;
				pivasFormula.expiryDateTitle = doNotStartUsingCheckBox.selected ? ExpiryDateTitle.DoNotStartUsing : ExpiryDateTitle.UseOnOrBefore;
				isLoading = true;
				dispatchEvent(new UpdatePivasFormulaEvent(pivasFormula, successUpdateMergeFormula, failUpdateMergeFormula));
			}
			
			private function successUpdateMergeFormula(...values):void
			{
				isLoading = false;
				PopUpManager.removePopUp(this);
				successUpdateFormulaFunction();
			}
			
			private function failUpdateMergeFormula(...values):void
			{
				isLoading = false;
			}
			
			private function cancel():void
			{
				PopUpManager.removePopUp(this);
				cancelFunction();
			}			
		]]>
	</fx:Script>
	
	<fc:ExtendedNavigatorContent>
		<fc:layout>
			<s:VerticalLayout paddingLeft="10" paddingTop="10" paddingRight="10" paddingBottom="10" gap="10"/>
		</fc:layout>

		<s:BorderContainer width="100%">
			<s:layout>
				<s:VerticalLayout paddingLeft="10" paddingTop="10" paddingRight="10" paddingBottom="10" gap="10"/>
			</s:layout>
		
			<s:HGroup width="100%" gap="10">
				<s:TextInput width="{labelColumnWidth}" enabled="false" text="{pivasFormula.itemCode}"/>
				<s:TextInput width="100%" enabled="false" text="{pivasFormula.itemCode == null ? '' : pivasFormula.printName}"/>
			</s:HGroup>
			
			<s:HGroup height="100%" gap="10">
				<s:Label width="{labelColumnWidth}" paddingTop="5" text="Drug"/>
				<fc:ExtendedDataGrid id="pivasFormulaDrugDataGrid" height="160" dataProvider="{pivasFormulaDrugList}"
									 horizontalScrollPolicy="off" verticalScrollPolicy="on"
									 resizableColumns="false" draggableColumns="false" sortableColumns="false"
									 wordWrap="true" variableRowHeight="true">
					<fc:columns>
						<fc:ExtendedDataGridColumn width="530" headerText="Drug" labelFunction="drugLabelFunction"/>
						<fc:ExtendedDataGridColumn width="100" headerText="Dosage" labelFunction="dosageLabelFunction"/>
						<fc:ExtendedDataGridColumn width="120" headerText="PDU" dataField="pivasDosageUnit"/>
					</fc:columns>
				</fc:ExtendedDataGrid>
			</s:HGroup>
			
			<s:HGroup gap="10" verticalAlign="middle">
				<s:Label width="{labelColumnWidth}" text="Route / Site"/>
				<fc:ExtendedTextInput id="siteInput" width="300" enabled="false" text="{pivasFormula.dmSite.ipmoeDesc}"/>
			</s:HGroup>
			
			<s:HGroup width="100%" gap="10" verticalAlign="middle">
				<s:Label width="{labelColumnWidth}" text="Diluent"/>
				<fc:ExtendedTextInput id="diluentInput" width="200" enabled="false"
									  text="{noDiluentRequiredCheckBox.selected ? 'Not applicable' : pivasFormula.dmDiluent.pivasDiluentDesc}"/>
				<s:CheckBox id="noDiluentRequiredCheckBox" label="No Diluent Required" enabled="false" selected="{pivasFormula.diluentCode == null}"/>
				<s:Spacer width="100%"/>
				<s:Label text="Final Volume"/>
				<fc:NumericInput id="finalVolumeInput" enabled="false" numberFormat="3.2" allowNegative="false" value="{pivasFormula.finalVolume}"/>
				<s:Label width="100" text="ML"/>
			</s:HGroup>
		</s:BorderContainer>

		<s:HGroup width="100%" gap="10" verticalAlign="middle">
			<s:Label width="{labelColumnWidth}" text="Print Name&#10;on Label"/>
			<fc:ExtendedTextArea id="printNameInput" width="100%" heightInLines="2" editable="{pivasFormula.itemCode == null}" maxLines="1" restrict="\u0020-\u007E"
								 text="@{pivasFormula.printName}"/>
		</s:HGroup>
		
		<s:HGroup width="100%" gap="10" verticalAlign="middle">
			<s:Label width="{labelColumnWidth}" text="Container"/>
			<fc:ExtendedDropDownList id="containerDropDownList" width="165" dataProvider="{constructContainerList(pivasContainerList)}" 
									 selectedItem="@{pivasFormula.pivasContainer}" equalFunction="containerEqualFunction"
									 labelFunction="containerLabelFunc" extLabelFunction="containerExtLabelFunc"/>
			<s:Spacer width="100%"/>
			<s:CheckBox id="doNotStartUsingCheckBox" width="200" label="Do not start using" selected="{pivasFormula.expiryDateTitle == ExpiryDateTitle.DoNotStartUsing}"/>
			<s:Label text="Label Option"/>
			<fc:ExtendedDropDownList id="numOfLabelDropDownList" width="45"
									 enabled="{labelOptionTypeDropDownList.selectedItem == LabelOptionType.ExtraProductLabel || labelOptionTypeDropDownList.selectedItem == LabelOptionType.ExtraSetLabel}"
									 selectedItem="@{pivasFormulaConfig.labelOption}"
									 labelFunction="numberLabelFunction"
									 open="numOfLabelDropDownListOpenHandler()">
				<s:ArrayCollection>
					<fx:Number>1</fx:Number>
					<fx:Number>2</fx:Number>
					<fx:Number>3</fx:Number>
					<fx:Number>4</fx:Number>
					<fx:Number>5</fx:Number>
					<fx:Number>6</fx:Number>
					<fx:Number>7</fx:Number>
					<fx:Number>8</fx:Number>
					<fx:Number>9</fx:Number>
				</s:ArrayCollection>				
			</fc:ExtendedDropDownList>
			<fc:ExtendedDropDownList id="labelOptionTypeDropDownList" width="164" dataProvider="{new ArrayCollection(LabelOptionType.constants)}"
									 enabled="false" labelFunction="enumLabelFunction" extLabelFunction="enumExtLabelFunction"
									 selectedItem="@{pivasFormulaConfig.labelOptionType}"
									 change="labelOptionTypeDropDownListChangeHandler()"/>
		</s:HGroup>
		
		<s:HGroup width="100%" gap="10" verticalAlign="middle" horizontalAlign="right">
			<s:Label text="Apply To"/>
			<fc:ExtendedDropDownList id="applyToDropDownList" width="130" dataProvider="{new ArrayCollection(TargetType.constants)}"
									 enabled="false" labelFunction="enumLabelFunction" extLabelFunction="enumExtLabelFunction"
									 selectedItem="@{pivasFormula.targetType}"/>
			<s:CheckBox id="batchPrepCheckBox" label="Batch Preparation" selected="@{pivasFormula.batchPrepFlag}"/>
		</s:HGroup>
		
		<s:Panel height="200" title="Preparation Method" id="preparationMethodPanel" mouseEnabled="true">
			<s:layout>
				<s:VerticalLayout gap="0"/>
			</s:layout>

			<fc:ExtendedDataGrid id="prepMethodDataGrid" width="100%" height="22"
								 borderStyle="none" focusEnabled="false" resizableColumns="false"
								 draggableColumns="false" sortableColumns="false">
				<fc:columns>
					<fc:ExtendedDataGridColumn width="{CORE_COLUMN_WIDTH}" headerText="Core" dataField="coreFlag"/>
					<fc:ExtendedDataGridColumn width="{LOCAL_SUSPEND_COLUMN_WIDTH}" headerText="S"/>
					<fc:ExtendedDataGridColumn width="{ITEM_CODE_COLUMN_WIDTH}" headerText="Item Code" />
					<fc:ExtendedDataGridColumn width="{MANUF_COLUMN_WIDTH}" headerText="Manuf."/>
					<fc:ExtendedDataGridColumn width="{ITEM_DESC_COLUMN_WIDTH}" headerText="Item Description"/>
					<fc:ExtendedDataGridColumn width="{REQUIRED_VOLUME_COLUMN_WIDTH}" headerText="Required Vol."/>
					<fc:ExtendedDataGridColumn width="{DILUENT_COLUMN_WIDTH}" headerText="Diluent"/>
					<fc:ExtendedDataGridColumn width="{SHELF_LIFE_COLUMN_WIDTH}" headerText="Shelf Life"/>
					<fc:ExtendedDataGridColumn width="{STATUS_COLUMN_WIDTH}" headerText="Status"/>
				</fc:columns>
			</fc:ExtendedDataGrid>
			<s:List id="prepMethodList" width="100%" height="100%" fontWeight="bold"
					useVirtualLayout="false" hasFocusableChildren="true"
					verticalScrollPolicy="on" horizontalScrollPolicy="off"
					doubleClickEnabled="true" doubleClick="editPrepMethod(event)"
					dataProvider="{pivasFormulaMethodList}"
					keyUp="prepMethodListKeyUpHandler(event)">
				<s:itemRenderer>
					<fx:Component>
						<s:ItemRenderer>
							<fx:Script>
								<![CDATA[
									import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
								]]>
							</fx:Script>
							
							<s:layout>
								<s:VerticalLayout gap="0"/>
							</s:layout>
							
							<s:HGroup width="100%" gap="0">
								<s:VGroup width="{outerDocument.CORE_COLUMN_WIDTH - 1}" height="100%" verticalAlign="middle" horizontalAlign="center">
									<s:CheckBox id="coreFlagCheckBox" selected="{outerDocument.checkCoreFlag(data.coreFlag)}"
												change="outerDocument.coreFlagCheckBoxChangeHandler(data, coreFlagCheckBox.selected)"/>
								</s:VGroup>
								<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
								<s:DataGroup width="{outerDocument.LOCAL_SUSPEND_COLUMN_WIDTH + outerDocument.ITEM_CODE_COLUMN_WIDTH + outerDocument.MANUF_COLUMN_WIDTH + outerDocument.ITEM_DESC_COLUMN_WIDTH + outerDocument.REQUIRED_VOLUME_COLUMN_WIDTH - 1}"
											 dataProvider="{data.pivasFormulaMethodDrugList}">
									<s:layout>
										<s:VerticalLayout gap="0"/>
									</s:layout>
									<s:itemRenderer>
										<fx:Component>
											<s:ItemRenderer>
												<fx:Script>
													<![CDATA[
														import hk.org.ha.view.pms.main.medprofile.MedProfileUtils;
													]]>
												</fx:Script>
												
												<s:layout>
													<s:VerticalLayout gap="0"/>
												</s:layout>
												<mx:HRule styleName="{itemIndex != 0 ? 'vettingMpRuleStyle' : 'vettingMpInvisibleRuleStyle'}" width="100%"/>
												<s:HGroup gap="0">
													<s:Label width="{outerDocument.outerDocument.LOCAL_SUSPEND_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
															 text="{outerDocument.outerDocument.localSuspendTextFunc(data.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode)}"/>
													<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
													<s:Label width="{outerDocument.outerDocument.ITEM_CODE_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
															 text="{data.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.itemCode}"/>
													<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
													<s:Label width="{outerDocument.outerDocument.MANUF_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
															 text="{data.pivasDrugManufMethod.pivasDrugManuf.manufacturerCode}"/>
													<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
													<s:Label width="{outerDocument.outerDocument.ITEM_DESC_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
															 text="{data.pivasDrugManufMethod.pivasDrugManuf.pivasDrug.dmDrug.fullDrugDesc}"/>
													<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
													<s:Label width="{outerDocument.outerDocument.REQUIRED_VOLUME_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
															 text="{MedProfileUtils.formatBrackets(data.afterVolume, '', 'ML')}"/>
												</s:HGroup>
											</s:ItemRenderer>
										</fx:Component>
									</s:itemRenderer>
								</s:DataGroup>
								<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
								<s:Label width="{outerDocument.DILUENT_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
										 text="{MedProfileUtils.formatBrackets(data.diluentVolume, '', 'ML')}"/>
								<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
								<s:Label width="{outerDocument.SHELF_LIFE_COLUMN_WIDTH - 1}" paddingLeft="5" paddingTop="5" paddingBottom="5"
										 text="{MedProfileUtils.formatBrackets(data.shelfLife, '', ' hr')}"/>
								<mp:VRule styleName="vettingMpRuleStyle" height="100%"/>
								<s:Label paddingLeft="5" paddingTop="5" paddingBottom="5"
										  text="{data.status}"/>
							</s:HGroup>
							<mx:HRule styleName="vettingMpRuleStyle" width="100%"/>
						</s:ItemRenderer>
					</fx:Component>
				</s:itemRenderer>
			</s:List>
			<s:controlBarContent>
				<s:Label text="&lt;Ctrl+Insert&gt; to add record; &lt;Ctrl+Delete&gt; to delete record" styleName="footNoteLabelStyle"/>
			</s:controlBarContent>
			<s:controlBarLayout>
				<s:HorizontalLayout horizontalAlign="left" verticalAlign="middle" paddingTop="5" paddingLeft="5"/>
			</s:controlBarLayout>
		</s:Panel>
		
		<s:HGroup width="100%" horizontalAlign="right">
			<fc:ExtendedButton id="okButton" label="Save" enabled="{!isLoading &amp;&amp; !duplicateFormulaExists}" click="save()"/>				
			<fc:ExtendedButton id="cancelButton" label="Cancel" enabled="{!isLoading}" click="cancel()"/>
		</s:HGroup>
	</fc:ExtendedNavigatorContent>
	
</s:TitleWindow>
