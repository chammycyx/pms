package hk.org.ha.model.pms.vo.label;

import hk.org.ha.model.pms.dms.udt.pivas.ExpiryDateTitle;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PivasMergeFormulaWorksheet")
@ExternalizedBean(type=DefaultExternalizer.class)
public class PivasMergeFormulaWorksheet implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = false)
	private List<PivasMergeFormulaWorksheetSolventInstructionDtl> pivasMergeFormulaWorksheetSolventInstructionDtlList;
	
	@XmlElement(required = true)
	private List<PivasMergeFormulaWorksheetDrugInstructionDtl> pivasMergeFormulaWorksheetDrugInstructionDtlList;
	
	@XmlElement(required = true)
	private List<PivasMergeFormulaWorksheetRawMaterialDtl> pivasMergeFormulaWorksheetRawMaterialDtlList;
	
	@XmlElement(required = true)
	private String lotNum;
	
	@XmlElement(required = true)
	private String printName;
	
	@XmlElement(required = true)
	private String siteDesc;
	
	@XmlElement(required = false)
	private String diluentDesc;
	
	@XmlElement(required = false)
	private String diluentVolume;
	
	@XmlElement(required = true)
	private String containerName;
	
	@XmlElement(required = true)
	private String prepQty;
	
	@XmlElement(required = true)
	private ExpiryDateTitle expiryDateTitle;
	
	@XmlElement(required = true)
	private Date prepExpiryDate;
	
	@XmlElement(required = true)
	private Date manufactureDate;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String targetProductItemCode;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private Integer targetProductItemDrugKey;
	
	// save for PIVAS report
	@XmlElement(required = false)
	private String diluentItemCode;
	
	// save for PIVAS report
	@XmlElement(required = true)
	private String formulaPrintName;
	
	// save for PIVAS report
	@XmlElement(required = true)
	private String finalVolume;
	
	public PivasMergeFormulaWorksheet() {
		expiryDateTitle = ExpiryDateTitle.UseOnOrBefore;
	}

	public List<PivasMergeFormulaWorksheetSolventInstructionDtl> getPivasMergeFormulaWorksheetSolventInstructionDtlList() {
		return pivasMergeFormulaWorksheetSolventInstructionDtlList;
	}

	public void setPivasMergeFormulaWorksheetSolventInstructionDtlList(
			List<PivasMergeFormulaWorksheetSolventInstructionDtl> pivasMergeFormulaWorksheetSolventInstructionDtlList) {
		this.pivasMergeFormulaWorksheetSolventInstructionDtlList = pivasMergeFormulaWorksheetSolventInstructionDtlList;
	}

	public List<PivasMergeFormulaWorksheetDrugInstructionDtl> getPivasMergeFormulaWorksheetDrugInstructionDtlList() {
		return pivasMergeFormulaWorksheetDrugInstructionDtlList;
	}

	public void setPivasMergeFormulaWorksheetDrugInstructionDtlList(
			List<PivasMergeFormulaWorksheetDrugInstructionDtl> pivasMergeFormulaWorksheetDrugInstructionDtlList) {
		this.pivasMergeFormulaWorksheetDrugInstructionDtlList = pivasMergeFormulaWorksheetDrugInstructionDtlList;
	}

	public List<PivasMergeFormulaWorksheetRawMaterialDtl> getPivasMergeFormulaWorksheetRawMaterialDtlList() {
		return pivasMergeFormulaWorksheetRawMaterialDtlList;
	}

	public void setPivasMergeFormulaWorksheetRawMaterialDtlList(
			List<PivasMergeFormulaWorksheetRawMaterialDtl> pivasMergeFormulaWorksheetRawMaterialDtlList) {
		this.pivasMergeFormulaWorksheetRawMaterialDtlList = pivasMergeFormulaWorksheetRawMaterialDtlList;
	}

	public String getLotNum() {
		return lotNum;
	}

	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public String getSiteDesc() {
		return siteDesc;
	}

	public void setSiteDesc(String siteDesc) {
		this.siteDesc = siteDesc;
	}

	public String getDiluentDesc() {
		return diluentDesc;
	}

	public void setDiluentDesc(String diluentDesc) {
		this.diluentDesc = diluentDesc;
	}

	public String getDiluentVolume() {
		return diluentVolume;
	}

	public void setDiluentVolume(String diluentVolume) {
		this.diluentVolume = diluentVolume;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getPrepQty() {
		return prepQty;
	}

	public void setPrepQty(String prepQty) {
		this.prepQty = prepQty;
	}

	public ExpiryDateTitle getExpiryDateTitle() {
		return expiryDateTitle;
	}

	public void setExpiryDateTitle(ExpiryDateTitle expiryDateTitle) {
		this.expiryDateTitle = expiryDateTitle;
	}

	public Date getPrepExpiryDate() {
		return prepExpiryDate;
	}

	public void setPrepExpiryDate(Date prepExpiryDate) {
		this.prepExpiryDate = prepExpiryDate;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public String getTargetProductItemCode() {
		return targetProductItemCode;
	}

	public void setTargetProductItemCode(String targetProductItemCode) {
		this.targetProductItemCode = targetProductItemCode;
	}

	public Integer getTargetProductItemDrugKey() {
		return targetProductItemDrugKey;
	}

	public void setTargetProductItemDrugKey(Integer targetProductItemDrugKey) {
		this.targetProductItemDrugKey = targetProductItemDrugKey;
	}

	public String getDiluentItemCode() {
		return diluentItemCode;
	}

	public void setDiluentItemCode(String diluentItemCode) {
		this.diluentItemCode = diluentItemCode;
	}

	public String getFormulaPrintName() {
		return formulaPrintName;
	}

	public void setFormulaPrintName(String formulaPrintName) {
		this.formulaPrintName = formulaPrintName;
	}

	public String getFinalVolume() {
		return finalVolume;
	}

	public void setFinalVolume(String finalVolume) {
		this.finalVolume = finalVolume;
	}
}
