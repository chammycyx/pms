package hk.org.ha.model.pms.biz.alert.mds;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.vo.PreparationProperty;
import hk.org.ha.model.pms.persistence.AlertEntity;
import hk.org.ha.model.pms.persistence.disp.MedOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedOrderItemAlert;
import hk.org.ha.model.pms.vo.alert.mds.AlertMsg;
import hk.org.ha.model.pms.vo.vetting.OrderViewInfo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("preparationCheckService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PreparationCheckServiceBean implements PreparationCheckServiceLocal {

	@In
	private AlertRetrieveManagerLocal alertRetrieveManager;

	@In
	private AlertMsgManagerLocal alertMsgManager;
	
	@In
	private OrderViewInfo orderViewInfo;
	
	@Out
	private List<MedOrderItemAlert> preparationAlertList;
	
	public AlertMsg checkPreparation(MedOrderItem medOrderItem, PreparationProperty preparationProperty, Boolean pregnancyCheckFlag) {

		orderViewInfo.setPregnancyCheckFlag(pregnancyCheckFlag);
		
		if (preparationProperty == null) {
			medOrderItem.setItemCode(null);
			medOrderItem.setStrength(null);
			medOrderItem.setVolume(null);
			medOrderItem.setVolumeUnit(null);
			medOrderItem.setExtraInfo(null);
		}
		else {
			medOrderItem.setStrength(preparationProperty.getStrength());
			medOrderItem.setExtraInfo(preparationProperty.getExtraInfo());
			medOrderItem.setItemCode(preparationProperty.getItemCode());
			medOrderItem.setBaseUnit(preparationProperty.getBaseUnit());
		}

		AlertMsg alertMsg = null;
		preparationAlertList = new ArrayList<MedOrderItemAlert>(medOrderItem.getMedOrderItemAlertList());
		
		if (alertRetrieveManager.isMdsEnable()) {

			preparationAlertList.removeAll(medOrderItem.getPatAlertList());
			List<MedOrderItemAlert> patAlertList = retrievePatAlert(medOrderItem);
			preparationAlertList.addAll(patAlertList);
			
			if (!patAlertList.isEmpty()) {
				medOrderItem.setMedOrderItemAlertList(preparationAlertList);
				alertMsg = alertMsgManager.retrieveAlertMsg(medOrderItem);
				alertMsg.setDdiAlertList(new ArrayList<AlertEntity>());
			}
		}

		return alertMsg;
	}

	private List<MedOrderItemAlert> retrievePatAlert(MedOrderItem moi) {
		List<MedOrderItemAlert> list = new ArrayList<MedOrderItemAlert>();
		for (AlertEntity alertEntity : alertRetrieveManager.retrieveAlertByMedOrderItem(moi, false)) {
			MedOrderItemAlert moia = new MedOrderItemAlert(alertEntity);
			moia.setMedOrderItem(moi);
			list.add(moia);
		}
		return list;
	}
	
	@Remove
	public void destroy() {
		if (preparationAlertList != null) {
			preparationAlertList = null;
		}
	}
}
