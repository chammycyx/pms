/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (DeliveryScheduleItem.as).
 */

package hk.org.ha.model.pms.persistence.medprofile {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import hk.org.ha.model.pms.udt.RecordStatus;
    import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemDueOnType;
    import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemOrderType;
    import hk.org.ha.model.pms.udt.medprofile.DeliveryScheduleItemWardType;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class DeliveryScheduleItemBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _deliveryRequestCreateDateStr:String;
        private var _deliveryRequestCreateUser:String;
        private var _deliveryRequestGroupList:ListCollectionView;
        private var _deliveryRequestWorkstation:String;
        private var _deliverySchedule:DeliverySchedule;
        private var _dueHour:Number;
        private var _dueHourStr:String;
        private var _dueOnType:DeliveryScheduleItemDueOnType;
        private var _generateDay:Number;
        private var _highlightFlag:Boolean;
        private var _id:Number;
        private var _multiDeliveryFlag:Boolean;
        private var _orderType:DeliveryScheduleItemOrderType;
        private var _overdueFlag:Boolean;
        private var _scheduleTime:Number;
        private var _scheduleTimeStr:String;
        private var _statFlag:Boolean;
        private var _status:RecordStatus;
        private var _urgentFlag:Boolean;
        private var _wardCodeCsv:String;
        private var _wardGroupCsv:String;
        private var _wardType:DeliveryScheduleItemWardType;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is DeliveryScheduleItem) || (property as DeliveryScheduleItem).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set deliveryRequestCreateDateStr(value:String):void {
            _deliveryRequestCreateDateStr = value;
        }
        public function get deliveryRequestCreateDateStr():String {
            return _deliveryRequestCreateDateStr;
        }

        public function set deliveryRequestCreateUser(value:String):void {
            _deliveryRequestCreateUser = value;
        }
        public function get deliveryRequestCreateUser():String {
            return _deliveryRequestCreateUser;
        }

        public function set deliveryRequestGroupList(value:ListCollectionView):void {
            _deliveryRequestGroupList = value;
        }
        public function get deliveryRequestGroupList():ListCollectionView {
            return _deliveryRequestGroupList;
        }

        public function set deliveryRequestWorkstation(value:String):void {
            _deliveryRequestWorkstation = value;
        }
        public function get deliveryRequestWorkstation():String {
            return _deliveryRequestWorkstation;
        }

        public function set deliverySchedule(value:DeliverySchedule):void {
            _deliverySchedule = value;
        }
        public function get deliverySchedule():DeliverySchedule {
            return _deliverySchedule;
        }

        public function set dueHour(value:Number):void {
            _dueHour = value;
        }
        public function get dueHour():Number {
            return _dueHour;
        }

        public function set dueHourStr(value:String):void {
            _dueHourStr = value;
        }
        public function get dueHourStr():String {
            return _dueHourStr;
        }

        public function set dueOnType(value:DeliveryScheduleItemDueOnType):void {
            _dueOnType = value;
        }
        public function get dueOnType():DeliveryScheduleItemDueOnType {
            return _dueOnType;
        }

        public function set generateDay(value:Number):void {
            _generateDay = value;
        }
        public function get generateDay():Number {
            return _generateDay;
        }

        public function set highlightFlag(value:Boolean):void {
            _highlightFlag = value;
        }
        public function get highlightFlag():Boolean {
            return _highlightFlag;
        }

        public function set id(value:Number):void {
            _id = value;
        }
        [Id]
        public function get id():Number {
            return _id;
        }

        public function set multiDeliveryFlag(value:Boolean):void {
            _multiDeliveryFlag = value;
        }
        public function get multiDeliveryFlag():Boolean {
            return _multiDeliveryFlag;
        }

        public function set orderType(value:DeliveryScheduleItemOrderType):void {
            _orderType = value;
        }
        public function get orderType():DeliveryScheduleItemOrderType {
            return _orderType;
        }

        public function set overdueFlag(value:Boolean):void {
            _overdueFlag = value;
        }
        public function get overdueFlag():Boolean {
            return _overdueFlag;
        }

        public function set scheduleTime(value:Number):void {
            _scheduleTime = value;
        }
        public function get scheduleTime():Number {
            return _scheduleTime;
        }

        public function set scheduleTimeStr(value:String):void {
            _scheduleTimeStr = value;
        }
        public function get scheduleTimeStr():String {
            return _scheduleTimeStr;
        }

        public function set statFlag(value:Boolean):void {
            _statFlag = value;
        }
        public function get statFlag():Boolean {
            return _statFlag;
        }

        public function set status(value:RecordStatus):void {
            _status = value;
        }
        public function get status():RecordStatus {
            return _status;
        }

        public function set urgentFlag(value:Boolean):void {
            _urgentFlag = value;
        }
        public function get urgentFlag():Boolean {
            return _urgentFlag;
        }

        public function set wardCodeCsv(value:String):void {
            _wardCodeCsv = value;
        }
        public function get wardCodeCsv():String {
            return _wardCodeCsv;
        }

        public function set wardGroupCsv(value:String):void {
            _wardGroupCsv = value;
        }
        public function get wardGroupCsv():String {
            return _wardGroupCsv;
        }

        public function set wardType(value:DeliveryScheduleItemWardType):void {
            _wardType = value;
        }
        public function get wardType():DeliveryScheduleItemWardType {
            return _wardType;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_id))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_id) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:DeliveryScheduleItemBase = DeliveryScheduleItemBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._deliveryRequestCreateDateStr, _deliveryRequestCreateDateStr, null, this, 'deliveryRequestCreateDateStr', function setter(o:*):void{_deliveryRequestCreateDateStr = o as String}, false);
               em.meta_mergeExternal(src._deliveryRequestCreateUser, _deliveryRequestCreateUser, null, this, 'deliveryRequestCreateUser', function setter(o:*):void{_deliveryRequestCreateUser = o as String}, false);
               em.meta_mergeExternal(src._deliveryRequestGroupList, _deliveryRequestGroupList, null, this, 'deliveryRequestGroupList', function setter(o:*):void{_deliveryRequestGroupList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._deliveryRequestWorkstation, _deliveryRequestWorkstation, null, this, 'deliveryRequestWorkstation', function setter(o:*):void{_deliveryRequestWorkstation = o as String}, false);
               em.meta_mergeExternal(src._deliverySchedule, _deliverySchedule, null, this, 'deliverySchedule', function setter(o:*):void{_deliverySchedule = o as DeliverySchedule}, false);
               em.meta_mergeExternal(src._dueHour, _dueHour, null, this, 'dueHour', function setter(o:*):void{_dueHour = o as Number}, false);
               em.meta_mergeExternal(src._dueHourStr, _dueHourStr, null, this, 'dueHourStr', function setter(o:*):void{_dueHourStr = o as String}, false);
               em.meta_mergeExternal(src._dueOnType, _dueOnType, null, this, 'dueOnType', function setter(o:*):void{_dueOnType = o as DeliveryScheduleItemDueOnType}, false);
               em.meta_mergeExternal(src._generateDay, _generateDay, null, this, 'generateDay', function setter(o:*):void{_generateDay = o as Number}, false);
               em.meta_mergeExternal(src._highlightFlag, _highlightFlag, null, this, 'highlightFlag', function setter(o:*):void{_highlightFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number}, false);
               em.meta_mergeExternal(src._multiDeliveryFlag, _multiDeliveryFlag, null, this, 'multiDeliveryFlag', function setter(o:*):void{_multiDeliveryFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._orderType, _orderType, null, this, 'orderType', function setter(o:*):void{_orderType = o as DeliveryScheduleItemOrderType}, false);
               em.meta_mergeExternal(src._overdueFlag, _overdueFlag, null, this, 'overdueFlag', function setter(o:*):void{_overdueFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._scheduleTime, _scheduleTime, null, this, 'scheduleTime', function setter(o:*):void{_scheduleTime = o as Number}, false);
               em.meta_mergeExternal(src._scheduleTimeStr, _scheduleTimeStr, null, this, 'scheduleTimeStr', function setter(o:*):void{_scheduleTimeStr = o as String}, false);
               em.meta_mergeExternal(src._statFlag, _statFlag, null, this, 'statFlag', function setter(o:*):void{_statFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._status, _status, null, this, 'status', function setter(o:*):void{_status = o as RecordStatus}, false);
               em.meta_mergeExternal(src._urgentFlag, _urgentFlag, null, this, 'urgentFlag', function setter(o:*):void{_urgentFlag = o as Boolean}, false);
               em.meta_mergeExternal(src._wardCodeCsv, _wardCodeCsv, null, this, 'wardCodeCsv', function setter(o:*):void{_wardCodeCsv = o as String}, false);
               em.meta_mergeExternal(src._wardGroupCsv, _wardGroupCsv, null, this, 'wardGroupCsv', function setter(o:*):void{_wardGroupCsv = o as String}, false);
               em.meta_mergeExternal(src._wardType, _wardType, null, this, 'wardType', function setter(o:*):void{_wardType = o as DeliveryScheduleItemWardType}, false);
            }
            else {
               em.meta_mergeExternal(src._id, _id, null, this, 'id', function setter(o:*):void{_id = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _deliveryRequestCreateDateStr = input.readObject() as String;
                _deliveryRequestCreateUser = input.readObject() as String;
                _deliveryRequestGroupList = input.readObject() as ListCollectionView;
                _deliveryRequestWorkstation = input.readObject() as String;
                _deliverySchedule = input.readObject() as DeliverySchedule;
                _dueHour = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _dueHourStr = input.readObject() as String;
                _dueOnType = Enum.readEnum(input) as DeliveryScheduleItemDueOnType;
                _generateDay = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _highlightFlag = input.readObject() as Boolean;
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _multiDeliveryFlag = input.readObject() as Boolean;
                _orderType = Enum.readEnum(input) as DeliveryScheduleItemOrderType;
                _overdueFlag = input.readObject() as Boolean;
                _scheduleTime = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _scheduleTimeStr = input.readObject() as String;
                _statFlag = input.readObject() as Boolean;
                _status = Enum.readEnum(input) as RecordStatus;
                _urgentFlag = input.readObject() as Boolean;
                _wardCodeCsv = input.readObject() as String;
                _wardGroupCsv = input.readObject() as String;
                _wardType = Enum.readEnum(input) as DeliveryScheduleItemWardType;
            }
            else {
                _id = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_deliveryRequestCreateDateStr is IPropertyHolder) ? IPropertyHolder(_deliveryRequestCreateDateStr).object : _deliveryRequestCreateDateStr);
                output.writeObject((_deliveryRequestCreateUser is IPropertyHolder) ? IPropertyHolder(_deliveryRequestCreateUser).object : _deliveryRequestCreateUser);
                output.writeObject((_deliveryRequestGroupList is IPropertyHolder) ? IPropertyHolder(_deliveryRequestGroupList).object : _deliveryRequestGroupList);
                output.writeObject((_deliveryRequestWorkstation is IPropertyHolder) ? IPropertyHolder(_deliveryRequestWorkstation).object : _deliveryRequestWorkstation);
                output.writeObject((_deliverySchedule is IPropertyHolder) ? IPropertyHolder(_deliverySchedule).object : _deliverySchedule);
                output.writeObject((_dueHour is IPropertyHolder) ? IPropertyHolder(_dueHour).object : _dueHour);
                output.writeObject((_dueHourStr is IPropertyHolder) ? IPropertyHolder(_dueHourStr).object : _dueHourStr);
                output.writeObject((_dueOnType is IPropertyHolder) ? IPropertyHolder(_dueOnType).object : _dueOnType);
                output.writeObject((_generateDay is IPropertyHolder) ? IPropertyHolder(_generateDay).object : _generateDay);
                output.writeObject((_highlightFlag is IPropertyHolder) ? IPropertyHolder(_highlightFlag).object : _highlightFlag);
                output.writeObject((_id is IPropertyHolder) ? IPropertyHolder(_id).object : _id);
                output.writeObject((_multiDeliveryFlag is IPropertyHolder) ? IPropertyHolder(_multiDeliveryFlag).object : _multiDeliveryFlag);
                output.writeObject((_orderType is IPropertyHolder) ? IPropertyHolder(_orderType).object : _orderType);
                output.writeObject((_overdueFlag is IPropertyHolder) ? IPropertyHolder(_overdueFlag).object : _overdueFlag);
                output.writeObject((_scheduleTime is IPropertyHolder) ? IPropertyHolder(_scheduleTime).object : _scheduleTime);
                output.writeObject((_scheduleTimeStr is IPropertyHolder) ? IPropertyHolder(_scheduleTimeStr).object : _scheduleTimeStr);
                output.writeObject((_statFlag is IPropertyHolder) ? IPropertyHolder(_statFlag).object : _statFlag);
                output.writeObject((_status is IPropertyHolder) ? IPropertyHolder(_status).object : _status);
                output.writeObject((_urgentFlag is IPropertyHolder) ? IPropertyHolder(_urgentFlag).object : _urgentFlag);
                output.writeObject((_wardCodeCsv is IPropertyHolder) ? IPropertyHolder(_wardCodeCsv).object : _wardCodeCsv);
                output.writeObject((_wardGroupCsv is IPropertyHolder) ? IPropertyHolder(_wardGroupCsv).object : _wardGroupCsv);
                output.writeObject((_wardType is IPropertyHolder) ? IPropertyHolder(_wardType).object : _wardType);
            }
            else {
                output.writeObject(_id);
            }
        }
    }
}
