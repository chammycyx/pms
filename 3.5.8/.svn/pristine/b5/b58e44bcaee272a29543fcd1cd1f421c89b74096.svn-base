package hk.org.ha.model.pms.biz.reftable.pivas;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.corp.cache.PasWardCacherInf;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasFormulaExclWard;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasWard;
import hk.org.ha.model.pms.dms.persistence.pivas.PivasWardPK;
import hk.org.ha.model.pms.persistence.corp.HospitalMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.vo.patient.PasWard;
import hk.org.ha.model.pms.vo.pivas.PivasWardInfo;
import hk.org.ha.model.pms.vo.pivas.PivasWardItem;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsPmsServiceJmsRemote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pivasWardMaintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PivasWardMaintServiceBean implements PivasWardMaintServiceLocal {
	@In
	private Workstore workstore;
	
	@In
	private DmsPmsServiceJmsRemote dmsPmsServiceProxy;
	
	@In
	private PasWardCacherInf pasWardCacher;
	
	private static PivasWardItemComparator pivasWardItemComparator = new PivasWardItemComparator();
	
	private static PivasFormulaAvailabilityComparator pivasFormulaAvailabilityComparator = new PivasFormulaAvailabilityComparator();
	
	public PivasWardInfo retrievePivasWardInfo() {
		List<String> patHospCodeList = retrievePatHospCodeList();
		List<PivasWardItem> pivasWardItemList = new ArrayList<PivasWardItem>();
		PivasWardInfo pivasWardInfo = new PivasWardInfo();
		
		if(!patHospCodeList.isEmpty()){
			List<PivasWard> pivasWardList = dmsPmsServiceProxy.retrievePivasWardList(patHospCodeList.get(0), workstore.getWorkstoreGroup().getWorkstoreGroupCode());
			
			List<PasWard> pasWardList = pasWardCacher.getPasWardList(patHospCodeList.get(0));
			
			Map<String, PivasWard> pivasWardMap = new HashMap<String, PivasWard>();
			for(PivasWard pivasWard:pivasWardList){
				pivasWardMap.put(pivasWard.getCompId().getWard(), pivasWard);
			}
			
			for(PasWard pasWard:pasWardList){
				PivasWardItem pivasWardItem = new PivasWardItem();
				pivasWardItem.setPivasWardEnableFlag(false);
				pivasWardItem.setPivasWardCode(pasWard.getPasWardCode());
				pivasWardItem.setPivasWardDesc(pasWard.getPasWardDesc());

				if (pivasWardMap.containsKey(pasWard.getPasWardCode())) {
					pivasWardItem.setPivasWardEnableFlag(true);
					pivasWardItem.setPivasWard(pivasWardMap.get(pasWard.getPasWardCode()));
				}
				
				pivasWardItemList.add(pivasWardItem);
			}
			
			pivasWardInfo.setPatHospCodeList(patHospCodeList);
			
			Collections.sort(pivasWardItemList, pivasWardItemComparator);
			
			pivasWardInfo.setPivasWardItemList(pivasWardItemList);
		}
		
		return pivasWardInfo;
	}
	
	public List<PivasWardItem> retrievePivasWardList(String patHospCode) {
		List<PivasWardItem> pivasWardItemList = new ArrayList<PivasWardItem>();
		List<PivasWard> pivasWardList = dmsPmsServiceProxy.retrievePivasWardList(patHospCode, workstore.getWorkstoreGroup().getWorkstoreGroupCode());
		List<PasWard> pasWardList = pasWardCacher.getPasWardList(patHospCode);
		
		Map<String, PivasWard> pivasWardMap = new HashMap<String, PivasWard>();
		for(PivasWard pivasWard:pivasWardList){
			pivasWardMap.put(pivasWard.getCompId().getWard(), pivasWard);
		}
		
		for(PasWard pasWard:pasWardList){
			PivasWardItem pivasWardItem = new PivasWardItem();
			pivasWardItem.setPivasWardEnableFlag(false);
			pivasWardItem.setPivasWardCode(pasWard.getPasWardCode());
			pivasWardItem.setPivasWardDesc(pasWard.getPasWardDesc());

			if (pivasWardMap.containsKey(pasWard.getPasWardCode())) {
				pivasWardItem.setPivasWardEnableFlag(true);
				pivasWardItem.setPivasWard(pivasWardMap.get(pasWard.getPasWardCode()));
			}
			
			pivasWardItemList.add(pivasWardItem);
		}
		
		Collections.sort(pivasWardItemList, pivasWardItemComparator);
		
		return pivasWardItemList;
	}
	
	public List<PivasWardItem> retrievePivasFormulaAvailabilityInfo(Long pivasFormulaId) {

		List<PivasWard> pivasWardList = new ArrayList<PivasWard>();
		for (String patHospCode: retrievePatHospCodeList()) {
			pivasWardList.addAll(dmsPmsServiceProxy.retrievePivasWardList(patHospCode, workstore.getWorkstoreGroup().getWorkstoreGroupCode()));
		}
		
		List<PivasFormulaExclWard> pivasFormulaExclWardList = dmsPmsServiceProxy.retrievePivasFormulaExclWardListByPivasFormulaId(Arrays.asList(pivasFormulaId));
		
		List<PivasWardItem> resultList = new ArrayList<PivasWardItem>();
		for (PivasWard pivasWard: pivasWardList) {
			PivasWardItem pivasWardItem = new PivasWardItem();
			pivasWardItem.setPivasWardCode(pivasWard.getCompId().getWard());
			pivasWardItem.setPivasWard(pivasWard);
			pivasWardItem.setPivasWardEnableFlag(true);
			for (PivasFormulaExclWard pivasFormulaExclWard: pivasFormulaExclWardList) {
				if (pivasWard.getCompId().getPatHospCode().equals(pivasFormulaExclWard.getPatHospCode()) && pivasWard.getCompId().getWard().equals(pivasFormulaExclWard.getWard())) {
					pivasWardItem.setPivasWardEnableFlag(false);
					break;
				}
			}
			resultList.add(pivasWardItem);
		}
		Collections.sort(resultList, pivasFormulaAvailabilityComparator);
		
		return resultList;
	}
	
	public void updatePivasWard(List<PivasWardItem> pivasWardItemList, String patHospCode){
		List<PivasWard> updatePivasWardList = new ArrayList<PivasWard>();
		List<PivasWard> deletePivasWardList = new ArrayList<PivasWard>();
			
		for(PivasWardItem pivasWardItem:pivasWardItemList){
			PivasWard pivasWard = new PivasWard();
			if(pivasWardItem.isPivasWardEnableFlag()){
				pivasWard.setCompId(new PivasWardPK(workstore.getWorkstoreGroup().getWorkstoreGroupCode(),patHospCode, pivasWardItem.getPivasWardCode()));
				updatePivasWardList.add(pivasWard);
			}else if(pivasWardItem.getPivasWard() != null){
				pivasWard.setCompId(new PivasWardPK(workstore.getWorkstoreGroup().getWorkstoreGroupCode(),patHospCode, pivasWardItem.getPivasWardCode()));
				deletePivasWardList.add(pivasWard);
			}
		}
		
		dmsPmsServiceProxy.updatePivasWardList(updatePivasWardList, deletePivasWardList);
	}
	
	public void updatePivasFormulaExclWardList(Long pivasFormulaId, List<PivasFormulaExclWard> pivasFormulaExclWardList) {
		dmsPmsServiceProxy.updatePivasFormulaExclWardList(pivasFormulaId, pivasFormulaExclWardList);
	}
	
	private List<String> retrievePatHospCodeList() {
		
		List<String> patHospCodeList = new ArrayList<String>();
		
		for (HospitalMapping hospitalMapping: workstore.getHospital().getHospitalMappingList()) {
			if (hospitalMapping.getPrescribeFlag()) {
				patHospCodeList.add(hospitalMapping.getPatHospCode());
			}
		}
		
		return patHospCodeList;
	}
	
	@Remove
	public void destroy() {
	}
	
	private static class PivasWardItemComparator implements Comparator<PivasWardItem>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWardItem pivasWardItem1, PivasWardItem pivasWardItem2) {
			int compareResult = 0;
			
			compareResult = Boolean.valueOf(pivasWardItem1.isPivasWardEnableFlag()).compareTo(Boolean.valueOf(pivasWardItem2.isPivasWardEnableFlag())) * -1;
			
			if ( compareResult == 0 ) {
				compareResult = new CompareToBuilder()
									.append( pivasWardItem1.getPivasWardCode(), pivasWardItem2.getPivasWardCode() )
									.append( pivasWardItem1.getPivasWardDesc(), pivasWardItem2.getPivasWardDesc() )
									.toComparison();
			}
			
			return compareResult;
		}
    }
	
	private static class PivasFormulaAvailabilityComparator implements Comparator<PivasWardItem>, Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public int compare(PivasWardItem pivasWardItem1, PivasWardItem pivasWardItem2) {
			
			PivasWardPK pivasWardPK1 = pivasWardItem1.getPivasWard().getCompId();
			PivasWardPK pivasWardPK2 = pivasWardItem2.getPivasWard().getCompId();
			
			return new CompareToBuilder()
					.append(pivasWardPK1.getPatHospCode(), pivasWardPK2.getPatHospCode())
					.append(pivasWardPK1.getWard(), pivasWardPK2.getWard())
					.toComparison();
		}
    }	
}
