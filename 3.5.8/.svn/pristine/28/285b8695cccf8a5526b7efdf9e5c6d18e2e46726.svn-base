package hk.org.ha.model.pms.persistence.disp;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.persistence.corp.CddhSpecialtyMapping;
import hk.org.ha.model.pms.persistence.corp.Workstore;
import hk.org.ha.model.pms.udt.cddh.CddhRemarkStatus;
import hk.org.ha.model.pms.udt.disp.DiscontinueStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderAdminStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderBatchProcessingType;
import hk.org.ha.model.pms.udt.disp.DispOrderDayEndStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderItemStatus;
import hk.org.ha.model.pms.udt.disp.DispOrderStatus;
import hk.org.ha.model.pms.udt.disp.InvoiceStatus;
import hk.org.ha.model.pms.udt.printing.PrintLang;
import hk.org.ha.model.pms.udt.vetting.OrderType;
import hk.org.ha.model.pms.util.StopWatcher;
import hk.org.ha.model.pms.vo.label.ChestLabel;
import hk.org.ha.model.pms.vo.label.ChestLabelList;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;
import org.eclipse.persistence.indirection.IndirectList;

@Entity
@Table(name = "DISP_ORDER")
public class DispOrder extends VersionEntity {

	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_LABEL = "hk.org.ha.model.pms.vo.label";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dispOrderSeq")
	@SequenceGenerator(name = "dispOrderSeq", sequenceName = "SQ_DISP_ORDER", initialValue = 100000000)
	private Long id;

	@Column(name = "DISP_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dispDate;
	
	@Converter(name = "DispOrder.status", converterClass = DispOrderStatus.Converter.class)
    @Convert("DispOrder.status")
	@Column(name="STATUS", nullable = false, length = 2)
	private DispOrderStatus status;
	
	@Converter(name = "DispOrder.adminStatus", converterClass = DispOrderAdminStatus.Converter.class)
    @Convert("DispOrder.adminStatus")
	@Column(name="ADMIN_STATUS", nullable = false, length = 1)
	private DispOrderAdminStatus adminStatus;
	
	@Converter(name = "DispOrder.externalAdminStatus", converterClass = DispOrderAdminStatus.Converter.class)
    @Convert("DispOrder.externalAdminStatus")
	@Column(name="EXTERNAL_ADMIN_STATUS", nullable = false, length = 1)
	private DispOrderAdminStatus externalAdminStatus;
	
	@Converter(name = "DispOrder.dayEndStatus", converterClass = DispOrderDayEndStatus.Converter.class)
    @Convert("DispOrder.dayEndStatus")
	@Column(name="DAY_END_STATUS", nullable = false, length = 1)
	private DispOrderDayEndStatus dayEndStatus;

	@Column(name = "DAY_END_BATCH_DATE")
	@Temporal(TemporalType.DATE)
	private Date dayEndBatchDate;

	@Column(name = "BATCH_PROCESSING_FLAG", nullable = false)
	private Boolean batchProcessingFlag;
	
	@Converter(name = "DispOrder.batchProcessingType", converterClass = DispOrderBatchProcessingType.Converter.class)
    @Convert("DispOrder.batchProcessingType")
	@Column(name="BATCH_PROCESSING_TYPE", nullable = false, length = 1)
	private DispOrderBatchProcessingType batchProcessingType;
		
	@Column(name = "BATCH_REMARK", length = 500)
	private String batchRemark;
	
	@Column(name = "CHECK_USER", length = 12)
	private String checkUser;
	
	@Column(name = "CHECK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date checkDate;
	
	@Column(name = "ISSUE_USER", length = 12)
	private String issueUser;
	
	@Column(name = "ISSUE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date issueDate;
	
	@Column(name = "PICK_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date pickDate;
	
	@Column(name = "ASSEMBLE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date assembleDate;
	
	@Column(name = "URGENT_FLAG", nullable = false)
	private Boolean urgentFlag;
	
	@Converter(name = "DispOrder.printLang", converterClass = PrintLang.Converter.class)
	@Convert("DispOrder.printLang")
	@Column(name="PRINT_LANG", nullable = false, length = 10)
	private PrintLang printLang;

	@Column(name = "CHECK_WORKSTATION_CODE", length = 4)
	private String checkWorkstationCode;
	
	@Column(name = "ISSUE_WINDOW_NUM", length = 2)
	private Integer issueWindowNum;

	@Column(name = "SFI_FLAG", nullable = false)
	private Boolean sfiFlag;
	
	@Column(name = "UNCOLLECT_FLAG", nullable = false)
	private Boolean uncollectFlag;	

	@Column(name = "UNCOLLECT_USER", length = 12)
	private String uncollectUser;
	
	@Column(name = "UNCOLLECT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date uncollectDate;
	
	@Column(name = "REFILL_FLAG", nullable = false)
	private Boolean refillFlag;
	
	@Column(name = "SUSPEND_CODE", length = 2)
	private String suspendCode;
	
	@Lob
	@Column(name = "CHEST_LABEL_XML")
	@Basic(fetch=FetchType.LAZY)
	private String chestLabelXml;
	
	@Column(name = "SPEC_CODE", length = 4)
	private String specCode;
	
	@Column(name = "WARD_CODE", length = 4)
	private String wardCode;
	
	@Column(name = "PAT_CAT_CODE", length = 2)
	private String patCatCode;
	
	@Column(name = "ORG_DISP_ORDER_ID")
	private Long orgDispOrderId;
	
	@Column(name = "PREV_DISP_ORDER_ID")
	private Long prevDispOrderId;
	
	@Column(name = "DISP_ORDER_NUM")
	private Integer dispOrderNum;

	@Column(name = "ORG_DISP_ORDER_NUM")
	private Integer orgDispOrderNum;

	@Column(name = "PREV_DISP_ORDER_NUM")
	private Integer prevDispOrderNum;
	
	@Column(name = "ALERT_CHECK_FLAG", nullable = false)
	private Boolean alertCheckFlag;
	
	@Lob
	@Column(name = "ALERT_CHECK_REMARK")
	@Basic(fetch=FetchType.LAZY)
	private String alertCheckRemark;
	
    @Column(name = "HOSP_CODE", nullable = false, insertable = false, updatable = false)
    private String hospCode;            
	
	@Column(name = "VET_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date vetDate;
	
	@Column(name = "TICKET_CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date ticketCreateDate;
	
	@Column(name = "TICKET_DATE")
	@Temporal(TemporalType.DATE)
	private Date ticketDate;
	
	@Converter(name = "DispOrder.orderType", converterClass = OrderType.Converter.class)
    @Convert("DispOrder.orderType")
	@Column(name="ORDER_TYPE", nullable = false, length = 1)
	private OrderType orderType;

	@Transient
	private OrderType ticketOrderType;
	
	@Column(name = "TICKET_NUM", length = 4)
	private String ticketNum;          
	
	@Column(name = "REVOKE_FLAG")
	private Boolean revokeFlag;         
	
	@Column(name = "REVOKE_USER", length = 12)
	private String revokeUser;    
	
	@Column(name = "REVOKE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date revokeDate;        
	
	@Column(name = "REFRIG_ITEM_FLAG")
	private Boolean refrigItemFlag;  

	@Column(name = "ITEM_COUNT")
	private Integer itemCount;

    @PrivateOwned
    @OrderBy("sortSeq,id")
    @OneToMany(mappedBy="dispOrder", cascade=CascadeType.ALL)
	private List<DispOrderItem> dispOrderItemList;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PHARM_ORDER_ID", nullable = false)
    private PharmOrder pharmOrder;

    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name = "TICKET_ID")
    private Ticket ticket;

    @ManyToOne
	@JoinColumns({
		@JoinColumn(name = "HOSP_CODE", referencedColumnName = "HOSP_CODE", nullable = false),
		@JoinColumn(name = "WORKSTORE_CODE", referencedColumnName = "WORKSTORE_CODE", nullable = false)
	})
	private Workstore workstore;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PATIENT_ID")
    private Patient patient;
    
	@OneToMany(mappedBy = "dispOrder")
	private List<Invoice> invoiceList;
	
	@Transient
    private List<RefillSchedule> refillScheduleList;
	
	@Transient
    private List<CapdVoucher> capdVoucherList;

	//obsolete after pms-pms version 3.1.4.1
	@Transient
	private CddhSpecialtyMapping cddhSpecialtyMapping;
	
	@Transient
	private String cddhSpecCode;
	
	@Transient
	private Boolean cddhExceptionFlag;
	
	@Transient
	private transient ChestLabelList chestLabelList;

	@Transient
	private transient DispOrderStatus prevStatus;
	
	@Transient
	private CddhRemarkStatus cddhRemarkStatus;
	
	public DispOrder() {
		printLang = PrintLang.Eng;
		urgentFlag = Boolean.FALSE;
		sfiFlag = Boolean.FALSE;
		refillFlag = Boolean.FALSE;
		adminStatus = DispOrderAdminStatus.Normal;
		externalAdminStatus = DispOrderAdminStatus.Normal;
		dayEndStatus = DispOrderDayEndStatus.None;
		batchProcessingFlag = Boolean.FALSE;
		batchProcessingType = DispOrderBatchProcessingType.None;
		cddhExceptionFlag = Boolean.FALSE;
		alertCheckFlag = Boolean.FALSE;
		uncollectFlag = Boolean.FALSE;
		revokeFlag = Boolean.FALSE;
		refrigItemFlag = Boolean.FALSE;
	}

    @PostLoad
    public void postLoad() {
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.resume();
    	}
    
    	ticketOrderType = orderType;
		
    	if (StopWatcher.isDebugEnabled()) {
    		StopWatcher.suspend();
    	}
    }
	
	@PrePersist
	@PreUpdate
	public void preSave() {
		if (orderType == null && ticketOrderType != null) {
			orderType = ticketOrderType;
		}
		
		if (chestLabelList != null) {
			JaxbWrapper<ChestLabelList> chestLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			chestLabelXml = chestLabelJaxbWrapper.marshall(chestLabelList);
		}		

		// only update sortSeq when dispOrderItemList is not a lazy list
		if ( !(dispOrderItemList instanceof IndirectList) || ((IndirectList) dispOrderItemList).isInstantiated()) {		
			int i = 1;
		
			for (DispOrderItem item : this.getDispOrderItemList()) {
				item.setSortSeq(Integer.valueOf(i++));
			}
			
			itemCount = getDispOrderItemList().size();
		}
	}
		
	public void loadChestLabel() {
		if (chestLabelXml != null && chestLabelList == null) {
			JaxbWrapper<ChestLabelList> chestLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
			chestLabelList = chestLabelJaxbWrapper.unmarshall(chestLabelXml);
		}
	}
	
	public void clearId() {
		this.setId(null);
		for (DispOrderItem item : this.getDispOrderItemList()) {
			item.setId(null);
		}		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getDispDate() {
		if (dispDate == null) {
			return null;
		}
		else {
			return new Date(dispDate.getTime());
		}
	}

	public void setDispDate(Date dispDate) {
		if (dispDate == null) {
			this.dispDate = null;
		}
		else {
			this.dispDate = new Date(dispDate.getTime());
		}
	}
	
	public DispOrderStatus getStatus() {
		return status;
	}

	public void setStatus(DispOrderStatus status) {
		this.status = status;
	}
	
	public DispOrderAdminStatus getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(DispOrderAdminStatus adminStatus) {
		this.adminStatus = adminStatus;
	}

	public DispOrderAdminStatus getExternalAdminStatus() {
		return externalAdminStatus;
	}

	public void setExternalAdminStatus(DispOrderAdminStatus externalAdminStatus) {
		this.externalAdminStatus = externalAdminStatus;
	}

	public DispOrderDayEndStatus getDayEndStatus() {
		return dayEndStatus;
	}

	public void setDayEndStatus(DispOrderDayEndStatus dayEndStatus) {
		this.dayEndStatus = dayEndStatus;
	}

	public Date getDayEndBatchDate() {
		return dayEndBatchDate;
	}

	public void setDayEndBatchDate(Date dayEndBatchDate) {
		this.dayEndBatchDate = dayEndBatchDate;
	}

	public Boolean getBatchProcessingFlag() {
		return batchProcessingFlag;
	}

	public void setBatchProcessingFlag(Boolean batchProcessingFlag) {
		this.batchProcessingFlag = batchProcessingFlag;
	}

	public DispOrderBatchProcessingType getBatchProcessingType() {
		return batchProcessingType;
	}

	public void setBatchProcessingType(DispOrderBatchProcessingType batchProcessingType) {
		this.batchProcessingType = batchProcessingType;
	}

	public String getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(String checkUser) {
		this.checkUser = checkUser;
	}

	public Date getCheckDate() {
		if (checkDate == null) {
			return null;
		}
		else {
			return new Date(checkDate.getTime());
		}
	}

	public void setCheckDate(Date checkDate) {
		if (checkDate == null) {
			this.checkDate = null;
		}
		else {
			this.checkDate = new Date(checkDate.getTime());
		}
	}

	public String getIssueUser() {
		return issueUser;
	}

	public void setIssueUser(String issueUser) {
		this.issueUser = issueUser;
	}

	public Date getIssueDate() {
		if (issueDate == null) {
			return null;
		}
		else {
			return new Date(issueDate.getTime());
		}
	}

	public void setIssueDate(Date issueDate) {
		if (issueDate == null) {
			this.issueDate = null;
		}
		else {
			this.issueDate = new Date(issueDate.getTime());
		}
	}

	public Date getPickDate() {
		if (pickDate == null) {
			return null;
		}
		else {
			return new Date(pickDate.getTime());
		}
	}

	public void setPickDate(Date pickDate) {
		if (pickDate == null) {
			this.pickDate = null;
		}
		else {
			this.pickDate = new Date(pickDate.getTime());
		}
	}

	public Date getAssembleDate() {
		if (assembleDate == null) {
			return null;
		}
		else {
			return new Date(assembleDate.getTime());
		}
	}

	public void setAssembleDate(Date assembleDate) {
		if (assembleDate == null) {
			this.assembleDate = null;
		}
		else {
			this.assembleDate = new Date(assembleDate.getTime());
		}
	}

	public Boolean getUrgentFlag() {
		return urgentFlag;
	}

	public void setUrgentFlag(Boolean urgentFlag) {
		this.urgentFlag = urgentFlag;
	}

	public PrintLang getPrintLang() {
		return printLang;
	}

	public void setPrintLang(PrintLang printLang) {
		this.printLang = printLang;
	}

	public String getCheckWorkstationCode() {
		return checkWorkstationCode;
	}

	public void setCheckWorkstationCode(String checkWorkstationCode) {
		this.checkWorkstationCode = checkWorkstationCode;
	}

	public Integer getIssueWindowNum() {
		return issueWindowNum;
	}

	public void setIssueWindowNum(Integer issueWindowNum) {
		this.issueWindowNum = issueWindowNum;
	}
	
	public Boolean getSfiFlag() {
		return sfiFlag;
	}

	public void setSfiFlag(Boolean sfiFlag) {
		this.sfiFlag = sfiFlag;
	}
	
	public Boolean getUncollectFlag() {
		return uncollectFlag;
	}

	public void setUncollectFlag(Boolean uncollectFlag) {
		this.uncollectFlag = uncollectFlag;
	}
	
	public String getUncollectUser() {
		return uncollectUser;
	}

	public void setUncollectUser(String uncollectUser) {
		this.uncollectUser = uncollectUser;
	}

	public Date getUncollectDate() {
		if (uncollectDate == null) {
			return null;
		}
		else {
			return new Date(uncollectDate.getTime());
		}
	}

	public void setUncollectDate(Date uncollectDate) {
		if (uncollectDate == null) {
			this.uncollectDate = null;
		}
		else {
			this.uncollectDate = new Date(uncollectDate.getTime());
		}
	}

	public Boolean getRefillFlag() {
		return refillFlag;
	}

	public void setRefillFlag(Boolean refillFlag) {
		this.refillFlag = refillFlag;
	}

	public String getChestLabelXml() {
		return chestLabelXml;
	}

	public void setChestLabelXml(String chestLabelXml) {
		this.chestLabelXml = chestLabelXml;
	}

	public List<DispOrderItem> getDispOrderItemList() {
		if (dispOrderItemList == null) {
			dispOrderItemList = new ArrayList<DispOrderItem>();
		}
		return dispOrderItemList;
	}
	
	public void setDispOrderItemList(List<DispOrderItem> dispOrderItemList) {
		this.dispOrderItemList = dispOrderItemList;
	}
	
	public void addDispOrderItem(DispOrderItem dispOrderItem) {
		dispOrderItem.setDispOrder(this);
		getDispOrderItemList().add(dispOrderItem);
	}
	
	public void clearDispOrderItemList() {
		dispOrderItemList = new ArrayList<DispOrderItem>();
	}
	
	public PharmOrder getPharmOrder() {
		return pharmOrder;
	}

	public void setPharmOrder(PharmOrder pharmOrder) {
		this.pharmOrder = pharmOrder;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Workstore getWorkstore() {
		return workstore;
	}

	public void setWorkstore(Workstore workstore) {
		this.workstore = workstore;
	}

	public List<RefillSchedule> getRefillScheduleList() {
		if (refillScheduleList == null) {
			refillScheduleList = new ArrayList<RefillSchedule>(); 
		}
		return refillScheduleList;
	}

	public void setRefillScheduleList(List<RefillSchedule> refillScheduleList) {
		this.refillScheduleList = refillScheduleList;
	}
	
	public void addRefillSchedule(RefillSchedule refillSchedule) {
		refillSchedule.setDispOrder(this);
		getRefillScheduleList().add(refillSchedule);
	}
	
	public void clearRefillScheduleList() {
		refillScheduleList = new ArrayList<RefillSchedule>(); 
	}
	
	public List<CapdVoucher> getCapdVoucherList() {
		if (capdVoucherList == null) {
			capdVoucherList = new ArrayList<CapdVoucher>(); 
		}
		return capdVoucherList;
	}

	public void setCapdVoucherList(List<CapdVoucher> capdVoucherList) {
		this.capdVoucherList = capdVoucherList;
	}
	
	public void addCapdVoucher(CapdVoucher capdVoucher) {
		capdVoucher.setDispOrder(this);
		getCapdVoucherList().add(capdVoucher);
	}
	
	public void clearCapdVoucherList() {
		capdVoucherList = new ArrayList<CapdVoucher>(); 
	}	
	
	public List<Invoice> getInvoiceList() {
		if (invoiceList == null) {
			invoiceList = new ArrayList<Invoice>();
		}
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}
	
	public void addInvoice(Invoice invoice) {
		invoice.setDispOrder(this);
		getInvoiceList().add(invoice);
	}
	
	public void clearInvoiceList() {
		invoiceList = new ArrayList<Invoice>();
	}

	public CddhSpecialtyMapping getCddhSpecialtyMapping() {
		return cddhSpecialtyMapping;
	}

	public void setCddhSpecialtyMapping(CddhSpecialtyMapping cddhSpecialtyMapping) {
		this.cddhSpecialtyMapping = cddhSpecialtyMapping;
	}

	public String getCddhSpecCode() {
		return cddhSpecCode;
	}

	public void setCddhSpecCode(String cddhSpecCode) {
		this.cddhSpecCode = cddhSpecCode;
	}

	public ChestLabelList getChestLabelList() {
		return chestLabelList;
	}

	public void setChestLabelList(ChestLabelList chestLabelList) {
		this.chestLabelList = chestLabelList;
	}

	public String getSuspendCode() {
		return suspendCode;
	}

	public void setSuspendCode(String suspendCode) {
		this.suspendCode = suspendCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getPatCatCode() {
		return patCatCode;
	}

	public void setPatCatCode(String patCatCode) {
		this.patCatCode = patCatCode;
	}

	public Long getOrgDispOrderId() {
		return orgDispOrderId;
	}

	public void setOrgDispOrderId(Long orgDispOrderId) {
		this.orgDispOrderId = orgDispOrderId;
	}

	public Long getPrevDispOrderId() {
		return prevDispOrderId;
	}

	public void setPrevDispOrderId(Long prevDispOrderId) {
		this.prevDispOrderId = prevDispOrderId;
	}

	public Integer getDispOrderNum() {
		return dispOrderNum;
	}

	public void setDispOrderNum(Integer dispOrderNum) {
		this.dispOrderNum = dispOrderNum;
	}

	public Integer getOrgDispOrderNum() {
		return orgDispOrderNum;
	}

	public void setOrgDispOrderNum(Integer orgDispOrderNum) {
		this.orgDispOrderNum = orgDispOrderNum;
	}

	public Integer getPrevDispOrderNum() {
		return prevDispOrderNum;
	}

	public void setPrevDispOrderNum(Integer prevDispOrderNum) {
		this.prevDispOrderNum = prevDispOrderNum;
	}

	public Boolean getAlertCheckFlag() {
		return alertCheckFlag;
	}

	public void setAlertCheckFlag(Boolean alertCheckFlag) {
		this.alertCheckFlag = alertCheckFlag;
	}

	public String getAlertCheckRemark() {
		return alertCheckRemark;
	}

	public void setAlertCheckRemark(String alertCheckRemark) {
		this.alertCheckRemark = alertCheckRemark;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getBatchRemark() {
		return batchRemark;
	}

	public void setBatchRemark(String batchRemark) {
		this.batchRemark = batchRemark;
	}

	public Boolean getCddhExceptionFlag() {
		return cddhExceptionFlag;
	}

	public void setCddhExceptionFlag(Boolean cddhExceptionFlag) {
		this.cddhExceptionFlag = cddhExceptionFlag;
	}

	public DispOrderStatus getPrevStatus() {
		return prevStatus;
	}

	public void setPrevStatus(DispOrderStatus prevStatus) {
		this.prevStatus = prevStatus;
	}

	@Transient
	private transient boolean childLoaded = false;
	
	public void loadChild() {
		this.loadChild(true);
	}
		
	public void loadChild(boolean includeInvoice) {
		
		if (getId() == null) return;

		if (!childLoaded) {			
			childLoaded = true;

			this.getTicket();
			this.getWorkstore();
			this.getChestLabelXml();
			
			if (this.getPharmOrder() != null) {
				this.getPharmOrder().loadChild();
				this.getPharmOrder().addDispOrder(this);
			}
	
			for (DispOrderItem dispOrderItem : this.getDispOrderItemList()) {
				dispOrderItem.getDispOrder();
				if (dispOrderItem.getPharmOrderItem() != null) {
					dispOrderItem.getPharmOrderItem().addDispOrderItem(dispOrderItem);
				}
			}
						
			if (includeInvoice) {
				this.loadInvoice();
			}
			
			// finally init the list
			this.clearRefillScheduleList();
			this.clearCapdVoucherList();
		}
	}
	
	public void loadInvoice() {
		for (Invoice invoice : this.getInvoiceList()) {
			invoice.loadChild();
		}
	}
	
	public void loadHeader() {
		this.getTicket();
		this.getWorkstore();
		if (this.getPharmOrder() != null) {
			this.getPharmOrder().getMedCase();
			if (this.getPharmOrder().getMedOrder() != null) {
				this.getPharmOrder().getMedOrder().getPatient();
			}
		}
	}
	
	public void loadPharmOrderItem() {
		for (PharmOrderItem item : this.getPharmOrder().getPharmOrderItemList()) {
			item.getPharmOrder();
		}
	}
	
	public void replace(PharmOrder po) {
		for (PharmOrderItem item : po.getPharmOrderItemList()) {
			item.clearDispOrderItemList();
		}

		for (DispOrderItem item : this.getDispOrderItemList()) {
			PharmOrderItem moi = po.getPharmOrderItemByItemNum(
					item.getPharmOrderItem().getItemNum());
			moi.addDispOrderItem(item);
		}
		
		po.clearDispOrderList();		
		po.addDispOrder(this);
		
		setPharmOrder(po);
	}	

	public List<Invoice> markDeleted() {
		if (this.getStatus() == DispOrderStatus.SysDeleted) {
			return new ArrayList<Invoice>();
		}
		
		return this.markDeleted(DispOrderStatus.Deleted);
	}
	
	public List<Invoice> markSysDeleted() {
		return this.markDeleted(DispOrderStatus.SysDeleted);
	}
	
	public List<Invoice> markDeleted(DispOrderStatus status) {
		List<Invoice> voidInvoiceList = new ArrayList<Invoice>();

		if (this.getStatus() != status) {
			this.setPrevStatus(this.getStatus());
			this.setStatus(status);
			
			for (Invoice invoice : this.getInvoiceList()) {
				if (invoice.getStatus() != InvoiceStatus.Void) {
					invoice.setStatus(InvoiceStatus.Void);
					voidInvoiceList.add(invoice);
				}
			}
			
			if( DispOrderStatus.Deleted == status ){
				this.markItemDeleted();
			}			
		}
		
		return voidInvoiceList;
	}
	
	public void markItemDeleted() {
		for (DispOrderItem item : this.getDispOrderItemList()) {
			item.setStatus(DispOrderItemStatus.Deleted);
		}
	}
	
	public DispOrderItem getDispOrderItemByPoItemNum(Integer itemNum) {
		for (DispOrderItem item : this.getDispOrderItemList()) {
			if (item.getPharmOrderItem().getItemNum().equals(itemNum)) {
				return item;
			}
		}
		return null;
	}
	
	public DispOrderItem getDispOrderItemByPoOrgItemNum(Integer itemNum) {
		for (DispOrderItem item : this.getDispOrderItemList()) {
			if (item.getPharmOrderItem().getOrgItemNum().equals(itemNum)) {
				return item;
			}
		}
		return null;
	}
	
	@Transient
	private transient List<Invoice> _invoiceList = null;

	public void disconnectInvoiceList() {
		
		_invoiceList = this.getInvoiceList();
		this.clearInvoiceList();
		
		for (DispOrderItem item : this.getDispOrderItemList()) {
			item.clearInvoiceItemList();
		}
	}
	
	public void reconnectInvoiceList() {
		
		this.setInvoiceList(_invoiceList);
		
		for (Invoice invoice : _invoiceList) {
			for (InvoiceItem invoiceItem : invoice.getInvoiceItemList()) {
				invoiceItem.getDispOrderItem().addInvoiceItem(invoiceItem);
			}
		}
	}
	
	@Transient
	private transient List<RefillSchedule> _refillScheduleList = null;
		
	public void disconnectRefillScheduleList() {
		
		_refillScheduleList = this.getRefillScheduleList();
		this.clearRefillScheduleList();
	}
	
	public void reconnectRefillScheduleList() {
				
		this.setRefillScheduleList(_refillScheduleList);
	}	
	
	@Transient
	private transient List<CapdVoucher> _capdVoucherList = null;
		
	public void disconnectCapdVoucherList() {
		
		_capdVoucherList = this.getCapdVoucherList();
		this.clearCapdVoucherList();
		
		for (DispOrderItem item : this.getDispOrderItemList()) {
			item.disconnectCapdVoucherItemList();
		}
	}
	
	public void reconnectCapdVoucherList() {
				
		if (_capdVoucherList != null) {
			this.setCapdVoucherList(_capdVoucherList);
			
			for (DispOrderItem item : this.getDispOrderItemList()) {
				item.reconnectCapdVoucherItemList();
			}
		}
	}	

	public void updateFromPrevDispOrder(DispOrder prevDispOrder) {

		if (!isDirty()) {

			DispOrderStatus prevDispOrderStatus = prevDispOrder.getPrevStatus();
			
			if (status.isLaterThanEqualTo(DispOrderStatus.Picked) && prevDispOrderStatus.isLaterThanEqualTo(DispOrderStatus.Picked)) {
				pickDate = prevDispOrder.getPickDate();
			}
			
			if (status.isLaterThanEqualTo(DispOrderStatus.Checked) && prevDispOrderStatus.isLaterThanEqualTo(DispOrderStatus.Checked)) {
				checkUser = prevDispOrder.getCheckUser();
				checkDate = prevDispOrder.getCheckDate();
			}
			
			if (status.isLaterThanEqualTo(DispOrderStatus.Assembled) && prevDispOrderStatus.isLaterThanEqualTo(DispOrderStatus.Assembled)) {
				assembleDate = prevDispOrder.getAssembleDate();
			}
			
			if (status.isLaterThanEqualTo(DispOrderStatus.Issued) && prevDispOrderStatus.isLaterThanEqualTo(DispOrderStatus.Issued)) {
				issueUser = prevDispOrder.getIssueUser();
				if ( ! (prevDispOrder.getAdminStatus() == DispOrderAdminStatus.Suspended && adminStatus == DispOrderAdminStatus.Normal) ) {
					// if DO is changed from suspend to unsuspend, skip setting issue date from previous order to make issueDate updated (for CDDH use)
					issueDate = prevDispOrder.getIssueDate();					
				}
			}
		}
		vetDate = prevDispOrder.getVetDate();
		
		issueWindowNum       = prevDispOrder.getIssueWindowNum();
		checkWorkstationCode = prevDispOrder.getCheckWorkstationCode();
		batchRemark         = prevDispOrder.getBatchRemark();
		orgDispOrderId       = prevDispOrder.getOrgDispOrderId();
		prevDispOrderId      = prevDispOrder.getId();
	}

	private boolean isDirty() {
		for (DispOrderItem doi : getDispOrderItemList()) {
			if (doi.getPharmOrderItem().isDirtyFlag()) {
				return true; 
			}
		}
		return false;
	}
	
	public Integer getMaxItemNum() {
		Integer maxItemNum = Integer.valueOf(0);
		for (DispOrderItem doi : getDispOrderItemList()) {
			if (doi.getItemNum() > maxItemNum) {
				maxItemNum = doi.getItemNum();
			}
		}
		return maxItemNum;
	}

	public CddhRemarkStatus getCddhRemarkStatus() {
		return cddhRemarkStatus;
	}

	public void setCddhRemarkStatus(CddhRemarkStatus cddhRemarkStatus) {
		this.cddhRemarkStatus = cddhRemarkStatus;
	}
	
	public boolean isDiscontinue() {
		return !getDiscontinueItem().isEmpty();
	}
	
	public List<MedOrderItem> getDiscontinueItem() {
		List<MedOrderItem> moiList = new ArrayList<MedOrderItem>();
		Set<MedOrderItem> moiSet = new HashSet<MedOrderItem>();
		for (DispOrderItem doi : getDispOrderItemList()) {
			MedOrderItem moi = doi.getPharmOrderItem().getMedOrderItem();
			if (moi.getDiscontinueStatus() == DiscontinueStatus.Confirmed || moi.getDiscontinueStatus() == DiscontinueStatus.Pending) {
				if ( !moiSet.contains(moi) ) {
					moiList.add(moi);
					moiSet.add(moi);
				}
			}
		}
		return moiList;
	}

	public void setVetDate(Date vetDate) {
		this.vetDate = vetDate;
	}
	
	public Date getVetDate() {
		return vetDate;
	}

	public void setTicketCreateDate(Date ticketCreateDate) {
		this.ticketCreateDate = ticketCreateDate;
	}

	public Date getTicketCreateDate() {
		return ticketCreateDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}
	
	public Date getTicketDate() {
		return ticketDate;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
		this.ticketOrderType = orderType;
	}
	
	public OrderType getOrderType() {
		return orderType != null ? orderType : ticketOrderType;
	}

	public void setTicketNum(String ticketNum) {
		this.ticketNum = ticketNum;
	}

	public String getTicketNum() {
		return ticketNum;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void setRevokeUser(String revokeUser) {
		this.revokeUser = revokeUser;
	}

	public String getRevokeUser() {
		return revokeUser;
	}

	public void setRevokeDate(Date revokeDate) {
		this.revokeDate = revokeDate;
	}

	public Date getRevokeDate() {
		return revokeDate;
	}

	public void setRefrigItemFlag(Boolean refrigItemFlag) {
		this.refrigItemFlag = refrigItemFlag;
	}

	public Boolean getRefrigItemFlag() {
		return refrigItemFlag;
	}

	public void setRevokeFlag(Boolean revokeFlag) {
		this.revokeFlag = revokeFlag;
	}

	public Boolean getRevokeFlag() {
		return revokeFlag;
	}
	
	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}

	public Integer getItemCount() {
		return itemCount;
	}

	public Boolean isTpnRequest() {
		for (DispOrderItem dispOrderItem : getDispOrderItemList()) {
			if (dispOrderItem.getTpnRequestId() != null) {
				return true;
			}
		}
		return false;
	}
	
	public void setLargeLayoutFlag(Boolean largeLayoutFlag) {
		if (chestLabelList == null) {
			return;
		}
		for (ChestLabel chestLabel : chestLabelList.getChestLabels()) {
			chestLabel.setLargeLayoutFlag(largeLayoutFlag);
		}
		
		if (chestLabelXml == null) {
			return;
		}
		JaxbWrapper<ChestLabelList> chestLabelJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LABEL);
		chestLabelXml = chestLabelJaxbWrapper.marshall(chestLabelList);
	}
}