package hk.org.ha.model.pms.persistence;

import flexjson.JSON;
import hk.org.ha.model.pms.persistence.reftable.Prop;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

@MappedSuperclass
public class PropEntity extends VersionEntity {

	private static final long serialVersionUID = 1L;	
		
	@Column(name = "VALUE", length=1000)
	private String value;
	
	@Column(name = "SORT_SEQ")
	private Integer sortSeq;
			
    @JoinFetch(JoinFetchType.INNER)
	@ManyToOne
	@JoinColumn(name = "PROP_ID", nullable = false)
	private Prop prop;	
	
	@Column(name = "PROP_ID", nullable = false, insertable = false, updatable = false)
	private Long propId;
    
	@Transient
	private String orgValue;
		
	public PropEntity() {
		prop = new Prop();
	}
	
	@PostLoad
	public void postLoad() {
		orgValue = value;
	}

	public Prop getProp() {
		return prop;
	}

	public void setProp(Prop prop) {
		this.prop = prop;
	}


	public String getValue() {
		if (_value != null) {
			return _value;
		}
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
			
	public Integer getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}
	
	@JSON(include=false)
	public Object getConvertedValue() {
		switch (prop.getType()) {
			
			case IntegerType :
			case DayType :
			case MonthType :
			case YearType :
			case MinuteType :
			case SecondType :
			case MillisecondType :
			case HourType :
			case WeekType :
			case Hour1To24Type :
			case DayOfWeekType : 
				return StringUtils.isBlank(this.getValue()) ? null : Integer.valueOf(this.getValue());
			case BooleanType :
				if ("Y".equalsIgnoreCase(this.getValue())) {
					return Boolean.TRUE;
				} else if ("N".equalsIgnoreCase(this.getValue())) {
					return Boolean.FALSE;
				} else {
					return Boolean.valueOf(this.getValue());
				}
			case DateTimeType :
				return DateTimeFormat.forPattern(this.getProp().getValidation())
					.parseDateTime(this.getValue());
			case ListStringType :
				return Arrays.asList(this.getValue().split(","));
			case ListIntegerType :
				String[] sa = this.getValue().split(",");
				Integer[] ia = new Integer[sa.length];
				for (int i = 0; i < ia.length; i++) {
					ia[i] = Integer.valueOf(sa[i]);
				}
				return Arrays.asList(ia);
			case DoubleType : 
				return StringUtils.isBlank(this.getValue()) ? null : Double.valueOf(this.getValue());
			default :
				return this.getValue();
		}
	}
	
	@Transient
	private transient String _value;
	
	public void setConvertedValue(Object o) {
		switch (prop.getType()) {
		case BooleanType :
			if (Boolean.TRUE.equals(o)) {
				_value = "Y";
			} else {
				_value = "N";
			}
			break;
		case DateTimeType :
			if (o instanceof Date) {
				_value = DateTimeFormat.forPattern(this.getProp().getValidation())
					.print(new DateTime((Date) o));
			} else if (o instanceof DateTime) {
				_value = DateTimeFormat.forPattern(this.getProp().getValidation())
					.print((DateTime) o);
			} else {
				_value = o.toString();
			}
			break;
		default :
			_value = o.toString();
		}
	}

	public void setOrgValue(String orgValue) {
		this.orgValue = orgValue;
	}

	public String getOrgValue() {
		return orgValue;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public Long getPropId() {
		return propId;
	}
}
