<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("routeFormDisplaySeqMaintView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.main.info.popup.ShowKeyGuidePopupEvent;
			import hk.org.ha.event.pms.main.reftable.RetrievePasSpecialtyListEvent;
			import hk.org.ha.event.pms.main.reftable.RetrieveRouteFormDefaultSeqListEvent;
			import hk.org.ha.event.pms.main.reftable.RetrieveRouteFormSortSpecListEvent;
			import hk.org.ha.event.pms.main.reftable.UpdatePmsRouteSortSpecListEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.core.ShortcutKeyEvent;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopup;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.fmk.pms.flex.components.window.UpdateCloseMenuItemEvent;
			import hk.org.ha.fmk.pms.flex.components.window.CloseActiveWindowEvent;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			import hk.org.ha.model.pms.dms.persistence.PmsFormNum;
			import hk.org.ha.model.pms.dms.persistence.PmsRouteFormSortSpec;
			import hk.org.ha.model.pms.dms.persistence.PmsRouteFormSortSpecPK;
			import hk.org.ha.model.pms.dms.persistence.PmsRouteNum;
			import hk.org.ha.model.pms.dms.persistence.PmsRouteSortSpec;
			import hk.org.ha.model.pms.dms.persistence.PmsRouteSortSpecPK;
			import hk.org.ha.model.pms.udt.patient.PasSpecialtyType;
			import hk.org.ha.model.pms.vo.patient.PasSpecialty;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			import mx.collections.ArrayCollection;
			import mx.controls.dataGridClasses.DataGridColumn;
			import mx.core.IFlexDisplayObject;
			import mx.core.IUIComponent;
			import mx.core.UIComponent;
			import mx.events.DragEvent;
			import mx.events.ListEvent;
			import mx.managers.DragManager;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			
			import spark.events.IndexChangeEvent;
			
			public var pmsRouteFormSortSpec:PmsRouteFormSortSpec;
			public var pmsRouteFormSortSpecPK:PmsRouteFormSortSpecPK;
			public var pmsRouteSortSpec:PmsRouteSortSpec;
			public var pmsRouteSortSpecPK:PmsRouteSortSpecPK;
			public var pmsRouteNum:PmsRouteNum;
			public var pmsFormNum:PmsFormNum;
			
			
			[In]
			public var sysMsgMap:SysMsgMap;
			
			[In][Bindable]
			public var routeFormPasSpecialtyList:ArrayCollection = new ArrayCollection();
			
			[In][Bindable]
			public var pmsRouteSortSpecList:ArrayCollection = new ArrayCollection();
			
			[Bindable]
			public var typeList:ArrayCollection = new ArrayCollection(PasSpecialtyType.constants);
			
			[In]
			public var closeMenuItem:MenuItem;	
			
			private var clonePmsRouteSortSpecList:ArrayCollection;
			private var clonePmsRouteFormSortSpecList:ArrayCollection;
			
			public override function onShowLater():void{
				if (!pmsToolbar.hasInited()) {
					type.selectedItem = PasSpecialtyType.Blank;
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.saveYesFunc = saveYesHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.defaultSequenceFunc = setDefaultSeqHandler;
					pmsToolbar.closeFunc = closeHandler;
					pmsToolbar.init();
					specialty.setFocus();
					pmsToolbar.saveButton.enabled = false;
					pmsToolbar.defaultSequenceButton.enabled = false;
					clearHandler();
				}
				callLater(disbaleMenuBarCloseButton);
				stage.addEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.setKeyGuideHandler(showRouteFormDisplaySeqMaintShortcutKeyHandler);
				
			}
			
			private function showRouteFormDisplaySeqMaintShortcutKeyHandler():void
			{
				dispatchEvent(new ShowKeyGuidePopupEvent("RouteFormDisplaySeqMaintView"));
			}
			
			private function disbaleMenuBarCloseButton():void
			{
				this.dispatchEvent(new UpdateCloseMenuItemEvent(false));
				
			}
			
			public override function onHide():void {
				stage.removeEventListener(KeyboardEvent.KEY_UP, pmsToolbarShortcutKeyHandler);
				parentApplication.clearKeyGuideHandler();
			}
			
			private function closeHandler():void {
				specialty.setFocus();
				pmsToolbar.saveButton.enabled = false;
				pmsToolbar.defaultSequenceButton.enabled = false;
				clearHandler();
				this.dispatchEvent(new CloseActiveWindowEvent());
			}
			
			private function initLookupPopup():void{
				if(type.selectedIndex == -1){
					type.selectedItem = PasSpecialtyType.Blank;
				}
				initLookupPopupCallBack();
				
				dispatchEvent(new RetrievePasSpecialtyListEvent(specialty.text, type.selectedItem));
			}
			
			public function initLookupPopupCallBack():void {
				var lookupPopProp:LookupPopupProp = new LookupPopupProp();	
				
				lookupPopProp.doubleClickHandler = function(evt:ListEvent):void {
					var lookupPopup:LookupPopup = (UIComponent(evt.currentTarget).parentDocument) as LookupPopup;
					var selectedItem:PasSpecialty = evt.itemRenderer.data as PasSpecialty;
					
					specialty.text = selectedItem.pasSpecCode;
					type.selectedItem = selectedItem.type;
					
					retrieveHandler();
					PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				};
				
				lookupPopProp.title = "PAS Specialty Lookup";
				lookupPopProp.lookupWinHeight=350;
				lookupPopProp.lookupWinWidth=600;
				var typeCol:DataGridColumn = LookupPopupProp.getColumn("Type","type");
				var specCodeCol:DataGridColumn = LookupPopupProp.getColumn("Spec. Code","pasSpecCode");
				var specDescCol:DataGridColumn = LookupPopupProp.getColumn("Description","pasDescription");
				typeCol.width = 48;
				typeCol.minWidth = 48;
				specCodeCol.width = 72;
				specCodeCol.minWidth = 75;
				specDescCol.minWidth = 145;
				
				typeCol.sortable = false;
				specCodeCol.sortable = false;
				specDescCol.sortable = false;
				
				typeCol.labelFunction = pasSpecTypeLabelFunc;
				lookupPopProp.columns = [typeCol, specCodeCol, specDescCol];
				lookupPopProp.dataProvider = routeFormPasSpecialtyList;	
				
				dispatchEvent(new ShowLookupPopupEvent(lookupPopProp));
			}
			
			private function pasSpecTypeLabelFunc(item:Object, column:DataGridColumn):String 
			{
				return item[column.dataField].displayValue;
			}
			
			private function mappingTypeLabelFunc(item:Object):String 
			{
				return item.displayValue;
			}
			
			private function checkDragLocation(evt:DragEvent, component:IUIComponent):void 
			{
				evt.preventDefault(); 
				if (evt.dragInitiator != component) {
					DragManager.acceptDragDrop(evt.target as IUIComponent);
				} else { 
					DragManager.showFeedback(DragManager.NONE); 
				}
			}
			
			private function retrieveHandler():void {
				
				clonePmsRouteSortSpecList = null;
				clonePmsRouteFormSortSpecList = null;
				
				if(type.selectedIndex == -1){
					type.selectedItem = PasSpecialtyType.Blank;
				}
				
				if (specialty.text == "" || specialty.text == null) {
					specialty.setFocus();
					specialty.errorString = sysMsgMap.getMessage("0420");
				}else {
					if(type.selectedItem == PasSpecialtyType.InPatient || type.selectedItem == PasSpecialtyType.OutPatient){
						dispatchEvent(new RetrieveRouteFormSortSpecListEvent(specialty.text, type.selectedItem));
						
						specialty.enabled = false;
						specialtyLookupBtn.enabled = false;
						type.enabled = false;
						
						pmsToolbar.saveButton.enabled = true;
						pmsToolbar.retrieveButton.enabled = false;
						
						pmsToolbar.defaultSequenceButton.enabled = true;
						
						type.errorString = "";
						specialty.errorString = "";
						
					}else{
						type.setFocus();
						type.errorString = sysMsgMap.getMessage("0421");
					}
				}
				
				
			}
			
			private function saveYesHandler():void {
				clonePmsRouteSortSpecList = ObjectUtil.copy(pmsRouteSortSpecList) as ArrayCollection;
				if (pmsRouteSortSpecList.length>0){
					dispatchEvent(new UpdatePmsRouteSortSpecListEvent(pmsRouteSortSpecList));
				}
			}
			
			private function clearHandler():void {
				specialty.enabled = true;
				specialtyLookupBtn.enabled = true;
				type.enabled = true;	
				
				routebox.dataProvider = null;
				formbox.dataProvider = null;
				
				specialty.text="";
				type.selectedItem="";
				
				specialty.setFocus();
				
				pmsToolbar.saveButton.enabled = false;
				pmsToolbar.retrieveButton.enabled = true;
				
				pmsToolbar.defaultSequenceButton.enabled = false;
				
				type.errorString = "";
				specialty.errorString = "";
				
				pmsRouteSortSpec = null;
				pmsRouteFormSortSpec = null;
				clonePmsRouteSortSpecList = null;
				clonePmsRouteFormSortSpecList = null;
			}
			
			public function setDisplayList():void {
				if(pmsRouteSortSpecList!=null){
					routebox.dataProvider = pmsRouteSortSpecList;
					routebox.selectedIndex = 0;
					routebox.setFocus();
					pmsRouteSortSpec = routebox.selectedItem;
					clonePmsRouteSortSpecList = ObjectUtil.copy(pmsRouteSortSpecList) as ArrayCollection;
					clonePmsRouteFormSortSpecList = new ArrayCollection(pmsRouteSortSpec.pmsRouteFormSortSpecList.toArray());
				}else{
					specialty.errorString = sysMsgMap.getMessage("0025");
					specialty.setFocus();
					specialty.enabled = true;
					specialtyLookupBtn.enabled = true;
					type.enabled = true;
					
					pmsToolbar.saveButton.enabled = false;
					pmsToolbar.retrieveButton.enabled = true;
					
					pmsToolbar.defaultSequenceButton.enabled = false;
				}
				
			}
			
			public function setDefaultSeqHandler():void {
				dispatchEvent(new RetrieveRouteFormDefaultSeqListEvent(specialty.text, type.selectedItem));		
			}
			
			public function showSystemMessage(errorCode:String):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if (errorCode == "0446") {
					msgProp.okHandler = refreshRouteFormDisplaySeqMaint;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function refreshRouteFormDisplaySeqMaint(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				pmsRouteSortSpec = null;
				pmsRouteFormSortSpec = null;
				clonePmsRouteFormSortSpecList = null;
				clearHandler();
			}
			
			private function pmsToolbarShortcutKeyHandler(evt:KeyboardEvent):void {
				if ( !PopupUtil.isAnyVisiblePopupExists() ) {
					if ( evt.charCode == 55 && evt.ctrlKey && pmsToolbar.saveButton.enabled){	
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.saveButton));
					}
					else if ( evt.charCode == 52 && evt.ctrlKey && pmsToolbar.clearButton.enabled){	
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.clearButton));
					}	
					else if ( evt.charCode == 48 && evt.ctrlKey && pmsToolbar.closeButton.enabled){  //ctrl + 0 - close
						dispatchEvent(new ShortcutKeyEvent(pmsToolbar.closeButton));
					}
				}
			}
			
			private function lookupShortcutHandler(evt:KeyboardEvent):void {
				if ( evt.charCode == 53 && evt.ctrlKey && specialtyLookupBtn.enabled){
					initLookupPopup();
				}
			}
			
			public function refreshDefaultSeqList():void {
				routebox.selectedIndex = 0;
				routebox.setFocus();
				pmsRouteSortSpec = routebox.selectedItem;
				clonePmsRouteFormSortSpecList = new ArrayCollection(pmsRouteSortSpec.pmsRouteFormSortSpecList.toArray());
			}
			
			private function focusNextItem(evt:FocusEvent):void {
				if ( evt.keyCode == Keyboard.TAB && pmsToolbar.retrieveButton.enabled) {
					switch ( evt.currentTarget ) {
						case specialty:
							evt.preventDefault();
							if (!evt.shiftKey) {
								callLater(specialtyLookupBtn.setFocus);
							}else {
								callLater(type.setFocus);
							}
							break;
						
						case specialtyLookupBtn:
							evt.preventDefault();
							if (!evt.shiftKey) {
								callLater(type.setFocus);
							}else {
								callLater(specialty.setFocus);
							}
							break;
						
						case type:
							evt.preventDefault();
							if (!evt.shiftKey) {
								callLater(specialty.setFocus);
							}else {
								callLater(specialtyLookupBtn.setFocus);
							}
							break;
					}
				}
			}
			
			private function pressEnterHandler(evt:KeyboardEvent):void {
				if ( evt.keyCode == 13) {
					retrieveHandler();
				}
			}
			
			public function restoreToModifyView():void {
				routebox.dataProvider = clonePmsRouteSortSpecList;
				for (var i:int=0; i < clonePmsRouteSortSpecList.length; i++)
				{
					if ((clonePmsRouteSortSpecList.getItemAt(i) as PmsRouteSortSpec).routeDesc == pmsRouteSortSpec.routeDesc)
					{
						routebox.selectedIndex = i;
						(clonePmsRouteSortSpecList.getItemAt(i) as PmsRouteSortSpec).pmsRouteFormSortSpecList = clonePmsRouteFormSortSpecList;
						break;
					}
				}
								
				if (pmsRouteFormSortSpec != null)
				{
					callLater(reselectFormItem);
				}				
			}
			
			private function reselectFormItem():void
			{
				for (var j:int=0; j < formbox.dataProvider.length; j++)
				{
					if ((formbox.dataProvider.getItemAt(j) as PmsRouteFormSortSpec).formDesc == pmsRouteFormSortSpec.formDesc)
					{
						formbox.selectedIndex = j;
						break;
					}
				}
			}
			
			private function listChangeHandler(evt:IndexChangeEvent):void
			{
				cloneReference(evt.currentTarget);
			}
			
			private function dragCompletedHandler(evt:DragEvent):void
			{
				cloneReference(evt.currentTarget);
			}
			
			private function cloneReference(obj:Object):void
			{
				switch (obj)
				{
					case routebox:
						pmsRouteSortSpec = obj.selectedItem as PmsRouteSortSpec;
						pmsRouteFormSortSpec = null;
						break;
					
					case formbox:
						pmsRouteSortSpec = routebox.selectedItem as PmsRouteSortSpec;
						pmsRouteFormSortSpec = obj.selectedItem as PmsRouteFormSortSpec;
						break;
				}
				
				clonePmsRouteFormSortSpecList = new ArrayCollection(pmsRouteSortSpec.pmsRouteFormSortSpecList.toArray());
			}
			
		]]>
	</fx:Script>
	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<s:VGroup width="100%" gap="10" left="0" right="0">
		<fc:Toolbar id="pmsToolbar" width="100%"/>
	</s:VGroup>
	<s:VGroup width="100%" height="100%" paddingTop="10" paddingLeft="10" paddingRight="10" paddingBottom="10" gap="10">
		<s:HGroup width="100%" height="5%" verticalAlign="middle" >
			<s:Label text="PAS Specialty"/>
			<fc:UppercaseTextInput id="specialty" enter="retrieveHandler()" maxChars="4" keyFocusChange="focusNextItem(event)" keyUp="lookupShortcutHandler(event)"/>
			<fc:LookupButton id="specialtyLookupBtn" click="initLookupPopup()" width="28"/>
			<s:VGroup width="21" height="100%">
			</s:VGroup>
			<s:Label text="Mapping Type"/>
			<fc:AdvancedDropDownList id="type" width="112" dataProvider="{typeList}" labelFunction="mappingTypeLabelFunc" prompt="(Blank)" 
							keyUp="pressEnterHandler(event)" keyFocusChange="focusNextItem(event)"/>
		</s:HGroup>
		
		<s:HGroup width="600" height="400">
			<s:Panel width="50%" height="100%" title="Route">
				<s:List id="routebox" styleName="routeBoxStyle" width="100%" height="100%" dragEnabled="true" change="listChangeHandler(event)"
						dataProvider="{pmsRouteSortSpecList}"
						dragMoveEnabled="true" dropEnabled="true" dragEnter="checkDragLocation(event, formbox)"
						dragComplete="dragCompletedHandler(event)"
						labelField="routeDesc" keyFocusChange="focusNextItem(event)"></s:List>
			</s:Panel>
			<mx:Spacer width="10"/>
			
			<s:Panel width="50%" height="100%" title="Form">
				<s:List id="formbox"  styleName="formBoxStyle" width="100%" height="100%" dragEnabled="true" change="listChangeHandler(event)"
						dragMoveEnabled="true" dropEnabled="true" dragEnter="checkDragLocation(event, routebox)"
					 	dragComplete="dragCompletedHandler(event)" dataProvider="{routebox.selectedItem.pmsRouteFormSortSpecList}"
						labelField="formDesc" keyFocusChange="focusNextItem(event)"></s:List>
			</s:Panel>
		</s:HGroup>
		<s:Label text="Drag &amp; Drop to arrange Route-Form display sequence" styleName="footNoteLabelStyle"/>
	</s:VGroup>
	
	
</fc:ExtendedNavigatorContent>