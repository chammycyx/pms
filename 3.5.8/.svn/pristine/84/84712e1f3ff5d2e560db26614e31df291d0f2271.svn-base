<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:mx="library://ns.adobe.com/flex/mx">
	
	<fx:Metadata>
		[Name("mpOrderEditItemPopup")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[							
			import hk.org.ha.event.pms.main.drug.RetrieveDrugDiluentListByRxItemEvent;
			import hk.org.ha.event.pms.main.drug.RetrieveWorkstoreDrugListByRxItemEvent;
			import hk.org.ha.event.pms.main.reftable.RetrieveSolventListEvent;
			import hk.org.ha.model.pms.dms.persistence.DmDrug;
			import hk.org.ha.model.pms.vo.drug.WorkstoreDrug;
			import hk.org.ha.model.pms.vo.rx.InjectionRxDrug;
			import hk.org.ha.model.pms.vo.rx.IvRxDrug;
			import hk.org.ha.model.pms.vo.rx.RxItem;
			
			import mx.collections.ListCollectionView;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			
			[In]
			public function set workstoreDrugList(val:ListCollectionView):void 
			{
				if (val) {
					_workstoreDrugList = val;
					if (_workstoreDrugList != null && _workstoreDrugList.length > 0) {
						drugNameLabel = WorkstoreDrug(_workstoreDrugList.getItemAt(0)).dmDrug.drugName						
					} else {
						drugNameLabel = "";
					}					
					setDefaultFocus();
				}
			}
			
			[In] [Bindable]
			public var drugDiluentList:ListCollectionView;						
			
			[In] [Bindable]
			public var solventList:ListCollectionView;
			
			[Bindable]
			public var rxItem:RxItem;						
			[Bindable]
			public var manualProfileFlag:Boolean;
			public var okHandler:Function;						
			
			[Bindable]
			private var _workstoreDrugList:ListCollectionView;
			[Bindable]
			private var drugNameLabel:String;
			[Bindable]
			private var itemListGridSelectedIndex:int=0;
			private var dmDrug:DmDrug;
			
			public function setDefaultExtendedListItem():void
			{
				if (_workstoreDrugList.length > 0) {
					itemListGrid.selectedIndex = 0;
				}
				itemListGrid.setFocus();
				itemListGrid.focusManager.showFocus();
				solventListGrid.selectedIndex = -1;
				drugDiluentListGrid.selectedIndex = -1;
			}
			
			public function setDefaultSolventListItem():void
			{
				if (solventList.length > 0) {
					solventListGrid.selectedIndex = 0;
				}
				solventListGrid.setFocus();
				solventListGrid.focusManager.showFocus();
				itemListGrid.selectedIndex = -1;
				drugDiluentListGrid.selectedIndex = -1;
			}
			
			public function set doubleClickHandler(doubleClickHandlerVal:Function):void
			{				
				this.itemListGrid.addEventListener(ListEvent.ITEM_DOUBLE_CLICK,doubleClickHandlerVal);
				this.drugDiluentListGrid.addEventListener(ListEvent.ITEM_DOUBLE_CLICK,doubleClickHandlerVal);
				this.solventListGrid.addEventListener(ListEvent.ITEM_DOUBLE_CLICK,doubleClickHandlerVal);
			}
			
			protected function okKeyDownHandler(event:KeyboardEvent):void
			{
				if ( event.keyCode != Keyboard.ENTER ) {
					return;
				} 
				else if (okHandler != null) {
					okHandler(event);
				}
			}
			
			protected function cancelHandler(event:Event):void {
				PopUpManager.removePopUp(this);
			}
			
			public function setDrugDiluentListVisibility(val:Boolean):void {				
				diluentGroup.visible = val;
				diluentGroup.includeInLayout = val;
			}
			
			protected function diluentChkboxClick(event:MouseEvent):void {
				setDrugDiluentListVisibility( diluentChkbox.selected );					
				if( diluentChkbox.selected ) {
					solventChkbox.selected = false;
					setSolventListVisibility( false );
					dispatchEvent( new RetrieveDrugDiluentListByRxItemEvent(rxItem) );					
				}
			}
			
			public function setSolventListVisibility(val:Boolean):void {				
				solventGroup.visible = val;
				solventGroup.includeInLayout = val;
			}
			
			protected function solventChkboxClick(event:MouseEvent):void {
				setSolventListVisibility( solventChkbox.selected );								
				if( solventChkbox.selected ) {
					diluentChkbox.selected = false;
					setDrugDiluentListVisibility( false );
					if( solventList == null || solventList.length == 0 ) {
						dispatchEvent( new RetrieveSolventListEvent(this) );
					} else {
						setDefaultSolventListItem();
					}
				}			
			}
			
			protected function extendedChkboxClickHandler(event:MouseEvent):void
			{		
				dispatchEvent( new RetrieveWorkstoreDrugListByRxItemEvent(rxItem, extendedChkbox.selected) );				
			}
			
			private function setDefaultFocus():void
			{
				if (_workstoreDrugList && _workstoreDrugList.length > 0) {
					itemListGridSelectedIndex = 0;
				} 
				if (itemListGrid != null) {
					itemListGrid.setFocus();
					itemListGrid.focusManager.showFocus();
				}
			}
		]]>
		
	</fx:Script>
	
	<s:Panel title="Item Code Lookup" width="765">
		<s:layout>
			<s:VerticalLayout paddingTop="10" paddingBottom="10" paddingLeft="10" paddingRight="10" gap="10"/>			
		</s:layout>	
		
		<s:VGroup width="100%" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="0">
			<s:BorderContainer width="100%" height="{itemListLblGroup.height}">
				<s:HGroup id="itemListLblGroup" paddingTop="2" paddingBottom="2" 
						  paddingLeft="2" paddingRight="2" gap="0"
						  verticalAlign="middle">
					<s:Label text="Item List (Drug Name: {drugNameLabel})" fontWeight="bold"/>	
				</s:HGroup>				
			</s:BorderContainer>
			<fc:ExtendedDataGrid id="itemListGrid" width="100%" height="200"
								 selectedIndex="@{itemListGridSelectedIndex}"
								 dataProvider="{_workstoreDrugList}"
								 keyDown="okKeyDownHandler(event)"
								 change="{drugDiluentListGrid.selectedIndex=-1;solventListGrid.selectedIndex=-1}"
								 doubleClickEnabled="true"						 
								 horizontalScrollPolicy="off">
				<fc:columns>
					<mx:DataGridColumn headerText="Suspend" dataField="suspend" width="80"									    
									   draggable="false" sortable="false" resizable="false">
						<mx:itemRenderer>							
							<fx:Component>
								<mx:Label text="{data.msWorkstoreDrug.suspend=='Y'?'Y':''}" paddingLeft="5" paddingRight="5"/>
							</fx:Component>
						</mx:itemRenderer>
					</mx:DataGridColumn>
					<mx:DataGridColumn headerText="Item Code" dataField="dmDrug.itemCode" width="80" draggable="false" sortable="false" resizable="false"/>
					<mx:DataGridColumn headerText="Item Description" dataField="dmDrug.fullDrugDesc" width="645" draggable="false" sortable="false" resizable="false"/>
				</fc:columns>
			</fc:ExtendedDataGrid>
			<s:BorderContainer width="100%" height="{extendedListRecordGroup.height}">				
				<s:HGroup id="extendedListRecordGroup" width="100%" 
						  paddingTop="2" paddingBottom="2" 
						  paddingLeft="2" paddingRight="2" 
						  gap="0" verticalAlign="middle">
					<s:HGroup width="50%" horizontalAlign="left" verticalAlign="middle">
						<s:CheckBox id="extendedChkbox" content="Extended List" click="extendedChkboxClickHandler(event)"/>		
					</s:HGroup>
					<s:HGroup width="50%" horizontalAlign="right" verticalAlign="middle">				
						<s:Label text="{_workstoreDrugList.length} record(s)" textAlign="center" />
					</s:HGroup>
				</s:HGroup>
			</s:BorderContainer>
		</s:VGroup>
		
		<s:VGroup id="diluentGroup" width="100%" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="0">			
			<s:BorderContainer width="100%" height="{drugDiluentListLblGroup.height}">
				<s:HGroup id="drugDiluentListLblGroup" paddingTop="2" paddingBottom="2" paddingLeft="2" paddingRight="2" gap="0">
					<s:Label width="100%" text="Diluent List" fontWeight="bold"/>
				</s:HGroup>
			</s:BorderContainer>
			<fc:ExtendedDataGrid id="drugDiluentListGrid" 
								 dataProvider="{drugDiluentList}"				 
								 change="{itemListGrid.selectedIndex=-1;solventListGrid.selectedIndex=-1}"
								 keyDown="okKeyDownHandler(event)"
								 doubleClickEnabled="true"
								 width="100%" height="200" 
								 horizontalScrollPolicy="off">
				<fc:columns>
					<mx:DataGridColumn headerText="Suspend" dataField="suspend" width="80"						    
									   draggable="false" sortable="false" resizable="false">			
						<mx:itemRenderer>							
							<fx:Component>
								<mx:Label text="{data.msWorkstoreDrug.suspend=='Y'?'Y':''}" paddingLeft="5" paddingRight="5"/>
							</fx:Component>
						</mx:itemRenderer>
					</mx:DataGridColumn>
					<mx:DataGridColumn headerText="Item Code" dataField="dmDrug.itemCode" width="80" draggable="false" sortable="false" resizable="false"/>
					<mx:DataGridColumn headerText="Item Description" dataField="dmDrug.fullDrugDesc" width="645" draggable="false" sortable="false" resizable="false"/>
				</fc:columns>
			</fc:ExtendedDataGrid>
			<s:BorderContainer width="100%" height="{drugDiluentListRecordGroup.height}">				
				<s:HGroup id="drugDiluentListRecordGroup" width="100%" 
						  horizontalAlign="right" verticalAlign="middle"						  
						  paddingTop="2" paddingBottom="2" paddingLeft="2" paddingRight="2" gap="0">
					<s:Label text="{drugDiluentList.length} record(s)"/>
				</s:HGroup>
			</s:BorderContainer>
		</s:VGroup>
		
		<s:VGroup id="solventGroup" width="100%" paddingTop="0" paddingBottom="0" paddingLeft="0" paddingRight="0" gap="0">			
			<s:BorderContainer width="100%" height="{solventListLblGroup.height}">
				<s:HGroup id="solventListLblGroup" paddingTop="2" paddingBottom="2" paddingLeft="2" paddingRight="2" gap="0">
					<s:Label width="100%" text="Solvent List" fontWeight="bold"/>
				</s:HGroup>
			</s:BorderContainer>
			<fc:ExtendedDataGrid id="solventListGrid" dataProvider="{solventList}"						 
								 change="{itemListGrid.selectedIndex=-1;drugDiluentListGrid.selectedIndex=-1}"
								 keyDown="okKeyDownHandler(event)"
								 width="100%" height="200" horizontalScrollPolicy="off">
				<fc:columns>
					<mx:DataGridColumn headerText="Suspend" dataField="suspend" width="80"									    
									   draggable="false" sortable="false" resizable="false">						
						<mx:itemRenderer>							
							<fx:Component>
								<mx:Label text="{data.msWorkstoreDrug.suspend=='Y'?'Y':''}" paddingLeft="5" paddingRight="5"/>
							</fx:Component>
						</mx:itemRenderer>
					</mx:DataGridColumn>
					<mx:DataGridColumn headerText="Item Code" dataField="dmDrug.itemCode" width="80" draggable="false" sortable="false" resizable="false"/>
					<mx:DataGridColumn headerText="Item Description" dataField="dmDrug.fullDrugDesc" width="645" draggable="false" sortable="false" resizable="false"/>
				</fc:columns>
			</fc:ExtendedDataGrid>
			<s:BorderContainer width="100%" height="{solventListRecordGroup.height}">				
				<s:HGroup id="solventListRecordGroup" width="100%" 
						  horizontalAlign="right" verticalAlign="middle"						  
						  paddingTop="2" paddingBottom="2" paddingLeft="2" paddingRight="2" gap="0">
					<s:Label text="{solventList.length} record(s)"/>
				</s:HGroup>
			</s:BorderContainer>
		</s:VGroup>
		
		<s:HGroup width="100%" verticalAlign="middle">
			<s:HGroup width="50%" horizontalAlign="left" visible="{!manualProfileFlag}">				
				<s:CheckBox id="diluentChkbox" content="Diluent List" 
							enabled="{rxItem is IvRxDrug || ( rxItem is InjectionRxDrug &amp;&amp; InjectionRxDrug(rxItem).regimen.firstDose.doseFluid != null )}" 
							click="diluentChkboxClick(event)"/>	
				<s:CheckBox id="solventChkbox" content="Solvent List" click="solventChkboxClick(event)"/>
			</s:HGroup>
			<s:HGroup width="50%" horizontalAlign="right">
				<fc:ExtendedButton label="OK" click="okHandler(event)"/>
				<fc:ExtendedButton label="Cancel" click="cancelHandler(event)"/>					
			</s:HGroup>
		</s:HGroup>		
		
	</s:Panel>	
	
</fc:ExtendedNavigatorContent>
