package hk.org.ha.model.pms.biz.vetting.mp;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.medprofile.MedProfile;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalEcrclLog;
import hk.org.ha.model.pms.persistence.medprofile.MpRenalMessage;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("renalAdjManager")
@MeasureCalls
public class RenalAdjManagerBean implements RenalAdjManagerLocal {

	private static final Integer pastDay = Integer.valueOf(90);

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<MpRenalEcrclLog> retrieveMpRenalEcrclLog(Long medProfileId) {
		
		DateTime dt = new DateTime(new Date());
		Date startDate = dt.minusDays(pastDay.intValue()).toDate();
		Date endDate = dt.plusDays(1).toDate();
		
		return em.createQuery(
				"select o from MpRenalEcrclLog o " + // 20151012 index check : MpRenalEcrclLog.medProfile,createDate : I_MP_RENAL_ECRCL_LOG_01
				"where o.medProfile.id = :medProfileId " +
				"and o.createDate between :startDate and :endDate " +
				"order by o.createDate DESC ")
				.setParameter("medProfileId", medProfileId)
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<MpRenalMessage> retrieveMpRenalMessage(Long medProfileId) {
		
		DateTime dt = new DateTime(new Date());
		Date startDate = dt.minusDays(pastDay.intValue()).toDate();
		Date endDate = dt.plusDays(1).toDate();
		
		return em.createQuery(
				"select o from MpRenalMessage o " + // 20151012 index check : MpRenalMessage.medProfile,createDate : I_MP_RENAL_MESSAGE_01
				"where o.medProfile.id = :medProfileId " +
				"and o.createDate between :startDate and :endDate " +
				"order by o.createDate DESC ")
				.setParameter("medProfileId", medProfileId)
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				.getResultList();
	}

	public void save(List<MpRenalEcrclLog> mpRenalEcrclLogList, List<MpRenalMessage> mpRenalMessageList, MedProfile medProfile) {
		for (MpRenalEcrclLog mpRenalEcrclLog : mpRenalEcrclLogList) {
			mpRenalEcrclLog.setMedProfile(medProfile);
			em.persist(mpRenalEcrclLog);
		}
		for (MpRenalMessage mpRenalMessage : mpRenalMessageList) {
			mpRenalMessage.setMedProfile(medProfile);
			em.persist(mpRenalMessage);
		}
		em.flush();
	}
}
