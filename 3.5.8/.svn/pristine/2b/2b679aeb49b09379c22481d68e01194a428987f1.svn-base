package hk.org.ha.model.pms.persistence.reftable;

import flexjson.JSON;
import hk.org.ha.model.pms.corp.cache.DmDrugCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.vo.drug.DmDrugLite;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.jboss.seam.security.Identity;

@Entity
@Table(name = "CHEST_ITEM")
public class ChestItem extends VersionEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chestItemSeq")
	@SequenceGenerator(name = "chestItemSeq", sequenceName = "SQ_CHEST_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
	
	@Column(name = "DOSE_QTY", precision=10, scale=4, nullable = false)
	private BigDecimal doseQty;
	
	@Column(name = "DOSE_UNIT", nullable = false, length = 15)
	private String doseUnit;

	@Column(name = "SORT_SEQ")
	private Integer sortSeq;
	
	@ManyToOne
	@JoinColumn(name = "CHEST_ID", nullable = false)
	private Chest chest;
			
	@Transient
	private DmDrug dmDrug;	
			
	@Transient
	private DmDrugLite dmDrugLite;	

	@Transient
	private boolean markDelete = false;	

	@PostLoad
	public void postLoad() {
		if (itemCode != null && Identity.instance().isLoggedIn()) {
			dmDrug = DmDrugCacher.instance().getDmDrug(itemCode);
		}
	}		

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public BigDecimal getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(BigDecimal doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	@JSON(include=false)
	public Chest getChest() {
		return chest;
	}

	public void setChest(Chest chest) {
		this.chest = chest;
	}
	
	@JSON(include=false)
	public DmDrug getDmDrug() {
		return dmDrug;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}	

	public boolean isMarkDelete() {
		return markDelete;
	}

	public void setMarkDelete(boolean markDelete) {
		this.markDelete = markDelete;
	}

	public void setDmDrugLite(DmDrugLite dmDrugLite) {
		this.dmDrugLite = dmDrugLite;
	}

	public DmDrugLite getDmDrugLite() {
		return dmDrugLite;
	}

	public void setSortSeq(Integer sortSeq) {
		this.sortSeq = sortSeq;
	}

	public Integer getSortSeq() {
		return sortSeq;
	}

}
