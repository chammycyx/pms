@echo off

if "%1" == "deploy" goto START
if "%1" == "undeploy" goto START
if "%1" == "redeploy" goto START
if "%1" == "start-app" goto START
if "%1" == "stop-app" goto START
if "%1" == "list-apps" goto START
if "%1" == "update-app" goto START
if "%1" == "help" goto START

echo Usage: weblogic (deploy ^| undeploy ^| redeploy ^| start-app ^| stop-app ^| list-apps ^| update-app ^| help)

goto EOF

:START

if "%DATABASE_ID%" == "" (
	set DATABASE_ID=%WORKSTATION_ID%
) 

for %%* in (.) do set CURR_FOLDER=%%~n*

@echo on

cd %CURR_FOLDER%-ear

call mvn weblogic:%*

cd ..

:EOF